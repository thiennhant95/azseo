<?php
if( session_id() == "") { session_start(); }
$arr     = array();
$error   = 'success';
$message = '';
$result  = '';

switch (@$method) {
    case 'add':

        // Nạp thông tin
        if( !is_login() ) {
            $_SESSION['user_ten']         = ws_get('hoten');
            $_SESSION['user_sodienthoai'] = ws_get('dienthoai');
            $_SESSION['user_email']       = ws_get('email');
            $_SESSION['user_ghichu']      = ws_get('ghichu');
        }

        if ( !isset($_SESSION['giohang']) ) {
            $_SESSION['giohang'] = array();
        }
        // neu da co san pham thi them so luong san pham
        $arrgiohang  = $_SESSION['giohang'];
        $tongsanpham = count($arrgiohang);
        $flag        = 0;
        $soluong     = 1;
        if ($_GET['sl'] > 0) {
            $soluong = $_GET['sl'];
        }
        if ($tongsanpham > 0) {
            $arraytemp = array();
            $arraytemp = $_SESSION['giohang'];
            for ($i = 0; $i < $tongsanpham; ++$i) {
                if ($_SESSION['giohang'][$i]['idsp'] == $id) {
                    $soluong = $_SESSION['giohang'][$i]['soluong'];
                    if ( $soluong > 999) {
                        $error = "quantity_unknown";
                        $soluong = 1;
                    }
                    if ($_GET['sl'] > 0) {
                        $soluong += $_GET['sl'];
                    } else {
                        $soluong += 1;
                    }
                    $_SESSION['giohang'][$i]['soluong'] = $soluong;
                    $flag                               = 1;
                    break;
                }
            }
        }

        // Them sản phẩm vào giỏ hàng
        if ($flag == 0) {
            // truy van lay du lieu
            $ten = $db->getNameFromID('tbl_noidung_lang', 'ten', 'idnoidung', "{$id} and idlang = {$_SESSION['_lang']}");
            // GIa sale
            if( $db->isSale($id) ){
                $gia = $db->getNameFromID('tbl_noidung', 'giasale', 'id', $id);
            }
            else {
                $gia = $db->getNameFromID('tbl_noidung', 'gia', 'id', $id);
            }
            $hinh = $db->getNameFromID('tbl_noidung', 'hinh', 'id', $id);
            $arraytemp[$tongsanpham]['idsp']    = $id;
            $arraytemp[$tongsanpham]['ten']     = $ten;
            $arraytemp[$tongsanpham]['hinh']    = $hinh;
            $arraytemp[$tongsanpham]['soluong'] = $soluong;
            $arraytemp[$tongsanpham]['gia']     = $gia;
            $_SESSION ['giohang']               = $arraytemp;
        }
        break;

    case 'update':
        $arraytemp = $_SESSION['giohang'];
        $geti      = $_GET['i'];
        $getsl     = $_GET['sl'];
        if ( $getsl > 999) {
            $error = "quantity_unknown";
            $arraytemp[$geti]['soluong'] = 1;
        }
        if ($getsl > 0) {
            $arraytemp[$geti]['soluong'] = $getsl;
        }
        $_SESSION['giohang'] = $arraytemp;
        break;

    case 'del':
        $arraytemp = array();
        $arraytemp = $_SESSION['giohang'];
        $geti      = $_GET['i'];
        $soluongsp = count($_SESSION['giohang']);
        if ($geti != '') {
            for ($t = $geti; $t < ($soluongsp - 1); ++$t) {
                $arraytemp[$t] = $arraytemp[$t + 1];
            }
            unset($arraytemp[$soluongsp - 1]);
        }
        $_SESSION['giohang'] = $arraytemp;
        break;
}
//xuat gio hang
$data_giohang = '';
$tongsanpham  = count($_SESSION['giohang']);
if ($tongsanpham > 0) {
    $tongtien = 0;
    $data_giohang .= '
   <div class="giohang_group">';
    foreach ($_SESSION['giohang'] as $key_giohang => $info_giohang) {
        $hinh = $_SESSION['ROOT_PATH'] . 'uploads/noidung/thumb/' . $info_giohang['hinh'];
        $idsp = $info_giohang['idsp'];
        // lay ten tu database và lấy theo ngôn ngữ
        $ten  = @$db->getNameFromID("tbl_noidung_lang", "ten", "idnoidung", "'{$idsp}' and idlang = {$_SESSION['_lang']}");
        $soluong  = $info_giohang['soluong'];
        $gia = $info_giohang['gia'];
        $tongtien += ($soluong * $gia);
        $url = $db->getNameFromID("tbl_noidung_lang", "url", "idnoidung", "'{$idsp}' and idlang = {$_SESSION['_lang']}");
        $link_sanpham        = $_SESSION['ROOT_PATH'] . $url;
        $link_giohang_update = $_SESSION['ROOT_PATH'] . "ajax/?op=giohang&id={$idsp}&i={$key_giohang}&method=update";
        $link_giohang_del    = $_SESSION['ROOT_PATH'] . "ajax/?op=giohang&id={$idsp}&i={$key_giohang}&method=del";
        $data_giohang .= '
            <div class="product_box clearfix">
                <div class="product_img">
                    <img onerror="xulyloi(this)" src="' . $hinh . '" alt="' . $tensanpham . '" width="70"  />
                </div><!-- ./product_img -->
                <div class="product_info">
                    <div class="name">
                        <div title="' . $arraybien['xoasanpham'] . '" class="deletesp" onclick="MuaHang(\'' . $link_giohang_del . '\',\'loaddatacart\')">x</div>
                        <a href="' . $link_sanpham . '" title="' . $ten . '">' . $ten . '</a>
                    </div><!-- ./name -->
                </div><!-- ./product_info -->
                <div class="product_sl clearfix">
                    <ul>
                        <li class="li1">' . $arraybien['soluong'] . '</li>
                        <li><input onchange="MuaHang(\'' . $link_giohang_update . '&sl=\'+this.value,\'loaddatacart\')" onkeyup="MuaHang(\'' . $link_giohang_update . '&sl=\'+this.value,\'loaddatacart\')" class="txt_soluong" name="C' . $key_giohang . '" type="number" min="1" max="100000" value="' . $soluong . '" size="4" maxlength="4" /></li>
                        <li>&nbsp; X &nbsp;' . number_format($gia) . ' đ = ' . number_format($soluong * $gia) . ' đ</li>
                    </ul>
                </div><!-- ./product_sl -->
            </div><!-- ./product_box -->
            <div class="product_line_nhe"></div><!-- ./product_line_nhe -->';
    }
    $data_giohang .= '
   </div><!-- ./giohang_group -->
   <div class="tongtien_giohang">
      ' . $arraybien['tongtien'] . ': ' . number_format($tongtien) . 'VNĐ
   </div><!-- ./tongtien_giohang -->';
    $data_giohang .= '<div class="xemgiohang btn btn-success"><a href="' . $_SESSION['ROOT_PATH'] . 'cart.htm" title="' . $arraybien['thanhtoan'] . '">' . $arraybien['thanhtoan'] . '</a></div><!-- ./xemgiohang -->';
    $data_giohang .= '<div class="clear"></div>';
} else {
    $data_giohang .= $arraybien['giohangrong'];
}

if ($_GET['divReturn'] == "giohang_load_view") {
    # RESULT CHO GIO HANG VIEW
    if ($tongsanpham > 0) {
        $tongtien = 0;
        $resultView .= '
       <div class="giohang_group_view" id="giohang_load_view">
          <div class="panel panel-default">
          <div class="panel-heading"><i class="fa fa-shopping-cart fa-2x"></i> ' . $arraybien['giohangcuaban'] . '</div>
          <div class="panel-body">';
        foreach ($_SESSION['giohang'] as $key_giohang => $info_giohang) {
                $hinh = $_SESSION['ROOT_PATH'].'uploads/noidung/thumb/'.$info_giohang['hinh'];
                $idsp = $info_giohang['idsp'];
            // lay ten tu database và lấy theo ngôn ngữ
            $ten                 = $db->getNameFromID('tbl_noidung_lang', 'ten', 'idnoidung', $idsp.' and idlang = '.$_SESSION['_lang']);
            $soluong             = $info_giohang['soluong'];
            $gia                 = $info_giohang['gia'];
            $tongtien            += ($soluong * $gia);
            $url                 = $db->getNameFromID('tbl_noidung_lang', 'url', 'idnoidung', $idsp.' and idlang = '.$_SESSION['_lang']);
            $link_sanpham        = $_SESSION['ROOT_PATH'].$url;
            $link_giohang_update = $_SESSION['ROOT_PATH'].'ajax/?op=giohang&id='.$idsp.'&i='.$key_giohang.'&method=update';
            $link_giohang_del    = $_SESSION['ROOT_PATH'].'ajax/?op=giohang&id='.$idsp.'&i='.$key_giohang.'&method=del';
                $resultView .= '
            <div class="product_box">
             <div class="product_img">
                <img onerror="xulyloi(this)" src="' .$hinh.'" alt="'.$tensanpham.'" width="70"  />
             </div>
             <div class="product_info">
                <div class="name"><div title="' .$arraybien['xoasanpham'].'" class="deletesp" onclick="MuaHang(\''.$link_giohang_del.'\',\'giohang_load_view\')">x</div><a href="'.$link_sanpham.'" title="'.$ten.'">'.$ten.'</a></div>
             </div>';
                $resultView .= '
             <div class="product_sl">
                <ul>
                   <li class="li1">' .$arraybien['soluong'].'</li>
                   <li><input onchange="MuaHang(\'' .$link_giohang_update.'&sl=\'+this.value,\'giohang_load_view\')" onkeyup="MuaHang(\''.$link_giohang_update.'&sl=\'+this.value,\'giohang_load_view\')" class="txt_soluong" name="C'.$key_giohang.'" type="number" min="1" max="100000" value="'.$soluong.'" size="4" maxlength="4" /></li>
                   <li>&nbsp; X &nbsp;' .number_format($gia).' đ = '.number_format($soluong * $gia).' đ</li>
                </ul>
             </div>
             <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="product_line_nhe"></div>';
            }
        $resultView .= '
       <div class="tongtien_giohang">
          ' . $arraybien['tongtien'] . ': ' . number_format($tongtien) . ' VNĐ
       </div>
       ';
        $resultView .= '
       <div class="clear"></div>
       <div class="xoagiohang">
          <a onclick="if(confirm(\'' . $arraybien['xoagiohang'] . '\')){ return true;}else{return false; }" href="' . $_SESION['ROOT_PATH'] . 'delete-cart.htm" title="' . $arraybien['xoagiohang'] . '">' . $arraybien['xoagiohang'] . '<i class="fa fa-times-circle fa-lg"></i></a>
       </div>
       <a href="'.ROOT_PATH.'" title="'.$arraybien['tieptucmuahang'].'"><input class="btn btn-primary btn_submit" type="submit" name="thanhtoan_muatiep" id="thanhtoan_muatiep" value="' . $arraybien['tieptucmuahang'] . '" /></a>
       ';
        $resultView .= '<div class="clear"></div>
       </div>'; // end div tong
    } else {
        $resultView .= $arraybien['giohangrong'];
    }
    $resultView .= '</div>
    </div>';

    $result = $resultView;
} else {
    # RESULT CHO POPUP
    $result = '

<style type="text/css">
    '.file_get_contents('../smarty/templates/layout1/css/cart.css').'
</style>

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">' . $arraybien['giohangcuaban'] . '</h4>
        </div><!-- ./modal-header -->
        <div class="modal-body">
            ' . $data_giohang . '
        </div><!-- ./modal -->
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">' . $arraybien['dong'] . '</button>
        </div><!-- ./modal-footer -->
    </div>';
}

$arr['error']    = $error;
$arr['message']  = $message;
$arr['result']   = $result;
$arr['method']   = $method;
$arr['quantity'] = $tongsanpham;

echo json_encode($arr);
