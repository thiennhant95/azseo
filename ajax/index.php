<?php
require '../configs/inc.php';

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// show_error();

$id         = ws_get('id');
$method     = ws_get('method');
$name       = ws_get('name');
$value      = ws_get('value');
$op         = ws_get('op');
$act        = ws_get('act');
    // echo '<pre>'; print_r($method); echo '</pre>'; exit();
if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
        // The request is using the POST method
    $id     = ws_post('id');
    $method = ws_post('method');
    $name   = ws_post('name');
    $value  = ws_post('value');
    $value2 = ws_post('value2');
    $value3 = ws_post('value3');
    $value4 = ws_post('value4');
    $op     = ws_post('op');
    $act    = ws_post('act');
}

$status     = 'success';
$result     = '';
$message    = '';
$urlreturn  = '';
$data       = '';
$dataupdate = '';




if ( $op ) {
    // chi include khi co ton tai op
    if (!empty($act)) {
        include "../modules/".$op."/".$act.'.php';
    } else {
        include $op.'.php';
    }
} else {
    /*Quay ngược lại trang vừa chuyển tới*/
    $previous = 'javascript:history.go(-1)';
    if (isset($_SERVER['HTTP_REFERER'])) {
        $previous = $_SERVER['HTTP_REFERER'];
    }

    switch ( $method ) {
        case 'historyPoint':
            $result.= '
            <div class="modal-content">
                <div class="closepopup" data-dismiss="modal"></div>
                <div class="modal-body">
                    <table id="lichsuTichdiem" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã đơn hàng</th>
                                <th>Số điểm</th>
                                <th>Lịch sử</th>
                                <th>Ngày</th>
                            </tr>
                        </thead>
                        <tbody>';
                    $qtd = $db->rawQuery("SELECT *
                        FROM tbl_tichdiem {$qtd_bonus} WHERE iduser = ".get_id_user()." ORDER BY id DESC
                    ");
                    if( count($qtd) > 0 ) {
                        foreach( $qtd as $ktd => $td ) {
                            // checkdonhang
                            $checkdh = $db->getNameFromID(
                                "tbl_donhang",
                                "id",
                                "id",
                                $td['iddonhang']
                            );
                            // checkUser
                            $checkuser = $db->getNameFromID(
                                "tbl_user",
                                "id",
                                "id",
                                $td['iduser']
                            );
                            if( !$checkdh || !$checkuser ) {
                                $db->where("id = {$td['id']}");
                                $db->delete("tbl_tichdiem");
                            }
                            $ten = $db->getUser('ten', $td['iduser']);
                            $ten = $ten ? $ten : '<i class="fa fa-user-times fa-2x" aria-hidden="true" style="font-size:25px;"></i>';
                            $madonhang = $db->getNameFromID(
                                'tbl_donhang',
                                'madonhang',
                                'id',
                                $td['iddonhang']
                            );
                            $result.= '
                            <tr>
                                <td>'.($ktd+1).'</td>
                                <td>'.$madonhang.'</td>
                                <td>'.round($td['sodiem'],3).'</td>
                                <td>'.$td['lichsu'].'</td>
                                <td>'.date('H:i:s - d/m/Y', strtotime($td['ngay'])).'</td>
                            </tr>';
                        }
                    }
                        $result.='
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>STT</th>
                                <th>Mã đơn hàng</th>
                                <th>Số điểm</th>
                                <th>Lịch sử</th>
                                <th>Ngày</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->';
            echo json_encode([
                'result' => $result
            ]);
            break;
        case 'getmagioithieu':
            $result = strtoupper(random_str());
            $db->where('id', get_id_user());
            $db->update('tbl_user', [
                'magioithieu' => $result
            ]);
            echo json_encode([
                'result' => $result
            ]);
            break;
        case 'sendtuvan':
            if (!empty($value)) {
                    # Check Token
                $getValue = strip_tags($value);

                $idInsert = $db->insert("tbl_dangky", [
                    "noidung" => $getValue,
                    "ngaygui" => $db->getDateTimes()
                ]);

                if (!$idInsert) {
                    $status = 'fail';
                    $message = $arraybien['coloixayravuilonglienhebqt'];
                } else {
                    $message = $arraybien['yeucaucuabandaduocguithanhcong'];
                        # SEND MAIL
                    if ( $_getArrayconfig['sendemail'] == 'on' ) {
                        $getValue = preg_replace('/<[^>]*>/', '', $value);
                        $getValue = stripslashes(html_entity_decode($value));
                        $subject  = 'Tư vấn ' . $_SERVER['HTTP_HOST'];
                        $mgs      = $db->getThongTin('noidungmailtuvan');

                        if( $mgs ){
                                //THONGTINMAILHOPTAC
                            $arrInfo      = json_decode($getValue, true);
                            $guestName    = $arrInfo['hoten'];
                            $guestPhone   = $arrInfo['sodienthoai'];
                            $guestEmail   = $arrInfo['email'];
                            $templates = '
                            <table cellspacing="0" cellpadding="10" border="1">
                            <tbody>
                            <tr>
                            <td>Họ tên</td>
                            <td><b>'.$guestName.'</b></td>
                            </tr>
                            <tr>
                            <td>Điện thoại</td>
                            <td><b>'.$guestPhone.'</b></td>
                            </tr>
                            <tr>
                            <td>Email</td>
                            <td><b>'.$guestEmail.'</b></td>
                            </tr>
                            </tbody>
                            </table> ';
                            $mgs = str_replace("[THONGTINMAILTUVAN]", $templates, $mgs);

                                // hot mail
                            sendmail([
                                "emailnhan" => $_getArrayconfig['email_nhan'],
                                "emailgui"  => $_getArrayconfig['hostmail_user'],
                                "hostmail"  => $_getArrayconfig['hostmail'],
                                "user"      => $_getArrayconfig['hostmail_user'],
                                "pass"      => $_getArrayconfig['hostmail_pass'],
                                "tieude"    => $subject,
                                "fullname"  => $_getArrayconfig['hostmail_fullname'],
                                "port"      => 25,
                                "ssl"       => 0,
                                "subject"   => $subject,
                                "message"   => $mgs,
                            ]);
                        }
                    }
                }

            }// End check value
            break;

        case 'saveproduct':
            $arrtmp = (array) json_decode($_COOKIE['productsave']);

            if( in_array($value, $arrtmp) ) {
                echo '<i class="fa fa-bookmark-o"></i>';
            }
            else {
                echo '<i class="fa fa-bookmark"></i>';
            }
            cookie_array($value, 4096, 'productsave');

            break;

        case 'nhantuvan':
            if (!empty($value)  ) {
                // Kiem tra email da ton tai chua
                $value = trim( strip_tags( $value ) );
                $value2= trim( strip_tags( ws_get('value2')) );
                $value3= trim( strip_tags( ws_get('value3')) );

                $sExi = "
                SELECT id
                FROM tbl_dangkymail
                WHERE email = '{$value}' ";
                $qExi = $db->sqlSelectSql($sExi);

                if ( count($qExi) > 0 ) {
                    $status  = 'exists';
                    $message = $arraybien['emailnaydaduocdangky'];
                } else {
                    $sDkmail = array(
                        'email'       => $value,
                        'hoten'       => $value2,
                        'sodienthoai' => $value3,
                        'ngaygui'     => $db->NgayHienTaiMysql(),
                        'thutu'       => $db->thuTu("tbl_dangkymail")
                    );
                    $idDk = $db->sqlInsert("tbl_dangkymail", $sDkmail);

                    # SEND MAIL
                    if ( $_getArrayconfig['sendemail'] == 'on' ) {
                        $value   = preg_replace('/<[^>]*>/', '', $value);
                        $value   = stripslashes(html_entity_decode($value));
                        $subject = 'Tư vấn ' . $_SERVER['HTTP_HOST'];
                        $mgs     = $db->getThongTin('noidungmailtuvan');

                        if( $mgs ){
                            //THONGTINMAILHOPTAC
                            $arrInfo      = json_decode($value, true);
                            $guestEmail   = $value;
                            $guestName    = $value2;
                            $guestPhone   = $value3;
                            $templates = '
                            <table cellspacing="0" cellpadding="10" border="1">
                            <tbody>
                            <tr>
                            <td>Email</td>
                            <td><b>'.$guestEmail.'</b></td>
                            </tr>
                            <tr>
                            <td>Họ tên</td>
                            <td><b>'.$guestName.'</b></td>
                            </tr>
                            <tr>
                            <td>Điện thoại</td>
                            <td><b>'.$guestPhone.'</b></td>
                            </tr>
                            </tbody>
                            </table> ';
                            $mgs = str_replace("[THONGTINMAILTUVAN]", $templates, $mgs);

                            // hot mail
                            sendmail([
                                "emailnhan" => $_getArrayconfig['email_nhan'],
                                "emailgui"  => $_getArrayconfig['hostmail_user'],
                                "hostmail"  => $_getArrayconfig['hostmail'],
                                "user"      => $_getArrayconfig['hostmail_user'],
                                "pass"      => $_getArrayconfig['hostmail_pass'],
                                "tieude"    => $subject,
                                "fullname"  => $_getArrayconfig['hostmail_fullname'],
                                "port"      => 25,
                                "ssl"       => 0,
                                "subject"   => $subject,
                                "message"   => $mgs,
                            ]);
                        }
                    }

                    if ( $idDk ) {
                        $message = $arraybien['dangkythanhcong'];
                    } else {
                        $message = $arraybien['coloixayravuilonglienhebqt'];
                    }
                }
            } else {
                $message = $arraybien['coloixayravuilonglienhebqt'];
            }
            $arr = array(
                'message' => $message,
                'status'  => $status);
            echo json_encode($arr);
            break;

        case 'dangkytuvan' :
            $message = '';
            $message.='
            <div class="modal-content">
            <div class="closepopup" data-dismiss="modal"></div>

            <div class="modal-body">

            <div class="contentlogin">

            <h2>'.$arraybien['vuilongnhapthongtin'].'</h2>

            <div class="loginwidth">
            <input type="text" name="emailtuvan2" class="emailtuvan" placeholder="'.$arraybien['email'].'" />
            <input type="text" name="hotentuvan2" class="hoten" placeholder="'.$arraybien['hovaten'].'"/>
            <input type="text" name="sdttuvan2" class="sdttuvan" placeholder="'.$arraybien['sodienthoai'].'" />
            <button type="button" class="nhanthongtin" data-loading-text="<i class=\'fa fa-spinner fa-spin fa-lg fa-fw\'></i>" id="dkmailBtn2">'.$arraybien['guithongtin'].'</button>
            </div>

            </div>
            </div>
            </div>';
            echo $message;
            break;

        case 'danhmuccon':
            $idCate = $_GET['id'];

            $dSP = $db->layNoiDung([
                "where" => "AND a.idtype LIKE '%{$idCate}%'
                AND colvalue LIKE '%home%'",
                "orderby" => ['thutu','Asc'],
                "limit" => 50
            ]);

            $status = null;

            $smarty->assign('arrsanpham',$dSP);
            $smarty->assign('root_path', $_SESSION['ROOT_PATH']);
            $result = $smarty->fetch(
                __DIR__."/../templates/layout1/modules/product/tpl/item.tpl");

            if($dSP>0){
                $status = 'success';
            }
            echo json_encode(array(
                'status' => $status,
                'result' => $result
            ));

            break;

        case 'dathang':
            $thisProduct = $db->sqlSelectSql("
                SELECT a.id,b.ten,a.hinh,url,gia
                FROM tbl_noidung a
                INNER JOIN tbl_noidung_lang b
                ON a.id = b.idnoidung
                WHERE anhien = 1
                AND a.id = {$id}
                ");
            $sp = $thisProduct[0];

            echo '
            <div class="modal-content">

            <div class="panel-body" id="buy_frm">
            <span class="close" data-dismiss="modal">&times;</span>

            <div class="product-info clearfix form-group">

            <span class="image">
            <img src="'.$_SESSION['ROOT_PATH'].'uploads/noidung/'.$sp['hinh'].'" alt="'.$sp['ten'].'" title="'.$sp['ten'].'" class="img-responsive" />
            </span>

            <span class="info">
            <strong>'.$sp['ten'].'</strong>';
            if( $sp['gia'] != '' ){
                echo '
                <p>'.number_format($sp['gia']).'<sup>đ</sup></p>';
            }
            echo'
            </span>

            </div>

            <div class="form-group"></div>

            <div class="form-group">
            <div class="h2">'.$arraybien['thongtinkhachhang'].'</div>
            </div>

            <div class="product-form clearfix" id="frmOrder">

            <div class="form-group hoten">
            <div class="row">
            <div class="col-xs-12 col-sm-3">
            <label>'.$arraybien['tendaydu'].'</label>
            </div>

            <div class="col-xs-12 col-sm-9">
            <input type="text" name="hoten" class="form-control ws-required" placeholder="'.$arraybien['tendaydu'].'" />
            </div>
            </div>
            </div>

            <div class="form-group">
            <div class="row">
            <div class="col-xs-12 col-sm-3">
            <label>'.$arraybien['sodienthoai'].'</label>
            </div>

            <div class="col-xs-12 col-sm-9">
            <input type="text" name="dienthoai" onkeyup="CheckNumber(this);" class="form-control ws-required ws-required-number" data-wserror="'.$arraybien['sodienthoai'].'" placeholder="'.$arraybien['sodienthoai'].'" />
            </div>
            </div>
            </div>

            <div class="form-group">
            <div class="row">
            <div class="col-xs-12 col-sm-3">
            <label>'.$arraybien['diachigiaohang'].'</label>
            </div>

            <div class="col-xs-12 col-sm-9">
            <input type="text" name="diachi" class="form-control ws-required" placeholder="'.$arraybien['diachigiaohang'].'" />
            </div>
            </div>
            </div>

            <div class="form-group">
            <div class="row">
            <div class="col-xs-12 col-sm-3">
            <label>'.$arraybien['ghichu'].'</label>
            </div>

            <div class="col-xs-12 col-sm-9">
            <textarea class="form-control ws-required" rows="4" name="ghichu" placeholder="'.$arraybien['ghichu'].'"></textarea>
            </div>
            </div>
            </div>

            <div class="form-group">
            <input type="hidden" name="id" value="'.$sp['id'].'" readonly />
            </div>

            <div class="form-group pull-right">
            <button type="button" id="buy_process" class="btn btn-info">
            <i class="fa fa-check"></i>
            '.$arraybien['dathang'].'
            </button>
            </div>

            </div>

            </div>

            </div>';
            die;
            break;

        // ĐÁNH GIÁ
        case 'rating':

            // Rate lan dau tien
            if( ! isset($_SESSION['rating'.$id]) ){

                $_SESSION['rating'.$id] = array(
                    'id'    => $id,
                    'value' => $value
                );

                // Bai viet co ton tai
                $baiCheck = $db->getNameFromID(
                    "tbl_noidung",
                    "id",
                    "id",
                    "'{$id}'"
                );
                if( ! empty($baiCheck) ){
                    // Update rating
                    $db->rawQuery("
                        UPDATE tbl_noidung
                        SET rating = (rating + {$value})
                        WHERE id = '{$id}'
                        ");

                    // Update ratingCount
                    $db->rawQuery("
                        UPDATE tbl_noidung
                        SET ratingCount = (ratingCount + 1)
                        WHERE id = '{$id}'
                        ");
                }
            }else{
            // Rating again

                // Rate cao hơn rate cũ
                if( $_SESSION['rating'.$id]['value'] < $value ){
                    $rate_diff = $value - $_SESSION['rating'.$id]['value'];
                    $db->rawQuery("
                        UPDATE tbl_noidung
                        SET rating = (rating + {$rate_diff})
                        WHERE id = '{$id}'
                        ");
                }elseif($_SESSION['rating'.$id]['value'] > $value){
                // Rate lại thấp hơn rate cũ
                    $rate_diff = $_SESSION['rating'.$id]['value'] - $value;
                    $db->rawQuery("
                        UPDATE tbl_noidung
                        SET rating = (rating - {$rate_diff})
                        WHERE id = '{$id}'
                        ");
                }

                // Cap nhat lai gia tri cho session
                $_SESSION['rating'.$id]['value'] = $value;
            }

            $output = array(
                "status" => $status
            );
            echo json_encode($output);
            break;

        // CHẾ ĐỘ XEM
        case 'flow':
            switch ($value) {
                case 2:
                $_SESSION['flow'] = 2;
                header('Location:'.$previous);
                break;
                default:
                $_SESSION['flow'] = 1;
                header('Location:'.$previous);
                break;
            }
            break;

        // CHẾ ĐỘ XEM TRONG NỘI DUNG
        case 'flowND':
            switch ($value) {
                case 2:
                $_SESSION['flowND'] = 2;
                header('Location:'.$previous);
                break;
                default:
                $_SESSION['flowND'] = 1;
                header('Location:'.$previous);
                break;
            }
            break;

        // KIỂM TRA MẬT KHẨU
        case 'passcheck':
            $passcheck = ws_get('passcheck');
            $getLat = $db->getNameFromID("tbl_user","lat","id",$_SESSION['user_id']);
            $passCompare = md5(md5($passcheck).$getLat);//f0dbb37b19ac09b69eb8c3508e2ac9b9

            $q = $db->sqlSelectSql("
                SELECT id FROM tbl_user
                WHERE matkhau = '{$passCompare}'
                ");

            if ( count($q) > 0 ) {
                echo true;
            } else {
                echo false;
                $message = $arraybien['matkhauhientaikhongdung'];
            }
            die;
            break;

        // ĐĂNG XUẤT
        case 'logout':

            unset($_SESSION['user_id']);
            $error = 'success';

            if (isset($_COOKIE['username'])) {
                unset($_COOKIE['username']);
                unset($_COOKIE['password']);
                setcookie('username', null, -1, '/', $_SERVER['HTTP_HOST']);
                setcookie('password', null, -1, '/', $_SERVER['HTTP_HOST']);
            }

            $out = array("error" => $error);
            echo json_encode($out);
            break;

        // LOAD FORM ĐĂNG KÝ
        case 'register':
            $message = null;
            if( !is_login() ){
                $message.='
                <div class="modal-content">
                    <div class="closepopup" data-dismiss="modal"></div>
                    <div class="modal-body" id="datalogin">
                        <div class="contentlogin">
                            <h2>'.$arraybien['dangkytaikhoan'].'</h2>


                            <div class="dangkyvoi">
                               <strong>'.$arraybien['dangkyvoi'].'</strong>
                            </div>

                            <input type="text" class="form-control" name="hovaten" data-require="'.$arraybien['vuilongnhaphoten'].'" placeholder="'.$arraybien['hovaten'].'" />

                            <br />

                            <input type="text" class="form-control" name="email" data-require="'.$arraybien['vuilongnhapemail'].'" placeholder="'.$arraybien['email'].'" autocomplete="off" />

                            <br />

                            <input type="text" class="form-control" name="manguoigioithieu" placeholder="'.$arraybien['magioithieu'].'" value="'.@$_SESSION['manguoigioithieu'].'" autocomplete="off" />

                            <br />

                            <div class="gioitinh">
                                <i class="fa fa-mars"></i> '.$arraybien['gioitinh'].':
                                <label><input type="radio" checked="checked" name="gioitinh" value="0">'.$arraybien['nam'].'</label>
                                <label><input type="radio" name="gioitinh" value="1">'.$arraybien['nu'].'</label>
                            </div>

                            <br />

                            <input type="password" class="form-control" name="password" data-require="'.$arraybien['vuilongnhapmatkhau'].'" placeholder="'.$arraybien['matkhau'].'" autocomplete="new-password" />

                            <br />

                            <input type="password" class="form-control" name="repassword" data-require="'.$arraybien['xacnhanmatkhau'].'" data-matkhausai="'.$arraybien['matkhausai'].'" placeholder="'.$arraybien['nhaplaimatkhau'].'" autocomplete="new-password" />

                            <br />

                            <div class="btnlogin" id="registerid">'.$arraybien['dangky'].'</div>

                            <br />

                            <div class="labeldangky">'.$arraybien['neubandacotaikhoan'].' <a href="#" onclick="Get_Data(\''.$_SESSION['ROOT_PATH'].'ajax/?method=login\',\'loaddatacart\');"  >'.$arraybien['dangnhap'].'</a></div>
                        </div>
                    </div><!-- ./modal -->
                </div>';
            }else{
                $message = is_login_template();
            }
            echo $message;
            break;


        // LOAD FORM ĐĂNG NHẬP
        case 'login':
            $message = '';
            if( !is_login() ){
                $message.='
                <div class="modal-content">
                <div class="closepopup" data-dismiss="modal"></div>
                <div class="modal-body" id="datalogin">
                <div class="contentlogin">
                <h2>'.$arraybien['dangnhap'].'</h2>


                <input type="text" class="form-control" data-require="'.$arraybien['vuilongnhapemail'].'" name="email" placeholder="'.$arraybien['emaildangnhap'].'" /><br />

                <input type="password" class="form-control" data-require="'.$arraybien['vuilongnhapmatkhau'].'" name="password" placeholder="'.$arraybien['matkhau'].'" /><br />

                <label><input type="checkbox" name="remember" value="1">'.$arraybien['nhotoi'].'</label>

                <a onclick="Get_Data(\''.$_SESSION['ROOT_PATH'].'ajax/?method=forgotpassword\',\'loaddatacart\');" href="#" class="forgotpassword">'.$arraybien['quenmatkhau'].'</a>

                <div class="btnlogin" id="loginid" data-loginerror="'.$arraybien['saiemailhoacmatkhau'].'" data-loginsuccess="'.$arraybien['dangnhapthanhcong'].'">'.$arraybien['dangnhap'].'</div><br />

                <div class="labeldangky">'.$arraybien['neubanchuacotaikhoan'].' <a href="#" onclick="Get_Data(\''.$_SESSION['ROOT_PATH'].'ajax/?method=register\',\'loaddatacart\');"  >'.$arraybien['dangkyngay'].'</a></div>
                </div>
                </div><!-- ./modal -->
                </div>';
            }else{
                $message = is_login_template();
            }
            echo $message;die;
            break;

        // LOAD FORM QUÊN MẬT KHẨU
        case 'forgotpassword':
            $message = '';
            $message.='
            <div class="modal-content" >
            <div class="closepopup" data-dismiss="modal"></div>
            <div class="modal-body" id="datalogin">
            <div class="contentlogin">
            <h2>'.$arraybien['laylaimatkhau'].'</h2>

            <input  data-require="'.$arraybien['vuilongnhapemail'].'"  type="text" class="form-control" name="email" placeholder="'.$arraybien['nhapemaildelaylaimatkhau'].'" />

            <br />

            <div class="btnlogin" id="forgotpasswordid">'.$arraybien['guilaimatkhau'].'</div>

            <br />

            <div class="dangkyvoi"><strong>'.$arraybien['hoac'].'</strong></div>

            <div class="labeldangky">'.$arraybien['neubanchuacotaikhoan'].' <a href="#" onclick="Get_Data(\''.$_SESSION['ROOT_PATH'].'ajax/?method=register\',\'loaddatacart\');"  >'.$arraybien['dangkyngay'].'</a></div>

            <div class="labeldangky">'.$arraybien['neubandacotaikhoan'].' <a href="#" onclick="Get_Data(\''.$_SESSION['ROOT_PATH'].'ajax/?method=login\',\'loaddatacart\');"  >'.$arraybien['dangnhap'].'</a></div>
            </div>
            </div><!-- ./modal -->
            </div>';
            echo $message;die;
            break;

        // UPLOAD HÌNH ĐẠI ĐIỆN
        case 'uploadhinhdaidien':
            if( ws_post('action') == 'upload' ){
                $upload_url = null;
                $upload_path = dirname(__DIR__).'/uploads/user/';
                create_dir($upload_path, false);

                // Check file type
                $validextensions = array("image/jpeg", "image/jpg", "image/png", "image/gif");
                if(!in_array($_FILES['filedaidien']['type'],$validextensions)){
                    echo '_invalid_type';
                    die();
                }

                // Check file size
                if($_FILES['filedaidien']['size'] > 2000000){
                    echo '_invalid_size';
                    die();
                }

                // Xoa hinh cu
                if( ws_post('tenhinhdaidien') ){
                    $tenhinhdaidien = ws_post('tenhinhdaidien');
                    if( file_exists( $upload_path . $tenhinhdaidien ) ){
                        unlink( $upload_path . $tenhinhdaidien );
                    }
                }

                $name = $upload_url.$_FILES['filedaidien']['name'];
                $name = vn_str_filter($name, '-');

                if( file_exists( $upload_path . $name ) ){
                    $ext  = pathinfo($name, PATHINFO_EXTENSION);
                    $name = explode('.', $name);
                    $ten  = reset($name);
                    $name = $ten . '-' . rand(111,999) .'.' . $ext;
                }

                $dest = $upload_path.$name;
                if(is_uploaded_file($_FILES['filedaidien']['tmp_name'])){
                    @move_uploaded_file($_FILES['filedaidien']['tmp_name'], $dest);
                }

                // Update DB
                $db->sqlUpdate(
                    'tbl_user',
                    array( 'hinhdaidien' => $name ),
                    " id = " . get_id_user()
                );

                echo $_SESSION['ROOT_PATH']."uploads/user/".$name; die;
            }else{
                echo 'Có lỗi xãy ra';
            }
            break;

        // THAY ĐỔI THÔNG TIN THÀNH VIÊN
        case 'uEdit':
            $result = $message = $status = null;
            $ary = ws_post('ary');

            if( is_array($ary) ){
                foreach ($ary as $kary => $vary) {
                    $strItem = explode(":", $vary);
                    $keyI    = current($strItem);
                    $valI    = end($strItem);

                    if($keyI == "action" || $keyI == "magioithieu" || $valI == "Upload" || $keyI == "method"){
                        continue;
                    }

                    $sI = array(
                        $keyI => $valI
                    );

                    $db->sqlUpdate(
                        "tbl_user", $sI,
                        "id = {$_SESSION['user_id']} "
                    );
                }

                $status = 'success';
            }else{
                $status = 'error';
            }

            echo json_encode(array(
                "message" => "Cập nhật thành công !",
                "status" => $ary
            ));
            break;

        // XỬ LÝ ĐỔI MẬT KHẨU
        case 'changepassquery':
            $result = $message = $status = null;

            $maactive = ws_post('maactive');
            $email    = ws_post('email');
            $password = $maactive ? ws_post('password') : ws_post('repassword');
            $status   = 'error';

            $s = null;
            if( $maactive ){
                $s = " AND maactive = '{$maactive}' ";
            }

            $check_email = $db->sqlSelectSql("
                SELECT id,lat FROM tbl_user
                WHERE email = '{$email}' {$s} ");

            if( count($check_email) > 0 ){

                $lat = $check_email[0]['lat'];
                $password = create_pass($password, $lat);

                $s2 = null;
                if( $maactive ){
                    $s2 = " AND maactive = '{$maactive}' ";
                }

                $db->rawQuery("
                    UPDATE tbl_user
                    SET matkhau = '{$password}', maactive = ''
                    WHERE email = '{$email}' {$s2}
                    ");

                $message = 'Đổi mật khẩu thành công';
                $status = 'success';

            }else{
                $message = 'Đổi mật khẩu thất bại';
                $status = 'error';
            }

            echo json_encode(array(
                'message' => $message,
                'status'  => $status
            ));
            break;

        // XỬ LÝ ĐĂNG KÝ
        case 'registerquery':

            $message          = null;
            $hovaten          = ws_get('hovaten');
            $email            = ws_get('email');
            $gioitinh         = ws_get('gioitinh');
            $loai             = ws_get('loai');
            $password         = ws_get('password');
            $magioithieu      = random_str(6, true);
            $manguoigioithieu = ws_get('manguoigioithieu');

            $check_email = $db->sqlSelectSql("
                SELECT id FROM tbl_user WHERE email = '{$email}' "
            );

            if (count($check_email) > 0) {
                $status = 'emailexits';
                $message = $arraybien['emaildatontai'];

            } else {

                $reg_lat        = rand(111,999);
                $reg_lat        = md5($reg_lat);
                $reg_lat        = substr($reg_lat, 8, 3);
                $reg_matkhauoke = create_pass($password, $reg_lat);
                $tendangnhap    = rand(0, 99999999999999);
                $active         = 1;

                $kq_dangky = $db->sqlInsert("tbl_user", array(
                    'tendangnhap'      => $tendangnhap,
                    'matkhau'          => $reg_matkhauoke,
                    'active'           => $active,
                    'lat'              => $reg_lat,
                    'nhom'             => 0,
                    'supperadmin'      => 0,
                    'ten'              => $hovaten,
                    'gioitinh'         => $gioitinh,
                    'email'            => $email,
                    'ngaycapnhat'      => $db->now(),
                    'ngaydangky'       => $db->now(),
                    'magioithieu'      => $magioithieu,
                    'manguoigioithieu' => $manguoigioithieu,
                ));
                // echo $db->getLastQuery();
                // echo $db->getLastError();

                if ($kq_dangky > 0) {
                    $status  = 'success';
                    $message = $arraybien['dangkythanhcong'];
                    $checkLogin = $db->checkLoginUser($email, md5($password));
                    if (count($checkLogin) == 1) {
                        session_regenerate_id();
                        $_SESSION['user_id'] = $checkLogin[0]['id'];

                        $_SESSION['tendangnhap']      = $checkLogin[0]['tendangnhap'];
                        $_SESSION['user_password']    = $password;
                        $_SESSION['user_id']          = $checkLogin[0]['id'];
                        $_SESSION['user_hotendem']    = $checkLogin[0]['hotendem'];
                        $_SESSION['user_supperadmin'] = $checkLogin[0]['supperadmin'];
                        $_SESSION['user_ten']         = $checkLogin[0]['ten'];
                        $_SESSION['user_gioitinh']    = $checkLogin[0]['gioitinh'];
                        $_SESSION['user_email']       = $checkLogin[0]['email'];
                        $_SESSION['user_diachi']      = $checkLogin[0]['diachi'];
                        $_SESSION['user_tinhthanh']   = $checkLogin[0]['tinhthanh'];
                        $_SESSION['user_sodienthoai'] = $checkLogin[0]['sodienthoai'];
                        $email  = $checkLogin[0]['email'];
                    }
                }
            }
            echo json_encode(array(
                'message' => $message,
                'status'  => $status
            ));
            break;

        // XỬ LÝ ĐĂNG NHẬP
        case 'loginquery':
            $result = $status = $message = null;

            if( is_login() ){
                $status = 'error';
                $message.= "Bạn đang đăng nhập.";

            }else{
                $email    = ws_get('email');
                $password = ws_get('password');
                $remember = ws_get('remember');
                $status   = 'error';
                $message  = $arraybien['saiemailhoacmatkhau'];

                if ( $email && $password ) {
                    $password = md5($password);
                    // kiem tra login
                    $checkLogin = $db->checkLoginUser($email, $password);

                    if (count($checkLogin) >= 1) {

                        $status = 'success';
                        $message = $arraybien['dangnhapthanhcong'];
                        session_regenerate_id();
                        $_SESSION['tendangnhap']      = $checkLogin[0]['tendangnhap'];
                        $_SESSION['user_password']    = $password;
                        $_SESSION['user_id']          = $checkLogin[0]['id'];
                        $_SESSION['user_hotendem']    = $checkLogin[0]['hotendem'];
                        $_SESSION['user_supperadmin'] = $checkLogin[0]['supperadmin'];
                        $_SESSION['user_ten']         = $checkLogin[0]['ten'];
                        $_SESSION['user_gioitinh']    = $checkLogin[0]['gioitinh'];
                        $_SESSION['user_email']       = $checkLogin[0]['email'];
                        $_SESSION['user_diachi']      = $checkLogin[0]['diachi'];
                        $_SESSION['user_tinhthanh']   = $checkLogin[0]['tinhthanh'];
                        $_SESSION['user_sodienthoai'] = $checkLogin[0]['sodienthoai'];
                        $email  = $checkLogin[0]['email'];

                        // Lưu lịch sử
                        if( !$checkLogin[0]['supperadmin'] ) {
                            $db->insert('tbl_login_history', [
                                'idtype' => $checkLogin[0]['id'],
                                'ip'     => get_client_ip(),
                                'ngay'   => $db->now(),
                                'thutu'  => $db->thuTu('tbl_login_history')
                            ]);
                        }

                        // dăng ký cookie nếu nhớ thông tin đăng nhập
                        if( $remember ){
                            setcookie('username', $email , time() + (86400 * 30 * 12),'/',$_SERVER['HTTP_HOST']);
                            setcookie('password', $password, time() + (86400 * 30 * 12),'/',$_SERVER['HTTP_HOST']);
                        }

                    }
                }

                if ( !is_array($topVal) ) {
                    $topVal = array();
                }

                $topVal['arrUser'] = array(
                    "tendangnhap" => $_SESSION['tendangnhap'],
                    "hotendem"    => $_SESSION['user_hotendem'],
                    "ten"         => $_SESSION['user_ten'],
                );

                $smarty->assign("arrTop", $topVal);
                $smarty->assignByRef("didong", $didong);
                $smarty->assign("root_path", $_SESSION['ROOT_PATH']);
                $result = $smarty->fetch(
                    __DIR__.'/../templates/layout1/modules/user/tpl/login_success.tpl'
                );
            }

            echo json_encode(array(
                'message' => $message,
                'result'  => $result,
                'status'  => $status
            ));
            break;

        // LOAD THÔNG TIN THÀNH VIÊN
        case 'changeUser':

            add_col_sql("tbl_user", "hinhdaidien", "VARCHAR(200)", "loai");
            $_SESSION['user_hotendem']    = $db->getUser("hotendem");
            $_SESSION['user_ten']         = $db->getUser("ten");
            $_SESSION['user_diachi']      = $db->getUser("diachi");
            $_SESSION['user_sodienthoai'] = $db->getUser("sodienthoai");
            $_SESSION['user_hinhdaidien'] = $db->getUser("hinhdaidien");
            $_SESSION['user_magioithieu'] = $db->getUser("magioithieu");
            $_SESSION['user_sodu']        = $db->layTongDiem(get_id_user()) ? $db->layTongDiem(get_id_user()) : 0;
            $smarty->assign('root_path', $_SESSION['ROOT_PATH']);
            echo $smarty->fetch(ROOT_DIR.'/templates/layout1/modules/thanhvien/thanhvien_thongtin.tpl');
            break;

        // XỬ LÝ ĐỔI MẬT KHẨU
        case 'forgotpasswordquery':
            // show_error();
            $email   = ws_get('email');
            $status  = 'error';
            $message = $arraybien['emailkhongtontai'];
            if ( $email ) {
                $check_email = $db->sqlSelectSql(
                    "SELECT id FROM tbl_user WHERE email = '{$email}' ");

                if ( count($check_email) > 0 ) {
                    $maactive = rand(0,999);
                    $maactive = md5($maactive);

                    $linkkichhoat = $_SESSION['ROOT_PATH'].'?op=user&act=forgotpasswordquery&email='.$email.'&maactive='.$maactive;
                    $noidung = $db->getThongTin("quenmatkhau");
                    $noidung = str_replace("[LINKKICHHOAT]", $linkkichhoat, $noidung);
                    $noidung = str_replace("[WEBSITE]", $_SERVER['HTTP_HOST'], $noidung);

                    $db->rawQuery("
                        Update tbl_user set maactive = '{$maactive}'
                        where email = '{$email}' and email != ''
                        ");

                    // hot mail
                    $subject = "Quên mật khẩu";
                    $array_sendmail = array(
                        "emailnhan" => $email,
                        "emailgui"  => $_getArrayconfig['hostmail_user'],
                        "hostmail"  => $_getArrayconfig['hostmail'],
                        "user"      => $_getArrayconfig['hostmail_user'],
                        "pass"      => $_getArrayconfig['hostmail_pass'],
                        "subject"    => $subject,
                        "tieude"    => $subject,
                        "fullname"  => $_getArrayconfig['hostmail_fullname'],
                        "port"      => $_getArrayconfig['hostmail_port'],
                        "ssl"       => $_getArrayconfig['hostmail_ssl'],
                        "subject"   => $arraybien['yeucaulaylaimatkhau'],
                        "message"   => $noidung,
                    );
                    sendmail($array_sendmail);

                    $status = 'success';
                    $message = $arraybien['vuilongcheckmaildelaylaimatkhau'];
                }
            }

            echo json_encode(array(
                'message' => $message,
                'status'  => $status
            ));
            break;

        //LOAD ĐỔI MẬT KHẨU
        case 'changePW':
            $smarty->assign('root_path', $_SESSION['ROOT_PATH']);
            echo $smarty->fetch(
                __DIR__."/../templates/layout1/modules/thanhvien/thanhvien_matkhau.tpl");
            break;

        //
        case 'dangkymail':
            if ( !empty($value) ) {
                // Kiem tra email da ton tai chua
                $value  = trim( strip_tags( $value ) );
                $value2 = trim( strip_tags( $value2 ) );
                $value3 = trim( strip_tags( $value3 ) );
                $value4 = trim( strip_tags( $value4 ) );

                $sExi = "SELECT id
                FROM tbl_dangkymail
                WHERE email     = '{$value}'
                AND hoten       = '{$value2}'
                AND sodienthoai = '{$value3}'
                AND noidung     = '{$value4}'";
                $qExi = $db->sqlSelectSql($sExi);
                // echo '<pre>'; print_r($qExi); echo '</pre>'; exit();

                if ( count($qExi) > 0 ) {
                    $status = 'exists';
                    $message = $arraybien['emailnaydaduocdangky'];
                } else {
                    $sDkmail = array(
                        'email'       => $value,
                        'hoten'       => $value2,
                        'sodienthoai' => $value3,
                        'noidung'     => $value4
                        // 'ngaygui'     => $db->day()
                    );
                    $idDk = $db->sqlInsert("tbl_dangkymail", $sDkmail);
                    if ( $idDk ) {
                        $message = $arraybien['dangkythanhcong'];
                    } else {
                        $message = $arraybien['coloixayravuilonglienhebqt'];
                    }
                }
            } else {
                $message = $arraybien['coloixayravuilonglienhebqt'];
            }
            $arr = array(
                'message' => $message,
                'status'  => $status);

            echo json_encode($arr);
            break;

        case 'question':
            if ( !empty($value) ) {
                // Kiem tra email da ton tai chua
                $idtype      = trim( strip_tags( $_GET['idtype'] ) );
                $idparent    = trim( strip_tags( $_GET['idparent'] ) );
                $hoten       = trim( strip_tags( $_GET['hoten'] ) );
                $email       = trim( strip_tags( $_GET['email'] ) );
                $dienthoai   = trim( strip_tags( $_GET['dienthoai'] ) );
                $noidung     = trim( strip_tags( $_GET['noidung'] ) );
                $captcha     = trim( strip_tags( $_GET['captcha'] ) );
                $ngay        = $db->getDateTimes();
                $anhien      = 0;

                if($captcha == $_SESSION['captcha_code']){
                    // update thutu
                    $sql_update  = "update tbl_comment set thutu = thutu+1 where idparent = '' ";
                    $db->rawQuery($sql_update);

                    $arr_noidung = array(
                        'loai'        =>  10,
                        'ngay'        => $db->getDateTimes(),
                        'ngaycapnhat' => $db->getDateTimes(),
                        'anhien'      => 0,
                        'idtype'      => $idtype,
                        'thutu'       => 1
                    );
                    $sql_update  = "update tbl_noidung set thutu = thutu+1 where loai = 10 ";
                    $db->rawQuery($sql_update);
                    $idnoidung = $db->sqlInsert("tbl_noidung", $arr_noidung);
                    $noidunginsert = 'Họ Tên: '.$hoten.'<br />Email: '.$email.'<br />Số điện thoại: '.$dienthoai;
                    $arr_noidung_lang = array(
                        'idnoidung'  => $idnoidung,
                        'idlang'     => $_SESSION['_lang'],
                        'ten'        => $noidung,
                        'tieude'     => $noidung,
                        'url'        => $config->ThayDoiURL($noidung),
                        'mota'       => $noidunginsert,
                        'tukhoa'     => $noidung.', '.tao_url($noidung),
                        'motatukhoa' => $noidung
                    );
                    $idnoidunglang = $db->sqlInsert("tbl_noidung_lang", $arr_noidung_lang);
                    if ( $idnoidunglang ){
                        $message = $arraybien['guicauhoithanhcong'];
                    } else {
                        $message = $arraybien['coloixayravuilonglienhebqt'];
                        $status = 'error';
                    }
                }else{
                    $message = $arraybien['saimacaptcha'];
                    $status = 'saicaptcha';
                }
            } else {
                $message = $arraybien['coloixayravuilonglienhebqt'];
            }
            //$message= $_SESSION['captcha_code'];
            $arr = array(
                'message' => $message,
                'status'  => $status);

            echo json_encode($arr);
            break;

        case 'comment':
            if ( !empty($value) ) {
                // Kiem tra email da ton tai chua
                $idtype      = trim( strip_tags( $_GET['idtype'] ) );
                $idparent    = trim( strip_tags( $_GET['idparent'] ) );
                $hoten       = trim( strip_tags( $_GET['hoten'] ) );
                $email       = trim( strip_tags( $_GET['email'] ) );
                $dienthoai   = trim( strip_tags( $_GET['dienthoai'] ) );
                $noidung     = trim( strip_tags( $_GET['noidung'] ) );
                $captcha     = trim( strip_tags( $_GET['captcha'] ) );
                $ngay        = $db->getDateTimes();
                $anhien      = 0;

                $sComment = array(
                    'hoten'     => $hoten,
                    'email'     => $email,
                    'dienthoai' => $dienthoai,
                    'noidung'   => $noidung,
                    'idtype'    => $idtype,
                    'idparent'  => $idparent,
                    'thutu'     => 1,
                    'daxem'     => 0,
                    'anhien'    => $anhien,
                    'ngay'      => $ngay
                );
                if($captcha == $_SESSION['captcha_code']){
                    // update thutu
                    $sql_update  = "update tbl_comment set thutu = thutu+1 where idparent = '' ";
                    $db->rawQuery($sql_update);
                    $idDk = $db->sqlInsert("tbl_comment", $sComment);
                    if ( $idDk ){
                        $message = $arraybien['guibinhluanthanhcong'];
                    } else {
                        $message = $arraybien['coloixayravuilonglienhebqt'];
                        $status = 'error';
                    }
                }else{
                    $message = $arraybien['saimacaptcha'];
                    $status = 'saicaptcha';
                }
            } else {
                $message = $arraybien['coloixayravuilonglienhebqt'];
            }
            //$message= $_SESSION['captcha_code'];
            $arr = array(
                'message' => $message,
                'status'  => $status);

            echo json_encode($arr);
            break;
        default:
            // code...
            break;
        }
    }
