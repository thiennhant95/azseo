<?php
$vmodal = '';
$vmodal.='

    <div id="error-send-tuvan"></div>
    <div class="form-group clearfix">

        <label for="linhvuctuvan">'.$arraybien['linhvuctuvan'].'</label>
        <select name="linhvuctuvan" class="form-control">'.$value.'</select>
    </div>

    <div class="form-group clearfix">

        <label for="hoten">'.$arraybien['hoten'].'</label>
        <input type="text" name="hoten" class="form-control ws-required" placeholder="'.$arraybien['vuilongnhaphoten'].'" />

    </div>

    <div class="form-group clearfix">
        <label for="dienthoai">'.$arraybien['dienthoai'].'</label>
        <input type="text" name="dienthoai" class="form-control ws-required ws-required-number" data-wserror-number="Vui lòng nhập 1 số điện thoại" placeholder="'.$arraybien['vuilongnhapsodienthoai'].'" />
    </div>

    <div class="form-group clearfix">

        <label for="email">'.$arraybien['email'].'</label>
        <input type="text" name="email" class="form-control ws-required ws-required-email" data-wserror-email="Vui lòng nhập 1 email" placeholder="'.$arraybien['vuilongnhapemail'].'" />

    </div>

    <div class="form-group clearfix">

        <label for="noidung">'.$arraybien['noidung'].'</label>
        <textarea name="noidung" class="form-control ws-required" placeholder="'.$arraybien['vuilongnhapnoidung'].'" rows="6"></textarea>

    </div>
';

return $vmodal;
