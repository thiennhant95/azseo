<?php
    require dirname(dirname(__DIR__)) . "/configs/inc.php";

    include dirname(dirname(__DIR__))."/plugins/mail/gmail/class.phpmailer.php";
    include dirname(dirname(__DIR__))."/plugins/mail/gmail/class.smtp.php";

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $id     = ws_get('id');
        $method = ws_get('method');
        $name   = ws_get('name');
        $value  = ws_get('value');
        $op     = ws_get('op');
        $act    = ws_get('act');
    } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $id     = ws_post('id');
        $method = ws_post('method');
        $name   = ws_post('name');
        $value  = ws_post('value');
        $op     = ws_post('op');
        $act    = ws_post('act');
    }
    $urlreturn  = '';
    $data       = '';
    $dataupdate = '';
    $resutl     = '';
    $message    = '';
    $status     = 'success';
