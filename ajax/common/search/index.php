<?php
session_start();
if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
    include dirname(__DIR__)."/../../404.php";
    exit;
}

require  "../head.php";

        $value = ws_post('value');
        $d_noidung = $db->layNoiDung([
            "op" => "product",
            "where" => "AND (ten LIKE '%{$value}%' OR a.id LIKE '%{$value}%' OR noidung LIKE '%{$value}%')
                        AND anhien = 1
                        AND idlang = {$_SESSION['_lang']}",
            "limit" => 50,
        ]);
        if ( count( $d_noidung ) > 0 ) {
            $rs.= '
                <ul>';

                    foreach( $d_noidung as $ks => $is ){

                        $href = $_SESSION['ROOT_PATH'] . $is['url'];
                        $src = $_SESSION['ROOT_PATH'] ."uploads/noidung/thumb/". $is['hinh'];

                        $file = dirname(__DIR__)."/uploads/noidung/thumb/".$is['hinh'];

                        $rs.='
                        <li class="clearfix item-ls">
                            <div class="image">
                                <a href="'.$href.'" title="">
                                    <img src="'.$src.'" alt="'.$is['ten'].'" class="img-thumbnail" width="50" />
                                </a>
                            </div>

                            <div class="name">
                                <a href="'.$href.'" title="'.$is['ten'].'">
                                    '.$is['ten'].'
                                </a>
                                <br />
                                '.(
                                    $is['gia'] ? '<span class="gia">
                                        Giá: '.number_format($is['gia']).' đ
                                    </span><!-- /.gia -->' : null
                                ).'

                            </div>

                        </li>';
                    }

                $rs.='
                </ul>';

            $status  = 'success';
            $message = '';
            $result  = $rs;
        } else {
            $status = 'empty';
            $message = '
            <div class="alert alert-danger">
                Không tìm thấy kết quả nào.
            </div><!-- /.alert alert-danger -->';
        }

require "../foot.inc.php";
