<?php
require  "../head.php";

if( ws_post('method') == 'doidiem' ) {

    // Lấy tỉ lệ đổi điểm thành tiền
    $tile_doidiem = lay_cauhinh_tichdiem('sotien');

    // Lấy tổng số điểm đang có
    $sodiem = $db->layTongDiem( get_id_user() );

    // Quy đổi thành tiền
    $doithanhtien = ($sodiem * $tile_doidiem);

    if( lay_cauhinh_tichdiem() ) {

        //$tongtien_donhang = tongtien_donhang( $_SESSION['giohang'] );

        if( ws_post('value') ) {
            $_SESSION['doidiem'] = $doithanhtien;
        }
        else {
            unset($_SESSION['doidiem']);
        }
    }
    else {
        $message = "Thao tác thất bại, Vui lòng liên hệ BQT";
    }

}

require "../foot.inc.php";