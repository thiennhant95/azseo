<?php
if ( session_id() == '' ) session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
$chenjs = '';
$pathcss     = '';
$pathplugin = '';
#-----------------------------------------------------
# DETECT FILE IS ( INCLUDE OR DIRECT )
#-----------------------------------------------------
    if(__FILE__ == realpath($_SERVER['SCRIPT_FILENAME'])) {
        # THIS IS DIRECT TO FILE
        $pathcss = LAYOUT_PATH."css/";
        $pathplugin = ROOT_PATH;
        echo "<h2>Deleted: <strong>". date('H:i:s', time())."</strong></h2>";
    } else {
        # THIS IS INCLUDE TO FILE
        $pathcss = LAYOUT_PATH."css/";
        $pathplugin = "";
    }

    $chencss = '';
    if (file_exists($pathcss."css.cache.css")) {
        unlink($pathcss."css.cache.css");
    }

    // $chencss .= '<style>';
    $chencss .= file_get_contents($pathplugin."plugins/font-awesome/css/font-awesome.min.css");
    $chencss .= file_get_contents($pathplugin."plugins/animate/animate.min.css");
    $chencss .= file_get_contents($pathplugin."plugins/bootstrap/css/bootstrap.min.css");
    $chencss .= file_get_contents($pathplugin."plugins/OwlCarousel2/owl.carousel.min.css");
    $chencss .= file_get_contents($pathplugin."plugins/OwlCarousel2/owl.theme.default.min.css");
    $chencss .= file_get_contents($pathplugin."plugins/hover-effect/hover-min.css");
    $chencss .= file_get_contents($pathplugin."plugins/sweet-alert/sweetalert.css");
    $chencss .= file_get_contents($pathplugin."plugins/jquery-news-ticker/styles/ticker-style.css");
    $chencss .= file_get_contents($pathplugin."plugins/elevatezoom/jquery.fancybox.css");
    $chencss .= file_get_contents($pathplugin."plugins/cubeslider/css/classic/cubeslider.css");
    $chencss .= file_get_contents($pathplugin."plugins/ws-form-error/ws-form-error.css");
    $chencss .= file_get_contents($pathplugin."plugins/prettyphoto/css/prettyPhoto.css");
    $chencss .= file_get_contents($pathplugin."plugins/rating/starrr.css");
    $chencss .= file_get_contents($pathplugin."plugins/select2/dist/css/select2.min.css");
    $chencss.= file_get_contents($pathplugin."plugins/data-table/jquery.dataTables.min.css");
    // $chencss .= '</style>';
    $chencss  = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $chencss);;
    $chencss  = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $chencss);
    $chencss  = str_replace('{$layout_path}', $_SESSION['ROOT_PATH'].$_SESSION['LAYOUT_PATH'], $chencss);
    $chencss  = str_replace('{$root_path}', $_SESSION['ROOT_PATH'], $chencss);

    if (!file_exists($pathcss."css.cache.css")) {
        $fw = fopen($pathcss."css.cache.css", "w");
        if ($fw) {
            fwrite($fw, $chencss);
            fclose($fw);
        }
    }
