{strip}
{$braucube}
<h1 class="product-title-big">{$ten}</h1>
{if $smarty.session.flow == 2}
    {$classView = 'ws-list'}
{else}
    {$classView = 'ws-grid'}
{/if}
{if !empty($noidung_menu)}
    <div class="noidung_menu">{$noidung_menu}</div>
{/if}
<div class="clearfix product-container {$classView}" id="loaditemsanpham">
{include file="../../{$op}/tpl/item.tpl" arrsanpham=$arrdata.video class='' owl=''}
</div>
<div class="clear"></div>
<h2 class="hide">{$ten}</h2>
{$phantrang}
{/strip}