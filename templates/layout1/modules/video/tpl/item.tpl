{strip}
{if $owl!=''}
    <div class="{$owl} owl-carousel owl-theme">
{/if}
{if $rowsp > 1}
    <div>
{/if}
{if count($arrsanpham) }
    {foreach key=cid item=arrVideo from=$arrsanpham}
        {* XU LY LIEN KET *}
        {if !empty($arrVideo.link)}
            {$explodearr = explode("=", $arrVideo.link)}
        {else}
            {$lienket = $root_path|cat:$arrVideo.url}
        {/if}
        {* XU LY GIA *}
        {if !empty($arrVideo.gia)}
            {$dvt = 'đ'}
            {if is_numeric($arrVideo.gia)}
                {$gia = number_format($arrVideo.gia, 0, ',', '.')|cat:$dvt}
            {else}
                {$gia = $arrVideo.gia}
            {/if}
        {else}
            {$gia = $smarty.session.arraybien.lienhe}
        {/if}
        {* XU LY TARGET *}
        {if !empty($arrVideo.target)}
            {$target = "target={$arrVideo.target}"}
        {else}
            {$target = null}
        {/if}
        {* PERCENT *}
        {if (!empty($arrVideo.giagoc) && !empty($arrVideo.gia)) && ($arrVideo.giagoc > $arrVideo.gia)}
            {$percent = ($arrVideo.gia - $arrVideo.giagoc) / $arrVideo.giagoc * 100}
            {$percent = {$percent|string_format:"%d"}  }
        {/if}
        {* RATING *}
        {if empty($arrVideo.rating)}
            {$rating = 10}
        {else}
            {$rating = $arrVideo.rating}
        {/if}
        {if empty($arrVideo.ratingCount)}
            {$ratingCount = 1}
        {else}
            {$ratingCount = $arrVideo.ratingCount}
        {/if}
        {$ratingvalue = number_format(($ratingCount/$rating),1)}
        {if empty($arrVideo.gia)}
            {$price = 1}
        {else}
            {$price = $arrVideo.gia}
        {/if}

        {* VIDEO *}

        <div itemscope itemtype="\/\/schema.org/Product" class="wow animated fadeInDown itemvideo  clearfix {$class} " data-wow-delay="0.{$cid}s">
                <div id="framevideo" class="clearfix">
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{$explodearr[1]}?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <span itemprop="name" class="name">
                    <a href="{$lienket}" title="{$arrVideo.ten}">{$arrVideo.ten}</a>
                </span>
        </div>

    {/foreach}
    </div><!-- /.video-group -->
{/if}
{if $rowsp > 1}
    </div>
{/if}
{if $owl!=''}</div>{/if}
{/strip}