{if $owl!=''}<div class="{$owl} owl-carousel owl-theme">{/if}
{foreach key=cid item=arr_gioithieu from=$arrgioithieu}

{* XU LY LIEN KET *}
{if !empty($arr_gioithieu.link)}
    {$lienket = $arr_gioithieu.link}
{else}
    {$lienket = $root_path|cat:$arr_gioithieu.url|cat:".html"}
{/if}

{* XU LY TARGET *}
{if !empty($arr_gioithieu.target)}
    {$target = "target={$arr_gioithieu.target}"}
{else}
    {$target = null}
{/if}
        <div class="itemnoidung {$class}">
            <div class="itemnoidung_content">
                <div class="img">
                    <a title="{$arr_gioithieu.ten}"
                    href="{$lienket}" {$target}>
                        <img src="{$root_path}uploads/noidung/thumb/{$arr_gioithieu.hinh}" alt="{$arr_gioithieu.ten}" class="img-responsive" />
                    </a>
                    <div class="iconsp icon_view">
                      <a href="{$lienket}">
                        <i class="fa fa-info-circle"></i>
                      </a>
                    </div>
                </div>
                <div class="ten">
                    <a title="{$arr_gioithieu.ten}"
                    href="{$lienket}" {$target}>{$arr_gioithieu.ten}</a>
                </div>
            </div>
        </div>
{/foreach}
{if $owl!=''}</div>{/if}