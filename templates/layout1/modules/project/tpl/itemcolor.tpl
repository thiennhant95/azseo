{strip}
{if $owl!=''}
    <div class="{$owl} owl-carousel owl-theme">
{/if}

{if $rowsp > 1}
    <div>
{/if}
{foreach key=colorid item=arrColorData from=$arrColor}
    {$activecolor = ''}
    {if $colorid==0}
        {$activecolor = ' activecolor'}
    {/if}
    <div class="coloritem {$activecolor}" style="background-color:#{$arrColorData.mau}" data-hinh="{$root_path}uploads/noidung/color/thumb/{$arrColorData.hinh}">
    </div>
    <!-- ./itemsanpham -->

    {if ($colorid+1) % $rowsp == 0 && $rowsp > 1}
        </div><div>
    {/if}
{/foreach}

{if $rowsp > 1}
    </div>
{/if}
{if $owl!=''}</div>{/if}
{/strip}