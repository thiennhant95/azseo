{strip}
{if $owl!=''}<div class="{$owl} owl-carousel owl-theme">{/if}
{if count($arrnoidung)}
    {foreach key=cid item=arr_noidung from=$arrnoidung}

    {* XU LY LIEN KET *}
    {if !empty($arr_noidung.link)}
        {$lienket = $arr_noidung.link}
    {else}
        {$lienket = $root_path|cat:$arr_noidung.url}
    {/if}

    {* XU LY TARGET *}
    {if !empty($arr_noidung.target)}
        {$target = "target={$arr_noidung.target}"}
    {else}
        {$target = null}
    {/if}

        <div itemscope itemtype="\/\/schema.org/NewsArticle" class="itemduan clearfix {$class}">
          <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="{$lienket}"/>
          <h3 itemprop="author" itemscope itemtype="https://schema.org/Person">
             <span itemprop="name" class="hidden">{$arr_noidung.ten}</span>
          </h3>
          <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject" class="image">
            <img src="{$root_path}uploads/noidung/thumb/{$arr_noidung.hinh}" alt="{$arr_noidung.ten}" />
            <meta itemprop="url" content="{$lienket}">
            <meta itemprop="width" content="350">
            <meta itemprop="height" content="auto">
          </div>
          <h2 itemprop="headline" class="name">
              <a title="{$arr_noidung.ten}" href="{$lienket}" {$target}>{$arr_noidung.ten}</a>
          </h2>
          <span itemprop="description" class="description">{$arr_noidung.mota}</span>
          <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject" class="hidden">
              <img src="{$root_path}uploads/noidung/thumb/{$arr_noidung.hinh}"/>
              <meta itemprop="url" content="{$lienket}">
              <meta itemprop="width" content="600">
              <meta itemprop="height" content="60">
            </div>
            <meta itemprop="name" content="Google">
          </div>
          <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
          <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>
        </div>

    {/foreach}
{/if}
{if $owl!=''}</div>{/if}
{/strip}