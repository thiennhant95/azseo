<h1 class="product-title-big">{$smarty.session.arraybien.ketquatimkiemtukhoa} "{$smarty.get.key}"</h1>
{if $smarty.session.flow == 2}
    {$classView = 'ws-list'}
{else}
    {$classView = 'ws-grid'}
{/if}
{if !empty($noidung_menu)}
<div class="noidung_menu">{$noidung_menu}</div>
{/if}
<div class="clearfix product-container {$classView}">
{include file="../../product/tpl/item.tpl" arrsanpham=$arrdata.sanpham class='' owl=''}
</div>
<div class="clear"></div>
{$phantrang}