{strip}
{$rowsp = 2}
<div class="ws-container">
    <div class="danhmucSp clearfix">
            <div class="dmRun owl-carousel">
            {if $rowsp > 1}
                <div>
            {/if}
            {foreach from=$danhmuc item=itemdm key=keydm}
            {* URL *}
            {$catpLink = "javascript:;"}
            {if !empty($itemdm.url)}
            {$catpLink = $root_path|cat:$itemdm.url|cat:'/'}
            {/if}
            {* LINK *}
            {if !empty($itemdm.link)}
            {$catpLink = $itemdm.link}
            {/if}
            {if !empty($itemdm.img)}
            {$catbg = $root_path|cat:'uploads/danhmuc/'|cat:$itemdm.img}
            {else}
            {$catbg = ''}
            {/if}
            {* NỘI DUNG *}
            {$noidungdanhmuc = ''}
            {if !empty($itemdm.noidung)}
            {$noidungdanhmuc = $itemdm.noidung}
            {else}
            {$noidungdanhmuc = ''}
            {/if}
            {* ICON *}
            {$icon = ''}
            {if !empty($itemdm.icon)}
            {$iconfile = $itemdm.icon}
            {if substr($iconfile,0,2)=='fa'}
            {$icon = '<span class="img"><i class="fa '|cat:$itemdm.icon|cat:'"></i></span>'}
            {else}
            {$icon = '<span class="img"><img src="'|cat:$root_path|cat:'uploads/danhmuc/'|cat:$itemdm.icon|cat:'" alt="'|cat:$itemdm.ten|cat:'" /></span>'}
            {/if}
            {/if}
            {$ididi = $itemdm.idtype}
            {if $catpLink ==  $smarty.server.SCRIPT_URI}
                {$cat_active = 'active'}
            {else}
                {$cat_active = ''}
            {/if}
            <div class="categories clearfix {$cat_active}">
                <div class="">
                    <a  data-id="{$itemdm.id}" href="{$catpLink}" title="{$itemdm.ten}">
                        <img src="{$root_path}uploads/danhmuc/{$itemdm.img}" alt="{$itemdm.ten}" class="img-responsive">
                        <span class="tentieude">{$itemdm.ten}</span>
                    </a>
                </div>
                {* <div class="{db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi}Box clearfix">
                    {if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'product'}
                    {include file="../../product/tpl/item.tpl" arrsanpham=$dataHome[$keydm].thread owl=''}

                    {/if}
                </div> *}
            </div>

            {if ($keydm+1) % $rowsp == 0 && $rowsp > 1 && count($danhmuc) > ($keydm+1)}
                </div><div>
            {/if}

            {/foreach}
            {if $rowsp > 1}
                </div>
            {/if}
            </div>
    </div>
</div><!-- /.ws-container -->
{/strip}