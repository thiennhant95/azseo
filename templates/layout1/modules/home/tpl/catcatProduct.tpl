{strip}
    {if count($dataHome.catp) > 0}
        <div class="catp-container clearfix">
            {foreach from=$dataHome.catp key=kycatp item=icatp}
                <div class="catp-content-item clearfix">
                    <div class="catp-cat clearfix">

                        <div class="cat-title">
                            <span class="ws-icon">
                                {if !empty($icatp.icon)}
                                {else}
                                    <span class="no-icon"></span>
                                {/if}
                            </span>
                            <a href="{$root_path}{$icatp.url}/" title="{$icatp.ten}">
                                {$icatp.ten}
                            </a>
                        </div>

                        <ul class="list-catp">
                    
                            {foreach from=$dataHome[$kycatp].categories key=kcatc item=icatc}
                                <li>
                                    <a href="{$root_path}{$icatc.url}/" title="{$icatc.ten}">
                                        {$icatc.ten}
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>

                    <div class="catp-prod clearfix">
                        <div class="row-prod clearfix">
                            {include file="../../product/tpl/item.tpl" arrsanpham=$dataHome[$kycatp].thread}
                        </div>
                    </div>
                    <!-- ./catp-prod -->
                </div>
            {/foreach}
        </div>
    {/if}
{/strip}