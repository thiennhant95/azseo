{strip}
{if $owl!=''}
    <div class="{$owl} owl-carousel owl-theme">
{/if}
{if $rowsp > 1}
    <div>
{/if}
<ul class="uldmcon">
{foreach key=cid item=arrCategories from=$arrsanpham}
    {* XU LY LIEN KET *}
    {if !empty($arrCategories.link)}
        {$lienket = $arrCategories.link}
    {else}
        {$lienket = $root_path|cat:$arrCategories.url}
    {/if}
    {* XU LY GIA *}
    {if !empty($arrCategories.gia)}
        {$dvt = 'đ'}
        {if is_numeric($arrCategories.gia)}
            {$gia = number_format($arrCategories.gia, 0, ',', '.')|cat:$dvt}
        {else}
            {$gia = $arrCategories.gia}
        {/if}
    {else}
        {$gia = $smarty.session.arraybien.lienhe}
    {/if}
    {* XU LY TARGET *}
    {if !empty($arrCategories.target)}
        {$target = "target={$arrCategories.target}"}
    {else}
        {$target = null}
    {/if}
    <li><a href="{$lienket}/" {$target} title="{$arrCategories.ten}"><i class="fa fa-angle-right"></i> {$arrCategories.ten}</a></li>
    {if ($cid+1) % $rowsp == 0 && $rowsp > 1}
    {/if}
{/foreach}
</ul>
{if $rowsp > 1}
    </div>
{/if}
{if $owl!=''}</div>{/if}
{/strip}