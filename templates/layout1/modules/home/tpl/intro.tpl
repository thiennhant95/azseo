{strip}
<div class="introContainer clearfix">
    <div class="introContent ws-container clearfix">
        <div class="titlebase clearfix">
            <a href="{$arrIntro[0].url}" title="{$arrIntro[0].ten}" >
                {$arrIntro[0].ten}
                <div class="line"></div>
            </a>
        </div>
        <!-- ./titlebase -->

        <div class="introbox row clearfix">
            <div class="intro-item introImg clearfix col-sm-6 col-xs-12">
                <img src="{$root_path}uploads/noidung/{$arrIntro[0].hinh}" alt="{$arrIntro[0].ten}" />
            </div>
            <div class="intro-item introDes clearfix col-sm-6 col-xs-12">
                {$arrIntro[0].mota}
            </div>
            <!-- ./introDes -->
        </div>

    </div>
    <!-- ./introContent -->
</div>
<!-- ./introContainer -->
{/strip}