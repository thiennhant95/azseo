{strip}
<div class="catContainer ws-container form-group clearfix">
    <div class="catContent clearfix">
        {foreach from=$dataHome.cat1 item=itemCat1 key=keyCat1}

            {$cat1BG = ''}
            {if !empty($itemCat1.img)}
                {$cat1BG = $root_path|cat:'uploads/danhmuc/'|cat:$itemCat1.img}
                {$cat1BG = 'style="background-image: url(\''|cat:$cat1BG|cat:'\'")'}
            {/if}

            {$cat1Link = "javascript:;"}
            {if !empty($itemCat1.url)}
                {$cat1Link = $root_path|cat:$itemCat1.url|cat:'/'}
            {/if}
            {if !empty($itemCat1.link)}
                {$cat1Link = $itemCat1.link}
            {/if}

            <div class="catItem clearfix" {$cat1BG}>

                <div class="catIBG clearfix">

                    <div class="catTitle clearfix">
                        <a href="{$cat1Link}" title="{$itemCat1.ten}">
                            {$itemCat1.ten}
                        </a>
                    </div>
                    <!-- ./catTitle -->

                    <div class="catChild clearfix">
                        <ul class="clearfix">
                            {foreach from=$dataHome[$keyCat1].cat2 item=itemCat2 key=keyCat2}
                                {$cat2Link = "javascript:;"}
                                {if !empty($itemCat2.url)}
                                    {$cat2Link = $root_path|cat:$itemCat2.url|cat:'/'}
                                {/if}
                                {if !empty($itemCat2.link)}
                                    {$cat2Link = $itemCat2.link}
                                {/if}

                                <li>
                                    <a href="{$cat2Link}" title="{$itemCat2.ten}">
                                        {$itemCat2.ten}
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                    <!-- ./catChild -->

                    <a class="btn btn-primary dangkylichhen" data-id="{$itemCat1.id}" data-toggle="modal" href='#modal-{$itemCat1.id}'>
                        <span>{$smarty.session.arraybien.datlichhen}</span>
                    </a>

                    <div id="select-data-{$itemCat1.id}"
                        class="hide"
                        data-value='
                            {foreach from=$dataHome[$keyCat1].cat2 item=itemCat2}
                                <option value="{$itemCat2.id}">{$itemCat2.ten}</option>
                            {/foreach}
                        '></div>

                    <div class="modal fade modal-tuvan" id="modal-{$itemCat1.id}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">{$itemCat1.ten}</h4>
                                </div>
                                <!-- ./modal-header -->
                                <div class="modal-body"></div>
                                <!-- ./modal-body -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        <i class="fa fa-close"></i>&nbsp;
                                        {$smarty.session.arraybien.dong}
                                    </button>
                                    <button type="button" data-loading-text="<i class=\'fa fa-spinner fa-spin \'></i> Đang gửi..." data-email="{$itemCat1.mota}" class="btn btn-primary btn-send-tuvan">
                                        <i class="fa fa-send"></i>&nbsp;
                                        {$smarty.session.arraybien.gui}
                                    </button>
                                </div>
                                <!-- ./modal-footer -->
                            </div>
                            <!-- ./modal-content -->
                        </div>
                        <!-- ./modal-dialog -->
                    </div>
                    <!-- ./modal fade -->
                </div>
                <!-- ./catIBG -->

            </div>
            <!-- ./catItem -->

        {/foreach}
    </div>
    <!-- ./catContent -->
</div>
<!-- ./catContainer -->
{/strip}