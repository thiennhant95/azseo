<script>
    function openTab(evt, tabName) {
        var i;
        var tabcontent = document.getElementsByClassName('tabcontent');
        var tablinks = document.getElementsByClassName('tablinks');

        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = 'none';
        }
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].classList.remove('vvvvv');
        }

        document.getElementById(tabName).style.display = 'block';
        event.currentTarget.classList.add('vvvvv');
    }
</script>
{strip}
<div class="catpContainer form-group clearfix">
    <div id="myTabs clearfix">
        {******* Tab *******}
        <div class="tabs">
            {foreach from=$dataHome.DmTabSp item=itemtab key=keytab}
            {* URL *}
            {$catpLink = "javascript:;"}
            {if !empty($itemtab.url)}
                {$catpLink = $root_path|cat:$itemtab.url|cat:'/'}
            {/if}

            {* LINK *}
            {if !empty($itemtab.link)}
                {$catpLink = $itemtab.link}
            {/if}
            {if !empty($itemtab.img)}
                {$catbg = $root_path|cat:'uploads/danhmuc/'|cat:$itemtab.img}
            {else}
                {$catbg = ''}
            {/if}
            {$ididi = $itemtab.idtype}


                <button class="tablinks" onclick="openTab(event, 'tab{$keytab}')" {if $keytab == 0}id="defaultOpen"{/if}>
                    <a  data-id="{$itemtab.id}" href="javascript:void(0);" title="{$itemtab.ten}">
                        <span class="tabName">{$itemtab.ten}</span>
                    </a>
                </button>
            {/foreach}
        </div>

        {*******Tab content*******}
        {foreach from=$dataHome.DmTabSp item=itemtab2 key=keytab2}
            {$ididi = $itemtab2.idtype}
            <div id="tab{$keytab2}" class="tabcontent">
                {if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'product'}
                    {include file="../../product/tpl/item.tpl" arrsanpham=$dataHome[$keytab2].NdTabSp owl='tabRun' rowsp='2'}
                {/if}
            </div>
        {/foreach}
        </div>
</div>
{/strip}