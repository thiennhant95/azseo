{strip}
<div class="catpContainer form-group clearfix">
    <div class="catpContent clearfix">
        {foreach from=$dataHome.catp item=itemcatp key=keycatp}
        {* URL *}
        {$catpLink = "javascript:;"}
        {if !empty($itemcatp.url)}
        {$catpLink = $root_path|cat:$itemcatp.url|cat:'/'}
        {/if}
        {* LINK *}
        {if !empty($itemcatp.link)}
        {$catpLink = $itemcatp.link}
        {/if}
        {if !empty($itemcatp.img)}
        {$catbg = $root_path|cat:'uploads/danhmuc/'|cat:$itemcatp.img}
        {else}
        {$catbg = ''}
        {/if}
        {* NỘI DUNG *}
        {$noidungdanhmuc = ''}
        {if !empty($itemcatp.noidung)}
        {$noidungdanhmuc = $itemcatp.noidung}
        {else}
        {$noidungdanhmuc = ''}
        {/if}
        {* ICON *}
        {$icon = ''}
        {if !empty($itemcatp.icon)}
        {$iconfile = $itemcatp.icon}
        {if substr($iconfile,0,2)=='fa'}
        {$icon = '<span class="img"><i class="fa '|cat:$itemcatp.icon|cat:'"></i></span>'}
        {else}
        {$icon = '<span class="img"><img src="'|cat:$root_path|cat:'uploads/danhmuc/'|cat:$itemcatp.icon|cat:'" alt="'|cat:$itemcatp.ten|cat:'" /></span>'}
        {/if}
        {/if}
        {$ididi = $itemcatp.idtype}

        {include file="../../quangcao/tpl/item.tpl" arrquangcao=$dataHome[$keycatp].quangcao class=""}
        {* {$keycatp|var_dump} *}
        <div class="catpItem cat{db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} clearfix">
            {* {if $keycatp!='0'} *}
            <div class="titlebase-home clearfix wow- animated- fadeInDown-" data-wow-delay="0s">
                <h2 class="category_tab">
                    <a  data-id="{$itemcatp.id}" href="{$catpLink}" title="{$itemcatp.ten}">
                        {* {$icon} *}
                        <span class="tentieude">{$itemcatp.ten}</span>
                    </a>
                    {if $itemcatp.mota != ''}
                    <span class="title-mota wow- animated- fadeInUp- clearfix">{$itemcatp.mota}</span>
                    {/if}
                <span class="tria"></span>
                </h2>
                <div class="danhmuccon">
                    <ul>
                        {foreach from=$dataHome[$keycatp].categories item=arrCategories key=cid}
                        {if !empty($arrCategories.link)}
                        {$lienket = $arrCategories.link}
                        {else}
                        {$lienket = $root_path|cat:$arrCategories.url}
                        {/if}
                        {$icon = ''}
                        {if !empty($arrCategories.icon)}
                        {$iconfile = $arrCategories.icon}
                        {if substr($iconfile,0,2)=='fa'}
                        {$icon = '<span class="img"><i class="fa '|cat:$arrCategories.icon|cat:'"></i></span>'}
                        {else}
                        {$icon = '<span class="img"><img src="'|cat:$root_path|cat:'uploads/danhmuc/'|cat:$arrCategories.icon|cat:'" alt="'|cat:$arrCategories.ten|cat:'" /></span>'}
                        {/if}
                        {/if}
                        <li class="category_tab">
                            <a data-id="{$arrCategories.id}" href="{$catpLink}" title="{$arrCategories.ten}">
                                {* {$icon} *}
                                <span class="tencon">{$arrCategories.ten}</span>
                                {* <div class="line"></div> *}
                            </a>
                        </li>
                        {/foreach}
                    </ul>
                </div>
                    {* <div class="xemtatca clearfix wow animated fadeInUp">
                        <a href="{$catpLink}" title="{$smarty.session.xemtatca}">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            {$smarty.session.arraybien.xemtatca}
                        </a>
                    </div> *}
            </div>
                {* {/if} *}
                <!-- /.titlebase -->
                {* {$keycatp} *}
                {* {$catpOP|var_dump} *}
                <div class="{db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi}Box clearfix">
                    {if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'product'}
                    {* <div class="bannertrai">
                        <img src='{$catbg}'/>
                        <span class="noidung">{$noidungdanhmuc}</span>
                    </div> *}
                    {include file="../../product/tpl/item.tpl" arrsanpham=$dataHome[$keycatp].thread owl='owl-carousel '|cat:'productRun'}
                    {elseif {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'video'}
                    {include file="../../video/tpl/item.tpl" arrsanpham=$dataHome[$keycatp].video owl='owl-carousel videoRun itemvideohome' rowsp="1"}
                    {else if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'service'}
                    {include file="../../service/tpl/item.tpl" arrservice=$dataHome[$keycatp].thread owl='owl-carousel serviceRun'}
                    {else if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'picture'}
                    {include file="../../picture/tpl/index.tpl" arrGallery=$dataHome[$keycatp].thread owl='owl-carousel '|cat:'pictureRun'}
                    {else if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'project'}
                    {include file="../../project/tpl/item.tpl" arrnoidung=$dataHome[$keycatp].thread owl='owl-carousel '|cat:'sanphamRun'}
                    {else if {db->getNameFromID p1="tbl_danhmuc_type" p2="op" p3="id" p4=$ididi} == 'intro'}
                    <div class="cottrai wow animated fadeInLeft">
                        <span class="image">
                            <a href="{$dataHome.$keycatp.intro.0.url}">
                                <img alt="{$dataHome.$keycatp.intro.0.ten}" class="img-responsive" src="{$root_path}uploads/noidung/{$dataHome.$keycatp.intro.0.hinh}" />
                            </a>
                        </span>
                    </div>
                    <div class="cotphai wow animated fadeInRight">
                        <span class="name">
                            <a href="{$dataHome.$keycatp.intro.0.url}" title="{$dataHome.$keycatp.intro.0.ten}">
                                {$dataHome.$keycatp.intro.0.ten}
                            </a>
                        </span>
                        <span class="gioithieumota">
                            {$dataHome.$keycatp.intro.0.mota}
                        </span>
                    </div>
                    {* {include file="../../intro/tpl/item.tpl" arrnoidung=$dataHome[$keycatp].thread owl=''} *}
                    {else}
                    {include file="../../content/tpl/item.tpl" arrnoidung=$dataHome[$keycatp].thread owl='owl-carousel '|cat:'contentRun'}
                    {/if}
                    {if count($dataHome[$keycatp].bannerqc) > 0}
                    {$qcHinh = $root_path|cat:'uploads/quangcao/'|cat:$dataHome[$keycatp].bannerqc[0].hinh}
                    {$qcHinh1 = $root_path|cat:'uploads/quangcao/'|cat:$dataHome[$keycatp].bannerqc[1].hinh}
                    <div class="banqcContainer clearfix">
                        <a href="{$dataHome[$keycatp].bannerqc[0].link}" title="{$dataHome[$keycatp].bannerqc[0].ten}">
                            <img src="{$qcHinh}" alt="{$dataHome[$keycatp].bannerqc[0].ten}" layout="responsive" />
                        </a>
                        <a href="{$dataHome[$keycatp].bannerqc[1].link}" title="{$dataHome[$keycatp].bannerqc[1].ten}">
                            <img src="{$qcHinh1}" alt="{$dataHome[$keycatp].bannerqc[1].ten}" layout="responsive" />
                        </a>
                    </div>
                    {/if}
                </div>
                {* <div class="xemthemsp ws-container">
                    <a href="{$catpLink}" title="{'xemthemsp'|arraybien}">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> {'xemthemsp'|arraybien}
                    </a>
                </div> *}
            </div>
            {* HÌNH GIỮA TRANG *}
            {* {if $keycatp=='0'}
                {$hinhgiuatrang}
            {/if} *}
            {/foreach}
        </div>
        <!-- ./catpContent -->
    </div>
{* <script type="text/javascript">
</script> *}
<!-- ./catpContainer -->
{/strip}