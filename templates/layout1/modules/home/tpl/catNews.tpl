{strip}
    <div class="new-container clearfix">
        {if count($dataHome.catNew) > 0}
            {foreach from=$dataHome.catNew key=kyNew item=iNew}
                <div class="new-wrap clearfix">
                    <div class="new-cat">
                        <span class="wsicon">
                            {$iNew.icon|getIcon:"{$root_path}uploads/danhmuc/"}
                        </span>
                        <a href="{$root_path}{$iNew.url}/" title="{$iNew.ten}">
                            {$iNew.ten}
                        </a>
                    </div>

                    <div class="new-content">
                        {include file="../../content/tpl/item.tpl" arrnoidung=$dataHome[$kyNew].catNewchild owl='owl-carousel owl-tintuchome'}
                    </div>
                </div>
            {/foreach}
        {/if}
    </div>
{/strip}