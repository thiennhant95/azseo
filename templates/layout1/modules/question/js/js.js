   // SEND QUESTION
    $('.btnSendQuestion').on('click', function() {
        //$(this).button('loading');
        var _idtype      = $(this).parent().parent().find('#this_product_idtype');
        var _idparent    = $(this).parent().parent().find('#this_product_idparent');
        var _hoten       = $(this).parent().parent().find('.ws-rq-hoten');
        var _email       = $(this).parent().parent().find('.ws-rq-email');
        var _dienthoai   = $(this).parent().parent().find('.ws-rq-dienthoai');
        var _noidung     = $(this).parent().parent().find('.ws-rq-noidung');
        var _captcha     = $(this).parent().parent().find('.ws-rq-captcha');
        if (!_hoten.val().length) {
            // Chua nhap Email
            $('.btnSendQuestion').button('reset');
            $(_hoten).focus();
            swal('Vui lòng nhập họ tên!', '', 'warning');
        }else if (!_noidung.val().length) {
            $('.btnSendQuestion').button('reset');
            $(_noidung).focus();
            swal('Vui lòng nhập nội dung!', '', 'warning');
        }else if (!_captcha.val().length) {
            $('.btnSendQuestion').button('reset');
            $(_captcha).focus();
            swal('Vui lòng nhập mã bảo vệ!', '', 'warning');
        }else{
          $.getJSON(_link + 'ajax/?method=question', {
              value       : 'question',
              idtype      : _idtype.val(),
              idparent    : _idparent.val(),
              hoten       : _hoten.val(),
              email       : _email.val(),
              dienthoai   : _dienthoai.val(),
              noidung     : _noidung.val(),
              captcha     : _captcha.val()
          }, function(json, textStatus) {
              if (json.status != 'success') {
                  if (json.status == 'exists') {
                      // Email ton tai
                      $('.btnSendQuestion').button('reset');
                      $(_hoten).val('');
                      swal(json.message, '', 'error');
                  } else if (json.status == 'saicaptcha'){
                      // Loi ngoai le
                      $('.btnSendQuestion').button('reset');
                      $(_captcha).val('');
                      swal(json.message, '', 'error');
                  }
              } else {
                  // Dang ky thanh cong
                  $('.btnSendQuestion').button('reset');
                  $(_hoten).val('');
                  $(_email).val('');
                  $(_dienthoai).val('');
                  $(_noidung).val('');
                  $(_captcha).val('');
                  // $(this).parent('li').find('#replace_binhluan:first').hide(300);
                  swal(json.message, '', 'success');
              }
          });
        }
    });

    $(".datcauhoi").click(function(){
        $(".formHoidap").toggle(300);
    });

    // AN HIEN HOI DAP
    $(".tieudehoidap").click(function(){
        $(this).parent().find(".noidunghoidap:last").toggle(300);
    });
