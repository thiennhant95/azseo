{strip}
{if $owl!=''}<div class="{$owl} owl-carousel owl-theme">{/if}
<ul class="ulHoidap">
{if count($arrnoidung) }
    {foreach key=cid item=arr_noidung from=$arrnoidung}

    {* XU LY LIEN KET *}
    {if !empty($arr_noidung.link)}
        {$lienket = $arr_noidung.link}
    {else}
        {$lienket = $root_path|cat:$arr_noidung.url}
    {/if}

    {* XU LY TARGET *}
    {if !empty($arr_noidung.target)}
        {$target = "target={$arr_noidung.target}"}
    {else}
        {$target = null}
    {/if}
        <li>
          <div class="tieudehoidap">
            <a href="javascript:" title="{$arr_noidung.ten}"><span><i class="fa fa-question"></i></span> {$arr_noidung.ten}</a>
          </div>
          <div class="noidunghoidap">{$arr_noidung.noidung}</div>
        </li>

    {/foreach}
{/if}
</ul>
{if $owl!=''}</div>{/if}
{/strip}