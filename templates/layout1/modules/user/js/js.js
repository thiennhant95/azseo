var _link = $("#rootPath").data("url");
$(function() {
    copy_to_clipboard = function(jd) {
        /* Get the text field */
        var copyText = $(jd);
        /* Select the text field */
        copyText.select();
        /* Copy the text inside the text field */
        document.execCommand("copy");
        /* Alert the copied text */
        $(window.event.target).attr("data-original-title", "Sao chép thành công").tooltip('show');
    },
    getmagioithieu = function(jd) {
        $btn = $(window.event.target);
        $.getJSON(_link + 'ajax/?method=getmagioithieu', function(json) {
            if( json && json.result ) {
                $(jd).val(_link + '?magioithieu=' +json.result);
                $btn.attr('onclick', 'copy_to_clipboard(\'#magioithieu\')').html('COPY');
            }
        });
    }
});
$(document).ready(function() {
   $("#myModal").on('shown.bs.modal', function(){
       $('.frmInfoUser').slideUp();
   });
   // Upload hinh dai dien
    $('#hinhdaidien_frm').live('submit', function(e){
        e.preventDefault();
        // AJAX
        $.ajax({
            cache: !1,
            url: _link + 'ajax/?method=uploadhinhdaidien',
            type: "POST",
            beforeSend: function(xhr) {
                $('.loading').show();
            },
            processData: !1,
            contentType: !1,
            data: new FormData(this),
            success: function(html) {
                $('.loading').hide();
                if( html.indexOf('_invalid_type') >= 0 ){
                    alert('Kiểu file không được phép !');
                }else if( html.indexOf('_invalid_size') >= 0 ){
                    alert('Kích thước file không được vượt quá 2MB !');
                }else{
                    $('#hinhdaidien_frm').trigger('reset');
                    $('#image-preview').attr('src', html);
                    $('.message').text('Upload thành công !').fadeIn().delay(2000).fadeOut();
                }
            }
        });
    });
    $('#filedaidien').live('change', function() {
        if( this.files && this.files[0] ){
            var $_files = this.files[0];
            var $_ext = $(this).val().substring(
                $(this).val().lastIndexOf('.') + 1
            ).toLowerCase();
            var $_validFileExtensions = [
                "jpeg",
                "png",
                "jpg",
                "gif"
            ];
            if( $.inArray($_ext, $_validFileExtensions) == -1 ){
                alert('Chỉ chấp nhận file hình');
            }else{
                var $_t = window.URL || window.webkitURL;
                var $_objectURL = $_t.createObjectURL($_files);
                $('#image-preview').attr('src', $_objectURL);
            }
        }
    });
    $('#submit').live('click', function(e) {
        e.preventDefault();
        $('#hinhdaidien_frm').ajaxSubmit({
            type: "POST",
            url: _link + 'ajax/',
            beforeSend: function(xhr){
                $('.loading').show();
            },
            success: function(html){
                $('.loading').hide();
                if( html.indexOf('_invalid_type') >= 0 ){
                    alert('Kiểu file không được phép !');
                }else if( html.indexOf('_invalid_size') >= 0 ){
                    alert('Kích thước file không được vượt quá 2MB !');
                }else{
                    $('#hinhdaidien_frm').trigger('reset');
                    $('#image-preview').attr('src', html);
                    $('.message').text('Upload thành công !').fadeIn().delay(10000).fadeOut();
                }
            }
        });
    });
   $("#UserUpdate").live("click", function() {
        $("#frmUEdit").wsFormError({
            notification: true,
            callback: function(wdata) {
                if ( !wdata ) {
                    var ary = [];
                    $("#frmUEdit input").each(function(index, el) {
                        var _data = $(this).data("value");
                        var _val  = $(this).val();
                        var _name = $(this).attr("name");
                        if (_val.length) {
                            // co du lieu
                            if (_data != _val) {
                                // Co chinh sua
                                $(this).attr("data-value", _val);
                                $(this).attr("value", _val);
                                ary.push(_name + ":" + _val);
                            }
                        }
                    });
                    if (ary.length) {
                        $.post(_link + "ajax/", {
                            ary: ary,
                            method: "uEdit"
                        }, function(data, textStatus, xhr) {
                            if( data ) {
                                ary = [];
                                swal(data.message, '' , 'success');
                                setTimeout(function() {
                                    $("#myModal").modal('hide');
                                }, 2e3);
                            }
                        }, 'json');
                    } else {
                        swal('Không có sự thay đổi','', 'info');
                    }
                }
            }
        });
    });
    $("#changePass").live("click", function() {
        $("#frmChangePass").wsFormError({
            notification: false,
            callback: function(data) {
                if (data == 0) {
                    var _passcurrent = $("#frmChangePass input[name=passcurrent]").val();
                    var _passnew = $("#frmChangePass input[name=passnew]").val();
                    var _passrenew = $("#frmChangePass input[name=passrenew]").val();
                    var _type = "success";
                    if (_passnew != _passrenew) {
                        swal("Mật khẩu không khớp !", "", "error");
                    } else {
                        $.post(_link + "ajax/?method=changepass", {
                            passcurrent: _passcurrent,
                            passnew: _passrenew
                        }, function(data, textStatus, xhr) {
                            if( data ) {
                                if( data.error ){
                                    if (data.error != "success") {
                                        _type = "danger";
                                    }
                                    if (data.error == "success") {
                                        _type = "success";
                                        $("#frmChangePass").find("input").val("");
                                    }
                                }
                                if ( data.message ) {
                                    swal(data.message, '', _type)
                                    setTimeout(function() {
                                        $("#myModal").modal('hide');
                                    }, 4000);
                                }
                            }
                        }, "json");
                    }
                }
            }
        });
    });
    // USER
    $("#userLogin").on("click", function() {
        $("#showError").html("");
        if (!$.isFunction($.fn.wsFormError)) {
            console.log("wsFormError undefine");
        }
        $("#frmCheckError").wsFormError({
            notification: true,
            callback: function(Data) {
                if (Data === 0) {
                    var U = $(this).find("input[name='username']").val();
                    var P = $(this).find("input[name='password']").val();
                    $.post(_link + "ajax/?method=login", {
                        method: "login",
                        username: U,
                        password: P
                    }, function(data) {
                        if( data ) {
                            if( data.message ){
                                if (!$("#showError").text().length) {
                                    $("#showError").prepend(data.message);
                                }
                            }
                            if (data.error == "success" && data.error) {
                                setTimeout(function() {
                                    location.reload();
                                }, 800);
                            }
                        }
                    }, "json");
                }
            }
        });
    });
    $("#logout").live("click", function() {
        if (confirm("Bạn có chắc muốn thoát?") === true) {
            $.getJSON(_link + "ajax/?method=logout", {}, function(json, textStatus) {
                if (typeof(json) !== 'undefined') {
                    if (json.error == "success") {
                        location.reload();
                    }
                } else {
                    location.reload();
                }
            });
        }
    });
    $("#userToggle").live('click', function() {
        if ($(".frmInfoUser").is(':visible')) {
            $(".frmInfoUser").hide();
            $(this).find('i.fa-times').removeClass('fa-times').addClass('fa-caret-down');
        } else {
            $(".frmInfoUser").show();
            $(this).find('i.fa-caret-down').removeClass('fa-caret-down').addClass('fa-times');
        }
    });
    $("#showU > p:first-child").live('click', function() {
        $this = $(this);
        if ($(".frmInfoUser").is(':visible')) {
            $(".frmInfoUser").hide();
           $this.prev('i.fa-times').removeClass('fa-times').addClass('fa-user');
        } else {
            $(".frmInfoUser").show();
           $this.prev('i.fa-user').removeClass('fa-user').addClass('fa-times');
        }
    });
    $("#frmUser").live("click", function() {
        $(".frmInfoUser").slideToggle("fast");
    });
    // SETUP AJAX
    $('#registerid').live('click',function(){
        var $parent          = $("#datalogin");
        var hovaten          = $parent.find("input[name='hovaten']");
        var email            = $parent.find("input[name='email']");
        var sodienthoai      = $parent.find("input[name='sodienthoai']");
        var gioitinh         = $parent.find("input[name='gioitinh']:checked");
        var loai             = $parent.find("input[name='loai']:checked");
        var password         = $parent.find("input[name='password']");
        var repassword       = $parent.find("input[name='repassword']");
        var manguoigioithieu = $parent.find("input[name='manguoigioithieu']");
        if( !hovaten.val().length ){
            swal_input(hovaten, hovaten.data('require'));
        }else if( !email.val().length ){
            swal_input(email, email.data('require'));
        }else if( !isEmail(email.val()) ){
            swal_input(email, email.data('require'));
        }else if( !sodienthoai.val().length ){
            swal_input(sodienthoai, sodienthoai.data('require'));
        }else if( !password.val().length ){
            swal_input(password, password.data('require'));
        }else if( password.val().length < 4  ){
           swal_input(password, 'Mật khẩu quá ngắn');
        }else if( !repassword.val().length ){
            swal_input(repassword, repassword.data('require'));
        }else if(password.val() != repassword.val()){
            swal_input(repassword, 'Mật khẩu không khớp');
        }else{
            $.getJSON(_link + 'ajax/', {
                method          : 'registerquery',
                hovaten         : hovaten.val(),
                email           : email.val(),
                sodienthoai     : sodienthoai.val(),
                loai            : loai.val(),
                gioitinh        : gioitinh.val(),
                password        : repassword.val(),
                manguoigioithieu: manguoigioithieu.val(),
            }, function(json, textStatus) {
                    if( json && json.status ) {
                        switch(json.status){
                            case 'emailexits':
                                swal(json.message,'','error');
                                break;
                            case 'success':
                                $('#myModal').modal('hide');
                                swal(json.message,'','success');
                                window.location.reload();
                                break;
                            default:
                                break;
                        }
                    }
            });
        }
    });
    $('#loginid').live('click',function(){
        var
            $thisparent = $("#datalogin"),
            email       = $thisparent.find("input[name='email']"),
            password    = $thisparent.find("input[name='password']"),
            remember    = $thisparent.find("input[name='remember']"),
            reval       = 0;
        if(remember.is(':checked')){
            reval = 1;
        }
        if( email.val() == '' ){
            email.focus();
            swal(email.data('require'),'','error');
            return false;
        }else if( password.val() == '' ){
            password.focus();
            swal(password.data('require'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                method: 'loginquery',
                email: email.val(),
                password:password.val(),
                remember: reval
            }, function(json, textStatus) {
                switch(json.status){
                    case 'error':
                        swal(json.message,'','error');
                        break;
                    case 'success':
                        if ( $(window).width() > 768 ) {
                            $(".login-box").html(json.result);
                        } else {
                            $(".user-mobile").html(json.result);
                            setTimeout(function() {
                                window.location.reload();
                            }, 800);
                        }
                        swal(json.message,'','success');
                        break;
                }
                $("#loaddatacart").html('');
                $('#myModal').modal('hide');
            });
        }
    });
    // Su dung nut enter
    $("input[name='password'],input[name='email']").live('keyup', function(e) {
        if ( e.keyCode == 13 ){
            $('#loginid').trigger('click');
        }
    });
    $('#forgotpasswordid').live('click',function(){
        var $thisparent = $("#datalogin");
        var email       = $thisparent.find("input[name='email']");
        if(email.val()==''){
            email.focus();
            swal(email.data('require'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                method: 'forgotpasswordquery',
                email: email.val()
            }, function(json, textStatus) {
                switch(json.status){
                    case 'error':
                        swal(json.message,'','error');
                        break;
                    case 'success':
                        swal(json.message,'','success');
                        $('#myModal').modal('hide');
                        break;
                }
            });
        }
    });
    $('#changepassid').live('click',function(){
        var thjs        = $(this),
            $thisparent = $("#frmChangePass"),
            password    = $thisparent.find("input[name='passcurrent']"),
            passnew     = $thisparent.find("input[name='passnew']"),
            repassword  = $thisparent.find("input[name='passrenew']");
        // forgostpass
        var maactive = $(this).data('maactive');
        var email = $(this).data('email');
        if( typeof(maactive) !== "undefined" ){
            var forgotFrm = $("#forgotpass_frm");
            password      = forgotFrm.find("input[name='password']");
            repassword    = forgotFrm.find("input[name='repassword']");
        }
        // Kiem da nhap mat khau hay chua
        if( password.val() == '' ){
            password.focus();
            swal('vui lòng nhập mật khẩu','','error');
            return false;
        }else{
            // FORGOT PASSWORD
            if( typeof(maactive) !== "undefined" ){
                if( repassword.val() == '' ){
                    repassword.focus();
                    swal('vui lòng nhập mật khẩu mới','','error');
                    return false;
                }else if( repassword.val().length < 5 ){
                    repassword.focus();
                    swal('Mật khẩu phải lớn hơn 6 ký tự','','error');
                    return false;
                }else if( password.val() != repassword.val() ){
                    repassword.focus();
                    swal('mật khẩu sai','','error');
                    return false;
                }else{
                    $.post(_link + 'ajax/', {
                        method: 'changepassquery',
                        email: email,
                        maactive: maactive,
                        password: repassword.val()
                    },
                        function (data, textStatus, jqXHR) {
                            if( data && data.status && data.message ){
                                swal(data.message,'',data.status);
                                if( data.status == 'success' ){
                                    setTimeout(function() {
                                        location.href = 'index.php';
                                    }, 2000);
                                }
                            }
                        },
                        "json"
                    );
                }
            }else{
                // kiem tra mat khau do da dung chua?
                $.ajax({
                    url: _link + 'ajax/',
                    sync: !1,
                    cache: !1,
                    type: "GET",
                    traditional: !0,
                    data: {
                        method: "passcheck",
                        passcheck: password.val()
                    },
                    success: function( data ) {
                        if ( data ) {
                            if( typeof(thjs.data('email')) === "undefined" ){
                                swal('Không thể đổi mật khẩu','','error');
                                return;
                            }
                            if( passnew.val() == '' ){
                                passnew.focus();
                                swal('vui lòng nhập mật khẩu mới','','error');
                                return false;
                            }else if( passnew.val().length < 5 ){
                                passnew.focus();
                                swal('Mật khẩu phải lớn hơn 6 ký tự','','error');
                                return false;
                            }else if(repassword.val()==''){
                                repassword.focus();
                                swal('vui lòng nhập mật khẩu mới','','error');
                                return false;
                            }else if(passnew.val() != repassword.val()){
                                repassword.focus();
                                swal('mật khẩu sai','','error');
                                return false;
                            }else{
                                $.post(_link + 'ajax/', {
                                    method: 'changepassquery',
                                    repassword: repassword.val(),
                                    email: thjs.data('email'),
                                    macctive: null
                                },function (data, textStatus, jqXHR) {
                                    if( data ) {
                                        $('#myModal').modal('hide');
                                        if( data.message ){
                                            swal(data.message,'',data.status);
                                        }
                                    }
                                }, "json" );
                            }
                        } else {
                            // Truong hop pass hien tai khong dung
                            swal('Mật khẩu hiện tại không đúng', '', 'error');
                        }
                    }
                })
            }
        }
    });
});
function isEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};
function swal_input( el, mess, mess_type='error' ){
    if( el !== null && typeof(el) !== 'undefined' ){
        el.focus();
    }
    swal(mess,'',mess_type);
    return false;
}