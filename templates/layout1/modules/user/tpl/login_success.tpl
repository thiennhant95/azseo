{strip}
	{if !$didong->isMobile()}
    <a href="javascript:;" id="frmUser" title="{$smarty.session.arraybien.xinchao}">
        {$smarty.session.arraybien.xinchao} <strong>{$smarty.session.user_ten}</strong>&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
    </a>
    {/if}

    {if $didong->isMobile()}
        <a href="javascript:void(0)" title="User" id="userToggle">
            <div class="fa fa-user fa-lg"></div>&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
        </a>
    {/if}

    {include file="./boxUserLogin.tpl" root_path=$root_path}

{/strip}