{strip}
<div class="btn btn-login">
    <a href="javascript:void(0)" class="btn-desktop" title="{$smarty.session.arraybien.dangnhap}">
        {$smarty.session.arraybien.dangnhap} <i class="fa fa-caret-down"></i>
    </a>
    <a href="javascript:void(0)" class="btn-mobile">
        <div class="fa fa-user fa-lg"></div>
        <!-- .fa fa-user fa-lg -->
    </a>
    {include file="{$smarty.current_dir}/boxUserLogin.tpl"}
</div>
{/strip}