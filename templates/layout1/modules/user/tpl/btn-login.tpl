<div class="btn-login">
    <p><i class="fa fa-user" aria-hidden="true"></i></p>
    <div class="hidden-sms">
        {'taikhoan'|arraybien}
    </div>
    <i class="fa fa-caret-down fa-lg" aria-hidden="true"></i>

    <ul class="login-choose clearfix">

        <li>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal" title="Đăng nhập" onclick="Get_Data('{$root_path}ajax/?method=login','loaddatacart');">
                <i class="fa fa-user fa-1x" aria-hidden="true"></i>&nbsp;
                {'dangnhap'|arraybien}
            </a>
        </li>

        <li>
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" title="Đăng ký" onclick="Get_Data('{$root_path}ajax/?method=register','loaddatacart');">
                <i class="fa fa-user-plus fa-1x" aria-hidden="true"></i>&nbsp;
                {'dangky'|arraybien}
            </a>
        </li>

    </ul>
</div>