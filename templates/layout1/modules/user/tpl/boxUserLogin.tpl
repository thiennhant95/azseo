<div class="frmInfoUser clearfix">
    <ul>
        <li>
            <strong>
               <a href="javascript:void(0)" title="{$smarty.session.user_ten}">
                   {$smarty.session.user_ten}
               </a>
            </strong>
        </li>
        <li>
            <a data-toggle="modal" data-target="#myModal" title="{'capnhatthongtin'|arraybien}" onclick="Get_Data('{$root_path}ajax/?method=changeUser', 'loaddatacart')">
                {'capnhatthongtin'|arraybien}
            </a>
        </li>
        <li>
            <a data-toggle="modal" data-target="#myModal" title="{'linkgioithieu'|arraybien}" onclick="Get_Data('{$root_path}ajax/?method=changeUser', 'loaddatacart')">
                {'linkgioithieu'|arraybien}
            </a>
        </li>
        <li>
            <a href="{$root_path}my-cart.html" title="{'donhangcuatoi'|arraybien}">
                {'donhangcuatoi'|arraybien}
            </a>
        </li>
        <li>
            <a data-toggle="modal" data-target="#myModal" title="{'doimatkhau'|arraybien}" onclick="Get_Data('{$root_path}ajax/?method=changePW', 'loaddatacart')">
                {'doimatkhau'|arraybien}
            </a>
        </li>
        <li>
            <a href="javascript:;" title="" id="logout">
                {'thoat'|arraybien}
            </a>
        </li>
    </ul>
</div>