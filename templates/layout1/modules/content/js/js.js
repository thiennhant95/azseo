$(function(){
	if(typeof _link === 'undefined' ) _link = $('#rootPath').data('url');
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
	
	$('#tuyendung-btn').on('click', function() {
		var frm = "#tuyendung_form",
			btn = $(this);

		$(frm).wsFormError({
			notification: true,
			callback: function(e) {
				if( e == 0 )
					$(frm).submit();
			}
		});
	})
});