{strip}
{foreach key=cid item=arr_noidung from=$arrnoidung}

{* XU LY LIEN KET *}
{if !empty($arr_noidung.link)}
    {$lienket = $arr_noidung.link}
{else}
    {$lienket = $root_path|cat:$arr_noidung.url}
{/if}

{* XU LY TARGET *}
{if !empty($arr_noidung.target)}
    {$target = "target={$arr_noidung.target}"}
{else}
    {$target = null}
{/if}
{$linkhinh = $root_path|cat:'uploads/noidung/thumb/'|cat:$arr_noidung.hinh}
{if $cid<4} {* CCHI LAY 4 TIN DAU TIEN *}
  {if $cid==0}
    {$linkhinh = $root_path|cat:'uploads/noidung/'|cat:$arr_noidung.hinh}
    <div class="news-col news-col1">
  {/if}
    <div itemscope itemtype="\/\/schema.org/NewsArticle" class="itemnoidung clearfix {$class}">
      <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="{$lienket}"/>
      <h3 itemprop="author" itemscope itemtype="https://schema.org/Person">
         <span itemprop="name" class="hidden">{$arr_noidung.ten}</span>
      </h3>
      <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject" class="image">
        <a title="{$arr_noidung.ten}" href="{$lienket}" {$target}><img src="{$linkhinh}" alt="{$arr_noidung.ten}" /></a>
        <meta itemprop="url" content="{$lienket}">
        <meta itemprop="width" content="350">
        <meta itemprop="height" content="auto">
      </div>
      <h2 itemprop="headline" class="name">
          <a title="{$arr_noidung.ten}" href="{$lienket}" {$target}>{$arr_noidung.ten}</a>
      </h2>
      <span class="ngay">{$arr_noidung.ngaycapnhat|date_format:"d/m/Y"}</span>
      <span itemprop="description" class="description">{$arr_noidung.mota}</span>
      <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject" class="hidden">
          <img src="{$root_path}uploads/noidung/thumb/{$arr_noidung.hinh}"/>
          <meta itemprop="url" content="{$lienket}">
          <meta itemprop="width" content="600">
          <meta itemprop="height" content="60">
        </div>
        <meta itemprop="name" content="Google">
      </div>
      <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
      <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>
      <div class="chitiet"><a title="{$smarty.session.arraybien.chitiet}" href="{$lienket}" {$target}>{$smarty.session.arraybien.chitiet} <i class="fa fa-angle-double-right"></i></a></div>
    </div>
  {if $cid==0}
    </div>
    <div class="news-col news-col2">
  {/if}
{/if} {* END CID >3 *}
{/foreach}
{if $cid>0}
  </div>
{/if}
{/strip}