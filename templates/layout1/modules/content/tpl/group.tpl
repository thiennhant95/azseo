
{foreach key=cid item=arr_noidung from=$arrnoidung}

{* XU LY LIEN KET *}
{if !empty($arr_noidung.link)}
    {$lienket = $arr_noidung.link}
{else}
    {$lienket = $root_path|cat:$arr_noidung.url}
{/if}


{* XU LY TARGET *}
{if !empty($arr_noidung.target)}
    {$target = "target={$arr_noidung.target}"}
{else}
    {$target = null}
{/if}

{if $cid==0}
    <div class="nhomtin_col1">
        <div class="itemtin">
            <div class="img">
                <a title="{$arr_noidung.ten}" href="{$lienket}" {$target}>
                   <img src="{$root_path}uploads/noidung/thumb/{$arr_noidung.hinh}" alt="{$arr_noidung.ten}"  />
                </a>
            </div>
            <div class="description">
                <p class="titletin"><a href="{$lienket}" {$target} title="{$arr_noidung.ten}">{$arr_noidung.ten}</a></p>
                <p class="mota">{strip_tags($arr_noidung.mota)}</p>
            </div>
        </div>
    </div>
    <div class="nhomtin_col2">
{else}
    <div class="itemtin">
        <div class="img">
            <a title="{$arr_noidung.ten}" href="{$lienket}" {$target}>
               <img src="{$root_path}uploads/noidung/thumb/{$arr_noidung.hinh}" alt="{$arr_noidung.ten}"  />
            </a>
        </div>
        <p class="titletin"><a title="{$arr_noidung.ten}" href="{$lienket}" {$target}>{$arr_noidung.ten}</a></p>
        <div class="mota">{strip_tags($arr_noidung.mota)}</div>
    </div>
{/if}

{if count($arr_noidung) == ($cid+1)}
    </div>
{/if}
{/foreach}