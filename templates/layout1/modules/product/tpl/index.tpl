{strip}
{* {$braucube} *}
<h1 class="product-title-big hidden">{$ten}</h1>

    {if $smarty.session.flow == 2}
        {$classView = 'ws-list'}
    {else}
        {$classView = 'ws-grid'}
    {/if}

    {if !empty($mota_menu)}
        <div class="noidung_menu">{$mota_menu}</div>
    {/if}

        {if $arrdata.subcat|@count gt 0}
            {include file="../../{$op}/tpl/itemGroup.tpl" arrsanpham=$arrdata.sanphamgroup }
        {else}
            {if $arrdata.datasort}
                {$arrdata.datasort}
            {/if}

            <div class="clearfix product-container {$classView}" id="loaditemsanpham">
                {include file="../../{$op}/tpl/item.tpl" arrsanpham=$arrdata.sanpham class='' owl=''}
                <div class="info-container2">
                    {$thongtincuoiwebsite}
                </div>
            </div>
        {/if}
    {if !empty($noidung_menu)}
        <div class="noidung_menu">{$noidung_menu}</div>
    {/if}
<div class="clear"></div>
<h2 class="hide">{$ten}</h2>
{$phantrang}
{/strip}