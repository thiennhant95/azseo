{strip}
{$braucube}
<h1 class="product-title-big option">{$ten}</h1>
{if $smarty.session.flow == 2}
    {$classView = 'ws-list'}
{else}
    {$classView = 'ws-grid'}
{/if}
{if !empty($noidung_menu)}
<div class="noidung_menu">{$noidung_menu}</div>
{/if}
{$filterdata}
<div class="clearfix product-container {$classView}" id="loaditemsanpham">
{include file="../../{$op}/tpl/item.tpl" arrsanpham=$arrdata.sanpham class='' owl=''}
</div>
<div class="clear"></div>
{$phantrang}
{/strip}