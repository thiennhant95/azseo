{strip}
{if $owl!=''}
    <div class="{$owl} owl-carousel owl-theme">
{/if}
{if $rowsp > 1}
    <div>
{/if}
{if count($arrsanpham)}
    {foreach key=cid item=arrCategories from=$arrsanpham}
        {* XU LY LIEN KET *}
        {if !empty($arrCategories.link)}
            {$lienket = $arrCategories.link}
        {else}
            {$lienket = $root_path|cat:$arrCategories.url}
        {/if}
        {* XU LY GIA *}
        {if !empty($arrCategories.gia)}
            {$dvt = 'đ'}
            {if is_numeric($arrCategories.gia)}
                {$gia = number_format($arrCategories.gia, 0, ',', '.')|cat:$dvt}
            {else}
                {$gia = $arrCategories.gia}
            {/if}
        {else}
            {$gia = $smarty.session.arraybien.lienhe}
        {/if}
        {* XU LY TARGET *}
        {if !empty($arrCategories.target)}
            {$target = "target={$arrCategories.target}"}
        {else}
            {$target = null}
        {/if}
        {* PERCENT *}
        {if (!empty($arrCategories.giagoc) && !empty($arrCategories.gia)) && ($arrCategories.giagoc > $arrCategories.gia)}
            {$percent = ($arrCategories.gia - $arrCategories.giagoc) / $arrCategories.giagoc * 100}
            {$percent = {$percent|string_format:"%d"}  }
        {/if}
        {* RATING *}
        {if empty($arrCategories.rating)}
            {$rating = 10}
        {else}
            {$rating = $arrCategories.rating}
        {/if}
        {if empty($arrCategories.ratingCount)}
            {$ratingCount = 1}
        {else}
            {$ratingCount = $arrCategories.ratingCount}
        {/if}
        {$ratingvalue = number_format(($ratingCount/$rating),1)}
        {if empty($arrCategories.gia)}
            {$price = 1}
        {else}
            {$price = $arrCategories.gia}
        {/if}
    <div itemscope itemtype="\/\/schema.org/Product" class="wow animated fadeInDown itemcategories  clearfix {$class} " data-wow-delay="0.{$cid}s">
        <div class="clearfix">
            <a href="{$lienket}/" title="{$arrCategories.ten}">
                <span itemprop="image" class="image">
                        <img src="{$root_path}uploads/danhmuc/thumb/{$arrCategories.img}" alt="{$arrCategories.ten}" class="img-responsive" />
                </span>
            </a>
            <span itemprop="name" class="name">
                <a href="{$lienket}/" title="{$arrCategories.ten}">{$arrCategories.ten}</a>
            </span>
            <span itemprop="description" class="description">{$arrCategories.mota}</span>
        </div>
    </div>
        {if ($cid+1) % $rowsp == 0 && $rowsp > 1}
            </div><div>
        {/if}
    {/foreach}
{/if}
{if $rowsp > 1}
    </div>
{/if}
{if $owl!=''}</div>{/if}
{/strip}