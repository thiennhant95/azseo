{strip}
{if $owl!=''}
    <div class="{$owl} owl-carousel">
{/if}
{if $rowsp > 1}
    <div>
{/if}
{if count($arrsanpham)}
    {foreach key=cid item=arrProduct from=$arrsanpham}
        {$dvt = ' đ'}
        {$gia = $arrProduct.gia}
        {$giagoc = $arrProduct.giagoc}
        {$gia_xl = number_format($gia, 0, ',', '.')|cat:$dvt}
        {$giagoc_xl = number_format($giagoc, 0, ',', '.')|cat:$dvt}
        {* XU LY LIEN KET *}
        {if !empty($arrProduct.link)}
            {$lienket = $arrProduct.link}
        {else}
            {$lienket = $root_path|cat:$arrProduct.url}
        {/if}
        {* XU LY GIA *}
        {if !empty($gia)}
            {if is_numeric($gia)}
                {$gia_xem = $gia_xl}
            {else}
                {$gia_xem = $gia}
            {/if}
        {else}
            {$gia_xem = $smarty.session.arraybien.lienhe}
        {/if}
         {* XU LY GIA GOC *}
        {if !empty($giagoc)}
            {if is_numeric($giagoc)}
                {$giagoc_xem = $giagoc_xl}
            {else}
                {$giagoc_xem = $giagoc}
            {/if}
        {else}
            {$giagoc_xem = ''}
        {/if}
        {* XU LY TARGET *}
        {if !empty($arrProduct.target)}
            {$target = "target={$arrProduct.target}"}
        {else}
            {$target = null}
        {/if}
        {* PERCENT *}
        {if (!empty($giagoc) && !empty($gia)) && ($giagoc > $gia)}
        {$percent = ($gia - $giagoc) / $giagoc * 100}
        {$percent = {$percent|string_format:"%d"}  }
        {/if}
        {* RATING *}
        {if $arrProduct.rating == 0}
            {$rating = 5}
        {else}
            {$rating = $arrProduct.rating}
        {/if}
        {if $arrProduct.ratingCount == 0}
            {$ratingCount = 1}
        {else}
            {$ratingCount = $arrProduct.ratingCount}
        {/if}
        {$ratingValue = number_format(($ratingCount/$rating),1)}
        {if empty($arrProduct.gia)}
            {$price = 1}
        {else}
            {$price = $arrProduct.gia}
        {/if}
        {* Thành phần *}
        {if !empty($arrProduct.thanhphan)}
            {$thanhphan = "{$arrProduct.thanhphan}"}
        {else}
            {$thanhphan = null}
        {/if}
    <div itemscope itemtype="\/\/schema.org/Product" class="itemsanpham wow- animated- zoomIn- clearfix {$class}" data-wow-delay="0.{$cid}s">
        <div class="clearfix itemsanphamBox">
            <span itemprop="brand hidden"></span>
                {* Hinh anh *}
                <div itemprop="image" class="image">
                    {if !empty($giagoc)}
                        <div class="percent">
                            {$percent}%
                        </div><!-- /.percent -->
                    {/if}
                    <a href="{$lienket}" title="{$arrProduct.ten}">
                        <img src="{$root_path}uploads/noidung/thumb/{$arrProduct.hinh}" alt="{$arrProduct.ten}" class="img-responsive" />
                    </a>
                    <div class="wishlist-box">
                        <div>
                            <a href="#" title="{'themgiohang'|arraybien}" rel="nofollow" data-toggle="modal"  data-target="#myModal" onclick="MuaHang('{$root_path}ajax/?op=giohang&id={$arrProduct.id}&method=add','loaddatacart');" >
                                <i class="fa fa-cart-plus"></i>
                            </a>
                            <a href="{$lienket}" title="{'chitiet'|arraybien}" rel="nofollow">
                                <i class="fa fa-info-circle"></i>
                            </a>
                            {* <a onclick="Get_Data('{$root_path}ajax/?method=saveproduct&value={$id}', 'save{$id}')" title="save" rel="nofollow" id="save{$id}"> *}
                                {* {exisCookie $id} *}
{*                                 {if {exisCookie value=$id} }
                                    <i class="fa fa-bookmark"></i>
                                {else}
                                    <i class="fa fa-bookmark-o"></i>
                                {/if} *}
                            {* </a> *}
                        </div>
                    </div><!-- /.wishlist-box -->
                </div>
                {* Ten *}
                <div itemprop="name" class="name">
                    <a href="{$lienket}" title="{$arrProduct.ten}">
                        {$arrProduct.ten}
                    </a>
                </div>
                {* GIÁ *}
                <div class="gia">
                    {if {db->isSale p1=$arrProduct.id}}
                        {$arrProduct.giasale|number_format:0:",":"."} {$dvt}
                        <span class="giagoc">{$gia_xem}</span>
                    {else}
                        {$gia_xem}
                        {if !empty($giagoc)}
                        <span class="giagoc">{$giagoc_xl}</span>
                        {/if}
                    {/if}
                </div>
                    <div class="deal-status">
                {if {db->isSale p1=$arrProduct.id}}
                        <div class="time" data-countdown="{$arrProduct.timesale}"></div>
                        <div class="process-bar">
                            <div style="width: {db->percentProcess p1=$arrProduct.id}%;"></div>
                            <span class="text">Đã bán {db->sold p1=$arrProduct.id}</span><!-- /.text -->
                        </div><!-- /.process-bar -->
                {/if}
                    </div><!-- /.deal-status -->
                {* Thành phần *}
                {* <div class="thanhphan">
                    {$thanhphan}
                </div> *}
                {* GIA BAN *}
                {* {if TRUE} *}
                    {* Gia *}
                    {* <div class="price">
                        <p>{$smarty.session.arraybien.giacu}:&nbsp;</b>
                            <span>{$giagoc_xem}</span></p>
                        <p>{$smarty.session.arraybien.giakm}:&nbsp;</b>
                            <span>{$gia_xem}</span></p>
                    </div> *}
                    {* Danh gia *}
                    {* {if !1}
                        <div class="rating">
                            {$ratingdt = "rating"|cat:$arrProduct.id}
                            <div class="starrr clearfix" data-id="{$arrProduct.id}" data-session="{$smarty.session.$ratingdt.value}"></div>
                        </div>
                    {/if}
                {/if} *}
                {* <div class="btn-detail btn-action">
                    <a href="{$lienket}" title="{$arrProduct.ten}">
                        {$smarty.session.arraybien.chitiet}
                    </a>
                </div> *}
                {* Mo ta *}
                {* {if !1}
                    <span class="description" itemprop="description">
                        {$arrProduct.mota|strip_tags:"<p>"|truncate:120}
                    </span>
                {/if} *}
                {* Struct *}
                <span itemprop="aggregateRating" itemscope itemtype="\/\/schema.org/AggregateRating" class="hidden">
                   <span itemprop="ratingValue">{$rating}</span> {$smarty.session.arraybien.sao}, {$smarty.session.arraybien.duatren} <span itemprop="reviewCount">{$ratingCount} </span> {$smarty.session.arraybien.danhgia}
                </span>
                <span itemprop="offers" itemscope itemtype="\/\/schema.org/AggregateOffer" class="hidden">
                    <span itemprop="lowPrice">{$price}</span> -
                    <span itemprop="highPrice">{$price}</span>
                    <meta itemprop="priceCurrency" content="VND" />
                </span>
                {* <div class="rating_group">
                    <div class="jrating jratingcategories" data-average="{$ratingValue}" data-id="{$arrProduct.id}"></div>
                </div>
                <span class="itemsanphamFooter clearfix">
                    <span class="price">
                            <b>{$smarty.session.arraybien.gia}:</b> {$gia}
                    </span>
                </span> *}
    {*             <div class="btn-group">
                    <div class="btn-cart btn-action">
                        <a href="#" title="" data-toggle="modal"  data-target="#myModal" onclick="MuaHang('{$root_path}ajax/?op=giohang&id={$arrProduct.id}&method=add','loaddatacart');" >
                            {$smarty.session.arraybien.dathang}
                        </a>
                    </div>
                </div> *}
        </div>
    </div>
        {if ($cid+1) % $rowsp == 0 && $rowsp > 1 && count($arrsanpham) > ($cid+1)}
            </div><div>
        {/if}
    {/foreach}
{/if}
{if $rowsp > 1}
    </div>
{/if}
{if $owl!=''}</div>{/if}
{/strip}