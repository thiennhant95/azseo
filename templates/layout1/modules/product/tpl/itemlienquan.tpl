{strip}
{if $owl!=''}
<div class="{$owl} owl-carousel owl-theme">
    {/if}
    {if $rowsp > 1}
    <div>
        {/if}
        {if count($arrsanpham) }
        {foreach key=cid item=arrProduct from=$arrsanpham}
        {* XU LY LIEN KET *}
        {if !empty($arrProduct.link)}
        {$lienket = $arrProduct.link}
        {else}
        {$lienket = $root_path|cat:$arrProduct.url}
        {/if}

        <div itemscope itemtype="\/\/schema.org/Product" class="itemsanpham wow animated zoomIn clearfix {$class}"  data-wow-delay="0.{$cid}s">
            <div class="clearfix itemsanphamBox">
                <span itemprop="brand hidden"></span>

                <span itemprop="image" class="image">
                    <a href="{$lienket}">
                        <img src="{$root_path}uploads/noidung/thumb/{$arrProduct.hinh}" alt="{$arrProduct.ten}" class="img-responsive" />
                    </a>
                </span>

                <span itemprop="name" class="name">
                    <a href="{$lienket}" title="{$arrProduct.ten}">
                        {$arrProduct.ten}
                    </a>
                </span>
                {* <div class="btn-detail btn-action">
                    <a href="{$lienket}" title="{$arrProduct.ten}" class="xemchitiet">
                        {$smarty.session.arraybien.xemthem}
                    </a>

                    <a href="{$root_path}uploads/files/{$arrProduct.file}" title"{$smarty.session.arraybien.downloadfile}" class="catalogue" download>
                        <i class="fa fa-cloud-download" aria-hidden="true"></i>
                        {$smarty.session.arraybien.catalogue}
                    </a>
                </div> *}

                <span itemprop="aggregateRating" itemscope itemtype="\/\/schema.org/AggregateRating" class="hidden">
                 <span itemprop="ratingValue">{$rating}</span> {$smarty.session.arraybien.sao}, {$smarty.session.arraybien.duatren} <span itemprop="reviewCount">{$ratingCount} </span> {$smarty.session.arraybien.danhgia}
             </span>
             <span itemprop="offers" itemscope itemtype="\/\/schema.org/AggregateOffer" class="hidden">
                <span itemprop="lowPrice">{$price}</span> -
                <span itemprop="highPrice">{$price}</span>
                <meta itemprop="priceCurrency" content="VND" />
            </span>
        </div>
    </div>
    {if ($cid+1) % $rowsp == 0 && $rowsp > 1}
</div><div>
    {/if}
    {/foreach}
    {/if}
    {if $rowsp > 1}
</div>
{/if}
{if $owl!=''}</div>{/if}
{/strip}