/** Send Filter */
$(".click_filter").click(function() {
    var idnhomid = $(this).find('input[name=this_value_nhomid]').val();
    var idnhomnd = $(this).find('input[name=this_value_nhomnd]').val();
    var idlink = $(this).find('input[name=this_link_loc]').val();
    $("#ws-wating").show();
    $.ajax({
        url: idlink,
        type: 'GET',
        data: { idnhomid: idnhomid, idnhomnd: idnhomnd },
        async: true,
        cache: false,
        success: function(idata) {
            setTimeout(function() {
                $("#ws-wating").hide();
                $("#loaditemsanpham").html(idata);
            }, 300);
        },
        error: function(request, status, error) {
            swal('Error', '', 'error');
        }
    });
    // e.preventDefault();
});

// LOC GIA
$("#filter_price li").on('click', function() {
    var thjs = $(this),
        val = thjs.data().price;

    thjs.parent().find('li').removeClass('active');
    thjs.addClass('active');

    if( !! val ){

        $.ajax({
            cache: false,
            type: "POST",
            url: _link + "modules/_files/locNhanh_ajax.php",
            data: {
                value: JSON.stringify(val)
            },
            success: function (response) {
                $("#loaditemsanpham").html(response);
            }
        });

    }
});

// DAT HANG NHANH
/*

$("#btn_order_quick").on('click', function() {
    var thjs = $(this),
        frm = "#frm-order";

    $(frm).wsFormError({
        notification: true,
        callback: function(data) {
            if (data == 0) {
                // your task
                var
                    hoten     = $(frm).find("input[name=hoten]").val(),
                    dienthoai = $(frm).find("input[name=dienthoai]").val(),
                    email     = $(frm).find("input[name=email]").val(),
                    idsp      = $(frm).find("input[name=idsp]").val(),
                    ghichu    = $(frm).find("textarea[name=ghichu]").val();

                $.ajax({
                    cache: false,
                    type: "GET",
                    url: _link + "ajax/?op=giohang",
                    data: {
                        id: idsp,
                        method: "add",
                        sl: 1,
                        hoten: hoten,
                        dienthoai: dienthoai,
                        email: email,
                        ghichu: ghichu
                    },
                    success: function (response) {
                        $("#thongbaodathang").html( alertBootStrap(`
                            <a href="`+_link+`cart.htm" title="Tiến hành thanh toán">
                                <button type="button" class="btn btn-small btn-success">
                                    Tiến hành thanh toán
                                </button>
                            </a>
                        `) );
                    }
                });
            }
        }
    });

});

*/

$('#frmOder').on('submit', function(e) {
    e.preventDefault();

    // SEND INFO
    var thjs = $(this),
        frm = "#frm-order";

    $(frm).wsFormError({
        notification: true,
        callback: function(data) {
            if (data == 0) {
                // your task
                var
                    hoten     = $(frm).find("input[name=hoten]").val(),
                    dienthoai = $(frm).find("input[name=dienthoai]").val(),
                    email     = $(frm).find("input[name=email]").val(),
                    idsp      = $(frm).find("input[name=idsp]").val(),
                    ghichu    = $(frm).find("textarea[name=ghichu]").val();

                $.ajax({
                    cache: false,
                    type: "GET",
                    url: _link + "ajax/?op=giohang",
                    data: {
                        id: idsp,
                        method: "add",
                        sl: 1,
                        hoten: hoten,
                        dienthoai: dienthoai,
                        email: email,
                        ghichu: ghichu
                    },
                    success: function (response) {

                        thjs.unbind("submit").submit();

                    }
                });
            }
        }
    });
});