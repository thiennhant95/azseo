if ( $.isFunction($.fn.prettyPhoto) ) {
    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: "",
        default_width: 800,
        default_height: 800,
        allow_resize: true,
        theme: "facebook",
        horizontal_padding: 20,
        autoplay_slideshow: true
    });
}