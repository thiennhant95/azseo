{strip}
{if $owl!=''}
<div class="{$owl} owl-carousel owl-theme">
    {/if}
    {if count($arrGallery) }

    {foreach from=$arrGallery item=igallery key=kgallery}
    <div class="gallery-item clearfix" >
        <a href="{$root_path}{$igallery.url}" title="{$igallery.ten}">
            <div class="image">
                {* <span class="glass"></span> *}
                <img src="{$root_path}uploads/noidung/thumb/{$igallery.hinh}" class="img-responsive" alt="{$igallery.ten}" title="{$igallery.ten}" />
            </div>
        </a>

    </div>
    {/foreach}

    {/if}
    {if $owl!=''}
</div>
{/if}
{$phantrang}
{/strip}