{strip}
{$braucube}
<h1 class="product-title-big">{$ten}</h1>
<div class="gallery-container clearfix">
    <div class="gallery-content clearfix">
        {foreach from=$gall item=igall key=kgall}
            <div class="gallery-item clearfix">
                <a href="{$root_path}uploads/noidung/{$igall.hinh}" rel="prettyPhoto['gallery']" title="Gallery">
                    <div class="image">
                        {* <span class="glass"></span> *}
                        <img src="{$root_path}uploads/noidung/{$igall.hinh}" class="img-responsive" alt="Gallery" title="Gallery" />
                    </div>
                    <!-- .image -->
                </a>

            </div>
            <!-- .gallery-item clearfix -->
        {/foreach}
    </div>
    <!-- .gallery-content clearfix -->
</div>
<!-- .gallery-container clearfix -->
{/strip}