{strip}
<div class="ws-container clearfix modal-content">
    <div class="closepopup" data-dismiss="modal"></div>

{* TIÊU ĐỀ *}
        <div class="panel-heading">
            <h3 class="h2 form-group clearfix text-center">
                {'thongtintaikhoan'|arraybien}
            </h3>
        </div>
        <!-- ./panel-heading -->


        <div class="panel-body" id="frmUEdit">
            <div id="wsnotify"></div>

{* EMAIL *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">Email:</label>
                <div class="col-sm-9">
                    <div class="form-control" disabled>{$smarty.session.user_email}</div>
                    <!-- .form-control -->
                </div>
            </div>
            <!--  -->

{* SO DU *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">{'sodiem'|arraybien}:</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <div class="form-control" disabled>
                        {$smarty.session.user_sodu}</div>
                        <span class="input-group-addon">
                            {'diem'|arraybien}
                        </span><!-- /.input-group-addon -->
                    </div><!-- /.input-group -->
                    <!-- .form-control -->
                </div>
            </div>
            <!--  -->

{* HO TEN DEM *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">{'hotendem'|arraybien}:</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="hotendem" data-value="{$smarty.session.user_hotendem}" value="{$smarty.session.user_hotendem}" />
                </div>
            </div>
            <!--  -->

{* TEN *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">{'ten'|arraybien}:</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="ten" data-value="{$smarty.session.user_ten}" value="{$smarty.session.user_ten}" />
                </div>
            </div>
            <!--  -->

{* DIA CHI *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">{'diachi'|arraybien}:</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="diachi" data-value="{$smarty.session.user_diachi}" value="{$smarty.session.user_diachi}" />
                </div>
            </div>
            <!--  -->

{* DIEN THOAI *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">{'dienthoai'|arraybien}:</label>
                    <div class="col-sm-9">
                <input class="form-control" data-wserror-number="Vui lòng nhập kiểu số" type="text" name="sodienthoai" onkeyup="CheckNumber(this)" data-value="{$smarty.session.user_sodienthoai}" value="{$smarty.session.user_sodienthoai}" />
                </div>
            </div>
            <!--  -->

{* MA GIOI THIEU *}
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">
                    {'linkgioithieu'|arraybien}:</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            {if $smarty.session.user_magioithieu}
                                <input class="form-control" type="text" name="magioithieu" id="magioithieu" value="{$root_path}?magioithieu={$smarty.session.user_magioithieu}" readonly />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" onclick="copy_to_clipboard('#magioithieu')">COPY</button>
                                </span><!-- /.input-group-btn -->
                            {else}
                                <input class="form-control" type="text" name="magioithieu" id="magioithieu" readonly />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" onclick="getmagioithieu('#magioithieu')">GET</button>
                                </span><!-- /.input-group-btn -->
                            {/if}
                        </div><!-- /.input-group -->
                        <div class="form-group"></div><!-- /.form-group -->
                        <a href="{$root_path}moi-ban-be.user" class="btn btn-default">
                            {'danhsachthanhvien'|arraybien}
                        </a>
                    </div>
            </div>
            <!--  -->

{* HINH DAI DIEN *}
			<div class="hinhdaidien form-group clearfix">

				<label for="hinhdaidien" class="col-sm-3 control-label">
					Hình đại diện:
				</label> <!-- /.col-sm-3 control-label -->

				<div class="col-sm-9">
					<form id="hinhdaidien_frm" class="panel panel-default panel-body clearfix" action="" method="post" enctype="multipart/form-data">

                        {$avatar_file = $smarty.session.ROOT_PAT|cat:"uploads/user/"|cat:$smarty.session.user_hinhdaidien}

						<div class="hinhdaidien-preview form-group">

                            {if $smarty.session.user_hinhdaidien}
                                <img src="{$avatar_file}" width="100%" height="100%" id="image-preview" class="img-responsive" />
                            {else}
                                <img src="" width="100%" height="100%" id="image-preview" class="img-responsive" />
                            {/if}

							<input type="hidden" name="tenhinhdaidien" value="{$smarty.session.user_hinhdaidien}" />
						</div>
						<!-- /.hinhdaidien-preview form-group -->
						<div class="message success"></div>
						<!-- /.message success -->
						<div class="loading">Loading...</div>
						<!-- /.loading -->
						<div class="hinhdaidien-upload clearfix">
							<div class="form-group">
								<label>chọn hình đại diện</label>
								<input type="file" name="filedaidien" id="filedaidien" class="filedaidien form-control" />
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<input type="hidden" name="action" value="upload" />
                                <input type="hidden" name="method" value="uploadhinhdaidien" />
								<input type="submit" value="Upload" class="btn btn-primary" id="submit" />
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.hinhdaidien-upload -->

					</form>
				</div>
				<!-- /.col-sm-6 -->

			</div>
			<!-- /.hinhdaidien form-group clearfix -->

{* NÚT LƯU *}
            <div class="form-group clearfix">
                <div class="col-sm-12">
                    <button type="button" id="UserUpdate" class="btnlogin">
                        <i class="fa fa-save fa-lg"></i>
                        {'luu'|arraybien}
                    </button>
                </div>
            </div>

        </div>
        <!-- ./panel-body -->

</div>
{/strip}