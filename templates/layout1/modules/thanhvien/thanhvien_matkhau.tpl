{strip}
<div class="ws-container clearfix modal-content">
    <div class="closepopup" data-dismiss="modal"></div>

    <div class="panel-heading">
        <h3 class="h2 form-group clearfix text-center">
            {'doimatkhau'|arraybien}
        </h3>
    </div>
    <!-- ./panel-heading -->


    <div class="panel-body">
        <div class="row">

            <div class="col-sm-8 col-sm-offset-2" id="frmChangePass">

                <div id="wsnotify"></div>

                <div class="form-group clearfix">
                   <div class="form-control" disabled>{$smarty.session.user_email}</div>
                        <!-- .form-control -->
                </div>
                <!--  -->

                <div class="form-group clearfix">
                    <input class="form-control ws-required" name="passcurrent" type="password" value="" placeholder="{'matkhauhientai'|arraybien}" />
                </div>

                <div class="form-group clearfix">
                    <input class="form-control ws-required" name="passnew" type="password" value="" placeholder="{'matkhaumoi'|arraybien}" />
                </div>

                <div class="form-group clearfix">
                    <input class="form-control ws-required" name="passrenew" type="password" value="" placeholder="{'nhaplaimatkhaumoi'|arraybien}" />
                </div>

                <div class="form-group clearfix">
                    <div class="col-sm-9">
                        <button type="button" id="changepassid" class="btnlogin" data-email="{$smarty.session.user_email}">
                            <i class="fa fa-save fa-lg"></i>&nbsp;
                            {'luu'|arraybien}
                        </button>
                    </div>
                </div>
            </div>
            <!-- .col-sm-8 col-sm-offset-2 -->

        </div>
        <!-- .row -->
    </div>
    <!-- ./panel-body -->



</div>
{/strip}