<?php
if (session_id() == '') session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
$chenjs = '';
$pathjs     = '';
$pathscript = '';
$pathplugin = '';
#-----------------------------------------------------
# DETECT FILE IS ( INCLUDE OR DIRECT )
#-----------------------------------------------------

$pathjs = dirname(__FILE__).'/js/';
$pathscript = dirname(__FILE__).'/../';
$pathplugin = dirname(__FILE__).'/../../';
$pathplugin_replace = $_SESSION['ROOT_PATH'].'templates/images/';
$pathfile = dirname(__FILE__).'/';
#---------------------------------------------------
# DETECT LOCALHOST OR HOSTING TO DELETE FILE CACHE
#---------------------------------------------------
// echo $pathplugin;
//   exit('abc');
if ($_SERVER['HTTP_HOST'] == 'a') {
    if (file_exists($pathjs."load.js")) {
        unlink($pathjs."load.cache.js");
    } else {
        echo "Khong tim thay file load.js";
    }
} else {
    if (file_exists($pathjs."load.cache.js")) {
        unlink($pathjs."load.cache.js");
    }
}

$arrScript = array(
    "home",
    "user",
    "comment",
    "contact",
    "content",
    "intro",
    "product",
    "project",
    "question",
    "search",
    "service",
    "gallery"
);

#-----------------------------------------------------
# GET CONTENT FILE
#-----------------------------------------------------
$chenjs .= file_get_contents($pathscript."javascript/jquery-1.12.4.min.js");
$chenjs .= file_get_contents($pathscript."javascript/jquery-migrate-1.4.1.min.js");
$chenjs .= file_get_contents($pathscript."javascript/bootstrap.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/jRating/jquery/jRating.jquery.js");
$chenjs .= file_get_contents($pathplugin."plugins/OwlCarousel2/owl.carousel.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/sweet-alert/sweetalert.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/jquery-news-ticker/includes/jquery.ticker.js");
$chenjs .= file_get_contents($pathplugin."plugins/elevatezoom/jquery.elevateZoom-3.0.8.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/elevatezoom/jquery.fancybox.pack.js");
$chenjs .= file_get_contents($pathplugin."plugins/jquery.easy-ticker/jquery.easy-ticker.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/jquery.easy-ticker/jquery.easy-ticker.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/ws-menudropdown/ws-menudropdown.js");
$chenjs .= file_get_contents($pathplugin."plugins/lazyload/jquery.lazyload.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/wow/wow.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/scrollreveal/scrollreveal.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/matchheight/jquery.matchHeight-min.js");
$chenjs .= file_get_contents($pathplugin."plugins/respondjs/script.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/ws-form-error/ws-form-error.js");
$chenjs .= file_get_contents($pathplugin."plugins/cubeslider/js/cubeslider-min.js");
$chenjs .= file_get_contents($pathplugin."plugins/rating/starrr.js");
$chenjs .= file_get_contents($pathplugin."plugins/select2/dist/js/select2.full.min.js");
$chenjs .= file_get_contents($pathplugin."plugins/select2/dist/js/i18n/vi.js");

$chenjs.= file_get_contents($pathplugin."plugins/data-table/jquery.dataTables.min.js");

/**====================================
@ prettyPhoto*/
$chenjs .= file_get_contents($pathplugin."plugins/prettyphoto/js/jquery.prettyPhoto.js");

$chenjs .= file_get_contents($pathplugin."plugins/jquery.countdown/jquery.countdown.min.js");

// jQuery FORM
$chenjs.= file_get_contents($pathplugin."plugins/jquery.form/jquery.form.min.js");

$chenjs .= file_get_contents($pathjs."load.js");

$jsModules = '';
foreach ($arrScript as $kjs => $vjs) {
    if (file_exists($pathfile."modules/".$vjs."/js/js.js")) {
        $jsModules .= file_get_contents($pathfile."modules/".$vjs."/js/js.js");
    }
}
$chenjs  = str_replace('{$loadReady}',$jsModules,$chenjs);
$chenjs  = str_replace('{$root_path}',$pathplugin_replace,$chenjs);
$chenjs  = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $chenjs);
#-----------------------------------------------------------------
# CREATE FILE CACHE JS
#-----------------------------------------------------------------
if (!file_exists($pathjs."load.cache.js")) {
    $fw = fopen($pathjs."load.cache.js", "w");
    if ($fw) {
        fwrite($fw, $chenjs);
        fclose($fw);
    }
}
if (isset($_GET['url']) && $_GET['url'] == 1) {
    echo "<script>location.href='".$_SESSION['ROOT_PATH']."';</script>";
}
 ?>