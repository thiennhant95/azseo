<?php

$inline_css = null;

$arrCss = array(
    "top.css",
    "search.css",
    // "comment.css",
    "banner.css",
    "menu.css",
    "slideshow.css",
    "bottom.css",
    "designby.css",
    "col.css",
    // "itemsupport.css",
    "ngonngu.css",
    // "popup.css",
    "option.css",
    "doitac.css",
    "share.css",
    "bottomfix.css",
    // "ykienkhachhang.css",
    // "jrating.css",
    "cart.css"
);

$allow_all_css = array(
    "user"
);

    /**=========================================
    @ SET BACKGROUND CHO TOP*/
    if( !empty($_getArrayconfig['bgtop']) ) :
        if ( substr($_getArrayconfig['bgtop'],0,4) == 'rgba' ) {
            $ws_colorTop = $_getArrayconfig['bgtop'];
        } else {
            $ws_colorTop = '#'.$_getArrayconfig['bgtop'];
        }
    else:
        $ws_colorTop = null;
    endif;

    /**========================================
    @ SET BACKGROUND CHO MENU*/
    if( !empty($_getArrayconfig['bgmenu']) ) :
        if ( substr($_getArrayconfig['bgmenu'],0,4) == 'rgba' ) {
            $ws_colorMenu = $_getArrayconfig['bgmenu'];
        } else {
            $ws_colorMenu = '#'.$_getArrayconfig['bgmenu'];
        }
    else:
        $ws_colorMenu = null;
    endif;

    /**===============================================
    @ SET BACKGROUND CHO MAIN*/
    if( !empty($_getArrayconfig['bgmain']) ) :
        if ( substr($_getArrayconfig['bgmain'],0,4) == 'rgba' ) {
            $ws_colorMain = $_getArrayconfig['bgmain'];
        } else {
            $ws_colorMain = '#'.$_getArrayconfig['bgmain'];
        }
    else:
        $ws_colorMain = '#333333';
    endif;

    /**====================================================
    @ SET BACKGROUND CHO BOTTOM*/
    if( !empty($_getArrayconfig['bgbottom']) ) :
        if ( substr($_getArrayconfig['bgbottom'],0,4) == 'rgba' ) {
            $ws_colorBottom = $_getArrayconfig['bgbottom'];
        } else {
            $ws_colorBottom = '#'.$_getArrayconfig['bgbottom'];
        }
    else:
        $ws_colorBottom = null;
    endif;

    /**===================================================
    @ SET BACKGROUND CHO DESIGNBY*/
    if( !empty($_getArrayconfig['bgdesign']) ) :
        if ( substr($_getArrayconfig['bgdesign'],0,4) == 'rgba' ) {
            $ws_colorDesign = $_getArrayconfig['bgdesign'];
        } else {
            $ws_colorDesign = '#'.$_getArrayconfig['bgdesign'];
        }
    else:
        $ws_colorDesign = null;
    endif;

    /**=============================================================
    @ SET BACKGROUND CHO Title*/
    if( !empty($_getArrayconfig['bgtitle']) ) :
        if ( substr($_getArrayconfig['bgtitle'],0,4) == 'rgba' ) {
            $ws_colorTitle = $_getArrayconfig['bgtitle'];
        } else {
            $ws_colorTitle = '#'.$_getArrayconfig['bgtitle'];
        }
    else:
        $ws_colorTitle = null;
    endif;

    /**=============================================================
    @ SET BACKGROUND CHO HOVER*/
    if( !empty($_getArrayconfig['bghover']) ) :
        if ( substr($_getArrayconfig['bghover'],0,4) == 'rgba' ) {
            $ws_colorHover = $_getArrayconfig['bghover'];
        } else {
            $ws_colorHover = '#'.$_getArrayconfig['bghover'];
        }
    else:
        $ws_colorHover = null;
    endif;

    $inline_css.= file_get_contents($_SESSION['LAYOUT_PATH']."css/css.css");
    $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/video/css/item.css");
    $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/content/css/item.css");
    $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/service/css/item.css");

    // Lặp qua các file được khai báo trong $arrCss
    foreach ($arrCss as $kcss => $vcss) {
        $f0 = $_SESSION['LAYOUT_PATH']."css/".$vcss;

        if (file_exists($f0)) {
            $inline_css.= file_get_contents($f0);
        }
    }

    # Trường hợp không ở trang chủ
    if (!empty($_op) || $_op != 'home') {
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/nothome.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/breadcrumb.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/tag.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/itemsupport.css");
        // $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/col.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/product/css/itemcategories.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/content/css/item_2cot.css");
        $inline_css.= file_get_contents($_SESSION['LAYOUT_PATH']."modules/dathang/css/style.css");
    }

    # Trường hợp ở trang chủ
    if ($_op == 'home') {
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/product/css/item.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/intro/css/item.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."modules/picture/css/item.css");
    }

    // Đưa css sản phẩm vào tìm kiếm
    if ($_op == 'search') {
        $inline_css.= file_get_contents($_SESSION['LAYOUT_PATH']."modules/product/css/item.css");
    }

    // Đưa css vào sản phẩm vs trang chủ
    if ($_op == 'content') {
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/comment.css");
    }
    if ($_op == 'product' || $_op == 'home') {
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/effect-box-shadown.css");
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/comment.css");
    }

    // Lấy css cho phép truy cập mọi nơi
    if( count($allow_all_css) > 0 ){
        foreach( $allow_all_css as $icss ){
            $f1 = $_SESSION['LAYOUT_PATH']."modules/{$icss}/css/item.css";
            $f2 = $_SESSION['LAYOUT_PATH']."modules/{$icss}/css/style.css";

            if( file_exists( $f1 ) ){
                $inline_css.= file_get_contents($f1);
            }

            if( file_exists( $f2 ) ){
                $inline_css.= file_get_contents($f2);
            }

        }
    }

    # LẤY CSS THEO MODULE
    $op_item = $_SESSION['LAYOUT_PATH']."modules/{$_op}/css/item.css";
    $op_style = $_SESSION['LAYOUT_PATH']."modules/{$_op}/css/style.css";

    if ( file_exists($op_item) && ! empty($_op) ) {
        $inline_css .= file_get_contents($op_item);
    }
    if ( file_exists($op_style) && ! empty($_op) ) {
        $inline_css .= file_get_contents($op_style);
    }

    if ($didong->isMobile()) {
        $inline_css .= file_get_contents($_SESSION['LAYOUT_PATH']."css/responsive.css");
    }

    $inline_css  = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $inline_css);
    $inline_css  = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $inline_css);
    $inline_css  = str_replace('../fonts/', $_SESSION['ROOT_PATH'].$_SESSION['LAYOUT_PATH'].'fonts/', $inline_css);
    $inline_css  = str_replace('../images/', $_SESSION['ROOT_PATH'].$_SESSION['LAYOUT_PATH'].'images/', $inline_css);
    $inline_css  = str_replace('[ws_colorTop]', $ws_colorTop, $inline_css);
    $inline_css  = str_replace('[ws_colorMain]', $ws_colorMain, $inline_css);
    $inline_css  = str_replace('[ws_colorMenu]', $ws_colorMenu, $inline_css);
    $inline_css  = str_replace('[ws_colorBottom]', $ws_colorBottom, $inline_css);
    $inline_css  = str_replace('[ws_colorDesign]', $ws_colorDesign, $inline_css);
    $inline_css  = str_replace('[ws_colorTitle]', $ws_colorTitle, $inline_css);
    $inline_css  = str_replace('[ws_colorHover]', $ws_colorHover, $inline_css);
    $inline_css  = str_replace('{$layout_path}', $_SESSION['ROOT_PATH'].$_SESSION['LAYOUT_PATH'], $inline_css);
    $inline_css  = str_replace('{$root_path}', $_SESSION['ROOT_PATH'], $inline_css);
 ?>