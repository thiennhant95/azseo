{strip}
<div class="content_main">
    {if ($checkhome) == 'nothome' && FALSE}
        {if !empty($catTimeline)}
            {$imgTimeline = $catTimeline}
        {else}
            {$imgTimeline = "timeline-default.png"}
        {/if}
        <div class="cat-timeline clearfix" style="background-image: url('{$root_path}uploads/danhmuc/{$imgTimeline}')">
            <div class="cat-timeline2">
                <div class="cat-timeline-content ws-container clearfix">
                    <div class="product-title-big-wrap clearfix">
                        <div class="product-title-big-row">
                            <h1 class="product-title-big">{$ten}</h1>
                            {$flow}
                        </div>
                    </div>
                    {$braucube}
                </div>
            </div>
        </div>
    {/if}
    <div class="content_content clearfix">
		<div id="colcontent">
		    {if isset( $smarty.get.act ) }
                {include file="../modules/{$op}/tpl/{$smarty.get.act}.tpl"}
            {else}
                {include file="../modules/{$op}/tpl/{$tplname}.tpl"}
            {/if}
		</div>
		<div id="col1">
		    {include file="./colA.tpl" class=''}
		</div>
    </div>
</div>
{/strip}