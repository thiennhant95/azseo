{strip}
<div class="content_main">
	{if $op=='home'}
		<div class="content_content clearfix {$checkhome}">
	{else if $op!='home'}
		<div class="content_content ws-container clearfix {$checkhome}">
	{/if}
        {if isset( $smarty.get.act ) }
            {include file="../modules/{$op}/tpl/{$smarty.get.act}.tpl"}
        {else}
            {include file="../modules/{$op}/tpl/{$tplname}.tpl"}
        {/if}
    </div>
</div>
{/strip}