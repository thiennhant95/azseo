{strip}
<!doctype html>
<html lang="{$keylang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{$title}</title>
    <link rel="canonical" href="{$url}">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <meta name="robots" content="{$robots}" />
    <meta name="title" content="{$title}" />
    <meta name="description" content="{$description}" />
    <meta name="keywords" content="{$keywords}" />
    <meta name="author" content="{$author}"/>
    <meta property="og:type" content="{$ogtype}"/>
    <meta property="og:title" content="{$title}"/>
    <meta property="og:url" content="{$url}" />
    <meta property="og:description" content="{$description}" />
    <meta property="og:image" content="{$images}" />
    <meta http-equiv="content-language" content="{$language}" />
    <meta name="DC.title" content="{$title}" />
    <meta name="geo.region" content="VN-SG" />
    <meta name="geo.position" content="{$geoposition}" />
    <meta name="ICBM" content="{$geoposition}" />
{if !empty($appfacebookid)}
    <meta name="fb:app_id" content="{$appfacebookid}" />
{/if}
{if !empty($mastertool)}
    <meta name="google-site-verification" content="{$mastertool}" />
{/if}
<link href="{$root_path}feed" rel="alternate" title="Dòng thông tin {$author}" type="application/rss+xml"/>
{if $op !='home'}
<link href="{$url}feed" rel="alternate" title="Nguồn cấp {$author} » {$title}" type="application/rss+xml"/>
{/if}
{* KIỂM TRA XEM favicon đang nằm ở đâu ? *}
{if file_exists("uploads/config/"|cat:$favicon)}
    <link rel="icon" href="{$root_path}uploads/config/{$favicon}" type="image/x-icon" />
{else}
    <link rel="icon" href="{$root_path}uploads/config/lib_favicon/{$favicon}" type="image/x-icon" />
{/if}
<script>(function(w) { var loadCSS = function( href, before, media ){ var doc = w.document, ss = doc.createElement( "link" ), ref;if( before ){ ref = before; } else { var refs = ( doc.body || doc.getElementsByTagName( "head" )[ 0 ] ).childNodes; ref = refs[ refs.length - 1]; } var sheets = doc.styleSheets; ss.rel = "stylesheet"; ss.href = href; ss.media = "only x"; function ready( cb ){ if( doc.body ){ return cb(); } setTimeout(function(){ ready( cb ); }); } ready( function(){ ref.parentNode.insertBefore( ss, ( before ? ref : ref.nextSibling ) ); }); var onloadcssdefined = function( cb ){ var resolvedHref = ss.href; var i = sheets.length; while( i-- ){ if( sheets[ i ].href === resolvedHref ){ return cb(); } } setTimeout(function() { onloadcssdefined( cb ); }); };  function loadCB(){ if( ss.addEventListener ){ ss.removeEventListener( "load", loadCB ); } ss.media = media || "all"; } if( ss.addEventListener ){ ss.addEventListener( "load", loadCB);  } ss.onloadcssdefined = onloadcssdefined; onloadcssdefined( loadCB ); return ss; }; if( typeof exports !== "undefined" ){ exports.loadCSS = loadCSS; } else { w.loadCSS = loadCSS; } }( typeof global !== "undefined" ? global : this ));</script>
<script id="loadcss"> loadCSS( "{$root_path}{$layout_path}css/css.cache.css", document.getElementById("loadcss") );</script>
<style rel="stylesheet"> {$inline_css} </style>
    <noscript> <link rel="stylesheet" href="{$root_path}{$layout_path}css/css.cache.css" /> </noscript>
{if !empty($manhungdau)} {$manhungdau} {/if}
{if isset($bgWebsite)} {$bgWebsite} {/if}
</head>
<body class="{$op}">
{/strip}