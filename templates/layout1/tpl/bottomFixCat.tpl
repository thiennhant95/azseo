{strip}
<div class="catpContainer form-group clearfix">
    <div class="catpContent clearfix">
        {* <div class="homeinfo">{$homeinfo}</div> *}
        {foreach from=$dataBottomFixCat.catp item=itemcatp key=keycatp}

            {$catpLink = "javascript:;"}
            {if !empty($itemcatp.url)}
                {$catpLink = $root_path|cat:$itemcatp.url|cat:'/'}
            {/if}
            {if !empty($itemcatp.link)}
                {$catpLink = $itemcatp.link}
            {/if}
             {if !empty($itemcatp.img)}
                {$catbg = 'style="background-image: url('|cat:$root_path|cat:'uploads/danhmuc/'|cat:$itemcatp.img|cat:');"'}
            {else}
                {$catbg = ''}
            {/if}
            {$icon = ''}
            {if !empty($itemcatp.icon)}
                {$iconfile = $itemcatp.icon}
                {if substr($iconfile,0,2)=='fa'}
                    {$icon = '<span class="img"><i class="fa '|cat:$itemcatp.icon|cat:'"></i></span>'}
                {else}
                    {$icon = '<span class="img"><img src="'|cat:$root_path|cat:'uploads/danhmuc/'|cat:$itemcatp.icon|cat:'" alt="'|cat:$itemcatp.ten|cat:'" /></span>'}
                {/if}
            {/if}
            {$catpOP = ''}
            {$catpOP = $models->getNameFromID("tbl_danhmuc_type", "op", "id", {$itemcatp.idtype})}
            <div class="catpItem cat{$catpOP} clearfix" {$catbg}>
                <div class="ws-container {$catpOP}-container">

                    <div class="titlebase-home clearfix wow animated fadeInDown" data-wow-delay="0s">
                        {* {$icon} *}
                        <h2>
                        {* <div class="subtitle">*}
                        <a href="{$catpLink}" title="{$itemcatp.ten}">
                            {$itemcatp.ten}
                            {* <div class="line"></div> *}
                        </a>
                        {* </div> *}
                        </h2>
                        {* <div class="xemtatca"><a href="{$catpLink}" title="{$smarty.session.xemtatca}">{$smarty.session.arraybien.xemtatca}</a></div> *}
                    </div>
                    <!-- /.titlebase -->

                    <div class="{$catpOP}Box  clearfix">
                        {if $dataBottomFixCat[$keycatp].categories}
                            {include file="../modules/product/tpl/itemCategories.tpl" arrsanpham=$dataBottomFixCat[$keycatp].categories  owl='owl-carousel '|cat:$catpOP|cat:'Run'}
                        {elseif $catpOP == 'product'}
                            {include file="../modules/product/tpl/item.tpl" arrsanpham=$dataBottomFixCat[$keycatp].noidung owl='owl-carousel '|cat:$catpOP|cat:'Run'}
                        {elseif $catpOP == 'video'}
                            {include file="../modules/video/tpl/item.tpl" arrsanpham=$dataBottomFixCat[$keycatp].noidung owl='owl-carousel '|cat:$catpOP|cat:'Run itemvideohome'}
                        {else if $catpOP == 'service'}
                            {include file="../modules/service/tpl/item.tpl" arrservice=$dataBottomFixCat[$keycatp].noidung owl='owl-carousel '|cat:$catpOP|cat:'Run'}
                        {else if $catpOP == 'project'}
                            {include file="../modules/project/tpl/item.tpl" arrnoidung=$dataBottomFixCat[$keycatp].noidung owl='owl-carousel '|cat:$catpOP|cat:'Run'}
                        {else if $catpOP == 'intro'}
                            <div class="motagioithieu wow fadeInUp animated">
                                {$dataBottomFixCat.$keycatp.gioithieu.0.mota}
                            </div>
                        {else}
                            {include file="../modules/content/tpl/item.tpl" arrnoidung=$dataBottomFixCat[$keycatp].noidung owl='noidungRun'}
                        {/if}

                    </div>
                {* <div class="tag_bottomfix">{$tagcontent}</div> *}
                </div>
            </div>

        {/foreach}
    </div>
    <!-- ./catpContent -->
</div>
<!-- ./catpContainer -->
{/strip}