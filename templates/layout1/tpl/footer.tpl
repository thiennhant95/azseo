{strip}
<footer>
    <div id="notification"></div>
    <div id="ws-wating" class="ws-wating clearfix">
        <div class="ws-wating-content">
            <div class="ws-wating-text">
                <p>Loading...</p>
            </div>
            <div class="ws-wating-load"></div>
        </div>
        <!-- ./ws-wating-content -->
    </div>

    {if !empty($eStopWeb) }
        {$eStopWeb}
    {/if}

    {if !empty($eViewfor) }
        {$eViewfor}
    {/if}
    {if $backtotop == 'on'}
        <a href="#" class="go-top">
            <span>
                <i class="fa fa-angle-up"></i>
            </span>
            {* <div class="tool-tip">
                Back To Top
            </div> *}
        </a>
    {/if}
    {* {$clickToCall} *}
    {if ! empty( $menu_right)}
        <ul class="clearfix menu-bottom">
            <li>
                <a href="tel:{$menu_right.hotline}" title="clickToCall">
                    <i class="fa fa-phone"></i> {$smarty.session.arraybien.goidien}
                </a>
            </li>
            <li>
                <a href="sms:{$menu_right.hotline}" title="Click to SMS">
                    <i class="fa fa-comments-o"></i> {$smarty.session.arraybien.sms}
                </a>
            </li>
            <li>
                <a href="{$menu_right.lienhe}" title="location">
                    <i class="fa fa-map-marker"></i> {$smarty.session.arraybien.chiduong}
                </a>
            </li>
        </ul>
    {/if}

    <div id="rootPath" data-url="{$root_path}"></div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="loaddatacart"></div>
    </div>
    {if !empty($analytics)}{$analytics}{/if}
    {if !empty($manhungcuoi)}{$manhungcuoi}{/if}
    <script src="{$root_path}{$layout_path}js/load.cache.js" async></script>
</footer>

</body>

</html>

{/strip}