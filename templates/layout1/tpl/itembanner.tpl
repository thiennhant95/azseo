{if !empty($class)} <div class="{$class}"> {/if}
{strip}
{if count($arrBNQC) > 0}
    <ul class="banner_quangcao">
        {foreach from=$arrBNQC item=itemBNQC key=keyBNQC}
            {$bannerName = $itemBNQC.ten}
            {$bannerImg = $itemBNQC.hinh}
            {$bannerLinkImg = $itemBNQC.linkhinh}
            {$bannerEmbed = $itemBNQC.manhung}
            {$bannerLink = $itemBNQC.link}
            {$bannerTarget = $itemBNQC.target}
            {$bannerWidth = $itemBNQC.rong}
            {$bannerHeight = $itemBNQC.cao}



            {if $bannerWidth > 0}
                {$bannerWidth = $bannerWidth|cat:'px'}
            {/if}
            {if $bannerHeight > 0}
                {$bannerHeight = $bannerHeight|cat:'px'}
            {/if}

            {$extFile = $bannerImg|pathinfo:$smarty.const.PATHINFO_EXTENSION}
            {$bannerPath = $root_path|cat:'uploads/logo/'}
            {if !empty($bannerLinkImg)}
                {$bannerImg = $bannerLinkImg}
            {/if}
            {$bannerTarget = ''}
            {if !empty($bannerTarget)}
                {$bannerTarget = 'target="'|cat:$bannerTarget|cat:'"'}
            {/if}

            {if !empty($bannerWidth)}
                {$bannerWidth = "width="|cat:$bannerWidth}
            {/if}
            {if !empty($bannerHeight)}
                {$bannerHeight = "height="|cat:$bannerHeight}
            {/if}
            {if empty($bannerLink)}
                {$bannerLink = "javascript:;"}
            {/if}



            {if !empty($bannerEmbed)}
                <li>{$bannerEmbed}</li>
            {else}
                {if $extFile == 'swf'}
                    <li><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" {$bannerWidth} {$bannerHeight}><param name="movie" value="{$bannerPath|cat:$bannerImg}" /><param name="quality" value="high" /><param name="WMode" value="Transparent"/><embed wmode="transparent" src="{$bannerImg}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" {$bannerWidth} {$bannerHeight}></object></li>
                {else}
                    <li>
                        <a href="{$bannerLink}" title="{$bannerName}" {$bannerTarget}>
                            <img src="{$bannerPath|cat:$bannerImg}" {$bannerWidth} {$bannerHeight} onerror="xulyloi(this);" alt="{$bannerName}" class="img-responsive" />
                        </a>
                    </li>
                {/if}
            {/if}
        {/foreach}
    </ul>
{/if}
{if !empty($class)} </div> {/if}
{strip}