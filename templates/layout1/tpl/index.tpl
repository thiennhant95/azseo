{include file="{$layout_path}tpl/header.tpl"}
{$popuptop}
{strip}
<section id="wapper_wapper" class="bg-df">
    <div id="wrap_noidung" class="{$checkhome}">
        {* TOP *}
        <div id="wapper_top" class="clearfix">
            <div class="top_content ws-container clearfix">
                {$top}
            </div>
        </div>
        {* BANNER *}
        <div id="wapper_banner" class="banner-wrap clearfix" {$bgbanner}>
            <div class="banner_content clearfix">
                {$banner}
                {* <nav class="menu_content ws-container clearfix" {$bgmenu}>
                    {$menu}
                </nav> *}
            </div>
        </div>
        {* MENU *}
        {* <header id="wapper_menu" class="clearfix" >
            <nav class="menu_content ws-container clearfix" {$bgmenu}>
                {$menu}
            </nav>
        </header> *}
        {* SLIDESHOW *}
        {if $op == 'home'}
        <div id="wapper_slide" class="clearfix">
            {* <div class="ws-container"> *}
                {* <div class="slide_content clearfix"> *}
                    <div class="slide-item sl">{$slide}</div>
                {* </div> *}
{*                 <div class="qc_content">
                    <div class="first-qc"></div>
                    <div class="second-qc"></div>
                </div> *}
            {* </div> *}
        </div>
        {/if}
        {include file="{$layout_path}modules/home/tpl/danhmucSp.tpl"}

        {* {if $op == 'home'}
            <div class="modulesIntro">
                <div class="ws-container">
                    {$modulesIntro}
                </div>
            </div>
        {/if} *}


        {* {$thongbaogiuatrang} *}
        {* MAIN *}
        <div id="wapper_main" class="clearfix {$op}">
            <div class="ws-container">
                {include file="{$layout_path}tpl/{$layout_name}"}
                {* {$frm_order_web} *}
            </div>
        </div>
        {$taisaochonngoaingu}
        {* MENU BOTTOM *}
        {* <div class="wrapper-menu-bott clearfix">
            {$menubottom}
        </div> *}
        {* BOTTOM FIX *}
        {* <div id="wapper_bottom_fix" class="clearfix bottomfix" {$bottomfixbg}>
            <div class="bottom_content clearfix wow reveal fadeIn ws-container">
                {$bottomfix}
            </div>
        </div> *}


        {* Ý KIẾN KHÁCH HÀNG *}
        {* {$rowykienkhachhang} *}
        {* CHECK COLVALUE BOTTOMFIX *}
        {* {include file="{$layout_path}tpl/bottomFixCat.tpl"}  *}
        {* ĐỐI TÁC *}
        {if $op == 'home'}
        {$rowdoitac}
        {/if}

        {if $op == 'home'}
        <div class="info-container">
            <div class="ws-container">
                {$onBottom}
            </div>
        </div>
        {/if}


        {* BOTTOM *}
        <div id="wapper_bottom" class="clearfix" {$bottombg}>
            <div class="bottom_content ws-container clearfix wow reveal fadeIn">
                {$bottom}
            </div>
        </div>
        {* DESIGNBY *}
        <div id="wapper_design" class="clearfix design-wrap">
            <div class="ws-container">
                <div class="design_content clearfix">
                    {$designby}
                </div>
            </div>
        </div>
    </div>
</section>
{$supportfix}
{$popuphome}
{/strip}
{include file="{$layout_path}tpl/footer.tpl"}