{* LOOP LAYOUT *}
{strip}
{foreach from=$arrColA.layout item=layout key=k_layout}
    {$background = $layout.background}
    {$bgbox=''}
    {$borderbox=''}
    {$icon = $layout.icon}
    {$iconstr = ''}
    {if !empty($icon)}
        {if substr($icon,0,2)=='fa'}
            {$iconstr = '<span class="img"><i class="fa '|cat:$icon|cat:'"></i></span>'}
        {else}
            {$iconstr = '<span class="img"><img src="'|cat:$root_path|cat:'uploads/layout/'|cat:$icon|cat:'" alt="'|cat:{$layout.titleLayout}|cat:'" /></span>'}
        {/if}
    {/if}
    {if $background !=''}
        {$bgbox = ' style="background-color:#'|cat:$background|cat:'" '}
        {$borderbox = ' style="border:1px dashed  #'|cat:$background|cat:'; border-top:none; border-radius:0px 0px 3px 3px;" '}
    {/if}
    <div class="group_tin clearfix" {$bgbox}>

        {if $layout.hienthitieudechuyenmuc}
            <div class="group_tin_tieude clearfix" {$bgbox}>
                <div class="htitle clearfix">
                    {$iconstr}
                    <span class="tentieude">{$layout.titleLayout}</span>
                </div>
                <!-- ./htitle -->
            </div>
            <!-- ./group_tin_tieude -->
        {/if}


        {if $layout.loai == 0}
            {* TRƯỜNG HỢP CHỌN DANH MUC SAN PHAM *}
            <div class="group_tin_noidung clearfix" {$borderbox}>

                {if $layout.laytin == 1}

                        {if count($layout.arrNoidungTPL) > 0}
                            <ul class="ul_noidung_templates">


                                {* Vòng lặp lấy dữ liệu của NỘI DUNG chính *}
                                {foreach from=$layout.arrNoidungTPL item=itemND key=keyND}

                                    {$noidungTen = $itemND.ten}
                                    {$noidungHinh = $itemND.hinh}
                                    {$noidungLink = $itemND.link}
                                    {$noidungUrl = $itemND.url}
                                    {$noidungTarget = $itemND.target}

                                    {$bein = $layout.arrIdkeyTemplateMau}

                                    {* Vòng lặp qua các idkey của template mẫu *}
                                    {foreach from=$layout.arrIdkeyTemplateMau item=itemIdkeyMau key=keyIdkeyMau}

                                        {if $itemIdkeyMau == 'link'}

                                            {if empty($noidungLink)}
                                                {$noidungLink = $root_path|cat:$noidungUrl}
                                            {/if}
                                            {$arrTemplateContent['{'|cat:$itemIdkeyMau|cat:'}'] = $noidungLink}

                                        {elseif $itemIdkeyMau == 'hinh'}

                                            {if !empty($noidungHinh)}
                                                {$noidungHinh = $root_path|cat:'uploads/noidung/thumb/'|cat:$noidungHinh}
                                            {/if}
                                            {$arrTemplateContent['{'|cat:$itemIdkeyMau|cat:'}'] = $noidungHinh}

                                        {elseif $itemIdkeyMau == 'chitiet'}

                                            {$chitiet = $smarty.session.arraybien.chitiet}
                                            {$arrTemplateContent['{'|cat:$itemIdkeyMau|cat:'}'] = $chitiet}

                                        {else}

                                            {$arrTemplateContent['{'|cat:$itemIdkeyMau|cat:'}'] = $itemND[$itemIdkeyMau]}

                                        {/if}


                                    {/foreach}

                                    {* Hiển thì template và thay thế tương ứng vs các từ khóa *}
                                    {Load_content_data($layout.noidungTemplateMau, $arrTemplateContent)}

                                {/foreach}

                            </ul>
                        {/if}
                {else}
                    {* DANH MUC *}
                        {* LẤY NỘI DUNG CỦA DANH MỤC NÀY *}
                    {if $layout.danhmuc_hiennoidung == 1}
                        <div class="noidung_danhmuc">
                            {$db->getNameFromID("tbl_danhmuc_lang", "noidung", "iddanhmuc", $layout.idtype|cat:" AND idlang = "|cat:$smarty.session._lang|cat:"")}
                        </div>

                    {else}
                        <div class="danhmuccol clearfix">
                            {$layout.menuCol}
                        </div>
                    {/if} {* END $layout.danhmuc_hiennoidung*}

                {/if} {* END $layout.laytin *}

            </div>
                <!-- ./group_tin_noidung -->

        {elseif $layout.loai == 1}

            {* TRƯỜNG HỢP CHỌN LÀ BANNER *}
            <div class="group_tin_noidung clearfix" {$borderbox}>
                {include file="./itembanner.tpl" arrBNQC=$layout.arrBNQC class=''}
            </div>

        {elseif $layout.loai == 2}

            <div class="group_tin_noidung clearfix" {$borderbox}>
                {$extTienIch = $layout.tienichFile|pathinfo:$smarty.const.PATHINFO_EXTENSION}
                {$pathInc = $layout.tienichFile|replace:$extTienIch:'tpl'}
                {if file_exists($pathInc)}
                    {include file="{$pathInc}" arrtienich=$layout.tienichFileInc class=''}
                {else}
                    {$layout.tienichFileInc}
                {/if}
            </div>

        {/if} {* END $layout.loai *}

    </div>
    <!-- ./group_tin -->


{/foreach}
{/strip}