var _link = $("#rootPath").data("url");
var select2opt = {
    amdLanguageBase: "./i18n/vi.js",
    language: "vi",
    containerCssClass: "form-control-select2"
}
$(function() {
    if( $.isFunction($.fn.DataTable) ){
        $('#lichsuTichdiem').DataTable( {
            "language": {
                "url": "plugins/data-table/Vietnamese.json"
            }
        });
    }
});
$(function() {
    historyPoint = function() {
        $.getJSON(_link + "ajax/", {
            method: 'historyPoint'
        }, function(json) {
            if( json && json.result ) {
                $('#loaddatacart').html(json.result);
                $('#myModal').modal('show');
                if( $.isFunction($.fn.DataTable) ){
                    $('#lichsuTichdiem').DataTable( {
                        "language": {
                            "url": "plugins/data-table/Vietnamese.json"
                        }
                    });
                }
            }
        });
    }
});
$(function() {
    $('input[name=doidiem][type=checkbox]').on('change', function() {
        var $this = $(this),
            $val = $this.prop('checked') ? $this.val() : 0;
        $.post(_link+"ajax/common/payment/", {
            method: 'doidiem',
            value: $val,
            // TOKEN_CART: $("[name='TOKEN_CART']").val()
        }, function ( json ) {
            if( json && json.message ) {
                swal(json.message,'','error');
                setTimeout(function() {
                    location.reload();
                }, 3e3);
            }
            else {
                location.reload();
            }
        }, 'json');
    });
});
window.onload = function() {
    if( jQuery().countdown && $('div[data-countdown]').length ) {
        $('div[data-countdown]').each(function(index, element) {
            var $el = $(element),
                timeout = $el.data('countdown');
            $el
            .countdown(timeout, function(event) {
                $(this).html(
                    event.strftime(`
                        <div class="time-run">
                            <span class="item day">%D ngày</span>
                            <span class="item hour">%H</span>
                            <span class="item minus">%M</span>
                            <span class="item second">%S</span>
                        </div>
                    `)
                );
            })
            .on('finish.countdown', function() {
                $(this).parent('.deal-status').slideUp('slow');
            });
        });
    }
}
$(function(){
    var _link = $("#rootPath").data("url");
    if ( jQuery().select2 ) {
        $('select.select2').select2(select2opt);
    }
    $('.service-list').css('visibility', 'visible').children('.service-loading').hide();
    if( jQuery().DataTable ){
        $('#cftable').DataTable({
            pageLength: 5,
            responsive: true,
            language: {
                url: _link+"plugins/datatable/vietnamese.json"
            },
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "Tất cả"]
            ]
        });
    }
});
$(function() {
    // console.log('test1');
    var typingTimer,
        doneTypingInterVal = 800,
        $inputLiveSearch = $("#searchtxt"),
        $resultLiveSearch = null;
    //on keydown, clear the countdown
    $inputLiveSearch.on('keydown', function () {
        clearTimeout(typingTimer);
    });
    $inputLiveSearch.on('keyup', function() {
        var thjs = $(this),
            val = $.trim(thjs.val());
        clearTimeout(typingTimer);
        typingTimer = setTimeout(hamTimKiem, doneTypingInterVal);
    });
    function hamTimKiem() {
        var
            $thjs = $inputLiveSearch,
            val = $.trim($thjs.val());
        $.post(_link+"ajax/common/search/", {
            value: val,
            method: 'search'
        },
        function (data, textStatus, jqXHR) {
            if( data ) {
                if( data.status != 'success' ) {
                    $thjs.val('');
                    $("#resSearch").html(data.message);
                }
                else {
                    $("#resSearch").html(data.result);
                }
            }
        },
        "json"
        );
    }
    $(window).click(function() {
        $("#resSearch").html('');
    });
    $("#resSearch").click(function(event){
        event.stopPropagation();
    });
});
function alertBootStrap($mess, $type = 'success') {
    return '<div class="alert alert-' + $type + '">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>' + $mess + '</strong></div>';
}
function MuaHang(url, div) {
    $.getJSON(url, {
        divReturn: div
    }, function(data) {
        if (data.error!="success") {
            if (data.error=="quantity_unknown") {
                swal("Số lượng không hợp lệ!", "", "error");
                self.location.href='';
            }
        } else {
            $('#'+div).html(data.result);
            $("#countCart").text(data.quantity);
        }
    });
}
(function($) {
    if( jQuery().lazyload ){
    $("img.lazy").lazyload({
        effect: "fadeIn"
    });
    $.fn._removeClass = function() {
        This = $(this);
        return This.find('li').each(function() {
            $(this).on('click', function() {
                $(this).parent().find('li').removeClass('active');
                $(this).addClass('active');
            })
        });
    }
    }
})(jQuery);
$(window).load(function() {
    $(".btn-dathang").removeAttr('disabled');
    // Hide image on Error
    $("img").each(function() {
        var image = $(this);
        if (image.context.naturalWidth == 0 || image.readyState == 'uninitialized') {
            $(image).unbind("error").hide();
        }
    });
    setTimeout(function() {
        $("#wapper_slide").addClass('opened');
    }, 100);
});
if ( $('body').find('.tab-duan').length ) {
var tabDuan = $('.tab-duan').offset().top,
    $window = $(window);
}
// MENU CO THE TRUOT THEO
$(window).scroll(function() {
    var _top = $(this).scrollTop();
    if ($(document).width() > 768) {
        var _hei = $('#wapper_banner').outerHeight();
        if (_top >= _hei) {
            $('#wapper_menu').addClass('fixed');
        } else {
            $('#wapper_menu').removeClass('fixed');
        }
    } else {
        if (_top >= 170) {
            $('#wapper_menu, .banner-row2').addClass('fixed');
        } else {
            $('#wapper_menu, .banner-row2').removeClass('fixed');
        }
    }
});
$(window).resize(function() {
    if ($(window).width() <= 768) {
        if (!$('#load-colleft').length) {
            $('#wapper_wapper').append('<div id="load-colleft"></div><div id="content-colleft"></div>');
        }
    } else {
        $('.menu_content').find('#btn-right').remove();
        $('#wapper_wapper').find('#load-colleft').remove();
        $('#wapper_wapper').find('#content-colleft').remove();
    }
    $('.owl-carousel-top').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    $('.owl-carousel-top').find('.owl-stage-outer').children().unwrap();
    // mainOwlCarousel();
});
var mouseX;
var mouseY;
$(document).mousemove(function(e) {
    mouseX = e.pageX;
    mouseY = e.pageY;
});
$(document).ready(function() {
    // document.getElementById('defaultOpen').click();
    var _link = $("#rootPath").data("url");
    // SETUP AJAX
    $.ajaxSetup({
        cache: false,
        beforeSend: besendfunc,
        complete: complefunc,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    });
    function besendfunc() {
        if ($("body").find("#ws-wating").length) {
            return $("#ws-wating").fadeIn('fast');
        }
    };
    function complefunc() {
        if ($("body").find("#ws-wating").length) {
            return $("#ws-wating").fadeOut('fast');
        }
    };
    {$loadReady}
    /**
     * Click show/hide cart+hotline
     */
    $(".cart > .repon, .hotline-slide > .repon").on('click', function() {
        var thjs = $(this);
        thjs.hide();
        thjs.parent().children(".content").show();
        return false;
    });
    // LOAD DANH MỤC CON
    // $('.category_tab').click(function(){
    //     var idCate = $(this).find('a').data('id');
    //     // CÂU LỆNH NÀY SẼ ADD CLASS THEMKHUNG VÀO LI NÀO ĐƯỢC BẤM VÀ XÓA THEMKHUNG NHỮNG LI CÒN LẠI
    //     $(this).addClass("themkhung").siblings(this).removeClass("themkhung");
    //     if(idCate){
    //        $.getJSON(_link + 'ajax/?method=danhmuccon',{
    //                 id: idCate
    //         }, function(json, textStatus) {
    //                 $('.productBox').html('');
    //                 if (json.status != 'success') {
    //                     alert('sai');
    //                 }else{
    //                     // ĐỔ KẾT QUẢ VÀO THẺ HTML PHÍA TRƯỚC, VÍ DỤ Ở ĐÂY LÀ .PRODUCTBOX
    //                     $('.productBox').html(json.result);
    //                 }
    //             });
    //     }
    // });
    //Rating
    // Khoi tao rating & set session
    // $('.starrr').starrr({
    //     rating: 'session',
    //     change: function(e, value){
    //         if (+value && value > 0 && value < 6) {
    //             $.getJSON(_link + 'ajax/?method=rating', {
    //                 value: value,
    //                 id: $(this).data('id')
    //             }, function(json, textStatus) {
    //                 if (json.status != 'success') {
    //                 }
    //                 else {
    //                     console.log(json.status);
    //                 }
    //             });
    //         }
    //     }
    // });
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    // Buy Process
    /* $("#buy_process").live('click', function() {
        var thjs = $(this),
            frm = $("#buy_frm");
        if(jQuery().wsFormError){
            frm.wsFormError({
                notification: true,
                callback: function(data){
                    if(data == 0){
                        var aray = JSON.stringify(
                            $("#buy_frm input, #buy_frm select, #buy_frm textarea").serializeObject()
                        );
                        thjs.button('loading');
                        $.ajax({
                            sync: !1,
                            cache: !1,
                            url: _link + 'ajax/common/buynow/',
                            type: 'POST',
                            data: {
                                value: aray
                            },
                            dataType: 'json',
                            success: function(d){
                                var type = 'success';
                                if(d.status == 'error'){
                                    type = 'error';
                                }
                                swal(d.message,'',type);
                                setTimeout(function(){
                                    $("#myModal").modal('hide');
                                }, 1e3);
                                thjs.button('reset');
                            }
                        });
                    }
                }
            })
        }else{
            console.error('wsFormError is not define');
        }
    }); */
    // Active Rating khi ton tai session
    $('.starrr').each(function(index, el) {
        var _valSess = $(el).data('session');
        if (!!_valSess) {
            for (var i = 0; i < 5; i++) {
                if (_valSess > i) {
                    $(el).children('a:eq('+i+')').removeClass('fa-star-o').addClass('fa-star');
                }
            }
        }
    });
    if ( $(window).width() <= 768 ) {
        $("body").on('click', function() {
            $('.repon').parent().children('.content').hide();
            $('.repon').show();
        });
    }
    // Click Login mobile
    if ( $(window).width() <= 768 ) {
        $(".user2 > .fa-user").live('click', function() {
            $('.login-choose,.frmInfoUser').slideDown();
            $(this).removeClass("fa-user").addClass("fa-times");
        });
        $(".user2 > .fa-times").live('click', function() {
            $('.login-choose,.frmInfoUser').slideUp();
            $(this).removeClass('fa-times').addClass('fa-user');
        });
    }
    if ( $(window).width() <= 768 ) {
        $(".searchBtnRepon").on('click', function() {
            var _thjs = $(this);
            if ( $(".searchmobi").hasClass("hide") ) {
                $(".searchmobi").removeClass("hide").css({
                    'display': 'table',
                    'position': 'relative',
                    'zIndex': 9999,
                    'top': 8 ,
                    'float': 'right'
                });
                _thjs.find("i.fa").removeClass("fa-search").addClass("fa-times");
            } else {
                _thjs.find("i.fa").removeClass("fa-times").addClass("fa-search");
                $(".searchmobi").addClass("hide").removeAttr('style');
            }
        });
    }
    $('#uudai-btn').on('click', function(){
        var _btn = $(this),
            _frm = '.uudai-body';
        $(_frm).wsFormError({
            notification: true,
            callback: function(data) {
                if ( !data ) {
                    $(_btn).button('loading');
                    var uudaidata = JSON.stringify($(_frm + ' input[type="text"]').serializeObject());
                    $.getJSON(_link + 'ajax/?method=uudai', {
                        value: uudaidata,
                        token: $(_frm).children('input[name=token_uudai]').val()
                    }, function(json, textStatus) {
                        if (json.status != "success") {
                            var type = 'danger';
                        }
                        $(_btn).button('reset');
                        $('#error-send-uudai').html(alertBootStrap(json.message, type));
                        $(_frm).find("input[type=text],textarea").val('');
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    });
                    $(_btn).button('reset');
                }
            }
        });
    });
    // CHAT
    $('#modalchat').on('click', function() {
        $('#chat_modal').show();
    });
    $('#chat_modal .close,.modal-chat').on('click', function(e){
        if( e.target !== this )
            return;
        $('#chat_modal').hide();
    });
    $(".ivideo").on("click", function() {
        var url = 'https://www.youtube.com/embed/'+$(this).data('video')+'?rel=0&amp;controls=1&amp;showinfo=0';
        $("#framevideo iframe").attr("src", url);
    });
    $("#selectVideo").on("change", function() {
        // Dau tien remove het tat ca class active
        //$(".video-group-list").find('.video-list').removeClass("active");
        //$(this).parent().addClass("active");
        $("#framevideo iframe").attr("src", $(this).val());
    });
    $(".video-list a").on("click", function() {
        // Dau tien remove het tat ca class active
        $(".video-group-list").find('.video-list').removeClass("active");
        $(this).parent().addClass("active");
        $("#framevideo iframe").attr("src", $(this).data('link'));
    });
    // jrating
    $('.jrating').jRating({
        type:'small',
        step:true,
        length : 5
    });
    $('.jratingbig').jRating({
        step:true,
        length : 5
    });
    // end jrating
    $('.item_option')._removeClass();
    var _link = $("#rootPath").data("url");
    if ($('body').find('#ws-blockcopy').length) {
        $(this).bind('copy', function(e) {
            e.preventDefault();
        });
        $(this).bind('cut', function(e) {
            e.preventDefault();
        });
        $(this).bind("contextmenu", function(e) {
            e.preventDefault();
        });
        $(this).mouseup(function(e) {
            if (e.which === 3) {
                e.preventDefault();
            }
        });
    }
    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        $('.list-duan a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('list-duan ul li').removeClass("active");
                currLink.parent('li').addClass("active");
            }
            else{
                currLink.parent('li').removeClass("active");
            }
        });
    }
    $(document).on("scroll", onScroll);
    $('[data-duan=1]').on('click', function() {
        $(document).off("scroll");
        $('li').each(function () {
            $(this).removeClass('active');
        })
        $(this).parent('li').addClass('active');
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
    if ( $('.tab-duan').length ) {
        var tabwidth = $('.tab-duan').width();
        $('.content-duan').css({
            marginLeft: tabwidth,
            width: 'calc(100% - ' + tabwidth +'px)',
        });
    }
    $(".why-content > li > a").on("click", function() {
        if ($(this).closest('.why-content').hasClass('news')) {
            return;
        }
        $(this).closest('.why-content').find("a").removeClass("active");
        $(this).addClass("active");
        if ($(this).next(".why-des").is(':visible')) {
            $(this).next(".why-des").slideUp(400);
            $(this).closest('.why-content').find("a").removeClass("active");
        } else {
            $(this).closest('.why-content').find(".why-des").slideUp(400);
            $(this).next(".why-des").slideDown(400);
        }
    });
    $('.prodnew-container').easyTicker({
        direction: 'up',
        easing: 'swing',
        speed: 'slow',
        interval: 5000,
        visible: 3,
        mousePause: 1,
    });
    // SUPPORT FOOTER
    $(".suportfix-btn").on("click", function() {
            if ($('body').find(".supportfix-content").length) {
                $(".support-fix").toggleClass('opened');
            }
        })
        //
    $(".slide-item.dm > ul > li > ul").parent().addClass('parent');
    if ($.isFunction($.fn.matchHeight)) {
        $(".parent > ul > li").matchHeight();
    }
    $("#price_request_show, .price-close").on("click", function() {
        $(".price-request").toggleClass("opened");
        $("#frm-pricerequest").toggle(400).css('maxHeight', 880);
        if ($(".price-request").hasClass('opened')) {
            $('#frm-pricerequest').find('.form-group').removeClass('has-error');
            $('#frm-pricerequest').find('input, textarea').val('');
        }
    });
    // Modal Product
    $('.btn-choose a').on('click', function() {
        var _id = $(this).data('id');
        if (!isNaN(parseFloat(_id) && isFinite(_id))) {
            $.getJSON(_link + 'ajax.php?act=getinfoprod', {
                id: _id
            }, function(data) {
                if (data.status != 'success') {} else {
                    $("#modal-product .modal-title").html(data.name);
                    $("#modal-product #this_product_id").val(data.id);
                }
            });
        }
    });
    // var widthdevice = $(window).width();
    //  if ($.isFunction($.fn.elevateZoom) && widthdevice > 768 && $("#img_01").length) {
    //     $("#img_01").elevateZoom({
    //         gallery: "gal1",
    //         // zoomType: "inner",
    //         lensSize: 150,
    //         cursor: "pointer"
    //     });
    //     $("#img_01").bind("click", function(e) {
    //         var ez = $('#img_01').data('elevateZoom');
    //         $.fancybox(ez.getGalleryList());
    //         return false;
    //     });
    // }
    // RESET INPUT FORM
    $("#modal-product").on('hidden.bs.modal', function() {
        $(this).find("input, textarea").val('');
        $(this).find('.form-group').removeClass('has-error');
    });
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    // WOW
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: '', // default
        offset: 100, // default
        mobile: false, // default
        live: true // default
    })
    wow.init();
    /* if ( $('.wow').length > 0 ) {
         window.sr=ScrollReveal({reset:true,duration:1000,delay:200})
         sr.reveal('.wow.reveal');
         // SANPHAM
         sr.reveal('.prod-item', { duration: 2500, reset: true }, 150);
     }*/
    $('.modal-tuvan').on('hidden.bs.modal', function() {
        $(this).find('.modal-body').html('').removeAttr('id');
    });
    $(document).on('click', '.dangkylichhen', function(event) {
        var _id = $(this).data('id');
        var selectData = $("#select-data-" + _id).data('value');
        $.getJSON(_link + 'ajax/', {
            method: 'loadtuvan',
            id: _id,
            value: selectData
        }, function(json, textStatus) {
            if (json.status == 'success') {
                $('#modal-' + _id).find('.modal-body').html(json.data).attr('id', 'frm-send-tuvan');
            }
        });
    });
    $(document).on('click', '.btn-send-tuvan', function(event) {
        event.preventDefault();
        $('#frm-send-tuvan').wsFormError({
            notification: true,
            callback: function(data) {
                if (data == 0) {
                    $('.btn-send-tuvan').button('loading');
                    var tuvandata = JSON.stringify($('#frm-send-tuvan input, #frm-send-tuvan select, #frm-send-tuvan textarea').serializeObject());
                    $.getJSON(_link + 'ajax/?method=sendtuvan', {
                        value: tuvandata,
                        emailsend: $('#frm-send-tuvan').next('.modal-footer').find('.btn-send-tuvan').data('email')
                    }, function(json, textStatus) {
                        if (json.status != "success") {
                            var type = 'danger';
                        }
                        $('.btn-send-tuvan').button('reset');
                        $('#error-send-tuvan').html(alertBootStrap(json.message, type));
                        $('#frm-send-tuvan').find("input,textarea").val('');
                        setTimeout(function() {
                            $('.modal-tuvan').modal('hide');
                        }, 4000)
                    });
                }
            }
        })
    });
    $(".ndlocnhanh-content li").click(function(event) {
        event.preventDefault();
        $(this).siblings().removeClass('choosed');
        $(this).addClass('choosed');
    });
    if ( $("#fullpopup").length ) {
        $(".closepopup").click(function() {
            $("#fullpopup").slideToggle(700);
        });
    }
    // KIEM TRA EMAIL
    function isMail(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
   // @ Dang Ky Nhan Mail
    // $('#dkmailBtn').on('click', function() {
    //     $(this).button('loading');
    //     var _input = $(this).prev('input[name=dkmail]');
    //     if (_input.val().length) {
    //         if (isMail(_input.val())) {
    //             $.getJSON(_link + 'ajax/?method=dangkymail', {
    //                 value: _input.val()
    //             }, function(json, textStatus) {
    //                 if (json.status != 'success') {
    //                     if (json.status == 'exists') {
    //                         // Email ton tai
    //                         $('#dkmailBtn').button('reset');
    //                         $(_input).val('');
    //                         swal(json.message, '', 'error');
    //                     } else {
    //                         // Loi ngoai le
    //                         $('#dkmailBtn').button('reset');
    //                         $(_input).val('');
    //                         swal(json.message, '', 'error');
    //                     }
    //                 } else {
    //                     // Dang ky thanh cong
    //                     $('#dkmailBtn').button('reset');
    //                     $(_input).val('');
    //                     swal(json.message, '', 'success');
    //                 }
    //             });
    //         } else {
    //             // Email khong hop le
    //             $('#dkmailBtn').button('reset');
    //             $(_input).val('');
    //             swal('Vui lòng nhập một email hợp lệ', '', 'warning');
    //         }
    //     } else {
    //         // Chua nhap Email
    //         $('#dkmailBtn').button('reset');
    //         $(_input).val('');
    //         swal('Vui lòng nhập email để tiếp tục', '', 'warning');
    //     }
    // });
    //
    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
    // @ Dang Ky Nhan sdt
    $('#dkmailBtn').on('click', function() {
        $(this).button('loading');
        var _input = $(this).prev('input[name=dksdt]');
        if (_input.val().length) {
            if (isNumeric(_input.val())) {
                $.getJSON(_link + 'ajax/?method=dangkymail', {
                    value: _input.val()
                }, function(json, textStatus) {
                    if (json.status != 'success') {
                        if (json.status == 'exists') {
                            // Email ton tai
                            $('#dkmailBtn').button('reset');
                            $(_input).val('');
                            swal(json.message, '', 'error');
                        } else {
                            // Loi ngoai le
                            $('#dkmailBtn').button('reset');
                            $(_input).val('');
                            swal(json.message, '', 'error');
                        }
                    } else {
                        // Dang ky thanh cong
                        $('#dkmailBtn').button('reset');
                        $(_input).val('');
                        swal(json.message, '', 'success');
                    }
                });
            } else {
                // SDT khong hop le
                $('#dkmailBtn').button('reset');
                $(_input).val('');
                swal('Vui lòng nhập SĐT hợp lệ', '', 'warning');
            }
        } else {
            // Chua nhap Email
            $('#dkmailBtn').button('reset');
            $(_input).val('');
            swal('Vui lòng nhập SĐT để tiếp tục', '', 'warning');
        }
    });
    // @ Dang Ky Nhan Email
    $('#dkmailBtn1').on('click', function() {
        $(this).button('loading');
        var _input = $(this).prev('input[name=dkmail]');
        if (_input.val().length) {
            if (isMail(_input.val())) {
                $.getJSON(_link + 'ajax/?method=dangkymail', {
                    value: _input.val()
                }, function(json, textStatus) {
                    if (json.status != 'success') {
                        if (json.status == 'exists') {
                            // Email ton tai
                            $('#dkmailBtn1').button('reset');
                            $(_input).val('');
                            swal(json.message, '', 'error');
                        } else {
                            // Loi ngoai le
                            $('#dkmailBtn1').button('reset');
                            $(_input).val('');
                            swal(json.message, '', 'error');
                        }
                    } else {
                        // Dang ky thanh cong
                        $('#dkmailBtn1').button('reset');
                        $(_input).val('');
                        swal(json.message, '', 'success');
                    }
                });
            } else {
                // SDT khong hop le
                $('#dkmailBtn1').button('reset');
                $(_input).val('');
                swal('Vui lòng nhập Email hợp lệ', '', 'warning');
            }
        } else {
            // Chua nhap Email
            $('#dkmailBtn1').button('reset');
            $(_input).val('');
            swal('Vui lòng nhập Email để tiếp tục', '', 'warning');
        }
    });
    // @ Dang Ky Nhan Mail Ở GÓC PHẢI
    $('#dkmailBtn2').live('click', function() {
        $(this).button('loading')
        var _input = $('input[name=emailtuvan2]');
        var _input2= $('input[name=hotentuvan2]');
        var _input3= $('input[name=sdttuvan2]');
        if (_input.val().length) {
                $.getJSON(_link + 'ajax/?method=nhantuvan', {
                    value: _input.val(),
                    value2: _input2.val(),
                    value3: _input3.val()
                }, function(json, textStatus) {
                    if (json.status != 'success') {
                        if (json.status == 'exists') {
                            $('#dkmailBtn2').button('reset');
                            $(_input).val('');
                            swal(json.message, '', 'error');
                        } else {
                            // Loi ngoai le
                            $('#dkmailBtn2').button('reset');
                            $(_input).val('');
                            swal(json.message, '', 'error');
                        }
                    } else {
                        // Dang ky thanh cong
                        $('#dkmailBtn2').button('reset');
                        $(_input).val('');
                        $(_input2).val('');
                        $(_input3).val('');
                        swal(json.message, '', 'success');
                    }
                });
                // console.log(value3);
        } else {
            $('#dkmailBtn2').button('reset');
            $(_input).val('');
            swal('Vui lòng nhập số điện thoại để tiếp tục', '', 'warning');
        }
    });
    $(".closepopuptop").click(function(event) {
        $(".popup_top").hide(300);
    });
    $(".cp-item.cat, .danhmuccol").wsMeMo({
        animate: 'slide',
        padLeft: true,
        padLeftVal: 10
    });
    /** SideBar */
    if ($(window).width() <= 768) {
        $('#wapper_wapper').append('<div id="load-colleft"></div><div id="content-colleft"></div>');
    }
    // CLOSE PANEL
    $("#load-colleft, body > .close-colleft").live('click', function() {
        $('#wrap_noidung').removeClass('pullRight');
        $("#content-colleft").removeClass('_maximize');
        $("#load-colleft").hide();
        $("body").find('.close-colleft').remove();
    });
    // OPEN PANEL
    $("#btn-right").live('click', function() {
        $('#wrap_noidung').addClass('pullRight');
        $("#content-colleft").addClass('_maximize');
        $("#load-colleft").show();
        if (!$('body').children('.close-colleft').length) {
            $("body").append('<div class="close-colleft">&times;</div>');
            $(".close-colleft").css({
                "position": "fixed",
                "top": 0,
                "right": 0,
                "zIndex": 99999,
                "backgroundColor": "#FFF",
                "color": "#000",
                "padding": "10px",
                "fontSize": "40px",
                "cursor": "pointer"
            });
        } else {
            $("body").find('.close-colleft').remove();
        }
        /** Load Ajax */
        $("#websoloading").show();
        $("#content-colleft").load($("#rootPath").data('url') + "plugins/menuSidebar/cotmobile.php", function() {
            setTimeout(function() {
                $("#websoloading").hide();
            }, 50);
            $(".danhmucmobile li ul").parent().addClass('has-child').prepend('<i class="fa fa-plus"></i>');
            //
            $(".danhmucmobile").find('li.has-child > i').bind("click", function() {
                if ($(this).hasClass('fa-plus')) {
                    $(this).removeClass('fa-plus').addClass('fa-minus');
                } else {
                    $(this).removeClass('fa-minus').addClass('fa-plus');
                }
                // $(this).parent('li.has-child').find('>ul').slideDown(400);
                $(this).parent('li.has-child').find('>ul').toggleClass('opened');
            });
        });
    });
    // bấm nút này hiện ra menu
    // $('#btn-menu').click(function(){
    //     $('#wapper_menu').toggle(400);
    // });
    if ($.isFunction($.fn.owlCarousel)) {
        $('.productRun').owlCarousel({
            dots: true,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ],
            responsive: {
                0: {
                    items: 2,
                    margin: 2,
										nav: true,
                },
                1024: {
                    items: 4,
										nav: false,
                },
                1200: {
                    items: 5
                }
            }
        });
        $('.contentRun').owlCarousel({
            nav: false,
            dots: true,
            items:4,
            margin:35,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                640: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1024: {
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
        $('.serviceRun').owlCarousel({
            nav: false,
            dots: true,
            margin: 20,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 2
                },
                480: {
                    items: 2
                },
                640: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1024: {
                    items: 4
                },
                1200: {
                    items: 5
                }
            }
        });
        $('.khuyenmaiRun').owlCarousel({
            dots: true,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 2,
                    margin: 2,
										nav: true,
                },
                1024: {
                    items: 4,
										nav: false,
                },
                1200: {
                    items: 5
                }
            }
        });
        $('.tabRun').owlCarousel({
            nav: false,
            dots: false,
            margin: 20,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1
                },
                375: {
                    items: 2
                },
                640: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1024: {
                    items: 4
                },
                1200: {
                    items: 5
                }
            }
        });
        $('.dmRun').owlCarousel({
            dots: true,
            margin: 0,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 3,
										nav: true,
                },
                360: {
                    items: 4,
										nav: true,
                },
                414: {
                    items: 5,
										nav: true,
                },
                768: {
                    items: 6,
										nav: false,
                },
                1024: {
                    items: 7
                },
                1200: {
                    items: 8
                }
            }
        });
        $('.videoRun').owlCarousel({
            nav: false,
            dots: false,
            items: 3,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ],
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                320: {
                    items: 1
                },
                360: {
                    items: 1
                },
                480: {
                    items: 2
                },
                640: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1024: {
                    items: 3
                }
            }
        });
        $('.ul_ykienkhachhang').owlCarousel({
            nav: true,
            dots: false,
            items:1,
            margin:15,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ]
        });
        $('.doitacRun').owlCarousel({
            nav: true,
            dots: false,
            items:6,
            margin: 0,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1
                },
                320: {
                    items: 1
                },
                360: {
                    items: 2
                },
                480: {
                    items: 3
                },
                640: {
                    items: 4
                },
                768: {
                    items: 5
                },
                1024: {
                    items: 6
                },
                1200: {
                    items: 6,
                    nav:true
                }
            }
        });
        $('.pictureRun').owlCarousel({
            nav: false,
            dots: false,
            items:6,
            margin:40,
            navText: [
                '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1
                },
                375:{
                    items: 1
                },
                480: {
                    items: 2,
                    margin:20
                },
                640: {
                    items: 2
                },
                768: {
                    items: 2
                },
                1024: {
                    items: 3
                },
                1200: {
                    items: 3,
                }
            }
        });
        $('.ykienkhachhangRun').owlCarousel({
            nav: true,
            dots: false,
            items:6,
            margin:0,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ],
            responsive: {
                0: {
                    items: 1
                },
                1200: {
                    items: 1,
                    nav:true
                }
            }
        });
        var tongslide = $(".owl-slide.slide").data('sumslide');
        $(".owl-slide").owlCarousel({
            autoplay: true,
            autoplayTimeout: 2500,
            items: 1,
            loop: ((tongslide > 1) ? true : false),
            nav: false,
            lazyLoad: true,
            lazyContent: true,
            dots: false
        });
        $(".owl_lienquan").owlCarousel({
            autoplay: 20000,
            loop: false,
            nav: true,
            margin: 20,
            dots: false,
            lazyLoad: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                1200:{
                    items: 3
                }
            },
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ]
        });
        $(".owl_splienquan").owlCarousel({
            autoplay: 20000,
            loop: false,
            nav: true,
            margin: 10,
            dots: false,
            lazyLoad: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                360:{
                    items:2
                },
                480: {
                    items: 2
                },
                640: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1024:{
                    items: 4
                },
                1200: {
                    items: 4
                }
            },
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ]
        });
    }
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
            $('.go-top').addClass('hien');
        } else {
            $('.go-top').fadeOut(200);
            $('.go-top').removeClass('hien');
        }
    });
    $('.go-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 300);
    });
    // tooltip
    $('[data-toggle="tooltip"]').tooltip();
    // require gio hang
    $("#thanhtoan_giohang").click(function() {
        if ($("#checkout input[name=dienthoai]").val().length == 0) {
            $("#checkout input[name=dienthoai]").focus();
            swal('', $("#checkout input[name=dienthoai_alert]").val(), 'warning');
            return false;
        } else if ($("#checkout input[name=hoten]").val().length == 0) {
            $("#checkout input[name=hoten]").focus();
            swal('', $("#checkout input[name=hoten_alert]").val(), 'warning');
            return false;
        } else if ($("#checkout input[name=diachi]").val().length == 0) {
            $("#checkout input[name=diachi]").focus();
            swal('', $("#checkout input[name=diachi_alert]").val(), 'warning');
            return false;
        } else {
            return true;
        }
    });
    // news
    // start the ticker
    // $('.newsscroll').ticker( //{displayType: 'fade'}
    // );
    // zoom anh
    var widthdevice = $(window).width();
    if ($.isFunction($.fn.elevateZoom) && widthdevice > 768 && $("#img_01").length) {
        $("#img_01").elevateZoom({
            gallery: "gal1",
            zoomType: "inner",
            lensSize: 120,
            cursor: "pointer"
        });
        $("#img_01").bind("click", function(e) {
            var ez = $('#img_01').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    }
    // slide chi tiet
    if ($(".owl-slide-chitiet").length > 0) { $(".noidung_img2").show() }
    $(".owl-slide-chitiet").owlCarousel({
        autoplay: true,
        autoplayTimeout: 2500,
        items: 1,
        nav: true,
        lazyLoad: true,
        lazyContent: true,
        dots: false,
        navText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>']
    });
    if ($.isFunction($.fn.cubeslider)) {
        $('#cs-slider').cubeslider({
            cubesNum: { rows: 5, cols: 5 },
            orientation: 'h',
            spreadPixel: 0,
            cubeSync: 10,
            autoplay: 6000,
            autoplayInterval: 6000,
            navigation: false,
            play: false,
            random: true,
            animationSpeed: 600
        });
    }
});
(function($) {
    $.fn.menumaker = function(options) {
        var cssmenu = $(this),
            settings = $.extend({
                title: "Menu",
                format: "dropdown",
                sticky: false
            }, options);
        return this.each(function() {
            cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
            $(this).find("#menu-button").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.hide().removeClass('open');
                } else {
                    mainmenu.show().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });
            cssmenu.find('li ul').parent().addClass('has-sub');
            multiTg = function() {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                    } else {
                        $(this).siblings('ul').addClass('open').show();
                    }
                });
            };
            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');
            if (settings.sticky === true) cssmenu.css('position', 'fixed');
            resizeFix = function() {
                if ($(window).width() > 768) {
                    cssmenu.find('ul').show();
                }
                if ($(window).width() <= 768) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);
        });
    };
})(jQuery);
(function($) {
    $(document).ready(function() {
        $("#cssmenu").menumaker({
            title: "Menu",
            format: "multitoggle"
        });
    // XU LY MENU
    // $(".cssmenu > ul > li.has-sub").append(`
    //     <span class="todown">
    //         <i class="fa fa-caret-down"></i>
    //     </span>
    // `);
    });
})(jQuery);
// JavaScript Document
function changecard(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                } else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function Get_money(url, div) {
    //document.getElementById('ctl00_udpTop').style.display = 'block';
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                } else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function Get_Data(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
                //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
}
/*function MuaHangg(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
                //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
}*/
function hidegiohang() {
    document.getElementById("id_sanpham_alt").style.display = 'none';
    document.getElementById("content_wapper_choice").style.display = 'none';
}
function GetPage(pagename, div) {
    document.getElementById(div).innerHTML = "<span style='color:#000000;'><img src='images/icon/loading19.gif' /> Đang tải dữ liệu </span>";
    url = pagename;
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById(div).style.display = 'none';
                } else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function check_order(url, div, thongbao) {
    document.getElementById('ctl00_udpTop').style.display = 'block';
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    response = h.responseText;
                    if (response == 0) {
                        alert(thongbao);
                    } else {
                        document.getElementById(div).innerHTML = h.responseText;
                    }
                    document.getElementById('ctl00_udpTop').style.display = 'none';
                } else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function CheckNumber(number) {
    var pattern = "0123456789.";
    var len = number.value.length;
    if (len != 0) {
        var index = 0;
        while ((index < len) && (len != 0))
            if (pattern.indexOf(number.value.charAt(index)) == -1) {
                if (index == len - 1) number.value = number.value.substring(0, len - 1);
                else if (index == 0) number.value = number.value.substring(1, len);
                else number.value = number.value.substring(0, index) + number.value.substring(index + 1, len);
                index = 0;
                len = number.value.length;
            } else index++;
    }
}
function checkorder() {
    if (document.form_order.diadiem.value == '') {
        alert("Bạn chưa nhập địa điểm giao hàng.");
        document.form_order.diadiem.focus();
        return false;
    } else {
        return true;
    }
}
function _substr(srt, dem) {
    var chuoi = explode(' ', str);
}
function xulyloi(obj) {
    obj.className = "an";
}
function goilai(url, div, id, link, sodienthoaikhongdung, thongbao) {
    if (document.getElementById('txtgoilai').value == "" || isNaN(document.getElementById('txtgoilai').value) || document.getElementById('txtgoilai').value.length <= 6) document.getElementById('ycgoilai').innerHTML = '<span style="color:#F00;padding:3px">' + sodienthoaikhongdung + '</span>';
    else {
        var phone = document.getElementById('txtgoilai').value;
        document.getElementById(div).innerHTML = '<span style="color:#000; width: 100%; display: block; text-align: center; padding: 0px; font-size: 120%;"><img src="./templates/images/loading.gif"/><br> Đang gửi ...</span>';
        var request = $.ajax({
            type: "POST",
            url: url,
            data: {
                method: 'goilai',
                phone: phone,
                id: id,
                lienket: link
            }
        });
        request.done(function(msg) {
            document.getElementById(div).innerHTML = '<span style="color:#F00; width: 100%; display: block; text-align: center; padding: 5px; font-size: 120%;">' + thongbao + '</span>';
        });
        request.fail(function(jqXHR, textStatus) {});
    }
}
function goilai2(url, div, id, link, sodienthoaikhongdung, thongbao) {
    if (document.getElementById('txtgoilai2').value == "" || isNaN(document.getElementById('txtgoilai2').value) || document.getElementById('txtgoilai2').value.length <= 6) document.getElementById('ycgoilai2').innerHTML = '<span style="color:#F00;padding:3px">' + sodienthoaikhongdung + '</span>';
    else {
        var phone = document.getElementById('txtgoilai2').value;
        document.getElementById(div).innerHTML = '<span style="color:#000; width: 100%; display: block; text-align: center; padding: 0px; font-size: 120%;"><img src="./application/templates/images/loading.gif"/><br> Đang gửi ...</span>';
        var request = $.ajax({
            type: "POST",
            url: url,
            data: {
                method: 'goilai',
                phone: phone,
                id: id,
                lienket: link
            }
        });
        request.done(function(msg) {
            document.getElementById(div).innerHTML = '<span style="color:#F00; width: 100%; display: block; text-align: center; padding: 5px; font-size: 120%;">' + thongbao + '</span>';
        });
        request.fail(function(jqXHR, textStatus) {});
    }
}