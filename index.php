<?php
require 'configs/inc.php';

define('ROOT_PATH', 'https://' . $hostname . $SCRIPT_NAME);
if( !isset($_SESSION['ROOT_PATH'])
    || $_SESSION['ROOT_PATH'] != ROOT_PATH ) {

    $_SESSION['ROOT_PATH'] = ROOT_PATH;
}
if ( !function_exists('ws_get')
    || !function_exists('ws_post') ) {
    exit('File ws_function.php not found !');
}

// ĐẾM SẢN PHẨM ĐÃ LƯU
$productsavecount = count(json_decode($_COOKIE['productsave']));
$smarty->assign('productsavecount', $productsavecount);
/**=======================================================
@ Create token website*/
$token_website    = new Token();
$token_website->setName('website');
$tokenWebsite     = $token_website->create();
$tokenWebsiteMeta = $token_website->meta();
if ( !empty($tokenWebsiteMeta) ) {
    $smarty->assign('token_website_meta', $tokenWebsiteMeta);
}
#=================== STOP WEBSITE =======================
$eStopWeb = '';
if ( $_getArrayconfig['stop_website'] == 'on' ) {
    $eStopWeb.= '
        <div class="alert alert-danger clearfix" style="position: fixed; top: 0; left: 0; right: 0; padding: 5px 0; z-index: 9999999;text-align: center;font-size: 25px;background-color:#f2dede;border: 1px solid #ebccd1;color: #a94442;">
            <h5 class="text-center" style="margin: 0;">'.$_getArrayconfig['thongbao_stop'].'</h5>
        </div>
    ';
    $smarty->assign('eStopWeb', $eStopWeb);
    if ( !$db->isAdmin() ) {
        # NẾU KHÔNG PHẢI LÀ ADMIN THÌ PHẢI ECHO RA CHO KHÁCH XEM :D
        echo '<meta charset="utf-8">'.$eStopWeb;
        exit();
    }
}

$eViewfor = '';
if ( $_getArrayconfig['viewfor'] != 1 ) {
    $eViewfor.= '
        <div class="alert alert-danger" style="position: fixed; top: 0; left: 0; right: 0; padding: 5px 0; z-index: 9999999;text-align: center;font-size: 25px;background-color:#f2dede;border: 1px solid #ebccd1;color: #a94442;">
            <h5 class="text-center" style="margin: 0;">WEBSITE DANG DUOC NANG CAP</h5>
        </div>
    ';
    $smarty->assign('eViewfor', $eViewfor);
    if ( !$db->isAdmin() ) {
        # NẾU KHÔNG PHẢI LÀ ADMIN THÌ PHẢI ECHO RA CHO KHÁCH XEM :D
        echo $eViewfor;
        exit();
    }
}
if ($_getArrayconfig['viewfor'] == 3) {
    if (!$db->isSupperAdmin()) {
        exit();
    }
}

// CHECK LOGIN COOKIE
if( isset( $_COOKIE['username'] )  && isset( $_COOKIE['password'] ) ){
    $checkLogin = $db->check_login_user(
        $_COOKIE['username'], $_COOKIE['password']
    );
    if (count($checkLogin) == 1) {
        session_regenerate_id();
        $_SESSION['user_id']          = $checkLogin[0]['id'];
        $_SESSION['tendangnhap']      = $checkLogin[0]['tendangnhap'];
        $_SESSION['user_password']    = $password;
        $_SESSION['user_hotendem']    = $checkLogin[0]['hotendem'];
        $_SESSION['user_ten']         = $checkLogin[0]['ten'];
        $_SESSION['user_gioitinh']    = $checkLogin[0]['gioitinh'];
        $_SESSION['user_email']       = $checkLogin[0]['email'];
        $_SESSION['user_sodienthoai'] = $checkLogin[0]['sodienthoai'];
        $email = $checkLogin[0]['email'];
    }
}
if( ws_get('magioithieu') ) {
    $_SESSION['manguoigioithieu'] = ws_get('magioithieu');
}
// ĐĂNG KÝ PHƯƠNG (Method) THỨC CHO SMARTY
// {xx->yy p1= p2=}
$smarty->registerObject('db', $db, [
    'getNameFromID',
    'isSale',
    'sold',
    'percentProcess',
], false);

$smarty->assignByRef("didong", $didong);

$smarty->registerPlugin('modifier', 'Load_content_data', 'load_content_data');

include "plugins/rewrite/rewrite.php";
$array_background = get_data('background', '*', 'default,=,1', '', '');
$array_background = (!empty($array_background[0]) ? $array_background[0] : null);
//include "plugin/rewrite.php";
// set default lang admin
$_lang = $db->getNameFromID('tbl_lang', 'id', 'macdinhwebsite', 1);
if ($_lang == '') {
    $_lang = $db->getNameFromID('tbl_lang', 'id', 'macdinhwebsite', 0);
}
if (!isset($_SESSION['_lang'])) {
    $_SESSION['_lang'] = $_lang;
} else {
    if ($db->getNameFromID('tbl_lang', 'anhien', 'id', $_SESSION['_lang']) == 0) {
        $_SESSION['_lang'] = $_lang;
    }
}

// Lấy link danh mục và chuyển hướng khi đổi ngôn ngữ
$_getcat  = ws_get('cat');
$db->catRedirectLang($_getcat);

// Lấy link bài viết và chuyển hướng khi đổi ngôn ngữ
$_geturl  = ws_get('url');
$db->contentRedirectLang($_geturl);

$_tplname = 'index';
$_op      = 'home';
$_act     = $__idtype_danhmuc = $layout_act = null;
if ($_getcat) {

    $db->join("tbl_danhmuc_lang b", "a.id = b.iddanhmuc", "inner");
    $db->where("url", $_getcat);
    $_idtype = $db->getValue("tbl_danhmuc a", "idtype");

    $db->where("url", $_getcat);
    $__idtype_danhmuc = $db->getValue("tbl_danhmuc_lang", "iddanhmuc");

} elseif ($_geturl != '') {

    $db->join("tbl_noidung_lang b", "a.id = b.idnoidung", "inner");
    $db->where("url", $_geturl);
    $_iddanhmuc = $db->getValue("tbl_noidung a", "idtype");

    $_arr_iddanhmuc   = explode(',', $_iddanhmuc);
    $_iddanhmuc_0     = $_arr_iddanhmuc[0];
    $__idtype_danhmuc = $_iddanhmuc_0;
    if ($_iddanhmuc_0 != '') {
        $_idtype = $db->getNameFromID('tbl_danhmuc', 'idtype', 'id', "'" . $_iddanhmuc_0 . "'");
    }
    $_act       = 'detail';
    $_tplname   ='detail';
}
$quet = explode("/", $_SERVER['REQUEST_URI']);
$quet = end($quet);
if (!empty($_idtype) && $_idtype != '') {
    $_op = $db->getNameFromID('tbl_danhmuc_type', 'op', 'id', "'{$_idtype}'");
} elseif ($quet == "cart.htm") {
    $_op = 'giohang';
}elseif(ws_get('op')){
    $_op = $_GET['op'];
} else {
    $_op = 'home';
}

if ($_op == '' || $_op == 'home' ) {
    $checkhome = "home";
} else {
    $checkhome = "nothome";
}
if (isset($_GET['op'])) {
    $_op = ws_get('op');
}
if (isset($_GET['act'])) {
    $_act = ws_get('act');
}
if ($_act != '') {
    $file_content = 'modules/' . $_op . '/' . $_act . '.php';
}else{
    $file_content = 'modules/' . $_op . '/index.php';
}

if( $hostname != 'a' ){
    echo check404(@$__idtype_danhmuc,$_geturl,$_getcat,$file_content);
}

/*CREATE CACHE CSS*/
if (!file_exists(LAYOUT_PATH.'css/css.cache.css')) {
    include LAYOUT_PATH.'content_css.php';
}
if ($_SERVER['HTTP_HOST'] == 'a' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1') {
    include LAYOUT_PATH.'content_js.php';
} else {
    if (!file_exists(LAYOUT_PATH.'js/load.cache.js')) {
        include LAYOUT_PATH.'content_js.php';
    }
}

// load bien
include 'configs/bien.php';
// smarty header
include 'modules/_files/title.php';

/* THỰC HIỆN TẠO TIMELINE CHO DANH MỤC */
if( $__idtype_danhmuc ) {
    $catTimeline = $db->getNameFromID("tbl_danhmuc", "img", "id", @$__idtype_danhmuc);
    if (!empty($catTimeline)) {
        $smarty->assign("catTimeline", $catTimeline);
    }
}

/* SET BACKGROUND FOR BANNER */
$bgBanner = $db->getThongTin("bg_banner");
$bgBanner = get_src_img($bgBanner);
$bgbn = ' style="background-image:url(\'' . $bgBanner . '\'); " ';
$smarty->assign("bgbanner", $bgbn);

/* SET BACKGROUND FOR MENU */
$bgmenu = $db->getThongTin("bgmenu");
$bgmenu = get_src_img($bgmenu);
$bgbn = ' style="background-image:url(\'' . $bgmenu . '\'); " ';
$smarty->assign("bgmenu", $bgbn);

/* SET BACKGROUND FOR YKIENKHACHHANG */
$bgykienkhachhang = $db->getThongTin("bgykienkhachhang");
$bgykienkhachhang = get_src_img($bgykienkhachhang);
$bgykienkhachhang = ' style="background-image:url(\'' . $bgykienkhachhang . '\'); " ';
$smarty->assign("bgykienkhachhang", $bgykienkhachhang);

/* SET BACKGROUND FOR BOTTOM */
$bgBanner = $db->getThongTin("bottombg");
$bgBanner = get_src_img($bgBanner);
$bgbn = ' style="background-image:url(\'' . $bgBanner . '\')" ';
$smarty->assign("bottombg", $bgbn);

/* SET BACKGROUND FOR BOTTOM2 */
$bgBottomFix = $db->getThongTin("bottomfix");
$bgBottomFix = get_src_img($bgBottomFix);
$bgbn = ' style="background-image:url(\'' . $bgBottomFix . '\');" ';
// $smarty->assign("bottomfixbg", $bgbn);

#============================================================
# GET KEY LANGUAGES
#============================================================
$keyLang = $db->getNameFromID("tbl_lang", "idkey", "id", "{$_SESSION['_lang']}");
$smarty->assign("keylang", substr($keyLang, 0, 2));

#============================================================
# NÚT BẤM GỌI HOTLINE MOBILE
#============================================================
$numberMobile = trim(strip_tags($db->getThongTin("phone_mobile")));
$clickToCall = '';
if (!empty($numberMobile)) {
    $clickToCall = '
            <a href="tel:'.$numberMobile.'" class="clickToCall" title="hotline">
                <i class="fa fa-phone fa-lg"></i>
            </a>
    ';
    if ($didong->isMobile()) {
        $smarty->assign("clickToCall", $clickToCall);
    }
}

/**========================================================
@ MENU RIGHT FIX*/
$idlienhe  = $db->getNameFromID("tbl_danhmuc_type","id","op","'contact'");
$db->join("tbl_danhmuc_lang b", "a.id = b.iddanhmuc", "inner");
$db->where("idtype", $idlienhe);
$urlLienhe = $db->getValue("tbl_danhmuc a", "url");

$hotline = strip_tags($db->getThongTin("phone_mobile"));
if( !$hotline ){
    $hotline = strip_tags($db->getThongTin("hotline"));

}
if( !$hotline ){
    $hotline = strip_tags($db->getThongTin("hotline_top"));
}
$firstQC = $db->getThongTin("firstQC");
$smarty->assign("firstQC", $firstQC);

$seconQC = $db->getThongTin("seconQC");
$smarty->assign("seconQC", $seconQC);

$thongtincuoiwebsite = $db->getThongTin("thongtincuoiwebsite");
$smarty->assign("thongtincuoiwebsite", $thongtincuoiwebsite);

$menu_right = array(
    "hotline" => $hotline,
    "fanpage" => $chatfanpage,
    "lienhe"  => ROOT_PATH.$urlLienhe."/"
);

if ( $smarty->getTemplateVars('menu_right') === null ) {
    $smarty->assign("menu_right", $menu_right);
}

$analytics = "";
if ($_getArrayconfig['analytics'] != '') {
    $analytics = "<script type=\'text/javascript\'>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', '" . $_getArrayconfig['analytics'] . "', 'auto');
  ga('send', 'pageview'); </script>";
}

// Đặt background cho website
$bgWebsite = create_background($array_background);
$smarty->assign('bgWebsite', $bgWebsite);

$home_doitac        = include 'modules/_files/doitac.php';
$modulesIntro = include 'modules/home/intro.php';
$smarty->assign("modulesIntro", $modulesIntro);


// $home_doitac = null;
$smarty->assign("rowdoitac", $home_doitac);
$smarty->assign('checkhome', $checkhome);
$smarty->assign('op', $_op);
$smarty->assign('root_path', ROOT_PATH);
$smarty->assign('layout_path', LAYOUT_PATH);
$smarty->assign('robots', $_arrtitle['robots']);
$smarty->assign('url', 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
$smarty->assign('favicon', $_getArrayconfig['favicon']);
$smarty->assign('ogtype', $_arrtitle['ogtype'], true);
$smarty->assign('title', $_arrtitle['tieude']);
$smarty->assign('_title', $_arrtitle['tieude']);
$smarty->assign('description', $_arrtitle['motatukhoa']);
$smarty->assign('keywords', $_arrtitle['tukhoa']);
$smarty->assign('author', 'https://' . $_SERVER['HTTP_HOST']);
$smarty->assign('images', $_arrtitle['images']);
$smarty->assign('language', 'vi');
$smarty->assign('ten', $_arrtitle['ten']);
$smarty->assign('analytics', $analytics);
$smarty->assign('mastertool', $_getArrayconfig['webmaster']);
$smarty->assign('backtotop', $_getArrayconfig['backtotop']);
$smarty->assign('manhungdau', $_getArrayconfig['manhungdau']);
$smarty->assign('manhungcuoi', $_getArrayconfig['manhungcuoi']);
$smarty->assign('appfacebookid', $_getArrayconfig['appfacebookid']);
$smarty->assign('webmaster', $_getArrayconfig['webmaster']);
$smarty->assign('geoposition',$arraybien['icbm'],true);
include LAYOUT_PATH.'inline_css.php';
$smarty->assign('inline_css', $inline_css);

//smarty content
if( true ){
    $top = include 'modules/_files/top.php';
    $smarty->assign('top', $top);
}

if(1){
    $banner = include 'modules/_files/banner.php';
    $smarty->assign('banner', $banner);
}

$qcBottom      = include 'modules/home/advertisementBottom.php';
$menu          = include 'modules/_files/menu.php';
$bottomfix     = include 'modules/_files/bottomfix.php';
// include "modules/_files/bottomFixCat.php";
$slide         = '';
include 'modules/home/danhmucSp.php';
$smarty->assign("cat_cur_name", $db->getFieldCat('ten'));
switch ($_op) {
    case 'home':
        $slide       = include 'modules/_files/slideshow.php';
        $cotb        = include 'modules/_files/cotb.php';
        // $slide    = include 'modules/_files/cubeslider.php';
        $layout_name = 'content.tpl';
        break;
    case 'cart':
        $layout_name = 'content.tpl';
        break;
    default:
        $smarty->assignByRef(
            'db',
            $db
        );
        $cotb = include 'modules/_files/cotb.php';
        $layout_name = 'content2cot.tpl';
        $braucube = '';
        $braucube = $db->createListLink(@$__idtype_danhmuc,ROOT_PATH,$_GET['url']);
        $smarty->assign("braucube",$braucube);
        break;
}
if ($_act = "detail" && $_act != "" && $_op == "product") {
    $layout_name = 'content.tpl';
}
$popuptop       = include "modules/_files/popup_top.php";
$popuphome      = include "modules/_files/popup_home.php";
// $supportfix     = include "modules/_files/support-is-mobile.php";
$smarty->assign('layout_name', $layout_name);
$smarty->assign('tplname', $_tplname);
$smarty->assign('layout_act', $layout_act);
$smarty->assign('popuptop', $popuptop);
$smarty->assign('popuphome', $popuphome);
$smarty->assign('menu', $menu);
$smarty->assign('slide', $slide);
// $smarty->assign('supportfix', $supportfix);
$smarty->assign('mota_menu', @$_arrtitle['mota']);
$smarty->assign('noidung_menu', @$_arrtitle['noidung']);

include $file_content;

// $arrdata la noi dung do tu cac modules ve
// samrty footer
// $menubottom = include 'modules/_files/menu-bottom.php';
$bottom = include 'modules/_files/bottom.php';
$onBottom = include 'modules/_files/onBottom.php';
// $map = $db->getThongTin("maphome");
$designby = '';
$designby .= '<div class="designby_info">'.$db->getThongTin('designby'). '</div>';
// $thongketruycapdesign = include("plugins/counter/counter_view.php");
$linkws = $arraybien['thietkewebsite'];
// if ($_op == 'home') {
//     $linkws = $arraybien['thietkewebsite'];
// }
// $designby .= '<div class="designby">' . $linkws . '</div>';
// $smarty->assign('map', $map);
//$smarty->assign('menubottom', $menubottom);
$smarty->assign('bottomfix', $bottomfix);
$smarty->assign('bottom', $bottom);
$smarty->assign('onBottom', $onBottom);
$smarty->assign('designby', $designby);
// kiemt ra truong khi xuat ra
$smarty->display(LAYOUT_PATH . 'tpl/index.tpl');
include "plugins/counter/counter.php";

// echo 'op:'. $_op;
// echo 'act:'. $_act;
// echo '<br />File modules: '. $file_content;
// echo '<br />QUERY_STRING:'. $_SERVER['QUERY_STRING'];
// echo '<br />REQUEST_URI:'. $_SERVER['REQUEST_URI'];