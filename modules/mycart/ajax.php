<?php
$dta_ctdonhang = '';
if (isset($id) && filter_var(trim($id), FILTER_VALIDATE_INT)) {

	$d = $db->rawQuery("
		SELECT * FROM tbl_donhangchitiet WHERE iddonhang = {$id}
	");

    $ghichu = $db->getNameFromID(
		"tbl_donhang",
		"ghichu",
		"id",
		$id
	);

    $dta_ctdonhang .= '
	   <div class="khachhang">'
	   	. $db->getUser('hotendem') . '&nbsp;'
	   	. $db->getUser('ten') . '</div>
       <div class="row chitiet-tinhtrang hide">
            <div class="col-md-6">' . $arraybien['chitietdonhangma'] . '</div>
            <div class="col-md-6">' . $arraybien['tinhtrang'] . '</div>
       </div>
       <div class="ghichu">' . $arraybien['ghichu'] . '<span>' . $ghichu . '</span></div>
        <table class="table table-striped tblDonhangCT">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>' . $arraybien['sanpham'] . '</th>
                    <th>' . $arraybien['soluong'] . '</th>
                    <th>' . $arraybien['gia'] . '</th>
                    <th width="80">' . $arraybien['thanhtien'] . '</th>
                </tr>
            </thead>
            <tbody>
                <tr>';
    if (count($d) > 0) {
		$tongdongia = $tongsoluong = $tongtien = 0;
        foreach ($d as $info_data2) {
            $data2_id        = $info_data2['id'];
            $data2_idsanpham = $info_data2['idsanpham'];
            $data2_dongia    = $info_data2['dongia'];
            $data2_soluong   = $info_data2['soluong'];
            $data2_tongtien  = ($data2_dongia * $data2_soluong);
			$d_tensp = $db->rawQuery("
				SELECT ten,url
				FROM tbl_noidung_lang
				WHERE idnoidung = {$data2_idsanpham}
				AND idlang = {$_SESSION['_lang']}
			");
            $d_tensp = $d_tensp[0];
            $dta_ctdonhang .= '
               <tr>
                  <td align="center">' . $data2_id . '</td>
                  <td>
                     <a href="../' . $d_tensp['url'] . '" title="' . $d_tensp['ten'] . '" target="_blank">' . $d_tensp['ten'] . '</a>
                  </td>
                  <td>' . number_format($data2_dongia, 0, ',', '.') . '&nbsp;ð</td>
                  <td>' . $data2_soluong . '</td>
                  <td>' . number_format($data2_tongtien, 0, ',', '.') . '&nbsp;ð</td>
               </tr>
            ';
            $tongdongia += $data2_dongia;
            $tongsoluong += $data2_soluong;
            $tongtien += $data2_tongtien;
        }
        $dta_ctdonhang .= '
            <tr style="background-color:#FFF9C4;">
               <td colspan="2" align="right">Tổng Cộng:</td>
               <td>' . number_format($tongdongia, 0, ',', '.') . '&nbsp;ð</td>
               <td>' . $tongsoluong . '</td>
               <td>' . number_format($tongtien, 0, ',', '.') . '&nbsp;ð</td>
            </tr>
         ';
    }

    $dta_ctdonhang .= '
                    <td></td>
                </tr>
            </tbody>
        </table>
       ';
}
echo $dta_ctdonhang;
