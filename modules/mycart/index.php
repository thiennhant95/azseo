<?php
$ret = '';

if ( ! is_login() ) {
    $ret.= _alert('Vui lòng đăng nhập để sử dụng chức năng này');
} else {
	$__cid     = $_GET['id'];

	$d_donhang = $db->rawQuery("
		SELECT * FROM tbl_donhang WHERE iduser = " . get_id_user() . " ORDER BY ngaydat DESC
	");

    $ret .= '
    <div class="panel panel-info">
        <div class="panel-heading">
			<h3 class="panel-title">
				' . $arraybien['donhangcua'] . '&nbsp;
				<strong>' . $db->getUser('ten') . '</strong>
			</h3>
        </div>
        <div class="panel-body2">
            <table class="table table-striped table-bordered table-donhang">
                <thead>
                    <tr>
                        <th style="text-align:center;">' . $arraybien['madonhang'] . '</th>
                        <th style="text-align:center;">' . $arraybien['ngaydat'] . '</th>
                        <th style="text-align:center;">' . $arraybien['soluong'] . '</th>
                        <th style="text-align:center;">' . $arraybien['tinhtrang'] . '</th>
                        <th style="text-align:center;">' . $arraybien['xem'] . '</th>
                    </tr>
                </thead>
                <tbody>';
// echo $s_sanpham2;
    if (count($d_donhang) > 0) {
        foreach ($d_donhang as $key_donhang => $value_donhang) {
            $donhang_id        = $value_donhang['id'];
            $donhang_madonhang = $value_donhang['madonhang'];
            $donhang_ngaydat   = $value_donhang['ngaydat'];
			$donhang_trangthai = $value_donhang['trangthai'];
			$donhang_idsanpham = $value_donhang['idsanpham'];
            $donhang_dongia    = $value_donhang['dongia'];
            $donhang_soluong   = $value_donhang['soluong'];
            $donhang_tongtien  = ($data2_dongia * $data2_soluong);

            $tongdonhang = $db->rawQuery("
				SELECT * FROM tbl_donhangchitiet WHERE iddonhang = {$donhang_id}
			");

            $ret .= '
				<tr>
					<td align="center">' . $donhang_madonhang . '</td>
					<td class="ngaydat">
						' . date("d/m/Y", strtotime($donhang_ngaydat)) . '
						<span>
						(' . date("H:i", strtotime($donhang_ngaydat)) . ')
						</span>
					</td>
					<td class="soluong" align="center">
							' . count($tongdonhang) . ' SP
					</td>
					<td class="trangthai" align="center">';
						if ($donhang_trangthai == 1) {
							$ret .= '
							<span class="dagui">' . $arraybien['dagui'] . '</span>';
						} else if ($donhang_trangthai == 2) {
							$ret .= '
							<span class="xuly">' . $arraybien['dangxuly'] . '</span>';
						} else if ($donhang_trangthai == 3) {
							$ret .= '
							<span class="hoantat">' . $arraybien['dahoantat'] . '</span>';
						} else if ($donhang_trangthai == 4) {
							$ret .= '
							<span class="huyhang">' . $arraybien['dahuy'] . '</span>';
						} else {
							$ret .= '
							<span class="choxuly">' . $arraybien['choxuly'] . '</span>';
						}

						$ret .= '
					</td>
					<td align="center">
						<a class="btn btn-default" onClick="Get_Data(\'' . ROOT_PATH . 'ajax/?op='.$_op.'&act=ajax&id=' . $donhang_id . '\',\'return_donhang\')" data-toggle="modal" href=\'.hienpopup\'>' . $arraybien['xem'] . '</a>
					</td>
			</tr>';
            /*$tongdongia += $data2_dongia;
        $tongsoluong += $data2_soluong;
        $tongtien += $data2_tongtien;*/
        }
        /*$ret .= '
    <tr style="background-color:#FFF9C4;">
    <td colspan="2" align="right">Tổng Cộng:</td>
    <td>' . number_format($tongdongia, 0, ',', '.') . '&nbsp;ð</td>
    <td>' . $tongsoluong . '</td>
    <td>' . number_format($tongtien, 0, ',', '.') . '&nbsp;ð</td>
    </tr>
    ';*/
    }
    $ret .= '
                </tbody>
            </table>
        </div>
    </div>';
// XUẤT CHI TIẾT ĐƠN HÀNG
    $ret .= '

    <div class="modal fade hienpopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">' . $arraybien['chitietdonhang'] . '</h4>
                </div>
                <div class="modal-body" id="return_donhang">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">' . $arraybien['donglai'] . '</button>
                </div>
            </div>
        </div>
    </div>';

}

$smarty->assign('mycart', $ret);