<?php
$data_comment_form = '';
$data_comment_form.='
  <div class="titlecomment"><i class="fa fa-question-circle"></i> '.$arraybien['guicauhoicuaban'].'</div>
<form action="" method="POST" accept-charset="utf-8" class="frm-comment" id="frm-comment">
	<div class="comment_col1">
        <input type="hidden" name="idtype" id="this_product_idtype" value="'.$__idtype_danhmuc.'" placeholder="" />
        <input type="hidden" name="idparent" id="this_product_idparent" value="'.$idparent.'" placeholder="" />

        <div class="form-group icon-input">
            <i class="fa fa-user fa-lg" arial-hidden="true"></i>
            <input type="text" require name="hoten" class="form-control ws-required ws-rq-hoten" placeholder="'.$arraybien['vuilongnhaphoten'].'..." />
            <span class="require">*</span>
        </div>

        <div class="form-group icon-input">
            <i class="fa fa-envelope"></i>
            <input type="text" data-wserror-email="Vui lòng nhập vào 1 Email" name="email" class="form-control ws-rq-email" placeholder="'.$arraybien['vuilongnhapemail'].'..." />
        </div>

        <div class="form-group icon-input">
            <i class="fa fa-phone fa-lg"></i>
            <input type="text" name="dienthoai" class="form-control ws-required ws-required-number ws-rq-dienthoai" data-wserror-phone="Vui lòng nhập vào 1 số điện thoại" placeholder="'.$arraybien['vuilongnhapsodienthoai'].'..." />
        </div>
	</div>
	<div class="comment_col2">
        <div class="form-group icon-input">
            <i class="fa fa-question"></i>
            <textarea require name="noidung" class="form-control ws-rq-noidung" rows="3" placeholder="'.$arraybien['vuilongnhapcauhoicuaban'].'..."></textarea>
            <span class="require">*</span>
        </div>
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-key"></i>&nbsp;' . $arraybien['captcha'] . '</div>
          <input style="height:47px;" require type="text" name="captcha"  class="form-control ws-rq-captcha" id="" placeholder="' . $arraybien['vuilongnhapcaptcha'] . '">
          <div class="input-group-addon captchadiv">
             <img src="' . $_SESSION['ROOT_PATH'] . 'plugins/captcha/image.php" id="img-captcha'.$id.'"/>
             <a onclick="$(\'#img-captcha'.$id.'\').attr(\'src\', \'' . $_SESSION['ROOT_PATH'] . 'plugins/captcha/image.php?rand=\' + Math.random())" title="reload">
                <img src="' . $_SESSION['ROOT_PATH'] . 'smarty/templates/images/reload.png" style="width: 30px; height: 30px; cursor: pointer;">
             </a>
             <input id="hi_captcha" type="hidden" value="'.$_SESSION['captcha_code'].'" />
          </div>
      </div>
  </div>
  <div class="clear"></div>
  <div class="btngroup">
    <button type="button" id="" data-loading-text="<i class=\'fa fa-spinner fa-spin fa-lg fa-fw\'></i>" class="btnSendQuestion btn btn-primary btn-center">
     <i class="fa fa-send"></i>
     '.$arraybien['guicauhoi'].'
    </button>
    <button type="reset" class="btn btn-default btn-center">
     <i class="fa fa-repeat"></i>
     '.$arraybien['nhaplai'].'
    </button>
  </div>
</form>
<div class="clear"></div>
';
if(isset($_GET['idcomment'])){
  echo $data_comment_form;
}else{
  return $data_comment_form;
}