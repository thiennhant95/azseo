<?php
$valCatNews = '';
###
# TODO
# 23/11/2016 15:06:28
# => Truy vấn danh mục check nổi bật
$type_menu = 'noidung';
$op_menu = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'$type_menu'");
$valdm = array();
$sdm = "SELECT a.id,img,b.ten,url,link,target
        FROM tbl_danhmuc AS a
           INNER JOIN tbl_danhmuc_lang AS b
           ON a.id = b.iddanhmuc
           AND b.idlang = {$_SESSION['_lang']}
        WHERE anhien = 1
           AND colvalue LIKE '%home%'
           AND a.keyname = 'trungtamhotro'
           AND idtype = $op_menu
        ORDER BY thutu ASC
        LIMIT 50";
$ddm = $db->sqlSelectSql($sdm);
if (count($ddm) > 0) {

    $bg_ = null;
    $bg = strip_tags($db->getThongTin("bgtrungtam"), "<img>");
    if (!empty($bg)) {
        $bg_ = get_src_img($bg);
        $bg_ = ' style="background-image: url(\''.$bg_.'\'); " ';
    }

    $valCatNews.='

    <div class="cat-center-container clearfix" '.$bg_.'>';
        foreach ($ddm as $kdm => $vdm) {
            $id   = $vdm['id'];
            $ten  = $vdm['ten'];
            $hinh  = ROOT_PATH."uploads/danhmuc/thumb/".$vdm['img'];
            $lket = !empty($vdm['link']) ? $vdm['link'] : ROOT_PATH . $vdm['url']."/";
            $tget = !empty($vdm['target']) ? 'target="'.$vdm['target'].'"': null;



        $valCatNews.='

        <div class="cat-center-box ws-container clearfix">';
            $valCatNews.='
            <div class="title-center clearfix">
                <a href="'.$lket.'" '.$tget.' title="'.$ten.'">'.$ten.'
                    <div class="line"></div>
                </a>
            </div>';
            ##
            # TODO
            #
            # => Truy vấn sản phẩm check nổi bật
            $s_news = "
            SELECT a.id,a.hinh,a.idtype,ten,url,link,target,mota
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND b.idlang = {$_SESSION['_lang']}
                AND a.idtype LIKE '%{$id}%'
                AND colvalue like '%home%'
            ORDER by a.thutu Asc
            LIMIT 120";
            $dnews = $db->sqlSelectSql($s_news);
            if (count($dnews) > 0) {
                $valCatNews.= '

                <div class="cat-center-content center-run owl-carousel clearfix">';
                    foreach ($dnews as $knews => $vnews) {
                        $idnews   = $vnews['id'];
                        $tennews  = $vnews['ten'];
                        $motanews = $vnews['mota'];
                        $hinhnews = ROOT_PATH."uploads/noidung/thumb/".$vnews['hinh'];
                        $lketnews = !empty($vnews['link']) ? $vnews['link'] : ROOT_PATH.$vnews['url'];


                        $valCatNews.='

                        <div class="center-item wow fadeInUp animated" data-wow-delay=".'.$knews.'s">
                            <div class="center-hinh">
                                <a href="'.$lketnews.'" title="'.$tennews.'">
                                    <img src="'.$hinhnews.'" alt="'.$tennews.'" class="img-renewsonsive" />
                                </a>
                            </div>
                            <!-- ./center-hinh -->
                            <div class="center-ten">
                                <a href="'.$lketnews.'" title="'.$tennews.'">'.$tennews.'</a>
                            </div>
                            <!-- ./center-ten -->

                            <div class="center-des">
                                '.$motanews.'
                            </div>
                            <!-- ./center-des -->
                        </div>
                        <!-- ./center-item -->

                        ';
                    }
                $valCatNews.= '

                </div>
                <!-- ./cat-center-content -->';
            }
        $valCatNews.='

        </div><!-- ./cat-center-box -->';
        }
    $valCatNews.='

    </div><!-- ./cat-center-container -->

    ';
}
return $valCatNews;
