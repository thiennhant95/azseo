<?php
    $ret = '';

    $sp = $db->layNoiDung([
        "op"     => "product",
        "select" => "timesale, sumsale, giasale",
        "where"  => " AND colvalue LIKE '%khuyenmai%'",
        "limit"  => 250
    ]);

    // RENDER
    $ret.='
    <div class="khuyenmai-container clearfix">
        <div class="ws-container">
            <div class="khuyenmai-inner clearfix">



                <div class="titlebase-home clearfix wow animated fadeInDown" data-wow-delay="0s">
                <h2 class="category_tab">
                    <a href="javascript:void(0);" title="'.$arraybien['sanphamkhuyenmai'].'">
                        <span class="tentieude">
                            '.$arraybien['sanphamkhuyenmai'].'</span>
                    </a>
                </h2>
                </div>
                <div class="khuyenmai-box clearfix">';
                    if( count($sp) > 0 ){

                        $smarty->assign("arrsanpham", $sp);
                        $smarty->assign("owl", "khuyenmaiRun");
                        $smarty->assign("rowsp", "2");
                        $ret.= $smarty->fetch(LAYOUT_PATH.'modules/product/tpl/item.tpl');
                    }
                $ret.='
                </div>
            </div>

            <!--<div class="ykienkhachhang-inner">';
                // $ret.= include 'customerReviews.php';
            $ret.='
            </div>-->

        </div>
    </div>';

    $dataHome['sanphamkhuyenmai'] = $ret;