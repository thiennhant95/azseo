<?php
    $ret = '';

    $sp = $db->layNoiDung([
        "op" => "product",
        "where" => " AND colvalue LIKE '%banchay%' ",
        "limit" => 50
    ]);

    // RENDER
    $ret.='
    <div class="banchay-container clearfix">
        <div class="banchay-inner ws-container clearfix">

            <h3 class="banchay-title">
                '.$arraybien['sanphamhoagiadepbanchay'].'</h3>

            <div class="banchay-box clearfix">';
                if( count($sp) > 0 ){

                    $smarty->assign("arrsanpham", $sp);
                    $smarty->assign("owl", "banchay-run");
                    $ret.= $smarty->fetch(LAYOUT_PATH.'modules/product/tpl/item.tpl');
                }

            $ret.='
            </div><!-- /.banchay-box -->

        </div><!-- /.banchay-inner -->
    </div><!-- /.banchay-container -->';

    $dataHome['sanphambanchay'] = $ret;