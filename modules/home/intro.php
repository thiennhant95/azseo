<?php
$dataIntro = '';

// Lay danh muc
$catintro = $db->layNoiDung([
    "op"    => "intro",
    "where" => "AND colvalue LIKE '%home%' ",
    "limit" => 1
]);

if (count($catintro) > 0) {
    foreach ($catintro as $kintro => $vintro) {
    $link  = ROOT_PATH . $vintro['url'];
    $ten   = $vintro['ten'];
    $hinh   = $vintro['hinh'];
    $mota  = $vintro['mota'];
    $noidung  = $vintro['noidung'];
    $dataIntro.= '<div class="introContainer clearfix">
                <div class="introContent ws-container clearfix">
                    

                    <div class="titlebase-home clearfix wow animated fadeInDown" data-wow-delay="0s">
		                <h2 class="category_tab">
		                    <a  data-id="{$itemcatp.id}" href="javascript:;" title="ADPHAR CO.,LTD">
		                        <span class="tentieude">ADPHAR CO.,LTD</span>
		                    </a>
		                    <span class="title-mota wow animated fadeInUp clearfix">Đối tác đáng tin cậy của bạn</span></span>
		                </h2>
                        <span class="tria"></span>
		            </div>



                    <div class="introbox row clearfix">
                        <div class="intro-item introImg clearfix col-md-6 col-xs-12">
                            <img src="'.ROOT_PATH.'uploads/noidung/'.$hinh.'" alt="'.$ten.'" />
                        </div>
                        <div class="intro-item introDes clearfix col-md-6 col-xs-12">
                            '.$mota.'
                        </div>
                    </div>

                </div>
            </div>';
    }
}
    return $dataIntro;
?>