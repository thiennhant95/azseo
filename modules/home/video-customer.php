<?php
$data = '';

// VIDEO
$videoType = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'video'");
$videosCat = $db->sqlSelectSql("
    SELECT b.id,a.iddanhmuc,ten,url,link,target,bgcolor,color,icon
    FROM tbl_danhmuc_lang AS a
        INNER join tbl_danhmuc AS b ON a.iddanhmuc = b.id
    WHERE b.anhien = 1
        AND a.idlang = {$_SESSION['_lang']}
        AND b.idtype LIKE '%{$videoType}%'
        AND colvalue LIKE '%home%'
    ORDER by thutu ASC
");
$videos = $db->sqlSelectSql("
    SELECT a.id,a.idtype,b.ten,link,file
    FROM tbl_noidung AS a
    INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
    WHERE b.idlang = {$_SESSION['_lang']}
        AND anhien = 1
        AND a.idtype LIKE '%{$videosCat[0]['id']}%'
        AND loai = 2
    ORDER by thutu ASC
    LIMIT 1
");

// Y KIEN KHACH HANG
$op_menu = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'customerreviews'");
$customersCat = $db->sqlSelectSql("
    SELECT b.id,a.iddanhmuc,ten,url,link,target,bgcolor,color,icon
    FROM tbl_danhmuc_lang AS a
        INNER join tbl_danhmuc AS b ON a.iddanhmuc = b.id
    WHERE b.anhien = 1
        AND a.idlang = {$_SESSION['_lang']}
        AND b.idtype LIKE '%{$op_menu}%'
    ORDER by thutu ASC
");
$idea_id = $customersCat[0]['id'];

$customers = $db->sqlSelectSql("
    SELECT a.id,a.hinh,masp,gia,giagoc,a.idtype,ten,url,link,target,mota,noidung
    FROM tbl_noidung AS a
    INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
    WHERE a.anhien = 1
        AND b.idlang = {$_SESSION['_lang']}
        AND colvalue like '%home%'
        AND a.loai = ".array_search("customerreviews", $_arr_loai_noidung)."
        AND a.idtype LIKE '%{$idea_id}%'
    ORDER by a.thutu Asc
    LIMIT 120
");

$thisbg = $db->getThongTin("bg_ykienkhachhang");
$thisbg = get_src_img($thisbg);
if (!empty($thisbg)) {
    $thisbg = 'style="background-image: url(\''.$thisbg.'\')";';
} else {
    $thisbg = null;
}


// RENDER
$data.='
<div class="video-customer clearfix">
    <div class="vc-item video">

        <div class="titlebase">
            <a href="javascript:;" title="'.$videosCat[0]['ten'].'">
                '.$videosCat[0]['ten'].'
                <span class="bo"></span>
            </a>
        </div>

        <div class="video-group">
    ';

        if( count($videos) > 0 ){
            $video = $videos[0];

            $videoName = $video['ten'];
            $videoLink = $video['link'];
            $keyVideo = end(explode("v=", $videoLink));

            $data.='
                <div id="framevideo" class="clearfix">
                    <iframe width="100%" height="220px" src="https://www.youtube.com/embed/'.$keyVideo.'?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0"></iframe>
                </div>';
        }

    $data.='
        </div>
    </div>
    <div class="vc-item customer">

        <div class="titlebase">
            <a href="javascript:;" title="'.$customersCat[0]['ten'].'">
                '.$customersCat[0]['ten'].'
                <span class="bo"></span>
            </a>
        </div>

        <div class="customer-group ul_ykienkhachhang owl-carousel" '.$thisbg.'>';
            if( count($customers) > 0 ){
                foreach( $customers as $customer ){

                    $data.='
                    <div class="customer-item">
                        <div class="image">
                            <img src="'.ROOT_PATH.'uploads/noidung/thumb/'.$customer['hinh'].'" alt="'.$customer['ten'].'" title="'.$customer['ten'].'" />
                        </div>
                        <div class="name">
                            <strong>'.$customer['ten'].'</strong>
                            <span>'.$customer['mota'].'</span>
                        </div>
                        <div class="speak">
                            '.$customer['noidung'].'
                        </div>
                    </div>';
                }
            }
        $data.='
        </div>

    </div>
</div>';

$dataHome['video_customer'] = $data;