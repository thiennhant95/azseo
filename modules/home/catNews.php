<?php

$valdm = array();
$data = '';

$ddm = $db->layDanhMuc(
    "content",
    " AND colvalue LIKE '%home%' ",
    null,
    10
);

    if (count($ddm) > 0) {
        $data.='
        <section class="catnew-container clearfix">
            <div class="ws-container">';
            foreach ($ddm as $kdm => $vdm) {

                $catNewId = $vdm['id'];
                $lienket = $vdm['link'] ? $vdm['link'] : ROOT_PATH.$vdm['url'] .'/';

                // Content

                $news = $db->layDanhMuc(
                    "content",
                    " AND a.idtype LIKE '%{$catNewId}%' AND colvalue like '%home%' ",
                    null,
                    120
                );

                 // Ads
                /* $adsType = $db->getNameFromID(
                    "tbl_banner_type",
                    "id",
                    "keyname",
                    "'trangchu'"
                );

                $qads = $db->sqlSelectSql("
                    SELECT b.ten,hinh,linkhinh,link,target,rong,cao
                    FROM tbl_banner AS a
                    INNER join tbl_banner_lang AS b
                        On a.id = b.idtype
                    WHERE a.loai = 2
                        AND b.idlang = {$_SESSION['_lang']}
                        AND a.idtype = {$adsType}
                        AND a.iddanhmuc = {$catNewId}
                        AND a.anhien = 1
                    ORDER by thutu asc
                "); */

                // Render cat
                $data.='
                <div class="catnew-item clearfix">';

                    $data.='
                    <div class="catnew-i cat-group">
                        <div class="catnew-title title-basses clearfix">
                            <a href="'.$lienket.'" title="'.$vdm['ten'].'">
                                '.$vdm['ten'].'
                                <span class="bo"></span>
                            </a>

                            <span class="xemtatca">
                                <a href="'.$lienket.'" title="'.$vdm['ten'].'">
                                    <i class="fa fa-plus-square-o"></i>
                                </a>
                            </span>
                        </div><!-- /.catnew-title -->

                        <div class="cat-content clearfix">';
                            foreach( $news as $new ){
                                $url = ROOT_PATH.$new['url'];
                                // RENDER NEWS
                                $data.='
                                <div class="new-item clearfix">

                                    <div class="image">
                                        <a href="'.$url.'" title="'.$new['ten'].'">
                                            <img src="'.ROOT_PATH.'uploads/noidung/thumb/'.$new['hinh'].'" class="img-responsive" title="'.$new['ten'].'" alt="'.$new['ten'].'" />
                                        </a>
                                    </div>
                                    <!-- ./image -->

                                    <div class="title">
                                        <a href="'.$url.'" title="'.$new['ten'].'">
                                            '.$new['ten'].'
                                        </a>
                                    </div>
                                    <!-- ./title -->
                                    <div class="description clearfix">
                                        '.$db->substr_word($new['mota'], 200).'...
                                    </div>
                                    <!-- ./description -->
                                </div>
                                <!-- ./new-item -->';
                            }
                        $data.='
                        </div>
                        <!-- ./cat-content -->
                    </div>
                    <!-- ./catnew-i -->

                    <!--div class="catnew-i ads-group">';
                        if( count($qads) > 0 ){
                            foreach( $qads as $ads ){

                                $lienket = $ads['link'];
                                $hinh = $ads['linkhinh'] ? $ads['linkhinh'] : ROOT_PATH."uploads/logo/".$ads['hinh'];
                                $target = $ads['target'] ? "target='".$ads['target']."'" : null;

                                $data.='
                                <div class="ads-item">
                                    <a href="'.$lienket.'" title="'.$ads['ten'].'" '.$target.'>
                                        <img src="'.$hinh.'" alt="'.$ads['ten'].'" title="'.$ads['ten'].'" class="img-responsive" />
                                    </a>
                                </div>
                                <!-- ./ads-item -->';

                            }
                        }
                    $data.='
                    </div-->
                    <!-- ./ads-group -->';

                $data.='
                </div>
                <!-- ./catnew-item -->';
            }
        $data.='
            </div><!-- /.ws-container -->
        </section>
        <!-- catnew-container -->';
    }
$dataHome['catnews'] = $data;
