<?php
$valProdHot = "";

$type_menu = 'sanpham';
$op_menu = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'$type_menu'");
$scat = "

    SELECT a.id,img,b.ten,url,link,target,mota,icon
    FROM tbl_danhmuc AS a
       INNER JOIN tbl_danhmuc_lang AS b
       ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
    WHERE anhien = 1
       AND colvalue LIKE '%home%'
       AND LENGTH(a.id) = 4
       AND a.idtype LIKE '%{$op_menu}%'
    ORDER BY a.thutu ASC

";

$qcat = $db->sqlSelectSql($scat);

if (count($qcat) > 0) {
    $dataHome['dmHome'] = $qcat;

    foreach ($qcat as $keycat => $dcat) {
        $catId = $dcat['id'];

        $scat2 = "

            SELECT a.id,img,b.ten,url,link,target,mota,icon
            FROM tbl_danhmuc AS a
               INNER JOIN tbl_danhmuc_lang AS b
               ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
            WHERE anhien = 1
               AND colvalue LIKE '%home%'
               AND a.id LIKE '%{$catId}%'
               AND LENGTH(a.id) = ".(strlen($catId) + 4)."
            ORDER BY a.thutu ASC

        ";
        $dcat2 = $db->sqlSelectSql($scat2);

        if (count($dcat2) > 0) {
            $dataHome[$keycat]['dmHome2'] = $dcat2;
        }

        $sql = "

            SELECT a.id,a.hinh,masp,gia,giagoc,a.idtype,ten,url,link,target,mota
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND b.idlang = {$_SESSION['_lang']}
                AND idtype LIKE '%{$catId}%'
            ORDER by a.thutu Asc
            LIMIT 120

        ";
        $que = $db->sqlSelectSql($sql);

        if (count($que) > 0) {
            $dataHome[$keycat]['prodHome'] = $que;
        }
    }


}
