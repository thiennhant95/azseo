<?php
$valVideo = "";
#-----------------------------------------------------------
# CUSTOMER
#-----------------------------------------------------------
$v_custom = '';
$sCus = "
SELECT a.id,b.ten,hinh,linkhinh,link,target,rong,cao
FROM tbl_banner a INNER JOIN tbl_banner_lang b
    ON a.id = b.idtype
WHERE a.anhien = 1
    AND a.loai = 3
    AND b.idlang = {$_SESSION["_lang"]}
ORDER BY thutu ASC";
$dCus = $db->sqlSelectSql($sCus);
if (count($dCus) > 0) {
    $v_custom.='<div class="video-container clearfix">
        <div class="titlebase clearfix">
            <a href="javascript:;" title="'.$arraybien['khachhang'].'">
                <i class="fa fa-empire fa-1x"></i>
                '.$arraybien['khachhang'].'
                <div class="line"></div>
            </a>
        </div>
        <div class="boxbase clearfix">';
        $v_custom .= '
         <div class="owl_doitac owl-carousel">';
        foreach ($dCus as $key_doitac => $value_doitac) {
            $doitac_id      = $value_doitac['id'];
            $doitac_ten     = $value_doitac['ten'];
            $doitac_hinh    = ($value_doitac['linkhinh'] != "") ? $value_doitac['linkhinh'] : ROOT_PATH . "uploads/logo/" . $value_doitac['hinh'];
            $doitac_lienket = ($value_doitac['link'] != "") ? $value_doitac['link'] : null;
            $doitac_target  = ($value_doitac['target'] != "") ? ' target="_blank" ' : null;
            $doitac_rong    = $value_doitac['rong'];
            $doitac_cao     = $value_doitac['cao'];
            $v_custom .= '
                <div class="item-doitac">
                   <a href="' . $doitac_lienket . '" title="' . $doitac_ten . '" ' . $doitac_target . '>
                      <img src="' . $doitac_hinh . '" class="img-responsive" alt="' . $doitac_ten . '" onerror="xulyloi(this)" />
                   </a>
                </div>';
        }
    $v_custom .= '</div>';
    $v_custom.='</div><!-- boxbase -->
    </div>';
}

#---------------------------------------------------------
# TIN TUC NOI BAT
#---------------------------------------------------------
$v_news = '';
#--------------------------------------------------
# QUERY why1
#--------------------------------------------------
$type_menu = 'noidung';
$op_menu = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'$type_menu'");
$swhy1 = "SELECT a.id,img,b.ten,url,link,target,mota
        FROM tbl_danhmuc AS a
           INNER JOIN tbl_danhmuc_lang AS b
           ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
        WHERE anhien = 1
           AND colvalue LIKE '%noibat%'
           AND a.idtype LIKE '%{$op_menu}%'
           AND keyname = ''
        ORDER BY a.thutu ASC";
$dwhy1 = $db->sqlSelectSql($swhy1);
if (count($dwhy1) > 0) {
    $v_news.='<div class="why-container clearfix">';
    #--------------------------------------------------
    # LOOP why1
    #--------------------------------------------------
        $vwhy1       = $dwhy1[0];
        $why1_id     = $vwhy1['id'];
        $why1_name   = $vwhy1['ten'];
        $why1_des    = $vwhy1['mota'];
        $why1_link   = !empty($vwhy1['link']) ? $vwhy1['link'] : ROOT_PATH.$vwhy1['url'].'/';
        $why1_target = !empty($vwhy1['target']) ? 'target="'.$vwhy1['target'].'"' : null;
        #---------------------------------------------------
        # ECHO why1
        #---------------------------------------------------
        $v_news.= '
        <div class="titlebase clearfix">
            <a href="javascript:;" title="'.$why1_name.'">
                <i class="fa fa-empire fa-1x"></i>
                '.$why1_name.'
                <div class="line"></div>
            </a>
        </div>
        <div class="boxbase clearfix">
        <ul class="why-content news clearfix">';
            #--------------------------------------------
            # QUERY why2
            #--------------------------------------------
            $swhy2 = "SELECT a.id,a.hinh,masp,gia,giagoc,a.idtype,ten,url,link,target,mota
                    FROM tbl_noidung AS a
                    INNER JOIN tbl_noidung_lang AS b
                        ON a.id = b.idnoidung
                    WHERE a.anhien = 1
                        AND b.idlang = {$_SESSION['_lang']}
                        AND loai = ".array_search("noidung", $_arr_loai_noidung)."
                        AND colvalue like '%home%'
                        AND a.idtype LIKE '%{$why1_id}%'
                    ORDER by a.thutu Asc
                    LIMIT 7";
            $dwhy2 = $db->sqlSelectSql($swhy2);
            if (count($dwhy2) > 0) {
                #----------------------------------------
                # LOOP CAT 2
                #----------------------------------------
                foreach ($dwhy2 as $kwhy2 => $vwhy2) {
                    $why2_id     = $vwhy2['id'];
                    $why2_name   = $vwhy2['ten'];
                    $why2_des   = $vwhy2['mota'];
                    $why2_link   = !empty($vwhy2['link']) ? $vwhy2['link'] : ROOT_PATH.$vwhy2['url'];
                    $why2_target = !empty($vwhy2['target']) ? 'target="'.$vwhy2['target'].'"' : null;
                    $why2_img    = !empty($vwhy2['img']) ? ROOT_PATH."uploads/danhmuc/".$vwhy2['img'] : null;

                    #-----------------------------------
                    # ECHO why2
                    #-----------------------------------
                    $v_news.='
                    <li>
                        <a href="'.$why2_link.'" title="'.$why2_name.'">
                            '.$why2_name.'
                        </a>
                    </li>';
                }
            }
        $v_news.='</ul>
            </div>';
    $v_news.='</div><!-- ./why-container-->';
}
#------------------------------------------
# TEMPLATES
#------------------------------------------
$valVideo.='
    <div class="videowhy ws-container rows2 form-group clearfix">
        <div class="videowhy-content clearfix">
            <div class="videowhy-item clearfix video">
                '.$v_custom.'
            </div>
            <div class="videowhy-item clearfix why">
                '.$v_news.'
            </div>
        </div>
        <!-- ./videowhy-content -->
    </div>
    <!-- ./videowhy -->
';
$dataHome["cusnews"] = $valVideo;
