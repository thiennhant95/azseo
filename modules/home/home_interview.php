<?php
    $data = '';

    $qcat = $db->layDanhMuc(
        "service",
        " AND colvalue LIKE '%noibat%' ",
        null,
        2
    );

    $box1 = '';
    if( count($qcat) > 0 ) {
        // FOREACH tile
        $linkPlus = '';
        $box1.='
        <ul class="clearfix highlight-title">';
            foreach( $qcat as $kcat => $icat ){
                $box1.='
                <li data-target="'.$icat['id'].'" '.(!$kcat?'class="active"':null).' data-href="'.ROOT_PATH.$icat['url'].'/">
                    <strong>'.$icat['ten'].'</strong>
                </li>';

                if( ! $kcat )
                    $linkPlus = ROOT_PATH.$icat['url'];
            }
        $box1.='
            <span class="viewmore">
                <a href="'.$linkPlus.'/" title="">
                    <i class="fa fa-plus-square-o fa-lg" aria-hidden="true"></i>
                </a>
            </span>
        </ul>';

        // FOREACH content
        $box1.='
        <!-- .highlight-content -->
        <div class="highlight-content clearfix">';
            foreach( $qcat as $cat ) {
                $id = $cat['id'];

                $qcont = $db->layDanhMuc(
                    "service",
                    " AND a.idtype LIKE '%{$id}%' AND colvalue LIKE '%noibat%' ",
                    null,
                    4
                );

                $box1.='
                <div id="'.$cat['id'].'">
                ';

                    if( count($qcont) > 0 ){
                        foreach( $qcont as $cont ){
                            $box1.='
                            <!-- .highlight-item -->
                            <div class="highlight-item clearfix">
                                <a href="'.ROOT_PATH.$cont['url'].'" title="'.$cont['ten'].'">
                                    '.$cont['ten'].'
                                </a>
                                <span class="date">
                                    '.$cont['ngay'].'
                                </span>
                            </div><!-- /.highlight-item -->';
                        }
                    }

                $box1.='
                </div>';
            }
        $box1.='
        </div><!-- /.highlight-content -->';

    }

    // box2
    $q1 = $db->layDanhMuc(
        "download",
        " AND colvalue LIKE '%noibat%' AND LENGTH(a.id) = 4 ",
        null,
        1
    );
    $box2 = '';
    if( count($q1) > 0 ){
        $box2.='
        <div class="attachment-container clearfix">';

            foreach( $q1 as $c1 ){

                $box2.='

                <div class="attachment-head clearfix">
                    <a href="'.ROOT_PATH.$c1['url'].'/" title="'.$c1['ten'].'">
                        '.$c1['ten'].'
                    </a>
                </div><!-- /.attachment-head -->

                <div class="attachment-body">';


                    $qattach = $db->layDanhMuc(
                        "download",
                        " AND a.id LIKE '%{$c1['id']}%' AND colvalue LIKE '%noibat%' AND LENGTH(a.id) = ".(strlen($c1['id']) + 4 )." ",
                        null,
                        3
                    );

                    if( count($qattach) > 0 ){
                        $box2.='
                        <ul class="clearfix">';

                            foreach( $qattach as $iattach ){

                                $box2.='
                                <li>
                                    <a href="'.ROOT_PATH.$iattach['url'].'/" title="'.$iattach['ten'].'">
                                        '.$iattach['ten'].'
                                        <span class="more">
                                            <i class="fa fa-caret-right"></i>
                                        </span>
                                    </a>
                                </li>';
                            }

                        $box2.='</ul>';
                    }

                $box2.='
                </div><!-- /.attachment-body -->';
            }

        $box2.='
        </div><!-- /.attachment-container -->';
    }

    // VIDEO
    $box3 = '';
    $qvideo = $db->layDanhMuc(
        "video",
        " AND colvalue LIKE '%noibat%' ",
        null,
        1
    );

    if( count($qvideo) > 0 ){
        $box3.='
        <div class="video-container">';
            foreach( $qvideo as $video ){
                $box3.='
                <a href="'.ROOT_PATH.$video['url'].'/" title="'.$video['ten'].'">
                    <img src="'.ROOT_PATH.'uploads/danhmuc/thumb/'.$video['img'].'" alt="'.$video['ten'].'" class="img-responsive" />
                </a>';
            }
        $box3.='
        </div><!-- /.video-container -->';
    }

    // RENDER
    $data.= strtr('
    <!-- .home-a -->
    <section class="home-a clearfix form-group">

        <div class="ws-container">

            <!-- .inner -->
            <div class="inner clearfix">

                <!-- .highlight-group -->
                <div class="home-a-item highlight-group">
                    :box1
                </div><!-- /.highlight-group -->

                <!-- .tepdinhkem -->
                <div class="home-a-item tepdinhkem">
                    :box2
                </div><!-- /.tepdinhkem -->

                <!-- .video -->
                <div class="home-a-item video">
                    :box3
                </div><!-- /.video -->

            </div><!-- /.inner -->

        </div><!-- /.ws-container -->

    </section><!-- /.home-a -->',
    array(
        ':box1' => $box1,
        ':box2' => $box2,
        ':box3' => $box3
    ));

    $dataHome['homecat'] = $data;