<?php

$data_doitac = "";
    $data_doitac.='
 <div id="doitac-wrapper" class="clearfix">
    <div class="doitac-content clearfix">

    <div class="clearfix ws-container wow partner-container">
                 <div class="titlebase clearfix">
                   <span class="img"><i class="fa fa-user"></i></span>
                <h2>
                    <div class="subtitle"><a href="javascript:">
                       '.$arraybien['doitac'].'
                        <div class="line"></div>
                        </a>
                    </div>
                </h2>
            </div>
    ';
    $s_doitac = "SELECT a.id,b.ten,hinh,linkhinh,link,target,rong,cao
                FROM tbl_banner a INNER JOIN tbl_banner_lang b
                    ON a.id = b.idtype
                WHERE a.anhien = 1
                    AND a.loai = 3
                    AND b.idlang = {$_SESSION["_lang"]}
                ORDER BY thutu ASC";
    $d_doitac = $db->sqlSelectSql($s_doitac);
    if (count($d_doitac) > 0) {
        $data_doitac .= '
             <div class="doitacRun owl-carousel">';
            foreach ($d_doitac as $key_doitac => $value_doitac) {
                $doitac_id      = $value_doitac['id'];
                $doitac_ten     = $value_doitac['ten'];
                $doitac_hinh    = ($value_doitac['linkhinh'] != "") ? $value_doitac['linkhinh'] : ROOT_PATH . "uploads/logo/" . $value_doitac['hinh'];
                $doitac_lienket = ($value_doitac['link'] != "") ? $value_doitac['link'] : null;
                $doitac_target  = ($value_doitac['target'] != "") ? ' target="_blank" ' : null;
                $doitac_rong    = $value_doitac['rong'];
                $doitac_cao     = $value_doitac['cao'];
                $data_doitac .= '
                    <div class="item-doitac">
                       <a href="' . $doitac_lienket . '" title="' . $doitac_ten . '" ' . $doitac_target . '>
                          <img src="' . $doitac_hinh . '" class="img-responsive" alt="' . $doitac_ten . '" onerror="xulyloi(this)" />
                       </a>
                    </div>';
            }
        $data_doitac .= '</div>';
    }
    $data_doitac .= '</div></div></div>';
return $data_doitac;