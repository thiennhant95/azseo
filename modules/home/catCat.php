<?php

$valCatCat = '';
#--------------------------------------------------
#--------------------------------------------------
$sCat1 = "SELECT a.id,img,b.ten,url,link,target,mota
        FROM tbl_danhmuc AS a
           INNER JOIN tbl_danhmuc_lang AS b
           ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
        WHERE anhien = 1
           AND colvalue LIKE '%noibat%'
           AND keyname = 'datlichhen'
        ORDER BY a.thutu ASC
        LIMIT 20";
$dCat1 = $db->sqlSelectSql($sCat1);
if (count($dCat1) > 0) {
    $dataHome['cat1'] = $dCat1;
    foreach ($dCat1 as $kCat1 => $vCat1) {
        $cat1_id     = $vCat1['id'];

        $sCat2 = "SELECT a.id,img,b.ten,url,link,target,mota
                FROM tbl_danhmuc AS a
                   INNER JOIN tbl_danhmuc_lang AS b
                   ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
                WHERE anhien = 1
                   AND a.id LIKE '%{$cat1_id}%'
                   AND a.id != {$cat1_id}
                ORDER BY a.thutu ASC
                LIMIT 5";
        $dCat2 = $db->sqlSelectSql($sCat2);
        if (count($dCat2) > 0) {
            $dataHome[$kCat1]['cat2'] = $dCat2;
        }
    }
}

