<?php

    $ret = '';

    $q_new = $db->layDanhMuc([
        "op" => "content",
        "where" => "AND colvalue LIKE '%moi%'",
        "limit" => 4
    ]);

    if( count($q_new) > 0 ){

        // RENDER
        $ret.='
        <div class="tinmoi-container clearfix">
            <div class="ws-container">

                <h3 class="tinmoi-title">
                    <strong>'.$arraybien['tintuchoagiamoinhat'].'</strong>
                </h3><!-- /.tinmoi-title -->

                <div class="tinmoi-inner clearfix">';
                    foreach($q_new as $k_new => $new){

                        $ten = $new['ten'];
                        $mota = $new['mota'];
                        $link = ROOT_PATH . $new['url'];
                        $linkhinh = ROOT_PATH . "uploads/noidung/thumb/" . $new['hinh'];
                        if( ! $k_new ){
                            $linkhinh = ROOT_PATH . "uploads/noidung/" . $new['hinh'];
                        }

                        $ret.='
                        <div class="tinmoi-item clearfix '.(
                            ! $k_new ? 'first-box' : null
                        ).'">

                            <div class="image">
                                <a href="'.$link.'" title="'.$ten.'">
                                    <img src="'.$linkhinh.'" alt="'.$ten.'" class="img-responsive">
                                </a>
                            </div><!-- /.image -->

                            <div class="title">
                                <a href="'.$link.'" title="'.$ten.'">
                                    '.$ten.'
                                </a>
                            </div><!-- /.title -->

                            <div class="description">
                                '.$mota.'</div><!-- /.description -->

                        </div><!-- /.tinmoi-item -->';

                        if( $k_new == 0 ){
                            $ret.='
                            <div class="second-box">';
                        }

                    }
                $ret.='
                    </div><!-- /.second-box -->
                </div><!-- /.tinmoi-inner -->

            </div><!-- /.ws-container -->
        </div><!-- /.tinmoi-container -->';
    }

    $dataHome['tintuc_moinhat'] = $ret;