<?php
$valVideo = "";
#-----------------------------------------------------------
# VIDEO
#-----------------------------------------------------------
$v_video = '';
$sVideo = "
SELECT a.id,a.idtype,b.ten,link,file
FROM tbl_noidung AS a
INNER JOIN tbl_noidung_lang AS b
    ON a.id = b.idnoidung
WHERE b.idlang = {$_SESSION['_lang']}
    AND anhien = 1
    AND loai = 2
ORDER by thutu ASC
LIMIT 6";
$dVideo = $db->sqlSelectSql($sVideo);
if (count($dVideo) > 0) {
    $v_video.='<div class="video-container clearfix">
        <div class="titlebase clearfix">
            <a href="javascript:;" title="'.$arraybien['video'].'">
                <i class="fa fa-empire fa-1x"></i>
                '.$arraybien['video'].'
                <div class="line"></div>
            </a>
        </div>
        <div class="boxbase clearfix">';
        foreach ($dVideo as $kVideo => $vVideo) {
            $videoName = $vVideo['ten'];
            $videoLink = $vVideo['link'];
            $videoFile = ROOT_PATH.'uploads/noidung/'.$vVideo['file'];
            if (!$kVideo) {
                # VIDEO SHOW
                if (!empty($videoLink)) {
                    $keyVideo = end(explode("v=", $videoLink));
                    $v_video.='
                    <div id="framevideo" class="clearfix">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/'.$keyVideo.'?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0"></iframe>
                    </div>
                    ';
                } elseif (!empty($videoFile)) {
                    $v_video.='
                    <video width="100%" height="100%"  controls  poster="">
                        <source src="'.$videoFile.'" type="video/mp4">
                        <source src="'.$videoFile.'" type="video/ogg">
                        <source src="'.$videoFile.'" type="video/webm">
                        <object type="application/x-shockwave-flash" data="\/\/cdn.jsdelivr.net/jwplayer/5.10/player.swf" width="100%" height="350">
                           <param name="movie" value="\/\/cdn.jsdelivr.net/jwplayer/5.10/player.swf" />
                           <param name="allowFullScreen" value="true" />
                           <param name="wmode" value="transparent" />
                           <param name="flashVars" value="controlbar=over&amp;file='.$videoFile.'" />
                           <span title="No video playback capabilities, please download the video below">September 2013 U-RUN</span>
                        </object>
                     </video>';
                }
            $v_video.='<div class="video-group-list clearfix">';
            }
            $v_video.='
            <div class="video-list '.($kVideo==0?"active":null).'" id="video_list">
                <a data-link="https://www.youtube.com/embed/'.end(explode("v=", $videoLink)).'?rel=0&amp;controls=1&amp;showinfo=0" title="'.$videoName.'">'.$videoName.'</a>
            </div>';
        }
    $v_video.='</div><!-- video-group-list -->
    </div><!-- boxbase -->
    </div>';
}

#---------------------------------------------------------
# TAI SAO CHON CHUNG TOI
#---------------------------------------------------------
$v_why = '';
#--------------------------------------------------
# QUERY why1
#--------------------------------------------------
$swhy1 = "SELECT a.id,img,b.ten,url,link,target,mota
        FROM tbl_danhmuc AS a
           INNER JOIN tbl_danhmuc_lang AS b
           ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
        WHERE anhien = 1
           AND colvalue LIKE '%home%'
           AND keyname = 'taisaochon'
        ORDER BY a.thutu ASC
        LIMIT 6";
$dwhy1 = $db->sqlSelectSql($swhy1);
if (count($dwhy1) > 0) {
    $v_why.='<div class="why-container clearfix">';
    #--------------------------------------------------
    # LOOP why1
    #--------------------------------------------------
        $vwhy1 = $dwhy1[0];
        $why1_id     = $vwhy1['id'];
        $why1_name   = $vwhy1['ten'];
        $why1_des    = $vwhy1['mota'];
        $why1_link   = !empty($vwhy1['link']) ? $vwhy1['link'] : ROOT_PATH.$vwhy1['url'].'/';
        $why1_target = !empty($vwhy1['target']) ? 'target="'.$vwhy1['target'].'"' : null;
        #---------------------------------------------------
        # ECHO why1
        #---------------------------------------------------
        $v_why.= '
        <div class="titlebase clearfix">
            <a href="javascript:;" title="'.$why1_name.'">
                <i class="fa fa-empire fa-1x"></i>
                '.$why1_name.'
                <div class="line"></div>
            </a>
        </div>
        <div class="boxbase clearfix">
        <ul class="why-content clearfix">';
            #--------------------------------------------
            # QUERY why2
            #--------------------------------------------
            $swhy2 = "SELECT a.id,img,b.ten,url,link,target,mota
                    FROM tbl_danhmuc AS a
                       INNER JOIN tbl_danhmuc_lang AS b
                       ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
                    WHERE anhien = 1
                       AND a.id LIKE '%{$why1_id}%'
                       AND a.id != {$why1_id}
                       AND LENGTH(a.id) = ".(strlen($why1_id) + 4)."
                    ORDER BY a.thutu ASC";
            $dwhy2 = $db->sqlSelectSql($swhy2);
            if (count($dwhy2) > 0) {
                #----------------------------------------
                # LOOP CAT 2
                #----------------------------------------
                foreach ($dwhy2 as $kwhy2 => $vwhy2) {
                    $why2_id     = $vwhy2['id'];
                    $why2_name   = $vwhy2['ten'];
                    $why2_des   = $vwhy2['mota'];
                    $why2_link   = !empty($vwhy2['link']) ? $vwhy2['link'] : ROOT_PATH.$vwhy2['url']."/";
                    $why2_target = !empty($vwhy2['target']) ? 'target="'.$vwhy2['target'].'"' : null;
                    $why2_img    = !empty($vwhy2['img']) ? ROOT_PATH."uploads/danhmuc/".$vwhy2['img'] : null;

                    #-----------------------------------
                    # ECHO why2
                    #-----------------------------------
                    $v_why.='
                    <li>
                        <a href="javascript:;" title="'.$why2_name.'">
                            '.$why2_name.'
                        </a>
                        <div class="why-des">
                            '.$why2_des.'
                        </div>
                    </li>';
                }
            }
        $v_why.='</ul>
            </div>';
    $v_why.='</div><!-- ./why-container-->';
}
#------------------------------------------
# TEMPLATES
#------------------------------------------
$valVideo.='
    <div class="videowhy ws-container form-group clearfix">
        <div class="videowhy-content clearfix">
            <div class="videowhy-item clearfix video">
                '.$v_video.'
            </div>
            <div class="videowhy-item clearfix why">
                '.$v_why.'
            </div>
        </div>
        <!-- ./videowhy-content -->
    </div>
    <!-- ./videowhy -->
';
$dataHome["videowhy"] = $valVideo;
