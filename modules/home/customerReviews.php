<?php
$valCusIdea = '';
$rowkh=1;
#---------------------------------
# QUERY CAT WITH KEYNAME -> ykienkhachhang
#---------------------------------
$type_menu = 'customerreviews';
$op_menu = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'$type_menu'");
$s_cat_idea = "SELECT b.id,a.iddanhmuc,ten,url,link,target,bgcolor,color,icon
FROM tbl_danhmuc_lang AS a
INNER join tbl_danhmuc AS b ON a.iddanhmuc = b.id
WHERE b.anhien = 1
AND a.idlang = {$_SESSION['_lang']}
AND b.idtype LIKE '%{$op_menu}%'
ORDER by thutu ASC";
$d_cat_idea = $db->sqlSelectSql($s_cat_idea);
// echo '<pre>'; print_r($d_cat_idea); echo '</pre>'; exit();

if (count($d_cat_idea) > 0) {
    $valCusIdea.='
    <div class="group_bottom_fix" '.$bgykienkhachhang.'>';
    $v_cat_idea = $d_cat_idea[0];
    $idea_id    = $v_cat_idea['id'];

    // TRUY VẤN LẤY NỘI DUNG Ý KIẾN KHÁCH HÀNG
    $s_cont_idea = "
    SELECT a.id,a.hinh,masp,gia,giagoc,a.idtype,ten,url,link,target,mota,noidung,hinh
    FROM tbl_noidung AS a
    INNER JOIN tbl_noidung_lang AS b
    ON a.id = b.idnoidung
    WHERE a.anhien = 1
    AND b.idlang = {$_SESSION['_lang']}
    AND a.loai = ".array_search("customerreviews", $_arr_loai_noidung)."
    AND a.idtype LIKE '%{$idea_id}%'
    ORDER by a.thutu Asc
    LIMIT 120";
    $d_cont_idea = $db->sqlSelectSql($s_cont_idea);

    if (count($d_cont_idea) > 0) {
        $valCusIdea.='
        <div class="contentfix">
        <div class="customer-idea-content clearfix">
        <div class="ul_ykienkhachhang1 ykienkhachhangRun owl-carousel">';
        if($rowkh>1){
            $valCusIdea.='<div class="rowkh">';
        }
        foreach ($d_cont_idea as $k_cont_idea => $v_cont_idea) {
            $cont_id   = $v_cont_idea['id'];
            $cont_name = $v_cont_idea['ten'];
            $cont_img  = ROOT_PATH."uploads/noidung/thumb/".$v_cont_idea['hinh'];
            $cont_ser  = $v_cont_idea['mota'];
            $cont_idea = $v_cont_idea['noidung'];
            $valCusIdea.='
            <div class="clearfix wow animated zoomIn" data-wow-delay=".'.$k_cont_idea.'s">
            <div class="cus-item cus-idea">
            <div class="profile">
            <span class="idea">"'.$cont_idea.'"</span>
            <span class="cus-ser"><p>'.$cont_name.'</p> - '.$cont_ser.'</span>
            <span class="img"><img src="'.$cont_img.'" title="'.$cont_name.'"/></span>
            </div>
            </div>
            </div>';
            if(($k_cont_idea+1) % $rowkh ==0 && $rowkh>1)
            {
                $valCusIdea.='</div><div class="rowkh'.$k_cont_idea.'">';
            }
        }
        if($rowkh>1){
            $valCusIdea.='</div>';
        }
        $valCusIdea.='
        </div>
        </div>
        </div>
        <!-- ./customer-idea-content -->';
    }
    $valCusIdea.='
    </div>';
}
return $valCusIdea;
