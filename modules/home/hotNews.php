<?php
$s_tin_noibat = " SELECT a.ngaycapnhat,ten,url,link,target,mota
              FROM tbl_noidung AS a
              INNER JOIN tbl_noidung_lang AS b
              ON a.id = b.idnoidung
              where a.anhien = 1
              and b.idlang = '" . $_SESSION['_lang'] . "'
              and a.noibat  = 1
              order by thutu Asc
              limit 0,3";
$d_tin_noibat   = $db->sqlSelectSql($s_tin_noibat);
$data_tinnoibat = '';
if (count($d_tin_noibat) > 0) {
    $data_tinnoibat .= '<div class="group_tinnoibat">';
    $data_tinnoibat .= '<ul>';
    foreach ($d_tin_noibat as $key_tinnoibat => $info_tinnoibat) {
        $ten         = $info_tinnoibat['ten'];
        $ngaycapnhat = $info_tinnoibat['ngaycapnhat'];
        $mota        = $info_tinnoibat['mota'];
        $url         = $info_tinnoibat['url'];
        $link        = $info_tinnoibat['link'];
        $target      = $info_tinnoibat['target'];
        $lienket     = ROOT_PATH . $url;
        if ($link != '') {
            $lienket      = $link;
            $target_value = ' target="' . $target . '" ';
        }
        $data_tinnoibat .= '<li>
         <div class="itemnoidung">
            <p class="tieude"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" />' . $ten . '</a></p>
            <span class="mota">' . $mota . '</span>
            <div class="xemchitiet"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" />' . $arraybien['xemchitiet'] . '</a></div>
            <div class="clear"></div>
         </div></li>';
    }
    $data_tinnoibat .= '</ul>
            </div>';
}
$s_tin_noibat_slide = " select a.hinh,a.ngaycapnhat,ten,url,link,target,mota
              FROM tbl_noidung AS a
              INNER JOIN tbl_noidung_lang AS b
              ON a.id = b.idnoidung
              where a.anhien = 1
              and b.idlang = '" . $_SESSION['_lang'] . "'
              and a.noibat  = 1
              order by thutu Asc
              limit 4,30";
$d_tin_noibat_slide = $db->sqlSelectSql($s_tin_noibat_slide);
if (count($d_tin_noibat_slide) > 0) {
    $data_slide_owl1 = '';
    $data_slide_owl2 = '';
    foreach ($d_tin_noibat_slide as $key_tinnoibat => $info_tinnoibat_slide) {
        $ten         = $info_tinnoibat_slide['ten'];
        $ngaycapnhat = $info_tinnoibat_slide['ngaycapnhat'];
        $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_tinnoibat_slide['hinh'];
        $mota        = $info_tinnoibat_slide['mota'];
        $url         = $info_tinnoibat_slide['url'];
        $link        = $info_tinnoibat_slide['link'];
        $target      = $info_tinnoibat_slide['target'];
        $lienket     = ROOT_PATH . $url;
        if ($link != '') {
            $lienket      = $link;
            $target_value = ' target="' . $target . '" ';
        }
        $data_slide_owl1 .= '<div class="item">
         <div class="home_tinslide">
            <div class="img"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" /><img onerror="xulyloi(this);" src="' . $hinh . '" alt="' . $ten . '" /></a></div>
            <p class="tieude"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" />' . $ten . '</a></p>
            <span class="mota">' . $mota . '</span>
            <div class="clear"></div>
         </div></div>';
        $data_slide_owl2 .= '<div class="item">
                     <img onerror="xulyloi(this);" src="' . $hinh . '" alt="' . $ten . '" />
                     </div>';
    }
}
$data_tinnoibat .= '<div class="group_tinnoibat_slide">';
$data_tinnoibat .= '<div id="demo">
            <div class="span12">
              <div id="sync1" class="owl-carousel">
            ' . $data_slide_owl1 . '
              </div>
              <div id="sync2" class="owl-carousel">
            ' . $data_slide_owl2 . '
              </div>
            </div>
    </div>';
$data_tinnoibat .= '</div>';
return $data_tinnoibat;
