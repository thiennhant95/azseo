<?php
/*
 */
// phan trang
$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongsanpham']; //limit in each page
$perpage    = 6;
$startpoint = ($page * $perpage) - $perpage;

$d_noidung = $db->layDanhMuc(
    $_op,
    " AND a.idtype like '%{$__idtype_danhmuc}%' ",
    null,
    array($startpoint,$perpage)
);

$sql_page = "SELECT COUNT(*) as num
            FROM  tbl_noidung As a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND a.idtype LIKE '%{$__idtype_danhmuc}%'
                AND b.idlang = {$_SESSION['_lang']} ";

$data_noidung = '<h1 class="title">
                    <a href="' . ROOT_PATH . $_getcat . '/" title="' . $_ten . '">'     .$_ten.'
                    </a>
                </h1>';

if ($_array_config_video['share'] == 1) {
    $data_noidung .= include "plugin/share/share.php";
    $data_noidung .= '<div class="clear"></div>';
}

if (count($d_noidung) > 0) {
    $arrdata['video'] = $d_noidung;
}

// do du lieu vao tpl
$smarty->assign("arrdata",$arrdata,true);
$smarty->assign("phantrang",$phantrang,true);
$smarty->assign("flow", $flow);
