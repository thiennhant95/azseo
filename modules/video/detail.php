<?php
$s_noidung = "SELECT a.id,a.idtype,hinh,solanxem,ngaycapnhat,b.ten,url,link,target,mota,noidung,tag,file
           FROM tbl_noidung AS a
           INNER JOIN tbl_noidung_lang AS b
           ON a.id = b.idnoidung
           where b.idlang = '" . $_SESSION['_lang'] . "'
           and url = '" . $_geturl . "'
           and anhien = 1
           and loai = 2
           order by thutu ASC ";
$d_noidung            = $db->sqlSelectSql($s_noidung);
$data_noidung_chitiet = '';
if (count($d_noidung) > 0) {
    $d_noidung   = $d_noidung[0];
    $ten         = $d_noidung['ten'];
    $id          = $d_noidung['id'];
    $idtype      = $d_noidung['idtype'];
    $hinh        = $d_noidung['hinh'];
    $solanxem    = $d_noidung['solanxem'];
    $url         = $d_noidung['url'];
    $link        = $d_noidung['link'];
    $file        = ROOT_PATH . 'uploads/noidung/' . $d_noidung['file'];
    $target      = $d_noidung['target'];
    $mota        = $d_noidung['mota'];
    $noidung     = $d_noidung['noidung'];
    $tag         = $d_noidung['tag'];
    $ngaycapnhat = $d_noidung['ngaycapnhat'];
    $ngayhientai = $db->getDateTimes();
    $arraylabel  = array("ngay" => " " . $arraybien['ngay'] . " ",
        "ngaytruoc"                 => " " . $arraybien['ngaytruoc'] . " ",
        "gio"                       => " " . $arraybien['gio'] . " ",
        "phut"                      => " " . $arraybien['phut'] . "",
        "luc"                       => "" . $arraybien['luc'] . " ",
        "cachday"                   => "" . $arraybien['cachday'] . " ");
    $data_noidung_chitiet .= '<div class="noidungchitiet">';
    $data_noidung_chitiet .= '<h1 class="title">' . $ten . '</h1>';
    if ($_array_config_video['ngay'] == 1) {
        $data_noidung_chitiet .= '<div class="ngaycapnhat">' . $db->ngaydang($ngayhientai, $ngaycapnhat, $arraylabel) . '</div>';
    }
    if ($_array_config_video['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }
    $data_noidung_chitiet .= '<div class="clear"></div>';
    $data_noidung_chitiet .= '<div class="mota">' . $mota . '</div>';
    // playvideo
    if ($link != '') {
        $explodearr = explode("=", $link);
        $data_noidung_chitiet .= '<iframe width="100%" height="350" src="https://www.youtube-nocookie.com/embed/' . $explodearr[1] . '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
    } else if ($d_noidung['file'] != '') {
        $data_noidung_chitiet .= '<video width="100%" height="300"  controls  poster="">
      <source src="' . $file . '" type="video/mp4">
      <source src="' . $file . '" type="video/ogg">
      <source src="' . $file . '" type="video/webm">
      <object type="application/x-shockwave-flash" data="http://player.longtailvideo.com/player.swf" width="100%" height="350">
         <param name="movie" value="http://player.longtailvideo.com/player.swf" />
         <param name="allowFullScreen" value="true" />
         <param name="wmode" value="transparent" />
         <param name="flashVars" value="controlbar=over&amp;file=' . $file . '" />
         <span title="No video playback capabilities, please download the video below">September 2013 U-RUN</span>
      </object>
   </video>';
    }
    $data_noidung_chitiet .= '<div class="noidung">' . $noidung . '</div>';
    $data_noidung_chitiet .= '<div class="clear"></div>';
    if ($_array_config_video['commentfacebook'] == 1) {
        $data_noidung_chitiet .= '
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=401401003393108";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, \'script\', \'facebook-jssdk\'));</script>
         <div class="fb-comments" data-href="http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] . '"
      data-width="100%" data-numposts="5" data-colorscheme="light"></div>';
    }
    if ($_array_config_video['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }
    $data_noidung_chitiet .= '<div class="clear"></div><br />';
    // lay tin lien quan
    if ($_array_config_video['tinlienquan'] == 1) {
        $s_noidung_lienquan = "select a.hinh,a.ngaycapnhat,ten,tieude,url,link,target,mota
                       FROM tbl_noidung AS a
                       INNER JOIN tbl_noidung_lang AS b
                       ON a.id = b.idnoidung
                       where a.anhien = 1
                       and a.idtype like '%" . $__idtype_danhmuc . "%'
                       and b.idlang = '" . $_SESSION['_lang'] . "'
                       and url != '" . $url . "'
                       order by thutu Asc
                       limit 0," . $_array_config_video['sotinlienquan'] . " ";
        $d_noidung_lienquan = $db->sqlSelectSql($s_noidung_lienquan);
        if (count($d_noidung_lienquan) > 0) {
            $data_noidung_chitiet .= '<div class="title_tinlienquan"><i class="fa fa-chevron-circle-right"></i>' . $arraybien['phimcungchuyenmuc'] . '<h2></h2></div>';
            if ($_array_config_video['tinlienquan_thumb'] == 1) // if thumb
            {
                foreach ($d_noidung_lienquan as $key_noidung_lienquan => $info_lienquan) {
                    $ten         = $info_lienquan['ten'];
                    $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_lienquan['hinh'];
                    $ngaycapnhat = $info_lienquan['ngaycapnhat'];
                    $tieude      = $info_lienquan['tieude'];
                    $url         = $info_lienquan['url'];
                    $link        = $info_lienquan['link'];
                    $target      = $info_lienquan['target'];
                    $mota        = $info_lienquan['mota'];
                    $noidung     = $info_lienquan['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    $data_noidung_chitiet .= '<div class="itemvideo">
               <div class="img"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" /><img onerror="xulyloi(this);" src="' . $hinh . '" alt="' . $ten . '" /></a></div>
               <div class="tieude"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" />' . $ten . '</a></div>
               <div class="clear"></div>
            </div>';
                }
            } else if ($_array_config_video['tinlienquan_thumb'] == 0) {
                // end if thumb
                $data_noidung_chitiet .= '<ul class="tinlienquan">';
                foreach ($d_noidung_lienquan as $key_noidung_lienquan => $info_lienquan) {
                    $ten         = $info_lienquan['ten'];
                    $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_lienquan['hinh'];
                    $ngaycapnhat = $info_lienquan['ngaycapnhat'];
                    $tieude      = $info_lienquan['tieude'];
                    $url         = $info_lienquan['url'];
                    $link        = $info_lienquan['link'];
                    $target      = $info_lienquan['target'];
                    $mota        = $info_lienquan['mota'];
                    $noidung     = $info_lienquan['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_noidung_chitiet .= '<li><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" /><i class="fa fa-angle-right fa-lg"></i> ' . $ten . '</a></li>';
                }
                $data_noidung_chitiet .= '</ul>';
            }
        }
    }
    $data_noidung_chitiet .= '<div class="clear"></div></div>'; // end div noi dung chi tiet
} else {
    $data_noidung_chitiet .= include "application/files/404.php";
}
return $data_noidung_chitiet;
