<?php
// phan trang
$phantrang =  '';
$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongtin']; //limit in each page
$startpoint = ($page * $perpage) - $perpage;

$sql_page = "SELECT COUNT(*) as num
            FROM  tbl_noidung As a
            INNER JOIN tbl_noidung_lang AS b
            ON a.id = b.idnoidung
            WHERE a.anhien = 1
            and b.idlang = {$_SESSION['_lang']}
            and a.idtype LIKE '%$__idtype_danhmuc%'
            and loai = ".array_search($_op, $_arr_loai_noidung)." ";

$d_noidung = $db->layNoiDung([
    "op" => $_op,
    "select" => "ngay",
    "where" => "AND a.idtype LIKE '%{$__idtype_danhmuc}%'
                AND anhien = 1
                AND idlang = {$_SESSION['_lang']}",
    "limit" => [$startpoint, $perpage]
]);
$arrdata = array();
if (count($d_noidung) > 0) {
    $arrdata['dichvu'] = $d_noidung;
}
$url_page = ROOT_PATH . $_getcat . '-trang-';
$phantrang .= Pages($sql_page, $perpage, $url_page);
/*Nếu chưa khai báo chế độ xem thì mặc định là grid view*/
if (!isset($_SESSION['flowND'])) {
    $_SESSION['flowND'] = 1;
}
$flowND = '';
$flowND .= '
<div class="type-view clearfix">
    <ul>
        <li>';
            if (isset($_SESSION['flowND']) && $_SESSION['flowND'] == 2) {
                $flowND .= '<a href="'.ROOT_PATH.'ajax/?method=flowND&value=1" title="">';
            }
            $flowND .= '<i class="fa fa-th fa-lg" aria-hidden="true"></i>';
            if (isset($_SESSION['flowND']) && $_SESSION['flowND'] == 2) {
                $flowND .= '</a>';
            }
            $flowND .= '
        </li>
        <li>';
            if (isset($_SESSION['flowND']) && $_SESSION['flowND'] != 2) {
                $flowND .= '<a href="'.ROOT_PATH.'ajax/?method=flowND&value=2" title="">';
            }
            $flowND .= '<i class="fa fa-th-list fa-lg" aria-hidden="true"></i>';
            if (isset($_SESSION['flowND']) && $_SESSION['flowND'] != 2) {
                $flowND .= '</a>';
            }$flowND .= '
        </li>
    </ul>
</div>';
// do du lieu vao tpl
$smarty->assign("arrdata", $arrdata);
$smarty->assign("phantrang", $phantrang);
$smarty->assign("flowND", $flowND);
