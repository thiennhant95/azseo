<?php

$d_noidung = $db->layNoiDung([
    "op" => $_op,
    "where" => "AND url = '{$_geturl}'
                AND anhien = 1
                AND idlang = {$_SESSION['_lang']}"
]);

$data_noidung_chitiet = '';
if (count($d_noidung) > 0) {
    $d_noidung   = $d_noidung[0];
    $ten         = $d_noidung['ten'];
    $id          = $d_noidung['id'];
    $idsp        = $d_noidung['id'];
    $idtype      = $d_noidung['idtype'];
    $hinh        = $d_noidung['hinh'];
    $solanxem    = $d_noidung['solanxem'];
    $url         = $d_noidung['url'];
    $link        = $d_noidung['link'];
    $target      = $d_noidung['target'];
    $mota        = $d_noidung['mota'];
    $noidung     = $d_noidung['noidung'];
    $tag         = $d_noidung['tag'];
    $ngaycapnhat = $d_noidung['ngaycapnhat'];
    $ngayhientai = $db->getDateTimes();
    $arraylabel  = array(
        "ngay"      =>  $arraybien['ngay'],
        "ngaytruoc" =>  $arraybien['ngaytruoc'],
        "gio"       =>  $arraybien['gio'],
        "phut"      =>  $arraybien['phut'],
        "luc"       =>  $arraybien['luc'],
        "cachday"   =>  $arraybien['cachday'],
    );
    $data_noidung_chitiet .= '<div class="noidungchitiet">';
    if ($_array_config_noidung['ngay'] == 1) {
        $data_noidung_chitiet .= '<div class="ngaycapnhat clearfix form-group">' . $db->ngaydang($ngayhientai, $ngaycapnhat, $arraylabel) . '</div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }

    /* NỘI DUNG ĐƯỢC HIỂN THỊ RA ĐÂY */
   // $data_noidung_chitiet.= '<div class="mota">'.$mota.'</div>';
    $data_noidung_chitiet.=$noidung;

    $luuynoidung = $db->getThongTin("luuynoidung");
    $data_noidung_chitiet.='<div class="luuysanpham">'.$luuynoidung.'</div>';

    $data_noidung_chitiet .= '<h2 class="hide">'.$ten.'</h2>';
// update so lan xem
if($_SESSION['countnoidung'][$id]==''){
    $s_update_solanxem = "update tbl_noidung set solanxem = solanxem+1 where id = '".$id."' ";
    $d_update_solanxem = $db->rawQuery($s_update_solanxem);
    $_SESSION['countnoidung'][$id]=$id;
}

    // tag
    $tagarr     = '';
    $tagcontent = '';
    $tag        = trim($tag);
    $tagcontent = '';
    $data_noidung_chitiet.= get_tag($tag, 'tags', '.tag');
    $data_noidung_chitiet .= $tagcontent;
    $data_noidung_chitiet .= '<div class="clear"></div>';

    if( $_array_config_sanpham['share'] == 1 || 1 ){
        $data_noidung_chitiet.='
        <div class="row_chitiet" style="float:right; display:table;">
            <div class="ct_label">' .$arraybien['chiase'].':</div>
            <div class="ct_noidung">';
        $data_noidung_chitiet .= include 'plugins/share/share.php';$data_noidung_chitiet.='
        </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>';
    }

    // $data_noidung_chitiet .= include "modules/dathang/index.php";

    if ( $_getArrayconfig['binhluanwebsite'] == 'on' ) {
        $data_noidung_chitiet.='<div role="tabpanel" class="tab-pane panel-body" id="comment">';
        // if ($_array_config_sanpham['commentform']) {
            // nhum comment
            $data_noidung_chitiet.='
            <div class="ws-container">';
            $data_noidung_chitiet.= include'modules/comment/index.php';
            $data_noidung_chitiet.= ' </div>
                <!-- .ws-container -->';
        // }else{
        //     $data_noidung_chitiet.='
        //     <div role="tabpanel" class="tab-pane panel-body" id="comment">

        //         <script>(function(d, s, id) {
        //             var js, fjs = d.getElementsByTagName(s)[0];
        //             if (d.getElementById(id)) return;
        //             js = d.createElement(s); js.id = id;
        //             js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=401401003393108";
        //             fjs.parentNode.insertBefore(js, fjs);
        //         }(document, \'script\', \'facebook-jssdk\'));</script>
        //             <div class="fb-comments" data-href="http://' .$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'].'"
        //         data-width="100%" data-numposts="5" data-colorscheme="light"></div>

        // </div>';
        // }
        $data_noidung_chitiet.='</div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }
    $data_noidung_chitiet .= '<div class="clear"></div><br />';

    // lay tin lien quan
    if ( $_array_config_noidung['tinlienquan'] ) {

            $noidunglienquan_title .= '
            <div class="titlebase clearfix">
                <h2>
                    <a href="javascript:">
                        <div class="img"><i class="fa fa-renren"></i></div>
                        <div class="tentieude">'.$arraybien['noidungcungchuyenmuc'].'</div>
                    </a>
                </h2>
            </div>
            ';

            $smarty->assign('noidunglienquan_title', $noidunglienquan_title);
            $smarty->assign('noidunglienquan', $db->layNoiDung([
                "op" => $_op,
                "where" => "AND a.idtype like '%{$__idtype_danhmuc}%'
                            AND url != '{$url}'
                            AND anhien = 1
                            AND idlang = {$_SESSION['_lang']}",
                "limit" => [0, $_array_config_noidung['sotinlienquan']],
                // "debug" =>true
            ]));
    }
    $data_noidung_chitiet .= '<div class="clear"></div></div>'; // end div noi dung chi tiet
} else {
    $data_noidung_chitiet .= include "application/files/404.php";
}
$chitietnoidung = $data_noidung_chitiet;

// do du lieu vao tpl
$smarty->assign('chitietnoidung', $chitietnoidung);
