<?php
// phan trang
$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongsanpham']; //limit in each page
$startpoint = ($page * $perpage) - $perpage;
$s_noidung  = "select a.id,a.hinh,a.idtype,a.ngaycapnhat,ten,url,link,target,mota
           FROM tbl_noidung AS a
           INNER JOIN tbl_noidung_lang AS b
           ON a.id = b.idnoidung
           where a.anhien = 1 ";
if ($_GET['tag'] != '') {
    $s_noidung .= "
            and b.tag like '%" . $_GET['tag'] . "%'
            and b.tag != '' ";
}
$s_noidung .= "
           and b.idlang = '" . $_SESSION['_lang'] . "'
           and loai = 0
           order by thutu Asc
           limit $startpoint,$perpage ";
$sql_page = "select COUNT(*) as num
         from  tbl_noidung As a
         INNER JOIN tbl_noidung_lang AS b
         ON a.id = b.idnoidung
         where a.anhien = 1 ";
if ($_GET['tag'] != '') {
    $sql_page .= "
            and b.tag like '%" . $_GET['tag'] . "%'
            and b.tag != '' ";
}
$sql_page .= "
         and b.idlang = '" . $_SESSION['_lang'] . "'
         and loai = 0 ";
$d_noidung = $db->sqlSelectSql($s_noidung);
$arrdata = array();
if (count($d_noidung) > 0) {
  $arrdata['dichvu'] = $d_noidung;
}
$url_page = ROOT_PATH . ws_get('urloption') . '.tags-trang-';
$phantrang .= Pages($sql_page, $perpage, $url_page);

$d_noidung    = $db->sqlSelectSql($s_noidung);
$data_noidung = '<h1 class="title"><a href="' . ROOT_PATH . $_getcat . '/" title="' . $_ten . '">' . $_ten . '</a></h1>';
if ($_array_config_noidung['share'] == 1) {
    $data_noidung .= include "plugin/share/share.php";
}
if (count($d_noidung) > 0) {
    foreach ($d_noidung as $key_noidung => $info_noidung) {
        $ten         = $info_noidung['ten'];
        $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_noidung['hinh'];
        $ngaycapnhat = $info_noidung['ngaycapnhat'];
        $url         = $info_noidung['url'];
        $link        = $info_noidung['link'];
        $target      = $info_noidung['target'];
        $mota        = $info_noidung['mota'];
        $noidung     = $info_noidung['noidung'];
        $lienket     = ROOT_PATH . $url;
        if ($link != '') {
            $lienket      = $link;
            $target_value = ' target="' . $target . '" ';
        }
        $data_noidung .= '<div class="itemnoidung">
         <div class="img"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '"><img onerror="xulyloi(this);" src="' . $hinh . '" alt="' . $ten . '" /></a></div>
         <p class="tieude"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '">' . $ten . '</a></p>
         <span class="mota">' . $mota . '</span>
         <div class="xemchitiet"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '">' . $arraybien['chitiet'] . '</a></div>
         <div class="clear"></div>
      </div>';
    }
    $url_page = ROOT_PATH . $_getcat . '-trang-';
    $data_noidung .= Pages($sql_page, $perpage, $url_page);
}
// do du lieu vao tpl
$braucube = $db->createListLink($__idtype_danhmuc,ROOT_PATH);
$smarty->assign("braucube",$braucube,true);
$smarty->assign("arrdata",$arrdata,true);
$smarty->assign("phantrang",$phantrang,true);
$smarty->assign("flow", $flow);
