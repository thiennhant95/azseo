	$.ajaxSetup({
		cache: false
	});
	/** Search Quick */
	// $.removeCookie("locksearch");
	if ($.cookie("locksearch") == "yes") {
		$('#keyword-quicksearch').val('search will run again the tomorrow');
		$('#keyword-quicksearch').addClass('disabled').attr('disabled','disabled');
	}
	var solanbam = 1;
	$('#keyword-quicksearch').focus(function() {
		$('.qs-content').addClass('focused');
		$('.search-input').append('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
	});
	$('#keyword-quicksearch').focusout(function() {
		$('.search-input').removeClass('loading');
		$('#keyword-quicksearch').siblings().remove();
		setTimeout(function() {
			$('#suggest').removeClass('haschild');
			$('.qs-content').removeClass('focused');
			$('#suggest').find('li').remove();
		},1000);
	});
	$("#keyword-quicksearch").on('keyup', function (e) {
		if (!$('.rootpath').data('url')) {
			alert('Vui lòng thiết lập RootPath');
		} else {
			var urlQS = $('.rootpath').data('url');
			var keyword = $(this).val();
			var url = urlQS+'modules/quickSearch/quicksearch.php';
			if (!$('#websoloading').length) {
				alert('Vui lòng thiết lập Loading...');
			}

			if (!$('.qs-content').data()) {
				$('.qs-content').attr('data-countsearch',solanbam);
			} else {
				$('.qs-content').attr('data-countsearch', solanbam += 1);
			}
			if (solanbam >= 95 && solanbam <= 99) {
				swal('Vui lòng không spam','','warning');
			} else if (solanbam == 100) {
				$('#suggest').remove();
				swal('Search bị vô hiệu hóa vì spam','','error');
				$('#keyword-quicksearch').val('Please Press F5');
				$('#keyword-quicksearch').attr('disabled','disabled');

				$.cookie("locksearch", "yes", { expires: 1 });
			}
			if ($.trim(keyword) != '' && $.cookie("locksearch") != "yes") {
				$('.search-input').addClass('loading');
				$.get( url, { tukhoa: keyword, action: "timnhanh"}, function(data_quicksearch) {
					if (data_quicksearch != "") {
						$('#suggest').addClass('haschild');
					}else {
						$('#suggest').removeClass('haschild');
					}
					$('#suggest>ul').html(data_quicksearch);
					setTimeout(function() {
						$('.search-input').removeClass('loading');
					}, 1000);
				});
			}
		}
	});