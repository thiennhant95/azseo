<?php
$tukhoacantim = $_GET['tukhoa'];
$hanhdong     = $_GET['action'];
$return_quick = "";
if ($tukhoacantim != "" && $hanhdong == 'timnhanh') {
	$s_qs = "SELECT a.id,b.ten,gia,hinh,url,link
			FROM tbl_noidung a
			INNER JOIN tbl_noidung_lang b
				ON a.id = b.idnoidung AND b.idlang = '" . $_SESSION['_lang'] . "'
			WHERE a.anhien = 1
				AND a.loai = 1
				AND b.ten LIKE '%" . $tukhoacantim . "%'
			ORDER BY b.ten DESC";
    $sd_quick = $db->sqlSelectSql($s_qs);
    if (count($sd_quick) > 0) {
        foreach ($sd_quick as $key_quick => $value_quick) {
            $quick_id      = $value_quick['id'];
            $quick_ten     = $value_quick['ten'];
            $quick_gia     =  number_format($value_quick['gia'],0,',','.').'₫';
            $quick_lienket = ($value_quick['link'] != "") ? $value_quick['link'] : $_SESSION['ROOT_PATH'].$value_quick['url'];
            $quick_hinh    = $_SESSION['ROOT_PATH'].'uploads/noidung/'.$value_quick['hinh'];

            $return_quick.='
            <li>
               <a href="'.$quick_lienket.'" title="'.$quick_ten.'">
                  <img src="'.$quick_hinh.'" alt="'.$quick_ten.'">
                  <span>'.$quick_ten.'</span>
                  <span class="gia">'.$quick_gia.'</span>
               </a>
            </li>';
        }
       echo $return_quick;
    } else {
      echo '<li><a href="#" title="">No results found</a></li>';
    }
} else {
   echo '';
}
