<?php
$exportlienhe = null;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $tendaidien   = $_POST['tendaidien'];
    $website      = $_POST['website'];
    $loaihoatdong = $_POST['loaihoatdong'];
    $hotro        = $_POST['hotro'];
    $bietden      = $_POST['bietden'];
    $freeoragen   = $_POST['freeoragen'];
    $loaidoitac   = $_POST['loaidoitac'];
    $hoten        = $_POST['hoten'];
    $email        = $_POST['email'];
    $sodienthoai  = $_POST['sodienthoai'];
    $diachi       = $_POST['diachi'];

    $arrloi    = array();
    $strloi    = "";
    if ($hoten == '') {
        $arrloi[] = 'nullhoten';
        $strloi   = $arraybien['banchuanhaphoten'];
    } elseif (strlen($hoten) < 2) {
        $arrloi[] = 'shorthoten';
        $strloi   = $arraybien['tenquanganvuilongnhaplai'];
    } elseif ($sodienthoai == '') {
        $arrloi[] = 'nulldienthoai';
        $strloi   = $arraybien['banchuanhapdienthoai'];
    } elseif (!is_numeric($sodienthoai)) {
        $arrloi[] = 'intdienthoai';
        $strloi   = $arraybien['dienthoaiphailaso'];
    } elseif (strlen($sodienthoai) < 10 || strlen($sodienthoai) > 11) {
        $arrloi[] = 'lendienthoai';
        $strloi   = $arraybien['dienthoaiphailonhon9vanhohon12so'];
    }
    if (empty($arrloi) || 1) {
        $your_ip = $config->get_client_ip();
        // Thuc hien SQL
        $arr_lienhe = array(
            'tendaidien'   => $tendaidien,
            'website'      => $website,
            'loaihoatdong' => $loaihoatdong,
            'hotro'        => $hotro,
            'bietden'      => $bietden,
            'freeoragen'   => $freeoragen,
            'loaidoitac'   => $loaidoitac,
            'hoten'        => $hoten,
            'email'        => $email,
            'sodienthoai'  => $sodienthoai,
            'diachi'       => $diachi,
            'ngaygui'      => $models->NgayHienTaiMysql()
        );
        $result_lienhe = $models->sql_insert("tbl_dangkydoitac", $arr_lienhe);
        // echo $result_lienhe; die();

        // GỬI MAIL
        if ($result_lienhe > 0) {
            // Nôi dung gui mail den admin
            if ($_getArrayconfig['sendemail'] == on) {
                $subject = $_SERVER['HTTP_HOST'] . ' Liên hệ';
                $message = '
           <table cellpadding=\'4\' cellspacing=\'0\' border=\'1\' style=\'border-collapse: collapse;border-color:#527fc0;width: 500px;\'>
            <tr>
                <td colspan="2" style="background-color: #527fc0;color: #FFF;">' . $arraybien['thongtinkhachhanglienhe'] . '</td>
            </tr>
            <tr>
                <td>' . $arraybien['tendaidien'] . '</td><td>' . $tendaidien . '</td>
            </tr>
            <tr>
                <td>' . $arraybien['website'] . '</td><td>' . $website . '</td>
            </tr>
            <tr>
                <td>' . $arraybien['loaihoatdong'] . '</td>
                <td>';
                if ($loaihoatdong==0){
                  $message .= 'Thiết kế website';
                }else if($loaihoatdong==1){
                  $message .= 'Lập trình ứng dụng';
                }else if($loaihoatdong==2){
                  $message .= 'Quảng cáo, Marketing';
                }else if($loaihoatdong==3){
                  $message .= 'Tên miền của quý khách';
                }
            $message .='</td>
            </tr>
            <tr>
                <td>' . $arraybien['hotro'] . '</td>
                <td>';
                if ($hotro==0){
                  $message .= 'Chưa từng hỗ trợ';
                }else if($hotro==1){
                  $message .= '1->10';
                }else if($hotro==2){
                  $message .= '11->25';
                }else if($hotro==3){
                  $message .= 'Trên 25';
                }
            $message .='</td>
            </tr>
            <tr>
                <td>' . $arraybien['bietden'] . '</td>
                <td>';
                if ($bietden==0){
                  $message .= 'Website(webseo.com.vn)';
                }else if($bietden==1){
                  $message .= 'Google';
                }else if($bietden==2){
                  $message .= 'Facebook';
                }else if($bietden==3){
                  $message .= 'Người khác giới thiệu';
                }else if($bietden==4){
                  $message .= 'Công cụ tìm kiếm khác';
                }
            $message .='</td>
            </tr>
            <tr>
                <td>' . $arraybien['freeoragen'] . '</td>
                <td>';
                if ($freeoragen==0){
                  $message .= 'Freelancer';
                }else if($freeoragen==1){
                  $message .= 'Agency';
                }
            $message .='</td>
            </tr>
            <tr>
                <td>' . $arraybien['loaidoitac'] . '</td>
                <td>';
                if ($loaidoitac==0){
                  $message .= 'Thiết kế website';
                }else if($loaidoitac==1){
                  $message .= 'Lập trình ứng dụng';
                }else if($loaidoitac==2){
                  $message .= 'Phát triển kho giao diện';
                }
            $message .='</td>
            </tr>
            <tr>
                <td>' . $arraybien['hoten'] . '</td><td>' . $hoten . '</td>
            </tr>
            <tr>
                <td>' . $arraybien['email'] . '</td><td>' . $email . '</td>
            </tr>
            <tr>
                <td>' . $arraybien['sodienthoai'] . '</td><td>' . $sodienthoai . '</td>
            </tr>
            <tr>
                <td>' . $arraybien['diachi'] . '</td><td>' . $diachi . '</td>
            </tr>
           </table>';
                include "plugins/mail/gmail/class.phpmailer.php";
                include "plugins/mail/gmail/class.smtp.php";
                // hot mail
                $array_sendmail = array(
                    "emailnhan" => $_getArrayconfig['email_nhan'],
                    "emailgui"  => $_getArrayconfig['hostmail_user'],
                    "hostmail"  => $_getArrayconfig['hostmail'],
                    "user"      => $_getArrayconfig['hostmail_user'],
                    "pass"      => $_getArrayconfig['hostmail_pass'],
                    "tieude"    => $_getArrayconfig['hostmail_fullname'],
                    "fullname"  => $_getArrayconfig['hostmail_fullname'],
                    "port"      => 25,
                    "ssl"       => 0,
                    "subject"   => $subject,
                    "message"   => $message,
                );
                $models->sendmail($array_sendmail);
            } // end send mail
        }
        if ($result_lienhe != '') {
            echo "<script>alert('Gửi đặt hàng thành công, chúng tôi sẽ phản hồi sớm')</script>";
            echo "<script>window.location.href='./'</script>";
        }
    }
}
$exportlienhe .= '
   <div class="">
      <div class="panel-heading">
          <h3 class="panel-title text-center">
            <label for="lienhe" class="gioithieu">' . $models->getthongtin("noidung","tieudetrothanhdoitac") . '</label>
          </h3>
      </div>
      <div class="box-dangky">
         <form action="" method="post">
         <span class="tieude">'.$arraybien['thongtincuaban'].'</span>
         <span class="nhanmanh">'.$arraybien['nhanmanh'].'</span>';
if (isset($strloi) && $strloi != '') {
    $exportlienhe .= '
               <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  ' . $strloi . '
               </div>
            ';
}
$exportlienhe .= '
            <div class="form-group">
               <div class="input-group noidungtext">
                  <input type="text" name="tendaidien" class="form-control" id="" placeholder="' . $arraybien['tendaidien'] . '" value="' . (isset($_POST['tendaidien']) ? $_POST['tendaidien'] : null) . '">
               </div>
            </div>

            <div class="form-group">
               <div class="input-group noidungtext">
                  <input type="text" name="website" class="form-control" id="" placeholder="' . $arraybien['diachiwebsite'] . '" value="' . (isset($_POST['website']) ? $_POST['website'] : null) . '">
               </div>
            </div>
            
            <div class="form-group group_option">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['loaihinhhoatdong'] . ':</div>
                  
                  <select name="loaihoatdong" class="nut_option">
                    <option value="0">'.$arraybien['thietkewebsite'].'</option>
                    <option value="1">'.$arraybien['laptrinhungdung'].'</option>
                    <option value="2">'.$arraybien['quangcao'].'</option>
                    <option value="3">'.$arraybien['tenmien'].'</option>
                  </select>

               </div>
            </div>

            <div class="form-group group_option">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['bandahotroduocbaonhieu'] . ':</div>
                  
                  <select name="hotro" class="nut_option">
                    <option value="0">'.$arraybien['chuatunghotro'].'</option>
                    <option value="1">'.$arraybien['1den10'].'</option>
                    <option value="2">'.$arraybien['11den25'].'</option>
                    <option value="3">'.$arraybien['tren25'].'</option>
                  </select>
               </div>
            </div>

            <div class="form-group group_option">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['banbietden'] . ':</div>
                  
                  <select name="bietden" class="nut_option">
                    <option value="0">'.$arraybien['webseo'].'</option>
                    <option value="1">'.$arraybien['google'].'</option>
                    <option value="2">'.$arraybien['facebook'].'</option>
                    <option value="3">'.$arraybien['nguoikhacgioithieu'].'</option>
                    <option value="4">'.$arraybien['congcukhac'].'</option>
                  </select>
               </div>
            </div>

            <div class="form-group group_option">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['banla'] . ':</div>
                  
                  <select name="freeoragen" class="nut_option">
                    <option value="0">'.$arraybien['freelancer'].'</option>
                    <option value="1">'.$arraybien['agency'].'</option>
                  </select>
               </div>
            </div>

            <div class="form-group">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['loaidoitac'] . ':</div>
                  
                  <span class="nhomcheckbox">
                    <input type="radio" name="loaidoitac" value="0"/>
                    <span class="tencheckbox">'.$arraybien['thietkewebsite'].'</span>
                  </span>

                  <span class="nhomcheckbox">
                    <input type="radio" name="loaidoitac" value="1"/>
                    <span class="tencheckbox">'.$arraybien['laptrinhungdung'].'</span>
                  </span>

                  <span class="nhomcheckbox">
                    <input type="radio" name="loaidoitac" value="2"/>
                    <span class="tencheckbox">'.$arraybien['phattrienkhogiaodien'].'</span>
                  </span>

               </div>
            </div>

            <span class="tieude">'.$arraybien['thongtinlienhe'].'</span>
            <span class="nhanmanh">'.$arraybien['lanhungthongtinquantrong'].'</span>

            <div class="form-group lienhe">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['hoten'] . ' '.$arraybien['hotennhangui'].'</div>
                  <input type="text" name="hoten" class="form-control" id="" placeholder="' . $arraybien['hoten'] . '">
               </div>
            </div>

            <div class="form-group lienhe">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['email'] . ' '.$arraybien['emailnhangui'].'</div>
                  <input type="text" name="email" class="form-control" id="" placeholder="' . $arraybien['email'] . '">
               </div>
            </div>

            <div class="form-group lienhe">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['dienthoai'] . '</div>
                  <input type="text" class="form-control" name="sodienthoai" id="" placeholder="' . $arraybien['sodienthoai'] . '">
               </div>
            </div>

            <div class="form-group lienhe">
               <div class="input-group">
                  <div class="tenmuc">' . $arraybien['diachi'] . '</div>
                  <input type="text" name="diachi" class="form-control" id="" placeholder="' . $arraybien['diachi'] . '">
               </div>
            </div>

            <div class="text-center">
              <div class="dkmail-frm">
              <button class="dkmaillienhe" type="submit" data-loading-text="<i class=\'fa fa-spinner fa-spin fa-lg fa-fw\'></i>">'.$arraybien['dangky'].'</button>
              </div>
            </div>
         </form>
      </div>
   </div>
   ';
return $exportlienhe;
