<?php ini_set('session.use_only_cookies', true);
session_start();
$data_dangnhap = "";
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
    echo "<script>
      swal({
         title: 'Bạn đã đăng nhập rồi',
         text: '',
         type: 'warning',
         timer: 5000,
         showConfirmButton: true,
         closeOnConfirm: false,
         closeOnCancel: false,
         confirmButtonText: 'Đóng'
      },
         function(isConfirm){
            if (isConfirm) {
               window.location.href='index.php';
            }
         }
      );
      </script>";
} else {
    $data_dangnhap .= '
      <div class="col-sm-8 can-giua">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title text-center"><label for="login">' . $arraybien['thanhviendangnhap'] . '</label></h3>
            </div>
            <div class="panel-body box_register">
               <form action="' . ROOT_PATH . 'checkLogin.htm" method="POST">
                  <div class="form-group">
                     <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i>&nbsp;' . $arraybien['tendangnhap'] . '</div>
                        <input type="text" class="form-control" name="lg_username" placeholder="' . $arraybien['vuilongnhaptendangnhap'] . '" required>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-lock"></i>&nbsp;' . $arraybien['matkhau'] . '</div>
                        <input type="password" class="form-control" name="lg_password" placeholder="' . $arraybien['vuilongnhapmatkhau'] . '" required>
                     </div>
                  </div>
                  <div class="form-group login-group-checkbox">
                     <input type="checkbox" id="lg_remember" name="lg_remember">
                     <label for="lg_remember">' . $arraybien['ghinho'] . '</label>
                  </div>
                  <div class="form-group text-center">
                     <button type="submit" class="btn btn-primary">' . $arraybien['dangnhap'] . '</button>
                  </div>
                  <div class="etc-login-form">
                     <p>' . $arraybien['banchuacotaikhoan'] . '? <a href="./register.htm">' . $arraybien['taotaikhoanmoi'] . '</a></p>
                  </div>';
    if ($_op != 'thanhvien') {
        $data_dangnhap .= '
                    <input type="hidden" name="hi_url" value="http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '" />';
    }
    $data_dangnhap .= '
               </form>
            </div>
         </div>
      </div>
      ';
    return $data_dangnhap;
}
