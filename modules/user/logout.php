<?php ini_set('session.use_only_cookies', true);
session_start();
unset($_SESSION['tendangnhap']);
unset($_SESSION['user_id']);
unset($_SESSION['user_hotendem']);
unset($_SESSION['user_supperadmin']);
unset($_SESSION['user_ten']);
unset($_SESSION['user_gioitinh']);
unset($_SESSION['user_email']);
unset($_SESSION['user_diachi']);
unset($_SESSION['user_tinhthanh']);
unset($_SESSION['user_sodienthoai']);
unset($_SESSION['user_ngaysinh']);
unset($_SESSION['user_website']);
unset($_SESSION['user_yahoo']);

// xoa cookie
if(isset($_COOKIE['username'])){
	 unset($_COOKIE['username']);
	 unset($_COOKIE['password']);
	 setcookie('username', null, -1, '/');
     setcookie('password', null, -1, '/');
}

echo "<script>location.href='" . $_SESSION['ROOT_PATH'] . "'</script>";
