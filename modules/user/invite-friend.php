<?php
    $data = null;
    if( is_login() ):
    $data.= '
    <div class="invite-container">
        <div class="modal-content">
            <div class="modal-body clearfix">
                <div class="modal-head invite-head">
                    '.$arraybien['magioithieuchobanbedongnghiep'].'
                </div><!-- /.invite-head -->
                <div class="invite-des col-sm-12 form-group">
                    '.$db->getThongTin('gioithieuchobanbedongnghiep').'
                </div><!-- /.invite-des -->
                <div class="form-group gioithieu-top clearfix">
                    <div class="col-sm-12">';
                            if( $db->getUser('magioithieu') ):
                            $data.='
                            <div class="input-group- custom2">
                                <input type="text" class="form-control input-link-invite" value="'.ROOT_PATH.'?magioithieu='.$db->getUser('magioithieu').'" id="magioithieu" >
                            </div><!-- /.input-group -->';
                            else:
                            $data.='
                            <div class="alert alert-danger">
                                <h4>'.$arraybien['banchuacomagioithieu'].'.</h4>
                                <!--a href="'.ROOT_PATH.'" title="'.$arraybien['laymagioithieu'].'" rel="nofollow" class="btn btn-danger btn-xs">'.$arraybien['laymagioithieu'].'</a-->
                            </div><!-- /.alert alert-danger -->';
                            endif;
                            $data.='
                    </div><!-- /.col-sm-12 -->
                </div><!-- /.form-group -->

                <div class="form-group clearfix">
                    <div class="modal-head">
                        '.$arraybien['danhsachdamoi'].'
                    </div><!-- /.modal-head -->

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>'.$arraybien['hovaten'].'</th>
                                    <th>'.$arraybien['email'].'</th>
                                    <th>'.$arraybien['sodienthoai'].'</th>
                                    <th>'.$arraybien['sodiem'].'</th>
                                    <th>Level giới thiệu</th>
                                    <th>'.$arraybien['ngaythamgia'].'</th>
                                </tr>
                            </thead>
                            <tbody>';
                            $db->where("manguoigioithieu", $db->getUser('magioithieu'));
                            $db->where("active", 1);
                            $nguoidamoi = $db->get("tbl_user", null, ['id','ten','email','sodienthoai', 'ngaydangky','magioithieu']);
                            if( $db->count > 0 ) {
                                foreach( $nguoidamoi as $kdamoi => $damoi ) {
                                    $id          = $damoi['id'];
                                    $hoten       = $damoi['ten'];
                                    $email       = $damoi['email'];
                                    $sodienthoai = $damoi['sodienthoai'];
                                    $ngay        = $damoi['ngaydangky'];

                                    $db->where("manguoigioithieu", $damoi['magioithieu']);
                                    $db->where("active", 1);
                                    $nguoidamoi2 = $db->get("tbl_user", null, ['id','ten','email','sodienthoai', 'ngaydangky']);

                                    $data.= '
                                    <tr>
                                        <td>'.$hoten.'</td>
                                        <td>'.$email.'</td>
                                        <td>'.$sodienthoai.'</td>
                                        <td>'.$db->layTongDiem($id).'</td>
                                        <td>Cấp 1</td>
                                        <td>'.$ngay.'</td>';

                                        if( $db->count ){
                                            foreach( $nguoidamoi2 as $k2 => $damoi2 ) {
                                                $id2          = $damoi2['id'];
                                                $hoten2       = $damoi2['ten'];
                                                $email2       = $damoi2['email'];
                                                $sodienthoai2 = $damoi2['sodienthoai'];
                                                $ngay2        = $damoi2['ngaydangky'];
                                                $data.='
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;'.$hoten2.'</td>
                                                    <td>'.$email2.'</td>
                                                    <td>'.$sodienthoai2.'</td>
                                                    <td>'.$db->layTongDiem($id2).'</td>
                                                    <td>Cấp 2</td>
                                                    <td>'.$ngay2.'</td>
                                                </tr>';
                                            }
                                        }

                                    $data.='
                                    </tr>';
                                }
                            }
                            $data.='
                            </tbody>
                        </table>
                    </div>

                </div><!-- /.form-group clearfix -->

            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.invite-container -->';
    else:
        $data.= not_login_template();
    endif;
    $smarty->assign("inviteFriend", $data);