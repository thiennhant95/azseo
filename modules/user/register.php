<?php ini_set('session.use_only_cookies', true);
session_start();
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
    echo "<script>
   swal({
      title: 'Bạn đang là thành viên',
      text: '',
      type: 'warning',
      timer: 5000,
      showConfirmButton: true,
      closeOnConfirm: false,
      closeOnCancel: false,
      confirmButtonText: 'Quay lại'
   },
      function(isConfirm){
         if (isConfirm) {
            window.location.href='index.php';
         }
      }
   );
   </script>";
} else {
/**
 * START tinhthanh
 */
    $_session_tinhthanh = $_SESSION['user_tinhthanh'];
// truy van lay tinh/thanh pho
    $s_thanhpho = "SELECT a.id,b.ten
            FROM tbl_type AS a
            INNER JOIN tbl_type_lang AS b
            ON a.id = b.idtype
            and b.idlang = '" . $_SESSION['_lang'] . "'
            and a.anhien = 1
            and length(a.id) = 4
            order by a.thutu ASC ";
    $d_thanhpho    = $db->sqlSelectSql($s_thanhpho);
    $data_thanhpho = '';
    if (count($d_thanhpho) > 0) {
        $linkgiohang = ROOT_PATH . 'modules/giohang/giohang_tinhthanh.php?name=quanhuyen&id=';
        $data_thanhpho .= '<select onchange="Get_Data(\'' . $linkgiohang . '\'+this.value,\'loadquanhuyen\')" name="tinhthanh" id="tinhthanh" class="form-control">
                  <option value="">' . $arraybien['vuilongchon'] . '</option>';
        foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
            $ten = $info_thanhpho['ten'];
            $id  = $info_thanhpho['id'];
            if ($id == substr($_session_tinhthanh, 0, 4)) {
                $data_thanhpho .= '<option selected="selected" value="' . $id . '">' . $ten . '</option>';
            } else {
                $data_thanhpho .= '<option value="' . $id . '">' . $ten . '</option>';
            }
        }
        $data_thanhpho .= '</select>';
    }
// truy van lay quan huyen
    $s_thanhpho = "SELECT a.id,b.ten
            FROM tbl_type AS a
            INNER JOIN tbl_type_lang AS b
            ON a.id = b.idtype
            and b.idlang = '" . $_SESSION['_lang'] . "'
            and a.id like '" . substr($_session_tinhthanh, 0, 4) . "%'
            and a.anhien = 1
            and length(a.id) = 8
            order by a.thutu ASC ";
    $d_thanhpho     = $db->sqlSelectSql($s_thanhpho);
    $data_quanhuyen = '';
    if (count($d_thanhpho) > 0) {
        $data_quanhuyen .= '<div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['quanhuyen'] . '</div>';
        $linkgiohang = ROOT_PATH . 'modules/giohang/giohang_tinhthanh.php?name=phuongxa&id=';
        $data_quanhuyen .= '<select onchange="Get_Data(\'' . $linkgiohang . '\'+this.value,\'loadphuongxa\')" name="quanhuyen" id="quanhuyen" class="form-control">
                  <option value="">' . $arraybien['vuilongchon'] . '</option>';
        foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
            $ten = $info_thanhpho['ten'];
            $id  = $info_thanhpho['id'];
            if ($id == substr($_session_tinhthanh, 0, 8)) {
                $data_quanhuyen .= '<option selected="selected" value="' . $id . '">' . $ten . '</option>';
            } else {
                $data_quanhuyen .= '<option value="' . $id . '">' . $ten . '</option>';
            }
        }
        $data_quanhuyen .= '</select>';
        $data_quanhuyen .= '</div>';
        $data_dangky .= $data_quanhuyen;
    }
// truy van lay phuong xa
    $s_thanhpho = "SELECT a.id,b.ten
            FROM tbl_type AS a
            INNER JOIN tbl_type_lang AS b
            ON a.id = b.idtype
            and b.idlang = '" . $_SESSION['_lang'] . "'
            and a.id like '" . substr($_session_tinhthanh, 0, 8) . "%'
            and a.anhien = 1
            and length(a.id) = 12
            order by a.thutu ASC ";
    $d_thanhpho    = $db->sqlSelectSql($s_thanhpho);
    $data_phuongxa = '';
    if (count($d_thanhpho) > 0) {
        $data_phuongxa .= '<div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['phuongxa'] . '</div>';
        $linkgiohang = ROOT_PATH . 'modules/giohang/giohang_tinhthanh.php?name=phuongxa&id=';
        $data_phuongxa .= '<select name="phuongxa" id="phuongxa" class="form-control">
                  <option value="">' . $arraybien['vuilongchon'] . '</option>';
        foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
            $ten = $info_thanhpho['ten'];
            $id  = $info_thanhpho['id'];
            if ($id == substr($_session_tinhthanh, 0, 12)) {
                $data_phuongxa .= '<option selected="selected" value="' . $id . '">' . $ten . '</option>';
            } else {
                $data_phuongxa .= '<option value="' . $id . '">' . $ten . '</option>';
            }
        }
        $data_phuongxa .= '</select>';
        $data_phuongxa .= '</div>';
    }
/**
 * END tinhthanh
 */
    $data_dangky = "";
    if (isset($_POST['dangky'])) {
        $reg_tendangnhap = trim($_POST['reg_username']);
        $reg_matkhau     = trim($_POST['reg_password']);
        $reg_rematkhau   = trim($_POST['reg_password_confirm']);
        $reg_email       = trim($_POST['reg_email']);
        $reg_hoten       = trim($_POST['reg_fullname']);
        $reg_gioitinh    = trim($_POST['reg_gender']);
        $reg_dongy       = trim($_POST['reg_agree']);
        $reg_tinhthanh   = trim($_POST['tinhthanh']);
        $reg_quanhuyen   = trim($_POST['quanhuyen']);
        $reg_phuongxa    = trim($_POST['phuongxa']);
        if ($reg_quanhuyen != '') {
            $reg_tinhthanh = $reg_quanhuyen;
        }
        if ($reg_phuongxa != '') {
            $reg_tinhthanh = $reg_phuongxa;
        }
        $str_err           = "";
        $check_tendangnhap = $db->sqlSelectSql("SELECT id FROM tbl_user WHERE tendangnhap = '" . $reg_tendangnhap . "' ");
        $check_email       = $db->sqlSelectSql("SELECT id FROM tbl_user WHERE email = '" . $reg_email . "' ");
        if (count($check_tendangnhap) > 0) {
            $str_err = "Tên đăng nhập [ " . $reg_tendangnhap . " ] đã được sử dụng";
        } elseif ($matkhau != $rematkhau) {
            $str_err = "Mật khẩu không khớp";
        } elseif (count($check_email) > 0) {
            $str_err = "Email [ " . $reg_email . " ] đã được sử dụng";
        } else {
            $reg_lat         = rand(100, 999);
            $reg_matkhauoke  = md5(md5($reg_rematkhau) . $reg_lat);
            $arr_insert_user = array(
                'tendangnhap' => $reg_tendangnhap,
                'matkhau'     => $reg_matkhauoke,
                'active'      => 1,
                'lat'         => $reg_lat,
                'nhom'        => 0,
                'supperadmin' => 0,
                'tinhthanh'   => $reg_tinhthanh,
                'ten'         => $reg_hoten,
                'gioitinh'    => $reg_gioitinh,
                'email'       => $reg_email,
                'ngaycapnhat' => $db->getDateTimes(),
                'ngaydangky'  => $db->getDateTimes(),
            );
            $kq_dangky = $db->sqlInsert("tbl_user", $arr_insert_user);
            if ($kq_dangky > 0) {
                $checkLogin = $db->checkLoginUser($tendangnhap, $matkhau);
                if (count($checkLogin) == 1) {
                    session_regenerate_id();
                    $_SESSION['user_id'] = $checkLogin[0]['id'];
                }
                echo "<script>alert('Chúc mừng bạn đã đăng ký thành công!')</script>";
                echo "<script>window.location.href='index.php'</script>";
            }
        }
    }
    $data_dangky .= '
   <!-- REGISTRATION FORM -->
   <div class="row">
      <div class="col-sm-8 can-giua">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title text-center"><label for="dangky">' . $arraybien['dangkythanhvien'] . '</label></h3>
            </div>
            <div class="panel-body box_register">
               <form id="register-form" action="" method="POST" class="text-left">';
    if ($str_err != '') {
        $data_dangky .= '
                  <div class="alert alert-danger">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <strong>' . $str_err . '</strong>
                  </div>
                  ';
    }
    $data_dangky .= '
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-user"></i> ' . $arraybien['tendangnhap'] . '</div>
                     <input type="text" class="form-control" name="reg_username" placeholder="' . $arraybien['vuilongnhaptendangnhap'] . '" pattern=".{4,}" required>
                  </div>
               </div>
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-lock"></i> ' . $arraybien['matkhau'] . '</div>
                     <input type="password" class="form-control" name="reg_password" placeholder="' . $arraybien['vuilongnhapmatkhau'] . '" pattern=".{6,}" required>
                  </div>
               </div>
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-lock"></i> ' . $arraybien['xacnhanmatkhau'] . '</div>
                     <input type="password" class="form-control" name="reg_password_confirm" placeholder="' . $arraybien['vuilongxacnhanmatkhau'] . '" pattern=".{6,}" required>
                  </div>
               </div>
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-envelope"></i> ' . $arraybien['email'] . '</div>
                     <input type="text" class="form-control" name="reg_email" placeholder="' . $arraybien['vuilongnhapemail'] . '" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                  </div>
               </div>
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-users"></i> ' . $arraybien['ten'] . '</div>
                     <input type="text" class="form-control" name="reg_fullname" placeholder="' . $arraybien['vuilongnhapten'] . '" pattern=".{2,}" required>
                  </div>
               </div>
               <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-compass"></i>&nbsp;' . $arraybien['tinhthanh'] . '</div>
                  ' . $data_thanhpho . '
               </div>
               </div>
               <div id="loadquanhuyen" class="form-group"></div>
               <div id="loadphuongxa" class="form-group"></div>
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-addon"><i class="fa fa-venus-mars"></i>&nbsp;' . $arraybien['gioitinh'] . '</div>
                     <div class="group-active">
                           <label>
                              <input type="radio" value="1" name="reg_gender" required>
                              ' . $arraybien['nam'] . '
                           </label>
                           <label>
                              <input type="radio" value="0" name="reg_gender" required>
                              ' . $arraybien['nu'] . '
                           </label>
                     </div>
                  </div>
               </div>
               <div class="form-group login-group-checkbox">
                  <input type="checkbox" class="" value="1" id="reg_agree" name="reg_agree" required>
                  <label for="reg_agree">' . $arraybien['toidongyvoi'] . ' <a href="#">' . $arraybien['dieukien'] . '</a></label>
               </div>
               <div class="form-group text-center">
                  <button type="submit" name="dangky" class="btn btn-danger">' . $arraybien['dangky'] . '</button>
                  <button type="reset" class="btn btn-default">' . $arraybien['reset'] . '</button>
               </div>
               <div class="etc-login-form">
                  <p>' . $arraybien['bandacotaikhoan'] . '? <a href="./login.htm">' . $arraybien['dangnhaptaiday'] . '</a></p>
               </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   ';
    return $data_dangky;
}
