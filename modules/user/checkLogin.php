<?php ini_set('session.use_only_cookies', true);
session_start();
/**
 * CHECK LOGIN
 */
// kiem tra dnag nhap
if ($_POST['lg_username']) {
    $tendangnhap = strip_tags($_POST['lg_username']);
    $matkhau     = strip_tags($_POST['lg_password']);
    if ($tendangnhap != '' and $matkhau != '') {
        $matkhau = md5($_POST['lg_password']);
        // kiem tra login
        $checkLogin = $db->checkLoginUser($tendangnhap, $matkhau);
        if (count($checkLogin) == 1) {
            session_regenerate_id();
            $_SESSION['tendangnhap']      = $tendangnhap;
            $_SESSION['user_id']          = $checkLogin[0]['id'];
            $_SESSION['user_hotendem']    = $checkLogin[0]['hotendem'];
            $_SESSION['user_supperadmin'] = $checkLogin[0]['supperadmin'];
            $_SESSION['user_ten']         = $checkLogin[0]['ten'];
            $_SESSION['user_gioitinh']    = $checkLogin[0]['gioitinh'];
            $_SESSION['user_email']       = $checkLogin[0]['email'];
            $_SESSION['user_diachi']      = $checkLogin[0]['diachi'];
            $_SESSION['user_tinhthanh']   = $checkLogin[0]['tinhthanh'];
            $_SESSION['user_sodienthoai'] = $checkLogin[0]['sodienthoai'];
            $_SESSION['user_ngaysinh']    = $checkLogin[0]['ngaysinh'];
            $_SESSION['user_website']     = $checkLogin[0]['website'];
            $_SESSION['user_yahoo']       = $checkLogin[0]['yahoo'];
            if ($_POST['hi_url'] != '') {
                echo "<script>location.href='" . $_POST['hi_url'] . "'</script>";
            } else {
                echo "<script>location.href='" . ROOT_PATH . "'</script>";
            }
        } else {
            echo "<script>location.href='./login.htm'</script>";
        }
    } else {
        echo "<script>location.href='./login.htm'</script>";
    }
}
