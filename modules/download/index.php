<?php
// phan trang
$arrdata = array();
$loaiDownload = array_search("download",$_arr_loai_noidung);
$pathDownload = ROOT_PATH."uploads/download/";
$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongtin']; //limit in each page
$startpoint = ($page * $perpage) - $perpage;

$d_noidung = $db->layNoiDung(
   $_op,
    " AND a.idtype like '%{$__idtype_danhmuc}%' ",
    "file",
    array($startpoint, $perpage)
);

$sql_page = "SELECT COUNT(*) as num
            FROM tbl_noidung As a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND a.idtype like '%{$__idtype_danhmuc}%'
                AND b.idlang = {$_SESSION['_lang']}
                AND loai = {$loaiDownload} ";

$data_noidung = '
<h1 class="title">
    <a href="' . ROOT_PATH . $_getcat . '/" title="' . $_ten . '">
        ' . $_ten . '
    </a>
</h1>';

if ($_array_config_noidung['share'] == 1) {
    $data_noidung .= include "plugin/share/share.php";
}

$demnoidung = count($d_noidung);

if ($demnoidung > 0) {
    $arrdata['noidung'] = $d_noidung;
    $url_page = ROOT_PATH . $_getcat . '-trang-';
    $phantrang = Pages($sql_page, $perpage, $url_page);
}

$smarty->assign("arrdata", $arrdata);
$smarty->assign("phantrang", $phantrang);
$smarty->assign("flowND", $flowND);