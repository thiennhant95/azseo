<?php
$exportlienhe = null;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $email       = $_POST['email'];
  $hoten       = $_POST['hoten'];
  $sodienthoai = $_POST['sodienthoai'];
  $dangkygoi   = $_POST['dangkygoi'];
  $ngaygui     = $_POST['ngaygui'];

  $arrloi    = array();
  $strloi    = "";
  if ($hoten == '') {
    $arrloi[] = 'nullhoten';
    $strloi   = $arraybien['banchuanhaphoten'];
  } elseif (strlen($hoten) < 2) {
    $arrloi[] = 'shorthoten';
    $strloi   = $arraybien['tenquanganvuilongnhaplai'];
  } elseif ($dienthoai == '') {
    $arrloi[] = 'nulldienthoai';
    $strloi   = $arraybien['banchuanhapdienthoai'];
  } elseif (!is_numeric($sodienthoai)) {
    $arrloi[] = 'intdienthoai';
    $strloi   = $arraybien['dienthoaiphailaso'];
  } elseif (strlen($sodienthoai) < 10 || strlen($sodienthoai) > 11) {
    $arrloi[] = 'lendienthoai';
    $strloi   = $arraybien['dienthoaiphailonhon9vanhohon12so'];
  }
  if (empty($arrloi) || 1) {
    // $your_ip = $config->get_client_ip();
        // Thuc hien SQL
    $arr_lienhe = array(
      'email'       => $email,
      'hoten'       => $hoten,
      'sodienthoai' => $sodienthoai,
      'dangkygoi'   => $dangkygoi,
      // 'ip'          => $your_ip,
      'ngaygui'     => $db->NgayHienTaiMysql()
    );
    $result_lienhe = $db->sqlInsert("tbl_dathang", $arr_lienhe);
        // echo $result_lienhe; die();

    if ($result_lienhe > 0) {
            // Nôi dung gui mail den admin
      if ($_getArrayconfig['sendemail'] == on) {
        $subject = $_SERVER['HTTP_HOST'] . ' Liên hệ';
        $message = '
        <table cellpadding=\'4\' cellspacing=\'0\' border=\'1\' style=\'border-collapse: collapse;border-color:#527fc0;width: 500px;\'>
        <tr>
        <td colspan="2" style="background-color: #527fc0;color: #FFF;">' . $arraybien['thongtinkhachhanglienhe'] . '</td>
        </tr>

        <tr>
        <td>' . $arraybien['email'] . '</td><td>' . $email . '</td>
        </tr>
        <tr>
        <td>' . $arraybien['hoten'] . '</td><td>' . $hoten . '</td>
        </tr>
        <tr>
        <td>' . $arraybien['sodienthoai'] . '</td><td>' . $sodienthoai . '</td>
        </tr>

        <tr>
        <td>' . $arraybien['dangkykhoahoc'] . '</td>
        <td>';
        if ($dangkygoi==0){
          $message .= '4-7 tuổi';
        }else if($dangkygoi==1){
          $message .= '7-11 tuổi';
        }else if($dangkygoi==2){
          $message .= '11-16 tuổi';
        }
        $message .='</td>
        </tr>
        </table>';
        include "plugins/mail/gmail/class.phpmailer.php";
        include "plugins/mail/gmail/class.smtp.php";
                // hot mail
        $array_sendmail = array(
          "emailnhan" => $_getArrayconfig['email_nhan'],
          "emailgui"  => $_getArrayconfig['hostmail_user'],
          "hostmail"  => $_getArrayconfig['hostmail'],
          "user"      => $_getArrayconfig['hostmail_user'],
          "pass"      => $_getArrayconfig['hostmail_pass'],
          "tieude"    => $_getArrayconfig['hostmail_fullname'],
          "fullname"  => $_getArrayconfig['hostmail_fullname'],
          "port"      => 25,
          "ssl"       => 0,
          "subject"   => $subject,
          "message"   => $message,
        );
        sendmail($array_sendmail);
            } // end send mail
          }
          if ($result_lienhe != '') {
            echo "<script>alert('Gửi đăng ký thành công, chúng tôi sẽ phản hồi sớm')</script>";
            echo "<script>window.location.href='./'</script>";
          }
        }
      }
      $exportlienhe .= '
      <div class="panel panel-default">
      <div class="panel-heading">
      <h3 class="panel-title text-center">
      <label for="lienhe">' . $db->getThongTin("tieudedangky") . '</label>
      </h3>
      </div>
      <div class="panel-body box-lienhe">
      <form action="" method="post">
      <span class="tieude">'.$arraybien['thongtindangky'].'</span>';
      if (isset($strloi) && $strloi != '') {
        $exportlienhe .= '
        <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        ' . $strloi . '
        </div>
        ';
      }
      $exportlienhe .= '
      <div class="form-group">
      <div class="input-group">
      <div class="input-group-addon">' . $arraybien['email'] . ':</div>
      <input type="text" class="form-control" name="email" id="" placeholder="' . $arraybien['vuilongnhapemail'] . '">
      </div>
      </div>

      <div class="form-group">
      <div class="input-group">
      <div class="input-group-addon">' . $arraybien['hoten'] . ':</div>
      <input type="text" name="hoten" class="form-control" id="" placeholder="' . $arraybien['vuilongnhapten'] . '" value="' . (isset($_POST['hoten']) ? $_POST['hoten'] : null) . '">
      </div>
      </div>

      <div class="form-group">
      <div class="input-group">
      <div class="input-group-addon">' . $arraybien['sodienthoai'] . ':</div>
      <input type="text" class="form-control" name="sodienthoai" id="" placeholder="' . $arraybien['vuilongnhapdienthoai'] . '">
      </div>
      </div>


      <span class="tieude">'.$arraybien['khoahocchoconban'].'</span>

      <div class="form-group">
      <div class="input-group">
      <div class="input-group-addon">' . $arraybien['dangkygoi'] . ':</div>

      <span class="nhomcheckbox">
      <input type="radio" name="dangkygoi" value="0"/>
      <span class="tencheckbox">'.$arraybien['4toi7'].'</span>
      </span>

      <span class="nhomcheckbox">
      <input type="radio" name="dangkygoi" value="1"/>
      <span class="tencheckbox">'.$arraybien['7toi11'].'</span>
      </span>

      <span class="nhomcheckbox">
      <input type="radio" name="dangkygoi" value="2"/>
      <span class="tencheckbox">'.$arraybien['11toi16'].'</span>
      </span>

      </div>
      </div>

      <span class="tieude">'.$arraybien['bancanchuanbi'].'</span>

      <div class="noidungnhangui">'.$db->getThongTin("noidungnhangui").'</div>

      <div class="text-center">
      <div class="dkmail-frm">
      <button type="submit" data-loading-text="<i class=\'fa fa-spinner fa-spin fa-lg fa-fw\'></i>">'.$arraybien['guithongtin'].'</button>
      </div>
      </div>
      </form>
      </div>
      </div>
      ';
      return $exportlienhe;
