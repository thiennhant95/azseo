<?php
$retMaGiamGia ='
<div class="magiamgia clearfix form-group">
<div class="panel panel-danger">
  <div class="panel-heading">
    <div class="panel-title h5">
      Mã giảm giá
    </div>
  </div>
  <div class="panel-body">
    <div class="input-group '.(!isset( $_SESSION['magiamgia'] ) ? 'hide' : null).'">
        <input type="text" name="magiamgia" class="form-control" value="'.$_SESSION['magiamgia'].'" readonly />
        <div class="input-group-btn">
            <button type="button" class="btn btn-default" id="btn_change_ma">
                Thay đổi
            </button>
        </div>
    </div>
    <div class="input-group '.(isset( $_SESSION['magiamgia'] ) ? 'hide' : null).'">
        <input type="text" name="magiamgia" class="form-control" value="'.$_SESSION['magiamgia'].'" />
        <div class="input-group-btn">
            <button type="button" class="btn btn-default" id="btn_giamgia">
                Áp dụng
            </button>
        </div>
    </div>
  </div>
</div>
</div>
';
return $retMaGiamGia;
?>