<?php
// xoa gio hang
if ($_GET['method'] == 'xoagiohang') {
    unset($_SESSION['giohang']);
}
if (count($_SESSION['giohang']) <= 0) {
    echo '<script>alert("' . $arraybien['giohangrong'] . '"); location.href="' . $_SESSION['ROOT_PATH'] . '"; </script>';
}
$_session_tinhthanh = $_SESSION['user_tinhthanh'];
// truy van lay tinh/thanh pho
$s_thanhpho = "SELECT a.id,b.ten
            FROM tbl_type AS a
            INNER JOIN tbl_type_lang AS b
            ON a.id = b.idtype
            and b.idlang = '" . $_SESSION['_lang'] . "'
            and a.anhien = 1
            and length(a.id) = 4
            order by a.thutu ASC ";
$d_thanhpho    = $db->sqlSelectSql($s_thanhpho);
$data_thanhpho = '';
if (count($d_thanhpho) > 0 || !1) {
    $linkgiohang = ROOT_PATH . 'modules/cart/tinhthanh.php?name=quanhuyen&id=';
    $data_thanhpho .= '<select onchange="Get_Data(\'' . $linkgiohang . '\'+this.value,\'loadquanhuyen\')" name="tinhthanh" id="tinhthanh" class="form-control">
                  <option value="">' . $arraybien['vuilongchon'] . '</option>';
    foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
        $ten = $info_thanhpho['ten'];
        $id  = $info_thanhpho['id'];
        if ($id == substr($_session_tinhthanh, 0, 4)) {
            $data_thanhpho .= '<option selected="selected" value="' . $id . '">' . $ten . '</option>';
        } else {
            $data_thanhpho .= '<option value="' . $id . '">' . $ten . '</option>';
        }
    }
    $data_thanhpho .= '</select>';
}
// truy van lay quan huyen
$s_thanhpho = "SELECT a.id,b.ten
            FROM tbl_type AS a
            INNER JOIN tbl_type_lang AS b
            ON a.id = b.idtype
            and b.idlang = '" . $_SESSION['_lang'] . "'
            and a.id like '" . substr($_session_tinhthanh, 0, 4) . "%'
            and a.anhien = 1
            and length(a.id) = 8
            order by a.thutu ASC ";
$d_thanhpho     = $db->sqlSelectSql($s_thanhpho);
$data_quanhuyen = '';
if (count($d_thanhpho) > 0) {
    $data_quanhuyen .= '<div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['quanhuyen'] . '</div>';
    $linkgiohang = ROOT_PATH . 'modules/cart/tinhthanh.php?name=phuongxa&id=';
    $data_quanhuyen .= '<select onchange="Get_Data(\'' . $linkgiohang . '\'+this.value,\'loadphuongxa\')" name="quanhuyen" id="quanhuyen" class="form-control">
                  <option value="">' . $arraybien['vuilongchon'] . '</option>';
    foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
        $ten = $info_thanhpho['ten'];
        $id  = $info_thanhpho['id'];
        if ($id == substr($_session_tinhthanh, 0, 8)) {
            $data_quanhuyen .= '<option selected="selected" value="' . $id . '">' . $ten . '</option>';
        } else {
            $data_quanhuyen .= '<option value="' . $id . '">' . $ten . '</option>';
        }
    }
    $data_quanhuyen .= '</select>';
    $data_quanhuyen .= '</div>';
}
// truy van lay phuong xa
$s_thanhpho = "SELECT a.id,b.ten
            FROM tbl_type AS a
            INNER JOIN tbl_type_lang AS b
            ON a.id = b.idtype
            and b.idlang = '" . $_SESSION['_lang'] . "'
            and a.id like '" . substr($_session_tinhthanh, 0, 8) . "%'
            and a.anhien = 1
            and length(a.id) = 12
            order by a.thutu ASC ";
$d_thanhpho    = $db->sqlSelectSql($s_thanhpho);
$data_phuongxa = '';
if (count($d_thanhpho) > 0) {
    $data_phuongxa .= '<div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['phuongxa'] . '</div>';
    $linkgiohang = ROOT_PATH . 'modules/cart/tinhthanh.php?name=phuongxa&id=';
    $data_phuongxa .= '<select name="phuongxa" id="phuongxa" class="form-control">
                  <option value="">' . $arraybien['vuilongchon'] . '</option>';
    foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
        $ten = $info_thanhpho['ten'];
        $id  = $info_thanhpho['id'];
        if ($id == substr($_session_tinhthanh, 0, 12)) {
            $data_phuongxa .= '<option selected="selected" value="' . $id . '">' . $ten . '</option>';
        } else {
            $data_phuongxa .= '<option value="' . $id . '">' . $ten . '</option>';
        }
    }
    $data_phuongxa .= '</select>';
    $data_phuongxa .= '</div>';
}
$data_giohang = '';
$data_giohang .= '
<div class="group_giohang ws-container">';
// chia thanh 2 cot trong gio hang
// cot 1 là cột danh sách sản phẩm
$data_giohang .= '
   <div class="cot_dssp">';
$data_giohang .= include "view.php";
$data_giohang .= '
   </div>'; // end cot danh sach san pham
$data_giohang .= '
   <div class="cot_form">';
// neu da dang nhap roi thi load form len luong và ngược lại là có 2 lựa chọn
if (isset($_SESSION['user_id'])) {
    $data_giohang .= '
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
         <div class="panel-heading">
           <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">';
    if (isset($_SESSION['user_id'])) {
        $data_giohang .= '
               <i class="fa fa-user fa-2x"></i> ' . $arraybien['thongtingiaohang'];
    } else {
        $data_giohang .= '
               <i class="fa fa-info-circle fa-2x"></i> ' . $arraybien['muahangkhongcandangky'];
    }
    $data_giohang .= '
            </a>
           </h4>
         </div>
         <div id="collapse1" class="panel-collapse collapse in">
           <div class="panel-body">
            <form action="' . ROOT_PATH . 'checkout.htm" method="post" name="checkout" id="checkout">
               <div class="form-group form_giohang">
                  <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-mobile fa-lg"></i> ' . $arraybien['dienthoai'] . ' <span>(*)</span></div>
                    <input class="form-control" name="dienthoai" id="dienthoai" value="' . $_SESSION['user_sodienthoai'] . '">
                    <input type="hidden" value="' . $arraybien['vuilongnhapsodienthoai'] . '" name="dienthoai_alert">
                  </div>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user-md fa-lg"></i> ' . $arraybien['hoten'] . ' <span>(*)</span></div>
                    <input class="form-control" name="hoten" id="hoten" value="' . $_SESSION['user_hotendem'] . ' ' . $_SESSION['user_ten'] . '">
                    <input type="hidden" value="' . $arraybien['vuilongnhaphoten'] . '" name="hoten_alert">
                  </div>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope-o fa-lg"></i> ' . $arraybien['email'] . '</div>
                    <input class="form-control" name="email" id="email" value="' . $_SESSION['user_email'] . '">
                  </div>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-map-marker fa-lg"></i> ' . $arraybien['diachi'] . ' <span>(*)</span></div>
                    <input class="form-control" name="diachi" id="diachi" value="' . $_SESSION['user_diachi'] . '">
                    <input type="hidden" value="' . $arraybien['vuilongnhapdiachi'] . '" name="diachi_alert">
                  </div>';
                if( @$enable_tinthanh ) {
                    $data_giohang.='
                    <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['tinhthanh'] . '</div>
                    ' . $data_thanhpho . '
                    </div>
                    <div id="loadquanhuyen">' . $data_quanhuyen . '</div>
                    <div id="loadphuongxa">' . $data_phuongxa . '</div>';
                }$data_giohang.='

                  <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-comments fa-lg"></i> ' . $arraybien['ghichu'] . '
                    </div>
                    <textarea class="form-control" rows="5" id="ghichu" name="ghichu">'.$_SESSION['user_ghichu'].'</textarea>
                  </div>';

                // Quy đổi điểm
                $data_giohang.= include 'doidiem.inc.php';

                // Phuong thuc thanh toan - Khi chua login
                // $data_giohang.= include 'phuongthucthanhtoan.inc.php';

                $data_giohang .= '
                  <input class="btn btn-primary btn_submit" type="submit" name="thanhtoan_giohang" id="thanhtoan_giohang" value="' . $arraybien['dathang'] . '" />
               </div>
            </form>
           </div>
         </div>
        </div>
      </div>';
} else {
    $data_giohang .= '
      <div class="panel-group" id="accordion">';
    if ($_array_config_cart['login'] == 1) {
        $keycolspanin = '';
        $data_giohang .= '
        <div class="panel panel-default">
         <div class="panel-heading">
           <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
            <i class="fa fa-sign-in fa-2x"></i> ' . $arraybien['dangnhap'] . '</a>
           </h4>
         </div>
         <div id="collapse1" class="panel-collapse collapse in">
           <div class="panel-body">
            <form action="' . ROOT_PATH . 'checkLogin.htm" method="post">
               <div class="form-group">
                 <div class="input-group width_100">
                   <div class="input-group-addon labellogin"><i class="fa fa-user"></i>&nbsp;' . $arraybien['tendangnhap'] . '</div>
                   <input type="text" class="form-control" name="lg_username" placeholder="' . $arraybien['vuilongnhaptendangnhap'] . '" required>
                 </div>
               </div>
               <div class="form-group">
                 <div class="input-group width_100">
                   <div class="input-group-addon labellogin"><i class="fa fa-lock"></i>&nbsp;' . $arraybien['matkhau'] . '</div>
                   <input type="password" class="form-control" name="lg_password" placeholder="' . $arraybien['vuilongnhapmatkhau'] . '" required>
                 </div>
               </div>
               <div class="form-group login-group-checkbox">
                 <input type="checkbox" id="lg_remember" name="lg_remember">
                 <label for="lg_remember">' . $arraybien['ghinho'] . '</label>
               </div>
               <div class="form-group text-center">
                 <button type="submit" class="btn btn-primary">' . $arraybien['dangnhap'] . '</button>
               </div>
               <div class="etc-login-form">
                 <p>' . $arraybien['banchuacotaikhoan'] . '? <a href="./register.htm">' . $arraybien['taotaikhoanmoi'] . '</a></p>
               </div>
               <input type="hidden" name="hi_url" value="http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '" />
            </form>
           </div>
         </div>
        </div>';
    } else {
        $keycolspanin = ' in';
    }
    $data_giohang .= '
        <div class="panel panel-default">
         <div class="panel-heading">
           <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
            <i class="fa fa-user fa-2x"></i> ' . $arraybien['muahangkhongcandangky'] . '</a>
           </h4>
         </div>
         <div id="collapse2" class="panel-collapse collapse ' . $keycolspanin . '">
           <div class="panel-body">
            <form action="' . ROOT_PATH . 'checkout.htm" method="post" name="checkout" id="checkout">
               <div class="form-group form_giohang">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-mobile fa-lg"></i> ' . $arraybien['dienthoai'] . ' <span>(*)</span></div>
                    <input class="form-control" name="dienthoai" id="dienthoai" value="' . $_SESSION['user_sodienthoai'] . '">
                    <input type="hidden" value="' . $arraybien['vuilongnhapsodienthoai'] . '" name="dienthoai_alert">
                  </div>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user-md fa-lg"></i> ' . $arraybien['hoten'] . ' <span>(*)</span></div>
                    <input class="form-control" name="hoten" id="hoten" value="' . $_SESSION['user_hotendem'] . '' . $_SESSION['user_ten'] . '">
                    <input type="hidden" value="' . $arraybien['vuilongnhaphoten'] . '" name="hoten_alert">
                  </div>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope-o fa-lg"></i> ' . $arraybien['email'] . '</div>
                    <input class="form-control" name="email" id="email" value="' . $_SESSION['user_email'] . '">
                  </div>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-map-marker fa-lg"></i> ' . $arraybien['diachi'] . ' <span>(*)</span></div>
                    <input class="form-control" name="diachi" id="diachi" value="' . $_SESSION['user_diachi'] . '">
                    <input type="hidden" value="' . $arraybien['vuilongnhapdiachi'] . '" name="diachi_alert">
                  </div>';
                if( @$enable_tinthanh ) {
                    $data_giohang.='
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['tinhthanh'] . '</div>
                    ' . $data_thanhpho . '
                  </div>
                  <div id="loadquanhuyen">' . $data_quanhuyen . '</div>
                  <div id="loadphuongxa">' . $data_phuongxa . '</div>';
                }$data_giohang.='
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-comments fa-lg"></i> ' . $arraybien['ghichu'] . '</div>
                    <textarea class="form-control" rows="5" id="ghichu" name="ghichu">'.$_SESSION['user_ghichu'].'</textarea>
                  </div>
                  <input class="btn btn-primary btn_submit" type="submit" name="thanhtoan_giohang" id="thanhtoan_giohang" value="' . $arraybien['dathang'] . '" />
               </div>
            </form>
           </div>
         </div>
        </div>
      </div>';
}
$data_giohang .= '
   </div>'; // end cot form san pham
$data_giohang .= '
</div>';

$smarty->assign("giohang",$data_giohang,true);