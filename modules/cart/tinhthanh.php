<?php
// truy van lay thanh pho
$data_thanhpho = '';
if (isset($_GET['id']) or isset($_GET['view'])) {

    $id     = $_GET['id'];
    $method = $_GET['method'];
    if ($id != '') {
        if ($_GET['name'] == 'quanhuyen') {
            $s_thanhpho = "SELECT a.id,b.ten
                     FROM tbl_type AS a
                     INNER JOIN tbl_type_lang AS b
                     ON a.id = b.idtype
                     and b.idlang = '" . $_SESSION['_lang'] . "'
                     and a.anhien = 1
                     and a.id like '" . $id . "%'
                     and length(a.id) = 8
                     order by a.thutu ASC ";
            $d_thanhpho    = $db->sqlSelectSql($s_thanhpho);
            $data_thanhpho = '';
            if (count($d_thanhpho) > 0) {
                $data_thanhpho .= '<div class="input-group">
                       <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['quanhuyen'] . '</div>';
                $data_thanhpho .= '<select onchange="Get_Data(\'' . $_SESSION['ROOT_PATH'] . 'modules/giohang/giohang_tinhthanh.php?nam=phuongxa&id=\'+this.value,\'loadphuongxa\')" name="quanhuyen" id="quanhuyen" class="form-control">
                           <option value="">' . $arraybien['vuilongchon'] . '</option>';
                foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
                    $ten = $info_thanhpho['ten'];
                    $id  = $info_thanhpho['id'];
                    $data_thanhpho .= '<option value="' . $id . '">' . $ten . '</option>';
                }
                $data_thanhpho .= '</select>';
                $data_thanhpho .= '
                     </div>';
            }
        } else {
            $s_thanhpho = "SELECT a.id,b.ten
                     FROM tbl_type AS a
                     INNER JOIN tbl_type_lang AS b
                     ON a.id = b.idtype
                     and b.idlang = '" . $_SESSION['_lang'] . "'
                     and a.anhien = 1
                     and a.id like '" . $id . "%'
                     and length(a.id) = 12
                     order by a.thutu ASC ";
            $d_thanhpho    = $db->sqlSelectSql($s_thanhpho);
            $data_thanhpho = '';
            if (count($d_thanhpho) > 0) {
                $data_thanhpho .= '<div class="input-group">
                       <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> ' . $arraybien['phuongxa'] . '</div>';
                $data_thanhpho .= '<select  name="phuongxa" id="phuongxa" class="form-control">
                           <option value="">' . $arraybien['vuilongchon'] . '</option>';
                foreach ($d_thanhpho as $key_thanhpho => $info_thanhpho) {
                    $ten = $info_thanhpho['ten'];
                    $id  = $info_thanhpho['id'];
                    $data_thanhpho .= '<option value="' . $id . '">' . $ten . '</option>';
                }
                $data_thanhpho .= '</select>';
                $data_thanhpho .= '
                     </div>';
            }
        }
    }
}
echo $data_thanhpho;
