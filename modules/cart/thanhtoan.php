<?php
$data_giohang = '';
if (count($_SESSION['giohang']) <= 0 && !isset($_GET['sms'])) {
    $data_giohang.= '<script>alert("' . $arraybien['giohangrong'] . '"); location.href="' . ROOT_PATH . '"; </script>';
}

if (isset($_POST['dienthoai']) && isset($_POST['thanhtoan_giohang'])) {
    $dienthoai       = ws_post('dienthoai');
    $ten             = ws_post('hoten');
    $diachi          = ws_post('diachi');
    $email           = ws_post('email');
    $tinhthanh       = ws_post('tinhthanh');
    $quanhuyen       = ws_post('quanhuyen');
    $phuongxa        = ws_post('phuongxa');
    $ghichu          = ws_post('ghichu');
    $trangthai       = 0;
    $idlang          = $_SESSION['_lang'];
    $ngaydat         = $db->getDateTimes();
    $iduser          = $_SESSION['user_id'];
    $value_tinhthanh = $tinhthanh;
    if ($quanhuyen != '') {
        $value_tinhthanh = $quanhuyen;
    }
    if ($phuongxa != '') {
        $value_tinhthanh = $phuongxa;
    }

    $doidiem = null;
    if( isset($_SESSION['doidiem']) && lay_cauhinh_tichdiem() && $_SESSION['doidiem'] ) {
        $doidiem = $_SESSION['doidiem'];
    }

    // them vao don hang
    $arr_dathang = array(
        "iduser"      => $iduser,
        "ten"         => $ten,
        "email"       => $email,
        "diachi"      => $diachi,
        "dienthoai"   => $dienthoai,
        "tinhthanh"   => $value_tinhthanh,
        "ghichu"      => $ghichu,
        "phigiaohang" => $phigiaohang,
        "ngaydat"     => $ngaydat,
        "idlang"      => $idlang,
        "trangthai"   => $trangthai,
        "diemtichluy" => $doidiem,
    );
    $id_insert = $db->insert("tbl_donhang", $arr_dathang);
    // update ma don hang
    $arraycollumn = array("madonhang" => 'DH' . $id_insert);
    $result       = $db->sqlUpdate("tbl_donhang", $arraycollumn, "id = $id_insert ");

    // Điểm tích lũy
    $diemtichluy = null;
    if( isset($_SESSION['doidiem']) && lay_cauhinh_tichdiem() ) {
        // Trừ điểm thành viên
        $diemtichluy = ($doidiem - ($doidiem * 2));
        $tien_ra_diem = doi_tien_ra_diem($diemtichluy);
        if( $iduser && is_numeric($diemtichluy) ) {
            $db->insert('tbl_tichdiem', array(
                "iduser"    => $iduser,
                "iddonhang" => $id_insert,
                "anhien"    => 1,
                "sodiem"    => $tien_ra_diem,
                "lichsu"    => $db->lichSuTichDiem($tien_ra_diem,$id_insert,$iduser),
                "ngay"      => $db->now()
            ));
        }
        unset($_SESSION['doidiem']);
    }

    // them vao don hang chi tiet
    // them vao don hang
    if ( $id_insert ) {
        $arraytmp_giohang = $_SESSION['giohang'];
        if (count($arraytmp_giohang) > 0) {
            foreach ($arraytmp_giohang as $key_giohang => $info_giohang) {
                $idsanpham           = $info_giohang['idsp'];
                $soluong             = $info_giohang['soluong'];
                $gia                 = $info_giohang['gia'];
                $arr_dathang_chitiet = array(
                        "iddonhang"  => $id_insert,
                        "idsanpham"  => $idsanpham,
                        "soluong"    => $soluong,
                        "size"       => $size,
                        "dongia"     => $gia,
                        "trongluong" => $trongluong
                );
                $db->insert("tbl_donhangchitiet", $arr_dathang_chitiet);

            }
        }
        unset($_SESSION['giohang']);

        // Gửi Email
        if ($_getArrayconfig['sendemail'] == on) {
            $subject         = 'Đơn hàng ' . $_SERVER['HTTP_HOST'];
            $message         = $db->getThongTin('thongtindonhang');
            $noidung_donhang = file_get_contents(ROOT_PATH . 'adminweb/modules/donhang/donhang_guikhachhang.php?id=' . $id_insert);
            $message         = str_replace("[THONGTINDONHANG]", $noidung_donhang, $message);
            // hot mail
            $array_sendmail = array(
                "emailnhan" => $_getArrayconfig['email_nhan'],
                "emailgui"  => $_getArrayconfig['hostmail_user'],
                "hostmail"  => $_getArrayconfig['hostmail'],
                "user"      => $_getArrayconfig['hostmail_user'],
                "pass"      => $_getArrayconfig['hostmail_pass'],
                "tieude"    => $subject,
                "fullname"  => $_getArrayconfig['hostmail_fullname'],
                "port"      => $_getArrayconfig['hostmail_port'],
                "ssl"       => $_getArrayconfig['hostmail_ssl'],
                "subject"   => $subject,
                "message"   => $message,
            );
            sendmail($array_sendmail);
            // gui den nguoi nhan
            // kiem tra xem email da dung dinh dang thi se gui mail
            if($email!=''){
                //$partten = "/^[A-Za-z0-9_\.]{6,32}@([a-zA-Z0-9]{2,12})(\.[a-zA-Z]{2,12})+$/";
               // if(preg_match($partten ,$email, $matchs))
                    $array_sendmail = array(
                        "emailnhan" => $email,
                        "emailgui"  => $_getArrayconfig['hostmail_user'],
                        "hostmail"  => $_getArrayconfig['hostmail'],
                        "user"      => $_getArrayconfig['hostmail_user'],
                        "pass"      => $_getArrayconfig['hostmail_pass'],
                        "tieude"    => $subject,
                        "fullname"  => $_getArrayconfig['hostmail_fullname'],
                        "port"      => $_getArrayconfig['hostmail_port'],
                        "ssl"       => $_getArrayconfig['hostmail_ssl'],
                        "subject"   => $subject,
                        "message"   => $message,
                    );
                    sendmail($array_sendmail);
                //}
            }

        } // end send mail
        $data_giohang.= '<script>location.href="' . ROOT_PATH . 'checkout-success.htm"; </script>';
    } else {
        $data_giohang.= '<script>location.href="' . ROOT_PATH . 'checkout-error.htm"; </script>';
    }
} else if ($_GET['sms'] == 1) {
    $data_giohang.= '<div class="panel panel-default ws-container">
      <div class="panel-heading panel-heading-edit"><i class="fa fa-shopping-cart fa-2x"></i> ' . $arraybien['thongbao'] . '</div>
      <div class="panel-body">
         ' . $db->getThongTin('muahangthanhcong') . '
      </div>
   </div>';
} else if ($_GET['sms'] == 0) {
    $data_giohang.= '<div class="panel panel-default ws-container">
      <div class="panel-heading panel-heading-edit"><i class="fa fa-shopping-cart fa-2x"></i> ' . $arraybien['thongbao'] . '</div>
      <div class="panel-body">
         ' . $db->getThongTin('muahangthatbai') . '
      </div>
   </div>';
}
$smarty->assign("giohang",$data_giohang,true);