<?php

$res = null;

if ( isset($_SESSION['magiamgia']) ) {
    // Kiem tra

    $bageGiam =  "-". $soGiam.$_arrLoaiGiamGia[$kieugiam];
} else {
    $tongtien2 = $tongtien;
    $bageGiam = 0;
}

// DOI DIEM
$kichhoat_doidiem = lay_cauhinh_tichdiem();
if( $kichhoat_doidiem && isset($_SESSION['doidiem']) ) {
    $tongtien2 -= $_SESSION['doidiem'];
}

$res.= '
    <ul class="list-group">

        <li class="list-group-item">
            Tổng cộng: ' . number_format( $tongtien ) . ' VNĐ
        </li>

        ';

        if( isset($_SESSION['doidiem']) && $_SESSION['doidiem'] && $kichhoat_doidiem ) {
            $res.=
            '<li class="list-group-item">
                <div class="label label-success label-giamgia">đổi điểm: -'.number_format($_SESSION['doidiem']).' VNĐ</div>
            </li>';
        }
        if( $tongtien != $tongtien2 ) {
            $res.='
            <li class="list-group-item">
                ' . $arraybien['tongtien'] . ': ' . number_format( $tongtien2 ) . ' VNĐ
            </li>';
        }$res.='

    </ul>
';

return $res;
?>