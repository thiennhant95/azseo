<?php
    $ret = null;
    $db->addColSql('tbl_donhang', 'diemtichluy', 'int', 'masothe');

    // Kích hoạt đổi điểm mới hiện thị
    if( lay_cauhinh_tichdiem() ) {
        $ret.= '
        <div class="form-group">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title">
                        '.$arraybien['thanhtoanbangdiemtichluy'].'
                    </div><!-- /.panel-title -->
                </div><!-- /.panel-heading -->
                <div class="panel-body panel-doidiem">';
                $sodiem = $db->layTongDiem(get_id_user());
                $sotien_cauhinh = lay_cauhinh_tichdiem('sotien');
                $sotien_quydoi = ($sodiem * $sotien_cauhinh);
                $ret.= '
                    <label>
                        <input type="checkbox" name="doidiem" value="1" '.(
                            isset($_SESSION['doidiem']) ? 'checked' : null
                        ).' '.(
                            $sotien_quydoi ? null : 'disabled'
                        ).' />
                        '.$arraybien['sudung'].' <strong>'.$sodiem.'</strong> '.$arraybien['diem'].' ('.$arraybien['ungvoi'].' <strong>'.number_format($sotien_quydoi).'</strong> VNĐ) '.$arraybien['dethanhtoan'].'
                    </label>

                </div><!-- /.panel-body -->
            </div><!-- /.panel panel-success -->
        </div><!-- /.form-group -->';

    }
    return $ret;