<?php
if( session_id() == "") { session_start(); }
$data_giohang_view = '';
$tongsanpham       = count($_SESSION['giohang']);
if ($tongsanpham > 0) {
    $tongtien = 0;
    $data_giohang_view .= '
   <div class="giohang_group_view" id="giohang_load_view">
      <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-shopping-cart fa-2x"></i> ' . $arraybien['giohangcuaban'] . '</div>
      <div class="panel-body">';
    foreach ($_SESSION['giohang'] as $key_giohang => $info_giohang) {
            $hinh = $_SESSION['ROOT_PATH'].'uploads/noidung/thumb/'.$info_giohang['hinh'];
            $idsp = $info_giohang['idsp'];
        // lay ten tu database và lấy theo ngôn ngữ
        $ten                 = $db->getNameFromID('tbl_noidung_lang', 'ten', 'idnoidung', $idsp.' and idlang = '.$_SESSION['_lang']);
        $soluong             = $info_giohang['soluong'];
        $gia                 = $info_giohang['gia'];
        $tongtien            += ($soluong * $gia);
        $url                 = $db->getNameFromID('tbl_noidung_lang', 'url', 'idnoidung', $idsp.' and idlang = '.$_SESSION['_lang']);
        $link_sanpham        = $_SESSION['ROOT_PATH'].$url;
        $link_giohang_update = $_SESSION['ROOT_PATH'].'ajax/?op=giohang&id='.$idsp.'&i='.$key_giohang.'&method=update';
        $link_giohang_del    = $_SESSION['ROOT_PATH'].'ajax/?op=giohang&id='.$idsp.'&i='.$key_giohang.'&method=del';
            $data_giohang_view .= '
        <div class="product_box">
         <div class="product_img">
            <img onerror="xulyloi(this)" src="' .$hinh.'" alt="'.$tensanpham.'" width="70"  />
         </div>
         <div class="product_info">
            <div class="name"><div title="' .$arraybien['xoasanpham'].'" class="deletesp" onclick="MuaHang(\''.$link_giohang_del.'\',\'giohang_load_view\')">x</div><a href="'.$link_sanpham.'" title="'.$ten.'">'.$ten.'</a></div>
         </div>';
            $data_giohang_view .= '
         <div class="product_sl">
            <ul>
               <li class="li1">' .$arraybien['soluong'].'</li>
               <li><input onchange="MuaHang(\'' .$link_giohang_update.'&sl=\'+this.value,\'giohang_load_view\')" onkeyup="MuaHang(\''.$link_giohang_update.'&sl=\'+this.value,\'giohang_load_view\')" class="txt_soluong" name="C'.$key_giohang.'" type="number" min="1" max="100000" value="'.$soluong.'" size="4" maxlength="4" /></li>
               <li>&nbsp; X &nbsp;' .number_format($gia).' đ = '.number_format($soluong * $gia).' đ</li>
            </ul>
         </div>
         <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="product_line_nhe"></div>';
        }
    $data_giohang_view .= '
   <div class="tongtien_giohang">';

        $data_giohang_view .= include 'tongtien.inc.php';
        $data_giohang_view .='</div>
   ';
    $data_giohang_view .= '
   <div class="clear"></div>
   <div class="xoagiohang">
      <a onclick="if(confirm(\'' . $arraybien['xoagiohang'] . '\')){ return true;}else{return false; }" href="' . $_SESION['ROOT_PATH'] . 'delete-cart.htm" title="' . $arraybien['xoagiohang'] . '">' . $arraybien['xoagiohang'] . '<i class="fa fa-times-circle fa-lg"></i></a>
   </div>
   <a href="'.ROOT_PATH.'" title="'.$arraybien['tieptucmuahang'].'"><input class="btn btn-primary btn_submit" type="submit" name="thanhtoan_muatiep" id="thanhtoan_muatiep" value="' . $arraybien['tieptucmuahang'] . '" /></a>
   ';
    $data_giohang_view .= '<div class="clear"></div>
   </div>'; // end div tong
} else {
    $data_giohang_view .= $arraybien['giohangrong'];
}
$data_giohang_view .= '</div>
</div>';

return $data_giohang_view;

