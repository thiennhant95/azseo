<?php
return '
<div class="phuongthucthanhtoan clearfix form-group">
<div class="form-group clearfix panel panel-info">
    <div class="panel-heading">
        <div class="panel-title">
            '.$arraybien['phuongthucthanhtoan'].'
        </div>
    </div>
    <div class="panel-body">
        <div class="form-group radio thanhtoan-group">
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <label>
                <input type="radio" name="phuongthuc" value="0" checked />
                <strong> '.$arraybien['thanhtoankhinhanhang'].' </strong>
              </label>
            </div>
            <div class="col-sm-6 col-xs-12">
              <label>
                <input type="radio" name="phuongthuc" value="1" />
                <strong>'.$arraybien['thanhtoanquanganhang'].'</strong>
              </label>
            </div>
          </div>
        </div>
        <div class="thongtinthanhtoan clearfix alert alert-success">
          '.$db->getThongTin("thongtinthanhtoan").'
        </div>
    </div>
</div>
</div>
';
?>