<?php
$s_maps = "SELECT a.*,b.ten,b.diachi,b.zoom,b.toado
               FROM tbl_maps a INNER JOIN tbl_maps_lang b ON
               a.id = b.idtype
               WHERE a.anhien = 1
               AND b.idlang = 1
               ORDER BY a.thutu ASC";
$d_maps = $db->sqlSelectSql($s_maps);
// Get Background
$array_maps = $d_maps[0];
$databando  = '';
if ($array_maps['anhien'] == 1) {
    $phongto = ($array_maps['zoom'] == '') ? 17 : $array_maps['zoom'];
    $databando .= '
    <div id="bando" name="bando"></div>
   <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 480px;
        width: 100%;
      }
    </style>
  <div id="map"></div>
  <div id="capture"></div>
  <script>
      function initMap() {
        var chicago = new google.maps.LatLng(' . $array_maps['toado'] . ');
        var map = new google.maps.Map(document.getElementById("map"), {
          center: chicago,
          zoom: ' . $phongto . '
        });
        var coordInfoWindow = new google.maps.InfoWindow();
        coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
        coordInfoWindow.setPosition(chicago);
        coordInfoWindow.open(map);
        map.addListener("zoom_changed", function() {
          coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
          coordInfoWindow.open(map);
        });
      }
      var TILE_SIZE = 256;
      function createInfoWindowContent(latLng, zoom) {
        return [
        "<b>' . $array_maps['ten'] . '</b>",
        "<b>Address: </b>: ' . $array_maps['diachi'] . '" ,
        ].join("<br>");
      }
      function project(latLng) {
        var siny = Math.sin(latLng.lat() * Math.PI / 180);
        siny = Math.min(Math.max(siny, -0.9999), 0.9999);
        return new google.maps.Point(
            TILE_SIZE * (0.5 + latLng.lng() / 360),
            TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPAkptLAWq7Va0KnEHgTzdiZL3SnBdLO4&callback=initMap">
    </script>
';
}
return $databando;
