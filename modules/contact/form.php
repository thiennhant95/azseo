<?php
$exportlienhe = null;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $hoten     = $_POST['hoten'];
    $dienthoai = $_POST['dienthoai'];
    $email     = $_POST['email'];
    $tieude    = $_POST['tieude'];
    $noidung   = $_POST['noidung'];
    $arrloi    = array();
    $strloi    = "";
    if ($hoten == '') {
        $arrloi[] = 'nullhoten';
        $strloi   = $arraybien['banchuanhaphoten'];
    } elseif (strlen($hoten) < 2) {
        $arrloi[] = 'shorthoten';
        $strloi   = $arraybien['tenquanganvuilongnhaplai'];
    } elseif ($dienthoai == '') {
        $arrloi[] = 'nulldienthoai';
        $strloi   = $arraybien['banchuanhapdienthoai'];
    } elseif (!is_numeric($dienthoai)) {
        $arrloi[] = 'intdienthoai';
        $strloi   = $arraybien['dienthoaiphailaso'];
    } elseif (strlen($dienthoai) < 10 || strlen($dienthoai) > 11) {
        $arrloi[] = 'lendienthoai';
        $strloi   = $arraybien['dienthoaiphailonhon9vanhohon12so'];
    } elseif ($noidung == '') {
        $arrloi[] = 'nullnoidung';
        $strloi   = $arraybien['banchuanhapnoidunglienhe'];
    } elseif (strlen($noidung) < 6) {
        $arrloi[] = 'lennoidung';
        $strloi   = $arraybien['noidunglienhecanrorang'];
    }
    if (empty($arrloi)) {
        $your_ip = get_client_ip();
        // Thuc hien SQL
        $result_lienhe = $db->insert("tbl_lienhe", [
            'ten'       => $hoten,
            'email'     => $email,
            'dienthoai' => $dienthoai,
            'chude'     => $tieude,
            'noidung'   => $noidung,
            'ip'        => $your_ip,
            'ngaytao'   => $db->getDateTimes(),
        ]);
        if ($result_lienhe > 0) {
            // Nôi dung gui mail den admin
            if ( $_getArrayconfig['sendemail'] == on ) {
                $subject = $_SERVER['HTTP_HOST'] . ' Liên hệ';
                $message = '
                <table cellpadding=\'4\' cellspacing=\'0\' border=\'1\' style=\'border-collapse: collapse;border-color:#527fc0;width: 500px;\'>
                    <tr>
                        <td colspan="2" style="background-color: #527fc0;color: #FFF;">' . $arraybien['thongtinkhachhanglienhe'] . '</td>
                    </tr>
                    <tr>
                        <td>' . $arraybien['hoten'] . '</td><td>' . $hoten . '</td>
                    </tr>
                    <tr>
                        <td>' . $arraybien['email'] . '</td><td>' . $email . '</td>
                    </tr>
                    <tr>
                        <td>' . $arraybien['dienthoai'] . '</td><td>' . $dienthoai . '</td>
                    </tr>
                    <tr>
                        <td>' . $arraybien['chude'] . '</td><td>' . $tieude . '</td>
                    </tr>
                    <tr>
                        <td>' . $arraybien['noidung'] . '</td><td>' . $noidung . '</td>
                    </tr>
                </table>';
                // hot mail
                sendmail([
                    "emailnhan" => $_getArrayconfig['email_nhan'],
                    "emailgui"  => $_getArrayconfig['hostmail_user'],
                    "hostmail"  => $_getArrayconfig['hostmail'],
                    "user"      => $_getArrayconfig['hostmail_user'],
                    "pass"      => $_getArrayconfig['hostmail_pass'],
                    "tieude"    => $_getArrayconfig['hostmail_fullname'],
                    "fullname"  => $_getArrayconfig['hostmail_fullname'],
                    "port"      => 25,
                    "ssl"       => 0,
                    "subject"   => $subject,
                    "message"   => $message,
                ]);
            } // end send mail
        }
        if ($result_lienhe != '') {
            echo "<script>alert('Gửi liên hệ thành công, chúng tôi sẽ liên hệ lại với bạn sớm nhất')</script>";
            echo "<script>window.location.href='./'</script>";
        }
    }
}
$exportlienhe .= '
   <div class="panel panel-default">
      <div class="panel-heading">
         <h3 class="panel-title text-center"><label for="lienhe">' . $arraybien['thongtinlienhe'] . '</label></h3>
      </div>
      <div class="panel-body box-lienhe">
         <form action="" method="post" >';
if (isset($strloi) && $strloi != '') {
    $exportlienhe .= '
               <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  ' . $strloi . '
               </div>
            ';
}
$exportlienhe .= '
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user"></i>&nbsp;' . $arraybien['hoten'] . '</div>
                  <input type="text" name="hoten" class="form-control" id="" placeholder="' . $arraybien['vuilongnhaphovaten'] . '" value="' . (isset($_POST['hoten']) ? $_POST['hoten'] : null) . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-phone"></i>&nbsp;' . $arraybien['dienthoai'] . '</div>
                  <input type="text" name="dienthoai" class="form-control" id="" placeholder="' . $arraybien['vuilongnhapsodienthoai'] . '" value="' . (isset($_POST['dienthoai']) ? $_POST['dienthoai'] : null) . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-envelope"></i>&nbsp;' . $arraybien['email'] . '</div>
                  <input type="text" name="email" class="form-control" id="" placeholder="' . $arraybien['vuilongnhapemail'] . '" value="' . (isset($_POST['email']) ? $_POST['email'] : null) . '">
               </div>
            </div>
            
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-bell"></i>&nbsp;' . $arraybien['tieude'] . '</div>
                  <input type="text" class="form-control" name="tieude" id="" placeholder="' . $arraybien['vuilongnhaptieude'] . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-file-text-o"></i>&nbsp;' . $arraybien['noidung'] . '</div>
                  <textarea class="form-control" name="noidung" rows="3" placeholder="' . $arraybien['vuilongnhapnoidung'] . '">' . (isset($_POST['noidung']) ? $_POST['noidung'] : null) . '</textarea>
               </div>
            </div>
            <div class="text-center">
              <input type="submit" name="" class="btn btn-primary btn-lg" value="' . $arraybien['gui'] . '">
              <input type="reset" name="reset" class="btn btn-default btn-lg" value="' . $arraybien['nhaplai'] . '">
           </div>
         </form>
      </div>
   </div>
   ';
return $exportlienhe;
