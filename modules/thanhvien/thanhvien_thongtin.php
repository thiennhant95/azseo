<?php
$arrThanhvien = array();
if (isset($_SESSION['user_token'])) {
    $arrThanhvien['error'] = 'success';
    $arrThanhvien['message'] = '';
    if (isset($_SESSION['user_id'])) {
        $sUser = "SELECT tendangnhap,hotendem,ten,diachi,email,sodienthoai
                    FROM tbl_user
                WHERE id = {$_SESSION['user_id']}
                LIMIT 1";
        $qUser = $db->sqlSelectSql($sUser);
        if (count($qUser) > 0) {
            $arrTvien = array(
                "tendangnhap" => $qUser[0]['tendangnhap'],
                "hotendem"    => $qUser[0]['hotendem'],
                "ten"         => $qUser[0]['ten'],
                "diachi"      => $qUser[0]['diachi'],
                "email"       => $qUser[0]['email'],
                "sodienthoai" => $qUser[0]['sodienthoai'],
            );
            $smarty->assign("arrTvien", $arrTvien);
        }
    } else {
        $arrThanhvien['error'] = 'empty_u';
        $arrThanhvien['message'] = 'BẠN CHƯA ĐĂNG NHẬP, VUI LÒNG ĐĂNG NHẬP ĐỂ TIẾP TỤC.';
        header('refresh: 9; url='.ROOT_PATH);
    }
} else {
    $arrThanhvien['error'] = 'empty_token';
    $arrThanhvien['message'] = 'CÓ LỖI XẢY RA, VUI LÒNG THỬ LẠI (code: TK404)';
    unset($_SESSION['user_id']);
    header('refresh: 9; url='.ROOT_PATH);
}
$smarty->assign('arrThanhvien', $arrThanhvien);