<?php 
$s_spBanChay = "
    SELECT a.id,a.hinh,masp,gia,giagoc,keywebsite,a.idtype,a.ngaycapnhat,ten,url,link,target,mota,rating,ratingCount
    FROM tbl_noidung AS a
    INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
    WHERE a.anhien = 1
        AND b.idlang = {$_SESSION['_lang']}
        AND colvalue like '%noibat%'
    ORDER by a.thutu Asc
    LIMIT 120";
    $dspBanChay = $models->sql_select_sql($s_spBanChay);
    if (count($dspBanChay) > 0) {
        $dataHome['spBanChay'] = $dspBanChay;
    }
$smarty->assign("dataHome", $dataHome);