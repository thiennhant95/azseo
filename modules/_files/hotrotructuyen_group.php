<?php
$data_hotrotructuyen = '';
$s_group = "SELECT a.id,b.ten
                FROM tbl_hotrotructuyen_type AS a
                INNER JOIN tbl_hotrotructuyen_type_lang AS b
                WHERE a.id = b.idtype
                and anhien = 1
                and idlang = '" . $_SESSION['_lang'] . "'
                and a.id in(select idtype from tbl_hotrotructuyen)
                order by thutu ASC ";
$d_group = $db->sqlSelectSql($s_group);
if(count($d_group)>0){
    foreach($d_group as $key_group => $info_group){
        $id  = $info_group['id'];
        $ten = $info_group['ten'];
        $data_hotrotructuyen.='<div class="title_hotrotructuyen">'.$ten.'</div>';
        $s_hotrotructuyen = "SELECT yahoo,skype,dienthoai,email,ten
                        FROM tbl_hotrotructuyen AS a
                        INNER JOIN tbl_hotrotructuyen_lang AS b
                        WHERE a.id = b.idtype
                        and  a.idtype = '".$id."'
                        and anhien = 1
                        and idlang = '" . $_SESSION['_lang'] . "'
                        order by thutu ASC ";
        $d_hotrotructuyen    = $db->sqlSelectSql($s_hotrotructuyen);
        if (count($d_hotrotructuyen) > 0) {
            $data_hotrotructuyen .= '<div class="hotrotructuyen_content">';
            foreach ($d_hotrotructuyen as $key_hotrotructuyen => $info_hotrotructuyen) {
                $yahoo     = $info_hotrotructuyen['yahoo'];
                $skype     = $info_hotrotructuyen['skype'];
                $dienthoai = $info_hotrotructuyen['dienthoai'];
                $email     = $info_hotrotructuyen['email'];
                $ten       = $info_hotrotructuyen['ten'];
                $data_hotrotructuyen .= '
              <div class="row_hotrotructuyen">
              <div class="itemname"><i class="fa fa-user fa-2x"></i> ' . $ten . '</div>
              <div class="clear"></div>
              <div class="itemgroup">
              ';
                if ($skype != '') {
                    $data_hotrotructuyen .= '
                 <div class="itemskype">
                    <a title="' . $skype . '" href="skype:' . $skype . '?chat" data-toggle="tooltip"><i class="fa fa-skype fa-2x"></i></a>
                 </div>';
                }
                if ($dienthoai != '') {
                    $data_hotrotructuyen .= '
                 <div class="itemphone">
                    <a href="tel:' . $dienthoai . '" title="Call" data-toggle="tooltip"><i class="fa fa-phone fa-2x" ></i>' . $dienthoai . '</a>
                 </div>';
                }
                if ($email != '') {
                    $data_hotrotructuyen .= '
                 <div class="clear"></div>
                 <div class="itememail">
                    <a href="mailto:' . $email . '" title="Send mail" data-toggle="tooltip"><i class="fa fa-envelope fa-lg" ></i>' . $email . '</a>
                 </div>';
                }
                $data_hotrotructuyen .= '
                 <div class="clear"></div>
              </div>
              </div>';
            }
            $data_hotrotructuyen .= '<div class="clear"></div>
           </div>';
        }
    }
}

return $data_hotrotructuyen;