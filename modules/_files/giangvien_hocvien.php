<?php
// lay tieu su giang vien va hoc vien
$data_home_right = '';
if ($_op != 'home') {
    $s_danhmuc_bottom = "SELECT   a.id,b.ten,url,link,target
                FROM tbl_danhmuc AS a
                INNER JOIN tbl_danhmuc_lang AS b
                ON a.id = b.iddanhmuc
                and b.idlang = '" . $_SESSION['_lang'] . "'
                WHERE anhien = 1
                and home = 1
                order by thutu ASC";
    $d_danhmuc_bottom = $db->sqlSelectSql($s_danhmuc_bottom);
    if (count($d_danhmuc_bottom) > 0) {
        foreach ($d_danhmuc_bottom as $key_danhmuc_bottom => $info_danhmucbottom) {
            $ten     = $info_danhmucbottom['ten'];
            $id      = $info_danhmucbottom['id'];
            $url     = ROOT_PATH . $info_danhmucbottom['url'] . '/';
            $link    = $info_danhmucbottom['link'];
            $target  = $info_danhmucbottom['target'];
            $lienket = $url;
            if ($link != '') {
                $lienket = $lienket;
            }
            if ($target != '') {
                $target = ' target="_blank" ';
            }
            $s_tin_bottom = " select a.hinh,a.ngaycapnhat,ten,tieude,url,link,target,mota
                    FROM tbl_noidung AS a
                    INNER JOIN tbl_noidung_lang AS b
                    ON a.id = b.idnoidung
                    where a.anhien = 1
                    and b.idlang = '" . $_SESSION['_lang'] . "'
                    and a.idtype like '%" . $id . "%'
                    order by thutu Asc
                    limit 0,10";
            $d_tin_bottom = $db->sqlSelectSql($s_tin_bottom);
            if (count($d_tin_bottom) > 0) {
                $data_home_right .= '<div class="group_danhmuc_home border_none group_hocvien_right">';
                //<div class="title_bottom"><a '.$target.' href="'.$lienket.'" title="'.$ten.'">'.$ten.'</a></div>';
                $data_home_right .= '
         <div class="group_danhmuc_content">
                    <div  class="owl-right owl-carousel">';
                foreach ($d_tin_bottom as $key_tinbottom => $info_tin_bottom) {
                    $ten         = $info_tin_bottom['ten'];
                    $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_tin_bottom['hinh'];
                    $ngaycapnhat = $info_tin_bottom['ngaycapnhat'];
                    $tieude      = $info_tin_bottom['tieude'];
                    $url         = $info_tin_bottom['url'];
                    $link        = $info_tin_bottom['link'];
                    $target      = $info_tin_bottom['target'];
                    $mota        = $info_tin_bottom['mota'];
                    $noidung     = $info_tin_bottom['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_home_right .= '<div class="boxhome">
                           <div class="img"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" /><img onerror="xulyloi(this);" src="' . $hinh . '" alt="' . $ten . '" /></a></div>
                           <p class="tieude"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '" />' . $ten . '</a></p>
                           <div class="mota">' . $mota . '</div>
                        </div>';
                }
                $data_home_right .= '
               </div>
      </div>';
                $data_home_right .= '<div class="clear"></div>';
                $data_home_right .= '</div>'; // end group danh muc bottom
            }
        }
    }
}
return $data_home_right;
