<?php
$data_banner = '';
$d_banner = $db->sqlSelectSql("
    SELECT b.ten,hinh,linkhinh,manhung,link,target,rong,cao
    FROM tbl_banner AS a
    INNER JOIN tbl_banner_lang AS b ON a.id = b.idtype
    WHERE a.loai = 0
    AND b.idlang = {$_SESSION['_lang']}
    AND a.anhien = 1
    ORDER by thutu asc
    LIMIT 1
    ");
if (count($d_banner) > 0) {
    foreach ($d_banner as $key_banner => $info_banner) {
        $ten      = $info_banner['ten'];
        $hinh     = $info_banner['hinh'];
        $linkhinh = $info_banner['linkhinh'];
        $manhung  = $info_banner['manhung'];
        $link     = $info_banner['link'];
        $target   = $info_banner['target'];
        $rong     = $info_banner['rong'];
        $cao      = $info_banner['cao'];
        $width = '';
        $height = '';
        if (!empty($rong)) {
            $width = 'width="'.$rong.'"';
        }
        if (!empty($cao)) {
            $height = 'height="'.$cao.'"';
        }
        if (empty($link)) {
            $link = 'javascript:;';
        }
        if (!empty($target)) {
            $target = 'target="'.$target.'"';
        } else {
            $target = null;
        }
        $extfile      = pathinfo($hinh, PATHINFO_EXTENSION);
        $duongdanhinh = ROOT_PATH . 'uploads/logo/' . $hinh;
        if ($linkhinh != '') {
            $duongdanhinh = $linkhinh;
        }
        if ($manhung != '') {
            $data_banner .= '<div class="banner_item">' . $manhung . '</div>';
        } else {
            // kiem tra neu là flash thì đưa flash vào
            if ($extfile == 'swf') {
                $data_banner .= '
                <div class="banner_item banner_item'.$key_banner.'">
                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="' . $width . '" height="' . $height . '"><param name="movie" value="' . $duongdanhinh . '" /><param name="quality" value="high" /><param name="WMode" value="Transparent"/><embed wmode="transparent" src="' . $hinh . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '"></object>
                </div>';
            } else {
                $data_banner .= '
                <div class="banner-item banner_item banner_item'.$key_banner.'">
                <a href="'.$link.'" title="'.$ten.'" '.$target.'>
                <img src="'.$duongdanhinh.'" alt="'.$ten.'" '.$height.' '.$width.' />
                </a>
                </div>';
            }
        }
    }
}
// CHỌN NGÔN NGỮ
// $val_ngonngu = '';
// $val_ngonngu .= include "chonngonngu.php";
// CHẠY SLIDE DÒNG CHỮ
// $val_chayslide .='<span class="marquee">
// <marquee>'.$db->getThongTin("banner_slide").'</marquee>
// </span>';
// SEARCH
$val_search = '';
$val_search.='
<div class="cover-search">
<form action="'.ROOT_PATH.'" method="GET">
    <div class="box-search">
        <input type="hidden" name="op" value="search" />
        <input type="text" autocomplete="off" name="key" value="'.ws_get('key').'" id="searchtxt" class="form-control" placeholder="'.$arraybien['nhaptukhoatimkiem'].'"/>
        <button type="submit" class="btn btn-searchsubmit">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
        <div class="clear"></div>
    </div>
    <div id="resSearch">
    </div>
</form>
</div>';
$val_search.='<div class="underSearch">'.$db->getThongTin("underSearch").'</div>';
// GIỎ HÀNG
$val_cart = '';
if( $didong->isMobile() ) {
    $val_cart = '
    <div class="cart clearfix">
        <a href="'.ROOT_PATH.'cart.htm" title="cart">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
           <p> '.$arraybien['giohang'].'</p>
        </a>
    </div>
    ';
}
            // <span id="countCart">'.count($_SESSION['giohang']).'</span>
// ĐĂNG KÝ NHẬN KHUYẾN MÃI
$dk_banner = '';
$dk_banner .='
<div class="noidung_bottom">
    <div class="dkmail-frm clearfix">
        <input type="text" name="dksdt" class="alo" placeholder="'.$arraybien['dangkytuvan'].'" />
        <button type="button" data-loading-text="<i class=\'fa fa-spinner fa-spin fa-lg fa-fw\'></i>" id="dkmailBtn">'.$arraybien['gui'].'</button>
    </div>
</div>';
// HOTLINE
// USER
// show_error();
$user = null;
if( $didong->isMobile() ){

    if ( is_login() ) {
        $btnLogin = $smarty->fetch(ROOT_DIR . "/templates/layout1/modules/user/tpl/boxUserLogin.tpl");
    } else {
        $btnLogin = $smarty->fetch(ROOT_DIR . "/templates/layout1/modules/user/tpl/btn-login.tpl");
    }
    $user .= '
    <div class="user2 clearfix">';
        if( is_login() ) {
            $user.= '
            <i class="fa fa-user fa-2x"></i>';
            if( $didong->isMobile() ) {
                $user.= '
                <div id="showU">
                <p>'.$arraybien['xinchao'].'! '.$db->getUser('ten').'</p>
                <p onclick="historyPoint();">'.$db->layTongDiem(get_id_user()).' điểm</p>
                </div><!-- /#showU -->';
            }
        }$user.='
        '.$btnLogin.'
    </div>';
}
// $val_hotline = '';
// $val_hotline.= strip_tags($db->getThongTin("hotline"));
// HOTLINE
// THANH TOÁN
// $val_thanhtoan = '';
// $val_thanhtoan .= $db->getThongTin("banner_thanhtoan");
// BANNER
// SOCIAL
$social = '';
$social.= $db->getThongTin("socialBanner");
$val_banner = '';
$val_banner.=strtr('
    <div class="banner-row1">
        :USER
        :CART
        :BANNER
    </div><!-- /.banner-row1 -->
    <div class="banner-row2">
    <div class="banner-item search-box">
        :SEARCH
    </div>
    <div class="dk_banner"> :DK </div>
    <div id="btn-right"><i class="fa fa-bars"></i></div>
    </div><!-- /.banner-row2 -->
    ',
    array(
        ':BANNER'       => $data_banner,
        ':SOCIAL'       => $social,
        ':SEARCH'       => $val_search,
        ':DK'           => $dk_banner,
        ':CART'         => $val_cart,
        ':USER'         => $user,
        // ':HOTLINE'      => $val_hotline,
    // ':SLIDECHU'  => $val_chayslide,
    // ':THANHTOAN' => $val_thanhtoan
    // ':NGONNGU'   => $val_ngonngu
    ));
return $val_banner;