<?php
// lay danh muc check bottom
$data_home        = '';
$s_danhmuc_bottom = "
SELECT a.id,b.ten,url,link,target,colvalue
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
WHERE anhien = 1
AND b.idlang = {$_SESSION['_lang']}
AND colvalue like '%duoi%'
AND length(a.id) = 4
order by thutu ASC
LIMIT 3";
$d_danhmuc_bottom = $db->sqlSelectSql($s_danhmuc_bottom);
// echo '<pre>'; print_r($d_danhmuc_bottom); echo '</pre>';exit();

if (count($d_danhmuc_bottom) > 0) {
    // $data_home .='<div class="danhmucbottom">';
    // $data_home .='<div class="gioithieudanhmuc_bottom">'.$db->getThongTin("gioithieudanhmucbottom").'</div>';
    foreach ($d_danhmuc_bottom as $key_danhmuc_bottom => $info_danhmucbottom) {
        $ten     = $info_danhmucbottom['ten'];
        $id      = $info_danhmucbottom['id'];
        $url     = $info_danhmucbottom['url'];
        $link    = $info_danhmucbottom['link'];
        $target  = $info_danhmucbottom['target'];
        $colvalue = $info_danhmucbottom['colvalue'];
        $lienket = ROOT_PATH . $url . '/';
        $target_value = null;
        if ($link != '') {
            $lienket      = $link;
            $target_value = ' target="' . $target . '" ';
        }
        $data_home .= '
        <div class="group_bottom group_bottom_menu col-md-6 col-xs-12">
        <div class="title_bottom"><a href="' . $lienket . '" title="' . $ten . '">' . $ten . '</a></div>
        <div class="noidung_bottom">';
        $colvalue = explode(",", $colvalue);

        if (! in_array("baiviet", $colvalue) ) {
            // neu check bai viet se lay tieu de bai viet
            $s_danhmuc_bottom = "SELECT a.id,b.ten,url,link,target,baiviet
            FROM tbl_danhmuc AS a
            INNER JOIN tbl_danhmuc_lang AS b
            ON a.id = b.iddanhmuc
            WHERE anhien = 1
            AND b.idlang = {$_SESSION['_lang']}
            AND a.id like '{$id}%'
            AND length(a.id) = " . (strlen($id) + 4) . "
            and colvalue like '%duoi%'
            order by thutu ASC";
            $d_danhmuc_bottom = $db->sqlSelectSql($s_danhmuc_bottom);
            // echo '<pre>'; print_r($d_danhmuc_bottom); echo '</pre>';

            if (count($d_danhmuc_bottom) > 0) {
                $data_home .= '<ul>';
                foreach ($d_danhmuc_bottom as $key_danhmuc_bottom => $info_danhmucbottom) {
                    $ten     = $info_danhmucbottom['ten'];
                    $id      = $info_danhmucbottom['id'];
                    $url     = $info_danhmucbottom['url'];
                    $link    = $info_danhmucbottom['link'];
                    $target  = $info_danhmucbottom['target'];
                    $baiviet = $info_danhmucbottom['baiviet'];
                    $lienket = ROOT_PATH . $url . '/';
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_home .= '
                    <li>
                    <a href="' . $lienket . '" title="' . $ten . '" ' . $target_value . ' >
                    <i class="fa fa-circle-thin" aria-hidden="true"></i> ' . $ten . '
                    </a></li>';
                }
                $data_home .= '</ul>';
            }
        } else {

            $s_tin_bottom = "
            SELECT a.ngaycapnhat,ten,tieude,url,link,target,mota,noidung
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
            ON a.id = b.idnoidung
            WHERE a.anhien = 1
            AND b.idlang = '{$_SESSION['_lang']}'
            AND a.idtype LIKE '%{$id}%'
            ORDER by thutu Asc
            LIMIT 0,5
            ";
            $d_tin_bottom = $db->sqlSelectSql($s_tin_bottom);
            // echo '<pre>'; print_r($d_tin_bottom); echo '</pre>'; exit();

            if (count($d_tin_bottom) > 0) {
                $data_home .= '<ul>';
                foreach ($d_tin_bottom as $key_tinbottom => $info_tin_bottom) {
                    $ten         = $info_tin_bottom['ten'];
                    $ngaycapnhat = $info_tin_bottom['ngaycapnhat'];
                    $tieude      = $info_tin_bottom['tieude'];
                    $url         = $info_tin_bottom['url'];
                    $link        = $info_tin_bottom['link'];
                    $target      = $info_tin_bottom['target'];
                    $mota        = $info_tin_bottom['mota'];
                    $noidung     = $info_tin_bottom['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_home .= '<li>
                    <a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '">
                    <i class="fa fa-circle-thin" aria-hidden="true"></i> ' . $ten . '
                    </a></li>';
                }
                $data_home .= '</ul>';
            }
        }
        $data_home .= '
        </div>
        </div>';
    }
    // $data_home .='</div>';
}

return $data_home;
