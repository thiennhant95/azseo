<?php
$valTOP = '';

// MENU TOP
$q_menu_top = $db->layDanhMuc([
    "where" => "AND colvalue LIKE '%tren%'"
]);

if( count($q_menu_top) > 0 ){
    $valTOP.='
    <ul class="menu-top clearfix">';
        foreach( $q_menu_top as $k_top => $itop ){
            $top_name = $itop['ten'];
            $top_link = ROOT_PATH . $itop['url'] . '/';

            $valTOP.='
            <li>
                <a href="'.$top_link.'" title="'.$top_name.'">
                    '.$top_name.'
                </a>
            </li>';
        }
        $email = strip_tags($db->getThongTin("email"));
        $valTOP.='
        <li><a href="mailto:'.$email.'" title="Email">Email: '.$email.'</a></li>
    </ul>';
}
// END MENU TOP

// GIỚI THIỆU CÔNG TY
$valTOP.='<div class="gioithieucongty">'.$db->getThongTin("gioithieutop").'</div>';

// GIỎ HÀNG
$valTOP .= '
<div class="cart">
<a href="'.ROOT_PATH.'cart.htm" title="cart">
<p><i class="fa fa-shopping-cart" aria-hidden="true"></i></p>
'.$arraybien['giohangcuaban'].'
</a>
</div>';

// HOTLINE TOP
$hotline = strip_tags($db->getThongTin("hotline_top"));
$valTOP.='
<ul class="hotline-top">
    <!--<li>
        Hotline: <strong> '.$hotline .'</strong>
    </li>-->
    <!--<li>
        '.$db->getThongTin("like_top").'
    </li>-->
    <li class="login-box">';
        if( is_login() ){
            $valTOP.='
            <span class="sodiem cu-pointer" onclick="historyPoint();">
                '.$arraybien['bandangco'].'
                <b>'.$db->layTongDiem(get_id_user()).'</b>
                '.$arraybien['diem'].'
            </span><!-- /.sodiem -->';
        }
        if( is_login() ){
            $valTOP.= $smarty->fetch(ROOT_DIR."/templates/layout1/modules/user/tpl/login_success.tpl");
        }else{
            $valTOP.= $smarty->fetch(ROOT_DIR."/templates/layout1/modules/user/tpl/btn-login.tpl");
        }$valTOP.='
    </li>
</ul>';


// HỖ TRỢ KHÁCH HÀNG
// $valTOP.='<div class="hotrokhachhangtop">'.$db->getThongTin("hotrokhachhangtop").'</div>';



return $valTOP;
