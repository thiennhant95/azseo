<?php
$d_slide = $db->sqlSelectSql("
    SELECT b.ten,hinh,linkhinh,manhung,link,target,rong,cao
    FROM tbl_banner AS a
    INNER join tbl_banner_lang AS b On a.id = b.idtype
    WHERE a.loai = 1
        AND b.idlang = {$_SESSION['_lang']}
        AND a.anhien = 1
		AND ".($didong->isMobile() ? " mobile = 1 " : " mobile = 0 ")."
    ORDER by thutu asc
");
$data_slide = '';

if (count($d_slide) > 0) {
    $data_slide .= '<div class="owl-slide slide owl-carousel owl-theme" data-sumslide="' . count($d_slide) . '">';
    foreach ($d_slide as $key_banner => $info_banner) {
        $banner_ten      = $info_banner['ten'];
        $banner_hinh     = !empty($info_banner['linkhinh']) ? $info_banner['linkhinh']
                            : ROOT_PATH . 'uploads/logo/' . $info_banner['hinh'];
        $banner_manhung  = $info_banner['manhung'];
        $banner_link     = !empty($info_banner['link']) ?
                            'href="' . $info_banner['link'] . '"': 'href="javascript:;"';
        $banner_target   = !empty($info_banner['target']) ?
                            'target="'.$info_banner['target'].'"' : null;
        $banner_rong     = $info_banner['rong'];
        $banner_cao      = $info_banner['cao'];
        if (empty($info_banner['linkhinh'])) {
            if (file_exists('uploads/logo/' . $info_banner['hinh'])) {
                $data_slide .= '<div>
                <a '.$target.' ' . $banner_link . ' title="' . $banner_ten . '">
                    <img src="' . $banner_hinh . '" alt="' . $banner_ten . '"  title="' . $banner_ten . '"/>
                    </a>
                </div>';
            }
        } else {
            $data_slide .= '<div>
            <a '.$target.'  ' . $banner_link . ' title="' . $banner_ten . '">
                <img src="' . $banner_hinh . '" alt="' . $banner_ten . '" title="' . $banner_ten . '" />
            </a></div>';
        }
    }
    $data_slide .= '</div>';
}
return $data_slide;
