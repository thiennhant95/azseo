<?php

$eCatP = "
SELECT a.id,a.idtype,img,b.ten,url,link,target,colvalue,icon,bgcolor,mota
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
AND b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
AND colvalue LIKE '%noibat%'
ORDER BY thutu ASC
LIMIT 50";
$fCatP = $db->sqlSelectSql($eCatP);
// echo '<pre>'; print_r($fCatP); echo '</pre>';exit();
$data_menu = '';
if (count($fCatP) > 0) {
	foreach ($fCatP as $kdm => $vdm){
		$id       = $vdm['id'];
		$idtype   = $vdm['idtype'];
		

		$catOP = $db->getNameFromID("tbl_danhmuc_type", "op", "id", "'{$idtype}'");
        if ($catOP == 'product' || $catOP == 'content' || $catOP == 'service' || $catOP=='intro' || $catOP=='video' || $catOP=='project') {

			$d_sp = "
			SELECT a.id,a.hinh,keywebsite,a.idtype,mota,ten,url,hinh,link,target,gia
			FROM tbl_noidung AS a
			INNER JOIN tbl_noidung_lang AS b
			ON a.id = b.idnoidung
			WHERE a.anhien = 1
			AND b.idlang = {$_SESSION['_lang']}
			AND a.idtype like '%{$id}%'
			ORDER by a.thutu Asc
			LIMIT 4";
			$eSP = $db->sqlSelectSql($d_sp);
            // echo '<pre>'; print_r($eSP); echo '</pre>'; exit();
			if (count($eSP) > 0) {
				$data_menu .= '<div class="menunoibat">';
				foreach ($eSP as $kdm => $vdm){
					$ten      = $vdm['ten'];
					$img      = $vdm['hinh'];
					$mota     = $vdm['mota'];
					if (!empty($vdm['link'])){
						$lienket = $vdm['link'];
					}
					else{
						$lienket = ROOT_PATH.$vdm['url'];
					}
					$linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$img;

					$data_menu .= '<div class="noidung_menunoibat">';
					$data_menu .= '<div class="img">
					<img src="'.$linkhinh.'"/>
					</div>';
					$data_menu .= '<div class="ten">'.$ten.'</div>';
					$data_menu .= '<div class="mota">'.$mota.'</div>';
					$data_menu .= '<a href="'.$lienket.'" class="readmore">'.$arraybien['xemchitiet'].'</a>';
					$data_menu .= '</div>';
				}
				$data_menu .= '</div>';
			}
		}

	}
}

$s_menu = "
SELECT a.id,a.hinh,masp,gia,giagoc,a.idtype,
a.ngaycapnhat,tag,ten,url,link,target,mota,noidung,
rating,ratingCount,colvalue {$select}
FROM tbl_noidung AS a
INNER JOIN tbl_noidung_lang AS b
ON a.id = b.idnoidung
WHERE a.anhien = 1
AND b.idlang = {$_SESSION['_lang']}
AND colvalue like 'noibat'
ORDER by thutu ASC
";
$d_menu = $db->sqlSelectSql($s_menu);
// echo '<pre>'; print_r($d_menu); echo '</pre>'; exit();

$data_menu ='';
if(count($d_menu)>0){
	$data_menu .= '<div class="menunoibat">';
	foreach ($d_menu as $kdm => $vdm){
		$ten      = $vdm['ten'];
		$img      = $vdm['img'];
		$mota     = $vdm['mota'];
		if (!empty($vdm['link'])){
			$lienket = $vdm['link'];
		}
		else{
			$lienket = ROOT_PATH.$vdm['url'].'/';
		}
		$linkhinh = ROOT_PATH.'uploads/danhmuc/'.$img;

		$data_menu .= '<div class="noidung_menunoibat">';
		$data_menu .= '<div class="img">
		<img src="'.$linkhinh.'"/>
		</div>';
		$data_menu .= '<div class="ten">'.$ten.'</div>';
		$data_menu .= '<div class="mota">'.$mota.'</div>';
		$data_menu .= '<a href="'.$lienket.'" class="readmore">'.$arraybien['xemchitiet'].'</a>';
		$data_menu .= '</div>';
	}
	$data_menu .= '</div>';
}


return $data_menu;
