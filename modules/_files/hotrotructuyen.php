<?php
$s_hotrotructuyen = "
SELECT yahoo,skype,dienthoai,email,ten,facebook
FROM tbl_hotrotructuyen AS a
INNER JOIN tbl_hotrotructuyen_lang AS b
WHERE a.id = b.idtype
and anhien = 1
and idlang = '" . $_SESSION['_lang'] . "'
order by thutu ASC ";
$d_hotrotructuyen    = $db->sqlSelectSql($s_hotrotructuyen);
// echo '<pre>'; print_r($d_hotrotructuyen); echo '</pre>'; exit();

$data_hotrotructuyen = '';
if (count($d_hotrotructuyen) > 0) {
  $data_hotrotructuyen .= '<div class="hotrotructuyen_content">';
  foreach ($d_hotrotructuyen as $key_hotrotructuyen => $info_hotrotructuyen) {
    $yahoo     = $info_hotrotructuyen['yahoo'];
    $skype     = $info_hotrotructuyen['skype'];
    $dienthoai = $info_hotrotructuyen['dienthoai'];
    $email     = $info_hotrotructuyen['email'];
    $ten       = $info_hotrotructuyen['ten'];
    $facebook  = $info_hotrotructuyen['facebook'];
    $data_hotrotructuyen .= '
    <div class="row_hotrotructuyen">
    <div class="clear"></div>
    <div class="itemgroup">
    ';
    if ($dienthoai != '') {
      $data_hotrotructuyen .= '
      <div class="itemphone">
      <a href="tel:' . $dienthoai . '" title="Call" data-toggle="tooltip">
      <i class="fa fa-mobile" aria-hidden="true"></i>
      <span>Hotline</br>' . $dienthoai . '</span>
      </a>
      </div>';
    }
    if ($email != '') {
      $data_hotrotructuyen .= '
      <div class="clear"></div>
      <div class="itememail">
      <a href="mailto:' . $email . '" title="Send mail" data-toggle="tooltip">
      <i class="fa fa-envelope fa-lg" ></i>
      <span>Mail</br>' . $email . '</span>
      </a>
      </div>';
    }
    if ($skype != '') {
      $data_hotrotructuyen .= '
      <div class="itemskype">
      <a title="' . $skype . '" href="skype:' . $skype . '?chat" data-toggle="tooltip">
      <i class="fa fa-skype fa-2x"></i>
      <span>Skype</br>' . $skype . '</span>
      </a>
      </div>';
    }
    if ($facebook != '') {
      $data_hotrotructuyen .= '
      <div class="itemfacebook">
      <a title="' . $facebook . '" href="' . $facebook . '" data-toggle="tooltip">
      <i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</br>' . $facebook . '</span>
      </a>
      </div>';
    }
    $data_hotrotructuyen .= '
    <div class="clear"></div>
    </div>
    </div>';
  }
  $data_hotrotructuyen .= '<div class="clear"></div>
  </div>';
}
return $data_hotrotructuyen;
