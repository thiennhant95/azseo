<?php

$taisao = '';

$eCatP = "SELECT a.id,a.idtype,img,b.ten,url,link,target,colvalue,icon,bgcolor,mota
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
AND b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
AND colvalue LIKE '%taisao%'
ORDER BY thutu ASC
LIMIT 50";
$fCatP = $db->sqlSelectSql($eCatP);
// echo '<pre>'; print_r($fCatP); echo '</pre>'; exit();

if (count($fCatP) > 0) {
	foreach ($fCatP as $kdm => $vdm){
		$ten        = $vdm['ten'];
		$bgcolor    = $vdm['bgcolor'];
		$background = 'style="background-color:#'.$bgcolor.'"';

		$taisao .='<div class="taisaochonngoaingu" '.$background.'>';
		$taisao .='<div class="bentrong">';

		$taisao .='<div class="titlebase-home clearfix wow animated fadeInUp" data-wow-delay="0s">';
		$taisao .='<h2><a href ="javascript:;" title="'.$ten.'">'.$ten.'</a></h2>';
		$taisao .='</div>';

		$taisao .='<div class="noidung">'.$db->getThongTin("taisaochonngoaingu").'</div>';

		$taisao .='</div>';
		$taisao .='</div>';
	}
}

return $taisao;