<?php
$data_bottom = '';
$idtype = $db->getNameFromID("tbl_banner_type","id","keyname","'bannerbottom'");
$s_banner = "select b.ten,hinh,linkhinh,manhung,link,target,rong,cao
         from tbl_banner AS a
         inner join tbl_banner_lang AS b On a.id = b.idtype
         where a.loai = 2
         and b.idlang = " . $_SESSION['_lang'] . "
         and a.idtype = ".$idtype."
         and a.anhien = 1
         order by thutu asc ";
$d_banner    = $db->sqlSelectSql($s_banner);
if (count($d_banner) > 0) {
    foreach ($d_banner as $key_banner => $info_banner) {
        $ten      = $info_banner['ten'];
        $hinh     = $info_banner['hinh'];
        $linkhinh = $info_banner['linkhinh'];
        $manhung  = $info_banner['manhung'];
        $link     = $info_banner['link'];
        $target   = $info_banner['target'];
        $rong     = $info_banner['rong'];
        $cao      = $info_banner['cao'];
        if ((int) $rong > 0) {
            $rong = $rong . 'px';
        }
        if ((int) $cao > 0) {
            $cao = $cao . 'px';
        }
        $extfile      = pathinfo($hinh, PATHINFO_EXTENSION);
        $duongdanhinh = ROOT_PATH . 'uploads/logo/' . $hinh;
        if ($linkhinh != '') {
            $duongdanhinh = $linkhinh;
        }
        if ($manhung != '') {
            $data_bottom .= '<div class="banner_item">' . $manhung . '</div>';
        } else {
        	$data_bottom.='<div class="banner_item">';
            // kiem tra neu là flash thì đưa flash vào
            if ($extfile == 'swf') {
                $data_bottom .= '
            <object  classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="' . $rong . '" height="' . $cao . '">
<param name="movie" value="' . $duongdanhinh . '" />
<param name="quality" value="high" /><param name="WMode" value="Transparent"/><embed wmode="transparent" src="' . $hinh . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $rong . '" height="' . $cao . '"><embed></object>
           ';
            } else {
                $data_bottom .= '<a href="' . $link . '" title="' . $ten . '" target="' . $target . '"><img width="' . $rong . '" height="' . $cao . '" onerror="xulyloi(this);" src="' . $duongdanhinh . '" alt="' . $ten . '" title="' . $ten . '" /></a>';
            }
            $data_bottom.='</div>';
        }
    }
}


return $data_bottom;

?>