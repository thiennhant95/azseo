<?php
    $data = $res = null;

    $qcat = $db->layDanhMuc([
        // "op"    => "product",
        "select" => "a.idtype,img,colvalue",
        "where" => " AND colvalue LIKE '%bottomfix%' ",
        "orderby" => ["thutu","asc"],
        // "debug" => false
    ]);
    // echo '<br ><br />';
    // print_r($qcat);
    if( count($qcat) > 0 ) {
        foreach( $qcat as $cat ) {

            $id = $cat['id'];
            $idtype = $cat['idtype'];
            $ten = $cat['ten'];
            $img = $cat['img'];
            $colvalue = $cat['colvalue'];
            $link = ROOT_PATH . $cat['url'] . "/";
            $db->where("id", $idtype);
            $op_select = $db->getOne("tbl_danhmuc_type");
            $op_select = $op_select['op'];
            $arr_colvalue = explode("dmcon",$colvalue);
            // check danh mục con
            $dmcon = 0;
            if(count($arr_colvalue)>1){
                $dmcon = 1;
            }
            $data_bg = '';
            if($img){
                $data_bg = ' style = background-image:url("'.ROOT_PATH.'uploads/danhmuc/'.$img.'"); background-repeat:no-repeat; background-size:100%; ';
            }
            $data.= '
            <div class="homepage-item clearfix" '.$data_bg.'>
                <div class="homepage-content">
                    <div class="spnb-title titlebase">
                        <a href="javascript:;" rel="nofollow" title="'.$ten.'">
                            <span>
                                '.$ten.'
                            </span>
                            <span class="tria"></span>
                        </a>
                    </div><!-- /.spnb-title -->
            ';

            // <div class="icon">';
            //     if( $cat['icon'] ) {
            //         $data.= get_icon($cat['icon'], ROOT_PATH."uploads/danhmuc/");
            //     }
            //     else {
            //         $data.='
            //         <i class="fa fa-star-o" aria-hidden="true"></i>';
            //     }
            //     $data.='
            // </div>

            if($dmcon==1){
                $strlenid = strlen($id)+4;
                $arrsp = $db->layDanhMuc([
                    // "op"    => "product",
                    "select" => "a.idtype,img",
                    "where" => " AND colvalue LIKE '%dmcon%' AND a.id like '%{$id}%' AND length(a.id) = {$strlenid}",
                    "orderby" => ["thutu","asc"],
                    // "debug" => false
                ]);
            }else{
                // Lấy Dữ liệu
                $arrsp = $db->layNoiDung([
                    // "op" => "product",
                    "where" => "AND colvalue LIKE '%home%' AND idtype LIKE '%{$id}%' ",
                    "limit" => 8
                ]);
            }


            if( count($arrsp) > 0 ) {
                $smarty->assign("arrnoidung", $arrsp);
                $data.= '
                <div class="sanphamtrangchu clearfix">';
                $linkop = "/templates/layout1/modules/".$op_select."/tpl/item.tpl";
                if($op_select=='intro'){
                    $data.= '
                    <div class="intro_home">
                        <div class="img">
                            <img src="'.ROOT_PATH.'uploads/noidung/'.$arrsp[0]['hinh'].'" alt="'.$arrsp[0]['ten'].'" title="'.$arrsp[0]['ten'].'" />
                        </div>
                        <div class="mota">
                            '.$arrsp[0]['mota'].'
                        </div>
                    </div>
                    ';

                }elseif($op_select=='project'){
                    $smarty->assign("owl",'projectRun');
                    $smarty->assign("rowsp",1);
                    $linkop = "/templates/layout1/modules/".$op_select."/tpl/item.tpl";
                    $data.= $smarty->fetch(ROOT_DIR .$linkop);
                }elseif($op_select=='video'){
                    // $smarty->assign("owl",'videoRun');
                    // $smarty->assign("rowsp",1);
                    $smarty->assign("arrsanpham", $arrsp);
                    $linkop = "/templates/layout1/modules/".$op_select."/tpl/item.tpl";
                    $data.= $smarty->fetch(ROOT_DIR .$linkop);
                }elseif($op_select=='product'){
                    $smarty->assign("owl",'productRun');
                    $smarty->assign("rowsp",1);
                    $linkop = "/templates/layout1/modules/".$op_select."/tpl/item.tpl";
                    $data.= $smarty->fetch(ROOT_DIR .$linkop);
                }elseif($op_select=='service'){
                    $smarty->assign("owl",'serviceRun');
                    $linkop = "/templates/layout1/modules/".$op_select."/tpl/item_dm.tpl";
                    $data.= $smarty->fetch(ROOT_DIR .$linkop);
                }elseif($op_select=='content'){
                    // $smarty->assign("owl",'contentRun');
                    // $smarty->assign("rowsp", 2);
                    $linkop = "/templates/layout1/modules/".$op_select."/tpl/item.tpl";
                    $data.= $smarty->fetch(ROOT_DIR .$linkop);
                }else{
                    $smarty->assign("owl",null);
                    $data.= $smarty->fetch(ROOT_DIR .$linkop);
                }
                $data.='
                </div><!-- /.sanphamtrangchu -->';

                // if( count($arrsp) >= 8 ) {
                //     $data.= '
                //     <div class="xemthemsanpham">
                //         <a href="'.$link.'" title="'.$arraybien['xemthemsanpham'].'">
                //             '.$arraybien['xemthemsanpham'].'
                //         </a>
                //     </div>';
                // }
            }

            $data.='
                </div><!--/.home-content -->
            </div><!-- /.homepage-item -->';
        }
    }

    // RENDER
    $res.= strtr('
            :sanphamtrangchu
    ',
    array(
        ':sanphamtrangchu' => $data,
    ));

    return $res;