<?php

// NỘI DUNG TIN TỨC TRÁI
$v_news = '';
$c_news = '';
$d_news = '';

$qCatP = $db->layDanhMuc([
	"where"  => " AND colvalue LIKE '%tintucsukien%'",
	"select" => "idtype",
]);
// echo '<pre>'; print_r($qCatP); echo '</pre>'; exit();
if (count($qCatP) > 0) {
	foreach ($qCatP as $kdm => $vdm) {
		$id     = $vdm['id'];
		$idtype = $vdm['idtype'];
		$ten    = $vdm['ten'];
		$url    = $vdm['url'];
		$link   = ROOT_PATH.$url.'/';
		$icon   = $vdm['icon'];
		// echo '<pre>'; print_r($icon); echo '</pre>'; exit();
		
        if (substr($icon,0,2) == 'fa'){
        	$iconfile = '<span class="img"><i class="fa '.$iconfile.'"></i></span>';
        }
        else{
        	$iconfile = '<span class="img"><img src="'.ROOT_PATH.'uploads/danhmuc/'.$icon.'" alt="'.$ten.'" /></span>';
        }
        // echo '<pre>'; print_r($iconfile); echo '</pre>'; exit();

		$v_news .= '<div class="titlebase-home clearfix wow animated fadeInUp" data-wow-delay="0s">';
		$v_news .= '<h2>
		<a href="'.$link.'" title="'.$ten.'">'.$iconfile.'
		<span class="tentieude">'.$ten.'</span>
		</a></h2>';
		$v_news .= '</div>';

		$catOP = $db->getNameFromID("tbl_danhmuc_type", "op", "id", "'{$idtype}'");
		if ($catOP == 'product' || $catOP == 'content' || $catOP == 'service' || $catOP=='intro' || $catOP=='video' || $catOP=='project') {
			$dSP = $db->layNoiDung([
				"where"   => "AND a.idtype LIKE '%{$id}%'
				AND colvalue LIKE '%tintuctrai%' ",
				"orderby" => ['thutu','Asc'],
				"limit"   => 1
			]);
			// echo '<pre>'; print_r($dSP); echo '</pre>'; exit();
			if (count($dSP) > 0) {
				$c_news .='<ul class="tintucsukien_trai">';
				foreach($dSP as $key_noidung => $info_noidung){
					$id       = $info_noidung['id'];
					$ten      = $info_noidung['ten'];
					$mota     = $info_noidung['mota'];
					$url      = $info_noidung['url'];
					$hinh     = $info_noidung['hinh'];
					$linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$hinh;
					$link     = ROOT_PATH.$url;
					$c_news.='<li>';
					$c_news.='<div class="noidungcontent">';
					$c_news.='<div class="img"><img src="'.$linkhinh.'" alt="'.$ten.'" /></div>';
					
					$c_news.='<div class="ten">
					<a href="'.$link.'" title="'.$ten.'">'.$ten.'</a>
					</div>';
					$c_news.='<div class="mota">'.substr($mota,0,243).'</div>';
					$c_news.='</div>';
					$c_news.='</li>';
				}
				$c_news.='</ul>';				
			}
		}
	}
}


// NỘI DUNG TIN TỨC PHẢI
$qCatP = $db->layDanhMuc([
	"where"  => " AND colvalue LIKE '%tintucsukien%'",
	"select" => "idtype",
]);
// echo '<pre>'; print_r($qCatP); echo '</pre>'; exit();
if (count($qCatP) > 0) {
	foreach ($qCatP as $kdm => $vdm) {
		$id     = $vdm['id'];

		$catOP = $db->getNameFromID("tbl_danhmuc_type", "op", "id", "'{$idtype}'");
		if ($catOP == 'product' || $catOP == 'content' || $catOP == 'service' || $catOP=='intro' || $catOP=='video' || $catOP=='project') {
			$dSP = $db->layNoiDung([
				"where"   => "AND a.idtype LIKE '%{$id}%'
				AND colvalue LIKE '%tintucphai%' ",
				"orderby" => ['thutu','Asc'],
				"limit"   => 6
			]);
			// echo '<pre>'; print_r($dSP); echo '</pre>'; exit();
			if (count($dSP) > 0) {
				$d_news .='<ul class="tintucsukien_phai">';
				foreach($dSP as $key_noidung => $info_noidung){
					$id       = $info_noidung['id'];
					$ten      = $info_noidung['ten'];
					$mota     = $info_noidung['mota'];
					$url      = $info_noidung['url'];
					$hinh     = $info_noidung['hinh'];
					$linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$hinh;
					$link     = ROOT_PATH.$url;
					$d_news.='<li>';
					$d_news.='<div class="noidungcontent">';
					$d_news.='<div class="img"><img src="'.$linkhinh.'" alt="'.$ten.'" /></div>';
					$d_news.='<div class="noidung">';
					$d_news.='<div class="ten">
					<a href="'.$link.'" title="'.$ten.'">'.substr($ten,0,50).'</a></div>';
					$d_news.='<div class="mota">'.substr($mota,0,115).'</div>';
					$d_news.='</div>';
					$d_news.='</div>';
					$d_news.='</li>';
				}
				$d_news.='</ul>';				
				// echo '<pre>'; print_r($d_news); echo '</pre>'; exit();
			}
		}
	}
}


$data_bottom_fix = '';
$data_bottom_fix .= $v_news;
$data_bottom_fix .='<ul class="ul_bottomfix">';
$data_bottom_fix .='<li class="noidungtrai wow animated slideInLeft">'.$c_news.'</li>';
$data_bottom_fix .='<li class="noidungphai wow animated slideInRight">'.$d_news.'</li>';
$data_bottom_fix .='</ul>';

return $data_bottom_fix;