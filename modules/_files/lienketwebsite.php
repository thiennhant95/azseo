<?php
$s_tienich = "select a.id,b.ten,b.link,b.target
              from tbl_lienketwebsite As a
              inner join tbl_lienketwebsite_lang As b
              where a.id = b.idtype
              and b.idlang = '" . $_SESSION['_lang'] . "'
              and a.anhien = 1";
$d_tienich           = $db->sqlSelectSql($s_tienich);
$data_lienketwebsite = '';
if (count($d_tienich) > 0) {
    $data_lienketwebsite .= '<ul class="lienketwebsite">';
    foreach ($d_tienich as $key_tienich => $info_tienich) {
        $tienich_ten    = $info_tienich['ten'];
        $tienich_link   = $info_tienich['link'];
        $tienich_target = $info_tienich['target'];
        $data_lienketwebsite .= '<li><a href="' . $tienich_link . '" target="' . $tienich_target . '" title="' . $tienich_ten . '"><i class="fa fa-link"></i>' . $tienich_ten . '</a></li>';
    }
    $data_lienketwebsite .= '</ul>';
}
return $data_lienketwebsite;
