<?php

$v_news ='';

$eCatP = "SELECT a.id,a.idtype,img,b.ten,url,link,target,colvalue,icon,bgcolor,mota
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
AND b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
AND colvalue LIKE '%huongdanmuagd%'
ORDER BY thutu ASC
LIMIT 50";
$fCatP = $models->sql_select_sql($eCatP);
// echo '<pre>'; print_r($fCatP); echo '</pre>';exit();

if (count($fCatP) > 0) {
      foreach ($fCatP as $kdm => $vdm){
            $id       = $vdm['id'];
            $idtype   = $vdm['idtype'];
            $colvalue = $vdm['colvalue'];
            $ten      = $vdm['ten'];
            $mota     = $vdm['mota'];
            $hinh         = $vdm['img'];
            $linkhinh = ROOT_PATH.'uploads/danhmuc/'.$hinh;
            if (!empty($vdm['link'])){
                  $lienket = $vdm['link'];
            }
            else{
                  $lienket = ROOT_PATH.$vdm['url'].'/';
            }


            $v_news.= '<div class="titlebase-home clearfix wow animated fadeInUp" data-wow-delay="0s">';
            $v_news.= '<h2><a href="'.$lienket.'" title="'.$ten.'">'.$ten.'</a>
            </h2>';
            $v_news.= '</div>';
        // echo '<pre>'; print_r($v_news); echo '</pre>'; exit();

            $catOP = $models->getNameFromID("tbl_danhmuc_type", "op", "id", "'{$idtype}'");
            if ($catOP == 'product' || $catOP == 'content' || $catOP == 'service' || $catOP=='intro' || $catOP=='video' || $catOP=='project') {
                  $d_sp = "
                  SELECT a.id,a.hinh,keywebsite,a.idtype,noidung,mota,ten,url,link,target,gia
                  FROM tbl_noidung AS a
                  INNER JOIN tbl_noidung_lang AS b
                  ON a.id = b.idnoidung
                  WHERE a.anhien = 1
                  AND b.idlang = {$_SESSION['_lang']}
                  AND a.idtype like '%{$id}%'
                  ORDER by a.thutu Asc
                  LIMIT 12";
            $eSP = $models->sql_select_sql($d_sp);
            // echo '<pre>'; print_r($eSP); echo '</pre>'; exit();

            if (count($eSP) > 0) {
                  $v_news .='<ul class="noidunghuongdanmuagd">';
                  foreach($eSP as $key_noidung => $info_noidung){
                        $id       = $info_noidung['id'];
                        $ten      = $info_noidung['ten'];
                        $url      = $info_noidung['url'];
                        $hinh     = $info_noidung['hinh'];
                        $linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$hinh;

                        $v_news.='<li>';
                        $v_news.='<div class="noidungtronghuongdan">';
                        $v_news.='<img src="'.$linkhinh.'" />';
                        $v_news.='<div class="ten">'.$ten.'</div>';
                        $v_news.='</div>';
                        $v_news.='</li>';
                  }
                  $v_news.='</ul>';


            }
        }
    }
}

$c_news = '';
$c_news .='
<div class="guiykienKH clearfix">
<div class="title_dangky">' . $arraybien['phananhchatluongdichvu'] . '</div>

<div class="noidung_dangky">
<div class="dkmail-intro">'.$models->getthongtin("noidung", 'thongdiepdangkythongtin').'</div>

<div class="dkmail-frm clearfix">

<div class="khuctren">
<input type="text" name="hotenmail" class="hoten" placeholder="'.$arraybien['hovaten'].'"/>
<input type="text" name="dkmail" class="email" placeholder="'.$arraybien['emailcuaban'].'" />
<input type="text" name="sodienthoai" class="sodienthoai" placeholder="'.$arraybien['sodienthoai'].'" />
</div>

<div class="khucduoi">
<textarea name="noidung" cols="40" rows="6" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required noidung" aria-required="true" aria-invalid="false" placeholder="'.$arraybien['noidungykien'].'"></textarea>
</div>

<button type="button" data-loading-text="<i class=\'fa fa-spinner fa-spin fa-lg fa-fw\'></i>" id="dkmailBtn">'.$arraybien['guiykien'].'</button>
</div>

</div>
</div>
';

$data_bottom_fix = '';
$data_bottom_fix.='<ul class="ulcuoiwebsite clearfix">';
$data_bottom_fix.='<li class="huongdanmuagd clearfix">'.$v_news.'</li>';
$data_bottom_fix.='<li class="phananhdichvu clearfix">'.$c_news.'</li>';
$data_bottom_fix.='</ul>';

return $data_bottom_fix;