<?php

$arrCol = array();

$s_layout = "
SELECT a.loai,a.idtype,a.idtemplates,
a.danhmuc_dieukien,a.laytin,a.soluongtin,a.icon,b.ten,
hienthitieudechuyenmuc,laytieudechuyenmucdachon,
danhmuc_hiennoidung,background,color
FROM tbl_layout AS a
INNER JOIN tbl_layout_lang AS b ON a.id = b.idtype
WHERE a.anhien = 1
AND (vitrihienthi LIKE '%$__idtype_danhmuc%' OR vitrihienthi = '' OR vitrihienthi IS NULL)
AND a.vitri = 'cotb'
AND b.idlang = {$_SESSION['_lang']}
ORDER BY thutu ASC ";
$d_layout      = $db->sqlSelectSql($s_layout);
// echo '<pre>'; print_r($d_layout); echo '</pre>'; exit();

$soluonglayout = count($d_layout);
$data_cota     = '';
if ($soluonglayout > 0) {
    $arrCol['layout'] = $d_layout;

    foreach ($d_layout as $key_layout => $info_layout) {
        $id                       = $info_layout['id'];
        $loai                     = $info_layout['loai'];
        $idtype                   = $info_layout['idtype'];
        $danhmuc_dieukien         = $info_layout['danhmuc_dieukien'];
        $danhmuc_hiennoidung      = $info_layout['danhmuc_hiennoidung'];
        $hienthitieudechuyenmuc   = $info_layout['hienthitieudechuyenmuc'];
        $laytieudechuyenmucdachon = $info_layout['laytieudechuyenmucdachon'];
        $laytin                   = $info_layout['laytin'];
        $soluongtin               = $info_layout['soluongtin'];
        $ten                      = $info_layout['ten'];
        $icon                     = $info_layout['icon'];
        $idtemplates              = $info_layout['idtemplates'];
        $background               = $info_layout['background'];
        $color                    = $info_layout['color'];


        switch($laytieudechuyenmucdachon) {
            case 1:
                // lay tieu de va url cua idtype
            $s_noidung_ten_id = "

            SELECT url,ten,link,target
            FROM tbl_danhmuc_lang
            WHERE idlang = {$_SESSION['_lang']}
            AND iddanhmuc = '{$idtype}'

            ";
            $d_noidung_ten_id = $db->sqlSelectSql($s_noidung_ten_id);
            if (count($d_noidung_ten_id) > 0) {
                $idtype_url    = ROOT_PATH . $d_noidung_ten_id[0]['url'] . '/';
                $idtype_ten    = $d_noidung_ten_id[0]['ten'];
                $idtype_link   = $d_noidung_ten_id[0]['link'];
                $idtype_target = $d_noidung_ten_id[0]['target'];
                if ($idtype_link != '') {
                    $idtype_url = $idtype_link;
                }
            }

            $title_tieude = '
            <a href="'.$idtype_url.'" title="'.$idtype_ten.'" >
            '.$idtype_ten.'
            </a>
            ';
            break;
            default:
            $title_tieude = $ten;
            break;
        }

        #=================================
        #ASSIGN TITLELAYOUT === FINISHED
        #=================================
        $arrCol['layout'][$key_layout]['titleLayout'] = $title_tieude;




        // kiem tra loai va lay du lieu theo loai

        switch($loai) {
            case 0:
                #[A1] TRƯỜNG HỢP CHỌN LÀ DANH MỤC SẢN PHẨM
            if ($laytin == 1) {
                    # [AA1] TRƯỜNG HỢP LẤY DANH SÁCH BÀI VIẾT

                    # [AAA1] NẾU TEMPLATE TỰ VIẾT TỒN TẠI
                if ($idtemplates > 0) {

                        // LẤY CÁC KEY TẠO RA TRONG TEMPLATES
                    $templates_idkeyname_0 = $db->getNameFromID("tbl_layout_template", "idkeyname", "id", "'{$idtemplates}'");

                        // LẤY CÁC NỘI DUNG TẠO RA TRONG TEMPLATES
                    $templates_noidung = $db->getNameFromID("tbl_layout_template", "noidung_tbl", "id", "'{$idtemplates}'");


                        // TÁCH MÃNG CÁC KEY TẠO RA
                    $check_laymang_cot_database = explode("|", $templates_idkeyname_0);

                        // RESULT ==> link,target,ten,mota,hinh
                    if (count($check_laymang_cot_database) > 0) {
                        $templates_idkeyname = $check_laymang_cot_database[0];
                    }


                    $sNDTPL = "

                    SELECT a.hinh,b.url, {$templates_idkeyname}
                    FROM tbl_noidung As a
                    INNER join tbl_noidung_lang As b
                    ON a.id = b.idnoidung
                    WHERE b.idlang = {$_SESSION['_lang']}
                    AND a.idtype LIKE '%{$idtype}%'

                    ";
                    if ($danhmuc_dieukien != '') {
                        $sNDTPL .= " AND colvalue like '%{$danhmuc_dieukien}%' ";
                    }
                    $sNDTPL .= " ORDER by thutu Asc";
                    $sNDTPL .= " LIMIT 0, {$soluongtin} ";

                    $dNDTPL = $db->sqlSelectSql($sNDTPL);


                    if (count($dNDTPL) > 0) {

                        $arrCol['layout'][$key_layout]['arrNoidungTPL'] = $dNDTPL;

                        foreach ($dNDTPL as $key_noidung => $info_noidung) {
                                // explode lay mang
                            $templates_idkeyname  = str_replace("|", ",", $templates_idkeyname_0);
                            $arr_idkeyname        = explode(",", $templates_idkeyname);
                                // $arr_template_content = array();

                                // VÒNG FOREACH QUA CÁC THẺ [ link,target,ten,mota,hinh ]

                            $arrCol['layout'][$key_layout]['arrIdkeyTemplateMau'] = $arr_idkeyname;
                            $arrCol['layout'][$key_layout]['noidungTemplateMau'] = $templates_noidung;
                        }

                    }

                } else {
                        # [AAA1] NẾU TEMPLATE TỰ VIẾT KHÔNG TỒN TẠI
                }

            } else {

                    # [AA1] TRƯỜNG HỢP LẤY DANH MỤC

                if (!$danhmuc_hiennoidung) {
                    $data_menu = '';
                    $strlenid  = strlen($idtype);
                    $s_menu    = "
                    SELECT a.id,b.iddanhmuc,ten,url,link,target
                    FROM tbl_danhmuc as a
                    INNER join tbl_danhmuc_lang as b on a.id = b.iddanhmuc
                    WHERE a.anhien = 1
                    AND b.idlang = {$_SESSION['_lang']}
                    AND a.id like '{$idtype}%'
                    ";
                    if ($danhmuc_dieukien != '') {
                        $s_menu .= " and colvalue like '%{$danhmuc_dieukien}%' ";
                    }
                    $s_menu .= " order by thutu Asc";
                    $menuCol = $db->menuDequySql((strlen($idtype) + 4), $s_menu, "ten", "id", @$__idtype_danhmuc);
                    $arrCol['layout'][$key_layout]['menuCol'] = $menuCol;
                }
            }
            break;
            case 1:
                #[A1] TRƯỜNG HỢP CHỌN LÀ BANNER

                // lay du lieu neu la banner
            $s_banner = "

            SELECT b.ten,hinh,linkhinh,manhung,link,target,rong,cao
            FROM tbl_banner AS a
            INNER join tbl_banner_lang AS b On a.id = b.idtype
            WHERE a.loai = 2
            AND b.idlang = {$_SESSION['_lang']}
            AND a.anhien = 1
            AND a.idtype = '{$idtype}'
            ORDER BY thutu ASC
            ";
            $d_banner = $db->sqlSelectSql($s_banner);
            if (count($d_banner) > 0) {
                $arrCol['layout'][$key_layout]['arrBNQC'] = $d_banner;
            }
            break;
            case 2:
                #[A1] TRƯỜNG HỢP CHỌN LÀ TIỆN ÍCH
            $dataTienich = '';
            $file_tienich = $db->getNameFromID("tbl_tienich", "link", "id", "'{$idtype}'");
            if ($file_tienich != '') {
                    $dataTienich = include $file_tienich; // include file templates
                    $arrCol['layout'][$key_layout]['tienichFile'] = $file_tienich;
                    $arrCol['layout'][$key_layout]['tienichFileInc'] = $dataTienich;
                }
                break;
            }

    }// END FOREACH
}

$smarty->assign("arrColA", $arrCol);

