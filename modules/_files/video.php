<?php

// TRUY VẤN
$data_video = '';
$data_selectvideo = '';
$sVideo = "
SELECT a.id,a.idtype,b.ten,link,file
FROM tbl_noidung AS a
INNER JOIN tbl_noidung_lang AS b
ON a.id = b.idnoidung
WHERE b.idlang = {$_SESSION['_lang']}
AND anhien = 1
AND loai = 2
ORDER by thutu ASC
LIMIT 10";
$dVideo = $db->sqlSelectSql($sVideo);
// echo '<pre>'; print_r($dVideo); echo '</pre>'; exit();

if (count($dVideo) > 0) {
   $data_selectvideo.='<select id="selectVideo" class="form-control">';
   foreach ($dVideo as $kVideo => $vVideo) {
    $videoName = $vVideo['ten'];
    $videoLink = $vVideo['link'];
    $videoFile = ROOT_PATH.'uploads/noidung/'.$vVideo['file'];
    if ($kVideo==0) {
                # VIDEO SHOW
        if (!empty($videoLink)) {
            $keyVideo = end(explode("v=", $videoLink));
            $data_video.='
            <div id="framevideo" class="clearfix">
            <iframe width="100%" height="260" src="https://www.youtube.com/embed/'.$keyVideo.'?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
            ';
        } elseif (!empty($videoFile)) {
            $data_video.='
            <video width="100%" height="100%"  controls  poster="">
            <source src="'.$videoFile.'" type="video/mp4">
            <source src="'.$videoFile.'" type="video/ogg">
            <source src="'.$videoFile.'" type="video/webm">
            <object type="application/x-shockwave-flash" data="\/\/cdn.jsdelivr.net/jwplayer/5.10/player.swf" width="100%" height="350">
            <param name="movie" value="\/\/cdn.jsdelivr.net/jwplayer/5.10/player.swf" />
            <param name="allowFullScreen" value="true" />
            <param name="wmode" value="transparent" />
            <param name="flashVars" value="controlbar=over&amp;file='.$videoFile.'" />
            <span title="No video playback capabilities, please download the video below">September 2013 U-RUN</span>
            </object>
            </video>';
        }
    }
    $data_selectvideo.='
    <option value="https://www.youtube.com/embed/'.end(explode("v=", $videoLink)).'?rel=0&amp;controls=1&amp;showinfo=0">'.$videoName.'</option>
    ';
}
$data_selectvideo.='
</select>';
}

$data_video.= $data_selectvideo;
return $data_video;
