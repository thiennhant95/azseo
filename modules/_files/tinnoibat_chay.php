<?php
$d_tinmoi = '';
$s_noidung = "SELECT a.id,a.hinh,masp,gia,giagoc,
                    a.idtype,a.ngaycapnhat,ten,
                    url,link,target,mota
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND b.idlang = " . $_SESSION['_lang'] . "
                AND a.colvalue like '%noibat%'
                AND loai = ".array_search("content", $_arr_loai_noidung)."
            ORDER by thutu Asc";

$d_noidung = $db->sqlSelectSql($s_noidung);
$arrdata = array();
if (count($d_noidung) > 0) {
  $d_tinmoi.='<div class="prodnew-container">';
  $d_tinmoi .='<ul class="newsscroll">';
  foreach($d_noidung as $key_noidung => $info_noidung){
    $id   = $info_noidung['id'];
    $ten  = $info_noidung['ten'];
    $mota = $info_noidung['mota'];
    $url  = $info_noidung['url'];
    $hinh   = $info_noidung['hinh'];
    $linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$hinh;
    $link = ROOT_PATH.$url;
    $d_tinmoi.='<li>';
    $d_tinmoi.='<div class="img"><img src="'.$linkhinh.'" alt="'.$ten.'" /></div><span><a href="'.$link.'" title="'.$ten.'">'.$ten.'</a></span>'.$mota;
    $d_tinmoi.='</li>';
  }
  $d_tinmoi.='</ul>';
  $d_tinmoi.='</div>';
}
return $d_tinmoi;