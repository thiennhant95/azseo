<?php

$v_news ='';

$eCatP = "SELECT a.id,a.idtype,img,b.ten,url,link,target,colvalue,icon,bgcolor,mota
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
AND b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
AND colvalue LIKE '%maumoinhat%'
ORDER BY thutu ASC
LIMIT 50";
$fCatP = $models->sql_select_sql($eCatP);
// echo '<pre>'; print_r($fCatP); echo '</pre>';exit();

if (count($fCatP) > 0) {
    foreach ($fCatP as $kdm => $vdm){
        $id       = $vdm['id'];
        $idtype   = $vdm['idtype'];
        $colvalue = $vdm['colvalue'];
        $ten      = $vdm['ten'];
        $mota     = $vdm['mota'];
        if (!empty($vdm['link'])){
            $lienket = $vdm['link'];
        }
        else{
            $lienket = ROOT_PATH.$vdm['url'].'/';
        }

        $v_news.= '<div class="titlebase-home clearfix wow animated fadeInUp" data-wow-delay="0s">';
        $v_news.= '<h2><a href="'.$lienket.'" title="'.$ten.'">'.$ten.'</a>
        <span class="title-mota wow animated fadeInUp clearfix">'.$mota.'</span>
        </h2>';
        $v_news.= '</div>';
        // echo '<pre>'; print_r($v_news); echo '</pre>'; exit();

        // MENU
        $s_giuamenu = "
        SELECT b.id,a.iddanhmuc,ten,url,link,target,bgcolor,color,icon
        FROM tbl_danhmuc_lang AS a
        INNER join tbl_danhmuc AS b ON a.iddanhmuc = b.id
        WHERE b.anhien = 1
        AND a.idlang = {$_SESSION['_lang']}
        AND colvalue LIKE '%menugiuatrang%'
        ORDER by thutu ASC
        ";

        $v_news .= '<div class="menugiuatrang">';
        $v_news .= '<div class="menuItem menu-item cssmenu" id="cssmenu">';
        $v_news .= $models->Menu_Dequy_Sql(4, $s_giuamenu,"ten","id", @$__idtype_danhmuc, true);
        $v_news .= '<div class="search-box">
        <form action="'.ROOT_PATH.'tim-kiem.htm" method="post">
        <input type="hidden" name="op" value="search" />
        <input type="text" class="txt no-focus"  name="key" placeholder="'.$arraybien['bantimgiaodiengi'].'" />
        <button type="submit" class="btnsearch no-focus" >
        <i class="fa fa-search kinhlup" aria-hidden="true"></i>
        </button>
        </form>
        </div>';
        $v_news .= '</div>';
        $v_news .= '</div>';
        // END MENU

        $catOP = $models->getNameFromID("tbl_danhmuc_type", "op", "id", "'{$idtype}'");
        if ($catOP == 'product' || $catOP == 'content' || $catOP == 'service' || $catOP=='intro' || $catOP=='video' || $catOP=='project') {
            $d_sp = "
            SELECT a.id,a.hinh,keywebsite,a.idtype,noidung,ten,url,hinh,link,target,gia
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
            ON a.id = b.idnoidung
            WHERE a.anhien = 1
            AND b.idlang = {$_SESSION['_lang']}
            AND a.idtype like '%{$id}%'
            -- AND colvalue like '%home%'
            ORDER by a.thutu Asc
            LIMIT 12";
            $eSP = $models->sql_select_sql($d_sp);
            // echo '<pre>'; print_r($eSP); echo '</pre>'; exit();

            if (count($eSP) > 0) {
                $v_news .='<ul class="noidungchitiet">';
                foreach($eSP as $key_noidung => $info_noidung){
                    $id       = $info_noidung['id'];
                    $ten      = $info_noidung['ten'];
                    $url      = $info_noidung['url'];
                    $hinh     = $info_noidung['hinh'];
                    $gia      = $info_noidung['gia'];
                    $fixgia   = number_format($gia, 0, '.', ',');
                    $linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$hinh;
                    $link     = ROOT_PATH.$url;

                    $v_news.='<li>';
                    $v_news.='<div class="noidungcontent">';
                    $v_news.='<a href="'.$link.'" title="'.$ten.'">
                    <div class="img"><img src="'.$linkhinh.'" alt="'.$ten.'" /></div>';
                    $v_news.='<div class="noidungduoi">';
                    $v_news.='<div class="ten">'.$ten.'</div>';
                    $v_news.='<div class="gia">'.$fixgia.' VNĐ</div>';
                    $v_news.='</div>';
                    $v_news.='</a>';
                    $v_news.='</div>';
                    $v_news.='</li>';
                }
                $v_news.='</ul>';
            }
        }
    }
}

return $v_news;