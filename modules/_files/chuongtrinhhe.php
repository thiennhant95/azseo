<?php

$v_news ='';

$eCatP = "SELECT a.id,a.idtype,img,b.ten,url,link,target,colvalue,icon,bgcolor,mota
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
AND b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
AND colvalue LIKE '%chuongtrinhhe%'
ORDER BY thutu ASC
LIMIT 50";
$fCatP = $db->sqlSelectSql($eCatP);
// echo '<pre>'; print_r($fCatP); echo '</pre>';exit();

if (count($fCatP) > 0) {
    foreach ($fCatP as $kdm => $vdm){
        $id       = $vdm['id'];
        $ten      = $vdm['ten'];
        $bgcolor  = $vdm['bgcolor'];
        $background = 'style="background-color:#'.$bgcolor.'"';
        if (!empty($vdm['link'])){
            $lienket = $vdm['link'];
        }
        else{
            $lienket = ROOT_PATH.$vdm['url'].'/';
        }

        $v_news .='<div class="chuongtrinhhe clearfix" '.$background.'>';
        $v_news .='<div class="noidungcthe">';

        $v_news .= '<div class="titlebase-home clearfix wow animated fadeInUp" data-wow-delay="0s">';
        $v_news .= '<h2>
        <a href="'.$lienket.'" title="'.$ten.'">'.$ten.'</a>
        </h2>';
        $v_news .= '</div>';
        
        $d_sp = "
        SELECT a.id,a.hinh,keywebsite,a.idtype,noidung,ten,url,hinh,link,target,gia
        FROM tbl_noidung AS a
        INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
        WHERE a.anhien = 1
        AND b.idlang = {$_SESSION['_lang']}
        AND a.idtype like '%{$id}%'
        ORDER by a.thutu Asc
        LIMIT 12";
        $eSP = $db->sqlSelectSql($d_sp);
        // echo '<pre>'; print_r($d_sp); echo '</pre>'; exit();

        if (count($eSP) > 0) {
            $v_news .='<ul class="noidunghe">';
            foreach($eSP as $key_noidung => $info_noidung){
                $id       = $info_noidung['id'];
                $ten      = $info_noidung['ten'];
                $url      = $info_noidung['url'];
                $link     = $info_noidung['link'];
                if( $link != '' ){
                    $lienket = $link;
                }
                else{
                    $lienket = ROOT_PATH.$url;
                }
                $hinh     = $info_noidung['hinh'];
                $gia      = $info_noidung['gia'];
                $fixgia   = number_format($gia, 0, '.', ',');
                $linkhinh = ROOT_PATH.'uploads/noidung/thumb/'.$hinh;

                $v_news.='<li>';
                $v_news.='<div class="img">
                <img src="'.$linkhinh.'" alt="'.$ten.'" />
                </div>';
                $v_news.='<div class="noidungduoi">';
                $v_news.='<div class="ten">('.$ten.')</div>';
                $v_news.='<div class="xemchitiet">
                <a href="'.$lienket.'" target="_blank" title="'.$arraybien['chitiet'].'">
                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                '.$arraybien['chitiet'].'
                <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                </a>
                </div>';
                $v_news.='</div>';
                $v_news.='</li>';
            }
            $v_news.='</ul>';
        }
        $v_news.='</div>';
        $v_news.='</div>';
    }
}

return $v_news;