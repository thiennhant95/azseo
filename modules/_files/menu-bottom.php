<?php
$vBB = "";
$s_mn = "
SELECT b.id,a.iddanhmuc,ten,url,link,target,bgcolor,color
FROM tbl_danhmuc_lang AS a
INNER JOIN tbl_danhmuc AS b
    ON a.iddanhmuc = b.id
WHERE b.anhien = 1
    AND a.idlang = {$_SESSION['_lang']}
    AND keyname = 'trungtamhotro'
ORDER BY thutu ASC
LIMIT 1";
$d_mn = $db->sqlSelectSql($s_mn);
$vBB .= '
<div class="form-group clearfix cat-bott-conta">
    <div class="ws-container clearfix">
        <ul class="clearfix rowBeforeBottom">


            <li class="ibottom menuCSKH">
                <div class="ibottomContent clearfix">
                    <a href="'.ROOT_PATH.$d_mn[0]['url'].'/" title="'.$d_mn[0]['ten'].'" class="tieude-">
                        '.$d_mn[0]['ten'].'
                    </a>';

                        $scat2 = "
                            SELECT a.id,b.iddanhmuc,ten,url,link,target,bgcolor,color,img
                            FROM tbl_danhmuc_lang AS b
                            INNER JOIN tbl_danhmuc AS a
                                ON b.iddanhmuc = a.id
                            WHERE a.anhien = 1
                                AND b.idlang = {$_SESSION['_lang']}
                                AND a.id LIKE '%{$d_mn[0]['id']}%'
                                AND a.id != {$d_mn[0]['id']}
                                AND LENGTH(a.id) = ".(strlen($d_mn[0]['id']) + 4)."
                            ORDER BY thutu ASC
                            LIMIT 4";
                    $qcat2 = $db->sqlSelectSql($scat2);
                    if (count($qcat2) > 0){
                        $vBB.='

                        <ul class="clearfix menu2wrap">';
                            foreach ($qcat2 as $kcat2 => $vcat2) {
                                $c2_Id    = $vcat2['id'];
                                $c2_Name  = $vcat2['ten'];
                                $c2_Image = ROOT_PATH."uploads/danhmuc/".$vcat2['img'];
                                $c2_Url   = ROOT_PATH.$vcat2['url']."/";

                                $vBB.='
                                    <li>
                                        <a href="'.$c2_Url.'" title="'.$c2_Name.'">
                                            <img src="'.$c2_Image.'" alt="'.$c2_Name.'">
                                            '.$c2_Name.'
                                        </a>
                                    </li>
                                ';
                            } $vBB.='

                        </ul>';
                    }

                $vBB.='
                </div>
            </li>
            <li class="ibottom video">
                <div class="ibottomContent clearfix">
            ';
#-----------------------------------------------------------
# VIDEO
#-----------------------------------------------------------
$v_video = '';
$sVideo = "
SELECT a.id,a.idtype,b.ten,link,file
FROM tbl_noidung AS a
INNER JOIN tbl_noidung_lang AS b
    ON a.id = b.idnoidung
WHERE b.idlang = {$_SESSION['_lang']}
    AND anhien = 1
    AND loai = 2
ORDER by thutu ASC
LIMIT 10";
$dVideo = $db->sqlSelectSql($sVideo);
if (count($dVideo) > 0) {
    $v_video.='<div class="video-container clearfix">

        <a href="javascript:;" title="'.$arraybien['video'].'" class="tieude-">
            '.$arraybien['video'].'
        </a>


        <div class="boxbase clearfix">';
        foreach ($dVideo as $kVideo => $vVideo) {
            $videoName = $vVideo['ten'];
            $videoLink = $vVideo['link'];
            $videoFile = ROOT_PATH.'uploads/noidung/'.$vVideo['file'];
            if (!$kVideo) {
                # VIDEO SHOW
                if (!empty($videoLink)) {
                    $keyVideo = end(explode("v=", $videoLink));
                    $v_video.='
                    <div id="framevideo" class="clearfix">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/'.$keyVideo.'?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    ';
                } elseif (!empty($videoFile)) {
                    $v_video.='
                    <video width="100%" height="100%"  controls  poster="">
                        <source src="'.$videoFile.'" type="video/mp4">
                        <source src="'.$videoFile.'" type="video/ogg">
                        <source src="'.$videoFile.'" type="video/webm">
                        <object type="application/x-shockwave-flash" data="\/\/cdn.jsdelivr.net/jwplayer/5.10/player.swf" width="100%" height="350">
                           <param name="movie" value="\/\/cdn.jsdelivr.net/jwplayer/5.10/player.swf" />
                           <param name="allowFullScreen" value="true" />
                           <param name="wmode" value="transparent" />
                           <param name="flashVars" value="controlbar=over&amp;file='.$videoFile.'" />
                           <span title="No video playback capabilities, please download the video below">September 2013 U-RUN</span>
                        </object>
                     </video>';
                }
                $v_video.='<div class="video-group-list clearfix">
                    <select id="selectVideo" class="form-control">
                ';
            }
            $v_video.='

            <option value="https://www.youtube.com/embed/'.end(explode("v=", $videoLink)).'?rel=0&amp;controls=1&amp;showinfo=0">'.$videoName.'</option>

            ';
        }
    $v_video.='
    </select>
    </div><!-- video-group-list -->
    </div><!-- boxbase -->
    </div>';
}

$vBB.=$v_video;
                $vBB.='
                </div>
            </li>
            <li class="ibottom facebook">
                <div class="ibottomContent clearfix">
                    <a href="javascript:;" title="'.$arraybien['video'].'" class="tieude-">
                        Facebook Fanpage
                    </a>
                    '.$db->getThongTin("fanpage").'
                </div>
            </li>


        </ul>
    </div>
</div><!-- ./cat-bott-conta -->';

return $vBB;
