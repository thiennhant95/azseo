<?php
if(isset($_GET['keytp'])){
    $_key = ws_get('keytp');
}
if(isset($_GET['tensp'])){
    $_tensp = ws_get('tensp');
}
if(isset($_GET['thanhphan'])){
    $_thanhphan = ws_get('thanhphan');
}

$_key = trim($_POST['keytp']);

$arayOptType = $_POST['optiontype'];

// phan trang
$phantrang =  '';
$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongsanpham'];
$startpoint = ($page * $perpage) - $perpage;
$product_type = array_search("product", $_arr_loai_noidung);

$s_noidung = "SELECT a.id,a.hinh,masp,gia,
                giagoc,a.idtype,a.ngaycapnhat,ten,url,
                link,target,mota
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND b.idlang = {$_SESSION['_lang']} ";

if ($_key != '' && $_tensp  != '') {
    $s_noidung .= " and (ten like '%{$_key}%' or tensp like '%{$_tensp}%')";
}
elseif($_thanhphan != '') {
    $s_noidung .= " and (thanhphan like '%{$_thanhphan}%')";
}

// Search width OptionType
if( count($arayOptType) > 0 ){
    foreach( $arayOptType as $iOpt ){
        if( ! empty($iOpt) ){

            // Truy van lay cac idnoidung trong bang OptionValue
            $qOptValue = $db->sqlSelectSql("
                SELECT idnoidung
                FROM tbl_option_value
                WHERE noidung LIKE '%{$iOpt}%'
            ");

            if( count($qOptValue) > 0 ){
                $s_noidung.=" AND ( ";
                foreach( $qOptValue as $kOptionValue => $iOptValue ){
                    $idProduct = $iOptValue['idnoidung'];

                    $s_noidung.= " a.id = '{$idProduct}' ";

                    if( ($kOptionValue+1) < count($qOptValue) ){
                        $s_noidung.=" OR ";
                    }
                }
                $s_noidung.=" ) ";
            }
        }
    }
}
$s_noidung.="
                AND a.idtype like '%{$__idtype_danhmuc}%'
                AND loai = {$product_type}
            ORDER by thutu Asc
            LIMIT $startpoint, $perpage";

$sql_page = "SELECT COUNT(*) as num
            FROM  tbl_noidung As a
            INNER JOIN tbl_noidung_lang AS b
            ON a.id = b.idnoidung
            WHERE a.anhien = 1 ";
if ($_key != '') {
    $sql_page .= " and (ten like '%" . $_key . "%' or masp like '%" . $_key . "%')";
}
$sql_page.="
            and b.idlang = " . $_SESSION['_lang'] . "
            and a.idtype like '%$__idtype_danhmuc%'
            and loai = $product_type ";

$d_noidung = $db->sqlSelectSql($s_noidung);

$arrdata = array();
if (count($d_noidung) > 0) {
  $arrdata['sanpham'] = $d_noidung;
}
$url_page = ROOT_PATH .'?op=timkiem&key=' .$_key . '&page=';
$phantrang .= Pages($sql_page, $perpage, $url_page);
/*Nếu chưa khai báo chế độ xem thì mặc định là grid view*/
if (!isset($_SESSION['flow'])) {
    $_SESSION['flow'] = 1;
}
if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
    $flowlist = 1;
} else {
    $flowlist = 0;
}
$flow = '';
$flow .= '
<div class="type-view clearfix">
    <ul>
        <li>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
                $flow .= '<a href="'.ROOT_PATH.'ajax/?method=flow&value=1" title="">';
            }
            $flow .= '<button type="button" class="btn btn-default" '.($flowlist==0?'disabled':null).'>
                        <i class="fa fa-th fa-lg" aria-hidden="true"></i>
                    </button>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
                $flow .= '</a>';
            }
            $flow .= '
        </li>
        <li>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] != 2) {
                $flow .= '<a href="'.ROOT_PATH.'ajax/?method=flow&value=2" title="">';
            }
            $flow .= '<button type="button" class="btn btn-default" '.($flowlist==1?'disabled':null).'>
                          <i class="fa fa-th-list fa-lg" aria-hidden="true"></i>
                      </button>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] != 2) {
                $flow .= '</a>';
            }$flow .= '
        </li>
    </ul>
</div>';
// do du lieu vao tpl
$smarty->assign("arrdata",$arrdata,true);
$smarty->assign("phantrang",$phantrang,true);
$smarty->assign("flow", $flow);
