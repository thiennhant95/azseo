<?php
$data_cota= '<div class="content_cotmobile">';
    $s_layout = "SELECT a.loai,
                      a.idtype,
                      a.idtemplates,
                      a.danhmuc_dieukien,
                      a.laytin,
                      a.soluongtin,
                      a.icon,
                      b.ten,
                      background,
                      color,
                      hienthitieudechuyenmuc,
                      laytieudechuyenmucdachon,
                      danhmuc_hiennoidung
              FROM tbl_layout AS a
              INNER JOIN tbl_layout_lang AS b ON a.id = b.idtype
              WHERE a.anhien = 1
              AND a.vitri = 2
              AND b.idlang = '" . $_SESSION['_lang'] . "'
              ORDER BY thutu ASC ";
    $d_layout      = $db->sqlSelectSql($s_layout);
    $soluonglayout = count($d_layout);
    if ($soluonglayout > 0) {
        foreach ($d_layout as $key_layout => $info_layout) {
            $loai                     = $info_layout['loai'];
            $idtype                   = $info_layout['idtype'];
            $danhmuc_dieukien         = $info_layout['danhmuc_dieukien'];
            $danhmuc_hiennoidung      = $info_layout['danhmuc_hiennoidung'];
            $hienthitieudechuyenmuc   = $info_layout['hienthitieudechuyenmuc'];
            $laytieudechuyenmucdachon = $info_layout['laytieudechuyenmucdachon'];
            $laytin                   = $info_layout['laytin'];
            $soluongtin               = $info_layout['soluongtin'];
            $ten                      = $info_layout['ten'];
            $icon                     = $info_layout['icon'];
            $idtemplates              = $info_layout['idtemplates'];
            $background               = $info_layout['background'];
            $color                    = $info_layout['color'];
            $title_tieude             = '';
            if (substr($icon, 3, 3) == 'fa-') {
                $icon_show = '<i class="' . $icon . '"></i>';
            } elseif (substr($icon, -4, 1) == '.') {
                $icon_show = '<img src="' . ROOT_PATH . "uploads/layout/" . $icon . '" width="40px" onerror="xulyloi(this)" class="img-responsive" alt="' . $ten . '" />';
            } else {
                $icon_show = "";
            }
             $style_color = '';
            if($background!=''){
                $style_color = ' style="background-color:#'.$background.';" ';
            }
            if ($laytieudechuyenmucdachon == 1) {
                // lay tieu de va url cua idtype
                $s_noidung_ten_id = "SELECT url,ten,link,target
                             from tbl_danhmuc_lang
                             where idlang = '" . $_SESSION['_lang'] . "'
                             and iddanhmuc = '" . $idtype . "'";
                $d_noidung_ten_id = $db->sqlSelectSql($s_noidung_ten_id);
                if (count($d_noidung_ten_id) > 0) {
                    $idtype_url    = ROOT_PATH . $d_noidung_ten_id[0]['url'] . '/';
                    $idtype_ten    = $d_noidung_ten_id[0]['ten'];
                    $idtype_link   = $d_noidung_ten_id[0]['link'];
                    $idtype_target = $d_noidung_ten_id[0]['target'];
                    if ($idtype_link != '') {
                        $idtype_url = $idtype_link;
                    }
                }
                 $title_tieude = '
                    <div class="group_tin_tieude titlelienquan mabot">
                        <div class="htitle">
                            <a target="' . $idtype_target . '" href="' . $idtype_url . '" title="' . $idtype_ten . '">' . $icon_show . '&nbsp;' . $idtype_ten . '</a>
                        </div>
                    </div>';
            } else {
                $title_tieude = '
                <div class="group_tin_tieude titlelienquan mabot">
                    <div class="htitle">
                        <a href="javascript:;" title="">
                            ' . $ten . '
                        </a>
                    </div>
                </div>';
            }
            if ($hienthitieudechuyenmuc == 1) {
                $data_cota .= '
                   <aside class="group_tin">
                      ' . $title_tieude . '
                      <div class="group_tin_noidung">';
            }
            // kiem tra loai va lay du lieu theo loai
            if ($loai == 0) {
                // neu loai la danh muc san pham
                if ($laytin == 1) {
                    // lay bai viet
                    // lay mau templates
                    if ($idtemplates > 0) {
                        $templates_idkeyname_0 = $db->getNameFromID("tbl_layout_template", "idkeyname", "id", "'" . $idtemplates . "'");
                        $templates_noidung     = $db->getNameFromID("tbl_layout_template", "noidung_tbl", "id", "'" . $idtemplates . "'");
                        // lay du lieu tu bang noi dung
                        $check_laymang_cot_database = explode("|", $templates_idkeyname_0);
                        if (count($check_laymang_cot_database) > 0) {
                            $templates_idkeyname = $check_laymang_cot_database[0];
                        }
                        $s_noidung = "SELECT a.hinh,b.url," . $templates_idkeyname . "
                              from tbl_noidung As a
                              inner join tbl_noidung_lang As b
                              On a.id = b.idnoidung
                              where b.idlang = '" . $_SESSION['_lang'] . "'
                              and a.idtype like '%" . $idtype . "%' ";
                        if ($danhmuc_dieukien != '') {
                            $s_noidung .= " and " . $danhmuc_dieukien . "  = 1 ";
                        }
                        $s_noidung .= " order by thutu Asc";
                        $s_noidung .= " limit 0," . $soluongtin . " ";
                        $d_noidung = $db->sqlSelectSql($s_noidung);
                        if (count($d_noidung) > 0) {
                            $data_cota .= '<ul class="ul_noidung_templates">';
                            foreach ($d_noidung as $key_noidung => $info_noidung) {
                                $noidung_ten    = $info_noidung['ten'];
                                $noidung_hinh   = $info_noidung['hinh'];
                                $noidung_link   = $info_noidung['link'];
                                $noidung_url    = $info_noidung['url'];
                                $noidung_target = $info_noidung['target'];
                                // explode lay mang
                                $templates_idkeyname  = str_replace("|", ",", $templates_idkeyname_0);
                                $arr_idkeyname        = explode(",", $templates_idkeyname);
                                $arr_template_content = array();
                                foreach ($arr_idkeyname as $key_noidung_tpl => $info_noidung_tbl) {
                                    if ($info_noidung_tbl == 'link') {
                                        if ($noidung_link == '') {
                                            $noidung_link = ROOT_PATH . $noidung_url;
                                        }
                                        $arr_template_content["{" . $info_noidung_tbl . "}"] = $noidung_link;
                                    } else if ($info_noidung_tbl == 'hinh') {
                                        if ($noidung_hinh != '') {
                                            $noidung_hinh = ROOT_PATH . 'uploads/noidung/thumb/' . $noidung_hinh;
                                        }
                                        $arr_template_content["{" . $info_noidung_tbl . "}"] = $noidung_hinh;
                                    } else if ($info_noidung_tbl == 'chitiet') {
                                        $chitiet                                             = $arraybien['chitiet'];
                                        $arr_template_content["{" . $info_noidung_tbl . "}"] = $chitiet;
                                    } else {
                                        $arr_template_content["{" . $info_noidung_tbl . "}"] = $info_noidung[$info_noidung_tbl] . '';
                                    }
                                }
                                $data_cota .= $db->Load_content_data($templates_noidung, $arr_template_content);
                            }
                            $data_cota .= '</ul>';
                        }
                    } else { // xuat ra mau mac dinh
                    }
                } else {
                    // neu lay danh muc san pham
                    if ($danhmuc_hiennoidung == 1) // hien thi noi dung cua danh muc dang chon
                    {
                        $data_menu = '<div class="noidung_danhmuc">';
                        $data_menu .= $db->getNameFromID("tbl_danhmuc_lang", "noidung", "iddanhmuc", "'" . $idtype . "' and idlang = '" . $_SESSION['_lang'] . "' ");
                        $data_menu .= '</div>';
                    } else {
                        $data_menu = '';
                        $strlenid  = strlen($idtype);
                        $s_menu    = "SELECT a.id,b.iddanhmuc,ten,url,link,target
                            from tbl_danhmuc as a
                            inner join tbl_danhmuc_lang as b on a.id = b.iddanhmuc
                            where a.anhien = 1
                            and b.idlang = " . $_SESSION['_lang'] . "
                            and a.id like '" . $idtype . "%' ";
                        if ($danhmuc_dieukien != '') {
                            $s_menu .= " and " . $danhmuc_dieukien . " = 1 ";
                        }
                        $s_menu .= " order by thutu Asc";
                        $data_menu .= '<div class="danhmucmobile">';
                        $data_menu .= $db->menuDequySql((strlen($idtype) + 4), $s_menu, "ten", "id", "");
                        $data_menu .= '</div>';
                    }
                    $data_cota .= $data_menu;
                }
            } else if ($loai == 1) {
                // lay du lieu neu la banner
                $s_banner = "SELECT b.ten,hinh,linkhinh,manhung,link,target,rong,cao
                      from tbl_banner AS a
                      inner join tbl_banner_lang AS b On a.id = b.idtype
                      where a.loai = 2
                      and b.idlang = " . $_SESSION['_lang'] . "
                      and a.anhien = 1
                      and a.idtype = '" . $idtype . "'
                      order by thutu asc ";
                $d_banner    = $db->sqlSelectSql($s_banner);
                $data_banner = '';
                if (count($d_banner) > 0) {
                    $data_banner .= '<ul class="banner_quangcao">';
                    foreach ($d_banner as $key_banner => $info_banner) {
                        $banner_ten      = $info_banner['ten'];
                        $banner_hinh     = $info_banner['hinh'];
                        $banner_linkhinh = $info_banner['linkhinh'];
                        $banner_manhung  = $info_banner['manhung'];
                        $banner_link     = $info_banner['link'];
                        $banner_target   = $info_banner['target'];
                        $banner_rong     = $info_banner['rong'];
                        $banner_cao      = $info_banner['cao'];
                        if ((int) $banner_rong > 0) {
                            $banner_rong = $banner_rong . 'px';
                        }
                        if ((int) $banner_cao > 0) {
                            $banner_cao = $banner_cao . 'px';
                        }
                        $extfile      = $db->LayPhanMoRongFileUpload($banner_hinh);
                        $duongdanhinh = ROOT_PATH . 'uploads/logo/' . $banner_hinh;
                        if ($banner_linkhinh != '') {
                            $duongdanhinh = $banner_linkhinh;
                        }
                        if ($banner_manhung != '') {
                            $data_banner .= '<li>' . $banner_manhung . '</li>';
                        } else {
                            // kiem tra neu là flash thì đưa flash vào
                            if ($extfile == 'swf') {
                                $data_banner .= '
                                  <li>
                                     <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="' . $banner_rong . '" height="' . $banner_cao . '">
                                              <param name="movie" value="' . $duongdanhinh . '" />
                                              <param name="quality" value="high" />
                                              <param name="WMode" value="Transparent"/>
                                              <embed wmode="transparent" src="' . $hinh . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $banner_rong . '" height="' . $banner_cao . '">
                                              </embed>
                                     </object>
                                  </li>';
                            } else {
                                $data_banner .= '<li><a href="' . $banner_link . '" title="' . $banner_ten . '" target="' . $banner_target . '"><img width="' . $banner_rong . '" height="' . $banner_cao . '" onerror="xulyloi(this);" src="' . $duongdanhinh . '" alt="' . $banner_ten . '" title="' . $banner_ten . '" /></a></li>';
                            }
                        }
                    }
                    $data_banner .= '</ul>';
                }
                $data_cota .= $data_banner;
            } else if ($loai == 2) {
                $file_tienich = '../.'.$db->getNameFromID("tbl_tienich", "link", "id", "'" . $idtype . "'");
                if ($file_tienich != '') {
                    $data_cota .= include $file_tienich; // include file templates
                }
            }
            if ($hienthitieudechuyenmuc == 1) {
                // neu cho phep hien thi tieu de cua nhom
                $data_cota .= '</div>'; // end div noi dung
                $data_cota .= '</aside>'; // end div group tin
            }
        }
    }
$data_cota.='</div>';
echo $data_cota;
