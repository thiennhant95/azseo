<?php
    require dirname(dirname(__DIR__)) . "/configs/inc.php";

    show_error( false );

include "../../configs/bien.php";
$getidLocnhanh                      = ws_get('dulieu');
$idnhomid                           = ws_get('idnhomid');
$idnhomnd                           = ws_get('idnhomnd');
$_SESSION['loc_idnhomid']           = $idnhomid;
$_SESSION['loc_idnhomnd'.$idnhomid] = $idnhomnd;

$__idtype_danhmuc = $_SESSION['loc_idtype'];

$s_sql2 = '';
$s_sql1 = '';
if($idnhomid != ''){
    // neu ton tai id roi thi moi truy van cap con
  $s_idnhom = "select id from tbl_option_type where locnhanh = 1";
  $d_nhom = $db->sqlSelectSql($s_idnhom);
  if(count($d_nhom)>0){
    // $s_sql2 .=" and ( c.noidung != '' ";
    foreach($d_nhom as $key_nhom => $info_nhom){
      $idnhom = $info_nhom['id'];
      if($_SESSION['loc_idnhomnd'.$idnhom]!='')
      {
         $s_sql2 .= " and a.id in( select idnoidung  from tbl_option_value where noidung like '%" . $_SESSION['loc_idnhomnd'.$idnhom] . "%' and idoptiontype = $idnhom ) ";
      }
    }
   // $s_sql2 .=" ) ";
  }
}// end iff ton tai id

// XỬ LÝ LỌC GIÁ
$value = ws_post('value');
$value = preg_replace('/<[^>]*>/', '', $value);
$value = stripslashes(html_entity_decode($value));

if( ! empty($value) ){
    $value = json_decode($value, TRUE);

    $price_idcat = $value['idcat'];
    $price_from  = $value['from'];
    $price_to    = $value['to'];

    $__idtype_danhmuc = $price_idcat;

    if( ! empty($price_to) || ! empty($price_from) ){

        if( empty($price_from) ){
            $s_sql2.="
            AND (gia <= {$price_to})";

        }elseif( empty($price_to) ){
            $s_sql2.="
            AND (gia >= {$price_from})";
        }else{
            $s_sql2.="
            AND (gia >= {$price_from} AND gia <= {$price_to} )";
        }

    }
}

$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongsanpham']; //limit in each page
$perpage    = 10;
$startpoint = ($page * $perpage) - $perpage;

if( empty($s_sql2) ){
    exit('Nothing Doing...');
}

$sql_page = "
    SELECT COUNT(*) as num
    FROM  tbl_noidung As a
    INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
    WHERE a.anhien = 1
        AND a.idtype LIKE '%{$__idtype_danhmuc}%'
        AND b.idlang = {$_SESSION['_lang']}
        AND loai = 1
        {$s_sql2}
    GROUP by a.id
    ORDER by thutu Asc
";

$d_noidung = $db->layNoiDung(
    "product",
    "AND a.idtype LIKE '%{$__idtype_danhmuc}%'
    {$s_sql2}",
    null,
    array($startpoint, $perpage)
);

if ($_array_config_noidung['share'] == 1) {
    $data_ajax .= include "plugin/share/share.php";
}

$data_ajax .= '<div class="clear"></div>';

if ( count($d_noidung) > 0) {

    $data_ajax.='
    <div class="clearfix" id="loaditemsanpham">';

        $smarty->assign("root_path", $_SESSION['ROOT_PATH']);
        $smarty->assign("arrsanpham", $d_noidung);
        $data_ajax.= $smarty->fetch(ROOT_DIR . "/".LAYOUT_PATH."/modules/product/tpl/item.tpl");

    $data_ajax.='
    </div>';

    $url_page = $_SESSION['ROOT_PATH'] . $_getcat . '-trang-';
    $data_ajax .= '<div class="clear"></div>';
    $data_ajax .= Pages($sql_page, $perpage, $url_page);

} else {
    $data_ajax.='

    <div class="alert alert-danger text-center">
            <div class="h4">KHÔNG TÌM THẤY SẢN PHẨM</div>
    </div>
    ';
}

echo  $data_ajax;