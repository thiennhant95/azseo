<?php
$dta_reg = '';
#
# XỬ LÝ POST FORM
# --------------------------------------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['flag']) && trim($_POST['flag']) == 'dangkytuvan') {
    // tbl_dangkytuvan
    $ten = trim($_POST['hoten']);
    //$email          = trim($_POST['email']);
    $dienthoai = trim($_POST['dienthoai']);
    $noidung   = trim($_POST['noidung']);
    $sql_cthoc = array(
        "ten"       => $ten,
        "dienthoai" => $dienthoai,
        "noidung"   => $noidung,
        "loai"      => 0,
        "anhien"    => 1,
        "ngaygui"   => $db->NgayHienTaiMysql(),
    );
    $idInsert = $db->sqlInsert("tbl_dangkytuvan", $sql_cthoc);
    if ($idInsert != '') {
        $dta_reg .= "
        <script>
            window.location.href='" . $_SESSION['ROOT_PATH'] . "';
            alert('SUCCESS !!!');
        </script>
        ";
    } else {
        $dta_reg .= "<script>
        swal('ERROR','','error');
    </script>";
    }
} else if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['flag']) && trim($_POST['flag']) == 'dangkynhanthongbao') {
    $email          = trim($_POST['email']);
    $sql_cthoc = array(
        "email"     => $email,
        "loai"      => 1,
        "anhien"    => 1,
        "ngaygui"   => $db->NgayHienTaiMysql(),
    );
    $idInsert = $db->sqlInsert("tbl_dangkytuvan", $sql_cthoc);
    if ($idInsert != '') {
        $dta_reg .= "
        <script>
            window.location.href='" . $_SESSION['ROOT_PATH'] . "';
            alert('SUCCESS !!!');
        </script>
        ";
    } else {
        $dta_reg .= "<script>
        swal('ERROR','','error');
    </script>";
    }
}

//return $dta_reg;

echo $dta_reg;
