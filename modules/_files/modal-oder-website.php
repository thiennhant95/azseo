<?php
$valOrderWebsite = '';
#-----------------------------------------------------------
# MODAL ORDER WEBSITE
#-----------------------------------------------------------
$bgorderweb = $db->getThongTin("bg_orderwebsite");
if (!empty($bgorderweb)) {
    $bgorderweb = get_src_img($bgorderweb);
}
$valOrderWebsite .= '
<div class="modal fade" id="modal-product">
    <div class="modal-dialog">
        <div class="modal-content" style="background-image: url('.$bgorderweb.')">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body order-body">
                <div class="form-group">
                    <input type="hidden" name="id" id="this_product_id" value="" placeholder="" />

                    <div class="form-group icon-input">
                        <i class="fa fa-user fa-lg" arial-hidden="true"></i>
                        <input type="text" name="hoten" class="form-control ws-required" placeholder="'.$arraybien['vuilongnhaphoten'].'..." />
                    </div>

                    <div class="form-group icon-input">
                        <i class="fa fa-phone fa-lg"></i>
                        <input type="text" name="dienthoai" class="form-control ws-required ws-required-number" data-wserror-phone="Vui lòng nhập vào 1 số điện thoại" placeholder="'.$arraybien['vuilongnhapsodienthoai'].'..." />
                    </div>

                    <div class="form-group icon-input">
                        <i class="fa fa-map-marker"></i>
                        <input type="text" name="diachi" class="form-control ws-required" placeholder="'.$arraybien['vuilongnhapdiachi'].'..." />
                    </div>
                    <div class="form-group icon-input">
                        <i class="fa fa-envelope"></i>
                        <input type="text" data-wserror-email="Vui lòng nhập vào 1 Email" name="email" class="form-control" placeholder="'.$arraybien['vuilongnhapemail'].'..." />
                    </div>

                    <div class="form-group icon-input">
                        <i class="fa fa-comment"></i>
                        <textarea name="ghichu" class="form-control" rows="5" placeholder="'.$arraybien['vuilongnhapdiachi'].'..."></textarea>
                    </div>

                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" id="order_product" class="btn text-center btn-primary"><i class="fa fa-send"></i> '.$arraybien['dangky'].'</button>
            </div>
        </div>
    </div>
</div>';
return $valOrderWebsite;
