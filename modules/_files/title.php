<?php
$_arrtitle = array();
$_arrtitle['robots'] = 'index,follow,snippet,archive';
$idmodules = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'home'");
$_bgcolorfollowdm  = $db->getNameFromID("tbl_danhmuc", "bgcolor", "id", "'" . @$__idtype_danhmuc . "'");
$_bgcolorfollowdm = (!empty($_bgcolorfollowdm) && $_bgcolorfollowdm != "FFFFFF") ? $_bgcolorfollowdm : null;
// Get Color follow dm
$_colorfollowdm  = $db->getNameFromID("tbl_danhmuc", "color", "id", "'" . @$__idtype_danhmuc . "'");
$_colorfollowdm = (!empty($_colorfollowdm) && $_colorfollowdm != "FFFFFF") ? $_colorfollowdm : null;
// Get Background follow Home
$_bgcolormain  = $db->getNameFromID("tbl_danhmuc", "bgcolor", "idtype", "'" . $idmodules . "'");
$_arrtitle['bgcolormain'] = (!empty($_bgcolormain) && $_bgcolormain != "FFFFFF") ? $_bgcolormain : null;
// Get Color follow Home
$_colormain  = $db->getNameFromID("tbl_danhmuc", "color", "idtype", "'" . $idmodules . "'");
$_arrtitle['colormain']  = (!empty($_bgcolormain) && $_bgcolormain != "FFFFFF") ? $_colormain : null;
$_arrtitle['ogtype'] = 'article';
if ($_op == 'home') {
    $_arrtitle['ogtype'] = 'website';
    // ke thua $_idtype la idmodules

    $iddanhmuc   = $db->getNameFromID(
        "tbl_danhmuc",
        "id",
        "idtype",
        "'{$idmodules}'"
    );

    $_arrtitle['ten'] = $db->getNameFromID(
        "tbl_danhmuc_lang",
        "ten",
        "iddanhmuc",
        "'{$iddanhmuc}' AND idlang = '{$_SESSION['_lang']}'"
    );

    $_arrtitle['tieude'] = $db->getNameFromID(
        "tbl_danhmuc_lang",
        "tieude",
        "iddanhmuc",
        "'{$iddanhmuc}' AND idlang = '{$_SESSION['_lang']}' "
    );

    $_arrtitle['tukhoa'] = $db->getNameFromID(
        "tbl_danhmuc_lang",
        "tukhoa",
        "iddanhmuc",
        " '{$iddanhmuc}' AND idlang = {$_SESSION['_lang']} "
    );

    $_arrtitle['motatukhoa'] = $db->getNameFromID(
        "tbl_danhmuc_lang",
        "motatukhoa",
        "iddanhmuc",
        "'{$iddanhmuc}' AND idlang = {$_SESSION['_lang']} "
    );

    $_arrtitle['images']     = ROOT_PATH . 'uploads/danhmuc/thumb/' . $db->getNameFromID("tbl_danhmuc", "img", "idtype", "'" . $idmodules . "'");
} else if ($_op == 'product' && $_act == 'option') {
    // lay id option de truy van noi dung
    $_idoption = $db->getNameFromID(" tbl_option_lang", "idtype", "url", "'" . $_GET['urloption'] . "'");
    $_arrtitle['ten']        = $db->getNameFromID(" tbl_option_lang", "ten", "idtype", "'" . $_idoption . "' and idlang = '" . $_SESSION['_lang'] . "' ");
    $_arrtitle['tieude']     = $db->getNameFromID(" tbl_option_lang", "tieude", "idtype", "'" . $_idoption . "' and idlang = '" . $_SESSION['_lang'] . "' ");
    $_arrtitle['tukhoa']     = $db->getNameFromID(" tbl_option_lang", "tukhoa", "idtype", "'" . $_idoption . "' and idlang = '" . $_SESSION['_lang'] . "' ");
    $_arrtitle['motatukhoa'] = $db->getNameFromID(" tbl_option_lang", "motatukhoa", "idtype", "'" . $_idoption . "' and idlang = '" . $_SESSION['_lang'] . "' ");
    $_idtype     = $db->getNameFromID(" tbl_option_lang", "idtype", "idtype", "'" . $_idoption . "' and idlang = '" . $_SESSION['_lang'] . "' ");
    $_arrtitle['images']     = ROOT_PATH . 'uploads/option/thumb/' . $db->getNameFromID("tbl_option", "img", "id", "'" . $_idoption . "'");
    $_arrtitle['noidung'] = $db->getNameFromID(" tbl_option_lang", "noidung", "idtype", "'" . $_idoption . "' and idlang = '" . $_SESSION['_lang'] . "' ");
} else if ($_op == 'product' && $_act == 'tag') {
    $_arrtitle['ten']        = $_GET['tag'];
    $_arrtitle['tieude']     = $_GET['tag'];
    $_arrtitle['tukhoa']     = $_GET['tag'];
    $_arrtitle['motatukhoa'] = $_GET['tag'];
} else if ($_op == 'content' && $_act == 'tag') {
    $_arrtitle['ten']        = $_GET['tag'];
    $_arrtitle['tieude']     = $_GET['tag'];
    $_arrtitle['tukhoa']     = $_GET['tag'];
    $_arrtitle['motatukhoa'] = $_GET['tag'];
} else if ($_op == 'search') {
    $_key = $_POST['key'];
    if (isset($_GET['key'])) {$_key = $_GET['key'];}
    $_arrtitle['ten']        = $arraybien['ketquatimkiemtukhoa'].' "'.$_key.'"';
    $_arrtitle['tieude']     = $arraybien['ketquatimkiemtukhoa'].' "'.$_key.'"';
    $_arrtitle['tukhoa']     = $arraybien['ketquatimkiemtukhoa'].' "'.$_key.'"';
    $_arrtitle['motatukhoa'] = $arraybien['ketquatimkiemtukhoa'].' "'.$_key.'"';
} else if ($_op == 'cart') {
    $_arrtitle['ten']        = $arraybien['giohang'];
    $_arrtitle['tieude']     = $arraybien['giohang'];
    $_arrtitle['tukhoa']     = $arraybien['giohang'];
    $_arrtitle['motatukhoa'] = $arraybien['giohang'];
    $idmodules   = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'home'");
    $_arrtitle['images']     = ROOT_PATH . 'uploads/danhmuc/thumb/' . $db->getNameFromID("tbl_danhmuc", "img", "idtype", "'" . $idmodules . "'");
} else if (($_op == 'thanhvien' || $_op == 'user') && $_act == 'dangky') {
    $_arrtitle['ten']        = $arraybien['dangkythanhvien'];
    $_arrtitle['tieude']     = $arraybien['dangkythanhvien'];
    $_arrtitle['tukhoa']     = $arraybien['dangkythanhvien'];
    $_arrtitle['motatukhoa'] = $arraybien['dangkythanhvien'];
    $idmodules   = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'home'");
    $_arrtitle['images']     = ROOT_PATH . 'uploads/danhmuc/thumb/' . $db->getNameFromID("tbl_danhmuc", "img", "idtype", "'" . $idmodules . "'");
} else if (($_op == 'thanhvien' || $_op == 'user') && $_act == 'dangnhap') {
    $_arrtitle['ten']        = $arraybien['dangnhap'];
    $_arrtitle['tieude']     = $arraybien['dangnhap'];
    $_arrtitle['tukhoa']     = $arraybien['dangnhap'];
    $_arrtitle['motatukhoa'] = $arraybien['dangnhap'];
    $idmodules   = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'home'");
    $_arrtitle['images']     = ROOT_PATH . 'uploads/danhmuc/thumb/' . $db->getNameFromID("tbl_danhmuc", "img", "idtype", "'" . $idmodules . "'");
} else if ($_op == 'product' && $_act == 'save') {
    $_arrtitle['ten']        = $arraybien['sanphamdaluu'];
    $_arrtitle['tieude']     = $arraybien['sanphamdaluu'];
    $_arrtitle['tukhoa']     = $arraybien['sanphamdaluu'];
    $_arrtitle['motatukhoa'] = $arraybien['sanphamdaluu'];
} else {
    if (isset($_GET['cat'])) {
        $_arrtitle['ogtype'] = 'object';
        // lay các thẻ title của bảng danh mục
        $_arrtitle['ten']        = $db->getNameFromID("tbl_danhmuc_lang", "ten", "iddanhmuc", "'" . $__idtype_danhmuc . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['tieude']     = $db->getNameFromID("tbl_danhmuc_lang", "tieude", "iddanhmuc", "'" . $__idtype_danhmuc . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['tukhoa']     = $db->getNameFromID("tbl_danhmuc_lang", "tukhoa", "iddanhmuc", "'" . $__idtype_danhmuc . "' and idlang = " . ((isset($_SESSION['lang'])) ? $_SESSION['lang'] : 1));
        $_arrtitle['motatukhoa'] = $db->getNameFromID("tbl_danhmuc_lang", "motatukhoa", "iddanhmuc", "'" . $__idtype_danhmuc . "' and idlang = " . ((isset($_SESSION['lang'])) ? $_SESSION['lang'] : 1));
        $_arrtitle['mota'] = $db->getNameFromID("tbl_danhmuc_lang", "mota", "iddanhmuc", "'" . $__idtype_danhmuc . "' and idlang = " . ((isset($_SESSION['lang'])) ? $_SESSION['lang'] : 1));
        $_arrtitle['noidung'] = $db->getNameFromID("tbl_danhmuc_lang", "noidung", "iddanhmuc", "'" . $__idtype_danhmuc . "' and idlang = " . ((isset($_SESSION['lang'])) ? $_SESSION['lang'] : 1));
        $_arrtitle['images']     = ROOT_PATH . 'uploads/danhmuc/thumb/' . $db->getNameFromID("tbl_danhmuc", "img", "id", "'" . $__idtype_danhmuc . "'");
        if($db->getNameFromID("tbl_danhmuc", "noindex", "id", "'" . $__idtype_danhmuc . "'" )==1){
            $_arrtitle['robots'] = 'noindex';
        }
    } else if (isset($_GET['url'])) {
        $_arrtitle['ogtype'] = 'article';
    // lay các thẻ title của bảng nội dung
        // lay id noi dung roi truy van voi lang de lay dung tieu de khi chuyen ngon ngu
        $_idnoidung  = $db->getNameFromID("tbl_noidung_lang", "idnoidung", "url", "'" . $_geturl . "'");
        $_arrtitle['ten']        = $db->getNameFromID("tbl_noidung_lang", "ten", "idnoidung", "'" . $_idnoidung . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['tieude']     = $db->getNameFromID("tbl_noidung_lang", "tieude", "idnoidung", "'" . $_idnoidung . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['tukhoa']     = $db->getNameFromID("tbl_noidung_lang", "tukhoa", "idnoidung", "'" . $_idnoidung . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['motatukhoa'] = $db->getNameFromID("tbl_noidung_lang", "motatukhoa", "idnoidung", "'" . $_idnoidung . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['noidung'] = $db->getNameFromID("tbl_noidung_lang", "noidung", "idnoidung", "'" . $_idnoidung . "' and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $idtypesp    = $db->getNameFromID("tbl_noidung_lang", "idnoidung", "idnoidung", "'" . $_idnoidung . "'and idlang = " . ((isset($_SESSION['_lang'])) ? $_SESSION['_lang'] : 1));
        $_arrtitle['images']     = ROOT_PATH . 'uploads/noidung/thumb/' . $db->getNameFromID("tbl_noidung", "hinh", "id", "'" . $_idnoidung . "'");
        if($db->getNameFromID("tbl_noidung", "noindex", "id", $_idnoidung )==1){
            $_arrtitle['robots'] = 'noindex';
        }
    }
}
if($_arrtitle['motatukhoa'] == ''){$_arrtitle['motatukhoa'] = $_arrtitle['ten'];}
if( !$_arrtitle['images'] ) {
    $_arrtitle['images'] = ROOT_PATH . "templates/images/no-image-thumb.jpg";
}
