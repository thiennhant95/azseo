<?php
// lay danh muc check bottom
$data_home        = '';
$s_danhmuc_bottom = "
    SELECT a.id,b.ten,url,link,target,baiviet,colvalue
    FROM tbl_danhmuc AS a
    INNER JOIN tbl_danhmuc_lang AS b
    ON a.id = b.iddanhmuc
    and b.idlang = {$_SESSION['_lang']}
    WHERE anhien = 1
    and colvalue like '%duoi%'
    order by thutu ASC";
$d_danhmuc_bottom = $db->sqlSelectSql($s_danhmuc_bottom);
if (count($d_danhmuc_bottom) > 0) {
    foreach ($d_danhmuc_bottom as $key_danhmuc_bottom => $info_danhmucbottom) {
        $ten     = $info_danhmucbottom['ten'];
        $id      = $info_danhmucbottom['id'];
        $url     = $info_danhmucbottom['url'];
        $link    = $info_danhmucbottom['link'];
        $target  = $info_danhmucbottom['target'];
        $colvalue = $info_danhmucbottom['colvalue'];
        $checkbaiviet = strpos($colvalue, 'baiviet');
        $baiviet = 0;
        if ($checkbaiviet > -1) {
            $baiviet = 1;
        }
        $lienket = ROOT_PATH . $url . '/';
        if ($link != '') {
            $lienket      = $link;
            $target_value = ' target="' . $target . '" ';
        }
        $data_home .= '<li>
      <div class="group_bottom_fix group_bottom_menu">
         <div class="titlefix"><a href="' . $lienket . '" title="' . $ten . '" ' . $target_value . ' >' . $ten . '</a></div>
         <div class="contentfix">';
        if ($baiviet == 0) {
            // neu check bai viet se lay tieu de bai viet
            $s_danhmuc_bottom = "SELECT   a.id,b.ten,url,link,target,baiviet
                         FROM tbl_danhmuc AS a
                         INNER JOIN tbl_danhmuc_lang AS b
                         ON a.id = b.iddanhmuc
                         and b.idlang = '" . $_SESSION['_lang'] . "'
                         WHERE anhien = 1
                         and a.id like '" . $id . "%'
                         and length(a.id) = " . (strlen($id) + 4) . "
                         and colvalue like '%duoi%'
                         order by thutu ASC";
            $d_danhmuc_bottom = $db->sqlSelectSql($s_danhmuc_bottom);
            if (count($d_danhmuc_bottom) > 0) {
                $data_home .= '<ul>';
                foreach ($d_danhmuc_bottom as $key_danhmuc_bottom => $info_danhmucbottom) {
                    $ten     = $info_danhmucbottom['ten'];
                    $id      = $info_danhmucbottom['id'];
                    $url     = $info_danhmucbottom['url'];
                    $link    = $info_danhmucbottom['link'];
                    $target  = $info_danhmucbottom['target'];
                    $baiviet = $info_danhmucbottom['baiviet'];
                    $lienket = ROOT_PATH . $url . '/';
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_home .= '<li><a href="' . $lienket . '" title="' . $ten . '" ' . $target_value . ' ><i class="fa fa-angle-right"></i> ' . $ten . '</a></li>';
                }
                $data_home .= '</ul>';
            }
        } else {
            $s_tin_bottom = "SELECT a.ngaycapnhat,ten,tieude,url,link,target
                       FROM tbl_noidung AS a
                       INNER JOIN tbl_noidung_lang AS b
                       ON a.id = b.idnoidung
                       where a.anhien = 1
                       and b.idlang = '" . $_SESSION['_lang'] . "'
                       and a.idtype like '%" . $id . "%'
                       order by thutu Asc
                       limit 0,5";
            $d_tin_bottom = $db->sqlSelectSql($s_tin_bottom);
            if (count($d_tin_bottom) > 0) {
                $data_home .= '<ul>';
                foreach ($d_tin_bottom as $key_tinbottom => $info_tin_bottom) {
                    $ten         = $info_tin_bottom['ten'];
                    $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_noidung['hinh'];
                    $ngaycapnhat = $info_tin_bottom['ngaycapnhat'];
                    $tieude      = $info_tin_bottom['tieude'];
                    $url         = $info_tin_bottom['url'];
                    $link        = $info_tin_bottom['link'];
                    $target      = $info_tin_bottom['target'];
                    $mota        = $info_tin_bottom['mota'];
                    $noidung     = $info_tin_bottom['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_home .= '<li><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '"><i class="fa fa-file-text-o"></i> ' . $ten . '</a></li>';
                }
                $data_home .= '</ul>';
            }
        }
        $data_home .= '
         </div>
      </div>
  </li>';
    }
}

return $data_home;
