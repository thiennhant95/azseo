<?php
$data_user = "";
$data_user .= '<div class="content-user clearfix" id="show-box-user">';
if (!isset($_SESSION['user_id'])) {
    $data_user .= '
      <div class="dang-nhap">
         <a href="#" title="" class="">
            <i class="fa fa-user"></i>&nbsp;
            ' . $arraybien['dangnhap'] . ' | ' . $arraybien['dangky'] . '
            &nbsp;<i class="fa fa-chevron-down"></i>
         </a>
         <div class="content-sigin ">
            <div class="row">
               <div class="col-sm-4 box-fag">
                  <h3>FAQ giải đáp thắc mắc</h3>
                  <ul>
                     <li>* Tôi đặt mua hàng như thế nào</li>
                  </ul>
               </div>
               <div class="col-sm-4 box-reg">
                  <h3>' . $arraybien['dangky'] . '</h3>
                  <p>' . $db->getThongTin("gioiydangky") . '</p>
                     <a href="./register.htm" title="' . $arraybien['dangkyngay'] . '">
                     <button type="button" class="btn btn-primary col-sm-12">' . $arraybien['dangkyngay'] . '</button></a>
               </div>
               <div class="col-sm-4 box-log">
                  <h3>' . $arraybien['dangnhap'] . '</h3>
               <form action="' . ROOT_PATH . 'checkLogin.htm" method="POST">
                  <div class="form-group">
                     <input type="text" name="lg_username" id="" class="form-control flat" value="" required="required" pattern=".{4,}" placeholder="' . $arraybien['vuilongnhaptendangnhap'] . '" />
                  </div>
                  <div class="form-group">
                     <input type="password" name="lg_password" id="" class="form-control flat" value="" required="required" pattern=".{6,}" placeholder="' . $arraybien['vuilongnhapmatkhau'] . '" />
                  </div>
                  <div class="form-group">
                     <button type="submit" class="btn btn-primary col-sm-12">' . $arraybien['dangnhap'] . '</button>
                  </div>
               </form>
               </div>
            </div>
         </div>
      </div>
      ';
} else {
    $isAdmin = $db->getNameFromID("tbl_user", "nhom", "id", $_SESSION['user_id'] . " AND active = 1 ");
    $data_user .= '
      <div class="dang-nhap">
         <a href="#" title="" class="">
            <i class="fa fa-user"></i>&nbsp;
            ' . $arraybien['dangxuat'] . ' | ' . $_SESSION['tendangnhap'] . '
            &nbsp;<i class="fa fa-chevron-down"></i>
         </a>
         <div class="content-sigin ">
            <div class="row">
               <div class="col-sm-8 box-fag">
                  <h3>FAQ giải đáp thắc mắc</h3>
                  <ul>
                     <li>* Tôi đặt mua hàng như thế nào</li>
                  </ul>
               </div>
               <div class="col-sm-4 box-info-user">
                  <h3>' . $arraybien['taikhoan'] . '</h3>
                  <a href="" title="">
                     <button type="button" class="btn btn-primary col-xs-12">' . $arraybien['quanlytaikhoan'] . '</button>
                  </a>';
    echo $isAdmin;
    $data_user .= '
                  <a href="' . ROOT_PATH . 'logout.htm" title="' . $arraybien['dangxuat'] . '">
                     <button type="button" class="btn btn-default col-xs-12">' . $arraybien['dangxuat'] . '</button>
                  </a>
               </div>
            </div>
         </div>
      </div>
      ';
}
$data_user .= ' </div>
         <div class="clear"></div>';
return $data_user;
