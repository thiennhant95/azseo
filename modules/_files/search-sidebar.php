<?php

    $data = '';
    $data.= '
        <div class="search-sidebar">

            <div class="search-sidebar-content">

                <form action="'.ROOT_PATH.'tim-kiem.htm" method="post">

                    <div class="form-group">
                        <input type="text" name="key" class="form-control" placeholder="'.$arraybien['nhaptukhoacantim'].'" />
                    </div>

                    <div class="form-group">
                        <select name="danhmuc" class="form-control">
                            <option value="">'.$arraybien['theodanhmuc'].'</option>
                        </select>
                    </div>';

                    // SQL optionType lấy ra phan_nhom check locnhanh
                    $qOptionType = $db->sqlSelectSql("
                        SELECT a.id,b.ten,datatype
                        FROM tbl_option_type a
                        INNER JOIN tbl_option_type_lang b
                            ON a.id = b.idtype
                        WHERE a.anhien = 1
                            AND b.idlang = {$_SESSION['_lang']}
                            AND locnhanh = 1
                        ORDER BY a.thutu ASC
                    ");

                    if( count($qOptionType) > 0 ){

                        foreach( $qOptionType as $iOptionType ){
                            $idOptionType = $iOptionType['id'];

                            // OptionType dạng text thì bỏ qua
                            if( $iOptionType['datatype'] != 0 ){

                                // RENDER SELECT BOX
                                $data.='
                                <div class="form-group">
                                    <select name="optiontype[]" class="select2 form-control">
                                        <option value="">
                                            '.$iOptionType['ten'].'
                                        </option>
                                ';

                                // SQL Option lấy ra các giá trị con của phan_nhom
                                $qOption = $db->sqlSelectSql("
                                    SELECT a.id,b.ten
                                    FROM tbl_option a
                                    INNER JOIN tbl_option_lang b
                                        ON a.id = b.idtype
                                    WHERE a.anhien = 1
                                        AND b.idlang = {$_SESSION['_lang']}
                                        AND a.idtype = {$idOptionType}
                                ");

                                if( count($qOption) > 0 ){

                                    foreach( $qOption as $iOption ){

                                        $data.='
                                        <option value="'.$iOption['id'].'">
                                            '.$iOption['ten'].'
                                        </option>';
                                    }
                                }

                                $data.='
                                    </select>
                                </div>';
                            }

                        }
                    }

                $data.='

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">
                            '.$arraybien['timkiem'].'
                        </button>
                    </div>

                </form>

            </div>

        </div>
    ';

    return $data;
