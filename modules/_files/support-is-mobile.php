<?php
$valsm = '';
if ($didong->isMobile()) {
$sSupport = "SELECT yahoo,skype,dienthoai,email,ten
            FROM tbl_hotrotructuyen AS a
            INNER JOIN tbl_hotrotructuyen_lang AS b
            WHERE a.id = b.idtype
            AND anhien = 1
            AND idlang = {$_SESSION['_lang']}
            ORDER by thutu ASC";
$dSupport = $db->sqlSelectSql($sSupport);
    $valsm.='
    <div class="support-fix clearfix">
        <div class="suportfix-btn">
            <span>'.$arraybien['hotro'].'</span>
            <div class="icon rotate360"></div>
        </div>';
            if (count($dSupport) > 0) {
                $valsm.='<div class="supportfix-content">';
                    foreach ($dSupport as $kSupport => $vSupport) {
                        $zalo      = $vSupport['yahoo'];
                        $skype     = $vSupport['skype'];
                        $dienthoai = $vSupport['dienthoai'];
                        $email     = $vSupport['email'];
                        $ten       = $vSupport['ten'];

                        $valsm.='
                        <div class="rowspo clearfix">
                            <div class="itemsp ten">'.$ten.'</div>
                            <div class="itemsp dienthoai">
                                <a href="tel:'.$dienthoai.'" title="phone">
                                    <i class="fa fa-phone fa-lg"></i>
                                </a>
                            </div>
                            <div class="itemsp zalo">
                                <a href="https://chat.zalo.me/" title="">
                                <img src="'.ROOT_PATH.'smarty/templates/images/zalo.png" alt="'.$ten.'" class="img-responsive" />
                                </a>
                            </div>
                            <div class="itemsp skype">
                                <a href="skype:'.$skype.'?chat">
                                    <img src="'.ROOT_PATH.'smarty/templates/images/skype.png" alt="'.$ten.'" class="img-responsive" />
                                </a>
                            </div>
                        </div>';
                    }
                $valsm.='</div>';
            }
            $valsm.='
    </div>';
    return $valsm;
} else {
    return null;
}


