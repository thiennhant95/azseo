<?php

$v_news ='';

$eCatP = "SELECT a.id,a.idtype,img,b.ten,url,link,target,colvalue,icon,bgcolor,mota
FROM tbl_danhmuc AS a
INNER JOIN tbl_danhmuc_lang AS b
ON a.id = b.iddanhmuc
AND b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
AND colvalue LIKE '%banggiathietke%'
ORDER BY thutu ASC
LIMIT 50";
$fCatP = $models->sql_select_sql($eCatP);
// echo '<pre>'; print_r($fCatP); echo '</pre>';exit();

// CỘT TRÁI
$v_news.= '<div class="cottraibanggia">';

if (count($fCatP) > 0) {
	foreach ($fCatP as $kdm => $vdm){
		$id       = $vdm['id'];
		$idtype   = $vdm['idtype'];
		$colvalue = $vdm['colvalue'];
		$ten      = $vdm['ten'];
		$mota     = $vdm['mota'];
		$hinh 	  = $vdm['img'];
		$linkhinh = ROOT_PATH.'uploads/danhmuc/'.$hinh;
		if (!empty($vdm['link'])){
			$lienket = $vdm['link'];
		}
		else{
			$lienket = ROOT_PATH.$vdm['url'].'/';
		}


		$v_news.= '<div class="titlebase-home clearfix wow animated fadeInUp" data-wow-delay="0s">';
		$v_news.= '<h2><a href="'.$lienket.'" title="'.$ten.'">'.$ten.'</a>
		<span class="title-mota wow animated fadeInUp clearfix">'.$mota.'</span>
		<img src="'.$linkhinh.'" />
		</h2>';
		$v_news.= '</div>';
        // echo '<pre>'; print_r($v_news); echo '</pre>'; exit();

		$catOP = $models->getNameFromID("tbl_danhmuc_type", "op", "id", "'{$idtype}'");
		if ($catOP == 'product' || $catOP == 'content' || $catOP == 'service' || $catOP=='intro' || $catOP=='video' || $catOP=='project') {
			$d_sp = "
			SELECT a.id,a.hinh,keywebsite,a.idtype,noidung,mota,ten,url,hinh,link,target,gia
			FROM tbl_noidung AS a
			INNER JOIN tbl_noidung_lang AS b
			ON a.id = b.idnoidung
			WHERE a.anhien = 1
			AND b.idlang = {$_SESSION['_lang']}
			AND a.idtype like '%{$id}%'
            -- AND colvalue like '%home%'
            ORDER by a.thutu Asc
            LIMIT 12";
            $eSP = $models->sql_select_sql($d_sp);
            // echo '<pre>'; print_r($eSP); echo '</pre>'; exit();

            if (count($eSP) > 0) {
            	$v_news .='<ul class="noidungchitiet_banggia">';
            	foreach($eSP as $key_noidung => $info_noidung){
            		$id       = $info_noidung['id'];
            		$ten      = $info_noidung['ten'];
            		$url      = $info_noidung['url'];
            		$mota     = $info_noidung['mota'];
            		$noidung  = $info_noidung['noidung'];
            		$gia      = $info_noidung['gia'];
            		$fixgia   = number_format($gia, 0, ',', '.');
            		$link     = ROOT_PATH.$url;

            		$v_news.='<li>';
            		$v_news.='<div class="noidungbangia">';

            		$v_news.='<div class="noidungtren">';
            		$v_news.='<div class="ten">'.$ten.'</div>';
            		$v_news.='<div class="mota">'.$mota.'</div>';
            		$v_news.='<div class="gia">'.$fixgia.' đ</div>';
            		$v_news.='</div>';

            		$v_news.='<div class="dangky"><a href="javascript:;">'.$arraybien['dangkyngay'].'</a></div>';

            		$v_news.='<div class="noidung">'.$noidung.'</div>';
            		$v_news.='</div>';
            		$v_news.='</li>';
            	}
            	$v_news.='</ul>';


            }
        }
    }
}
// END CỘT TRÁI BẢNG GIÁ
$v_news.='</div>';

// CỘT PHẢI BẢNG GIÁ
$v_news.='<div class="cotphaibanggia"><img src="'.ROOT_PATH.'uploads/images/bgphai.png"/></div>';

return $v_news;