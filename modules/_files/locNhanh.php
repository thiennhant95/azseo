<?php
// khoi tao session loc
if($_SESSION['loc_idtype'] != $__idtype_danhmuc){
  unset($_SESSION["loc_idnhomid"]);
}
$data_locNhanh = "";
$s_phannhom    = "SELECT a.id,b.ten
               FROM tbl_option_type a INNER JOIN tbl_option_type_lang b ON
               a.id = b.idtype
               AND b.idlang = '" . $_SESSION['_lang'] . "'
               WHERE a.anhien = 1
               AND locnhanh = 1
               ORDER BY a.thutu ASC";
$d_phannhom = $db->sqlSelectSql($s_phannhom);
$dem_nhomchinh = 0;

if (count($d_phannhom) > 0 && $_op == 'product' && $_act == '') {
$data_locNhanh.='
<div class="filter-container height-filter">
<div class="filter-wrap height-filter">';
  $data_nhomchinh_locNhanh = '';
    foreach ($d_phannhom as $key_phannhom => $value_phannhom) {
        $phannhom_id     = $value_phannhom['id'];
        $phannhom_idtype = $value_phannhom['idtype'];
        $phannhom_ten    = $value_phannhom['ten'];

        if($_SESSION['loc_idtype'] != $__idtype_danhmuc){
            unset($_SESSION["loc_idnhomnd".$phannhom_id]);
        }

        // Truy van cap Con h
        $s_ndNhom = "SELECT a.id,b.ten,b.idtype
                           FROM tbl_option a
                           INNER JOIN tbl_option_lang b
                           ON a.id = b.idtype
                           AND b.idlang = '" . $_SESSION['_lang'] . "'
                           WHERE a.anhien = 1
                           AND a.idtype = " . $phannhom_id . " ";
        $d_ndNhom = $db->sqlSelectSql($s_ndNhom);
        if (count($d_ndNhom) > 0) {

        $data_nhom_locnhanh = '
            <div class="phannhom-item">
               <h3>' . $phannhom_ten . '</h3>';
            $data_nhom_locnhanh .= '
                     <div class="ndlocnhanh-content">
                        <ul class="ulloc">';
            $dem_option = 0;
            foreach ($d_ndNhom as $key_ndNhom => $value_ndNhom) {
                $ndNhom_id     = $value_ndNhom['id'];
                $ndNhom_idtype = $value_ndNhom['idtype'];
                $ndNhom_ten    = $value_ndNhom['ten'];
                $s_sp_inside   = "SELECT a.id FROM tbl_option_value a
                                 INNER JOIN tbl_option_lang b ON
                                 b.idtype = '" . $ndNhom_id . "'
                                 AND b.idlang = '" . $_SESSION['_lang'] . "'
                                 AND a.noidung LIKE '%" . $ndNhom_id . "%'
                                 INNER JOIN tbl_noidung c ON
                                 a.idnoidung = c.id
                                 AND c.idtype LIKE '%" . $__idtype_danhmuc . "%'
                                 AND c.loai = 1 ";
                $d_sp_inside = $db->sqlSelectSql($s_sp_inside);
                // echo '<pre>'; print_r($d_sp_inside); echo '</pre>'; die();
                // Kiểm tra session để active thẻ li đang được chọn
                $checkLocnhanh = '';
                if ($_SESSION['loc_idnhomnd'.$phannhom_id]== $ndNhom_id)
                {
                  $checkLocnhanh = ' choosed ';
                }
                if (count($d_sp_inside) > 0) {
                  $dem_option++;
                    $data_nhom_locnhanh .= '
                     <li class="click_filter ' . $checkLocnhanh . '" >
                        <i class="item-check"></i>
                        <a href="javascript:void(0);" title="' . $ndNhom_ten . '" ' . $checkLocnhanh . '>
                           ' . $ndNhom_ten . ' ';
                           //<span>(' . count($d_sp_inside) . ')</span>
                          $data_nhom_locnhanh.='
                           <input type="hidden" name="this_value_nhomid" value="' . $phannhom_id . '">
                           <input type="hidden" name="this_value_nhomnd" value="' . $ndNhom_id . '">
                           <input type="hidden" name="this_link_loc" value="' . ROOT_PATH . 'modules/_files/locNhanh_ajax.php">
                        </a>
                     </li>';
                }
            }
            $data_nhom_locnhanh .= '</ul></div>';
            $data_nhom_locnhanh .= '</div>';
        }
        if($dem_option>0){
          $data_nhomchinh_locNhanh.=$data_nhom_locnhanh;
          $dem_nhomchinh++;
        }
    }
    if($dem_nhomchinh >0){
      $data_locNhanh .= '<div class="phannhom-content">'.$data_nhomchinh_locNhanh.'</div>';
    }
$data_locNhanh.='</div></div>';// End filter-wrap
}

/** Return Muc Gia */
/*
$sd_pricefilter = $db->sqlSelectSql("SELECT * FROM tbl_khoanggia WHERE anhien = 1");
if (count($sd_pricefilter) > 0 && $_op == 'sanpham' && $_act == '') {
    $data_locNhanh .= '<div class="phannhom-content khoanggia-content">
    <div class="phannhom-item"><h3>Khoảng giá</h3></div>
    <div class="ndlocnhanh-content"><ul>';
    foreach ($sd_pricefilter as $key_pricefilter => $value_pricefilter) {
        $pricefilter_id     = $value_pricefilter['id'];
        $pricefilter_ten    = $value_pricefilter['ten'];
        $pricefilter_giatu  = $value_pricefilter['giatri_start'];
        $pricefilter_giaden = $value_pricefilter['giatri_end'];

        //Kiem tra va gan checked
        if ($_SESSION['giaTu'] == $pricefilter_giatu && $_SESSION['giaDen'] == $pricefilter_giaden) {
           $checkedprice = ' choosed ';
           $setChoose = 'checked';
        } else {
            $checkedprice = '';
            $setChoose = 'unchecked';
        }

        $data_locNhanh .= '<li data-prices=\'{"start":"'.$pricefilter_giatu.'","end":"'.$pricefilter_giaden.'"}\' data-choose="'.$setChoose.'" class="filter-price '.$checkedprice.'"><i class="item-check"></i><a href="javascript:void(0);" title="'.$pricefilter_ten.'">'.$pricefilter_ten.'</a></li>';
    }
    $data_locNhanh .= '</ul></div></div>
    <input type="hidden" name="this_gia_loc" value="' . ROOT_PATH . 'application/files/locgia_ajax.php">';
}
*/

// set session theo idtype
$_SESSION['loc_idtype'] = $__idtype_danhmuc;
return $data_locNhanh;
