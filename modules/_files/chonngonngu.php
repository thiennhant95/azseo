<?php
   $data_chonngonngu = "";
   $data_chonngonngu_content = "";
   $data_lang_default = '';
   // Truy van ngon ngu
   $d_lang = $db->sqlSelectSql("
      SELECT a.id,b.ten,a.hinh
      FROM tbl_lang AS a
      INNER JOIN tbl_lang_lang AS b
            ON a.id = b.iddanhmuc
      WHERE a.anhien = 1
            AND b.idlang = {$_SESSION['_lang']}
      ORDER BY a.thutu ASC
   ");
   if (count($d_lang) > 1) {

         foreach ($d_lang as $key_lang => $value_lang) {
            $lang_id          = $value_lang['id'];
            $lang_name        = $value_lang['ten'];
            $lang_hinh        = ROOT_PATH.'uploads/lang/'.$value_lang['hinh'];
            $class_disable    = ($_SESSION['_lang']==$lang_id)? ' class="disabled" ': NULL;
            $data_chonngonngu_content .='

            <li'.$class_disable.'>
                  <a href="'.ROOT_PATH.'changelang-'.$lang_id.'.htm" title="'.$lang_name.'">
                        <img class="img" src="'.$lang_hinh.'" alt="'.$lang_name.'" />
                  </a>
            </li>';
         }

   }

    $data_chonngonngu.='

    <div class="flag-lang">
        <ul class="ngonngu" ">
            '.$data_chonngonngu_content.'
        </ul>
    </div>';

   return $data_chonngonngu;
 ?>