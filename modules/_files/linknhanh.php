<?php
$vCaNe = '';
#================================================
# QUERY CAT
#================================================
$sCat = "

SELECT a.id,img,b.ten,url,link,target,mota
FROM tbl_danhmuc AS a
   INNER JOIN tbl_danhmuc_lang AS b
   ON a.id = b.iddanhmuc and b.idlang = {$_SESSION['_lang']}
WHERE anhien = 1
   AND colvalue LIKE '%home%'
   AND keyname = 'linknhanh'
ORDER BY a.thutu ASC
LIMIT 50

";

$dCat = $db->sqlSelectSql($sCat);

if (count($dCat) > 0) {
    $vCaNe.='

        <div class="clearfix form-group ws-container catnew-container">';

        foreach ($dCat as $kCat => $vCat) {
            $cat_id     = $vCat['id'];
            $cat_name   = $vCat['ten'];
            $cat_link   = !empty($vCat['link']) ? $vCat['link'] : ROOT_PATH. $vCat['url'].'/';
            $cat_target = !empty($vCat['target']) ? 'target="'.$vCat['target'].'"' : null;

            $vCaNe.='

                <div class="catnew-item clearfix form-group">

                    <div class="form-group clearfix catnew-title">
                        <a href="'.$cat_link.'" title="'.$cat_name.'" '.$cat_target.'>
                            '.$cat_name.'
                        </a>
                    </div>
                    <!-- ./catnew-title -->';

                    #================================================
                    # QUERY NEW
                    #================================================
                    $sNew = "

                        SELECT a.id,a.hinh,masp,gia,giagoc,a.idtype,ten,url,link,target,mota
                        FROM tbl_noidung AS a
                        INNER JOIN tbl_noidung_lang AS b
                            ON a.id = b.idnoidung
                        WHERE a.anhien = 1
                            AND b.idlang = {$_SESSION['_lang']}
                            AND a.idtype LIKE '%{$cat_id}%'
                            AND colvalue like '%home%'
                        ORDER by a.thutu Asc
                        LIMIT 120

                    ";

                    $dNew = $db->sqlSelectSql($sNew);

                    if (count($dNew) > 0) {
                        $vCaNe.='

                            <div class="catnew-content clearfix form-group">
                                <ul class="clearfix">';

                            foreach ($dNew as $kNew => $vNew) {
                                $new_id   = $vNew['id'];
                                $new_name = $vNew['ten'];
                                $new_link = !empty($vNew['link']) ? $vNew['link'] : ROOT_PATH.$vNew['url'];

                                $vCaNe.='

                                    <li class="clearfix">
                                        <a href="'.$new_link.'" title="'.$new_name.'">
                                            '.$new_name.'
                                        </a>
                                    </li>

                                ';
                            }


                        $vCaNe.='

                                </ul>
                            </div>
                            <!-- ./catnew-content -->

                        ';
                    }



            $vCaNe.='

                </div>
                <!-- ./catnew-item -->

            ';
        }

    $vCaNe.='

        </div>
        <!-- ./catnew-container -->

    ';
}

return $vCaNe;
