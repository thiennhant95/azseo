<?php
/**=============================================
@ TRUY VAN HINH ANH CHI TIET*/
$sGallery = "
SELECT a.id,a.hinh
FROM tbl_noidung AS a
INNER JOIN tbl_noidung_lang AS b
     ON a.id = b.idnoidung
 WHERE a.anhien = 1
     AND b.idlang = {$_SESSION['_lang']}
     AND loai = ".array_search("picture", $_arr_loai_noidung)."
     AND url = '{$_geturl}'
 ORDER by a.thutu Asc";
$qGallery = $db->sqlSelectSql($sGallery);

if ( count($qGallery) > 0 ) {
    $idGallery = $qGallery[0]['id'];
    $sGall = "
    SELECT hinh
    FROM tbl_noidung_hinh
    WHERE idnoidung = '{$idGallery}'
    ORDER BY id ASC";
    $qGall = $db->sqlSelectSql($sGall);
    if ( count($qGall) > 0 ) {
        $smarty->assign("gall", $qGall);
    }
}
?>