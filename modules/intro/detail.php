<?php
$d_gioithieu = $db->layNoiDung([
    "op"     => $_op,
    "where"  => "AND url = '{$_geturl}'
                AND anhien = 1
                AND idlang = {$_SESSION['_lang']}",
    // "debug"  => true

]);
// echo '<pre>'; print_r($d_gioithieu); echo '</pre>'; exit();

$data_gioithieu_chitiet = '';
if (count($d_gioithieu) > 0) {
    $d_gioithieu = $d_gioithieu[0];
    $ten         = $d_gioithieu['ten'];
    $id          = $d_gioithieu['id'];
    $idtype      = $d_gioithieu['idtype'];
    $hinh        = $d_gioithieu['hinh'];
    $solanxem    = $d_gioithieu['solanxem'];
    $url         = $d_gioithieu['url'];
    $link        = $d_gioithieu['link'];
    $target      = $d_gioithieu['target'];
    $mota        = $d_gioithieu['mota'];
    $noidung     = $d_gioithieu['noidung'];
    $hinh        = $d_gioithieu['hinh'];
    $linkhinh    = ROOT_PATH.'uploads/noidung/'.$hinh;
    $tag         = $d_gioithieu['tag'];
    $ngaycapnhat = $d_gioithieu['ngaycapnhat'];
    $ngayhientai = $db->getDateTimes();
    $arraylabel  = array(
        "ngay"      =>  $arraybien['ngay'],
        "ngaytruoc" =>  $arraybien['ngaytruoc'],
        "gio"       =>  $arraybien['gio'],
        "phut"      =>  $arraybien['phut'],
        "luc"       =>  $arraybien['luc'],
        "cachday"   =>  $arraybien['cachday'],
    );
    $data_gioithieu_chitiet .= '<div class="noidungchitiet">';
    $data_gioithieu_chitiet .= '<h1 class="title">' . $ten . '</h1>';
    if ($_array_config_noidung['ngay'] == 1) {
        $data_gioithieu_chitiet .= '<div class="ngaycapnhat clearfix form-group">' . $db->ngaydang($ngayhientai, $ngaycapnhat, $arraylabel) . '</div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_gioithieu_chitiet .= include "plugin/share/share.php";
    }

    /* NỘI DUNG ĐƯỢC HIỂN THỊ RA ĐÂY */
    $data_gioithieu_chitiet .= '<div class="hinh">
    <img src="'.$linkhinh.'" title="'.$ten.'"/>
    </div>';
    $data_gioithieu_chitiet .= '<div class="mota">'.$mota.'</div>';
    $data_gioithieu_chitiet .= '<div class="noidung">'.$noidung.'</div>';
    $data_gioithieu_chitiet .= '<h2 class="hide">'.$ten.'</h2>';
// update so lan xem
if($_SESSION['countnoidung'][$id]==''){
    $s_update_solanxem = "update tbl_noidung set solanxem = solanxem+1 where id = '".$id."' ";
    $d_update_solanxem = $db->rawQuery($s_update_solanxem);
    $_SESSION['countnoidung'][$id]=$id;
}

// tag
    $tagarr     = '';
    $tagcontent = '';
    $tags       = trim($tags);
    $tagcontent = '';
    if ($tag != '') {
        $tagarr = explode('|', $tag);
        if (count($tagarr) > 0) {
            $tagcontent .= '<div class="clear"></div>';
            $tagcontent .= '<ul class="tags"><li class="tags_label"><i class="fa fa-tags"></i>Tags</li>';
            $soluongtag = count($tagarr);
            for ($i = 0; $i <= $soluongtag; $i++) {
                if ($tagarr[$i] != '' && $tagarr[$i] != ',') {
                    $link_tag = ROOT_PATH . urlencode(trim($tagarr[$i]) . '.tag');
                    $tagcontent .= '<li class="tagitem"><a href="' . $link_tag . '" title="' . $tagarr[$i] . '">' . $tagarr[$i] . '</a></li>';
                }
            }
            $tagcontent .= '</ul>';
        }
    }
    $data_gioithieu_chitiet .= $tagcontent;
    $data_gioithieu_chitiet .= '<div class="clear"></div>';
    if ( $_getArrayconfig['binhluanwebsite'] == 'on' ) {
        $data_gioithieu_chitiet .= '
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=401401003393108";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, \'script\', \'facebook-jssdk\'));</script>
            <div class="fb-comments" data-href="http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] . '"
        data-width="100%" data-numposts="5" data-colorscheme="light"></div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_gioithieu_chitiet .= include "plugin/share/share.php";
    }
    $data_gioithieu_chitiet .= '<div class="clear"></div><br />';
    // lay tin lien quan
    if ( $_array_config_noidung['tinlienquan'] ) {
            $noidunglienquan_title .= '
            <div class="titlebase clearfix">
                <h2>
                    <div class="subtitle"><a href="javascript:">
                        '.$arraybien['noidungcungchuyenmuc'].'
                        <div class="line"></div>
                        </a>
                    </div>
                </h2>
            </div>';
            $smarty->assign('noidunglienquan_title', $noidunglienquan_title);
            $smarty->assign('gioithieulienquan', $db->layDanhMuc([
                "op" => $_op,
                "where" => "AND a.idtype like '%{$__idtype_danhmuc}%'
                            AND anhien = 1
                            AND idlang = {$_SESSION['_lang']}
                            AND url != '{$url}'",
                "limit" => [0, $_array_config_noidung['sotinlienquan']]
            ]));
    }
    $data_gioithieu_chitiet .= '<div class="clear"></div></div>'; // end div noi dung chi tiet
} else {
    $data_gioithieu_chitiet .= include "application/files/404.php";
}
$chitietgioithieu = $data_gioithieu_chitiet;

// do du lieu vao tpl
$smarty->assign('chitietgioithieu', $chitietgioithieu);
