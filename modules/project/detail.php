<?php

$d_noidung = $db->sqlSelectSql("
    SELECT a.id,a.idtype,hinh,
        solanxem,ngaycapnhat,b.ten,url,link,
        target,mota,noidung,tag
    FROM tbl_noidung AS a
        INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
    WHERE b.idlang = {$_SESSION['_lang']}
        AND url = '{$_geturl}'
        AND anhien = 1
    ORDER by thutu ASC
");
$data_noidung_chitiet = '';
if (count($d_noidung) > 0) {
    $d_noidung   = $d_noidung[0];
    $ten         = $d_noidung['ten'];
    $id          = $d_noidung['id'];
    $idsp        = $d_noidung['id'];
    $idtype      = $d_noidung['idtype'];
    $hinh        = $d_noidung['hinh'];
    $solanxem    = $d_noidung['solanxem'];
    $url         = $d_noidung['url'];
    $link        = $d_noidung['link'];
    $target      = $d_noidung['target'];
    $mota        = $d_noidung['mota'];
    $noidung     = $d_noidung['noidung'];
    $tag         = $d_noidung['tag'];
    $ngaycapnhat = $d_noidung['ngaycapnhat'];
    $ngayhientai = $db->getDateTimes();
    $arraylabel  = array(
        "ngay"      =>  $arraybien['ngay'],
        "ngaytruoc" =>  $arraybien['ngaytruoc'],
        "gio"       =>  $arraybien['gio'],
        "phut"      =>  $arraybien['phut'],
        "luc"       =>  $arraybien['luc'],
        "cachday"   =>  $arraybien['cachday'],
    );

$sduan = "
SELECT * FROM `tbl_noidung_duan` WHERE idtype = {$id}";
$qduan = $db->sqlSelectSql($sduan);

    $data_noidung_chitiet .= '
    <div class="noidungchitiet clearfix"> ';

if ( !!count($qduan) ) {
    $data_noidung_chitiet.='
        <div class="tab-duan clearfix">
            <ul class="list-duan clearfix">';
            foreach ($qduan as $kduan => $vduan) {
                $data_noidung_chitiet.='
                <li>
                    <a data-toggle="tooltip" href="#'.$vduan['flag'].'" data-duan="1" title="'.$vduan['ten'].'">'.$vduan['ten'].'</a>
                </li>
                ';
            }
        /**=======================================================================
        @ START THAY THE NUT FORM*/
        $btnNhanUuDai = '
        <a class="btn btn-primary" data-toggle="modal" href=\'.form-info\'>'.$arraybien['nhanuudai'].'</a>
        ';
        /*=======================================================================
        @ END THAY THE NUT FORM*/
            $data_noidung_chitiet.='
            </ul>
        </div>
        <!-- .tab-duan clearfix -->
        <div class="content-duan clearfix">';
            foreach ($qduan as $kduan => $vduan) {
                $vduan['noidung'] = str_replace("[FORMINFO]", $btnNhanUuDai, $vduan['noidung']);
                $data_noidung_chitiet.='
                <div class="'.$vduan['flag'].' clearfix duan-item" id="'.$vduan['flag'].'">

                    <h3 class="h3 duan-title">
                        '.$vduan['ten'].'
                    </h3>
                    <div class="duan-box clearfix">
                        '.$vduan['noidung'].'
                    </div>
                    <!-- .duan-box clearfix -->
                <!-- .h3 -->

                </div>';
            }$data_noidung_chitiet.='
        </div>
        <!-- .content-duan clearfix -->
    ';
}



    if ($_array_config_noidung['ngay'] == 1) {
        $data_noidung_chitiet .= '<div class="ngaycapnhat clearfix form-group">' . $db->ngaydang($ngayhientai, $ngaycapnhat, $arraylabel) . '</div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }

/* NỘI DUNG ĐƯỢC HIỂN THỊ RA ĐÂY */
$data_noidung_chitiet.= '<div class="mota">'.$mota.'</div>';
$data_noidung_chitiet.= '<div class="noidung">'.$noidung.'</div>';
$data_noidung_chitiet .= '<h2 class="hide">'.$ten.'</h2>';

$data_noidung_chitiet.= '

<div class="modal fade form-info" id="" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">'.$arraybien['nhanuudaingayhomnay'].'</h4>
            </div>
            <div class="modal-body uudai-body">';
$token_uudai = new Token();
$token_uudai->setName('uudai');
$data_noidung_chitiet.= '
                <input type="hidden" name="token_uudai" value="'.$token_uudai->create('anytime').'" />
                <div id="error-send-uudai"></div><!-- #error-send-uudai -->
                <div class="form-group">
                    <label>
                        <i class="fa fa-user"></i>
                        '.$arraybien['hovaten'].'
                    </label>
                    <input type="text" class="form-control ws-required" name="hoten" />
                </div>
                <!-- .form-group -->

                <div class="form-group">
                    <label>
                        <i class="fa fa-phone"></i>
                        '.$arraybien['sodienthoai'].'
                    </label>
                    <input type="text" name="dienthoai" class="form-control ws-required ws-required-number" data-wserror-number="'.$arraybien['vuilongnhapsodienthoai'].'" />
                </div>
                <!-- .form-group -->
            </div>
            <div class="modal-footer">
                <button type="button" id="uudai-btn" class="btn btn-primary">
                    '.$arraybien['dangkyngay'].'
                    <i class="fa fa-pencil-square-o"></i>
                </button>
            </div>
        </div>
    </div>
</div>
';


// update so lan xem
if($_SESSION['countnoidung'][$id]==''){
    $s_update_solanxem = "update tbl_noidung set solanxem = solanxem+1 where id = '".$id."' ";
    $d_update_solanxem = $db->rawQuery($s_update_solanxem);
    $_SESSION['countnoidung'][$id]=$id;
}

// tag
    $tagarr     = '';
    $tagcontent = '';
    $tags       = trim($tags);
    $tagcontent = '';
    if ($tag != '') {
        $tagarr = explode('|', $tag);
        if (count($tagarr) > 0) {
            $tagcontent .= '<div class="clear"></div>';
            $tagcontent .= '<ul class="tags"><li class="tags_label"><i class="fa fa-tags"></i>Tags</li>';
            $soluongtag = count($tagarr);
            for ($i = 0; $i <= $soluongtag; $i++) {
                if ($tagarr[$i] != '' && $tagarr[$i] != ',') {
                    $link_tag = ROOT_PATH . urlencode(trim($tagarr[$i]) . '.tag');
                    $tagcontent .= '<li class="tagitem"><a href="' . $link_tag . '" title="' . $tagarr[$i] . '">' . $tagarr[$i] . '</a></li>';
                }
            }
            $tagcontent .= '</ul>';
        }
    }
    $data_noidung_chitiet .= $tagcontent;
    $data_noidung_chitiet .= '<div class="clear"></div>';
    if ($_array_config_noidung['commentfacebook'] == 1) {
        $data_noidung_chitiet .= '
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=401401003393108";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, \'script\', \'facebook-jssdk\'));</script>
         <div class="fb-comments" data-href="http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] . '"
      data-width="100%" data-numposts="5" data-colorscheme="light"></div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }
    $data_noidung_chitiet .= '<div class="clear"></div><br />';
    // lay tin lien quan
    if ($_array_config_noidung['tinlienquan'] == 1) {
        $s_noidung_lienquan = "
        SELECT a.hinh,a.ngaycapnhat,ten,tieude, url,link,target,mota
        FROM tbl_noidung AS a
        INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung AND b.idlang = ".$_SESSION['_lang']."
        where a.anhien = 1
        and a.idtype like '%$__idtype_danhmuc%'
        and url != '$url'
        order by thutu Asc
        limit 0," . $_array_config_noidung['sotinlienquan'] . " ";
        $d_noidung_lienquan = $db->sqlSelectSql($s_noidung_lienquan);
        $arrdata['noidung'] = $d_noidung_lienquan;
        if (count($d_noidung_lienquan) > 0) {
            $noidunglienquan_title .= '
                <div class="titlebase clearfix">
                    <h2>
                        <div class="subtitle"><a href="javascript:">
                           '.$arraybien['noidungcungchuyenmuc'].'
                            <div class="line"></div>
                            </a>
                        </div>
                    </h2>
                </div>
                ';
            // do du lieu vao tpl
            $smarty->assign('noidunglienquan_title', $noidunglienquan_title);
            $smarty->assign('noidunglienquan', $arrdata);
        }
    }
    $data_noidung_chitiet .= '<div class="clear"></div></div>'; // end div noi dung chi tiet
} else {
    $data_noidung_chitiet .= include "application/files/404.php";
}
$chitietnoidung = $data_noidung_chitiet;

// do du lieu vao tpl
$smarty->assign('chitietnoidung', $chitietnoidung, true);
