<?php

 $s_sql2 = '';
 $s_sql1 = '';
if ( $_SESSION['loc_idtype'] != '' ) {
    // neu ton tai id roi thi moi truy van cap con
    $d_nhom = $db->sqlSelectSql("
        SELECT id FROM tbl_option_type WHERE locnhanh = 1
    ");
    if ( count($d_nhom)>0 ) {
        foreach ($d_nhom as $key_nhom => $info_nhom) {
            $idnhom = $info_nhom['id'];
            if ( $_SESSION['loc_idnhomnd'.$idnhom] !='' ) {
                $s_sql2 .= " AND a.id in( SELECT idnoidung FROM tbl_option_value WHERE noidung LIKE '%{$_SESSION['loc_idnhomnd'.$idnhom]}%' AND idoptiontype = {$idnhom} ) ";
            }
        }
    }
}// end iff ton tai id

// phan trang
// kiểm tra nếu còn danh mục con
$arrdata = array();
$strlenid = strlen($__idtype_danhmuc)+4;

$qCatP = $db->layDanhMuc([
    "op" => $_op,
    "where" => "AND a.id LIKE '{$__idtype_danhmuc}%'
                AND LENGTH(a.id) = {$strlenid}
                AND idlang = {$_SESSION['_lang']}
                AND anhien = 1",
    "limit" => 50
]);

if( count($qCatP) > 0 ){
    $arrdata['danhmuccon'] = $qCatP;
}

// Truy van danh muc con
if ( count($qCatP) > 0 && !1 ) {
    $arrdata['sanphamgroup'] = $qCatP;

    foreach ($qCatP as $key => $infoGroup) {
        $id = $infoGroup['id'];
        $dSP = $db->sqlSelectSql(
            "SELECT a.id,a.hinh,masp,gia,giagoc,keywebsite,a.idtype,a.ngaycapnhat,ten,url,link,target,mota,rating,ratingCount
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
                ON a.id = b.idnoidung
            WHERE a.anhien = 1
                AND b.idlang = {$_SESSION['_lang']}
                AND a.idtype LIKE '%{$id}%'
            ORDER by a.thutu Asc
            LIMIT 8"
        );
        if (count($dSP) > 0) {
            $arrdata['spgroup'][$id] = $dSP;
        }
    }
    $smarty->assign("arrdata", $arrdata, true);
} else {
    $phantrang =  '';
    $page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
    $page       = ($page == 0 ? 1 : $page);
    $perpage    = $_getArrayconfig['soluongsanpham'];
    $startpoint = ($page * $perpage) - $perpage;
    $product_type = array_search($_op, $_arr_loai_noidung);
    $sql_page = "";

    $sql_sort = null;
    $get_session_sort = $_SESSION['sort'];
    if( isset($get_session_sort) ) {
        switch ($get_session_sort) {
            case 0:
                $sql_sort = ["solanxem", "ASC"];
                break;
            case 1:
                $sql_sort = ["gia", "ASC"];
                break;
            case 2:
                $sql_sort = ["gia", "DESC"];
                break;
            case 3:
                $sql_sort = ["solanmua", "DESC"];
                break;
            case 4:
                $sql_sort = ["solanmua", "DESC"];
                break;
            case 5:
                $sql_sort = ["solanxem", "DESC"];
                break;
            default:
                # code...
                break;
        }
    }
    $d_noidung = $db->layNoiDung([
        "op" => $_op,
        "select" => "timesale, sumsale, giasale",
        "where" => "AND a.idtype LIKE '%{$__idtype_danhmuc}%'
                    AND anhien = 1
                    AND idlang = {$_SESSION['_lang']}
                    {$s_sql2}",
        "limit" => [$startpoint, $perpage],
        "orderby" => $sql_sort,
    ]);
    // echo $d_noidung;


    if (count($d_noidung) > 0) {
        $arrdata['sanpham'] = $d_noidung;
    }
    $url_page = ROOT_PATH . $_getcat . '-trang-';
    $phantrang .= Pages(
        "SELECT COUNT(*) as num
        FROM  tbl_noidung As a
        INNER JOIN tbl_noidung_lang AS b
        ON a.id = b.idnoidung
        WHERE a.anhien = 1
            AND b.idlang = {$_SESSION['_lang']}
            AND a.idtype LIKE '%{$__idtype_danhmuc}%'
            AND loai = {$product_type}
        {$s_sql2}"
        , $perpage, $url_page);

    /*Nếu chưa khai báo chế độ xem thì mặc định là grid view*/
    if (!isset($_SESSION['flow'])) {
        $_SESSION['flow'] = 1;
    }
    if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
        $flowlist = 1;
    } else {
        $flowlist = 0;
    }
    $flow = '';
    $flow .= '
    <div class="type-view clearfix">
        <ul>
            <li>';
    if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
        $flow .= '<a href="'.ROOT_PATH.'ajax/?method=flow&value=1" title="">';
    }
    $flow .= '<button type="button" class="btn btn-default" '.($flowlist==0?'disabled':null).'>
                            <i class="fa fa-th fa-lg" aria-hidden="true"></i>
                        </button>';
    if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
        $flow .= '</a>';
    }
    $flow .= '
            </li>
            <li>';
    if (isset($_SESSION['flow']) && $_SESSION['flow'] != 2) {
        $flow .= '<a href="'.ROOT_PATH.'ajax/?method=flow&value=2" title="">';
    }
    $flow .= '<button type="button" class="btn btn-default" '.($flowlist==1?'disabled':null).'>
                              <i class="fa fa-th-list fa-lg" aria-hidden="true"></i>
                          </button>';
    if (isset($_SESSION['flow']) && $_SESSION['flow'] != 2) {
        $flow .= '</a>';
    }
    $flow .= '
            </li>
        </ul>
    </div>';

    $data_sort = null;
    if( count($_arr_sort) > 0) {
        $data_sort.= '
        <div class="sort-container-h">
            <div class="sort-title" >';
                if( $_SESSION['sort'] != '' && isset($_SESSION['sort']) ) {
                    if( $_SESSION['sort'] == 2 ) {
                        $data_sort.= '
                        <i class="fa fa-sort-amount-asc"></i>';
                    }
                    else {
                        $data_sort.= '
                        <i class="fa fa-sort-amount-desc"></i>';
                    }
                    $data_sort.= $_arr_sort[$_SESSION['sort']];
                }
                else {
                    $data_sort.= $arraybien['sapxep'];
                }
                $data_sort.='
            </div>
            <ul class="sort-main">';
                // if( isset($_SESSION['sort']) ) {
                //     $data_sort.='
                //     <li><a href="'.ROOT_PATH.'modules/_files/changelang.php?sort=">
                //         <i class="fa fa-sort-asc"></i>
                //         '.$arraybien['sapxep'].'
                //     </a></li>';
                // }
                foreach( $_arr_sort as $ksort =>  $sort ) {
                    $data_sort.= '
                    <li><a href="'.ROOT_PATH.'modules/_files/changelang.php?sort='.$ksort.'">
                        '.$sort.'
                    </a></li>';
                }
                $data_sort.='
            </ul>
        </div>
        <div class="clear"></div>';
    }

    // Sort DROP-DOWN
    // if( count($_arr_sort) > 0) {
    //     $data_sort.= '
    //     <div class="sort-container">
    //         <div class="dropdown">
    //         <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
    //             if( $_SESSION['sort'] != '' && isset($_SESSION['sort']) ) {
    //                 if( $_SESSION['sort'] == 0 || $_SESSION['sort'] == 2 ) {
    //                     $data_sort.= '
    //                     <i class="fa fa-sort-amount-asc"></i>';
    //                 }
    //                 else {
    //                     $data_sort.= '
    //                     <i class="fa fa-sort-amount-desc"></i>';
    //                 }
    //                 $data_sort.= $_arr_sort[$_SESSION['sort']];
    //             }
    //             else {
    //                 $data_sort.= $arraybien['sapxep'];
    //             }
    //             $data_sort.='
    //             <span class="caret"></span>
    //         </button>
    //         <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
    //             if( isset($_SESSION['sort']) ) {
    //                 $data_sort.='
    //                 <li><a href="'.ROOT_PATH.'modules/_files/changelang.php?sort=">
    //                     <i class="fa fa-sort-asc"></i>
    //                     '.$arraybien['sapxep'].'
    //                 </a></li>';
    //             }
    //             foreach( $_arr_sort as $ksort =>  $sort ) {
    //                 $data_sort.= '
    //                 <li><a href="'.ROOT_PATH.'modules/_files/changelang.php?sort='.$ksort.'">
    //                     <i class="fa fa-angle-double-right"></i>
    //                     '.$sort.'
    //                 </a></li>';
    //             }
    //             $data_sort.='
    //         </ul>
    //         </div>
    //     </div><!-- /.sort-container -->
    //     <div class="clear"></div><!-- /.clear -->';
    // }

    $arrdata['datasort'] = $data_sort;

    // do du lieu vao tpl
    $smarty->assign("arrdata", $arrdata);
    $smarty->assign("phantrang", $phantrang);
    $smarty->assign("flow", $flow);
}
