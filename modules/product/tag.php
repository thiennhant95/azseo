<?php
// phan trang
$page       = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page       = ($page == 0 ? 1 : $page);
$perpage    = $_getArrayconfig['soluongsanpham']; //limit in each page
$startpoint = ($page * $perpage) - $perpage;
$s_noidung  = "
SELECT a.id,a.hinh,masp,gia,giagoc,
        a.idtype,a.ngaycapnhat, ten,
        url,link,target,mota
FROM tbl_noidung AS a
INNER JOIN tbl_noidung_lang AS b
    ON a.id = b.idnoidung
WHERE a.anhien = 1 ";

if ( ws_get('tag') ) {
    $s_noidung .= "
    AND b.tag LIKE '%" . ws_get('tag') . "%'
    AND b.tag != '' ";
}
$s_noidung .= "
           AND b.idlang = {$_SESSION['_lang']}
           AND loai = 1
           ORDER by thutu Asc
           LIMIT $startpoint,$perpage ";

$sql_page = "SELECT COUNT(*) as num
         from  tbl_noidung As a
         INNER JOIN tbl_noidung_lang AS b
         ON a.id = b.idnoidung
         where a.anhien = 1 ";

if ( ws_get('tag') ) {
    $sql_page .= "
    AND b.tag LIKE '%" . ws_get('tag') . "%'
    AND b.tag != '' ";
}

$sql_page .= "
and b.idlang = {$_SESSION['_lang']}
and loai = 1 ";

$d_noidung = $db->rawQuery($s_noidung);

$arrdata = array();
if (count($d_noidung) > 0) {
  $arrdata['sanpham'] = $d_noidung;
}
$url_page = ROOT_PATH . ws_get('urloption') . '.tags-trang-';
$phantrang .= Pages($sql_page, $perpage, $url_page);
/*Nếu chưa khai báo chế độ xem thì mặc định là grid view*/
if (!isset($_SESSION['flow'])) {
    $_SESSION['flow'] = 1;
}
if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
    $flowlist = 1;
} else {
    $flowlist = 0;
}
$flow = '';
$flow .= '
<div class="type-view clearfix">
    <ul>
        <li>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
                $flow .= '<a href="'.ROOT_PATH.'ajax/?method=flow&value=1" title="">';
            }
            $flow .= '<button type="button" class="btn btn-default" '.($flowlist==0?'disabled':null).'>
                        <i class="fa fa-th fa-lg" aria-hidden="true"></i>
                    </button>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] == 2) {
                $flow .= '</a>';
            }
            $flow .= '
        </li>
        <li>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] != 2) {
                $flow .= '<a href="'.ROOT_PATH.'ajax/?method=flow&value=2" title="">';
            }
            $flow .= '<button type="button" class="btn btn-default" '.($flowlist==1?'disabled':null).'>
                          <i class="fa fa-th-list fa-lg" aria-hidden="true"></i>
                      </button>';
            if (isset($_SESSION['flow']) && $_SESSION['flow'] != 2) {
                $flow .= '</a>';
            }$flow .= '
        </li>
    </ul>
</div>';
// do du lieu vao tpl
$braucube = $db->createListLink($__idtype_danhmuc,ROOT_PATH);
$smarty->assign("braucube",$braucube);
$smarty->assign("arrdata",$arrdata);
$smarty->assign("phantrang",$phantrang);
$smarty->assign("flow", $flow);
