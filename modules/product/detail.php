<?php

$d_noidung = $db->layNoiDung([
    "op"     => $_op,
    "select" => "tag, solanxem, timesale, sumsale, giasale",
    "where"  => "AND url = '{$_geturl}'
                AND anhien = 1
                AND idlang = {$_SESSION['_lang']}"
]);

$data_noidung_chitiet = '';
if (count($d_noidung) > 0) {

    $d_noidung   = $d_noidung[0];
    $ten         = $d_noidung['ten'];
    $masp        = $d_noidung['masp'];
    $thanhphan   = $d_noidung['thanhphan'];
    $id          = $d_noidung['id'];
    $idsp        = $d_noidung['id'];
    $idtype      = $d_noidung['idtype'];
    $hinh        = $d_noidung['hinh'];
    $solanxem    = $d_noidung['solanxem'];
    $url         = $d_noidung['url'];
    $link        = $d_noidung['link'];
    $target      = $d_noidung['target'];
    $mota        = $d_noidung['mota'];
    $noidung     = $d_noidung['noidung'];
    $tag         = $d_noidung['tag'];
    $gia         = $d_noidung['gia'];
    $giagoc      = $d_noidung['giagoc'];
    $giasale     = $d_noidung['giasale'];
    $timesale    = $d_noidung['timesale'];
    $sumsale     = $d_noidung['sumsale'];
    $ngaycapnhat = $d_noidung['ngaycapnhat'];
    $rating      = $d_noidung['rating'];
    $ratingCount = $d_noidung['ratingCount'];
    $ngayhientai = $db->getDateTimes();
    $lienketsp = ROOT_PATH.$url;

    if (empty($rating)){
      $rating = 5;
    }
    if (empty($ratingCount)){
      $ratingCount = 1;
    }
    $ratingValue = number_format(($ratingCount/$rating),1);

    // update so lan xem
    if ($_SESSION['countnoidung'][$id] == '') {

        $db->where("id", $id);
        $d_update_solanxem = $db->update(
            "tbl_noidung",
            [
                "solanxem" => $db->inc(1)
            ]
        );

        $_SESSION['countnoidung'][$id] = $id;
    }

    // tinh giam gia
    $phantram = null;
    if( $db->isSale($id) ) {
        $gia    = $giasale;
        $giagoc = $d_noidung['gia'];
    }

    if ( $gia && $giagoc && is_numeric($gia) && is_numeric($giagoc) && $gia < $giagoc ) {
        $phantram = '
        <div class="phantramgiamgia">
            -'.floor((($giagoc - $gia) / $giagoc) * 100).'%'.'
        </div>';
    }


    $arraylabel = array(
        'ngay'      => $arraybien['ngay'],
        'ngaytruoc' => $arraybien['ngaytruoc'],
        'gio'       => $arraybien['gio'],
        'phut'      => $arraybien['phut'],
        'luc'       => $arraybien['luc'],
        'cachday'   => $arraybien['cachday']
    );

    $data_noidung_chitiet .= '
    <div class="noidungchitiet">
        <div class="sanpham_group clearfix form-group">
             <div class="sanpham_img">';

    $db->where("idnoidung = $id");
    $db->orderBy("hinhdaidien", "Desc");
    $d_hinh = $db->get(
        "tbl_noidung_hinh",
        null,
        ["hinh"]
    );

    $data_hinhslide = '';
    $data_hinhchitiet = '
    <div class="full_thumblist">
        <div id="gal1" class="thumblist">';

        if (count($d_hinh) > 0) {

            foreach ($d_hinh as $key_hinh => $info_hinh) {

                $hinh = ROOT_PATH.'uploads/noidung/'.$info_hinh['hinh'];
                $hinh_thumb = ROOT_PATH.'uploads/noidung/thumb/'.$info_hinh['hinh'];

                if ( $key_hinh == 0 ) {
                    $data_hinhchitiet1 = '
                    <a href="#" data-image="'.$hinh.'" data-zoom-image="'.$hinh.'" >
                        <img id="img_01" src="' .$hinh.'" alt="'.$ten.'" title="'.$ten.'" />
                    </a>';
                }

                $data_hinhchitiet .= '
                    <a href="javascript:" data-image="'.$hinh.'" data-zoom-image="'.$hinh.'" title="'.$ten.'">
                        <img src="' .$hinh_thumb.'" alt="'.$ten.'" title="'.$ten.'" />
                    </a>';
                $data_hinhslide .= '
                <div class="item">
                    <img src="'.$hinh.'" alt="'.$ten.'" title="'.$ten.'" />
                </div>';
            }
        } $data_hinhchitiet .= '
        </div>
        <div class="clear"></div>
    </div>';

    // xua hinh chi tiet zoom
    if (class_exists('Mobile_detect') && !$didong->isMobile()) {
        $data_noidung_chitiet .= '
             <div class="noidung_img1">
                ' .$data_hinhchitiet1.'
                ' .$data_hinhchitiet. '
             </div>';
    } else {
        // neu chay slide
        if (count($d_hinh) > 1) {
            $run = "owl-carousel owl-theme";
        } else {
            $run = "";
        }
        $data_noidung_chitiet .= '
         <div class="noidung_img2">
            <div id="slide-chitiet">
                 <div class="owl-slide-chitiet slide '.$run.'">
                  ' .$data_hinhslide.'
                 </div>
            </div>
         </div>';
    }

    $data_noidung_chitiet .= '
      </div>';

    // thong tin chi tiet san pham
    $data_noidung_chitiet .= '
    <div class="sanpham_info">
        <h1 class="title">'.$ten.'</h1>';

    // NGAY
    if ( $_array_config_sanpham['ngay'] ) {
        $data_noidung_chitiet .= '<div class="ngaycapnhat">'.$db->ngaydang($ngayhientai, $ngaycapnhat, $arraylabel).'</div>';
        $data_noidung_chitiet .= '<div class="clear"></div>';
    }



    // RATE
    if( !1 ){
        $data_noidung_chitiet .= '
        <div class="row_chitiet">
            <div class="ct_label">' .$arraybien['danhgiasanpham'].':</div>
            <div class="row_noidung"><div class="jratingbig jratingcategories" data-average="'.$ratingValue.'" data-id="'.$id.'"></div></div>
            <div class="clear"></div>
        </div>';
    }

    // GIA SP
    if ( $gia ) {
        if ( is_numeric($gia) ) {
            $gia = number_format($gia).' đ';
        }

        if ( is_numeric($giagoc) && $giagoc ) {
            $giagoc = '
            <div class="giagoc">'.number_format($giagoc).' đ</div>';
        }
        if ($giagoc != '') {
            $data_noidung_chitiet .= '
            <div class="row_chitiet">
                <div class="ct_label giagocL">' .$arraybien['giagoc'].':</div>
                <div class="ct_noidung">' .$giagoc.' </div>
                <div class="clear"></div>
            </div>';
        }
        $data_noidung_chitiet .= '
            <div class="row_chitiet">
                <div class="ct_noidung"><div class="gia">' .$gia.'</div>  '.$phantram.'</div>
                <div class="clear"></div>
            </div>
            <div class="luuygia">'.$db->getThongTin('luuygia').'</div>';
    }


    //MSP
    if( $masp ){
        $data_noidung_chitiet .= '
        <div class="row_chitiet">
            <div class="ct_label">' .$arraybien['masp'].':</div>
            <div class="row_noidung">' .$masp.'</div>
            <div class="clear"></div>
        </div>';
    }

    //ThanhPhan
    if( $thanhphan ){
        $data_noidung_chitiet .= '
        <div class="row_chitiet">
            <div class="ct_label">' .$arraybien['thuonghieu'].':</div>
            <div class="row_noidung">' .$thanhphan.'</div>
            <div class="clear"></div>
        </div>';
    }

    // LUOT XEM
    if( $solanxem ){
        $data_noidung_chitiet .= '
        <div class="row_chitiet">
            <div class="ct_label">' .$arraybien['solanxem'].':</div>
            <div class="row_noidung">' .$solanxem.'</div>
            <div class="clear"></div>
        </div>';
    }

    // SHARE
    if( $_array_config_sanpham['share'] == 1 || 1 ){
        $data_noidung_chitiet.='
        <div class="row_chitiet" style="float:left; display:table;">
            <div class="ct_label">' .$arraybien['chiase'].':</div>
            <div class="ct_noidung">';
        $data_noidung_chitiet .= include 'plugins/share/share.php';$data_noidung_chitiet.='
        </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>';
    }

    // MOTA
    // if ( $mota ) {
    //     $data_noidung_chitiet .= '
    //     <div class="row_chitiet mota">
    //         <div class="row_noidung">' .$mota.'</div>
    //         <div class="clear"></div>
    //     </div>';
    // }


    if($count_checkgiohang==0){
        $data_script .= ' if(false){}';
    }

    // YEU CAU GOI LAI
    // if( 1 ){
    //     $link_goilai = ROOT_PATH.'modules/product/goilai.php';
    //     $data_noidung_chitiet .= '
    //     <div class="row">
    //         <div class="col-sm-9">
    //             <form id="yeucaugoilai" name="yeucaugoilai" class="clearfix">
    //                 <div class="group_btn_dathang yeucaugoilai" id="contentycgoilai">
    //                 <div class="ycgoilai" id="ycgoilai">'.$arraybien['yeucaugoilai'].'</div>
    //                     <div class="form-group">
    //                         <div class="input-group input-group-sm margin-au">
    //                             <div class="input-group-addon">
    //                                 '.$arraybien['sodienthoai'].'
    //                             </div>
    //                             <input class="form-control" name="txtgoilai" id="txtgoilai" value="" placeholder="'.$arraybien['nhapsodienthoai'].'" type="number" minlength="8" maxlength="11" size="10" />
    //                             <div class="input-group-btn">
    //                                 <button type="button" class="btn btn-info btn-dathang">
    //                                     <a title="'.$arraybien['gui'].'" href="javascript:;"  onclick="goilai(\''.$link_goilai.'\',\'contentycgoilai\',\''.$id.'\',\''.$lienketsp.'\',\''.$arraybien['sodienthoaikhongdung'].'\',\''.$arraybien['guithongtinthanhcong'].'\'); return false;"><i class="fa fa-send"></i> '.$arraybien['gui'].'
    //                                     </a>
    //                                 </button>
    //                                 <input type="hidden" name="idsp_hi"  id="idsp_hi" value="'.$id.'" />
    //                             </div>
    //                         </div>
    //                     </div>
    //                 </div>
    //             </form>
    //         </div><!-- /.col-sm-7 -->
    //     </div><!-- /.row -->';
    // }

    // NÚT ĐẶT HÀNG
    if( !1 ){
        $data_noidung_chitiet.= '
        <div class="cart-btn clearfix buynow form-group"> '.
            $db->getThongTin("nutdathang")
        .'</div>';
    }

    // NÚT GỌI LẠI
    // if( !1 ){
    //     $call_detail_phone = strip_tags(
    //         $db->getThongTin("nutgoilai")
    //     );
    //     $data_noidung_chitiet.= '
    //     <div class="cart-btn clearfix detailcall form-group">
    //         <span class="icon">
    //             <i class="fa fa-phone fa-lg"></i>
    //         </span>
    //         <a href="tel:'.$call_detail_phone.'" title="call now">
    //             <span class="call-label">
    //                 <p>'.$arraybien['goingay'].'&nbsp;<small>('.$arraybien['nhandegoi'].')</small></p>
    //             </span>
    //             <span class="call-phone hide">
    //                 '.$call_detail_phone.'
    //             </phone>
    //         </a>
    //     </div>';
    // }



    $link_giohang = ROOT_PATH.'ajax/?op=giohang&id='.$id.'&method=add&sonhom='.$count_checkgiohang.'&sl=';
      // tao scrip kiem tra
    $data_noidung_chitiet .= '<script>
        function check_muahang(){
          '.$data_script.'
          else{
            MuaHang(\'' .$link_giohang.'\'+document.getElementById(\'slspct\').value'.$script_additem.',\'loaddatacart\');
            $(\'#myModal\').modal(\'show\');
            return true;
          }
        }
    </script>';

    if ( $_getArrayconfig['nutdathang'] == 'on' ) {

        /**
         * Tinh so luong da mua
         */
        $soluongdamua = 1;
        if ( isset($_SESSION['giohang']) && count($_SESSION['giohang']) > 0 ) {
            foreach ( $_SESSION['giohang'] as $iGH ) {
                if ( $iGH['idsp'] == $idsp ) {
                    $soluongdamua = $iGH['soluong'];
                }
            }
        }
        // $data_noidung_chitiet .= '
        //     <div class="form-group btn-order">

        //         <div class="row">

        //             <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        //                 <div class="group_btn_dathang input-group input-group-sm">
        //                     <span class="soluongsp input-group-addon">'.$arraybien['soluong'].'</span>
        //                     <input class="form-control" name="slspct" id="slspct" value="'.$soluongdamua.'" type="number" min="1" maxlength="5" size="10" />
        //                     <div class="input-group-btn">

        //                         <button type="button" class="btn btn-danger btn-dathang" onclick="check_muahang(); return false;" disabled>
        //                             <i class="fa fa-cart-plus fa-lg"></i> ' .$arraybien['datmua'].'
        //                         </button>

        //                     </div>
        //                 </div>
        //             </div>

        //         </div><!-- /.row -->

        //     </div>';
    }

    $data_noidung_chitiet .= '
        <div class="sp-info-bot">';











        // lay cac option danh muc

    $db->join(
        "tbl_option_type_lang b",
        "a.id = b.idtype",
        "inner"
    );
    $db->join(
        "tbl_option_value c",
        "a.id = c.idoptiontype",
        "inner"
    );
    $db->where("
        c.idnoidung = '{$id}'
        AND b.idlang = {$_SESSION['_lang']}
        AND c.idlang = {$_SESSION['_lang']}
        AND a.anhien = 1"
    );
    $db->orderBy("thutu", "asc");
    $d_option_value = $db->get(
        "tbl_option_type a", null, [
            "a.id","a.datatype","b.ten",
            "c.noidung","a.giohang"
        ]
    );

    $data_script    = '';
    $script_additem = '';

    // PHAN NHOM
    if ( count($d_option_value) > 0 ) {
        $data_noidung_chitiet.='
        <div class="option-group clearfix">';
        $count_checkgiohang = 0;
        foreach ($d_option_value as $key_option_value => $info_option_value) {
            $option_value_datatype = $info_option_value['datatype'];
            $option_value_id       = $info_option_value['id'];
            $option_value_ten      = $info_option_value['ten'];
            $option_value_noidung  = trim($info_option_value['noidung']);
            $option_value_giohang  = trim($info_option_value['giohang']);
            // lay id mang giohang

            // tao scrip check mua hang kiem tra tung nhom du lieu
            if ($option_value_giohang == 1) {
                ++$count_checkgiohang;
                if ($count_checkgiohang > 1) {
                    $data_script .= ' else ';
                }
                $data_script .= '
                if(document.getElementById(\'nhom'.$option_value_id.'\').value==""){
                    swal("'.$arraybien['vuilongchonmotgiatri'].' '.$option_value_ten.'","","warning");
                    return false;
                }';
                $script_additem .= '+\'&nhom'.$count_checkgiohang.'=\'+document.getElementById(\'nhom'.$option_value_id.'\').value+\'&idnhom'.$count_checkgiohang.'=\'+document.getElementById(\'idnhom'.$option_value_id.'\').value';
                $data_noidung_chitiet .= '<input type="hidden" name="nhom'.$option_value_id.'" id="nhom'.$option_value_id.'" value="'.$_SESSION['giohang'][$id]['nhom'][$option_value_id]['value'].'" />
              <input type="hidden" name="idnhom'.$option_value_id.'" id="idnhom'.$option_value_id.'" value="'.$option_value_id.'" />';
            } else {
                // $data_script .= ' if(0==1){ ';
            }
            // xua du lieu dang text
            if ($option_value_noidung != '') {
                if ($option_value_datatype == 0) {
                    $data_noidung_chitiet .= '
                    <div class="row_chitiet">
                        <div class="ct_label1">' .$option_value_ten.':</div>
                        <div class="row_noidung">' .$option_value_noidung.'</div>
                        <div class="clear"></div>
                    </div>';
                } elseif ($option_value_datatype == 1) {

                    $db->where("
                        idtype = {$option_value_noidung}
                        AND idlang = {$_SESSION['_lang']}
                    ");
                    $d_option = $db->get("tbl_option_lang", null, [
                        "ten", "url", "link", "target"
                    ]);

                    if (count($d_option) > 0) {
                        $d_option      = $d_option[0];
                        $option_url    = $d_option['url'];
                        $option_link   = $d_option['link'];
                        $option_target = $d_option['target'];
                        $option_ten    = $d_option['ten'];
                        $lienket       = ROOT_PATH.$option_url.'.option';
                        if ($link != '') {
                            $lienket = $link;
                            $target_value = ' target="'.$target.'" ';
                        }
                        $data_noidung_chitiet .= '
                        <div class="row_chitiet">
                            <div class="ct_label1">' .$option_value_ten.':</div>
                            <div class="row_noidung clearfix">
                                <a href="' .$lienket.'" title="'.$option_ten.'">
                                    '.$option_ten.'
                                </a>
                            </div>
                        </div>';
                    }
                } elseif ($option_value_datatype == 2) {
                    $arr_value = explode(',', $option_value_noidung);
                    $soluongitem = count($arr_value);
                    if ($soluongitem > 0) {
                        $data_noidung_chitiet .= '
                        <div class="row_chitiet">
                            <div class="ct_label1">' .$option_value_ten.':</div>
                            <div class="row_noidung clearfix">
                                <ul class="item_option">';
                                for ($i = 0; $i < $soluongitem; ++$i) {

                                    $value_option_i = $arr_value[$i];
                                    if ($value_option_i != '') {

                                        $db->where("
                                            idtype = {$value_option_i}
                                            AND idlang = {$_SESSION['_lang']}
                                        ");
                                        $d_option = $db->get("tbl_option_lang", null, [
                                            "ten", "url", "link",
                                            "target", "id", "idtype"
                                        ]);

                                        if (count($d_option) > 0) {
                                            $d_option      = $d_option[0];
                                            $option_url    = $d_option['url'];
                                            $option_idtype = $d_option['idtype'];
                                            $option_link   = $d_option['link'];
                                            $option_target = $d_option['target'];
                                            $option_ten    = $d_option['ten'];
                                            $lienket = ROOT_PATH.$option_url.'.option';
                                            if ($link != '') {
                                                $lienket = $link;
                                                $target_value = ' target="'.$target.'" ';
                                            }
                                            if ($option_value_giohang == 1) {
                                                $classactive = '';
                                                if ($_SESSION['giohang'][$id]['nhom'][$option_value_id]['value'] == $option_idtype) {
                                                    $classactive = 'class = "active" ';
                                                }
                                                $data_noidung_chitiet .= '
                                                <li '.$classactive.' onclick="document.getElementById(\'nhom'.$option_value_id.'\').value=\''.$option_idtype.'\';">
                                                    '.$option_ten.'
                                                </li>';
                                            } else {
                                                $data_noidung_chitiet .= '
                                                <li>
                                                    <a href="'.$lienket.'" title="'.$option_ten.'"> '.$option_ten.'</a></li>';
                                            }
                                        }
                                    } // end count i
                                }$data_noidung_chitiet .= '
                                </ul>
                                <!-- .item_option -->
                            </div>
                            <!-- .row_noidung -->
                        </div>
                        <!-- row_chitiet -->';
                    }
                }
            }// end if noi dung==''
        }
        $data_noidung_chitiet.='
        </div><!-- /.option-group -->';
    }












    //so luong
    $data_noidung_chitiet .= '<div class="row_chitiet">
                                <div class="ct_label">'.$arraybien['soluong'].'</div>
                                <div class="row_noidung">
                                    <input class="form-control" name="slspct" id="slspct" value="'.$soluongdamua.'" type="number" min="1" maxlength="5" size="10" />
                                </div>
                                <div class="clear"></div>
                            </div>';

    //button
    $data_noidung_chitiet .= '<div class="btn-container">
        <a href="javascript:;"><button type="button"><i class="fa fa-commenting-o" aria-hidden="true"></i><span>Chat Ngay</span></button></a>
        <a href="javascript:;" onclick="MuaHang(\''.$link_giohang.'\');"><button type="button"><i class="fa fa-cart-plus" aria-hidden="true"></i></i><span>Thêm vào giỏ hàng</span></button></a>
        <a href=""><button type="button" class="btn btn-danger btn-dathang" onclick="check_muahang(); return false;" disabled><span>Mua Ngay</span></button></a>
        <a href="tel:'.strip_tags($db->getThongTin('hotline')).'"><button type="button"><span>Gọi Lại</span></button></a>
    </div>';





    $data_noidung_chitiet.='</div></div></div>'; // end san pham group
    $data_noidung_chitiet .= '<div class="clear"></div>';

    // CHI TIẾT SẢN PHẨM
    $data_noidung_chitiet .= '
    <div role="tabpanel" class="tab-sanpham">

        <div class="info-right">
            <div class="chinhsach">
                <div class="cs-tittle"><img src="'.ROOT_PATH.'templates/images/icon-bottom.png"><span>Chính sách</span></div>
                <div class="cs-main">
                    '.$db->getThongTin("chinhsach").'
                </div>
            </div>

            <div class="chinhsach">
                <div class="cs-tittle"><img src="'.ROOT_PATH.'templates/images/icon-bottom.png"><span>Chúng tôi cam kết</span></div>
                <div class="cs-main">
                    '.$db->getThongTin("camket").'
                </div>
            </div>
        </div>


        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#chitiet" aria-controls="chitiet" role="tab" data-toggle="tab" class="tab-title">'.$arraybien['thongtinsanpham'].'</a>
            </li>';

            if ($_getArrayconfig['binhluanwebsite'] == 'on') {
                $s_countbinhluan = "
                    SELECT id
                    FROM tbl_comment
                    WHERE idtype = '{$idsp}'
                    AND anhien = 1
                    ORDER BY thutu ASC";
                $d_countbinhluan = $db->rawQuery($s_countbinhluan);
                $data_count_comment = '';
                if(count($d_countbinhluan)>0){
                    $data_count_comment = '<span class="countcomment">( '.count($d_countbinhluan).' )</span>';
                }
                $data_noidung_chitiet.='
                <li role="presentation">
                    <a href="#comment" aria-controls="comment" role="tab" data-toggle="tab" class="tab-title">'.$arraybien['nhanxetcuakhachhang'].''.$data_count_comment.'</a>
                </li>';
            }

            // if ( $_array_config_sanpham['share'] || 1) {
            //     $data_noidung_chitiet.='
            //     <div class="clearfix share">
            //         <div class="pull-right">';
            //         $data_noidung_chitiet .= include 'plugins/share/share.php';
            //     $data_noidung_chitiet.='
            //         </div><!-- /.pull-right -->
            //     </div><!-- /.clearfix share -->';
            // }
            $data_noidung_chitiet .= '
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane panel-body active" id="chitiet">
                '.$noidung.'';
            $luuysanpham = $db->getThongTin("luuysanpham");
            $data_noidung_chitiet.='<div class="luuysanpham">'.$luuysanpham.'</div>';
        $data_noidung_chitiet.='
            </div>';

            if ( $_getArrayconfig['binhluanwebsite'] == 'on' ) {
                $data_noidung_chitiet.='<div role="tabpanel" class="tab-pane panel-body" id="comment">';
                // if ($_array_config_sanpham['commentform']) {
                    // nhum comment
                    $data_noidung_chitiet.='
                    <div class="ws-container">';
                    $data_noidung_chitiet.= include'modules/comment/index.php';
                    $data_noidung_chitiet.= ' </div>
                        <!-- .ws-container -->';
                // }else{
                //     $data_noidung_chitiet.='
                //     <div role="tabpanel" class="tab-pane panel-body" id="comment">

                //         <script>(function(d, s, id) {
                //             var js, fjs = d.getElementsByTagName(s)[0];
                //             if (d.getElementById(id)) return;
                //             js = d.createElement(s); js.id = id;
                //             js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=401401003393108";
                //             fjs.parentNode.insertBefore(js, fjs);
                //         }(document, \'script\', \'facebook-jssdk\'));</script>
                //             <div class="fb-comments" data-href="http://' .$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'].'"
                //         data-width="100%" data-numposts="5" data-colorscheme="light"></div>

                // </div>';
                // }
                $data_noidung_chitiet.='</div>';
            }
        $data_noidung_chitiet.='

        </div>

    <div class="info-left">'.$db->getThongTin("thongtincuoiwebsite").'</div>


    <!-- close TAB -->
    </div>';

    // tag
    $tagarr     = '';
    $tagcontent = '';
    $tag        = trim($tag);
    $tagcontent = '';
    $data_noidung_chitiet.= get_tag($tag, 'tags', '.tags');
    $data_noidung_chitiet .= $tagcontent;
    // $data_noidung_chitiet .= '<div class="clear"></div>';




    $chitietsanpham = $data_noidung_chitiet;
    $sanphamlienquan = '';
    $sanphamlienquan_title = '';
    // lay tin lien quan
    if ( $_array_config_sanpham['tinlienquan'] ) {

        $d_noidung_lienquan = $db->layNoiDung([
            "op" => $_op,
            "select" => "timesale, sumsale, giasale",
            "where" => "AND a.idtype like '%{$__idtype_danhmuc}%'
                        AND anhien = 1
                        AND idlang = {$_SESSION['_lang']}
                        AND url != '{$url}'",
            "limit" => [0, $_array_config_sanpham['sotinlienquan']]
        ]);

        $arrdata['sanpham'] = $d_noidung_lienquan;
        if (count($d_noidung_lienquan) > 0) {
            $sanphamlienquan_title .= '
            <div class="titlebase form-group clearfix">
            <span class="img"><i class="fa fa-retweet"></i></span>
            <div class="subtitle">
            <a href="javascript:;" rel="nofollow" title="'.$arraybien['sanphamcungchuyenmuc'].'">
            '.$arraybien['sanphamcungchuyenmuc'].'
            </a>
            </div>
            </div>
            ';

            // do du lieu vao tpl
            $smarty->assign('sanphamlienquan_title', $sanphamlienquan_title);
            $smarty->assign('sanphamlienquan', $arrdata);
        }
    } // end div noi dung chi tiet
    // $data_noidung_chitiet .= '<div class="clear"></div>';
}
$chitietsanpham = $data_noidung_chitiet;

// if ($_array_config_sanpham['commentform'] == 1) {
//     // nhum comment
//     $chitietsanpham.= include'modules/comment/index.php';
// }

// do du lieu vao tpl
$smarty->assign('chitietsanpham', $chitietsanpham, true);
