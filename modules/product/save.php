<?php

    $ret = null;

    $arsave = json_decode($_COOKIE['productsave']);

    if( count($arsave) > 0 ) {
        foreach( $arsave as $ksave => $isave ) {

            $db->join("tbl_noidung_lang b", "a.id = b.idnoidung", "inner");
            $db->where("idnoidung", $isave);
            $db->where("idlang", $_SESSION['_lang']);
            $arsp = $db->get("tbl_noidung a",null, ["a.id", "hinh", "gia", "giagoc", "ten", "url"]);

            $smarty->assign("arrsanpham", $arsp);
            $smarty->assign("root_path", ROOT_PATH);

            $ret.= $smarty->fetch(ROOT_DIR."/templates/layout1/modules/product/tpl/item.tpl");
        }
    }

    $smarty->assign("arrsave", $ret);
    $smarty->assign("ten", $arraybien['sanphamdaluu']);