<?php
    require dirname(dirname(__DIR__)) . "/configs/inc.php";

if ( ws_post('method') == 'goilai' ) {

    $idnoidung     	    = ws_post('id');
    $sodienthoai 	    = ws_post('phone');
    $link 			    = ws_post('lienket');
    $ngay 			    = $db->getDateTimes();

    $id_insert = $db->insert("tbl_goilai", [
        "idnoidung"		=> $idnoidung,
        "sodienthoai"    => $sodienthoai,
        "ngay"           => $ngay,
        "anhien"         => 0
    ]);

    if( $id_insert ) {
        if ($_getArrayconfig['sendemail'] == 'on') {
            $subject         = 'Y/c gọi lại ' . $_SERVER['HTTP_HOST'];
            $message         = $db->getThongTin('thongtindonhang');
            if( !$message ) {
                vard('không tìm thấy thông tin với khóa [thongtindonhang]');
            }
            $masp  = $db->getNameFromID("tbl_noidung","masp","id",$idnoidung);
            $message = 'Thông tin khách hàng yêu cầu gọi lại<br />Số điện thoại: '.$sodienthoai.'<br />Mã SP: '.$masp.'<br />Liên kết: '.$link.'.html <br /> Ngày gửi: '.$ngay;

            include ROOT_DIR."/plugins/mail/gmail/class.phpmailer.php";
            include ROOT_DIR."/plugins/mail/gmail/class.smtp.php";
            // hot mail

            $array_sendmail = array(
                "emailnhan" => $_getArrayconfig['email_nhan'],
                "emailgui"  => $_getArrayconfig['hostmail_user'],
                "hostmail"  => $_getArrayconfig['hostmail'],
                "user"      => $_getArrayconfig['hostmail_user'],
                "pass"      => $_getArrayconfig['hostmail_pass'],
                "tieude"    => $subject,
                "fullname"  => $_getArrayconfig['hostmail_fullname'],
                "port"      => 25,
                "ssl"       => 0,
                "subject"   => $subject,
                "message"   => $message,
            );
            sendmail($array_sendmail);
        }
    echo'OK';
	}
}
?>