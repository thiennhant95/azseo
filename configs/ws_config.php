<?php
/**
 * Tệp tin cấu hình chung cho toàn bộ website
 * Vị trí: /application/ws_config.php
 */
if (!defined('BASEPATH')) define('BASEPATH', dirname(__FILE__));

if ( ! defined('BASEPATH')) exit('403');

/** Tên database */
if (!defined('DB_NAME')) define('DB_NAME', '');

/** Tài khoản database */
if (!defined('DB_USER')) define('DB_USER', 'root');

/** Mật khẩu database */
if (!defined('DB_PASSWORD')) define('DB_PASSWORD', '');

/** Hostname */
if (!defined('DB_HOST')) define('DB_HOST', 'localhost');

/** Database Charset */
if (!defined('DB_CHARSET')) define('DB_CHARSET', 'utf8');

/** Tiền tố */
if (!defined('DB_PREFIX')) define('DB_PREFIX', 'ws_');

/** Thư mục chứa thư viện */
if (!defined('WS_INC')) define('WS_INC', 'application/config/');

/** Thư mục chứa giao diện */
if (!defined('WS_FRONTEND')) define('WS_FRONTEND', 'application/templates/');

/** Thư mục upload */
if (!defined('WS_UPLOAD')) define('WS_UPLOAD', 'uploads');

/** Thư mục chưa plugin */
if (!defined('WS_PLUGIN')) define('WS_PLUGIN', 'plugin');

/** Thư mục quản trị  */
if (!defined('WS_ADMIN')) define('WS_ADMIN', 'adminweb');

/** Cổng kết nối */
if (!defined('SERVER_PORT')) define('SERVER_PORT', '');

/** Địa chỉ website */
$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
if (!defined('SITE_URL')) define('SITE_URL', $protocol.$_SERVER['SERVER_NAME'].SERVER_PORT);

/** Đường dẫn thư mục */
if (!defined('FOLDER_PATH')) define('FOLDER_PATH', '/adangonngu/');

/** Thời gian cookie */
if (!defined('COOKIE_EXPIRES')) define('COOKIE_EXPIRES', 3600);

/** Báo lỗi */
if (!defined('WS_DEBUG')) define('WS_DEBUG', FALSE);

/** Time zone */
date_default_timezone_set('Asia/Ho_Chi_Minh');
