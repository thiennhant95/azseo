<?php
use Lazer\Classes\Database as Lazer;
/**
 * Lấy thời gian của múi giờ +7
 * ===
 * @param string $format Định dạng thời gian
 * @return void
 */
function get_time_asia($format = null)
{
    $date = new DateTime("now", new DateTimeZone('Asia/Ho_Chi_Minh'));
    $cur = $date->format('Y-m-d H:i:s');
    if ($format !== null) {
        $cur = $date->format($format);
    }

    return $cur;
}

/**
 * Kiểm tra thời gian trên server có = +7
 *
 * @param string $format Định dạng thời gian
 * @return void
 */
function check_time_server($format = null)
{
    // Ngày hiện tại +7
    $timeAsia = get_time_asia($format);
    $timeServer = get_time_server($format);

    return strtotime($timeServer) !== strtotime($timeAsia) ? false : true;
}

/**
 * Xóa File
 * ===
 * @param string $pathfile Đường dẫn tới file
 * @return void
 */
function remove_file($pathfile)
{
    if (empty($pathfile)) {
        return;
    }

    if (file_exists($pathfile)) {
        unlink($pathfile);
    }
}

/**
 * Kiểm tra trình duyệt đang sử dụng
 *
 * $name = Firefox | Chrome | Opera Mini | Opera | Safari
 * @param string $name Tên trình duyệt
 * @return boolean
 */
function is_browser($name = 'Chrome')
{
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false) {
        return true;
    } else {
        return false;
    }
}

/**
 * Đặt null cho các field ngày tháng
 *
 * Có giá trị mặc định: 0000-00-00 00:00:00
 *
 * @param  string $table Bảng cần reset
 * @param  string $field Cột cần reset
 * @return void
 */
function reset_date_null($table, $field)
{
    mysql_query("
        UPDATE {$table}
        SET {$field} = NULL
        WHERE {$field} = '0000-00-00 00:00:00'
    ");
}

/**
 * ## Loại bỏ dấu tiếng việt
 * @param  string $str Chuỗi cần loại bỏ
 * @return string      Chuỗi đã loại bỏ
 */
function vn_str_filter($str, $char = null)
{
    $unicode = array(
        'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
        'd'=>'đ',
        'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        'i'=>'í|ì|ỉ|ĩ|ị',
        'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
        'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'D'=>'Đ',
        'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
        'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
    );

    foreach ($unicode as $nonUnicode=>$uni) {
        $str = preg_replace("/($uni)/i", $nonUnicode, $str);
    }

    if ($char) {
        $str = str_replace(' ', $char, $str);
    }

    return $str;
}

/**
 * Lấy src của một tấm hình
 *
 * @param string $img Link hình
 * @return string
 */
function get_src_img($img)
{
    $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
    preg_match($pattern, $img, $link);
    $link = @$link[1];
    $link = urldecode($link);

    return $link;
}

/**
 * Chuyển đổi ngày giờ dd/mm/yyyy H:i:s => yyyy/mm/dd H:i:s
 *
 * @param datetime $daytime
 * @return void
 */
function convert_date_time_mysql($daytime)
{
    $dt = explode(' ', $daytime);
    $d = reset($dt);
    $d = convert_date_mysql($d);
    $t = end($dt);

    return $d . ' ' . $t;
}

/**
 * Hàm chuyển đổi dd/mm/yyyy => yyyy-mm-dd
 *
 * @param date $dayyear
 * @return void
 */
function convert_date_mysql($dayyear)
{
    $date = str_replace('/', '-', $dayyear);
    return date('Y-m-d', strtotime($date));
}

/**
 * Tạo thư mục
 *
 * @param string $pathdir : Dường dẫn
 * @param boolean $thumbdir : có tạo thumb ko
 * @return void
 */
function create_dir($pathdir, $thumbdir = true)
{
    if (!is_dir($pathdir)) {
        mkdir($pathdir, 0777, true);
    }

    if ($thumbdir) {
        if (! is_dir($pathdir . "/thumb/")) {
            mkdir($pathdir . "/thumb/", 0777, true);
        }
    }
}

/**
 * kết thúc code với string
 *
 * @param string $string Thông điệp khi exit
 * @return void
 */
function _exit($string)
{
    header('Content-Type: text/html; charset=utf-8');
    return exit("<h2 style='padding: 25px;font-size: 35px;'><center>{$string}</center></h2>");
}

/**
 * Xóa file trong thư mục và tất cả thư mục con
 *
 * @param string $dir Thư mục cần xóa
 * @param string $filename Tên file cần xoa
 * @return void
 */
function remove_all_file($dir, $filename)
{
    $ffs = scandir($dir);

    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);

    // prevent empty ordered elements
    if (count($ffs) < 1) {
        return;
    }

    foreach ($ffs as $ff) {
        if ($filename == $ff) {
            if (file_exists($dir . '/' . $ff)) {
                unlink($dir . '/' . $ff);
            }
        }
        if (is_dir($dir . '/' . $ff)) {
            remove_all_file($dir . '/' . $ff, $filename);
        }
    }
}

/**
 * Tạo mật khẩu
 *
 * @param string $pass mật khẩu cần tạo
 * @param string $lat lat cần tạo
 * @return string
 */
function create_pass($pass, $lat)
{
    return !empty($pass) && !empty($lat) ? md5(md5($pass).$lat) : null;
}

/**
 * Lấy ID của user hiện tại
 *
 * @return void
 */
function get_id_user()
{
    return is_login() ? $_SESSION['user_id'] : null;
}

/**
 * Hiển thị thông báo tới người dùng
 *
 * @param string $str
 * @param string $type
 * @return void
 */
function _alert($str, $type='danger')
{
    if (!!$str) {
        # code...
        return '
			<div class="alert alert-'.$type.'">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong class="h3">'.$str.'</strong>
			</div>
		';
    }
}

/**
 * Chuyển hướng về trang trước
 *
 * @return void
 */
function redirect_history()
{
    $previous = "javascript:history.go(-1)";
    if (isset($_SERVER['HTTP_REFERER'])) {
        $previous = $_SERVER['HTTP_REFERER'];
    }

    redirect_to($previous);
}

/**
 * Hiển thị thông điệp chưa đăng nhập
 *
 * @return void
 */
function not_login_template()
{
    return '
	<div class="panel panel-danger">
		<div class="panel-heading">
			<div class="panel-title">
				<strong>'.$_SESSION['arraybien']['canhbao'].'</strong>
			</div>
		</div>
		<div class="panel-body">
			<h4>'.$_SESSION['arraybien']['banchuadangnhap'].'</h4>
		</div>
	</div>';
}

/**
 * Hiện thị thông điệp đã đăng nhập
 *
 * @return boolean
 */
function is_login_template()
{
    return '
	<div class="panel panel-success">
		<div class="panel-heading">
			<div class="panel-title">
				<strong>'.$_SESSION['arraybien']['thongdiep'].'</strong>
			</div>
		</div>
		<div class="panel-body">
			<h4>'.$_SESSION['arraybien']['bandadangnhap'].'</h4>
		</div>
	</div>';
}

/**
 * Hiện thị lỗi
 *
 * @return void
 */
function show_error($enable = true)
{
    if ($enable) {
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
    } else {
        error_reporting(0);
    }
}

/**
 * Chuyển hướng
 *
 * @param string $url Đường dẫn cần tới
 * @return void
 */
function redirect_to($url, $delay = false)
{
    if ($delay) {
        echo "
		<script>
			setTimeout(\"location.href = '".$url."';\", ".$delay.");
		</script>";
    } else {
        echo "<script>location.href = '{$url}';</script>";
    }
}

/**
 * Đã đăng nhập vào hệ thống
 *
 * @return boolean
 */
function is_login()
{
    return isset($_SESSION['user_id']) ? true : false;
}

/**
 * Thêm cột mới vào Database
 *
 * @param string $table    Bảng cần thêm
 * @param string $column   Cột cần thêm
 * @param string $type     Loại của cột
 * @param string $colAfter Tên cột thêm sau
 */
function add_col_sql($table, $column, $type, $colAfter='id')
{
    $r = mysql_num_rows(mysql_query("SHOW columns FROM $table WHERE field='$column'"));
    if (!$r) {
        mysql_query("ALTER TABLE `$table`
					ADD COLUMN `$column` $type NULL AFTER `$colAfter`");
        exit("CAP NHAT CO SO DU LIEU THANH CONG! F5 DE TIEP TUC.");
    }
}

/**
 * var_dump
 *
 * @param array $arr Mảng cần xem
 * @param boolean $die dừng chạy code
 * @return void
 */
function vard($arr, $die=true)
{
    var_dump($arr);
    if ($die) {
        die();
    }
}

/**
 * Hàm GET
 *
 * @param string $parameter Giá trị cần GET
 * @param bool $default Giá trị mặc định
 * @return void
 */
function ws_get($parameter, $default = null)
{
    if (isset($_GET[$parameter]) && !empty($_GET[$parameter])) {
        return $_GET[$parameter];
    } else {
        return $default;
    }
}

/**
 * $_POST
 *
 * @param string $parameter Giá trị cần POST
 * @param bool $default Giá trị mặc định
 * @param boolean $nullString
 * @return void
 */
function ws_post($parameter, $default = null, $nullString = true)
{
    if (isset($_POST[$parameter])) {
        if ($_POST[$parameter] == '') {
            if ($nullString) {
                return $_POST[$parameter];
            } else {
                return $default;
            }
        } else {
            if (is_array($_POST[$parameter])) {
                return $_POST[$parameter];
            } else {
                return $_POST[$parameter];
            }
        }
    } else {
        return $default;
    }
}

/**
 * $arraybien[] Dành cho Smarty template
 *
 * @param string $params Biến truyền vào
 * @param class $smarty
 * @return void
 */
function smarty_modifier_arraybien($params, &$smarty)
{
    global $arraybien;
    return $arraybien[$params] ? $arraybien[$params] : $params;
}

/**
 * ## Loại Bỏ Ký tự Đặc Biệt
 *
 * @param string $str
 * @return void
 */
function remove_special_char($str)
{
    return preg_replace('/[^\w\s]+/u', '', $str);
}

/**
 * ## Tạo url
 *
 * - Không có ký tự đặc biệt
 *
 * - Phân cách bởi dấu gạch ngang
 *
 * - Chuyển về chữ in thường
 *
 * @param string $str
 * @return void
 */
function tao_url2($str)
{
    $st = remove_special_char($str);
    $st = vn_str_filter($st, '-');
    $st = str_replace('_', '', $st);
    $st = trim($st, '-');
    return strtolower($st);
}

/**
 * ### Tách hình ảnh ra hỏi nội dung
 *
 * Trả về: mảng hình ảnh
 *
 * @param string $content
 * @return void
 */
function extract_image_in_content($content)
{
    preg_match_all('/<img[^>]+>/i', $content, $result);
    return $result;
}

/**
 * ### Xóa tất cả hình ảnh trong nội dung
 *
 * @param string $noiDung
 * @param string $pathToFile
 * @param string $domain
 * @return void
 */
function delete_image_in_content($noiDung, $pathToFile, $domain=null)
{
    if ($domain === null) {
        $domain = $_SESSION['ROOT_PATH'];
    }

    $imgs = extract_image_in_content($noiDung);
    if (count($imgs) > 0) {
        foreach ($imgs as $img) {
            for ($i = 0; $i < count($img); $i++) {
                $path = get_src_img($img[$i]);
                $path = str_replace($domain, '', $path);
                $path = $pathToFile.$path;
                $result.= $path;
                @unlink($path);
            }
        }
    }
}

/**
 * ### Kiểm tra trang web đang refresh
 *
 * @return boolean
 */
function is_refresh()
{
    $pageRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && ($_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0' ||  $_SERVER['HTTP_CACHE_CONTROL'] == 'no-cache');
    return $pageRefreshed == 1 ? true : false;
}

function tao_url($bien)
{
    $bien = remove_special_char($bien);
    if ($bien != '') {
        $marTViet = array(
            'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ', 'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', 'ì', 'í', 'ị', 'ỉ', 'ĩ', 'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ', 'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ', 'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ', 'đ', 'À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ', 'È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ', 'Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ', 'Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ', 'Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ', 'Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ', 'Đ', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')');
        $marKoDau = array(
            'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'y', 'y', 'y', 'y', 'y', 'd', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'Y', 'Y', 'Y', 'Y', 'Y', 'D', '', '', '', '', '', '', '', '', '', '');
        $bien = trim($bien);
        $bien = str_replace('/', '', $bien);
        $bien = str_replace(':', '', $bien);
        $bien = str_replace('!', '', $bien);
        $bien = str_replace('(', '', $bien);
        $bien = str_replace(')', '', $bien);
        $bien = str_replace($marTViet, $marKoDau, $bien);
        $bien = str_replace('-', '', $bien);
        $bien = str_replace('  ', ' ', $bien);
        $bien = str_replace(' ', '-', $bien);
        $bien = str_replace('%', '-', $bien);
        $bien = str_replace("'", '', $bien);
        $bien = str_replace('“', '', $bien);
        $bien = str_replace('”', '', $bien);
        $bien = str_replace(',', '', $bien);
        $bien = str_replace('.', '', $bien);
        $bien = str_replace('"', '', $bien);
        $bien = str_replace('\\', '', $bien);
        $bien = str_replace('//', '', $bien);
        $bien = str_replace('?', '', $bien);
        $bien = str_replace('&', '', $bien);

        return $bien;
    }
}

function tao_tu_khoa($bien)
{
    if ($bien != '') {
        $marTViet = array(
            'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ', 'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', 'ì', 'í', 'ị', 'ỉ', 'ĩ', 'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ', 'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ', 'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ', 'đ', 'À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ', 'È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ', 'Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ', 'Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ', 'Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ', 'Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ', 'Đ', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', );
        $marKoDau = array(
            'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'y', 'y', 'y', 'y', 'y', 'd', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'Y', 'Y', 'Y', 'Y', 'Y', 'D', '', '', '', '', '', '', '', '', '', '', );
        $bien = trim($bien);
        $bien = str_replace($marTViet, $marKoDau, $bien);
    }

    return $bien;
}

function create_background($array_background)
{
    $s = '<style type="text/css">';
    if (count($array_background) > 0) {
        $s = $s . 'body {';
        if ($array_background['xemhinh'] == 1) {
            $s = $s . 'background-image:url("' . ROOT_PATH . 'uploads/background/' . $array_background['urlfile'] . '"); ';
            $s = $s . 'background-position:' . $array_background['canle'] . '; ';
            $s = $s . 'background-repeat:' . $array_background['laphinh'] . '; ';
            $s = $s . 'background-attachment:' . $array_background['fixed'] . '; ';
        }
        $s = $s . 'background-color:#' . $array_background['color'] . '; ';
        $s = $s . 'margin:0px; ';
        $s = $s . '}';
    }
    $s = $s . '</style>';
    return $s;
}

function load_content($file_templates, $array_bien)
{
    $result    = '';
    $templates = file_get_contents($file_templates);
    if (count($array_bien) > 0) {
        foreach ($array_bien as $key_bien => $info_bien) {
            $key_value = $info_bien;
            if ($result == '') {
                $result = str_ireplace($key_bien, $key_value, $templates);
            } else {
                $result = str_ireplace($key_bien, $key_value, $result);
            }
        }
    }

    return $result;
}

function load_content_data($file_templates, $array_bien)
{
    $result = '';
    if (count($array_bien) > 0) {
        foreach ($array_bien as $key_bien => $info_bien) {
            $key_value = $info_bien;
            if ($result == '') {
                $result = str_ireplace($key_bien, $key_value, $file_templates);
            } else {
                $result = str_ireplace($key_bien, $key_value, $result);
            }
        }
    }

    return $result;
}

function utf8_substr($str, $from, $len)
{
    return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,' . $from . '}' . '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,' . $len . '}).*#s', '$1', $str);
}

function sendmail($array_mail) {
    global $mailer;
    $mail = $mailer;
    // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;
        // Enable verbose debug output
        $mail->isSMTP();
        // Set mailer to use SMTP
        $mail->Host = $array_mail['hostmail'];
        $mail->Mailer   = "smtp";
        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;
        // Enable SMTP authentication
        $mail->Username = $array_mail['user'];
        // SMTP username
        $mail->Password = $array_mail['pass'];
        // SMTP password
        $mail->SMTPSecure = $array_mail['ssl'] == 'on' ? 'ssl' : '';
        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $array_mail['port'];
        // TCP port to connect to
        $mail->SMTPSecure  = '';
        $mail->SMTPAutoTLS = false;
        $mail->CharSet     = 'UTF-8';

        //Recipients
        $mail->setFrom($array_mail['emailgui'], $array_mail['tieude']);

        //$mail->addAddress('joe@example.net', 'Joe User');
        // Add a recipient
        //$mail->addAddress('ellen@example.com');

        $emailnhan = explode(",", $array_mail['emailnhan']);
        if (is_array($emailnhan)) {
            foreach ($emailnhan as $mailn) {
                $mail->AddAddress($mailn,  $array_mail['tieude']);
            }
        } else {
            $mail->AddAddress($emailnhan, $tieude);
            // $mail->AddReplyTo($emailnhan, $fullname);
        }


        // Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');


        //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');
        // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');
        // Optional name


        //Content
        $mail->isHTML(true);
        // Set email format to HTML
        $mail->Subject = $array_mail['subject'];
        $mail->Body    = $array_mail['message'];
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        return true;
    } catch (Exception $e) {
        return false;
    }

}

function in_logo_len_hinh($hinhgoc, $hinh_logo, $hinhsetao, $vitrilogo)
{
    $marge_right  = 10;
    $marge_bottom = 10;
    $watermark    = imagecreatefrompng($hinh_logo);
    imagealphablending($watermark, false);
    imagesavealpha($watermark, true);
    $img      = imagecreatefromjpeg($hinhgoc);
    $img_w    = imagesx($img);
    $img_h    = imagesy($img);
    $wtrmrk_w = imagesx($watermark);
    $wtrmrk_h = imagesy($watermark);
    if ($vitrilogo == 0) { // can giua
        $dst_x = ($img_w / 2) - ($wtrmrk_w / 2); // For centering the watermark on any image
        $dst_y = ($img_h / 2) - ($wtrmrk_h / 2); // For centering the watermark on any image
    } elseif ($vitrilogo == 1) { // can tren trai
        $dst_x = 10; // For centering the watermark on any image
        $dst_y = 10; // For centering the watermark on any image
    } elseif ($vitrilogo == 2) { // can tren phai
        $dst_x = ($img_w) - ($wtrmrk_w + 10); // For centering the watermark on any image
        $dst_y = 10; // For centering the watermark on any image
    } elseif ($vitrilogo == 3) { // can duoi phai
        $dst_x = ($img_w) - ($wtrmrk_w + 10); // For centering the watermark on any image
        $dst_y = ($img_h) - ($wtrmrk_h + 10); // For centering the watermark on any image
    } elseif ($vitrilogo == 4) { // can duoi phai
        $dst_x = 10; // For centering the watermark on any image
        $dst_y = ($img_h) - ($wtrmrk_h + 10); // For centering the watermark on any image
    }
    imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
    imagejpeg($img, $hinhsetao, 100);
    imagedestroy($img);
    imagedestroy($watermark);
}

/**
 * Chuyển đổi mã màu sang %.
 *
 * @param [type] $color   [description]
 * @param bool   $opacity [description]
 *
 * @return [type] [description]
 */
function hex2rgba($color, $opacity = false)
{
    $default = 'rgb(0,0,0)';
    //Return default if no color provided
    if (empty($color)) {
        return $default;
    }
    //Sanitize $color if "#" is provided
    if ($color[0] == '#') {
        $color = substr($color, 1);
    }
    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array(
            $color[0] . $color[1],
            $color[2] . $color[3],
            $color[4] . $color[5],
        );
    } elseif (strlen($color) == 3) {
        $hex = array(
            $color[0] . $color[0],
            $color[1] . $color[1],
            $color[2] . $color[2],
        );
    } else {
        return $default;
    }
    //Convert hexadec to rgb
    $rgb = array_map('hexdec', $hex);
    //Check if opacity is set(rgba or rgb)
    if ($opacity) {
        if (abs($opacity) > 1) {
            $opacity = 1.0;
        }
        $output = 'rgba(' . implode(',', $rgb) . ',' . $opacity . ')';
    } else {
        $output = 'rgb(' . implode(',', $rgb) . ')';
    }
    //Return rgb(a) color string
    return $output;
}

function rgba2rgb($value)
{
    if (!$value) {
        return false;
    }
    $rs = substr($value, 4);
    $rs = trim($rs, '()');
    $rs = explode(',', $rs);
    if (count($rs) == 4) {
        array_pop($rs);
        $rs = implode(",", $rs);
        $rs = "rgb(".$rs.")";
    } else {
        return false;
    }

    return $rs;
}

function rgba2opacity($color)
{
    if (!$color) {
        return false;
    }
    $rs = substr($color, 4);
    $rs = trim($rs, '()');
    $rs = explode(',', $rs);
    $rs = end($rs);

    return substr($rs, 2);
}

function get_icon($icon, $linkIcon)
{
    $icon_show = '';
    $icon_link = '';
    // Xu ly link linkIcon
    if (!empty($linkIcon)) {
        $icon_link = $linkIcon;
    } else {
        $icon_link = 'uploads/layout/';
    }
    if (!empty($icon)) {
        if (substr($icon, 3, 3) == 'fa-') {
            $icon_show = '
            <span class="ws-icon">
                <i class="' . $icon . '"></i>
            </span>';
        } elseif (substr($icon, -4, 1) == '.') {
            $icon_show = '
            <span class="ws-icon">
                <img src="' . $icon_link . $icon . '" width="20px" onerror="xulyloi(this)" class="img-responsive" alt="' . $ten . '" />
            </span>';
        } else {
            $icon_show = null;
        }
    } else {
        $icon_show = null;
    }

    return $icon_show;
}

function check404($getIdtype = null, $getUrl = null, $getCat = null, $fileModules)
{
    if (($getUrl!='' or $getCat!='') and $getIdtype=='') {
        return header('location: 404.php');
    } elseif ($fileModules!='') {
        if (!file_exists($fileModules)) {
            return header('location: 404.php');
        } else {
            return null;
        }
    } else {
        return null;
    }
}

/**
 * Kiểm tra đường dẫn hiện tại có là admin không
 *
 * @return void
 */
function admin_path()
{
    $get_url_full = explode("/", $_SERVER['REQUEST_URI']);
    return in_array('adminweb', $get_url_full) ? true : false;
}

function replace_html($str)
{
    str_replace("'", "&apos;", trim($str));
    str_replace('"', '&quot;', trim($str));
    str_replace('&', '&amp;', trim($str));
    str_replace('>', '&gt;', trim($str));
    str_replace('<', '&lt;', trim($str));
    return $str;
}

function check_select($str)
{
    $str = trim($str);
    $str = replace_html($str);
    $str = htmlentities($str, ENT_QUOTES, "UTF-8");
    return $str;
}

function lay_loai_noidung($op)
{
    global $_arr_loai_noidung;
    return array_search($op, $_arr_loai_noidung);
}

function max_number_in_array($isarray, $column_name)
{
    $totla_item = count($isarray);
    @$max       = $isarray[0][$column_name];
    for ($i = 1; $i < $totla_item; ++$i) {
        $max = ($max < $isarray[$i][$column_name]) ? $isarray[$i][$column_name] : $max;
    }

    return $max;
}

/**
 * Tìm và lấy id ngôn ngữ mặc định
 *
 * @return void
 */
function get_id_lang()
{
    if (!empty($_SESSION['_lang'])) {
        $lang = $_SESSION['_lang'];
    } elseif (!empty($_SESSION['__defaultlang'])) {
        $lang = $_SESSION['__defaultlang'];
    } else {
        $lang = 1;
    }

    return $lang;
}

function is_dir_empty($dir)
{
    if (!is_readable($dir)) {
        return null;
    }
    return (count(scandir($dir)) == 2);
}

/**
 * ## Kiểm tra kiểu json
 *
 * @param json $string
 * @return boolean
 */
function is_json($string)
{
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * ## Thay đổi kích hình ảnh
 *
 * - json: Mảng json chứa các thông số cắt hình
 *
 * - class: Lớp cắt hình
 *
 * - pathin: Đường dẫn thư mục chứa hình cần cắt
 *
 * - pathout: Đường dẫn thư mục lưu hình đã cắt
 *
 * - imgin: Tên hình cần cắt
 *
 * - imgout: Tên hình cần lưu
 *
 * @param array $args
 * @return void
 */
function resize_image($args = [])
{
    $ret = null;
    // THÔNG SỐ KÍCH THƯỚC CẮT HÌNH
    if (isset($args['json']) && $args['json']) {
        $json = $args['json'];

        if (isset($args['thumb']) && $args['thumb']) {
            $chonchieurong  = $json->chonchieurong2;
            $chonchieucao   = $json->chonchieucao2;
            $chieurong      = $json->chieurong2;
            $chieucao       = $json->chieucao2;
            $chatluong      = $json->chatluong2 < 60 ? 60:  $json->chatluong2;
            $border         = $json->border;
            $bordercolor    = $json->bordercolor;
            $canvas         = $border;
            $color          = $bordercolor;
        } else {
            $chonchieurong  = $json->chonchieurong;
            $chonchieucao   = $json->chonchieucao;
            $chieurong      = $json->chieurong;
            $chieucao       = $json->chieucao;
            $chatluong      = $json->chatluong < 60 ? 60:  $json->chatluong;
            $border         = null;
            $bordercolor    = null;
            $canvas         = $json->canvas;
            $color          = $json->color;
        }
    } else {
        return "Không đọc được thông số cắt hình";
    }

    // ĐƯƠNG DẪN HÌNH
    if (!isset($args['pathin'])) {
        return "Không đọc được đường dẫn";
    }

    if (!isset($args['pathout'])) {
        return "Không đọc được đường dẫn";
    }

    // HÌNH ẢNH TRUYỀN VÀO
    if (!isset($args['imgin'])) {
        return "Không đọc được hình";
    }
    if (!isset($args['imgout'])) {
        return "Không đọc được hình";
    }

    // Khai báo lớp
    if (isset($args['class'])) {
        $img        = $args['class']->make($args['pathin'] . $args['imgin']);
        $get_width  = $img->width();
        $get_height = $img->height();

        // Có nhập chiều rộng và chiều rộng hình lớn hơn chiều rộng nhập vào
        if ($chieurong > 0 && $get_width > $chieurong) {

            // Có nhập chiều cao và chiều cao hình lớn hơn chiều cao nhập vào
            if ($chieucao > 0 && $get_height > $chieucao) {

                // Resize theo cả 2 chiều
                $tile_rong = $get_width / $chieurong;
                $tile_cao = $get_height / $chieucao;

                // Chọn canvas
                if ($canvas) {
                    if (isset($args['thumb']) && $args['thumb']) {
                        $chieurong -= $border;
                        $chieucao -= $border;
                    }
                    if ($tile_rong < $tile_cao) {
                        $img->resize(null, $chieucao, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    else {
                        $img->resize($chieurong, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }

                    if (isset($args['thumb']) && $args['thumb']) {
                        $img->crop($chieurong, $chieucao);
                        $img->resizeCanvas(($chieurong + $border), ($chieucao + $border), 'center', false, $bordercolor);
                    }
                    else {
                        $img->resizeCanvas($chieurong, $chieucao, 'center', false, $color);
                    }
                }
                else {

                    // Không chọn canvas
                    if ($tile_rong > $tile_cao) {
                        $img->resize(null, $chieucao, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    else {
                        $img->resize($chieurong, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $img->crop($chieurong, $chieucao);
                }
            }
            else if( $chieucao > 0 && $get_height < $chieucao ){
                // Resize Theo chiều rộng
                if( $get_width > $chieurong ) {
                    $img->resize($chieurong, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                // Canvas
                $img->resizeCanvas($chieurong, $chieucao, 'center', false, $color);
            }
            else{
                // Resize Theo chiều rộng
                $img->resize($chieurong, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
        }
        elseif ($chieurong > 0 && $get_width < $chieurong && $chieucao > 0) {
            // Resize Theo chiều cao
            if( $get_height > $chieucao ) {
                $img->resize(null, $chieucao, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            // Canvas
            $img->resizeCanvas($chieurong, $chieucao, 'center', false, $color);
        }
        elseif ($chieucao > 0 && $get_height > $chieucao) {
            // Resize Theo chiều cao
            $img->resize(null, $chieucao, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $img->save($args['pathout'] . $args['imgout'], $chatluong);
    }
    else {
        $ret = "Không đọc được lớp img";
    }

    // BÁO LỖI
    if ($ret) {
        return $ret;
    }
}

/**
 * Store an array inside a cookie as a JSON String
 * Useful for passing large bits of data :)
 * @author LiamC
 * @param $var = cookie value
 * @param $limit = size limit. default and max size for a cookie is 4kb.
**/
function cookie_array($var, $limit=4096, $cookie_name="my_cookie")
{
    //check if we have cookie
    if ($_COOKIE[$cookie_name]) {

        //decode cookie string from JSON
        $cookieArr = (array) json_decode($_COOKIE[$cookie_name]);

        if($var) {
            if( !in_array($var, $cookieArr) ) {
                //push additional data
                array_push($cookieArr, $var);
            }
            else {
                $key = array_search($var, $cookieArr);
                unset($cookieArr[$key]);
                // //remove empty values
                // foreach ($cookieArr as $k => $v) {
                //     if ($v == '' || is_null($v) || $v == 0) {
                //         unset($cookieArr[$k]);
                //     }
                // }
            }
        }

        //encode data again for cookie
        $cookie_data = json_encode($cookieArr);

        //need to limit cookie size. Default is set to 4Kb. If it is greater than limit. it then gets reset.
        $cookie_data = mb_strlen($cookie_data) >= $limit ? '' : $cookie_data;

        //destroy cookie
        setcookie($cookie_name, '', time()-3600, '/');

        //set cookie with new data and expires after a week
        setcookie($cookie_name, $cookie_data, time()+(3600*24*7), '/');
    } else {

        //create cookie with json string etc.
        $cookie_data = json_encode($var);
        //set cookie expires after a week
        setcookie($cookie_name, $cookie_data, time()+(3600*24*7), '/');
    }//end if
}//end cookieArray

function smarty_function_exisCookie( $params, &$smarty ) {
    if( isset($_COOKIE['productsave']) ) {
        $arrCookie = json_decode($_COOKIE['productsave']);
        return in_array($params['value'], $arrCookie) ? true : false;
    }
    else {
        return null;
    }
}

/**
 * ### Thêm Title và Alt cho hình ảnh trong nội dung
 *
 * @param string $content: Nội dung cần tìm
 * @param string $title: tiêu đề thay thế
 * @return void
 */
function replace_image_content( $content, $title ) {
    if( !$content ) return null;
    // Bóc tách hình ảnh ra khỏi nội dung
    preg_match_all('/<img[^>]+>/i',$content, $result);
    $arrimg = $result[0];
    $ret = null;

    if( count($arrimg) > 0 ) {
        // Chạy vòng lặp lấy các thẻ img
        $doc = new DOMDocument();
        $doc->loadHTML($content);
        $xml=simplexml_import_dom($doc); // just to make xpath more simple
        $images=$xml->xpath('//img');

        foreach ($images as $kimg => $img) {
            $alt      = $img['alt'];
            $titleimg = $img['title'];
            $retAlt = null;
            // THAY THẾ ALT
            if( !$alt || $alt == '' || $alt == "" || is_null($alt) ) {
                $retAlt = str_replace('alt=""', '', $arrimg[$kimg]);
                $retAlt = str_replace("alt=''", '', $retAlt);
                $retAlt = str_replace("alt", '', $retAlt);
                $retAlt = str_replace('<img', '<img alt="'.$title.'" ', $arrimg[$kimg]);
            }
            else {
                $retAlt = $arrimg[$kimg];
            }

            // THAY THẾ TITLE
            if( !$titleimg || $titleimg == '' || $titleimg == "" || is_null($titleimg) ) {
                $retAlt = str_replace('title=""', '', $retAlt);
                $retAlt = str_replace("title=''", '', $retAlt);
                $retAlt = str_replace("title", '', $retAlt);
                $retAlt = str_replace('<img', '<img title="'.$title.'" ', $retAlt);

            }

            $content = str_replace($arrimg[$kimg], $retAlt, $content);

        }
    }
    return $content;
}

/**
 * Lấy IP hiện tại
 *
 * @return void
 */
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

/**
 * Lấy tag nội dung
 *
 * @param string $tag
 * @param string $class
 * @param string $ext
 * @return void
 */

function ThayDoiURL($bien)
{
    if ($bien != '') {
        $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
            "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề"
            , "ế", "ệ", "ể", "ễ",
            "ì", "í", "ị", "ỉ", "ĩ",
            "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ"
            , "ờ", "ớ", "ợ", "ở", "ỡ",
            "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
            "ỳ", "ý", "ỵ", "ỷ", "ỹ",
            "đ",
            "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă"
            , "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
            "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
            "Ì", "Í", "Ị", "Ỉ", "Ĩ",
            "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ"
            , "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
            "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
            "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
            "Đ",
            "!", "@", "#", "$", "%", "^", "&", "*", "(", ")");
        $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"
            , "a", "a", "a", "a", "a", "a",
            "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
            "i", "i", "i", "i", "i",
            "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o"
            , "o", "o", "o", "o", "o",
            "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
            "y", "y", "y", "y", "y",
            "d",
            "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A"
            , "A", "A", "A", "A", "A",
            "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
            "I", "I", "I", "I", "I",
            "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"
            , "O", "O", "O", "O", "O",
            "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
            "Y", "Y", "Y", "Y", "Y",
            "D",
            "", "", "", "", "", "", "", "", "", "");
        $bien = trim($bien);
        $bien = str_replace("/", "", $bien);
        $bien = str_replace(":", "", $bien);
        $bien = str_replace("!", "", $bien);
        $bien = str_replace("(", "", $bien);
        $bien = str_replace(")", "", $bien);
        $bien = str_replace($marTViet, $marKoDau, $bien);
        $bien = str_replace("-", "", $bien);
        $bien = str_replace("  ", " ", $bien);
        $bien = str_replace(" ", "-", $bien);
        $bien = str_replace("%", "-", $bien);
        $bien = str_replace("'", "", $bien);
        $bien = str_replace("“", "", $bien);
        $bien = str_replace("”", "", $bien);
        $bien = str_replace(",", "", $bien);
        $bien = str_replace(".", "", $bien);
        $bien = str_replace('"', "", $bien);
        $bien = str_replace('\\', '', $bien);
        $bien = str_replace('//', '', $bien);
        $bien = str_replace('?', '', $bien);
        $bien = str_replace('&', '', $bien);
        return $bien;
    }
}

function get_tag( $tag, $class="tags", $ext=".tags", $label=true ) {
    $ret = null;
    $tag = trim($tag);
    if ($tag != '') {
        $tagarr = explode(',', $tag);
        if (count($tagarr) > 0) {
            $ret .= '<div class="clear"></div>
            <ul class="'.$class.' clearfix form-group">';
            if( $label ) {
                $ret.='
                <li class="tags_label"><i class="fa fa-tags"></i> Tags: </li>';
            }
            $soluongtag = count($tagarr);
            for ($i = 0; $i <= $soluongtag; ++$i) {
                if ($tagarr[$i] != '' && $tagarr[$i] != ',') {
                    $link_tag = ROOT_PATH.urlencode(trim($tagarr[$i]).$ext);
                    $ret .= '
                    <li class="tagitem">
                        <a href="'.$link_tag.'" title="'.$tagarr[$i].'">
                            '.$tagarr[$i].'
                        </a>
                    </li>';
                }
            }
            $ret .= '</ul>';
        }
    }
    return $ret;
}
/**
 * ### Kiểu tiền
 *
 * @param int $val
 * @return void
 */
function kieu_tien( $val ) {
    if( is_null($val) ) return null;
    $val = str_replace(".", ",", $val);
    return preg_replace('/[^0-9,]/', '', $val);
}

/**
 * ### Hàm lấy cấu hình tích điểm
 *
 * Truyền tham số '*' để lấy tất cả
 *
 * @param string $field
 * @return void
 */
function lay_cauhinh_tichdiem( $field = kichhoat ) {
    $data = Lazer::table("tichdiem")->find();
    // Lấy tất cả field
    if( $data->count() )
    if( $field === '*' ) {
        $ret = $data;
    }
    // Lấy 1 field duy nhất
    else {
        $ret = $data->$field;
    }
    return $ret;
}
/**
 * ### Tính phần trăm
 *
 * @param int $sotien
 * @param int $phantram
 * @return void
 */
function tinh_phantram( $sotien, $phantram  ) {
    return ($phantram / 100) * $sotien;
}
/**
 * ### Hàm tính kết quả phần trăm giữa 2 số
 *
 * * @param int $oldNumber => Số gốc
 * * @param int $newNumber => Số mới
 * * @param boolean $format => Định dạng kết quả
 * * @param boolean $invert => Đảo ngước 2 số
 * @return void
 */
function phantram( $oldNumber, $newNumber, $aVsB = false, $format = true, $invert = false ) {
    $ret = null;
    // A phần trăm với B
    if( $aVsB ) {
        if( $invert ){
            $ret = $oldNumber/$newNumber*100;
        }
        else {
            $ret = $newNumber/$oldNumber*100;
        }
    }
    else {
        if( $invert ){
            // Đảo ngược
            $ret = ($oldNumber - $newNumber) / $newNumber * 100;
        }
        else {
            // Thuận
            $ret = ($oldNumber - $newNumber) / $oldNumber * 100;
        }
    }
    // Định dạng kết quả
    if( $format ){
        number_format($ret, 2);
    }
    return $ret;
}
/**
 *### Trả về tổng giá trị đơn hàng
 *
 * @param array $arr_giohang
 * @return void
 */
function tongtien_donhang( $arr_giohang = array() ) {
    $tongtien = 0;
    if( count($arr_giohang) && is_array($arr_giohang) ) {
        foreach( $arr_giohang as $gh ) {
            $idsp    = $gh['idsp'];
            $soluong = $gh['soluong'];
            $dongia  = $gh['gia'];
            $tongtien += ($soluong * $dongia);
        }
    }
    return $tongtien;
}
/**
 * Tách số ra từ 1 chuỗi
 *
 * @param string $str
 * @return void
 */
function tach_so_tu_chuoi($str) {
    return (int) filter_var($str, FILTER_SANITIZE_NUMBER_INT);
}
/**
 * ## Đổi điểm thành tiền
 *
 * @param int $sodiem
 * @param int $tyletien
 * @return void
 */
function quydoi_tien( $sodiem, $tyletien ) {
    return $sodiem * $tyletien;
}
/**
 * ## Làm tròn số
 *
 * @param int $number
 * @param integer $precision
 * @return void
 */
function truncate_number( $number, $precision = 2) {
    // Zero causes issues, and no need to truncate
    if ( 0 == (int)$number ) {
        return $number;
    }
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}
/**
 * ## Đổi điểm ra tiền
 *
 * * int $diem
 * * int $tyledoi
 * @return void
 */
function doi_diem_ra_tien( $diem, $tyledoi = null ) {
    $lazer = Lazer::table('tichdiem')->find();
    if( $tyledoi === null ) {
        $tyledoi = $lazer->sotien; // 1 diem = sotien
    }
    return truncate_number($diem * $tyledoi);
}
/**
 * ## Đổi tiền thành điểm
 *
 * * int $tien
 * * int $tyledoi
 * @return void
 */
function doi_tien_ra_diem( $tien, $tyledoi = null ) {
    $lazer = Lazer::table('tichdiem')->find();
    if( $tyledoi === null ) {
        $tyledoi = $lazer->sotien; // 1 diem = sotien
    }
    return ($tien / $tyledoi);
}
/**
 * ## Hàm tạo chuỗi ngẫu nhiên
 *
 * @param integer $length
 * @return void
 */
function random_str( $length = 6, $uppercase = false ) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if( $length ) {
        $characters = substr($characters, 0, $length);
    }
    if( $uppercase ) {
        $characters = strtoupper($characters);
    }
    return str_shuffle($characters);
}
/**
 * ### Tạo mã kích hoạt
 *
 * @return void
 */
function create_code_active() {
    return rand(1111,9999);
}