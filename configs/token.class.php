<?php
/**
* LỚP TOKEN
*/
class Token {

    /** Khoi tao bien */
    protected $_name  = '';

    /** Ham set name cho token */
    public function setName($name) {
        return $this->_name = $name;
    }

    /** Ham get name cho token */
    public function getName() {
        return $this->_name;
    }

    /** ham get name token full */
    public function getNameFull() {
        return 'token_'.$this->_name;
    }

    /** Ham tao moi token */
    public function create($time_create = 'onetime') {
        # Khai báo tham số(parameter) không hợp lệ
        if ( $time_create != 'anytime' && $time_create != 'onetime' ) {
            return 'error';
            die();
        }

        # Kiem tra neu dev chua set name thi die()
        if ( empty($this->_name) ) {
            $this->delete();
            return 'Please set name token';
            die();
        }

        # Tao token cho moi lan refresh
        if ( $time_create == 'anytime' ) {
            // Tạo nhiều lần cho mỗi lần refresh
            return $_SESSION[$this->getNameFull()] = base64_encode(openssl_random_pseudo_bytes(32));
        } else {
            // Tao 1 lan
            return !isset( $_SESSION[$this->getNameFull()] ) || empty( $_SESSION[$this->getNameFull()] )
                ? $_SESSION[$this->getNameFull()] = base64_encode(openssl_random_pseudo_bytes(32))
                : $_SESSION[$this->getNameFull()];
        }
    }

    /** HAM Kiem Tra Token [true/false] */
    public function check($token) {
        return isset($_SESSION[$this->getNameFull()]) && $token === $_SESSION[$this->getNameFull()]
            ? true : false;
    }

    /** Ham Xoa 1 Token */
    public function delete() {
        if ( $this->check( @$_SESSION[$this->getNameFull()] ) ) {
            unset( $_SESSION[$this->getNameFull()] );
            return true;
        }
        return false;
    }

    /** Ham Xoa tat ca token */
    public function deleteAll() {
        foreach ($_SESSION as $kToken => $vToken) {
            if ( strpos( $kToken, 'token_' ) === 0 ) {
                unset( $_SESSION[$kToken] );
            }
        }
    }

    /** Ham tao meta */
    public function meta($key = 'csrf-token') {
        return '<meta name="'.$key.'" content="'.@$_SESSION[$this->getNameFull()].'" />';
    }
}
?>