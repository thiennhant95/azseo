<?php
$_arr_sort = [
    // 0 => @$arraybien['tentangdan'],
    // 1 => @$arraybien['tengiamdan'],
    0 => @$arraybien['xemnhieu'],
    1 => @$arraybien['giatangdan'],
    2 => @$arraybien['giagiamdan'],
    3 => @$arraybien['muanhieu']
];

$_arr_loai_noidung = array(
    0  => "content",
    1  => "product",
    2  => "video",
    3  => "intro",
    4  => "download",
    5  => "service",
    6  => "viewweb",
    7  => "customer",
    8  => "customerreviews",
    9  => "project",
    10 => "question",
    11 => "gallery",
    12 => "picture"
);
$_arr_loai_banner = array(
    0  => "banner",
    1  => "slideshow",
    2  => "bannerqc",
    3  => "doitac",
    4  => "thuvienanh",
    5  => "duan",
    10 => "sanpham_logo",
);
$_array_config_noidung = array(
    "ngay"              => 0,
    "tag"               => 1,
    "share"             => 1,
    "tinlienquan"       => 1,
    "tinlienquan_thumb" => 0,
    "tincungtag"        => 1,
    "sotinlienquan"     => 5,
    "commentfacebook"   => 0,
    "commentform"       => 1
);
$_array_config_sanpham = array(
    "ngay"              => 0,
    "tag"               => 1,
    "share"             => 1,
    "tinlienquan"       => 1,
    "tinlienquan_thumb" => 1,
    "tincungtag"        => 1,
    "sotinlienquan"     => 10,
    "commentfacebook"   => 1,
    "commentform"       => 0
);
$_array_config_video = array(
    "ngay"              => 1,
    "tag"               => 1,
    "share"             => 1,
    "tinlienquan"       => 1,
    "tinlienquan_thumb" => 1,
    "tincungtag"        => 1,
    "sotinlienquan"     => 10,
    "commentfacebook"   => 0,
    "commentform"       => 1
);
$_array_config_lang = array(
    "icon"     => 1,
    "text"     => 1,
    "dangxo"   => 0,
    "danglist" => 1,
);
$_array_config_cart = array(
    "login" => 0,
);
