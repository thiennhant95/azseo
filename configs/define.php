<?php
ini_set('session.use_only_cookies', true);
if (session_id() == '') session_start();
$hostname = $_SERVER['HTTP_HOST'];
$dirname = '/';
//if ($hostname == 'localhost' || $hostname == 'a' || $hostname == 'phamtri') {
//    define("LOCALHOST", "localhost");
//    define("USERNAME", "root");
//    define("PASSWORD", "");
//    define("DBNAME", "azseo_2");
//    $dirname =  basename(realpath(__DIR__ . '/..'));
//} else {
//    define("LOCALHOST", "localhost");
//    define("USERNAME", "root");
//    define("PASSWORD", "");
//    define("DBNAME", "azseo_2");
//}
define("LOCALHOST", "localhost:3308");
define("USERNAME", "root");
define("PASSWORD", "");
define("DBNAME", "azseo_2");

if ( (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') || (! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
	$server_request_scheme = 'https';
} else {
	$server_request_scheme = 'http';
}
$protocol = $server_request_scheme . '://';
$SCRIPT_NAME = str_ireplace("index.php", "", $_SERVER['SCRIPT_NAME']);
$SCRIPT_NAME = str_ireplace("adminweb/", "", $SCRIPT_NAME);
$_SESSION['BASEURL']   = $protocol . $hostname . $SCRIPT_NAME.
"uploads/";
define('ROOT_DIR', dirname(__DIR__));
define('LAZER_DATA_PATH', realpath(dirname(__FILE__)).'/json/');
//Path to folder with tables
// CHECK VERSION PHP
if ( version_compare(PHP_VERSION, '5.4', '<') ) {
    header( 'Content-Type: text/html; charset=utf-8' );
    exit('<center><strong>Phiên bản PHP hiện tại của Server là quá thấp để chạy được Website này. <p>Vui lòng nâng cấp PHP lên phiên bản: 5.4</p></strong></center>');
} elseif ( version_compare(PHP_VERSION, '7.0', '>=') ) {
    header( 'Content-Type: text/html; charset=utf-8' );
    exit('<center><strong>Phiên bản PHP hiện tại của Server là không phù hợp để chạy được Website này. <p>Vui lòng điều chỉnh PHP về phiên bản: từ 5.4 đến 5.6 </p></strong></center>');
}