<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require __DIR__.'/define.php';
    require ROOT_DIR.'/vendor/autoload.php';

    $models = new Models();
    $didong = new Mobile_Detect();
    $db     = new Webso();
    $mailer = new PHPMailer();

    if( admin_path() ) {
        // admin
        include ROOT_DIR.'/adminweb/plugin/pagination/pagination.php';
        include ROOT_DIR.'/adminweb/application/files/bien.php';
        include ROOT_DIR.'/adminweb/application/files/mang.php';
        include ROOT_DIR.'/adminweb/application/files/khaibao.php';
    }else{
        // site
        include ROOT_DIR.'/plugins/pagination/pagination.php';
        include ROOT_DIR.'/configs/bien.php';
        include ROOT_DIR.'/configs/mang.php';
    }

    require ROOT_DIR.'/smarty/configs/config.php';

    $folder_xml = ROOT_DIR.'/adminweb/plugin/xml/data/';
    include ROOT_DIR.'/adminweb/plugin/xml/database.php';
    $_getArrayconfig = get_data('config', '*', '', '', '');
    $_getArrayconfig = $_getArrayconfig[0];

    if ( $_getArrayconfig['baoloiweb'] == 'off' ) {
        error_reporting(0);
    } else {
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set("display_errors", 1);
    }