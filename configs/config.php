<?php
// Table name
class Config
{
    public function CatChuoi($bien, $sokytu)
    {
        if ($bien != '') {
            $mang = array("<div>", "</div>", "<p>", "</p>", "<a>", "</a>", '"');
            $bien = str_replace($mang, "", $bien);
            //$bien = htmlspecialchars($bien);
            $bien = strip_tags($bien);
            for ($i = $sokytu; $i > $sokytu - 10; $i--) {
                $chuoisokytuhientai = substr($bien, $i, 1);
                if ($chuoisokytuhientai == ' ') {
                    $k = $i;
                    break;
                }
            }
            if (strlen($bien) > $sokytu) {
                $bien = substr($bien, 0, $k) . '...';
            }
        }
        $bien = trim($bien);
        return $bien;
    }
    // Thay doi dia chi URL
    public function ThayDoiURL($bien)
    {
        if ($bien != '') {
            $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
                "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề"
                , "ế", "ệ", "ể", "ễ",
                "ì", "í", "ị", "ỉ", "ĩ",
                "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ"
                , "ờ", "ớ", "ợ", "ở", "ỡ",
                "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
                "ỳ", "ý", "ỵ", "ỷ", "ỹ",
                "đ",
                "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă"
                , "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
                "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
                "Ì", "Í", "Ị", "Ỉ", "Ĩ",
                "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ"
                , "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
                "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
                "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
                "Đ",
                "!", "@", "#", "$", "%", "^", "&", "*", "(", ")");
            $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"
                , "a", "a", "a", "a", "a", "a",
                "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
                "i", "i", "i", "i", "i",
                "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o"
                , "o", "o", "o", "o", "o",
                "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
                "y", "y", "y", "y", "y",
                "d",
                "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A"
                , "A", "A", "A", "A", "A",
                "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
                "I", "I", "I", "I", "I",
                "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"
                , "O", "O", "O", "O", "O",
                "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
                "Y", "Y", "Y", "Y", "Y",
                "D",
                "", "", "", "", "", "", "", "", "", "");
            $bien = trim($bien);
            $bien = str_replace("/", "", $bien);
            $bien = str_replace(":", "", $bien);
            $bien = str_replace("!", "", $bien);
            $bien = str_replace("(", "", $bien);
            $bien = str_replace(")", "", $bien);
            $bien = str_replace($marTViet, $marKoDau, $bien);
            $bien = str_replace("-", "", $bien);
            $bien = str_replace("  ", " ", $bien);
            $bien = str_replace(" ", "-", $bien);
            $bien = str_replace("%", "-", $bien);
            $bien = str_replace("'", "", $bien);
            $bien = str_replace("“", "", $bien);
            $bien = str_replace("”", "", $bien);
            $bien = str_replace(",", "", $bien);
            $bien = str_replace(".", "", $bien);
            $bien = str_replace('"', "", $bien);
            $bien = str_replace('\\', '', $bien);
            $bien = str_replace('//', '', $bien);
            $bien = str_replace('?', '', $bien);
            $bien = str_replace('&', '', $bien);
            return $bien;
        }
    }
    public function MangKhongDau($bien)
    {
        if ($bien != '') {
            $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
                "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề"
                , "ế", "ệ", "ể", "ễ",
                "ì", "í", "ị", "ỉ", "ĩ",
                "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ"
                , "ờ", "ớ", "ợ", "ở", "ỡ",
                "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
                "ỳ", "ý", "ỵ", "ỷ", "ỹ",
                "đ",
                "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă"
                , "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
                "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
                "Ì", "Í", "Ị", "Ỉ", "Ĩ",
                "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ"
                , "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
                "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
                "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
                "Đ");
            $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"
                , "a", "a", "a", "a", "a", "a",
                "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
                "i", "i", "i", "i", "i",
                "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o"
                , "o", "o", "o", "o", "o",
                "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
                "y", "y", "y", "y", "y",
                "d",
                "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A"
                , "A", "A", "A", "A", "A",
                "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
                "I", "I", "I", "I", "I",
                "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"
                , "O", "O", "O", "O", "O",
                "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
                "Y", "Y", "Y", "Y", "Y",
                "D");
            $bien = trim($bien);
            $bien = str_replace("/", "", $bien);
            $bien = str_replace(":", "", $bien);
            $bien = str_replace($marTViet, $marKoDau, $bien);
            return $bien;
        }
    }

    public function ReplaceContent($str, $size, $title_baiviet, $page_keyword, $gethost)
    {
        $str = str_ireplace('<img', '<img onload="if(this.width>' . $size . ') {this.resized=true; this.width=' . $size . ';}" title="' . $title_baiviet . '" alt="' . $title_baiviet . '"', $str);
        $str = str_ireplace('<table', '<table onload="if(this.width>' . $size . ') {this.width=' . $size . ';}"', $str);
        $str = str_ireplace('<div', '<div onload="if(this.width>' . $size . ') {this.resized=true; this.width=' . $size . ';}"', $str);
        $str = str_ireplace('<a', '<a  title="' . $title_baiviet . '" ', $str);
        $str = str_ireplace('/Images_upload', $gethost . 'Images_upload', $str);
        return $str;
    }
    public function checkEmail($email)
    {
        return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? true : false;
    }
    public function CheckViewWebsite($text)
    {
        return mysql_escape_string($text);
    }
    public function get_client_ip()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}
