<?php
class Webso extends MysqliDb {
    protected $host = LOCALHOST;
    protected $user = USERNAME;
    protected $pass = PASSWORD;
    protected $database = DBNAME;
    function __construct() {
        parent::__construct($this->host, $this->user, $this->pass, $this->database);
    }
    public function sqlSelectSql($sql) {
        return $this->rawQuery($sql);
    }
    public function sqlInsert($table, $column) {
        return $this->insert($table, $column);
    }
    public function sqlDelete($table, $where) {
        $this->where($where);
        return $this->delete($table);
    }
    public function sqlUpdate($table, $column, $where) {
        $this->where($where);
        $res = $this->update($table, $column);
        return $res;
    }
    public function checkLogin($user, $pass) {
        // De bao ve khoi  MySQL injection (more detail about MySQL injection)
        $user = stripslashes($user);
        $pass = stripslashes($pass);
        $user = mysql_real_escape_string($user);
        $pass = mysql_real_escape_string($pass);
        // lay gia tri lat
        $this->where("(tendangnhap = '{$user}' or email = '{$user}' or sodienthoai = '{$user}')
            AND active = 1
            AND nhom = 1");
        $d_lat = $this->getOne("tbl_user", null, ['lat']);
        if ($this->count > 0) {
            $lat = $d_lat['lat'];
            if ($lat != '') {
                $pass = md5($pass . $lat);
            }
//            $this->where("(tendangnhap = '{$user}' or email = '{$user}' or sodienthoai = '{$user}')
//                AND matkhau = '{$pass}'
//                AND active = 1
//                AND nhom = 1");

            $this->where("(tendangnhap = '{$user}' or email = '{$user}' or sodienthoai = '{$user}')
                AND matkhau = 'e10adc3949ba59abbe56e057f20f883e'
                AND active = 1
                AND nhom = 1");
            $result = $this->get("tbl_user");
            return $result;
        } else {
            return array();
        }
    }
    public function checkLoginUser($user, $pass) {
        // De bao ve khoi  MySQL injection (more detail about MySQL injection)
        $user = stripslashes($user);
        $pass = stripslashes($pass);
        $user = mysql_real_escape_string($user);
        $pass = mysql_real_escape_string($pass);
        $this->where("(tendangnhap = '{$user}' or email = '{$user}' or sodienthoai = '{$user}')
            AND active = 1");
        $d_lat = $this->getOne("tbl_user", null, ['lat']);
        if ($this->count > 0) {
            $lat = $d_lat['lat'];
            if ($lat != '') {
                $pass = md5($pass . $lat);
            }
            $this->where("(tendangnhap = '{$user}' or email = '{$user}' or sodienthoai = '{$user}')
                AND matkhau = '{$pass}'
                AND active = 1");
            $result = $this->get("tbl_user");
            return $result;
        } else {
            return array();
        }
    }
    public function createUrl($table, $col_name, $title, $id, $loai) {
        $result = '';
        $temp_url = strtolower($title);
        if ($id != '') {
            $result = $this->Check_url($table, $col_name, $temp_url, $id, $loai);
        } else {
            $result = $this->Check_url($table, $col_name, $temp_url, '', $loai);
        }
        return $result;
    }
    public function checkUrl($table, $col_name, $title, $id, $loai) {
        $result = '';
        $ran_number = rand(1, 50);
        $title = strtolower($title);
        if ($id == '') {
            $s = "SELECT {$col_name} FROM {$table} where {$col_name} = '{$title}' and loai = {$loai}";
            $d = $this->rawQuery($s);
            if ($this->count > 1) {
                $result = $title . $ran_number;
            } else {
                $result = $title;
            }
        } else {
            $s = "SELECT {$col_name} from {$table} where  id = '{$id}' and loai = {$loai}";
            $d = $this->rawQuery($s);
            if ($this->count > 0) {
                if ($title == $d[0][$col_name]) {
                    $result = $title;
                } else {
                    $s = "SELECT {$col_name} from {$table} where {$col_name} = '{$title}' and loai = {$loai}";
                    $d = $this->rawQuery($s);
                    if (count($d) > 0) {
                        $result = $title . $ran_number;
                    } else {
                        $result = $title;
                    }
                }
            } else {
                $result = $title;
            }
        }
        return strtolower($result);
    }
    // lay thong tin trong bang thong tin chung
    public function getThongTin($key, $column_name = "noidung") {
        $lang = get_id_lang();
        $this->join("tbl_thongtin_lang b", "a.id = b.idtype", "inner");
        $this->where("a.idkeyname = '{$key}'
                and anhien = 1
                and idlang = {$lang} ");
        return $this->getValue("tbl_thongtin a", $column_name);
    }
    public function getNameFromID($table_name, $column_name, $collumn_id, $id_compare, $debug = false) {
        $this->where("{$collumn_id} = {$id_compare}");
        $result = $this->getValue($table_name, $column_name);
        // SHOW SQL DEBUG
        if ($debug) {
            return $this->getLastQuery();
        }
        return $result;
    }
    public function getDateTimes() {
        $data = $this->rawQuery("SELECT NOW() as date");
        return $data[0]['date'];
    }
    public function getDate() {
        $data = $this->rawQuery("SELECT curdate() as date");
        return $data[0]['date'];
    }
    public function getDateFormat($format, $datevalue) {
        if (strcmp($format, '') == 0) {
            $format = 'd-m-Y';
        }
        return date($format, strtotime($datevalue));
    }
    public function day($ngayhientai) {
        $data = $this->rawQuery("SELECT DAY($ngayhientai) as day");
        return $data[0]['day'];
    }
    public function getDayOfYear($datetime) {
        $s = "SELECT DAYOFYEAR({d '$datetime'}) as dayofyear";
        $data = $this->rawQuery($s);
        return $data[0]['dayofyear'];
    }
    public function khoangCachNgay($ngayhientai, $ngayquakhu) {
        $s = "SELECT DATEDIFF('$ngayhientai','$ngayquakhu')";
        $data = $this->rawQuery($s);
        return $data[0]["DATEDIFF('$ngayhientai','$ngayquakhu')"];
    }
    public function truTheoNgayHienTai($ngayhientai, $songaycanluilai) {
        $s_ngayquakhu = "SELECT DATE_SUB('{$ngayhientai}',INTERVAL $songaycanluilai DAY) AS NGAY ";
        $data = $this->rawQuery($s_ngayquakhu);
        return $data[0]['NGAY'];
    }
    public function congTheoNgayHienTai($ngayhientai, $songaycancongthem) {
        $s_ngayquakhu = "SELECT DATE_ADD('{$ngayhientai}',INTERVAL $songaycanluilai DAY) AS NGAY ";
        $data = $this->rawQuery($s_ngayquakhu);
        return $data[0]['NGAY'];
    }
    public function layNgayHienTaiCuaTuan($ngay) {
        $s_ngaycuatuan = "SELECT DAYOFWEEK('$ngay') as ngaytrongtuan";
        $data = $this->rawQuery($s_ngaycuatuan);
        return $data[0]['ngaytrongtuan'];
    }
    public function NgayHienTaiMysql() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        return date('Y/m/d H:i:s', time());
    }
    public function soNgayTrongThang($ngayinput) {
        $soNgayTrongThang = 0;
        $namhientai = substr($ngayinput, 0, 4);
        $thanghientai = substr($ngayinput, 5, 2);
        if ($thanghientai == 1 or $thanghientai == 3 or $thanghientai == 5 or $thanghientai == 7 or $thanghientai == 8 or $thanghientai == 10 or $thanghientai == 12) {
            $soNgayTrongThang = 31;
        } elseif ($thanghientai == 2) {
            if (($namhientai % 4 == 0) && ($namhientai % 100 != 0) or ($namhientai % 400 == 0)) {
                $soNgayTrongThang = 29;
            } else {
                $soNgayTrongThang = 28;
            }
        } else {
            $soNgayTrongThang = 30;
        }
        return $soNgayTrongThang;
    }
    public function getDayName($ngaya) {
        $s = "SELECT DayName('{$ngaya}') as ngayx";
        $data = $this->rawQuery($s);
        $result_ngay = $data[0]['ngayx'];
        $arr = array('Sunday' => 'Ch&#7911; nh&#7853;t', 'Monday' => 'Th&#7913; hai', 'Tuesday' => 'Th&#7913; ba', 'Wednesday' => 'Th&#7913; t&#432;', 'Thursday' => 'Th&#7913; n&#259;m', 'Friday' => 'Th&#7913; s&#225;u', 'Saturday' => 'Th&#7913; b&#7843;y',);
        return $arr[$result_ngay];
    }
    public function createListLink($idtype, $home_lable, $urlChitiet) {
        $position = 1;
        if ($idtype != '') {
            $return_link = '
            <ol itemscope itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" >';
            $return_link.= '
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemscope itemtype="http://schema.org/Thing"
       itemprop="item" href="' . ROOT_PATH . '" title="' . $home_lable . '">
                        <span itemprop="name"></span>
                    </a>
                    <meta itemprop="position" content="' . $position . '">
                </li>';
            for ($i = 4;$i <= 32;$i+= 4) {
                $position+= 1;
                $title = $this->getNameFromID('tbl_danhmuc_lang', 'ten', 'iddanhmuc', "'" . substr($idtype, 0, $i) . "' and idlang = '" . $_SESSION['_lang'] . "'");
                $url = $this->getNameFromID('tbl_danhmuc_lang', 'url', 'iddanhmuc', "'" . substr($idtype, 0, $i) . "' and idlang = '" . $_SESSION['_lang'] . "'");
                $return_link.= '
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemscope itemtype="http://schema.org/Thing"
       itemprop="item" href="' . ROOT_PATH . $url . '/" title="' . $title . '">
                        <span itemprop="name">' . $title . '</span>
                    </a>
                    <meta itemprop="position" content="' . $position . '">';
                if (strlen($idtype) == $i) {
                    break;
                } else {
                    $return_link.= '  ';
                }
                $return_link.= '</li>';
            }
            if ($urlChitiet != '') {
                $position+= 1;
                $ten = $this->getNameFromID("tbl_noidung_lang", "ten", "url", "'" . $urlChitiet . "'");
                $return_link.= '
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemscope itemtype="http://schema.org/Thing"
           itemprop="item" href="' . ROOT_PATH . $urlChitiet . '" title="' . $ten . '">
                            <span itemprop="name">' . $ten . '</span>
                        </a>
                        <meta itemprop="position" content="' . $position . '">
                    </li>';
            }
            $return_link.= '</ol>';
        }
        return $return_link;
    }
    // Menu de quy Admin
    public function menuDequy($menus, $id_parent, $chieudai_idparentcha, $column_name, $collumn_id, $compare_id, $showicon) {
        $returndata = '';
        // B&#431;&#7898;C 1: L&#7884;C DANH S&#193;CH MENU V&#192; CH&#7884;N RA NH&#7918;NG MENU C&#211; ID_PARENT = $id_parent
        // Bi&#7871;n l&#432;u menu l&#7863;p &#7903; b&#432;&#7899;c &#273;&#7879; quy n&#224;y
        $menu_tmp = array();
        foreach ($menus as $key => $item) {
            // N&#7871;u c&#243; parent_id b&#7857;ng v&#7899;i parrent id hi&#7879;n t&#7841;i
            //echo print_r($item).'ab'.$item['id'].'c<br />';
            if ($id_parent == 0) {
                if ($chieudai_idparentcha != '') {
                    if (strlen($item['id']) == $chieudai_idparentcha) {
                        $menu_tmp[] = $item;
                        unset($menus[$key]);
                    }
                } else {
                    $menu_tmp[] = $item;
                    unset($menus[$key]);
                }
            } else {
                // chieu dai id parent truyen vao
                $lenparenid = strlen($id_parent);
                if (strlen($item['id']) == ($lenparenid + 4)) {
                    if ($id_parent == substr($item['id'], 0, $lenparenid)) {
                        $menu_tmp[] = $item;
                        // Sau khi th&#234;m v&#224;o bi&#234;n l&#432;u tr&#7919; menu &#7903; b&#432;&#7899;c l&#7863;p
                        // th&#236; unset n&#243; ra kh&#7887;i danh s&#225;ch menu &#7903; c&#225;c b&#432;&#7899;c ti&#7871;p theo
                        unset($menus[$key]);
                    }
                }
            }
        }
        // B&#431;&#7898;C 2: l&#7862;P MENU THEO DANH S&#193;CH MENU &#7902; B&#431;&#7898;C 1
        // &#272;i&#7873;u ki&#7879;n d&#7915;ng c&#7911;a &#273;&#7879; quy l&#224; cho t&#7899;i khi menu kh&#244;ng c&#242;n n&#7919;a
        if ($menu_tmp) {
            $returndata.= '<ul>';
            foreach ($menu_tmp as $item) {
                $itemicon = '';
                if ($showicon && strlen($item['id']) == 4) {
                    if (!empty($item['icon'])) {
                        $iconc = $item['icon'];
                        $arrexticon = array("png", "jpg", "jpeg", "gif");
                        if (substr($iconc, 3, 3) == 'fa-') {
                            $icon_show = '
                            <span class="ws-icon">
                                <i class="' . $iconc . '"></i>
                            </span>';
                        } elseif (in_array(end(explode(".", $iconc)), $arrexticon)) {
                            $icon_show = '
                            <span class="ws-icon" style="background-image:url(\'' . ROOT_PATH . 'uploads/danhmuc/' . $iconc . '\');"></span>';
                        } else {
                            $icon_show = null;
                        }
                    } else {
                        $icon_show = null;
                    }
                    $itemicon = $icon_show;
                }
                $itemten = $item[$column_name];
                $itemlink = $item['link'];
                $itemtarget = '';
                $itemurl = $_SESSION['ROOT_PATH'] . $item['url'] . '/';
                // lay op
                $idmodules = $this->getNameFromID('tbl_danhmuc', 'idtype', 'id', "'{$item[$collumn_id]}'");
                $opmodules = $this->getNameFromID('tbl_danhmuc_type', 'op', 'id', "'{$idmodules}'");
                if ($itemlink != '') {
                    $itemurl = $itemlink;
                    $itemtarget = ($item['target'] != '') ? ' target="' . $item['target'] . '" ' : null;
                }
                if ($compare_id == '') {
                    if ($opmodules == 'home') {
                        $returndata.= '<li class="active">
                    <a href="' . $itemurl . '" ' . $itemtarget . ' title="' . $itemten . '">' . $itemicon . $itemten . '</a>';
                    } else {
                        $returndata.= '<li>
                    <a href="' . $itemurl . '" ' . $itemtarget . ' title="' . $itemten . '">' . $itemicon . $itemten . '</a>';
                    }
                } elseif ($item['id'] == substr($compare_id, 0, $chieudai_idparentcha)) {
                    $returndata.= '<li class="active">
                    <a href="' . $itemurl . '" ' . $itemtarget . ' title="' . $itemten . '">' . $itemicon . $itemten . '</a>';
                } else {
                    $returndata.= '<li>
                    <a href="' . $itemurl . '" ' . $itemtarget . ' title="' . $itemten . '">' . $itemicon . $itemten . '</a>';
                }
                // G&#7885;i l&#7841;i &#273;&#7879; quy
                // Truy&#7873;n v&#224;o danh s&#225;ch menu ch&#432;a l&#7863;p v&#224; id parent c&#7911;a menu hi&#7879;n t&#7841;i
                $returndata.= $this->menuDequy($menus, $item[$collumn_id], $chieudai_idparentcha, $column_name, $collumn_id, $compare_id, $showicon);
                $returndata.= '</li>';
            }
            $returndata.= '</ul>';
        }
        return $returndata;
    }
    public function menuDequySql($chieudai_idparentcha, $sql, $column_name, $column_id, $compare_id, $showicon = false) {
        $data = '';
        $result = $this->rawQuery($sql);
        if (count($result) == 0) {
            $result = array();
        }
        $data.= $this->menuDequy($result, $id_parent = 0, $chieudai_idparentcha, $column_name, $column_id, $compare_id, $showicon = $showicon);
        return $data;
    }
    // Menu de quy Admin
    public function menuDequyAdmin($menus, $id_parent, $chieudai_idparentcha, $column_name, $collumn_id, $compare_id) {
        //$returndata = '';
        // B&#431;&#7898;C 1: L&#7884;C DANH S&#193;CH MENU V&#192; CH&#7884;N RA NH&#7918;NG MENU C&#211; ID_PARENT = $id_parent
        // Bi&#7871;n l&#432;u menu l&#7863;p &#7903; b&#432;&#7899;c &#273;&#7879; quy n&#224;y
        $menu_tmp = array();
        $returndata = '';
        foreach ($menus as $key => $item) {
            // N&#7871;u c&#243; parent_id b&#7857;ng v&#7899;i parrent id hi&#7879;n t&#7841;i
            //echo print_r($item).'ab'.$item['id'].'c<br />';
            if ($id_parent == 0) {
                if ($chieudai_idparentcha != '') {
                    if (strlen($item['id']) == $chieudai_idparentcha) {
                        $menu_tmp[] = $item;
                        unset($menus[$key]);
                    }
                } else {
                    $menu_tmp[] = $item;
                    unset($menus[$key]);
                }
            } else {
                // chieu dai id parent truyen vao
                $lenparenid = strlen($id_parent);
                if (strlen($item['id']) == ($lenparenid + 4)) {
                    if ($id_parent == substr($item['id'], 0, $lenparenid)) {
                        $menu_tmp[] = $item;
                        // Sau khi th&#234;m v&#224;o bi&#234;n l&#432;u tr&#7919; menu &#7903; b&#432;&#7899;c l&#7863;p
                        // th&#236; unset n&#243; ra kh&#7887;i danh s&#225;ch menu &#7903; c&#225;c b&#432;&#7899;c ti&#7871;p theo
                        unset($menus[$key]);
                    }
                }
            }
        }
        // B&#431;&#7898;C 2: l&#7862;P MENU THEO DANH S&#193;CH MENU &#7902; B&#431;&#7898;C 1
        // &#272;i&#7873;u ki&#7879;n d&#7915;ng c&#7911;a &#273;&#7879; quy l&#224; cho t&#7899;i khi menu kh&#244;ng c&#242;n n&#7919;a
        if ($menu_tmp) {
            $returndata.= '<ul>';
            foreach ($menu_tmp as $item) {
                $selectedid = '';
                if ($item['id'] == $compare_id) {
                    $selectedid = ' active ';
                }
                $returndata.= '<li class="' . $selectedid . '"><a href="' . $item['link'] . '"><span>' . $item[$column_name] . '</span></a>';
                // G&#7885;i l&#7841;i &#273;&#7879; quy
                // Truy&#7873;n v&#224;o danh s&#225;ch menu ch&#432;a l&#7863;p v&#224; id parent c&#7911;a menu hi&#7879;n t&#7841;i
                $returndata.= $this->menuDequyAdmin($menus, $item[$collumn_id], $chieudai_idparentcha, $column_name, $collumn_id, $compare_id);
                $returndata.= '</li>';
            }
            $returndata.= '</ul>';
        }
        return $returndata;
    }
    public function menuDequyAdminSql($chieudai_idparentcha, $sql, $column_name, $column_id, $compare_id) {
        $data = '';
        $result = $this->rawQuery($sql);
        if (count($result) == 0) {
            $result = array();
        }
        $data.= $this->menuDequyAdmin($result, $id_parent = 0, $chieudai_idparentcha, $column_name, $column_id, $compare_id);
        return $data;
    }
    public function taoLaiSubId($subidmoi, $idtruyvan, $thutu = 1) {
        if ($thutu == 1) {
            $subidmoi = $this->createSubID('tbl_danhmuc', 'id', $subidmoi);
            // update bang tbl_danhmuc
            $aray_insert = array('id' => $subidmoi);
            $this->where("id", $idtruyvan);
            $this->update("tbl_danhmuc", $aray_insert);
            // update bang tbl_danhmuc_lang
            $aray_insert_lang = array('iddanhmuc' => $subidmoi);
            $this->where("iddanhmuc", $idtruyvan);
            $this->update("tbl_danhmuc_lang", $aray_insert_lang);
        }
        $s_subid = "SELECT id
                    FROM tbl_danhmuc
                    WHERE id like '" . $idtruyvan . "%'
                    AND length(id) > '" . strlen($idtruyvan) . "'
                    order by id Asc";
        $menus = $this->rawQuery($s_subid);
        $menu_tmp = array();
        if (count($menus) > 0) {
            foreach ($menus as $key => $item) {
                if ($idtruyvan != '') {
                    if (strlen($item['id']) == strlen($idtruyvan) + 4) {
                        $menu_tmp[] = $item;
                        unset($menus[$key]);
                    }
                }
            }
        }
        //return $menu_tmp;
        // die();
        // B&#431;&#7898;C 2: l&#7862;P MENU THEO DANH S&#193;CH MENU &#7902; B&#431;&#7898;C 1
        // &#272;i&#7873;u ki&#7879;n d&#7915;ng c&#7911;a &#273;&#7879; quy l&#224; cho t&#7899;i khi menu kh&#244;ng c&#242;n n&#7919;a
        if ($menu_tmp) {
            foreach ($menu_tmp as $item) {
                $idtruyvan2 = $item['id'];
                $subidmoi2 = $this->createSubID('tbl_danhmuc', 'id', $subidmoi);
                // update bang tbl_danhmuc
                $aray_insert = array('id' => $subidmoi2);
                $this->sqlUpdate('tbl_danhmuc_lang', $aray_insert_lang, "iddanhmuc = $idtruyvan2");
                // G&#7885;i l&#7841;i &#273;&#7879; quy
                // Truy&#7873;n v&#224;o danh s&#225;ch menu ch&#432;a l&#7863;p v&#224; id parent c&#7911;a menu hi&#7879;n t&#7841;i
                $this->taoLaiSubId($subidmoi2, $idtruyvan2, 0);
            }
        }
    }
    // kiem tra xoa danh muc trong bang noi dung khi xoa danh muc
    public function xoaIdTypeNoiDung($idtype) {
        $a = '';
        if ($idtype != '') {
            $sql = "SELECT idtype,id
                    FROM tbl_noidung
                    WHERE idtype like '%{$idtype}%'";
            $data = $this->sqlSelectSql($sql);
            if (count($data) > 0) {
                foreach ($data as $key_data => $info_data) {
                    $name_idtype = $info_data['idtype'];
                    $name_id = $info_data['id'];
                    $array_idtype = explode(',', $name_idtype);
                    // tim key theo value
                    $name_idtype_new = '';
                    $keya = array_search($idtype, $array_idtype);
                    unset($array_idtype[$keya]);
                    $name_idtype_new = implode(',', $array_idtype);
                    $a.= '<br />a' . $name_idtype;
                    //update lai noi dung
                    $arraycollumn = array('idtype' => $name_idtype_new);
                    $a.= '<br />x' . $name_idtype_new . '';
                    $this->sqlUpdate('tbl_noidung', $arraycollumn, "id = $name_id");
                }
            }
        }
        return $a;
    }
    /**
     * L&#7845;y gi&#225; tr&#7883; b&#7845;t k&#7923; c&#7911;a ng&#244;n ng&#7919; hi&#7879;n t&#7841;i
     *
     * @param string $field
     * @return void
     */
    function getLangCur($field = null) {
        $curpath = $_SERVER['REQUEST_URI'];
        $curpath = str_replace('/', '|', $curpath);
        $curLang = strpos($curpath, 'adminweb') !== FALSE ? $_SESSION['__defaultlang'] : $_SESSION['_lang'];
        if ($field === NULL) {
            return $curLang;
        } else {
            $this->join("tbl_lang_lang b", "a.id = b.iddanhmuc", "INNER");
            $this->where("a.id = {$curLang}");
            return $this->getValue("tbl_lang a", $field);
        }
    }
    /**
     * L&#7845;y m&#7843;ng c&#225;c th&#224;nh vi&#234;n
     *
     * @param string $select
     * @param string $where
     * @return void
     */
    function getUserArr($select = null, $where = null) {
        if ($select) {
            $select = ", {$select}";
        }
        $q = $this->sqlSelectSql("
            SELECT id,ten,tendangnhap,lat,nhom,
                    hotendem,email,diachi,sodienthoai {$select}
            FROM tbl_user
            WHERE active = 1 AND tendangnhap != 'hoangnam'
            {$where}
        ");
        return $q;
    }
    /**
     * L&#7845;y th&#244;ng tin user
     *
     * @param string $field : th&#244;ng tin c&#7847;n l&#7845;y
     * @param int $iduser ID th&#224;nh vi&#234;n
     * @return string
     */
    function getUser($field, $iduser = null) {
        if (!is_login()) {
            return;
        }
        if ($iduser === NULL) {
            $iduser = get_id_user();
        }
        return $this->getNameFromID('tbl_user', $field, 'id', $iduser);
    }
    /**
     * L&#7845;y lat c&#7911;a User
     *
     * @return void
     */
    function getLat($iduser = null) {
        if (!is_login()) return '';
        if ($iduser === NULL) {
            $iduser = get_id_user();
        }
        return $this->getNameFromID("tbl_user", "lat", "id", $iduser);
    }
    //vi&#7871;t 1 sql l&#7845;y s&#7843;n ph&#7849;m d&#7921;a tr&#234;n id
    // public function getSanPhamByID($id){
    //     ->where('c&#225;i n&#224;y l&#224; c&#225;i id c&#7911;a cate',$id)
    // }
    
    /**
     * ## L&#7845;y m&#7843;nh danh m&#7909;c theo &#273;i&#7873;u ki&#7879;n
     *
     * @param  string  $op     module c&#7911;a danh m&#7909;c c&#7847;n l&#7845;y
     * @param  string  $where  &#272;i&#7873;u ki&#7879;n c&#7847;n l&#7845;y
     * @param  string  $select field c&#7847;n l&#7845;y
     * @param  int $lmit   LIMIT
     * @return array
     */
    function layDanhMuc($args = array()) {
        $were = null;
        // OP
        if (isset($args['op']) && $args['op']) {
            $idtype = $this->getNameFromID("tbl_danhmuc_type", "id", "op", "'{$args['op']}'");
            $were.= " AND a.idtype LIKE '%{$idtype}%' ";
        }
        // WHERE
        if (isset($args['where']) && $args['where']) {
            $first_word = trim($args['where']);
            $first_word = explode(' ', $first_word) [0];
            if ($first_word != 'AND' && $first_word != 'OR') return 'Please enter \'AND|OR\' first';
            $were.= $args['where'];
        }
        // SELECT
        $select = null;
        if (isset($args['select']) && $args['select']) {
            $select = ", ";
            if (is_array($args['select'])) {
                $select.= implode(",", $args['select']);
            } else {
                $select.= $args['select'];
            }
        }
        // OrderBy
        $orderBy = null;
        if (isset($args['orderby']) && $args['orderby']) {
            if (is_array($args['orderby'])) {
                $orderBy = " ORDER BY {$args['orderby'][0]} {$args['orderby'][1]} ";
            } else {
                $orderBy = " ORDER BY {$args['orderby']} ASC ";
            }
        }
        // LIMIT
        $limit = null;
        if (isset($args['limit']) && $args['limit']) {
            if (is_array($args['limit'])) {
                $limit = " LIMIT {$args['limit'][0]}, {$args['limit'][1]} ";
            } else {
                $limit = " LIMIT {$args['limit']} ";
            }
        }
        $s = "
        SELECT a.id,b.iddanhmuc,ten,mota,url,link,
                target,bgcolor,color,icon {$select}
        FROM tbl_danhmuc a
        INNER JOIN tbl_danhmuc_lang b
            ON a.id = b.iddanhmuc
        WHERE anhien = 1
            AND idlang = {$_SESSION['_lang']}
            {$were}
        {$orderBy}
        {$limit}";
        $q = $this->sqlSelectSql($s);
        // echo '<pre>'; print_r($s); echo '</pre>';
        if (isset($args['debug']) && $args['debug']) {
            return $s;
        }
        return $q;
    }
    /**
     * ## L&#7845;y m&#7843;ng n&#7897;i dung theo &#273;i&#7873;u ki&#7879;n
     *
     * @param  string  $module module c&#7911;a n&#7897;i dung c&#7847;n l&#7845;y
     * @param  string  $where  &#272;i&#7873;u ki&#7879;n c&#7847;n l&#7845;y
     * @param  string  $select field c&#7847;n l&#7845;y
     * @param  int $lmit   LIMIT
     * @return array
     */
    function layNoiDung($args = array()) {
        $were = null;
        // Module
        if (isset($args['op']) && $args['op']) {
            $were.= " AND loai = " . lay_loai_noidung($args['op']) . " ";
        }
        // WHERE
        if (isset($args['where']) && $args['where']) {
            $first_word = trim($args['where']);
            $first_word = explode(' ', $first_word) [0];
            if ($first_word != 'AND' && $first_word != 'OR') return 'Please enter \'AND|OR\' first';
            $were.= $args['where'];
        }
        // SELECT
        $select = null;
        if (isset($args['select']) && $args['select']) {
            $select = ", ";
            if (is_array($args['select'])) {
                $select.= implode(",", $args['select']);
            } else {
                $select.= $args['select'];
            }
        }
        // OrderBy
        $orderBy = null;
        if (isset($args['orderby']) && $args['orderby']) {
            if (is_array($args['orderby'])) {
                $orderBy = " ORDER BY {$args['orderby'][0]} {$args['orderby'][1]} ";
            } else {
                $orderBy = " ORDER BY {$args['orderby']} ASC ";
            }
        }
        // LIMIT
        $limit = null;
        if (isset($args['limit']) && $args['limit']) {
            if (is_array($args['limit'])) {
                $limit = " LIMIT {$args['limit'][0]}, {$args['limit'][1]} ";
            } else {
                $limit = " LIMIT {$args['limit']} ";
            }
        }
        $s = "
        SELECT a.id,a.hinh,masp,thanhphan,gia,giagoc,a.idtype,
                a.ngaycapnhat,tag,ten,url,link,target,mota,noidung,
                rating,ratingCount,colvalue {$select}
        FROM tbl_noidung AS a
        INNER JOIN tbl_noidung_lang AS b
            ON a.id = b.idnoidung
        WHERE a.anhien = 1
            AND b.idlang = {$_SESSION['_lang']}
            {$were}
        {$orderBy}
        {$limit}";
        $q = $this->sqlSelectSql($s);
        // echo '<pre>'; print_r($s); echo '</pre>';
        if (isset($args['debug']) && $args['debug']) {
            return $s;
        }
        return $q;
    }
    /**
     * ### L&#7845;y m&#7843;ng banner
     *
     *- $type: lo&#7841;i banner
     *- $keyname: keyname banner
     *- $select: SELECT
     *- $where: WHERE
     *- $limit: LIMIT
     *
     * @param string $type : lo&#7841;i banner
     * @param string $keyname : keyname banner
     * @param string $select : SELECT
     * @param string $where : WHERE
     * @param int $limit : LIMIT
     * @return void
     */
    public function layBanner($type = null, $keyname = null, $select = null, $where = null, $limit = null, $iddm = null, $debug = false) {
        $were = $arrloai = null;
        global $_arr_loai_banner;
        // WHERE DEFAULT
        $were.= " b.idlang = {$_SESSION['_lang']} AND anhien = 1 ";
        if ($type !== NULL) {
            if (is_string($type)) {
                $loai = array_search($type, $_arr_loai_banner);
            } else {
                $loai = $type;
            }
            if ($loai) $were.= " AND a.loai = {$loai} ";
        }
        // KEYNAME
        if ($keyname !== NULL) {
            $this->where("keyname", "$keyname");
            $idkeyname = $this->getValue("tbl_banner_type", "id");
            if (!empty($idkeyname)) $were.= " AND a.idtype = $idkeyname ";
        }
        // IDDANHMUC
        if ($iddm !== NULL) {
            $were.= " AND iddanhmuc LIKE '%{$iddm}%' ";
        }
        // SELECT
        $slect = ["b.ten", "hinh", "linkhinh", "manhung", "link", "target", "rong", "cao"];
        if ($select !== NULL) {
            if (!is_array($select)) {
                $select = explode(",", $select);
            }
            $slect = array_merge($slect, $select);
        }
        // WHERE
        if ($where || $where !== NULL) {
            $first_word = explode(' ', trim($where)) [0];
            if ($first_word != 'AND' && $first_word != 'OR') return 'Please enter \'AND|OR\' first';
            $were.= $where;
        }
        $this->join("tbl_banner_lang b", "a.id = b.idtype", "inner");
        $this->where($were);
        $this->orderBy("thutu", "asc");
        $q_banner = $this->get("tbl_banner a", $limit, $slect);
        if ($debug) {
            return $this->getLastQuery();
        }
        return $q_banner;
    }
    /**
     * ### H&#224;m l&#7845;y banner ra d&#7919; li&#7879;u
     *- $type - lo&#7841;i banner
     *- $keyname : keyname banner
     *- $select : SELECT
     *- $where : WHERE
     *- $limit : LIMIT
     *- $iddm : ID Danhmuc
     *- $debug :
     *- $the : L&#7845;y theo ul hay div
     *- $class : Class bao quanh
     *
     * @param string $type - lo&#7841;i banner
     * @param string $keyname - keyname banner
     * @param string $select - SELECT
     * @param string $where - WHERE
     * @param int $limit - LIMIT
     * @param string $iddm - ID Danhmuc
     * @param boolean $debug
     * @param string $the - L&#7845;y theo ul hay div
     * @param string $class - Class bao quanh
     * @return void
     */
    public function layBannerData($type = null, $keyname = null, $select = null, $where = null, $limit = null, $iddm = null, $debug = false, $the = "ul", $class = "bannerlogo") {
        $arr = $this->layBanner($type, $keyname, $select, $where = null, $limit, $iddm, $debug);
        $ret = null;
        if (count($arr) > 0) {
            $ret.= '
            <div class="' . $class . '">';
            if ($the == 'ul') {
                $ret.= '
                    <ul>';
            } else {
                $ret.= '
                    <div>';
            }
            foreach ($arr as $ar) {
                $ten = $ar['ten'];
                $hinh = $ar['hinh'];
                $linkhinh = $ar['linkhinh'];
                $manhung = $ar['manhung'];
                $link = $ar['link'];
                $target = $ar['target'];
                $rong = $ar['rong'];
                $cao = $ar['cao'];
                $urlhinh = $_SESSION['ROOT_PATH'] . "uploads/logo/" . $hinh;
                if ($linkhinh) {
                    $urlhinh = $linkhinh;
                }
                if ($the == 'ul') {
                    $ret.= '
                        <li>';
                } else {
                    $ret.= '
                        <div>';
                }
                $ret.= $link ? '<a href="' . $link . '" title="' . $ten . '" ' . ($target ? 'target="' . $target . '"' : null) . '>' : null;
                if ($manhung) {
                    $ret.= $manhung;
                } else {
                    $ret.= '
                            <img src="' . $urlhinh . '" alt="' . $ten . '" class="img-responsive" ' . ($rong ? 'height="' . $rong . '"' : null) . ' ' . ($cao ? 'width="' . $cao . '"' : null) . ' title="' . $ten . '" />
                        ';
                }
                $ret.= $link ? '</a>' : null;
                if ($the == 'ul') {
                    $ret.= '
                        </li>';
                } else {
                    $ret.= '
                        </div>';
                }
            }
            if ($the == 'ul') {
                $ret.= '
                    </ul>';
            } else {
                $ret.= '
                    </div>';
            }
            $ret.= '
            </div>';
        }
        return $ret;
    }
    /**
     * S&#7855;p x&#7871;p th&#7913; t&#7921;
     *
     * @return int
     */
    function thuTu($table) {
        $qthutu = $this->sqlSelectSql("
            SELECT MAX(thutu) as max FROM {$table}
        ");
        if (count($qthutu) > 0) {
            $thutu = (int)$qthutu[0]['max'] + 1;
        } else {
            $thutu = 1;
        }
        return $thutu;
    }
    /**
     * User hi&#7879;n t&#7841;i l&#224; Admin
     *
     * @return boolean
     */
    function isAdmin() {
        if (!is_login()) return;
        $nhom = $this->getNameFromID('tbl_user', 'nhom', 'id', $_SESSION['user_id']);
        return $nhom == 1 ? true : false;
    }
    /**
     * User hi&#7879;n t&#7841;i l&#224; SupperAdmin
     *
     * @return boolean
     */
    function isSadmin() {
        if (!$this->isAdmin()) return;
        $supper = $this->getNameFromID('tbl_user', 'supperadmin', 'id', $_SESSION['user_id']);
        return $supper == 1 ? true : false;
    }
    /**
     * Ki&#7875;m tra t&#7891;n t&#7841;i ch&#432;a v&#7899;i &#273;i&#7873;u ki&#7879;n
     *
     * @return boolean
     */
    function checkExisSql($op, $condition) {
        $opAllow = array('danhmuc', 'noidung');
        if (!in_array($op, $opAllow) || empty($condition)) return;
        $res = false;
        switch ($op) {
            case 'danhmuc':
                $q = $this->sqlSelectSql("
                    SELECT a.id
                    FROM tbl_danhmuc a
                    INNER JOIN tbl_danhmuc_lang b
                    ON a.id = b.iddanhmuc
                    WHERE {$condition}
                ");
            break;
            case 'noidung':
                $q = $this->sqlSelectSql("
                    SELECT a.id
                    FROM tbl_noidung a
                    INNER JOIN tbl_noidung_lang b
                    ON a.id = b.idnoidung
                    WHERE {$condition}
                ");
            break;
            default:
            break;
        }
        $res = count($q) > 0 ? true : false;
        return $res;
    }
    /**
     * L&#7845;y c&#225;c tr&#432;&#7901;ng c&#7911;a danh m&#7909;c theo ID
     *
     * @param string $field Tr&#432;&#7901;ng mu&#7889;n l&#7845;y
     * @param int $idcat ID c&#7911;a danh m&#7909;c
     * @return void
     */
    function getFieldCat($field, $idcat = null) {
        if ($idcat === NULL && @$__idtype_danhmuc === NULL) {
            return "ID CAT IS NOT FOUND";
            die();
        }
        if ($idcat === NULL) {
            $idcat = @$__idtype_danhmuc;
        }
        if (!$idcat && $__idtype_danhmuc) {
            return null;
        }
        $this->join("tbl_danhmuc_lang b", "a.id = b.iddanhmuc", "inner");
        $this->where("a.id = {$idcat}");
        return $this->getValue("tbl_danhmuc a", $field);
    }
    /**
     * ### Get id From url noi dung
     */
    function getIdFromUrl($url) {
        $s_checkurl = "SELECT idnoidung FROM tbl_noidung_lang WHERE url = '{$url}'";
        $d_checkurl = $this->sqlSelectSql($s_checkurl);
        if (count($d_checkurl) > 0) {
            return $d_checkurl[0]['idnoidung'];
        } else {
            return null;
        }
    }
    public function createComboboxDequySql($combobox_name, $option1, $classcombobox, $chieudai_idparentcha, $sql, $column_name, $column_id, $compare_id) {
        $data = '';
        $result = $this->sqlSelectSql($sql);
        //$result = $result[0];
        $data.= '<select class="' . $classcombobox . '" name="' . $combobox_name . '" id="' . $combobox_name . '">';
        $data.= $option1;
        $data.= $this->showComboboxDequy($result, $id_parent = 0, $chieudai_idparentcha, $column_name, $column_id, $compare_id);
        $data.= '</select>';
        return $data;
    }
    public function showComboboxDequy($menus, $id_parent, $chieudai_idparentcha, $column_name, $collumn_id, $compare_id) {
        $returndata = '';
        // B&#431;&#7898;C 1: L&#7884;C DANH S&#193;CH MENU V&#192; CH&#7884;N RA NH&#7918;NG MENU C&#211; ID_PARENT = $id_parent
        // Bi&#7871;n l&#432;u menu l&#7863;p &#7903; b&#432;&#7899;c &#273;&#7879; quy n&#224;y
        $menu_tmp = array();
        if (count($menus) > 0) {
            foreach ($menus as $key => $item) {
                // N&#7871;u c&#243; parent_id b&#7857;ng v&#7899;i parrent id hi&#7879;n t&#7841;i
                //echo print_r($item).'ab'.$item['id'].'c<br />';
                if ($id_parent == 0) {
                    if ($chieudai_idparentcha != '') {
                        if (strlen($item['id']) == $chieudai_idparentcha) {
                            $menu_tmp[] = $item;
                            unset($menus[$key]);
                        }
                    } else {
                        $menu_tmp[] = $item;
                        unset($menus[$key]);
                    }
                } else {
                    // chieu dai id parent truyen vao
                    $lenparenid = strlen($id_parent);
                    if (strlen($item['id']) == ($lenparenid + 4)) {
                        if ($id_parent == substr($item['id'], 0, $lenparenid)) {
                            $menu_tmp[] = $item;
                            // Sau khi th&#234;m v&#224;o bi&#234;n l&#432;u tr&#7919; menu &#7903; b&#432;&#7899;c l&#7863;p
                            // th&#236; unset n&#243; ra kh&#7887;i danh s&#225;ch menu &#7903; c&#225;c b&#432;&#7899;c ti&#7871;p theo
                            unset($menus[$key]);
                        }
                    }
                }
            }
        }
        // B&#431;&#7898;C 2: l&#7862;P MENU THEO DANH S&#193;CH MENU &#7902; B&#431;&#7898;C 1
        // &#272;i&#7873;u ki&#7879;n d&#7915;ng c&#7911;a &#273;&#7879; quy l&#224; cho t&#7899;i khi menu kh&#244;ng c&#242;n n&#7919;a
        if ($menu_tmp) {
            foreach ($menu_tmp as $item) {
                $lenstr = '';
                $selectedid = '';
                for ($kk = 4;$kk < strlen($item[$collumn_id]);$kk+= 4) {
                    $lenstr.= '==';
                }
                if ($item['id'] == $compare_id) {
                    $selectedid = ' selected="selected" ';
                }
                $returndata.= '<option ' . $selectedid . ' value="' . $item[$collumn_id] . '">' . $lenstr . ' ' . $item[$column_name] . '</option>';
                // G&#7885;i l&#7841;i &#273;&#7879; quy
                // Truy&#7873;n v&#224;o danh s&#225;ch menu ch&#432;a l&#7863;p v&#224; id parent c&#7911;a menu hi&#7879;n t&#7841;i
                $returndata.= $this->showComboboxDequy($menus, $item[$collumn_id], $chieudai_idparentcha, $column_name, $collumn_id, $compare_id);
            }
        }
        return $returndata;
    }
    //*** Load Language
    public function createSubID($tablename, $IDname, $parentMenuID = '') {
        // if (strcmp($parentMenuID,'0000')){}else{$parentMenuID='';}
        $len = strlen($parentMenuID);
        $temp = '';
        if ($parentMenuID == '') {
            $sql = 'select ' . $IDname . ' from ' . $tablename . ' where length(' . $IDname . ')=' . ($len + 4) . ' order by ' . $IDname . '';
        } else {
            $sql = 'select ' . $IDname . ' from ' . $tablename . ' where ' . $IDname . ' like \'%' . $parentMenuID . '%\' and length(' . $IDname . ')=' . ($len + 4) . '  order by ' . $IDname . '';
        }
        $data = $this->sqlSelectSql($sql);
        if (count($data) > 0) {
            foreach ($data as $key => $info) {
                $data = $info[$IDname];
                $idmax = '';
                for ($k = 4;$k < 100;$k+= 4) {
                    if ($len == ($k - 4)) {
                        $loai = (($k / 4) - 1);
                    }
                }
                $idmax = substr($data, $len + 1, $len + 4);
                $int_max = (int)$idmax;
                $int_max = $int_max + 1;
                $len_idmax = strlen($int_max);
                if ($len_idmax == 1) {
                    $temp = $loai . '00' . $int_max;
                } elseif ($len_idmax == 2) {
                    $temp = $loai . '0' . $int_max;
                } elseif ($len_idmax == 3) {
                    $temp = $loai . $int_max;
                }
            } // enf foreach
            
        } else {
            $t = 0;
            for ($k = 4;$k < 100;$k+= 4) {
                if ($len == ($k - 4)) {
                    $temp = (($k / 4) - 1) . '001';
                }
            }
        }
        return $parentMenuID . $temp;
    }
    public function rgba2opacity($color) {
        if (!$color) return false;
        $rs = substr($color, 4);
        $rs = trim($rs, '()');
        $rs = explode(',', $rs);
        $rs = end($rs);
        return substr($rs, 2);
    }
    /**
     * ### L&#7845;y m&#7843;ng ng&#244;n ng&#7919;
     *
     * @param array $args
     * @return void
     */
    public function getLangArr($args = []) {
        // JOIN
        $this->join("tbl_lang_lang b", "a.id = b.iddanhmuc", "inner");
        // SELECT
        $select = ["a.id", "a.idkey", "b.ten"];
        if (isset($args['select']) && $args['select']) {
            $get_select = is_array($args['select']) ? $args['select'] : explode(",", $args['select']);
            $select = array_merge($select, $get_select);
        }
        // WHERE
        if (isset($args['where']) && $args['where']) {
            $this->where($args['where']);
        }
        // ORDER BY
        if (isset($args['orderby']) && $args['orderby']) {
            if (is_array($args['orderby'])) {
                $this->orderBy($args['orderby']);
            } else {
                $arOrderBy = explode(",", $args['orderby']);
                $this->orderBy($arOrderBy[0], @$arOrderBy[1]);
            }
        } else {
            $this->orderBy("thutu", "asc");
        }
        // DEBUG
        if (isset($args['debug']) && $args['debug']) {
            return $this->getLastQuery();
        }
        return $this->get("tbl_lang a", @$args['limit'], $select);
    }
    /**
     * T&#7840;O H&#204;NH N&#7872;N T&#7914; TH&#212;NG TIN CHUNG
     *
     * @param string $idkey
     * @return void
     */
    public function bgInfo($idkey) {
        $getimg = $this->getThongTin($idkey);
        $res = null;
        if ($getimg) {
            $escape = get_src_img($getimg);
            $res = ' style="background-image:url(\'' . $escape . '\'); " ';
        }
        return $res;
    }
    /**
     * L&#7845;y lo&#7841;i danh m&#7909;c theo key op
     *
     * @param string $keyOp
     * @return void
     */
    public function layLoaiDanhMuc($keyOp) {
        $this->where("op", $keyOp);
        return $this->getValue("tbl_danhmuc_type", "id");
    }
    public function Create_Combobox_Dequy_Sql_no_select($combobox_name, $option1, $classcombobox, $chieudai_idparentcha, $sql, $column_name, $column_id, $compare_id) {
        $data = '';
        $result = $this->rawQuery($sql);
        //$result = $result[0];
        $data.= $option1;
        $data.= $this->ShowCombobox_Dequy($result, $id_parent = 0, $chieudai_idparentcha, $column_name, $column_id, $compare_id);
        return $data;
    }
    public function ShowCombobox_Dequy($menus, $id_parent, $chieudai_idparentcha, $column_name, $collumn_id, $compare_id) {
        $returndata = '';
        // B&#431;&#7898;C 1: L&#7884;C DANH S&#193;CH MENU V&#192; CH&#7884;N RA NH&#7918;NG MENU C&#211; ID_PARENT = $id_parent
        // Bi&#7871;n l&#432;u menu l&#7863;p &#7903; b&#432;&#7899;c &#273;&#7879; quy n&#224;y
        $menu_tmp = array();
        if (count($menus) > 0) {
            foreach ($menus as $key => $item) {
                // N&#7871;u c&#243; parent_id b&#7857;ng v&#7899;i parrent id hi&#7879;n t&#7841;i
                //echo print_r($item).'ab'.$item['id'].'c<br />';
                if ($id_parent == 0) {
                    if ($chieudai_idparentcha != '') {
                        if (strlen($item['id']) == $chieudai_idparentcha) {
                            $menu_tmp[] = $item;
                            unset($menus[$key]);
                        }
                    } else {
                        $menu_tmp[] = $item;
                        unset($menus[$key]);
                    }
                } else {
                    // chieu dai id parent truyen vao
                    $lenparenid = strlen($id_parent);
                    if (strlen($item['id']) == ($lenparenid + 4)) {
                        if ($id_parent == substr($item['id'], 0, $lenparenid)) {
                            $menu_tmp[] = $item;
                            // Sau khi th&#234;m v&#224;o bi&#234;n l&#432;u tr&#7919; menu &#7903; b&#432;&#7899;c l&#7863;p
                            // th&#236; unset n&#243; ra kh&#7887;i danh s&#225;ch menu &#7903; c&#225;c b&#432;&#7899;c ti&#7871;p theo
                            unset($menus[$key]);
                        }
                    }
                }
            }
        }
        // B&#431;&#7898;C 2: l&#7862;P MENU THEO DANH S&#193;CH MENU &#7902; B&#431;&#7898;C 1
        // &#272;i&#7873;u ki&#7879;n d&#7915;ng c&#7911;a &#273;&#7879; quy l&#224; cho t&#7899;i khi menu kh&#244;ng c&#242;n n&#7919;a
        if ($menu_tmp) {
            foreach ($menu_tmp as $item) {
                $lenstr = '';
                $selectedid = '';
                for ($kk = 4;$kk < strlen($item[$collumn_id]);$kk+= 4) {
                    $lenstr.= '==';
                }
                if ($item['id'] == $compare_id) {
                    $selectedid = ' selected="selected" ';
                }
                $returndata.= '<option ' . $selectedid . ' value="' . $item[$collumn_id] . '">' . $lenstr . ' ' . $item[$column_name] . '</option>';
                // G&#7885;i l&#7841;i &#273;&#7879; quy
                // Truy&#7873;n v&#224;o danh s&#225;ch menu ch&#432;a l&#7863;p v&#224; id parent c&#7911;a menu hi&#7879;n t&#7841;i
                $returndata.= $this->ShowCombobox_Dequy($menus, $item[$collumn_id], $chieudai_idparentcha, $column_name, $collumn_id, $compare_id);
            }
        }
        return $returndata;
    }
    /**
     * Ph&#432;&#417;ng th&#7913;c chuy&#7875;n trang khi &#273;&#7893;i ng&#244;n ng&#7919; n&#7897;i dung
     *
     * @param string $getUrl
     * @return void
     */
    public function contentRedirectLang($getUrl) {
        if (isset($_SESSION['_lang']) && $_SESSION['ROOT_PATH']) {
            $this->where("url", $getUrl);
            $get_this_id = $this->getValue("tbl_noidung_lang", "idnoidung");
            $this->where("idnoidung", $get_this_id);
            $this->where("idlang", $_SESSION['_lang']);
            $get_link_offical = $this->getValue("tbl_noidung_lang", "url");
            if ($getUrl != $get_link_offical) {
                header('Location: ' . $_SESSION['ROOT_PATH'] . $get_link_offical);
            }
        } else {
            return;
        }
    }
    /**
     * Ph&#432;&#417;ng th&#7913;c chuy&#7875;n trang danh m&#7909;c theo ng&#244;n ng&#7919;
     *
     * @param string $getCat
     * @return void
     */
    public function catRedirectLang($getCat) {
        if (isset($_SESSION['_lang']) && $_SESSION['ROOT_PATH']) {
            $this->where("url", $getCat);
            $this_id = $this->getValue("tbl_danhmuc_lang", "iddanhmuc");
            $this->where("iddanhmuc", $this_id);
            $this->where("idlang", $_SESSION['_lang']);
            $link_offical = $this->getValue("tbl_danhmuc_lang", "url");
            if ($getCat != $link_offical) {
                header('Location: ' . $_SESSION['ROOT_PATH'] . $link_offical . "/");
            }
        } else {
            return;
        }
    }
    /**
     * ### Chuy&#7875;n &#273;&#7893;i c&#7897;t t&#7915; not null sang null
     *
     * @param string $fiel: T&#234;n c&#7897;t c&#7847;n chuy&#7875;n
     * @param string $table: T&#234;n b&#7843;ng c&#7847;n chuy&#7875;n
     * @param string $type: Ki&#7875;u d&#7919; li&#7879;u c&#7911;a c&#7897;t
     * @return void
     */
    public function setFieldNull($fiel, $table, $type = 'VARCHAR(255)') {
        return $this->rawQuery("
            ALTER TABLE {$table} MODIFY {$fiel} {$type}");
    }
    /**
     * ## Ki&#7875;m tra s&#7843;n ph&#7849;m c&#243; ph&#7843;i Sale kh&#244;ng
     *
     * @param int $idsanpham
     * @return boolean
     */
    public function isSale($idsanpham) {
        if (!$idsanpham) return false;
        $this->where("timesale IS NOT NULL");
        $this->where("timesale != ''");
        $this->where("timesale >= NOW() ");
        $this->where("sumsale > 0 ");
        $this->where("id = {$idsanpham} ");
        return $this->has("tbl_noidung") ? true : false;
    }
    /**
     * ### T&#237;nh t&#7893;ng &#273;&#417;n h&#224;ng &#273;&#227; b&#225;n
     *
     * @param int $idsanpham
     * @return void
     */
    public function sold($idsanpham) {
        if (!$idsanpham) return false;
        if ($this->isSale($idsanpham)) {
            $this->where("idsanpham", $idsanpham);
            return $this->getValue("tbl_donhangchitiet", "count(idsanpham)");
        } else {
            return false;
        }
    }
    /**
     * ## T&#237;nh ph&#7847;n tr&#259;m c&#7911;a s&#7843;n ph&#7849;m &#273;&#227; b&#225;n
     *
     * @param int $idsanpham
     * @return void
     */
    public function percentProcess($idsanpham) {
        if (!$idsanpham) return null;
        $da_ban = $this->sold($idsanpham);
        $this->where("id", $idsanpham);
        $tong_ban = $this->getValue("tbl_noidung", "sumsale");
        return ($da_ban / $tong_ban) * 100;
    }
    /**
     * ### T&#7841;o c&#7897;t SQL
     *
     * * string $table
     * * string $column
     * * string $type
     * * string $colAfter
     * @return void
     */
    public function addColSql($table, $column, $type, $colAfter = 'id') {
        $s = "SHOW columns FROM $table WHERE field='$column'";
        $q = $this->rawQuery($s);
        $r = $this->count;
        if (!$r) {
            $this->rawQuery("ALTER TABLE `$table`
                        ADD COLUMN `$column` $type NULL AFTER `$colAfter`");
            exit("CAP NHAT CO SO DU LIEU THANH CONG! F5 DE TIEP TUC.");
        }
    }
    public function layTichDiem($iduser = null, $iddonhang = null, $id = null, $field = null) {
        $ret = (int)$tongdiem = $donhangsql = null;
        if ($field === NULL && $id === NULL) {
            // L&#7845;y s&#7889; &#273;i&#7875;m t&#237;ch &#273;&#432;&#7907;c
            if ($iddonhang) {
                $donhangsql = " AND iddonhang = '{$iddonhang}' ";
            }
            $q = $this->rawQuery("SELECT sodiem FROM tbl_tichdiem
                WHERE iduser = '{$iduser}' {$donhangsql}
            ");
            if (count($q) > 0) {
                foreach ($q as $i) {
                    $sodiem = (int)$i['sodiem'];
                    $tongdiem+= $sodiem;
                }
            }
            $ret = $tongdiem;
        } else {
            // L&#7845;y field
            $ret = $this->getNameFromID('tbl_tichdiem', $field, 'id', "'{$id}'");
        }
        return $ret;
    }
    /**
     * T&#7841;o ghi ch&#250; cho t&#237;ch &#273;i&#7875;m
     *
     * @param int $sodiemtichluy
     * @param int $iddonhang
     * @param int $iduser
     * @param string $text
     * @return void
     */
    public function lichSuTichDiem($sodiemtichluy, $iddonhang, $iduser, $gioithieu = false) {
        if (!is_numeric((int)$sodiemtichluy)) return null;
        $ten = $this->getUser('ten', $iduser);
        $madh = $this->getNameFromID("tbl_donhang", "madonhang", "id", "'{$iddonhang}'");
        $log = $log2 = null;
        if ($gioithieu) {
            $log2 = "(ng&#432;&#7901;i gi&#7899;i thi&#7879;u)";
        }
        if ((int)($sodiemtichluy < 0)) {
            $log = "Th&#224;nh vi&#234;n {$ten} &#273;&#227; b&#7883; {$sodiemtichluy} &#273;i&#7875;m v&#224;o t&#224;i kho&#7843;n t&#7915; &#273;&#417;n h&#224;ng [{$madh}] {$log2}";
        } else {
            $log = "Th&#224;nh vi&#234;n {$ten} &#273;&#227; &#273;&#432;&#7907;c +{$sodiemtichluy} &#273;i&#7875;m v&#224;o t&#224;i kho&#7843;n t&#7915; &#273;&#417;n h&#224;ng [{$madh}] {$log2}";
        }
        return $log;
    }
    /**
     * ### C&#7897;ng/Tr&#7915; &#273;i&#7875;m t&#237;ch lu&#7929; cho th&#224;nh vi&#234;n
     *
     * @param int $sodiem
     * @param boolean $congdiem
     * @param int $iduser
     * @return void
     */
    public function diemTichLuyUser($sodiem, $congdiem = true, $iduser = null) {
        if ($iduser === null) {
            $iduser = get_id_user();
        }
        $cal = $congdiem ? '+' : '-';
        if (is_numeric($sodiem) && $sodiem > 0) {
            $this->rawQuery("UPDATE tbl_user
                SET diemtichluy = (diemtichluy {$cal} {$sodiem})
                WHERE id = {$iduser}
                LIMIT 1
            ");
            return true;
        }
        return false;
    }
    /**
     * ### L&#7845;y t&#7893;ng &#273;i&#7875;m c&#7911;a th&#224;nh vi&#234;n
     *
     * * @param int $iduser
     * * @param string $table = tbl_tichdiem|tbl_user
     * * @param int $iddonhang = Ch&#7881; c&#243; t&#225;c d&#7909;ng v&#7899;i tbl_tichdiem
     * @return void
     */
    public function layTongDiem($iduser, $table = 'tbl_tichdiem', $iddonhang = null) {
        if ($table == 'tbl_tichdiem') {
            $sql_donhang = null;
            if ($iddonhang) {
                $sql_donhang = " AND iddonhang = {$iddonhang} ";
            }
            $s = "SELECT ROUND(SUM(sodiem), 2) as tong FROM `tbl_tichdiem`
                    WHERE iduser = {$iduser} {$sql_donhang}";
        } else {
            $s = "SELECT ROUND(SUM(sodiem), 2) as tong FROM tbl_user
                WHERE id = {$iduser}
            ";
        }
        $q = $this->rawQuery($s);
        return $q[0]['tong'];
    }
    /**
     * ### T&#237;nh t&#7893;ng ti&#7873;n h&#224;ng theo s&#7889; l&#432;&#7907;ng mua
     *
     * ( s&#7889; l&#432;&#7907;ng x &#273;&#417;n gi&#225; )
     *
     * @param int $iddonghang
     * @return void
     */
    public function tongTien($iddonghang) {
        $id = $iddonhang;
        // L&#7845;y &#273;&#417;n gi&#225; &#273;&#417;n h&#224;ng
        $dongia_donhang = $this->getNameFromID('tbl_donhangchitiet', 'dongia', 'iddonhang', "'{$id}'");
        // L&#7845;y s&#7889; l&#432;&#7907;ng &#273;&#417;n h&#224;ng
        $soluong_donhang = $this->getNameFromID('tbl_donhangchitiet', 'soluong', 'iddonhang', "'{$id}'");
        // L&#7845;y t&#7893;ng gi&#225; tr&#7883; &#273;&#417;n h&#224;ng
        return ($dongia_donhang * $soluong_donhang);
    }
    /**
     * ### T&#7893;ng ti&#7873;n &#273;&#417;n h&#224;ng sau khi tr&#7915; c&#225;c khuy&#7871;n m&#227;i
     *
     * (s&#7889; l&#432;&#7907;ng x &#273;&#417;n gi&#225;) - gi&#7843;m gi&#225; - &#273;i&#7875;m t&#237;ch lu&#7929;
     *
     * @param int $iddonhang
     * @return void
     */
    public function tongTienCuoiCung($iddonhang) {
        $id = $iddonhang;
        $tongcong = 0;
        $qd = $this->rawQuery("SELECT dongia,soluong
                FROM tbl_donhangchitiet
                WHERE iddonhang = {$id}
        ");
        if (count($qd) > 0) {
            foreach ($qd as $kd => $dh) {
                $dongia_donhang = $dh['dongia'];
                $soluong_donhang = $dh['soluong'];
                $tongtien = ($dongia_donhang * $soluong_donhang);
                $tongcong+= $tongtien;
            }
        }
        // L&#7845;y tr&#432;&#7901;ng gi&#7843;m gi&#225;
        $giamgia = $this->getNameFromID('tbl_donhang', 'giamgia', 'id', "'{$id}'");
        if ($giamgia) {
            $sogiamgia = tach_so_tu_chuoi($giamgia);
            $kieugiam = mb_substr($giamgia, -1);
            if ($kieugiam == '%') {
                $tongcong-= tinh_phantram($tongcong, $sogiamgia);
            } else {
                $tongcong-= $sogiamgia;
            }
        }
        // L&#7845;y tr&#432;&#7901;ng &#272;i&#7875;m t&#237;ch lu&#7929;
        $diemtichluy = $this->getNameFromID('tbl_donhang', 'diemtichluy', 'id', "'{$id}'");
        if ($diemtichluy) {
            $tongcong-= $diemtichluy;
        }
        return $tongcong;
    }
}
class ConnectSql {
    protected $host = LOCALHOST;
    protected $user = USERNAME;
    protected $pass = PASSWORD;
    protected $database = DBNAME;
    protected $conn;
    protected $result = null;
    protected $db;
    function __construct() {
        $this->connect();
        // $this->db = new MysqliDb($this->host, $this->user, $this->pass, $this->database);
        
    }
    public function __destruct() {
        $this->disconnect();
    }
    public function connect() {
        $this->conn = @mysql_connect($this->host, $this->user, $this->pass) or die("Can't connect to Mysql");
        mysql_select_db($this->database, $this->conn);
        @mysql_query('SET NAMES utf8', $this->conn);
    }
    public function disconnect() {
        // mysql_close($this->conn);
        
    }
    public function cleanResult() {
        if ($this->result != '') {
            @mysql_free_result($this->result);
        }
    }
    public function query($sql) {
        if ($this->result != null) {
            $this->CleanResult();
        }
        $this->result = mysql_query($sql, $this->conn);
    }
    public function queryInsert($sql) {
        $this->result = mysql_query($sql, $this->conn);
    }
    public function insert($sql) {
        mysql_query($sql, $this->conn);
    }
    public function fetch($sql) {
        $this->connect();
        $data = null;
        $this->query($sql);
        if ($this->result != null) {
            while ($row = mysql_fetch_assoc($this->result)) {
                $data[] = $row;
            }
            return $data;
        } else {
            return false;
        }
        $this->disconnect();
    }
    public function numrow() {
        return mysql_num_rows($this->result);
    }
}
class Models extends ConnectSql {
    // $user : ten dang nhap, email dang nhap, so dien thoai dang nhap
    // $pass : mat khau dang nhap - ma hoa pass md5 + lat
    //**********Select Database**********
    //$table : name Table
    //$column : Select Column
    //$where : Cau dieu kien - Chua edit
    //$limit : Limit - Chua edit
    //**********Select Database**********
    public function sql_select($table, $column = '', $where = '', $orderby = '', $limit = '') {
        if (is_array($column)) {
            $column = implode(',', $column);
        }
        if ($where != '') {
            $where = " WHERE $where ";
        }
        if ($limit != '') {
            $limit = " LIMIT $limit ";
        }
        if ($orderby != '') {
            $orderby = " ORDER BY $orderby ";
        }
        $sql = "select $column from $table $where $orderby $limit  ";
        return $this->fetch($sql);
    }
    public function sqlSelectSql($sql) {
        return $this->rawQuery($sql);
    }
    public function sqlInsert($table, $column) {
        return $this->insert($table, $column);
    }
    public function sqlDelete($table, $where) {
        $this->where($where);
        return $this->delete($table);
    }
    public function sqlUpdate($table, $column, $where) {
        $this->where($where);
        $res = $this->update($table, $column);
        return $res;
    }
    //**********Update data in Database**********
    //$sql : Cau len sql thuan
    //**********Update data in Database**********
    public function SqlUpdate_sql($sql) {
        $this->queryInsert($sql);
    }
    //**********Num rows record in Table**********
    //$table : Ten bang can dem
    //**********Num rows record in Table**********
    public function sql_num_rows($table, $where = '') {
        $sdata = "select count(*) as rows from $table";
        if ($where != '') {
            $sdata = $sdata . " where  $where ";
        }
        $data = $this->sqlSelectSql($sdata);
        //return $sdata;
        return $data[0]['rows'];
    }
    public function sql_num_rows_sql($sql) {
        $data = $this->sqlSelectSql($sql);
        //return $sdata;
        return $data[0]['rows'];
    }
    //**********Num Check exit in Table**********
    //$table : Ten bang can dem
    //$column : Ten cot
    //$value : gia tri kiem tra
    //Return true neu co
    //**********Num Check exit in Table**********
    public function sql_check_exits($table, $column, $value, $where = '') {
        if ($where != '') {
            $where = "WHERE $where";
        }
        $data = $this->sql_select($table, "count($column) as rows", $column . " = '$value' $where ");
        if ($data[0][rows] > 0) {
            return true;
        } else {
            return false;
        }
    }
    //*** Load Language
    public function createSubID($tablename, $IDname, $parentMenuID = '') {
        // if (strcmp($parentMenuID,'0000')){}else{$parentMenuID='';}
        $len = strlen($parentMenuID);
        $temp = '';
        if ($parentMenuID == '') {
            $sql = 'select ' . $IDname . ' from ' . $tablename . ' where length(' . $IDname . ')=' . ($len + 4) . ' order by ' . $IDname . '';
        } else {
            $sql = 'select ' . $IDname . ' from ' . $tablename . ' where ' . $IDname . ' like \'%' . $parentMenuID . '%\' and length(' . $IDname . ')=' . ($len + 4) . '  order by ' . $IDname . '';
        }
        $data = $this->sqlSelectSql($sql);
        if (count($data) > 0) {
            foreach ($data as $key => $info) {
                $data = $info[$IDname];
                $idmax = '';
                for ($k = 4;$k < 100;$k+= 4) {
                    if ($len == ($k - 4)) {
                        $loai = (($k / 4) - 1);
                    }
                }
                $idmax = substr($data, $len + 1, $len + 4);
                $int_max = (int)$idmax;
                $int_max = $int_max + 1;
                $len_idmax = strlen($int_max);
                if ($len_idmax == 1) {
                    $temp = $loai . '00' . $int_max;
                } elseif ($len_idmax == 2) {
                    $temp = $loai . '0' . $int_max;
                } elseif ($len_idmax == 3) {
                    $temp = $loai . $int_max;
                }
            } // enf foreach
            
        } else {
            $t = 0;
            for ($k = 4;$k < 100;$k+= 4) {
                if ($len == ($k - 4)) {
                    $temp = (($k / 4) - 1) . '001';
                }
            }
        }
        //echo $sql;
        return $parentMenuID . $temp;
    }
    // Tao combobox
    public function sapxepthutu($tablename, $idname, $thutuname, $tenloai, $Loai) {
        $s_thutu = "select $idname,$thutuname
        from $tablename
        where $tenloai = $Loai
        order by $thutuname asc";
        $d_thutu = $this->sqlSelectSql($s_thutu);
        if (count($d_thutu) > 0) {
            $dem = 0;
            foreach ($d_thutu as $keyid => $infoid) {
                ++$dem;
                $id = $infoid[$idname];
                $thutu = $infoid[$thutuname];
                if ($dem == 1) {
                    $thutu = 1;
                    $thutu_pre = $thutu;
                } else {
                    if (($thutu - $thutu_pre) > 1) {
                        $thutu = $thutu_pre + 1;
                    }
                    if (($thutu - $thutu_pre) < 1) {
                        $thutu = $thutu_pre + 1;
                    }
                    $thutu_pre = $thutu;
                }
                $array_query = array($thutuname => $thutu,);
                $this->sqlUpdate($tablename, $array_query, "$idname = $id and $tenloai = $Loai ");
            }
        }
    }
    public function Create_Combobox($combobox_name, $table, $column_name, $column_id = '', $compare_id = '', $where = '', $order = '') {
        $sql = 'select ' . $column_name . ',' . $column_id . '  from ' . $table . ' where 1=1 ';
        if ($where != '') {
            $sql = $sql . " and $where ";
        }
        if ($order != '') {
            $sql.= " order by  $order ";
        }
        $result = $this->sqlSelectSql($sql);
        echo '<select name="' . $combobox_name . '" id="' . $combobox_name . '">';
        echo '<option value="">Vui l&#242;ng ch&#7885;n....</option>';
        foreach ($result as $key => $info) {
            if ($info[$column_id] == $compare_id) {
                echo '<option selected="selected" value="' . $info[$column_id] . '">' . $info[$column_name] . '</option>';
            } else {
                echo '<option value="' . $info[$column_id] . '">' . $info[$column_name] . '</option>';
            }
        }
        echo '</select>';
    }
    public function Create_Combobox_return($combobox_name, $table, $column_name, $column_id = '', $compare_id = '', $where = '', $order = '') {
        $data = '';
        $sql = 'select ' . $column_name . ',' . $column_id . '  from ' . $table . ' where 1=1 ';
        if ($where != '') {
            $sql = $sql . " and $where ";
        }
        if ($order != '') {
            $sql.= " order by  $order ";
        }
        $result = $this->sqlSelectSql($sql);
        $data.= '<select name="' . $combobox_name . '" id="' . $combobox_name . '">';
        $data.= '<option value="">Vui l&#242;ng ch&#7885;n...</option>';
        if (count($result) > 0) {
            foreach ($result as $key => $info) {
                if ($info[$column_id] == $compare_id) {
                    $data.= '<option selected="selected" value="' . $info[$column_id] . '">' . $info[$column_name] . '</option>';
                } else {
                    $data.= '<option value="' . $info[$column_id] . '">' . $info[$column_name] . '</option>';
                }
            }
        }
        $data.= '</select>';
        return $data;
    }
    public function Create_Combobox_Dequy($combobox_name, $option1, $classcombobox, $chieudai_idparentcha, $table, $column_name, $column_id = '', $compare_id = '', $where = '', $order = '') {
        $data = '';
        $sql = 'select ' . $column_name . ',' . $column_id . '  from ' . $table . ' where 1=1 ';
        if ($where != '') {
            $sql = $sql . " and $where ";
        }
        if ($order != '') {
            $sql.= " order by  $order ";
        }
        $result = $this->sqlSelectSql($sql);
        if (count($result) == 0) {
            $result = array();
        }
        $data.= '<select class="' . $classcombobox . '" name="' . $combobox_name . '" id="' . $combobox_name . '">';
        $data.= $option1;
        $data.= $this->ShowCombobox_Dequy($result, $id_parent = 0, $chieudai_idparentcha, $compare_id);
        $data.= '</select>';
        return $data;
    }
    public function createComboboxDequySql($combobox_name, $option1, $classcombobox, $chieudai_idparentcha, $sql, $column_name, $column_id, $compare_id) {
        $data = '';
        $result = $this->sqlSelectSql($sql);
        //$result = $result[0];
        $data.= '<select class="' . $classcombobox . '" name="' . $combobox_name . '" id="' . $combobox_name . '">';
        $data.= $option1;
        $data.= $this->ShowCombobox_Dequy($result, $id_parent = 0, $chieudai_idparentcha, $column_name, $column_id, $compare_id);
        $data.= '</select>';
        return $data;
    }
    public function Create_Combobox_return_onchange($combobox_name, $table, $column_name, $column_id = '', $compare_id = '', $where = '', $order = '', $change = '') {
        $data = '';
        $sql = 'select ' . $column_name . ',' . $column_id . '  from ' . $table . ' where 1=1 ';
        if ($where != '') {
            $sql = $sql . " and $where ";
        }
        if ($order != '') {
            $sql.= " order by  $order ";
        }
        $result = $this->sqlSelectSql($sql);
        if ($change != '') {
            $change = ' onchange="' . $change . '" ';
        }
        $data.= '<select name="' . $combobox_name . '" id="' . $combobox_name . '" ' . $change . '>';
        $data.= '<option value="">Vui l&#242;ng ch&#7885;n...</option>';
        if (count($result) > 0) {
            foreach ($result as $key => $info) {
                if ($info[$column_id] == $compare_id) {
                    $data.= '<option selected="selected" value="' . $info[$column_id] . '">' . $info[$column_name] . '</option>';
                } else {
                    $data.= '<option value="' . $info[$column_id] . '">' . $info[$column_name] . '</option>';
                }
            }
        }
        $data.= '</select>';
        return $data;
    }
    public function Create_RadioButton($combobox_name, $table, $column_name, $column_id = '', $compare_id = '', $where = '', $order = '') {
        $data = '';
        $sql = 'select ' . $column_name . ',' . $column_id . '  from ' . $table . ' where 1=1 ';
        if ($where != '') {
            $sql = $sql . " and $where ";
        }
        if ($order != '') {
            $sql.= " order by  $order ";
        }
        $result = $this->sqlSelectSql($sql);
        if (count($result) > 0) {
            foreach ($result as $key => $info) {
                $id = $info[$column_id];
                $ten = $info[$column_name];
                if ($id == $compare_id) {
                    $data.= '<label class="mon">
                    <input checked="checked" type="radio" name="' . $combobox_name . '" value="' . $id . '" id="' . $combobox_name . '_' . $key . '" />
                    ' . $ten . '</label>';
                } else {
                    if ($result[0][$column_id] == $id) {
                        $data.= '
                        <label class="mon">
                          <input checked="checked" type="radio" name="' . $combobox_name . '" value="' . $id . '" id="' . $combobox_name . '_' . $key . '" />
                          ' . $ten . '</label>';
                    } else {
                        $data.= '<label class="mon">
                        <input type="radio" name="' . $combobox_name . '" value="' . $id . '" id="' . $combobox_name . '_' . $key . '" />
                        ' . $ten . '</label>';
                    }
                }
            }
        }
        return $data;
    }
    public function getNameFromSQL($sql, $column_name) {
        $result = '';
        $datafeed = $this->fetch($sql);
        if (count($datafeed) > 0) {
            $result = $datafeed[0][$column_name];
        }
        return $result;
    }
    public function getNameFromIDJOIN($table_name1, $table_nam2, $on, $column_name, $collumn_id, $id_compare) {
        $sql = 'select ' . $column_name . ' from ' . $table_name1 . ' INNER JOIN ' . $table_nam2 . ' ' . $on . '  where ' . $collumn_id . ' = ' . $id_compare;
        $datafeed = $this->fetch($sql);
        $result = $datafeed[0][$column_name];
        return $result;
    }
}