<?php

require_once dirname(__DIR__)."/libs/Smarty.class.php";
$smarty = new Smarty();

if (!defined("SMARTY_DIR")) {
    define('SMARTY_DIR', 'smarty/');
}
if (!defined('LAYOUT_PATH')) {
    define('LAYOUT_PATH', 'templates/layout1/');
}
$_SESSION['LAYOUT_PATH'] = LAYOUT_PATH;

$smarty->setTemplateDir(dirname(__DIR__).'/../templates');
$smarty->setCompileDir(dirname(__DIR__).'/../smarty/templates_c');
$smarty->setCacheDir(dirname(__DIR__).'/../smarty/cache');
$smarty->setConfigDir(dirname(__DIR__).'/../smarty/configs');
$smarty->debugging = false;
$smarty->caching = false;
if ( isset($_GET['BUG_STY']) ) {
    $smarty->debugging = true;
}
$smarty->clearCache('templates/layout1/tpl/index.tpl');

// $smarty->testInstall();