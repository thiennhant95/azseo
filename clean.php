<?php

// Clean tbl_option_value
if( isset($_GET['tbl']) ){
    $get_table = $_GET['tbl'];
}elseif( isset($_GET['table']) ){
    $get_table = $_GET['table'];
}else{
    $get_table = null;
}

if( ! empty($get_table) ){

    switch( $get_table ){

        case 'tbl_option_value':
            $q = $db->sqlSelectSql("
                SELECT id,idnoidung,noidung
                FROM tbl_option_value
            ");

            if( count($q) > 0 ){
                foreach( $q as $i ){
                    $idnoidung = $i['idnoidung'];

                    $exis = $db->getNameFromID(
                        "tbl_noidung",
                        "id",
                        "id",
                        "'{$idnoidung}'"
                    );

                    if( empty($exis) ){
                        $db->sqlDelete(
                            "tbl_option_value",
                            "idnoidung = '{$idnoidung}' OR noidung = '' "
                        );

                        echo "Clean Success ! <br/>";
                    }else{
                        echo "Clean not found ! <br/>";
                    }
                }
            }

            break;

        case 'tbl_noidung_lang':
            $q = $db->sqlSelectSql("
                SELECT id
                FROM tbl_noidung_lang
                WHERE idnoidung = ''
            ");

            if( count($q) > 0 ){
                foreach($q as $i){
                    $idnoidunglang = $i['id'];

                    $db->sqlDelete(
                        "tbl_noidung_lang",
                        " id = '{$idnoidunglang}' "
                    );
                }

                echo "Clean noidung_lang Success";
            }else{
                echo "Nothing clean";
            }
            break;

        default:
            break;
    }

}else{
    echo "clean.php?tbl=your_table";
}
