<?php
require dirname(__DIR__).'/configs/inc.php';

// require_once ('libraries/Google/autoload.php');

require_once 'Google/Google_Client.php';
require_once 'Google/contrib/Google_Oauth2Service.php';
show_error();
if( !is_array($_getArrayconfig) || !$_getArrayconfig['fbid'] || !$_getArrayconfig['fbsecret'] || !$_getArrayconfig['domain'] ) {
    echo '<h2>Vui long hoan thanh cai dat trong cau hinh</h2>';
    exit;
}

$google_client_id     = $_getArrayconfig['glid'];
$google_client_secret = $_getArrayconfig['glsecret'];
$google_redirect_url  = $_getArrayconfig['domain'].'login-with-google/';
$google_developer_key = $_getArrayconfig['gldev'];

########################################################################
########################################################################
########################################################################
########################################################################

$gClient = new Google_Client();
$gClient->setApplicationName('Login to Google');
$gClient->setClientId($google_client_id);
$gClient->setClientSecret($google_client_secret);
$gClient->setRedirectUri($google_redirect_url);
$gClient->setDeveloperKey($google_developer_key);
$google_oauthV2 = new Google_Oauth2Service($gClient);

//If user wish to log out, we just unset Session variable
if (isset($_REQUEST['reset'])) {
    unset($_SESSION['token']);
    $gClient->revokeToken();
    header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
}
//If code is empty, redirect user to google authentication page for code.
//Code is required to aquire Access Token from google
//Once we have access token, assign token to session variable
//and we can redirect user back to page and login.
if (isset($_GET['code'])) {
    $gClient->authenticate($_GET['code']);
    $_SESSION['token'] = $gClient->getAccessToken();
    header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
    return;
}
if (isset($_SESSION['token'])) {
    $gClient->setAccessToken($_SESSION['token']);
}
if ($gClient->getAccessToken()) {
    //For logged in user, get details from google using access token
    $user              = $google_oauthV2->userinfo->get();
    $user_id           = $user['id'];
    $user_name         = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
    $email             = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
    $profile_url       = filter_var($user['link'], FILTER_VALIDATE_URL);
    $profile_image_url = filter_var($user['picture'], FILTER_VALIDATE_URL);
    $personMarkup      = "$email<div><img src='$profile_image_url?sz=50'></div>";
    $_SESSION['token'] = $gClient->getAccessToken();
} else {
    //For Guest user, get google login url
    $authUrl = $gClient->createAuthUrl();
}

if (isset($authUrl)) { //user is not logged in, show login button
    header("Location: ".$authUrl);
} else {
    // user logged in
    //list all user details
    // echo '<pre>';
    // print_r($user);
    // echo '</pre>';

////////////////////////////////////////////////////////////
// XU LY TREN WEB //
////////////////////////////////////////////////////////////

    // Tạo Cơ sở dữ liệu
    add_col_sql('tbl_user', 'appidgl', 'VARCHAR(200)', 'id');

    if( !empty($user['email']) ) {

        // 1. Kiểm tra xem email đã tồn tại trong DB chưa?
        $db->where("email", $user['email']);
        $qid = $db->getOne("tbl_user", ["appidgl,id,ten,email,tendangnhap"]);

        if( $db->count > 0 ){
            // Tồn tại trong DB - Cho login

            $_SESSION['tendangnhap'] = $qid['tendangnhap'];
            $_SESSION['user_id']     = $qid['id'];
            $_SESSION['user_ten']    = $qid['ten'];
            $_SESSION['user_email']  = $qid['email'];

            echo '<script>location.href="'.$_getArrayconfig['domain'].'"</script>';
        }else{
            // Chưa tồn tại trong DB - Lưu thông tin
            $matkhausend = substr(md5(time()), 0, 6);
            $lat         = substr(md5(time()), 0, 3);
            $matkhau     = create_pass($matkhausend, $lat);
            $tendangnhap = $user['email'];
            if( empty($tendangnhap) )
                $tendangnhap = strtolower(vn_str_filter($user['name']));

            $insU = $db->insert("tbl_user", [
                    'appidgl'     => $user['id'],
                    'email'       => $user['email'],
                    'ten'         => $user['name'],
                    'active'      => 1,
                    'tendangnhap' => $user['email'],
                    'matkhau'     => $matkhau,
                    'lat'         => $lat,
                    'loai'        => 0,
                    'nhom'        => 0,
                    'supperadmin' => 0
                ]
            );
            $account = !empty($user['email']) ? $user['email'] : $tendangnhap;

            // Khong lay dc id Insert
            if( empty($insU) )
                $db->where('email', $user['email']);
                $insU = $db->getValue('tbl_user', 'id');

            if( !empty($user['email']) ){
                // Send mail
                $tieude = "Tài khoản của bạn tại ". $_SERVER['HTTP_HOST'];
                $noidung = $db->getThongTin("thongtintaotaikhoan");
                $content = '
                    <br/>
                    <p>Tên đăng nhập: ' . $account . '</p>
                    <p>Mật khẩu: '.$matkhausend.'
                ';
                $noidung = str_replace("[THONGTINTAOTAIKHOAN]", $content, $noidung);

                $user_sendmail = array(
                    "emailnhan" => $user['email'],
                    "emailgui"  => $_getArrayconfig['hostmail_user'],
                    "hostmail"  => $_getArrayconfig['hostmail'],
                    "user"      => $_getArrayconfig['hostmail_user'],
                    "pass"      => $_getArrayconfig['hostmail_pass'],
                    "tieude"    => $tieude,
                    "fullname"  => $_getArrayconfig['hostmail_fullname'],
                    "port"      => $_getArrayconfig['hostmail_port'],
                    "ssl"       => $_getArrayconfig['hostmail_ssl'],
                    "subject"   => $tieude,
                    "message"   => $noidung,
                );
                sendmail($user_sendmail);
            }

            $_SESSION['user_id']    = $insU;
            $_SESSION['user_ten']   = $user['name'];
            $_SESSION['user_email'] = $user['email'];

        echo '<script>location.href="'.$_getArrayconfig['domain'].'"</script>';
        }
        echo ' <script>location.href="'.$_getArrayconfig['domain'].'"</script>';
    }else{
    // Không lấy đc Email
        echo '<script>location.href="'.$_getArrayconfig['domain'].'"</script>';
    }
}
