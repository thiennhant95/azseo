<?php

// Thiết lập cấu trúc fiel là xml
header("Content-type: text/xml");
session_start();

require 'configs/inc.php';
define('ROOT_PATH', 'http://' . $hostname . $SCRIPT_NAME);
if( !isset($_SESSION['ROOT_PATH'])
    || $_SESSION['ROOT_PATH'] != ROOT_PATH ) {
    $_SESSION['ROOT_PATH'] = ROOT_PATH;
}
if ( !function_exists('ws_get')
    || !function_exists('ws_post') ) {
    exit('File ws_function.php not found !');
}

// error_reporting(E_ALL);
// ini_set("display_errors", 1);

$_getcat = ws_get('cat');
if ($_getcat != '') {
    // lay idtype truy van tu bảng menu
    $__idtype_danhmuc = $db->getNameFromID('tbl_danhmuc_lang', 'iddanhmuc', 'url', "'".$_getcat."' ");
    $_idtype          = $db->getNameFromID('tbl_danhmuc','idtype', 'id', "'".$__idtype_danhmuc."'");
}

$_lang = $db->getNameFromID('tbl_lang', 'id', 'macdinhwebsite', 1);
if ($_lang == '') {
    $_lang = $db->getNameFromID('tbl_lang', 'id', 'macdinhwebsite', 0);
}
if (!isset($_SESSION['_lang'])) {
    $_SESSION['_lang'] = $_lang;
} else {
    if ($db->getNameFromID('tbl_lang', 'anhien', 'id', $_SESSION['_lang']) == 0) {
        $_SESSION['_lang'] = $_lang;
    }
}
//  // Hàm chuyển đổi những ký tự đặc biệt để khỏi lỗi XML
// function xml_entities($string) {
//     return str_replace(
//             array("&", "<", ">", '"', "'"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), $string
//     );
// }
function remove_empty_p( $content ){
    // clean up p tags around block elements
    $content = preg_replace( array(
        '#<p>\s*<(div|aside|section|article|header|footer)#',
        '#</(div|aside|section|article|header|footer)>\s*</p>#',
        '#</(div|aside|section|article|header|footer)>\s*<br ?/?>#',
        '#<(div|aside|section|article|header|footer)(.*?)>\s*</p>#',
        '#<p>\s*</(div|aside|section|article|header|footer)#',
    ), array(
        '<$1',
        '</$1>',
        '</$1>',
        '<$1$2>',
        '</$1',
    ), $content );
    return preg_replace('#<p>(\s| )*+(<br\s*/*>)*(\s| )*</p>#i', '', $content);
}

 // Hàm chuyển đổi những ký tự đặc biệt để khỏi lỗi XML
function xml_entities($string) {
    $string = remove_empty_p($string);
    // $data = preg_replace("</p>", "<br />", $data);
    $string =  str_replace(
            array("&"), array("&amp;"), $string
            // array("&", "<", ">", '"', "'"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), $string
    );
    return $string;
}

function catSoTuCanLay($bien, $sotu)
{
    if ($bien != '') {
        $bien = trim($bien);
        $bienarr = explode(" ", $bien);
        $chuoinoi = '...';
        if(count($bienarr)<$sotu){
            $sotu = count($bienarr);
            $chuoinoi = '';
        }
        $chuoireturn = '';
        for($i = 0; $i<=$sotu; $i++) {
            if($i>0){
                $chuoireturn.= " ";
            }
            $chuoireturn.= $bienarr[$i];
        }
    }
    // $chuoireturn = trim($chuoireturn);
    return $chuoireturn.$chuoinoi;
}


$snoi = '';
if($_GET['cat']!=''){
    $snoi = " and a.idtype like '%" . $__idtype_danhmuc . "%' ";
}else{
    $snoi = " and (colvalue like '%home%' OR colvalue like '%noibat%') and colvalue != '' ";
}
$s_noidung = "select a.id,a.hinh,masp,gia,giagoc,a.idtype,a.ngay,ten,url,link,target,mota,noidung
            FROM tbl_noidung AS a
            INNER JOIN tbl_noidung_lang AS b
            ON a.id = b.idnoidung
            where a.anhien = 1
            and b.idlang = '" . $_SESSION['_lang'] . "'
            ".$snoi."
            order by thutu Asc";
$d_noidung = $db->rawQuery($s_noidung);
$data_rss = '';
$idmodules = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'home'");
// echo $s_noidung;
// exit();
$iddanhmuc   = $db->getNameFromID("tbl_danhmuc", "id", "idtype", "'" . $idmodules . "'");
$tieudesite = $db->getNameFromID("tbl_danhmuc_lang", "tieude", "iddanhmuc", "'" . $iddanhmuc . "' and idlang = '" . $_SESSION['_lang'] . "' ");
$motasite = $db->getNameFromID("tbl_danhmuc_lang", "motatukhoa", "iddanhmuc", "'" . $iddanhmuc . "' and idlang = '" . $_SESSION['_lang'] . "' ");
$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
$data_rss .='<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    >
    <channel>
        <title>'.$tieudesite.'</title>
        <atom:link href="'.$host.'feed" rel="self" type="application/rss+xml" />
        <link>'.$host.'</link>
        <description>'.$motasite.'</description>
        <lastBuildDate>'.$db->getDateTimes().' +0000</lastBuildDate>
        <language>vi</language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <generator>Website</generator>';
if(count($d_noidung)>0){
	foreach($d_noidung as $key_noidung => $info_noidung){
        $id      = xml_entities($info_noidung['id']);
		$idtype  = xml_entities($info_noidung['idtype']);
		$ten     = xml_entities($info_noidung['ten']);
        $mota    = xml_entities($info_noidung['mota']);
		if($mota==''){
			$mota = $ten;
		}
		$url     = $info_noidung['url'];
		$ngay    = xml_entities($info_noidung['ngay']);
		$noidung = xml_entities($info_noidung['noidung']);

        // $mota    = catSoTuCanLay($mota,40);
        $mota    = xml_entities($mota);
		$lienket = xml_entities($host.$url);
       $danhmucname = $db->getNameFromID("tbl_noidung_lang","ten","idnoidung","'".$idtype."'");
$data_rss.='
        <item>
            <title>'.$ten.'</title>
            <link>'.$lienket.'</link>
            <pubDate>'.$ngay.'</pubDate>
            <dc:creator><![CDATA['.explode(".",$_SERVER['HTTP_HOST'])[0].']]></dc:creator>
                    <category><![CDATA['.$danhmucname.']]></category>

            <guid isPermaLink="false">'.$lienket.'</guid>
            <description><![CDATA[<p>'.$mota.'  <a href="'.$lienket.'">Readmore</a></p>'.$noidung.'
    <p>The post <a rel="nofollow" href="'.$lienket.'">'.$ten.'</a> appeared first on <a rel="nofollow" href="'.$host.'">'.$_SERVER['HTTP_HOST'].'</a>.</p>
    ]]></description>
            <content:encoded><![CDATA['.$noidung.']]></content:encoded>
        </item>
        ';
	}
}
	$data_rss.='
    </channel>
</rss>';
echo $data_rss;