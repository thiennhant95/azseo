<?php
// set default lang admin
$__defaultlang = $db->getNameFromID("tbl_lang","id","macdinh",1);
if($__defaultlang=='')
{
   $__defaultlang = $db->getNameFromID("tbl_lang","id","macdinh",0);
}
if($_SESSION['__defaultlang']==NULL)
{
   $_SESSION['__defaultlang']=$__defaultlang;
}
//$__defaultlang = 3;
$_number_item = 1;
include_once("application/files/bien.php");
$__getop     = $_GET['op'];
$__getmethod = $_GET['method'];
$__post_task = $_POST['task'];
$__getid     = $_GET['id'];
$__postid    = $_POST['id'];
$__getaction = $_GET['action'];
$__getpage   = $_GET['page'];
$__getsort   = $_GET['sort'];
$__getidtype = $_GET['idtype'];
$__gettukhoa = $_GET['tukhoa'];
$__getanhien = $_GET['anhien'];
$__sortname  = $_GET['sortname'];
if($__getsort=='asc')
{
   $__sortcurent = 'desc';
}else
{
   $__sortcurent = 'asc';
}
// link page va link sort
$path_page = './?op='.$__getop.'&idtype='.$__getidtype.'&tukhoa='.$__gettukhoa.'&anhien='.$__getanhien.'&sort='.$__getsort.'&sortname='.$__sortname.'&page=';
$path_sort = './?op='.$__getop.'&idtype='.$__getidtype.'&tukhoa='.$__gettukhoa.'&anhien='.$__getanhien.'&page='.$__getpage.'&sort='.$__sortcurent;
// option cho de quy select
$option1 = '<option value="">Vui lòng chọn...</option>';
// luu session pagekhi vao trang
if($_SESSION['__limit']=='' or !isset($_SESSION['__limit']) or $_SESSION['__limit']>11)
{
   $_SESSION['__limit']= 2;
}


?>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Quản trị website</title>
<!-- CSS-mergre -->
<link rel="stylesheet" href="../application/templates/css/bootstrap-awesome-animate-hover.min.css">
<!-- library Jquery & Upgrade -->
<script type="text/javascript" src="application/templates/js/before_dropzon.js"></script>
<script type="text/javascript" src="../application/templates/js/jquery-lib-webso.min.js"></script>
<script type="text/javascript" src="application/templates/js/load.js"></script>
<script type="text/javascript" src="../application/templates/js/bootstrap-carousel-sweetalert.min.js"></script>
<!-- Dropzon -->
<link rel="stylesheet" href="plugin/dropzon/css/dropzone.css" />
<script type="text/javascript" src="plugin/dropzon/js/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="application/templates/css/css.css" />
<link rel="stylesheet" type="text/css" href="application/templates/css/cssv2.css" />
<!-- script -->
<script type="application/javascript" src="application/templates/js/javascript.js" ></script>
<script type="application/javascript" src="application/templates/js/ajax.js" ></script>
<!-- sort table -->
<link rel="stylesheet" href="plugin/sorttable/jquery-ui.css">
<script src="plugin/sorttable/jquery-ui.js"></script>
<!-- menu -->
<link rel="stylesheet" type="text/css" href="plugin/cssmenu/styles.css" />
<script type="application/javascript" src="plugin/cssmenu/script.js" ></script>
<!-- menu -->
<link rel="stylesheet" type="text/css" href="plugin/datetimepicker/bootstrap-datepicker3.min.css" />
<script src="plugin/datetimepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="application/javascript" src="plugin/datetimepicker/bootstrap-datepicker.vi.min.js" ></script>
<!-- table scroll header -->
<script type="application/javascript" src="application/templates/js/jquery.fixedtableheader.min.js" ></script>
<!-- ckeditor -->
<script type="text/javascript" language="javascript" src="plugin/fckeditor/ckeditor/ckeditor.js" ></script>
<!-- jscolor -->
<script src="plugin/jscolor/jscolor.js" type="text/javascript"></script>


<style>
/* latin-ext */
body{font-family: 'Ropa Sans', sans-serif; color:#666; font-size:14px; color:#333; background-color:#666666;}
li,ul,body,input{margin:0; padding:0; list-style:none}
#login-form{width:400px; background:#FFF;  margin:0 auto; margin-top:70px; background:#f8f8f8; overflow:hidden; border-radius:7px}
.form-header{display:table; clear:both}
.form-header label{display:block; cursor:pointer; z-index:999}
.form-header li{margin:0; line-height:60px; width:200px; text-align:center; background:#eee; font-size:18px; float:left; transition:all 600ms ease}

/*sectiop*/
.section-out{width:800px; float:left; transition:all 600ms ease}
.section-out:after{content:''; clear:both; display:table}
.section-out section{width:400px; float:left}

.login{padding:20px}
.ul-list{clear:both; display:table; width:100%}
.ul-list:after{content:''; clear:both; display:table}
.ul-list li{ margin:0 auto; margin-bottom:12px}
.input{background:#fff; transition:all 800ms; width:310px; border-radius:3px 0 0 3px; font-family: 'Ropa Sans', sans-serif; border:solid 1px #ccc; border-right:none; outline:none; color:#999; height:40px; line-height:40px; display:inline-block; padding-left:10px; font-size:16px}
.input,.login span.icon{vertical-align:top}
.login span.icon{width:50px; transition:all 800ms; text-align:center; color:#999; height:40px; border-radius:0 3px 3px 0; background:#e8e8e8; height:40px; line-height:40px; display:inline-block; border:solid 1px #ccc; border-left:none; font-size:16px}
.input:focus:invalid{border-color:red}
.input:focus:invalid+.icon{border-color:red}
.input:focus:valid{border-color:green}
.input:focus:valid+.icon{border-color:green}
#check,#check1{top:1px; position:relative}
.btn{border:none; outline:none; background:#0099CC; border-bottom:solid 4px #006699; font-family: 'Ropa Sans', sans-serif; margin:0 auto; display:block; height:40px; width:100%; padding:0 10px; border-radius:3px; font-size:16px; color:#FFF}

.social-login{padding:15px 20px; background:#f1f1f1; border-top:solid 2px #e8e8e8; text-align:right}
.social-login a{display:inline-block; height:35px; text-align:center; line-height:35px; width:35px; margin:0 3px; text-decoration:none; color:#FFFFFF}
.form a i.fa{line-height:35px}
.fb{background:#305891} .tw{background:#2ca8d2} .gp{background:#ce4d39} .in{background:#006699}
.remember{width:50%; display:inline-block; clear:both; font-size:14px}
.remember:nth-child(2){text-align:right}
.remember a{text-decoration:none; color:#666}

.hide{display:none}

/*swich form*/
#signup:checked~.section-out{margin-left:-400px}
#login:checked~.section-out{margin-left:0px}
#login:checked~div .form-header li:nth-child(1),#signup:checked~div .form-header li:nth-child(2){background:#e8e8e8}
</style>

</head>
<body>
<?php
// Gui lai mat khau
if($_GET['checkrepass']==1)
{
  $email = trim($_POST['email']);
  if($_POST['captcha']!=$_SESSION['captcha_code']){
    echo'<script language="javascript">alert("'.$arraybien['saimacaptcha'].'"); location.href="./login.php";</script>';
    exit();
  }

  if($email != '')
  {
    $id_usercheck = $db->getNameFromID("tbl_user","id","email","'".$email."'");
    if($id_usercheck>0)
    {
      // insert ma active
      $activekey = md5(rand(1000,9999));
      $array_update = array("maactive"  => $activekey);
      $id_update = $db->sqlUpdate("tbl_user",$array_update," id = $id_usercheck ");

      // if UPdate thanh cong
       echo'<script language="javascript">alert("'.$arraybien['guimatkhauthanhcong'].'"); location.href="./login.php";</script>';

      $link_doimatkhau = 'http://'.$_SERVER['HTTP_HOST'].'/adminweb/resetpass.php?resetpass=1&id='.$id_usercheck.'&code='.$activekey;
       // Gửi mật khẩu

      $subject  = 'Đổi mật khẩu: '.$_SERVER['HTTP_HOST'];
      $message  = 'Xin kính chào Quý Khách,<br /><br />

    Bạn hoặc ai đó xác nhận rằng bạn (Tài khoản: '.$email.') quên mật khẩu tại http://'.$_SERVER['HTTP_HOST'].'/adminweb/<br /><br />

        . Nếu không phải từ bạn, xin vui lòng bỏ qua email này.<br />
        . Nếu đúng là từ bạn, nhấn vào '.$link_doimatkhau.' để tạo lại mật khẩu. Bạn cũng có thể copy/paste '.$link_doimatkhau.' vào trình duyệt web.
<br /><br />
Trân trọng,<br />
Web Số Support
';

      include("../plugin/mail/gmail/class.phpmailer.php");
      include ("../plugin/mail/gmail/class.smtp.php");
      // hot mail
      $array_sendmail = array("emailnhan"   => $email,
                              "emailgui"    => $_getArrayconfig['hostmail_user'],
                              "hostmail"    => $_getArrayconfig['hostmail'],
                              "user"        => $_getArrayconfig['hostmail_user'],
                              "pass"        => $_getArrayconfig['hostmail_pass'],
                              "tieude"      => $subject,
                              "fullname"    => $_getArrayconfig['hostmail_fullname'],
                              "port"        => 25,
                              "ssl"         => 0,
                              "subject"     => $subject,
                              "message"     => $message
                              );
      sendmail($array_sendmail);




      exit();
    }else{
      echo'<script language="javascript">alert("'.$arraybien['emailkhongtontaitronghethong'].'"); location.href="./login.php";</script>';
      exit();

    }
  }
}

// đổi mật khẩu và xóa mã active đi
if($_GET['changepass']==1){

  $_getid   = $_GET['id'];
  $_getcode = $_GET['code'];
  $s_check = 'select lat from tbl_user where id = '.$_getid.' and maactive = "'.$_getcode.'" and maactive != "" ';
  $d_check  = $db->sqlSelectSql($s_check);
  $matkhau  = $_POST['matkhau'];
  if(count($d_check)>0){
      $lat = $d_check[0]['lat'];
      // doi lai mat khau
      $matkhaureset = md5(md5($matkhau).$lat);
      $array_updatemk = array("matkhau" => $matkhaureset,
                              "maactive"=> "");
      $db->sqlUpdate("tbl_user",$array_updatemk," id = $_getid and maactive = '$_getcode' ");
      echo'<script>alert("Đổi mật khẩu thành công!"); location.href="./login.php"; </script>';

  }else{
      echo'<script>alert("Link đổi mật khẩu không đúng hoặc đã hết hạn sử dụng! Vui lòng nhấn vào tab quên mật khẩu và gửi lại yêu cầu đổi mật khẩu"); location.href="./login.php"; </script>';
  }

}

if($_GET['resetpass']==1){
  // Form cập nhật mật khẩu
  $_getid   = $_GET['id'];
  $_getcode = $_GET['code'];
  $s_check = 'select lat from tbl_user where id = '.$_getid.' and maactive = "'.$_getcode.'" and maactive != "" ';
  $d_check  = $db->sqlSelectSql($s_check);
  if(count($d_check)>0){
    // neu dung thong tin link doi mat khau thi reset lại mật khẩu theo form sau cho khách hàng
  ?>
  <script language="javascript">
  function check_newpass(){
    if(document.getElementById('matkhau').value!=document.getElementById('re_matkhau').value){
      alert("Mật khẩu nhập lại không trùng khớp!");
      return false;
    }else{ return true;}
  }
  </script>
  <div id="login-form">

  <input type="radio" checked id="login" name="switch" class="hide">
  <input type="radio" id="signup" name="switch" class="hide">

  <div class="well well-sm" style="text-align:  center; font-weight: bold;">
    <i class="fa fa-lock"></i> ĐỔI MẬT KHẨU<label for="login">
  </div>

  <div class="section-out">
  <section class="login-section">
  <div class="login">
  <form action="./resetpass.php?changepass=1&id=<?php echo $_getid; ?>&code=<?php echo $_getcode; ?>" method="post">
  <ul class="ul-list">
  <li><input name="matkhau" id="matkhau" type="password" required class="input" placeholder="Mật khẩu mới"/><span class="icon"><i class="fa fa-lock"></i></span></li>
  <li><input name="re_matkhau" id="re_matkhau" type="password" required class="input" placeholder="Nhập lại mật khẩu mới"/><span class="icon"><i class="fa fa-lock"></i></span></li>
  <li><input type="submit" value="ĐỔI MẬT KHẨU" class="btn" onclick="return check_newpass();"></li>
  </ul>
  </form>
  </section>
  </div>

  </div>

  <?php
  }else{
      echo'<script>alert("Link đổi mật khẩu không đúng hoặc đã hết hạn sử dụng! Vui lòng nhấn vào tab quên mật khẩu và gửi lại yêu cầu đổi mật khẩu"); location.href="./login.php"; </script>';

  }
} // end reset pass form

?>
</body>
</html>