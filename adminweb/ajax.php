<?php

    require dirname(__DIR__).'/configs/inc.php';

include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage       = new SimpleImage();
$ResizeImage_thumb = new SimpleImage();

if (!function_exists('ws_get') || !function_exists('ws_post')) {
    exit('File ws_function.php not found !');
}
if (count($db->checkLogin($_SESSION['tendangnhap'], $_SESSION['matkhau'])) == 0) {
    exit('Ban khong co quyen truy cap vao khu vuc nay');
}

$id        = ws_get('id');
$idnoidung = ws_get('idnoidung');
$name      = ws_get('name');
$value     = ws_get('value');
$op        = ws_get('op');
$act       = ws_get('act');
$urlcu     = trim(ws_get('urlcu'));
$title     = trim(ws_get('title'));
$title_url = tao_url($title);
$title_url = strtolower($title_url);
$rannumber = rand(100, 999);
$type      = ws_get('type');
$table     = ws_get('table');
$urlreturn = '';
@$__getop   = $__getop ? $__getop : ws_get('op');

$data       = '';
$dataupdate = '';

if (!empty($op)) {
    include 'modules/'.$op.'/'.$op.'_ajax.php';
} else {
    exit('op not defined.');
}
