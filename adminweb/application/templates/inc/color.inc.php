<?php

/*
#
# INSERT_COLOR
# --------------------------------------------------------------------------
if (isset($_POST['chonmausac']) && !empty($_POST['chonmausac'])) {
    $inserBG = ws_post('bg_danhmuc');
    $inserCL = ws_post('color_danhmuc');
    $arr_bg = array(
        'bgcolor' => $inserBG,
        'color'   => $inserCL
    );
    $db->sqlUpdate($__table, $arr_bg, "id = '$subid' ");
}


#
# UPDATE_COLOR
# --------------------------------------------------------------------------
if (isset($_POST['chonmausac'])) {
    if (!empty($_POST['chonmausac'])) {
        $arr_bg_update = array(
            'bgcolor' => ws_post('bg_danhmuc'),
            'color'   => ws_post('color_danhmuc')
        );
    } else {
        $arr_bg_update = array(
            'bgcolor' => null,
            'color'   => null,
        );
    }
    $db->sqlUpdate($__table, $arr_bg_update, "id = '{$__postid}'");
}
 */




$valColor = '';
if ($__config['mausac'] == 1) {
    $anhien_chonmau = empty($dBackground) ? ' style="display:none;" ' : none;
    $valColor .= '
        <div class="form-group mausacGroup">
            <div class="input-group">
                <div class="input-group-addon">' . $arraybien['mausac'] . '</div>
                <div class="group-active">
                    <label for="kochon">
                        <input type="radio" id="kochon" name="chonmausac" value="0" ' . (empty($dBackground) ? 'checked="checked"' : null) . ' />
                        &nbsp;Không
                    </label>
                    <label for="cochon">
                        <input type="radio" id="cochon" name="chonmausac" value="1" ' . (!empty($dBackground) ? 'checked="checked"' : null) . ' />
                        &nbsp;Có
                    </label>
                </div>
            </div>
            <div class="form-group"></div>
            <div id="anhien_chonmau" ' . $anhien_chonmau . '>
                <input type="text" class="form-control jscolor" name="bg_danhmuc" value="' . $dBackground . '" />
                <input type="hidden" class="form-control" name="color_danhmuc" value="' . $dColor . '" />
            </div>
         </div><!-- ./mausacGroup -->';
}
return $valColor;
