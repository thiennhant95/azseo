<?php
/*

QUERY

#
# INSERT ICON
# --------------------------------------------------------------------------
// B1. Neu la chon san
$kieuIcon = ws_post('type_icon');
if ($kieuIcon == 1 && $kieuIcon != -1) {
    $iconName = ws_post('icon_faicon');
} else if ($kieuIcon == 0 && $kieuIcon != -1) {
    $iconName = $_FILES['icon_img']['name'];
    if (!empty($iconName)) {
        $tenicon =  $images_name ."-". $iconName;
        move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img']. $tenicon);
        $iconName = $tenicon;
    }
} else {
    $iconName = '';
}



#
# UPDATE ICON
# --------------------------------------------------------------------------
$kieuIcon = ws_post('type_icon');
$iconOld  = ws_post('iconname');
if ($kieuIcon == 1 && $kieuIcon != -1) {
    // xoa icon cũ
    if (file_exists($__config['path_img'].$iconOld)) {
        unlink($__config['path_img'].$iconOld);
    }
    // Trường hợp update là fa-icon
    $iconName = ws_post('icon_faicon');
    $db->sqlUpdate($__table, array("icon" => $iconName), "id = '{$__postid}'");
} else if ($kieuIcon == 0 && $kieuIcon != -1) {
    $iconName = $_FILES['icon_img']['name'];
    if (!empty($iconName)) {
        // 1. update icon new
        // xoa icon cũ
        if (file_exists($__config['path_img'].$iconOld)) {
            unlink($__config['path_img'].$iconOld);
        }
        $tenicon =  $images_name ."-". $iconName;
        move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img']. $tenicon);
        $iconName = $tenicon;
        $db->sqlUpdate($__table, array("icon" => $iconName), "id = '{$__postid}'");
    }
} else {
    $iconName = '';
    // 4. doi tu img -> khong (xoa icon)
    // xoa icon cũ
    if (file_exists($__config['path_img'].$iconOld)) {
        unlink($__config['path_img'].$iconOld);
    }
    $db->sqlUpdate($__table, array("icon" => null), "id = '{$__postid}'");
}
// 2. check xoa icon
if (isset($_POST['xoaicon']) && $_POST['xoaicon'] == 1) {
    if (file_exists($__config['path_img'].$iconOld)) {
        unlink($__config['path_img'].$iconOld);
    }
    $db->sqlUpdate($__table, array("icon" => null), "id = '{$__postid}'");
}

 */
$valIcon = '';
if ($__config['icon']) {

#====== BLOCK ICON IMAGES =================
$arrModImg = array("adminloai");
if (in_array($__getop, $arrModImg)) {
    $blockImg = true;
} else {
    $blockImg = false;
}

#====== BLOCK ICON FAICON =================
$arrModFA = array();
if (in_array($__getop, $arrModFA)) {
    $blockFa = true;
} else {
    $blockFa = false;
}


#============== TAO FOLDER ===============
create_dir($__config['path_img'], false);


// ICON
// --------------------------------------------------------------------------
$iconChonsan = substr($icon, 3, 3) == 'fa-' ? 'checked' : null;
$iconUpload  = substr($icon, -4, 1) == '.' ? 'checked' : null;
$iconMacdinh = empty($icon) ? 'checked' : null;
$valIcon .= '
        <div class="form-group iconGroup">
            <div class="input-group">
                <div class="input-group-addon">' . $arraybien['kieuicon'] . '</div>
                <div class="group-active">
                    <label>
                        <input type="radio" name="type_icon" value="-1" ' . $iconMacdinh . '>
                        ' . $arraybien['khong'] . '
                    </label>';

                    if (!$blockFa) {
                        $valIcon.='

                        <label>
                            <input type="radio" name="type_icon" value="1" ' . $iconChonsan . '>
                            ' . $arraybien['chonsan'] . '
                        </label>';

                    }



                    if (!$blockImg) {
                        $valIcon.='

                            <label>
                                <input type="radio" name="type_icon" value="0" ' . $iconUpload . '>
                                ' . $arraybien['upload'] . '
                            </label>

                        ';
                    }

                    $valIcon.='
                </div><!-- ./group-active -->
            </div>
        </div>
        <!-- ./iconGroup -->';

        if (!$blockImg) {
            $valIcon.='
                <div class="form-group" id="icon-upload">
                 <div class="input-group">
                    <div class="input-group-addon">' . $arraybien['icon'] . '</div>
                    <input type="file" class="form-control" name="icon_img" id="icon" />
                    <input class="form-control" type="hidden" name="icon_post" value="' . $icon . '" />
                </div>
                <!-- ./icon-upload -->';

                // VIEW ICON IMAGE && CHECKBOX DEL ICON
                if (!empty($icon)) {
                    $valIcon .= '
                        <input type="hidden" name="iconname" id="iconname" value="' . $icon . '" />
                        <label>
                            <img src="' . $__config['path_img'] .$icon. '" onerror="xulyloi(this)" class="img-responsive"  border="0" width="45px" />
                            <input type="checkbox" class="input-md" name="xoaicon" id="xoaicon" value="1" />&nbsp;' . $arraybien['xoa'] . '
                        </label>
                           ';
                }

                $valIcon .= '
                  </div>
                  <!-- ./icon-upload -->
               ';
       }// $blockImg


    // TRUONG HOP ICON LA CHON FA-ICON
    $s_fa = 'SELECT * FROM tbl_faicon WHERE anhien = 1 ORDER BY ten ASC';
    $d_fa = $db->sqlSelectSql($s_fa);
    if (count($d_fa) > 0 && !$blockFa) {
        $valIcon .= '
        <div class="form-group" id="icon-select">
            <div class="input-group iconchonsanGroup">
            <div class="input-group-addon">' . $arraybien['icon'] . '</div>

                <select name="icon_faicon" id="wsIcon" class="selectpicker form-control" data-live-search="true">

                ';
        foreach ($d_fa as $key_fa => $info_fa) {
            $checkfa = ($info_fa['bieutuong'] == $data['icon']) ? 'selected' : null;

            $valIcon.='
                <option
                    data-tokens="' . $info_fa['bieutuong'] . '"
                    value="' . $info_fa['bieutuong'] . '"
                    data-icon="' . $info_fa['bieutuong'] . ' fa-lg"
                    '.($icon==$info_fa['bieutuong']?'selected':null).'
                    >
                </option>
            ';
        }
        $valIcon .= '

            </select>

         </div><!-- ./iconchonsanGroup -->
      </div>';
    }
}
return $valIcon;
