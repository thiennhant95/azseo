if ($.isFunction($.fn.swfupload)) {
    $(function() {
        $('#swfupload-control').swfupload({
                upload_url: "plugin/jqueryupload/upload-file.php",
                file_post_name: 'uploadfile',
                file_size_limit: "1024",
                file_types: "*.jpg;*.png;*.gif",
                file_types_description: "Image files",
                file_upload_limit: 20,
                flash_url: "plugin/jqueryupload/js/swfupload/swfupload.swf",
                button_image_url: 'plugin/jqueryupload/js/swfupload/wdp_buttons_upload_114x29.png',
                button_width: 114,
                button_height: 29,
                button_placeholder: $('#button')[0],
                debug: false
            })
            .bind('fileQueued', function(event, file) {
                var listitem = '<li id="' + file.id + '" >' +
                    'File: <em>' + file.name + '</em> (' + Math.round(file.size / 1024) + ' KB) <span class="progressvalue" ></span>' +
                    '<div class="progressbar" ><div class="progress" ></div></div>' +
                    '<p class="status" >Pending</p>' +
                    '<span class="cancel" >&nbsp;</span>' +
                    '</li>';
                $('#log').append(listitem);
                $('li#' + file.id + ' .cancel').bind('click', function() {
                    var swfu = $.swfupload.getInstance('#swfupload-control');
                    swfu.cancelUpload(file.id);
                    $('li#' + file.id).slideUp('fast');
                });
                // start the upload since it's queued
                $(this).swfupload('startUpload');
            })
            .bind('fileQueueError', function(event, file, errorCode, message) {
                alert('Size of the file ' + file.name + ' is greater than limit');
            })
            .bind('fileDialogComplete', function(event, numFilesSelected, numFilesQueued) {
                $('#queuestatus').text('Files Selected: ' + numFilesSelected + ' / Queued Files: ' + numFilesQueued);
            })
            .bind('uploadStart', function(event, file) {
                $('#log li#' + file.id).find('p.status').text('Uploading...');
                $('#log li#' + file.id).find('span.progressvalue').text('0%');
                $('#log li#' + file.id).find('span.cancel').hide();
            })
            .bind('uploadProgress', function(event, file, bytesLoaded) {
                //Show Progress
                var percentage = Math.round((bytesLoaded / file.size) * 100);
                $('#log li#' + file.id).find('div.progress').css('width', percentage + '%');
                $('#log li#' + file.id).find('span.progressvalue').text(percentage + '%');
            })
            .bind('uploadSuccess', function(event, file, serverData) {
                var item = $('#log li#' + file.id);
                item.find('div.progress').css('width', '100%');
                item.find('span.progressvalue').text('100%');
                var pathtofile = '<a href="plugin/jqueryupload/uploads/' + file.name + '" target="_blank" >view &raquo;</a>';
                item.addClass('success').find('p.status').html('Done!!! | ' + pathtofile);
            })
            .bind('uploadComplete', function(event, file) {
                // upload has completed, try the next one in the queue
                $(this).swfupload('startUpload');
            })
    });

    $(function() {
        $("#sortable tbody").sortable({
            update: function(event, ui) {
                var order = $(this).sortable('serialize');
                $.ajax({
                    data: order,
                    type: 'POST',
                    url: 'modules/danhmuc/danhmuc_ajax.php'
                });
            }
        }).disableSelection();
    });
}
