// JavaScript Document
function loading_show() {
    $('#loading').html("<img src='application/templates/images/loading.gif'/>").fadeIn('fast');
}

function loading_hide() {
    $('#loading').fadeOut('fast');
}

function bootstrap_alert(message, alerttype) {
    $('#alert_placeholder').append('<div id="alertdiv" class="alert ' + alerttype + '" role="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    setTimeout(function() {
        $("#alertdiv").remove();
    }, 3000);
}
// Run function on load, could also run on dom ready
window.onload = function() {
    $('#box_doimatkhau').hide();
}

function showhidediv(div) {
    if (document.getElementById(div).style.display == "block") {
        $("#" + div).hide(200);
    } else {
        $(".inputalt").hide(200);
        $("#" + div).show(300);
    }
}
$(document).ready(function() {

    // PASS HOT MAIL
    $(document).on('click', '#viewHostmail', function(event) {
        event.preventDefault();
        var _inputHostmail = $('#hostmail_pass').find("input[name='hostmail_pass']");
        if (_inputHostmail.attr('type') == 'password') {
            _inputHostmail.attr('type', 'text');
        } else {
            _inputHostmail.attr('type', 'password');
        }
        if ($(this).children('i').hasClass('fa-eye')) {
            $(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            $(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });

    $(document).on('click', '#testMail', function(event) {
        event.preventDefault();
        if (confirm('VUI LÒNG LƯU CẤU HÌNH TRƯỚC KHI TEST MAIL.')) {
            $(this).button("loading");

            $.getJSON('ajax.php?op=cauhinh&name=testmail', {
            }, function(json, textStatus) {
                if (json.result) {
                    $('#testMail').button("reset");
                    swal('Gửi mail thành công.', '', 'success');
                } else {
                    $('#testMail').button("reset");
                    swal('Gửi mail thất bại, vui lòng kiểm tra cấu hình.', '', 'error');
                }

            });
        }
    });

    $(document).on('click', '.pending', function() {
        event.preventDefault();
        swal('Hiện đang được phát triển', '', 'info');
        $(this).attr('disabled', 'disabled');

    });


    if ($.isFunction($.fn.sortable)) {

        /*sortable DM*/
        $('#keotha_dM').sortable({
            placeholder: 'tr-sortable-highlight',
            opacity: '0.8',
            cursor: 'move',
        });

        /*Sortable*/
        $('#keotha_row').sortable({
            placeholder: 'tr-sortable-highlight',
            axis: 'y',
            opacity: '0.8'
        });
        $("#keotha_row").on('sortupdate', function() {
            var thisSizeOf   = $('#keotha_row').find('tr').length;
            var this_op      = $('input[name=this_op]').val();
            var paged        = $("#paged").find('li.active > a').text();
            paged            = (paged != '' && paged > 0) ? paged : 1;
            var itemSelected = parseInt($("#limit :selected").text());
            itemSelected     = (isNaN(itemSelected)) ? thisSizeOf : itemSelected;
            var i            = (parseInt(paged) - 1) * itemSelected;
            i++;
            // Tinh so item bat dau cua trang
            $(".content_tr").each(function(index) {
                var id = $(this).data('id');
                $('.text_area[data-id=' + id + ']').text(i).val(i);
                $.get('./ajax.php', {
                    name: 'sapxepthutu',
                    id: id,
                    op: this_op,
                    thutu: i
                }, function() {});
                i++;
            });
            if ($.isFunction($.fn.notify)) {
                $.notify('Đã lưu lại thứ tự', {
                    globalPosition: 'top right',
                    className: 'success'
                });
            } else {
                console.error('Khong tim thay plugin notify \n[code: notify404] ');
            }
        });
    } else {
        console.error('Khong tim thay function sortable \n[code: sortable404]');
    }

    // TAT CẢ HÌNH ANH LOAI
    $("#tatcahinhanh-loai").on("change", function() {
        if ($(this).val().length) {
            $(".btnSave").removeClass("disabled");
        } else {
            $(".btnSave").addClass("disabled");
        }
    });
    $("input[name='chonmausac']").change(function() {
        var thisvalue = $(this).val();
        if (thisvalue == 0) {
            $("#anhien_chonmau").fadeOut();
        } else {
            $("#anhien_chonmau").fadeIn();
        }
    });
    // Hide Button xoa khi nhap so
    $('input#suasoluotxem').on('keyup', function(event) {
        event.preventDefault();
        if ($(this).val().length) {
            $('button#delview').addClass('disabled');
            $('button#delview').attr('disabled', 'disabled');
        } else {
            $('button#delview').removeClass('disabled');
            $('button#delview').removeAttr('disabled');
        }
    });
    // START QUICK SEARCH
    $.ajaxSetup({
        cache: false
    });
    $("#timnhanh-admin").on('keyup', function(event) {
        var tableSelected = $('#changselect').val();
        var urlsend = $(this).data('url') + 'adminweb/application/files/quick_search.php';
        event.preventDefault();
        $.get(urlsend, {
            keyword: $.trim($(this).val()),
            table: tableSelected,
        }, function(data) {
            /*optional stuff to do after success */
            $(".suggest ul").html(data).prepend('<div class="form-group"></div>');
        });
    });
    $("body, #row_top").click(function() {
        $("#timnhanh-admin").val(null);
        $(".suggest ul").children().remove();
    });
    // END QUICK SEARCH
    var type_icon = $("input:radio[name='type_icon']:checked");
    if (type_icon.val() == 1) {
        $("#icon-select").show();
        $("#icon-upload").hide();
    } else if (type_icon.val() == 0) {
        $("#icon-select").hide();
        $("#icon-upload").show();
    } else {
        $("#icon-select").hide();
        $("#icon-upload").hide();
    }
    $("input:radio[name='type_icon']").change(function() {
        if ($(this).val() == 1) {
            $("#icon-upload").hide();
            $("#icon-select").slideDown(400);
        } else if ($(this).val() == 0) {
            $("#icon-select").hide();
            $("#icon-upload").slideDown(400);
        } else {
            $("#icon-select").slideUp(400);
            $("#icon-upload").slideUp(400);
        }
    });
    $(".list-icon li").on("click", function() {
        $("input#chon_faicon").val($(this).data('value'));
        $("#result_faicon i").removeClass().addClass($(this).data('value'));
    });
    /* HUY thongbao_nhanh2 */
    $("#hide_thongbaonhanh2").click(function() {
        $.ajax({
            url: 'modules/home/home_ajax.php',
            type: 'POST',
            async: true,
            cache: false,
            data: {
                value: 1,
                action: 'cancel_thongbao2'
            },
            success: function(reThongbao2) {},
            error: function() {
                swal("Có lỗi xảy ra", "", "error");
            }
        });
    });
    /* HUY thongbao_nhanh1 */
    var yetVisited = sessionStorage['visited'];
    if (!yetVisited) {
        // open popup
        var thongbao1 = $('#thongbao_webso').html();
        $("#alert_box .for-user .content").html(thongbao1);
        setTimeout(function() {
            $("#alert_box .for-user").slideUp('slow', function() {
                $("#alert_box").remove();
                $("#thongbao_webso").remove();
            });
        }, 8000);
        $("#alert_box .for-user .close").click(function() {
            $("#alert_box").remove();
            $("#thongbao_webso").remove();
        });
        sessionStorage['visited'] = "yes";
    } else {
        $("#alert_box").remove();
        $("#thongbao_webso").remove();
    }
    /************ Send Feedback ************/
    $("#submit_feedback").click(function() {
        if ($("#feedback_frm input[name=hoten]").val().length == 0) {
            $("#feedback_frm input[name=hoten]").focus();
            swal('', 'Vui lòng điền họ tên', 'warning');
        } else if ($("#feedback_frm input[name=hoten]").val().length < 2) {
            $("#feedback_frm input[name=hoten]").focus();
            swal('', 'Họ tên phải trên 2 ký tự', 'warning');
        } else if ($("#feedback_frm textarea[name=noidung]").val().length == 0) {
            $("#feedback_frm textarea[name=noidung]").focus();
            swal('', 'Vui lòng nhập nội dung phản hồi', 'warning');
        } else if ($("#feedback_frm textarea[name=noidung]").val().length < 6) {
            $("#feedback_frm textarea[name=noidung]").focus();
            swal('', 'Nội dung cần rõ ràng.', 'warning');
        } else if ($("#feedback_frm input[name=captcha]").val().length == 0) {
            $("#feedback_frm input[name=captcha]").focus();
            swal('', 'Vui lòng nhập mã Captcha.', 'warning');
        } else if ($("#feedback_frm input[name=captcha]").val().length < 3) {
            $("#feedback_frm input[name=captcha]").focus();
            swal('', 'Captcha phải 3 ký tự.', 'warning');
        } else {
            var datacaptcha = $("#feedback_frm input[name=captcha]").val();
            // Check Captcha
            $.ajax({
                url: 'plugin/captcha/checkcaptcha.php',
                type: 'GET',
                async: true,
                cache: false,
                data: {
                    capt: datacaptcha
                },
                success: function(recaptcha) {
                    if (recaptcha == 1) {
                        // THuc hien ajax send
                        var send_hoten = $("#feedback_frm input[name=hoten]").val();
                        var send_dienthoai = $("#feedback_frm input[name=dienthoai]").val();
                        var send_email = $("#feedback_frm input[name=email]").val();
                        var send_noidung = $("#feedback_frm textarea[name=noidung]").val();
                        var send_op = $("#feedback_frm input[name=getop]").val();
                        var send_method = $("#feedback_frm input[name=getmethod]").val();
                        var send_lang = $("#feedback_frm input[name=getlang]").val();
                        var send_time = $("#feedback_frm input[name=getthoigian]").val();
                        $.ajax({
                            url: 'http://webso.vn/phanhoi.php',
                            type: 'POST',
                            async: true,
                            cache: false,
                            data: {
                                gui_hoten: send_hoten,
                                gui_dienthoai: send_dienthoai,
                                gui_email: send_email,
                                gui_noidung: send_noidung,
                                gui_op: send_op,
                                gui_method: send_method,
                                gui_lang: send_lang,
                                gui_time: send_time
                            },
                            success: function(rewebso) {
                                console.log('data', rewebso);
                                if (rewebso == 1) {
                                    swal('Gửi thành công', '', 'success');
                                    $("#myModal2").hide();
                                    $(".modal-backdrop.in").css('opacity', '0');
                                } else {
                                    swal('Gửi thất bại', '', 'error');
                                    $("#myModal2").hide();
                                    $(".modal-backdrop.in").css('opacity', '0');
                                }
                            },
                            error: function() {
                                swal("Có lỗi xảy ra", "", "error");
                            }
                        });
                    } else {
                        $("#feedback_frm input[name=captcha]").focus();
                        swal('', 'Mã captcha không đúng !', 'warning');
                    }
                }
            });
        }
    });
    /************ Get color Current ************/
    $('.color').change(function() {
        var thisColor = $(this).css('color');
        $("input[name=return_color]").val(thisColor);
        $("input[name=color_danhmuc]").val(thisColor);
    });
    /************ ########################### ************/
    if ($.isFunction($.fn.datepicker)) {
        $('.datepicker').datepicker({
            autoSize: true,
            defaultDate: +7,
            gotoCurrent: true,
            showButtonPanel: true
        });
    } else {
        console.error('Khong tim thay ham datepicker \n[code: datepicker404]');
    }
    /************ Dong/Mo thiet lap mail ************/
    if ($("input:radio[name=sendemail]:checked").val() == '0') {
        $("#body_setup_email").hide();
    } else {
        $("#body_setup_email").show();
    }
    $("input:radio[name=sendemail]").change(function() {
        var value = $("input:radio[name=sendemail]:checked").val();
        if (value == 1) {
            $("#body_setup_email").slideDown();
        } else {
            $("#body_setup_email").slideUp();
        }
    });
    /************ Dong/Mo thong bao dong website ************/
    if ($("input:radio[name=stop_website]:checked").val() == '0') {
        $("#thongbao_stop").hide();
    } else {
        $("#thongbao_stop").show();
    }
    $("input:radio[name=stop_website]").change(function() {
        var value = $("input:radio[name=stop_website]:checked").val();
        if (value == 1) {
            $("#thongbao_stop").slideDown();
        } else {
            $("#thongbao_stop").slideUp();
        }
    });
    /************ Validate Background Website ************/
    if ($("#frm-bgwebsite input[name='mota']").val() == "") {
        $("a.btn-save").addClass('disabled');
    } else {
        $("a.btn-save").removeClass('disabled');
    }
    $("#frm-bgwebsite input[name='mota']").keyup(function() {
        if ($(this).val().length >= 4) {
            $("a.btn-save").removeClass('disabled');
            $(this).parent().removeClass('has-error').addClass('has-success');
        } else {
            $("a.btn-save").addClass('disabled');
            $(this).parent().removeClass('has-success').addClass('has-error');
        }
    });
    /************ An ChonHinh Di khi Check ko hinh ************/
    if ($("input[name=xemhinh]:checked").val() == 1) {
        $("#urlfile").show();
    } else {
        $("#urlfile").hide();
    }
    $("input[name='xemhinh']").change(function(event) {
        if ($(this).val() == 1) {
            $("#urlfile").hide().slideDown(400);
        } else {
            $("#urlfile").show().slideUp(400);
        }
    });
    /************ An ChonNhom Di khi User dang la SupperAdmin ************/
    if ($("#changechonquyen").val() > 0) {
        $("#box-chonnhom").hide();
    } else {
        $("#box-chonnhom").show();
    }
    $("#changechonquyen").change(function() {
            if ($(this).val() > 0) {
                $("#box-chonnhom").slideUp(400);
            } else {
                $("#box-chonnhom").slideDown(400);
            }
        })
        /*********************** PERMISSION CHECK ALL **********************/
    $("#checkAllPer").change(function() {
        $("#list_checkPer input:checkbox").prop('checked', $(this).prop("checked"));
    });
    /*********************** VALIDATE THONG TIN DANG KY **********************/
    // HIEN THI DOI MAT KHAU
    $('#click_matkhau').click(function() {
        $("#box_doimatkhau").slideToggle(this.checked);
    });
    // show tab trong frm
    $('#tabs > div.tab-text > div').hide();
    $('#tabs > div.tab-text > div:first').show();
    $('#tabs ul li:first').addClass('active');
    $('#tabs ul li').click(function() {
        $('#tabs ul li').removeClass('active');
        $(this).addClass('active');
        var currentTab = $(this).attr('type');
        $('#tabs > div.tab-text > div').hide();
        $(currentTab).show();
        return false;
    });
    $('#tabs > #frm-register > div').show();
    //Go top
    var widthscroll = $("#row_top").innerHeight() + $("#row_menu").innerHeight() + 60;
    $(window).scroll(function() {
        if ($(this).scrollTop() > widthscroll) {
            $(".row_content_top").addClass("fixed");
        } else {
            $(".row_content_top").removeClass("fixed");
        }
    });
    // fix table th
    if ($.isFunction($.fn.fixedtableheader)) {
        $('.adminlist').fixedtableheader();
    } else {
        console.error('Khong tim thay ham fixedtableheader \n[code: fixedtableheader404]');
    }
});
