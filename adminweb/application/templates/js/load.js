$(function() {
    anhiendiv = function( target ) {
        $this = $(window.event.target);
        $this.parent('.head-check').next('.body-check').children('[id^=tap]').addClass('hide');
        $(target).removeClass('hide');
    }
});
$(function() {
    $('.option-txt > label').on('click', function() {
        $thjs = $(this),
        $id = $thjs.children("input[name^='optionlang']:checked").data('id');
        $('#'+$id).parent().find('input[type=text]').hide();
        $('#'+$id).show();
    });
});
$(function() {
    $('#lichsuTichdiem,#thongkeTichdiem').DataTable( {
        "language": {
            "url": "../plugins/data-table/Vietnamese.json"
        }
    } );
});
$(function() {
    $(".list-item").find(".edit").on('click', function() {
        var $this = $(this);
        $('#contentCol').find('.content').hide();
        $('#contentCol').find('.edit').show();
        $this.hide();
        $this.closest('li').children('.content').show();
    });
    $("button.cancel").on('click', function() {
        var $this = $(this);
        $this.closest('.content').prev('.item-view').show();
        $this.closest('.content').hide();
        $this.closest('.content').prev('.item-view').find('.edit').show();
    });
});
var select2opt = {
    amdLanguageBase: "./i18n/vi.js",
    language: "vi",
    containerCssClass: "form-control-select2"
}
$(function(){
    if ( jQuery().select2 ) {
        $('select.select2').select2(select2opt);
    }
});
$(function() {
    $("#inlogonoidung").click(function() {
        if( $(this).prop('checked') ) {
            $('#panelLogo').slideDown();
        }
        else {
            $('#panelLogo').slideUp();
        }
    });
});
function check_and_show( t, id ){
    if( $(t).prop('checked') ) {
        $('#'+id).removeAttr('disabled');
    }
    else {
        $('#'+id).attr('disabled', 'disabled');
    }
}
// JavaScript Document
function loading_show() {
	$('#loading').html("<img src='application/templates/images/loading.gif'/>").fadeIn('fast');
}
function loading_hide() {
	$('#loading').fadeOut('fast');
}
function bootstrap_alert(message, alerttype) {
	$('#alert_placeholder').append('<div id="alertdiv" class="alert ' + alerttype + '" role="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
	setTimeout(function () {
		$("#alertdiv").remove();
	}, 3000);
}
// Run function on load, could also run on dom ready
window.onload = function () {
	$('#box_doimatkhau').hide();
};
function showhidediv(div) {
	if (document.getElementById(div).style.display == "block") {
		$("#" + div).hide(200);
	} else {
		$(".inputalt").hide(200);
		$("#" + div).show(300);
	}
}
$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
	if ($.isFunction($.fn.selectpicker)) {
		$('.selectpicker').selectpicker({
			iconBase: 'fa',
			tickIcon: 'fa-check'
		});
	}
	// Schedule
	$('input[name=schedule]').on('change', function(){
		console.log(
			$(this).val()
		);
		// disable
		if( $(this).val() != 1 ){
			if( $('#schedule_group').length ){
				$('#schedule_group').hide()
			}else{
				$('input[name=schedule_date]').hide();
			}
		}else{
		// Enable
			if( $('#schedule_group').length ){
				$('#schedule_group').show()
			}else{
				$('input[name=schedule_date]').show();
			}
		}
	});
	/*VITRIHIENTHI*/
	$('.vitrihienthi_change').on('changed.bs.select', function () {
		var _mess = '';
		var _lat = '';
		var _val = ($(this).val() != null) ? $(this).val().join(",") : $(this).val();
		$.getJSON('ajax.php', {
			id: $(this).data('getid'),
			name: 'vitrihienthi',
			op: 'layout',
			value: _val
		}, function (data) {
			if (data.error != 'success') {
				_mess = 'Không thể cập nhật !';
				_lat = 'alert-danger';
			} else {
				_mess = 'Thành công !';
				_lat = 'alert-success';
			}
			$('body').append('<div id="hn-notifi" class="alert ' + _lat + '" style="position:fixed;bottom:0;right:0;z-index:9999;">' + _mess + '</div>');
			setTimeout(function () {
				$('body').find('#hn-notifi').remove();
			}, 1200);
		});
	});
	function makeid() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 5; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	function locdau(str) {
		str = str.toLowerCase();
		str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
		str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
		str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
		str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
		str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
		str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
		str = str.replace(/đ/g, "d");
		str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
		str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
		str = str.replace(/^\-+|\-+$/g, ""); //cắt bỏ ký tự - ở đầu và cuối chuỗi
		return str;
	}
	$('.keyduan').live('change', function () {
		$(this).val(locdau($(this).val().split(" ").join("")));
	});
	$(document).on('click', '.btnAdd', function (event) {
		event.preventDefault();
		var dataClone = $('.duan-box').children('.panel-duan').last().clone();
		var _randID = makeid();
		$('.duan-box').append(dataClone);
		// RESET DATA
		$('.duan-box').children('.panel').last().find('input').val('');
		// Remove CKeditor
		$(this).prev('.duan-box').children('.panel').last().find("textarea").next('.cke').remove();
		$(this).prev('.duan-box').children('.panel').last().find("textarea").removeClass('ckeditor').removeAttr('id').attr('id', _randID)
		CKEDITOR.replace(_randID);
		// Show button remove duan if 2 item child
		if ($('.duan-box').children('.panel-info').length > 1) {
			$('.duan-box > .panel-info').find('.remove-duan').show();
		}
		$('.remove-duan').children('i').show('fast');
		$('.remove-duan').children('span').hide('fast');
		// Toll
		$('[data-toggle="tooltip"]').tooltip();
	});
	$('.remove-duan').live('click', function (event) {
		$(this).children('i').hide('slow');
		$(this).children('span').show('slow');
	});
	$('.remove-duan > span').live('click', function (event) {
		$(this).closest('.panel').slideUp('400', function () {
			$(this).remove();
			if ($('.duan-box').children('.panel-info').length < 2) {
				$('.duan-box > .panel-info').find('.remove-duan').hide();
			}
		});
		if (!!$(this).data('id')) {
			$.getJSON('ajax.php?op=duan&name=xoaduan', {
				id: $(this).data('id')
			}, function (json, textStatus) {});
		}
	});
	// PASS HOT MAIL
	$(document).on('click', '#viewHostmail', function (event) {
		event.preventDefault();
		var _inputHostmail = $('#hostmail_pass').find("input[name='hostmail_pass']");
		if (_inputHostmail.attr('type') == 'password') {
			_inputHostmail.attr('type', 'text');
		} else {
			_inputHostmail.attr('type', 'password');
		}
		if ($(this).children('i').hasClass('fa-eye')) {
			$(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
		} else {
			$(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
		}
	});
	$(document).on('click', '.configColorDisable', function () {
		if ($(this).prop('checked')) {
			$(this).closest('.input-group-addon').next('input').css('visibility', 'hidden');
			$(this).closest('.input-group-addon').next().next().next('input').css('visibility', 'hidden');
		} else {
			$(this).closest('.input-group-addon').next('input').css('visibility', 'visible');
			$(this).closest('.input-group-addon').next().next().next('input').css('visibility', 'visible');
		}
	});
	$('#backupConfig').on('click', function () {
		if (confirm('Bạn có chắc muốn sao lưu?')) {
			$(this).button('loading');
			$.getJSON('ajax.php?op=cauhinh', {
				name: 'backup_config'
			}, function (json, textStatus) {
				$('#backupConfig').button('reset');
				if (json.status == 'success') {
					$('#restoreConfig').removeClass('disabled');
					swal('Sao lưu thành công!', '', 'success');
				} else {
					swal('Sao lưu thất bại!', '', 'error');
				}
			});
		}
	});
	$('#restoreConfig').on('click', function () {
		if (confirm('Bạn có chắc muốn khôi phục cấu hình gần nhất không?')) {
			$(this).button('loading');
			$.getJSON('ajax.php?op=cauhinh', {
				name: 'restore_config'
			}, function (json, textStatus) {
				$('#restoreConfig').button('reset');
				if (json.status == 'success') {
					swal('Khôi phục thành công!', '', 'success');
				} else {
					swal('Khôi phục thất bại!', '', 'error');
				}
			});
		}
	});
	$(document).on('click', '#collapseConfig', function () {
		if ($(this).closest('.config-row').hasClass('expand')) {
			$(this).closest('.config-row').removeClass('expand').addClass('collapse');
		} else {
			$(this).closest('.config-row').removeClass('collapse').addClass('expand');
		}
	});
	//================== SET HEIGHT=======================
	$('.config-L').height($('.right-item.active > .right-box > .panel-body').height());
	$(document).on('click', '.left-item', function (event) {
		event.preventDefault();
		$('.config-R').find('.right-item').removeClass('active');
		$($(this).find('.config-click').attr('href')).addClass('active');
		$(this).parent('.config-box').find('.left-item').removeClass('active');
		$(this).addClass('active');
		var _index = $(this).data('name');
		$.getJSON('ajax.php?op=cauhinh', {
			name: 'menu_active',
			index: _index
		}, function (json, textStatus) {});
		//================== SET HEIGHT=======================
		var _heightL = $('.config-L'),
			_heightR = $('.config-R'),
			_maxHeight = 0;
		if (_heightL.height() < _heightR.height()) {
			_heightL.height(_heightR.height());
			_maxHeight = _heightR.height();
		} else {
			_heightL.height(_heightL.height());
			_maxHeight = _heightL.height();
		}
		$('.config-container').height(_maxHeight);
	});
	$(document).on('click', '#testMail', function (event) {
		event.preventDefault();
		if (confirm('VUI LÒNG LƯU CẤU HÌNH TRƯỚC KHI TEST MAIL.')) {
			$(this).button("loading");
			$.getJSON('ajax.php?op=cauhinh&name=testmail', {}, function (json, textStatus) {
				if( json ) {
                    if (json.result) {
                        $('#testMail').button("reset");
                        swal('Gửi mail thành công.', '', 'success');
                    } else {
                        $('#testMail').button("reset");
                        swal('Gửi mail thất bại, vui lòng kiểm tra cấu hình.', '', 'error');
                    }
                }
                else {
                    $('#testMail').button("reset");
                    swal('Xử lý thất bại, Vui lòng liên hệ BQT.', '', 'error');
                }
			});
		}
	});
	$(document).on('click', '.pending', function () {
		event.preventDefault();
		swal('Hiện đang được phát triển', '', 'info');
		$(this).attr('disabled', 'disabled');
	});
	if ($.isFunction($.fn.sortable)) {
		/*sortable DM*/
		$('#keotha_dM').sortable({
			placeholder: 'tr-sortable-highlight',
			opacity: '0.8',
			cursor: 'move',
		});
		/*Sortable*/
		$('#keotha_row').sortable({
			placeholder: 'tr-sortable-highlight',
			axis: 'y',
			opacity: '0.8'
		});
		$("#keotha_row").on('sortupdate', function () {
			var thisSizeOf = $('#keotha_row').find('tr').length;
			var this_op = $('input[name=this_op]').val();
			var paged = $("#paged").find('li.active > a').text();
			paged = (paged !== '' && paged > 0) ? paged : 1;
			var itemSelected = parseInt($("#limit :selected").text());
			itemSelected = (isNaN(itemSelected)) ? thisSizeOf : itemSelected;
			var i = (parseInt(paged) - 1) * itemSelected;
			i++;
			// Tinh so item bat dau cua trang
			$(".content_tr").each(function (index) {
				var id = $(this).data('id');
				$('.text_area[data-id=' + id + ']').text(i).val(i);
				$.get('./ajax.php', {
					name: 'sapxepthutu',
					id: id,
					op: this_op,
					thutu: i
				}, function () {});
				i++;
			});
			if ($.isFunction($.fn.notify)) {
				$.notify('Đã lưu lại thứ tự', {
					globalPosition: 'top right',
					className: 'success'
				});
			} else {
				console.error('Khong tim thay plugin notify \n[code: notify404] ');
			}
		});
	} else {
		console.error('Khong tim thay function sortable \n[code: sortable404]');
	}
	// TAT CẢ HÌNH ANH LOAI
	$("#tatcahinhanh-loai").on("change", function () {
		if ($(this).val().length) {
			$(".btnSave").removeClass("disabled");
		} else {
			$(".btnSave").addClass("disabled");
		}
	});
	$("input[name='chonmausac']").change(function () {
		var thisvalue = $(this).val();
		if (thisvalue == 0) {
			$("#anhien_chonmau").fadeOut();
		} else {
			$("#anhien_chonmau").fadeIn();
		}
	});
	// Hide Button xoa khi nhap so
	$('input#suasoluotxem').on('keyup', function (event) {
		event.preventDefault();
		if ($(this).val().length) {
			$('button#delview').addClass('disabled');
			$('button#delview').attr('disabled', 'disabled');
		} else {
			$('button#delview').removeClass('disabled');
			$('button#delview').removeAttr('disabled');
		}
	});
	// START QUICK SEARCH
	$.ajaxSetup({
		cache: false
	});
	$("#timnhanh-admin").on('keyup', function (event) {
		var tableSelected = $('#changselect').val();
		var urlsend = $(this).data('url') + 'adminweb/application/files/quick_search.php';
		event.preventDefault();
		$.get(urlsend, {
			keyword: $.trim($(this).val()),
			table: tableSelected,
		}, function (data) {
			/*optional stuff to do after success */
			$(".suggest ul").html(data).prepend('<div class="form-group"></div>');
		});
	});
	$("body, #row_top").click(function () {
		$("#timnhanh-admin").val(null);
		$(".suggest ul").children().remove();
	});
	// END QUICK SEARCH
	var type_icon = $("input:radio[name='type_icon']:checked");
	if (type_icon.val() == 1) {
		$("#icon-select").show();
		$("#icon-upload").hide();
	} else if (type_icon.val() == 0) {
		$("#icon-select").hide();
		$("#icon-upload").show();
	} else {
		$("#icon-select").hide();
		$("#icon-upload").hide();
	}
	$("input:radio[name='type_icon']").change(function () {
		if ($(this).val() == 1) {
			$("#icon-upload").hide();
			$("#icon-select").slideDown(400);
		} else if ($(this).val() == 0) {
			$("#icon-select").hide();
			$("#icon-upload").slideDown(400);
		} else {
			$("#icon-select").slideUp(400);
			$("#icon-upload").slideUp(400);
		}
	});
	$(".list-icon li").on("click", function () {
		$("input#chon_faicon").val($(this).data('value'));
		$("#result_faicon i").removeClass().addClass($(this).data('value'));
	});
	/* HUY thongbao_nhanh2 */
	$("#hide_thongbaonhanh2").click(function () {
		$.ajax({
			url: 'modules/home/home_ajax.php',
			type: 'POST',
			async: true,
			cache: false,
			data: {
				value: 1,
				action: 'cancel_thongbao2'
			},
			success: function (reThongbao2) {},
			error: function () {
				swal("Có lỗi xảy ra", "", "error");
			}
		});
	});
	/* HUY thongbao_nhanh1 */
	var yetVisited = sessionStorage['visited'];
	if (!yetVisited) {
		// open popup
		var thongbao1 = $('#thongbao_webso').html();
		$("#alert_box .for-user .content").html(thongbao1);
		setTimeout(function () {
			$("#alert_box .for-user").slideUp('slow', function () {
				$("#alert_box").remove();
				$("#thongbao_webso").remove();
			});
		}, 8000);
		$("#alert_box .for-user .close").click(function () {
			$("#alert_box").remove();
			$("#thongbao_webso").remove();
		});
		sessionStorage['visited'] = "yes";
	} else {
		$("#alert_box").remove();
		$("#thongbao_webso").remove();
	}
	/************ Send Feedback ************/
	$("#submit_feedback").click(function () {
		if ($("#feedback_frm input[name=hoten]").val().length == 0) {
			$("#feedback_frm input[name=hoten]").focus();
			swal('', 'Vui lòng điền họ tên', 'warning');
		} else if ($("#feedback_frm input[name=hoten]").val().length < 2) {
			$("#feedback_frm input[name=hoten]").focus();
			swal('', 'Họ tên phải trên 2 ký tự', 'warning');
		} else if ($("#feedback_frm textarea[name=noidung]").val().length == 0) {
			$("#feedback_frm textarea[name=noidung]").focus();
			swal('', 'Vui lòng nhập nội dung phản hồi', 'warning');
		} else if ($("#feedback_frm textarea[name=noidung]").val().length < 6) {
			$("#feedback_frm textarea[name=noidung]").focus();
			swal('', 'Nội dung cần rõ ràng.', 'warning');
		} else if ($("#feedback_frm input[name=captcha]").val().length == 0) {
			$("#feedback_frm input[name=captcha]").focus();
			swal('', 'Vui lòng nhập mã Captcha.', 'warning');
		} else if ($("#feedback_frm input[name=captcha]").val().length < 3) {
			$("#feedback_frm input[name=captcha]").focus();
			swal('', 'Captcha phải 3 ký tự.', 'warning');
		} else {
			var datacaptcha = $("#feedback_frm input[name=captcha]").val();
			// Check Captcha
			$.ajax({
				url: 'plugin/captcha/checkcaptcha.php',
				type: 'GET',
				async: true,
				cache: false,
				data: {
					capt: datacaptcha
				},
				success: function (recaptcha) {
					if (recaptcha == 1) {
						// THuc hien ajax send
						var send_hoten = $("#feedback_frm input[name=hoten]").val();
						var send_dienthoai = $("#feedback_frm input[name=dienthoai]").val();
						var send_email = $("#feedback_frm input[name=email]").val();
						var send_noidung = $("#feedback_frm textarea[name=noidung]").val();
						var send_op = $("#feedback_frm input[name=getop]").val();
						var send_method = $("#feedback_frm input[name=getmethod]").val();
						var send_lang = $("#feedback_frm input[name=getlang]").val();
						var send_time = $("#feedback_frm input[name=getthoigian]").val();
						$.ajax({
							url: 'http://webso.vn/phanhoi.php',
							type: 'POST',
							async: true,
							cache: false,
							data: {
								gui_hoten: send_hoten,
								gui_dienthoai: send_dienthoai,
								gui_email: send_email,
								gui_noidung: send_noidung,
								gui_op: send_op,
								gui_method: send_method,
								gui_lang: send_lang,
								gui_time: send_time
							},
							success: function (rewebso) {
								console.log('data', rewebso);
								if (rewebso == 1) {
									swal('Gửi thành công', '', 'success');
									$("#myModal2").hide();
									$(".modal-backdrop.in").css('opacity', '0');
								} else {
									swal('Gửi thất bại', '', 'error');
									$("#myModal2").hide();
									$(".modal-backdrop.in").css('opacity', '0');
								}
							},
							error: function () {
								swal("Có lỗi xảy ra", "", "error");
							}
						});
					} else {
						$("#feedback_frm input[name=captcha]").focus();
						swal('', 'Mã captcha không đúng !', 'warning');
					}
				}
			});
		}
	});
	/**==================================================
	@ TAO SITEMAP*/
	$('#createSitemap').on('click', function () {
		var v = $('#thoigianquet').val();
		if (!!v) {
			$.post('./?op=cauhinh&method=taositemap', {
				thoigianquet: v
			}, function (data, textStatus, xhr) {
				$("#sitemapNotifi").show().children('strong').html('Tạo sitemap thành công');
				setTimeout(function () {
					$("#sitemapNotifi").hide();
				}, 3000);
			});
		}
	});
	$('.jscolor').change(function () {
		var thisColor = $(this).css('color');
		if ($(this).next('[data-getcolor]').length) {
			$(this).next('[data-getcolor]').val(thisColor);
		}
		$("input[name=return_color]").val(thisColor);
		$("input[name=color_danhmuc]").val(thisColor);
	});
	/************ ########################### ************/
	$('input[name=schedule_change]').on('change', function(){
		if( $(this).prop('checked') == true ){
			$('input[name=schedule_view]').hide();
			$('input[name=schedule_date]').show();
		}else{
			$('input[name=schedule_view]').show();
			$('input[name=schedule_date]').hide();
		}
	});
	var date = new Date();
		//tomorow = date.setDate(date.getDate() + 1);
	if( $.isFunction($.fn.datetimepicker) ){
		$('.datetimepicker').datetimepicker({
			minDate: date,
			defaultDate: date,
			locale: 'vi',
			sideBySide: !0,
			icons: {
				time: 'fa fa-clock-o',
				date: 'fa fa-calendar',
				up: 'fa fa-chevron-up',
				down: 'fa fa-chevron-down',
				previous: 'fa fa-chevron-left',
				next: 'fa fa-chevron-right',
				today: 'fa fa-screenshot',
				clear: 'fa fa-trash',
				close: 'fa fa-remove'
			}
		});
	}
	if ($.isFunction($.fn.datepicker)) {
        var dateToday = new Date();
		$('.datepicker').datepicker({
            showButtonPanel: true,
			defaultDate: +7,
			language: 'vi',
			clearBtn: true,
			minDate: dateToday,
		});
	} else {
		console.error('Khong tim thay ham datepicker \n[code: datepicker404]');
	}
	/************ Dong/Mo thiet lap mail ************/
	if ($("input:radio[name=sendemail]:checked").val() == 'off') {
		$("#body_setup_email").hide();
	} else {
		$("#body_setup_email").show();
	}
	$("input:radio[name=sendemail]").change(function () {
		var value = $("input:radio[name=sendemail]:checked").val();
		if (value == 'on') {
			$("#body_setup_email").slideDown();
		} else {
			$("#body_setup_email").slideUp();
		}
	});
	/************ Dong/Mo thong bao dong website ************/
	if ($("input:radio[name=stop_website]:checked").val() == 'off') {
		$("#thongbao_stop").hide();
	} else {
		$("#thongbao_stop").show();
	}
	$("input:radio[name=stop_website]").change(function () {
		var value = $("input:radio[name=stop_website]:checked").val();
		if (value == 'on') {
			$("#thongbao_stop").slideDown();
		} else {
			$("#thongbao_stop").slideUp();
		}
	});
	/************ Validate Background Website ************/
	if ($("#frm-bgwebsite input[name='mota']").val() === "") {
		$("a.btn-save").addClass('disabled');
	} else {
		$("a.btn-save").removeClass('disabled');
	}
	$("#frm-bgwebsite input[name='mota']").keyup(function () {
		if ($(this).val().length >= 4) {
			$("a.btn-save").removeClass('disabled');
			$(this).parent().removeClass('has-error').addClass('has-success');
		} else {
			$("a.btn-save").addClass('disabled');
			$(this).parent().removeClass('has-success').addClass('has-error');
		}
	});
	/************ An ChonHinh Di khi Check ko hinh ************/
	if ($("input[name=xemhinh]:checked").val() == 1) {
		$("#urlfile").show();
	} else {
		$("#urlfile").hide();
	}
	$("input[name='xemhinh']").change(function (event) {
		if ($(this).val() == 1) {
			$("#urlfile").hide().slideDown(400);
		} else {
			$("#urlfile").show().slideUp(400);
		}
	});
	/************ An ChonNhom Di khi User dang la SupperAdmin ************/
	if ($("#changechonquyen").val() > 0) {
		$("#box-chonnhom").hide();
	} else {
		$("#box-chonnhom").show();
	}
	$("#changechonquyen").change(function () {
		if ($(this).val() > 0) {
			$("#box-chonnhom").slideUp(400);
		} else {
			$("#box-chonnhom").slideDown(400);
		}
	})
	/*********************** PERMISSION CHECK ALL **********************/
	$("#checkAllPer").change(function () {
		$("#list_checkPer input:checkbox").prop('checked', $(this).prop("checked"));
	});
	/*********************** VALIDATE THONG TIN DANG KY **********************/
	// HIEN THI DOI MAT KHAU
	$('#click_matkhau').click(function () {
		$("#box_doimatkhau").slideToggle(this.checked);
	});
	// show tab trong frm
	$('#tabs > div.tab-text > div').hide();
	$('#tabs > div.tab-text > div:first').show();
	$('#tabs ul li:first').addClass('active');
	$('#tabs ul li').click(function () {
		$('#tabs ul li').removeClass('active');
		$(this).addClass('active');
		var currentTab = $(this).attr('type');
		$('#tabs > div.tab-text > div').hide();
		$(currentTab).show();
		return false;
	});
	$('#tabs > #frm-register > div').show();
	//Go top
	var widthscroll = $("#row_top").innerHeight() + $("#row_menu").innerHeight() + 60;
	$(window).scroll(function () {
		if ($(this).scrollTop() > widthscroll) {
			$(".row_content_top").addClass("fixed");
		} else {
			$(".row_content_top").removeClass("fixed");
		}
	});
	// fix table th
	if ($.isFunction($.fn.fixedtableheader)) {
		$('.adminlist').fixedtableheader();
	} else {
		console.error('Khong tim thay ham fixedtableheader \n[code: fixedtableheader404]');
	}
});