<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    require dirname(dirname(dirname(__DIR__))).'/configs/inc.php';

if ( ws_get('keyword') ) {

    $keyword_   = trim(ws_get('keyword'));
    $setUrl     = preg_replace('#[ -]+#', '-', $keyword_);
    $valTintuc  = '';
    $valSanpham = '';

    /** TRUY VẤN DANH MUC */
    $s_search1 = "
    SELECT a.id,b.ten,url
    FROM tbl_danhmuc a
    INNER JOIN tbl_danhmuc_lang b
        ON a.id = b.iddanhmuc
        AND b.idlang = '{$_SESSION['__defaultlang']}'
    WHERE b.ten LIKE '%" . addslashes($keyword_) . "%'";
    $d_search = $db->rawQuery($s_search1);

    if (count($d_search) > 0) {
        echo '
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Menu</h3>
            </div>
            <div class="panel-body frm-danhmuc">';
            foreach ($d_search as $key_search => $value_search) {
                $search_id      = $value_search['id'];
                $search_ten     = $value_search['ten'];
                $search_lienket = $value_search['url'];

                echo '
                <a href="./?op=danhmuc&method=frm&id=' . $search_id . '" title="' . $search_ten . '">
                    <li>' . $search_ten . '</li>
                </a>';
            }
        echo '
        </div>
            </div>';
    }
    /** Nội dung */
    $s_search2 = "
    SELECT a.id,a.loai,b.ten,url
    FROM tbl_noidung a
    INNER JOIN tbl_noidung_lang b
        ON a.id = b.idnoidung
        AND b.idlang = '{$_SESSION['__defaultlang']}'
    WHERE a.loai = 0 AND (url LIKE '%{$setUrl}%' OR b.ten LIKE '%" . addslashes($keyword_) . "%')";
    $d_search2 = $db->rawQuery($s_search2);

    if (count($d_search2) > 0) {
        echo '
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Nội dung</h3>
            </div>
            <div class="panel-body frm-tintuc">
            ';
            foreach ($d_search2 as $key_search2 => $value_search2) {
                $search2_id      = $value_search2['id'];
                $search2_ten     = $value_search2['ten'];
                $search2_loai    = $value_search2['loai'];
                $search2_lienket = $value_search2['url'];
                echo '<a href="./?op=noidung&method=frm&id=' . $search2_id . '" title="' . $search2_ten . '"><li>' . $search2_ten . '</li></a>';
            }
        echo '
            </div>
        </div>';
    }
    /** Sản phẩm */
    $s_search3 = "
    SELECT a.id,a.loai,b.ten,url
    FROM tbl_noidung a
    INNER JOIN tbl_noidung_lang b
        ON a.id = b.idnoidung
        AND b.idlang = '{$_SESSION['__defaultlang']}'
    WHERE a.loai = 1 AND (url LIKE '%{$setUrl}%' OR b.ten LIKE '%" . addslashes($keyword_) . "%')";
    $d_search3 = $db->rawQuery($s_search3);

    if (count($d_search3) > 0) {
        echo '
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Sản phẩm</h3>
            </div>
            <div class="panel-body frm-sanpham">';
            foreach ($d_search3 as $key_search3 => $value_search3) {
                $search3_id      = $value_search3['id'];
                $search3_ten     = $value_search3['ten'];
                $search3_loai    = $value_search3['loai'];
                $search3_lienket = $value_search3['url'];
                echo '<a href="./?op=sanpham&method=frm&id=' . $search3_id . '" title="' . $search3_ten . '"><li>' . $search3_ten . '</li></a>';
            }
        echo '
            </div>
        </div>';
    }

    if (count($d_search2) < 1 && count($d_search3) < 1 && count($d_search) < 1) {
        echo '<li><a href="#" title="">No result not found</a></li>';
    }

}
