<?php
$_arr_excel = array('bat' => 1);
if(@$_SESSION['user_supperadmin']==1){
    $_arr_excel = array('bat' => 1);
}
$_arr_loai_noidung = array(
    0  => "content",
    1  => "product",
    2  => "video",
    3  => "intro",
    4  => "download",
    5  => "service",
    6  => "viewweb",
    7  => "customer",
    8  => "customerreviews",
    9  => "project",
    10 => "question",
    11 => "gallery",
    12 => "picture"
);
$_arr_loai_banner = array(
    0  => "banner",
    1  => "slideshow",
    2  => "bannerqc",
    3  => "doitac",
    4  => "thuvienanh",
    5  => "duan",
    10 => "sanpham_logo"
);
$_arr_target = array(
    ""       => "Mở trang hiện tại",
    "_blank" => "Mở trang mới"
);
$_arr_listpage = array(
    "1"  => "10",
    "2"  => "20",
    "3"  => "30",
    "4"  => "50",
    "5"  => "60",
    "6"  => "70",
    "7"  => "80",
    "8"  => "100",
    "9"  => "200",
    "10" => "500",
    "11" => "All"
);
$_arr_anhien = array(
    0 => "Ẩn",
    1 => "Hiển thị"
);
$_arr_datatype = array(
    0 => "Dữ liệu dạng text",
    1 => "Dữ liệu dạng Selected [chọn được 1 giá trị]",
    2 => "Dữ liệu dạng checkbox [chọn nhiều giá trị]"
);
$_arr_layout_vitri = array(
    "0" => "Cột A",
    "1" => "Cột B",
    "2" => "Hàng A"
);
$_arr_layout_loaidulieu = array(
    "0" => "Danh mục menu",
    "1" => "banner quảng cáo",
    "2" => "tiện ích khác"
);
$_arr_layout_dieukien = array(
    ""          => "Tất cả - không cần check",
    "colleft"   => "Cột A",
    "colright"  => "Cột B",
    "coltop"    => "Phía trên",
    "colbottom" => "Phía dưới"
);
$_arr_layout_dieukien_tin = array(
    ""          => "Tất cả - không cần check",
    "home"      => "Trang chủ",
    "noibat"    => "Nổi bật",
    "moi"       => "Mới",
    "banchay"   => "Bán chạy",
    "khuyenmai" => "Khuyến mãi"
);
$_arr_trangthai_donhang = array(
    '0' => '<td class="chuaxem">Chưa xem</td>',
    '1' => '<td class="daxem">Đã xem</td>',
    '2' => '<td class="dangxuly">Đang xử lý</td>',
    '3' => '<td class="hoanthanh">Hoàn thành</td>',
    '4' => '<td class="dahuy">Đã hủy</td>'
);
$_arr_xuly_trangthai_donhang = array(
    '2' => 'Đang xử lý',
    '3' => 'Hoàn thành',
    '4' => 'Hủy hàng'
);
$_arr_hinhanh_slide = array(
    // '0' => 'Banner',
    // '1' => 'SlideShow',
    // '2' => 'BannerQC',
    '3' => 'Đối tác',
    '4' => 'Thư viện ảnh',
    '5' => 'Dự án'
);
$_arr_khachhang = array(
    '4' => 'Khách hàng',
    '5' => 'Nhà phân phối'
);
$_arr_chon_tim = array(
    'danhmuc' => 'Menu',
    'sanpham' => 'Sản phẩm',
    'noidung' => 'Nội dung'
);
$_arr_vitrilogo = array(
    '0' => 'Logo gắn giữa',
    '1' => 'Logo gắn Trên Trái',
    '2' => 'Logo gắn Trên Phải',
    '3' => 'Logo gắn Dưới Phải',
    '4' => 'Logo gắn Dưới Trái'
);

