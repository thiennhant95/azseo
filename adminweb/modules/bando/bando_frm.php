<?php
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Process Thêm mới
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}
$_id_name = $__config['id'];
$_getop   = ws_get('op');
$_idtype  = $__config['type'];
$_idSP    = $__config['idsp'];
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
//*** Xử lý quá trình Chỉnh sửa ***//
if ($__getid != '') {
    $__cid = ws_get('id');
} else {
//*** Xử lý quá trình Thêm mới ***//
    $id        = "";
    $idtype    = "";
    $baiviet   = "";
    $icon      = "";
    $img       = "";
    $file      = "";
    $coltop    = "";
    $colleft   = "";
    $colright  = "";
    $colbottom = "";
    $main      = "";
    $home      = "";
    $loai      = "";
    $thutu     = "";
    $anhien    = "";
    $iduser    = "";
}
$ten_post = replace_html(ws_post('ten'.$__defaultlang));
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luuvadong'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
   <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">&nbsp;</div>
      <div class="panel-body">
   <div id="tabs">
   <ul>';
$s_lang = "SELECT a.id,a.idkey,b.ten
           from tbl_lang AS a
           inner join tbl_lang_lang AS b
           where a.id = b.iddanhmuc
           and b.idlang = '{$__defaultlang}'
           and a.anhien = 1
           order by thutu Asc";
$d_lang = $db->rawQuery($s_lang);
// echo $s_lang;
if (count($d_lang) > 0) {
    $sum_lang = count($d_lang);
    foreach ($d_lang as $key_lang => $info_lang) {
        $ten    = $info_lang['ten'];
        $idlang = $info_lang['id'];
        $idkey  = $info_lang['idkey'];
        $CONTENT .= '<li type="#tab' . ($key_lang + 1) . '">' . $ten . '</li>';
    }
}
$CONTENT .= '
   <div class="clear"></div>
   </ul>
    <div style="clear:both"></div>
    <div class="tab-text">';
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $tenlang = $info_lang['ten'];
        $idlang  = $info_lang['id'];
        $idkey   = $info_lang['idkey'];
        // lay du lieu theo ngon ngu
        $s_noidung_data = "SELECT * FROM {$__tablenoidung} where idlang = {$idlang} and idtype = '" . $__cid . "' ";
        $d_noidung_data = $db->rawQuery($s_noidung_data);
        @$d_noidung_data = $d_noidung_data[0];
        $ten            = $d_noidung_data['ten'];
        $diachi         = $d_noidung_data['diachi'];
        $zoom           = $d_noidung_data['zoom'];
        $toado          = $d_noidung_data['toado'];
        $CONTENT .= '
         <div id="tab' . ($key_lang + 1) . '">
         <div class="adminform">';
        if ($__config['ten'] == 1) {
            $CONTENT .= '
               <div class="form-group">
                  <label for="title">' . $arraybien['tencongty'] . '&nbsp;' . $tenlang . '</label>
                  <input type="text" name="ten' . $idlang . '" value="' . $ten . '"  class="form-control" placeholder="' . $arraybien['nhaptencongty'] . '" />
               </div>
            ';
        }
        if ($__config['diachi'] == 1) {
            $CONTENT .= '
               <div class="form-group">
                  <label for="title">' . $arraybien['diachi'] . '&nbsp;' . $tenlang . '</label>
                  <input id="txtautocomplete" autocomplete="off" type="text" name="diachi' . $idlang . '" value="' . $diachi . '"  class="form-control seo-placeholder" placeholder="' . $arraybien['nhapdiachicongty'] . '" />
               </div>
            ';

        }
        $CONTENT .= '
            <div class="row">
               <div class="col-sm-6">';
        if ($__config['zipcode'] == 1) {
            $CONTENT .= '
                        <div class="form-group">
                           <label for="title">' . $arraybien['zipcode'] . '&nbsp;' . $tenlang . '</label>
                           <input type="text" name="zipcode' . $idlang . '" class="form-control" placeholder="' . $arraybien['nhapzipcode'] . '" />
                        </div>
                     ';
        }
        $CONTENT .= '
               </div>
               <div class="col-sm-6">';
        if ($__config['thanhpho'] == 1) {
            $CONTENT .= '
                        <div class="form-group">
                           <label for="title">' . $arraybien['thanhpho'] . '&nbsp;' . $tenlang . '</label>
                           <input type="text" name="thanhpho' . $idlang . '" placeholder="' . $arraybien['thanhphohientai'] . '" class="form-control" />
                        </div>
                     ';
        }
        $CONTENT .= '
               </div>
            </div>
         ';
        if ($__config['maptype'] == 1) {
            $CONTENT .= '
               <div class="form-group">
                  <label for="maptite">' . $arraybien['maptype'] . '&nbsp;' . $tenlang . '</label>
                  <select name="maptype' . $idlang . '" id="map_type_combo" class="form-control">
                     <option value="roadmap">' . $arraybien['giaothong'] . '</option>
                     <option value="satellite">' . $arraybien['vetinh'] . '</option>
                     <option value="hybrid">' . $arraybien['vetinhvoitenduong'] . '</option>
                     <option value="terrain">' . $arraybien['diathe'] . '</option>
                  </select>
               </div>
            ';
        }
        if ($__config['zoommap'] == 1 && false) {
            $CONTENT .= '
               <div class="form-group">
                  <label for="maptite">' . $arraybien['zoom'] . '&nbsp;' . $tenlang . '</label>
                  <select name="zoommap' . $idlang . '" id="map_type_combo" class="form-control">
                     <option value="21"> 2.5 m </option>
                     <option value="20"> 5 m </option>
                     <option value="19"> 10 m </option>
                     <option value="18"> 20 m </option>
                     <option value="17"> 50 m </option>
                     <option value="16"> 100 m </option>
                     <option value="15"> 200 m </option>
                     <option value="14"> 400 m </option>
                     <option value="13"> 1 km </option>
                     <option value="12"> 2 km </option>
                     <option value="11"> 4 km </option>
                     <option value="10"> 8 km </option>
                     <option value="9"> 15 km </option>
                     <option value="8"> 30 km </option>
                  </select>
               </div>
            ';
        } else {
            $CONTENT .= '<div class="form-group">
               <label for="maptite">' . $arraybien['zoom'] . '&nbsp;' . $tenlang . '</label>
               <input type="text" name="zoom' . $idlang . '" class="form-control" value="' . $zoom . '" />
            </div>';
        }
        $CONTENT .= '
            <div class="row">
               <div class="col-sm-6">';
        if ($__config['chieucao'] == 1) {
            $CONTENT .= '
                        <div class="form-group">
                           <label for="title">' . $arraybien['chieucao'] . '&nbsp;' . $tenlang . '</label>
                           <div class="input-group">
                              <input type="text" onkeyup="CheckNumber(this)" name="heightmap' . $idlang . '" class="form-control" />
                              <div class="input-group-addon">px</div>
                           </div>
                        </div>
                     ';
        }
        $CONTENT .= '
               </div>
               <div class="col-sm-6">';
        if ($__config['chieurong'] == 1) {
            $CONTENT .= '
                        <div class="form-group">
                           <label for="title">' . $arraybien['chieurong'] . '&nbsp;' . $tenlang . '</label>
                           <div class="input-group">
                              <input type="text" onkeyup="CheckNumber(this)" name="widthmap' . $idlang . '" class="form-control" />
                              <div class="input-group-addon">px</div>
                           </div>
                        </div>
                     ';
        }
        $CONTENT .= '
               </div>
            </div>
         ';
        if ($__config['toado'] == 1) {
            $CONTENT .= '<div class="form-group">
               <label for="toado">' . $arraybien['toado'] . '&nbsp;' . $tenlang . '</label>
               <input type="text" value="' . $toado . '" name="toado' . $idlang . '" id="toado' . $idlang . '" class="form-control">
            </div>';
        }
        $CONTENT .= '
            </div>
         </div>
         ';
    }
}
$CONTENT .= '
            </div><!-- End tab-text -->
         </div><!-- End tabs -->
      </div><!-- End panel-body -->
   </div><!-- End  panel-default -->
</div><!-- End col_info -->
      ';
//********** col right ********** //
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading thuoctinhtitle">&nbsp;' . $arraybien['xemtruoc'] . '</div>
  <div class="panel-body">';
$CONTENT .= include 'bando_preview.php';
$CONTENT .= '
      </div>
   </div>
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '</div>'; // end col data
//<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPAkptLAWq7Va0KnEHgTzdiZL3SnBdLO4&libraries=places"></script>
$CONTENT .= '
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu1UbAR1uZ4JmgVdevNeIAF_fO_QbqrO0&libraries=places"></script>

<script language="javascript">
google.maps.event.addDomListener(window, \'load\', intilize);
function intilize() {
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById("txtautocomplete"));
    google.maps.event.addListener(autocomplete, \'place_changed\', function() {
        var place = autocomplete.getPlace();
        var location = "";
        location += place.geometry.location.lat() + ",";
        location += place.geometry.location.lng();
        document.getElementById(\'toado1\').value = location
    });



};
</script>';
$CONTENT .= '<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
