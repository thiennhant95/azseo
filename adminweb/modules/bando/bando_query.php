<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    // ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    // HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    // LƯU THỨ TỰ MỤC TIN
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyenxoa'] . '");
         location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        // update thutu
        $s_update_thutu = "update " . $__table . " set thutu = thutu-1 where thutu > $thutu_value ";
        $d_update_thutu = $db->rawQuery($s_update_thutu);
        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idtype  = '{$id_order}' ");
        }
    }
}
if ($__getaction == 'save') {
    // LƯU TRỮ DỮ LIỆU
    $_getidSP = "";
    if ($__getid != '') {
        // neu la cap nhat
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->createSubID($__table, $__id, "");
    }
    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $images_name = ws_post('url'.$__defaultlang);
    if ($__getid == '') {
        $aray_insert = array(
            "thutu"  => $thutu,
            "anhien" => 1,
        );
        $id_insert = $db->insert($__table, $aray_insert);
        // them du lieu vao bang noi dung danh muc
        $s_lang = "SELECT * from tbl_lang where anhien = 1";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten    = trim(ws_post('ten'.$idlang));
                $diachi = trim(ws_post('diachi'.$idlang));
                $zoom   = trim(ws_post('zoom'.$idlang));
                $toado  = trim(ws_post('toado'.$idlang));
                // luu du lieu vao bang danh muc lang
                $aray_insert_lang = array(
                    "idtype" => $id_insert,
                    "idlang" => $idlang,
                    "ten"    => $ten,
                    "diachi" => $diachi,
                    "zoom"   => $zoom,
                    "toado"  => $toado,
                );
                $db->insert($__tablenoidung, $aray_insert_lang);
            }
        }
    } else {
        // them du lieu vao bang noi dung danh muc
        $s_lang = " SELECT * from tbl_lang where anhien = 1 ";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten    = trim(ws_post('ten'.$idlang));
                $diachi = trim(ws_post('diachi'.$idlang));
                $zoom   = trim(ws_post('zoom'.$idlang));
                $toado  = trim(ws_post('toado'.$idlang));
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "SELECT id from $__tablenoidung where idtype = '{$__postid}' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) {
                    // luu du lieu vao bang danh muc lang
                    $aray_insert_lang2 = array(
                        "ten"    => $ten,
                        "diachi" => $diachi,
                        "zoom"   => $zoom,
                        "toado"  => $toado,
                    );
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang2, "idtype = $__getid AND idlang = $idlang ");
                } else {
                    // luu du lieu vao bang danh muc lang
                    $aray_insert_lang = array(
                        "idtype" => $__postid,
                        "idlang" => $idlang,
                        "ten"    => $ten,
                        "diachi" => $diachi,
                        "zoom"   => $zoom,
                        "toado"  => $toado,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                }
            }
        }
    }
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $id;
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
