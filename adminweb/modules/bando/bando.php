<?php session_start();
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Quản lý bản đồ",
    "table"           => 'tbl_maps',
    "tablenoidung"    => 'tbl_maps_lang',
    "id"              => 'id',
    "ten"             => 1,
    "diachi"          => 1,
    "thanhpho"        => 0,
    "zipcode"         => 0,
    "maptype"         => 0,
    "zoommap"         => 1,
    "chieucao"        => 0,
    "chieurong"       => 0,
    "toado"           => 1,
    "anhien"          => 1,
    "action"          => 1,
    "add_item"        => 1,
    "action"          => 1,
    "sizeimagesthumb" => 300,
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
