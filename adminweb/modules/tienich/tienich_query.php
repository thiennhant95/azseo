<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    // ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'publish') {
    // HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'saveorder') {
    // LƯU THỨ TỰ MỤC TIN
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
    $db->thuTu($__table, $__id, "thutu", "loai", $__config['loai']);
} elseif ($__post_task == 'remove') {
    // XOA DU LIEU
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            if ($value_file != '') {
                $__path = $__config['path_file'] . $value_file;
                unlink("$__path");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idtype  = '{$id_order}' ");
        }
    }
}

// THÊM MỚI DỮ LIỆU
if ($__getaction == 'save') {
    $vitri      = ws_post('vitri');
    $loai       = $__config['loai'];
    $idtype     = ws_post('idtype');
    $laytin     = ws_post('laytin');
    $soluongtin = ws_post('soluongtin');
    $thutu      = 1;
    $anhien     = ws_post('anhien');
    $link       = ws_post('link');
    $_getidSP   = "";
    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $images_name = ws_post('url'.$__defaultlang);
    // neu them moi
    if ($__getid == '') {
        $sql_update = "UPDATE $__table set thutu = thutu+1 where 1 = 1 ";
        $db->rawQuery($sql_update);

        $id_insert = $db->insert($__table, [
            "loai"   => $loai,
            "thutu"  => $thutu,
            "link"   => $link,
            "anhien" => $anhien,
        ]);

        if( $id_insert ) {
            // them du lieu vao bang noi dung danh muc
            $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
            $d_lang = $db->rawQuery($s_lang);
            if (count($d_lang) > 0) {
                foreach ($d_lang as $key_lang => $info_lang) {
                    $tenlang = $info_lang['ten'];
                    $idlang  = $info_lang['id'];
                    $idkey   = $info_lang['idkey'];
                    // get noi dung post qua
                    $ten = ws_post('ten'.$idlang);
                    // kiem tra url neu da co roi thì them ki tu cuoi url
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                    // luu du lieu vao bang danh muc lang
                    $db->insert($__tablenoidung, [
                        "idtype" => $id_insert,
                        "idlang" => $idlang,
                        "ten"    => $ten
                    ]);
                }
            }
        }
    } // CẬP NHẬT DỮ LIỆU
    else {
        $aray_insert = array(
            "anhien" => $anhien,
            "link"   => $link,
        );
        $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                // lap theo so luong ngon ngu
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten = ws_post('ten'.$idlang);
                // kiem tra url neu da co roi thì them ki tu cuoi url
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "select id from $__tablenoidung where idtype = '{$__postid}' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) {
                    // da tồn tại nội dung rồi thì update lại nội dung
                    // cap nhat lai du lieu
                    $aray_insert_lang = array("ten" => $ten);
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "idtype = '{$__postid}' and idlang = '{$idlang}'");
                } else {
                    $aray_insert_lang = array(
                        "idtype" => $__postid,
                        "idlang" => $idlang,
                        "ten"    => $ten);
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang
    } // end if cap nhat
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $link_redirect = './?op=' . ws_get("op") . '&method=frm';
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
