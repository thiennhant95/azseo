<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Liên kết website",
    "table"           => 'tbl_lienketwebsite',
    "tablenoidung"    => 'tbl_lienketwebsite_lang',
    "id"              => 'id',
    "thutu"           => 1,
    "loai"            => 0,
    "anhien"          => 1,
    "ngaytao"         => 1,
    "ngaycapnhat"     => 1,
    "ten"             => 1,
    "link"            => 1,
    "target"          => 1,
    "action"          => 1,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/logo/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
