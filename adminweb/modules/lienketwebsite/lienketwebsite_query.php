<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') // ẨN MỤC TIN
{
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') // HIỂN THỊ MỤC TIN
{
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') // LƯU THỨ TỰ MỤC TIN
{
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
    $db->thuTu($__table, $__id, "thutu", "loai", $__config['loai']);
} else if ($__post_task == 'remove') // XOA DU LIEU
{
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            if ($value_file != '') {
                $__path = $__config['path_file'] . $value_file;
                unlink("$__path");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idtype  = '{$id_order}' ");
        }
    }
}
if ($__getaction == 'save') // LƯU TRỮ DỮ LIỆU
{
    $idtype      = ws_post('idtype');
    $thutu       = 1;
    $loai        = $__config['loai'];
    $anhien      = ws_post('anhien');
    $img_url     = '../uploads/logo/' . $img;
    $anhien      = ws_post('anhien');
    $ngayhientai = $db->getDateTimes();
    // Create url lien ket
    $urllink = '';
    $_cid    = ws_get('id');
    if (ws_post('action') == 'add') {
        $thutu = (int) substr($subid, -3);
    } else {
        $thutu = (int) substr($id, -3);
    }
    $images_name = ws_post('url'.$__defaultlang);
    // neu them moi danh mục
    if ($__getid == '') {
        // update thutu tang len 1 cho tat ca
        $sql_update = "update $__table set thutu = thutu+1 where 1 = 1 ";
        $db->rawQuery($sql_update);
        $aray_insert = array("thutu" => 1,
            "loai"                       => $loai,
            "anhien"                     => $anhien,
            "ngaytao"                    => $ngayhientai,
            "ngaycapnhat"                => $ngayhientai,
            "iduser"                     => $_SESSION['user_id'],
        );
        $id_insert = $db->insert($__table, $aray_insert);
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten      = ws_post('ten'.$idlang);
                $hinh     = $_FILES["hinh" . $idlang]["name"];
                $linkhinh = ws_post('linkhinh'.$idlang);
                $manhung  = ws_post('manhung'.$idlang);
                $link     = ws_post('link'.$idlang);
                $target   = ws_post('target'.$idlang);
                $rong     = ws_post('rong'.$idlang);
                $cao      = ws_post('cao'.$idlang);
                $ten_img  = tao_url($ten);
                // upload hinh theo ngon ngu
                if ($hinh != '') {
                    $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
                    $imgfile = $ten_img . '-' . $tenlang . '.' . $extfile;
                    if (file_exists($__config['path_img'] . $imgfile)) {
                        $imgfile = rand(100, 1000) . $imgfile;
                    }
                    move_uploaded_file($_FILES["hinh" . $idlang]["tmp_name"], $__config['path_img'] . $imgfile);
                }
                // luu du lieu vao bang danh muc lang
                $aray_insert_lang = array("idtype" => $id_insert,
                    "idlang"                           => $idlang,
                    "ten"                              => $ten,
                    "link"                             => $link,
                    "target"                           => $target,
                );
                $db->insert($__tablenoidung, $aray_insert_lang);
            }
        }
    } // end box id != rong
    else {
        $aray_insert = array("anhien" => $anhien,
            "ngaycapnhat"                 => $ngayhientai,
            "iduser"                      => $_SESSION['user_id'],
        );
        $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) // lap theo so luong ngon ngu
            {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten      = ws_post('ten'.$idlang);
                $hinh     = $_FILES["hinh" . $idlang]["name"];
                $linkhinh = ws_post('linkhinh'.$idlang);
                $manhung  = ws_post('manhung'.$idlang);
                $link     = ws_post('link'.$idlang);
                $target   = ws_post('target'.$idlang);
                $rong     = ws_post('rong'.$idlang);
                $cao      = ws_post('cao'.$idlang);
                $xoaimg   = ws_post('xoaimg'.$idlang);
                $imgname  = ws_post('imgname'.$idlang);
                $ten_img  = tao_url($ten);
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "select id from $__tablenoidung where idtype = '{$__getid}' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) // da tồn tại nội dung rồi thì update lại nội dung
                {
                    // cap nhat lai du lieu
                    $aray_insert_lang = array("ten" => $ten,
                        "link"                          => $link,
                        "target"                        => $target,
                    );
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "idtype = '{$__postid}' and idlang = '{$idlang}'");
                    // neu cap nhat img
                    if ($hinh != '') {
                        if ($imgname != '') {
                            unlink($__config['path_img'] . $imgname);
                        }
                        //up file moi len
                        $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
                        $imgfile = $ten_img . '-' . $tenlang . '.' . $extfile;
                        if (file_exists("../uploads/logo/" . $imgfile)) {
                            $imgfile = rand(0, 100) . $imgfile;
                        }
                        move_uploaded_file($_FILES["hinh" . $idlang]["tmp_name"], $__config['path_img'] . $imgfile);
                        $aray_insert = array("hinh" => $imgfile);
                        $result      = $db->sqlUpdate($__tablenoidung, $aray_insert, "idtype = $__postid  and idlang = $idlang  ");
                    }
                } else {
                    // upload hinh theo ngon ngu
                    if ($hinh != '') {
                        $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
                        $imgfile = $ten_img . '-' . $tenlang . '.' . $extfile;
                        if (file_exists($__config['path_img'] . $imgfile)) {
                            $imgfile = rand(0, 100) . $imgfile;
                        }
                        move_uploaded_file($_FILES["hinh" . $idlang]["tmp_name"], $__config['path_img'] . $imgfile);
                    }
                    $aray_insert_lang = array("idtype" => $__getid,
                        "idlang"                           => $idlang,
                        "ten"                              => $ten,
                        "link"                             => $link,
                        "target"                           => $target,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
                // kiem tra neu thay hinh thi sẽ xóa hình cũ đi
                // xoa img
                if ($xoaimg == 1) {
                    unlink($__config['path_img'] . $imgname);
                    $aray_insert = array("hinh" => "");
                    $result      = $db->sqlUpdate($__tablenoidung, $aray_insert, "idtype = $__getid  and idlang = $idlang ");
                }
            } // end for lang
        } // end count lang
    } // end if cap nhat
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $id;
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
