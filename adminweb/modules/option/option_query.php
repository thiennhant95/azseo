<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    // ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'publish') {
    // HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'saveorder') {
    // LƯU THỨ TỰ MỤC TIN
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
    $db->thuTu($__table, $__id, "thutu", "loai", $__config['loai']);
} elseif ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idtype  = '{$id_order}' ");
        }
    }
}
if ($__getaction == 'save') {
    // BẤM NÚT LƯU
    $idtype   = trim(ws_post('idtype'));
    $img      = trim($_FILES["img"]["name"]);
    $loai     = trim(ws_post('loai'));
    $thutu    = trim(ws_post('thutu'));
    $anhien   = trim(ws_post('anhien'));
    $img_url  = '../uploads/option/' . $img;
    $id       = trim(ws_post('id'));
    $xoaimg   = trim(ws_post('xoaimg'));
    $imgname  = trim(ws_post('imgname'));
    $filename = trim(ws_post('filename'));
    $_getidSP = "";
    if ($__getid != '') {
        // neu la cap nhat
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->createSubID($__table, $__id, "");
    }
    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $images_name = ws_post('url'.$__defaultlang);
    // neu them moi danh mục

    // THÊM MỚI DỮ LIỆU
    if ($__getid == '') {
        // up load icon
        if ($icon != '') {
            $extfile  = pathinfo($icon, PATHINFO_EXTENSION);
            $iconfile = $images_name . '-icon.' . $extfile;
            if (file_exists("../uploads/option/" . $iconfile)) {
                $iconfile = rand(0, 100) . $iconfile;
            }
            move_uploaded_file($_FILES["icon"]["tmp_name"], "../uploads/option/$iconfile");
        }
        // upload hinh
        if ($img != '') {
            $extfile = pathinfo($img, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/option/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/option/$imgfile");
            $ResizeImage->load("../uploads/option/" . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save("../uploads/option/thumb/" . $imgfile);
        }
        $aray_insert = array(
            "id"     => $subid,
            "idtype" => $idtype,
            "img"    => $imgfile,
            "loai"   => $loai,
            "thutu"  => $thutu,
            "anhien" => $anhien,
        );
        $id_insert = $db->insert($__table, $aray_insert);

        if( $id_insert ) {
            // them du lieu vao bang noi dung danh muc
            $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
            $d_lang = $db->rawQuery($s_lang);
            if (count($d_lang) > 0) {
                foreach ($d_lang as $key_lang => $info_lang) {
                    $tenlang = $info_lang['ten'];
                    $idlang  = $info_lang['id'];
                    $idkey   = $info_lang['idkey'];
                    // get noi dung post qua
                    $ten        = trim(ws_post('ten'.$idlang));
                    $tieude     = trim(ws_post('tieude'.$idlang));
                    $mota       = trim(ws_post('mota'.$idlang));
                    $noidung    = trim(ws_post('noidung'.$idlang));
                    $url        = trim(ws_post('url'.$idlang));
                    $link       = trim(ws_post('link'.$idlang));
                    $target     = trim(ws_post('target'.$idlang));
                    $tukhoa     = trim(ws_post('tukhoa'.$idlang));
                    $motatukhoa = trim(ws_post('motatukhoa'.$idlang));

                    if( !$url ) {
                        $url = tao_url2($ten);
                    }

                    // kiem tra url neu da co roi thì them ki tu cuoi url
                    $s_checkurl = "SELECT url FROM {$__tablenoidung} WHERE url = '$url'";
                    $d_checkurl = $db->rawQuery($s_checkurl);
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                    // luu du lieu vao bang danh muc lang
                    $aray_insert_lang = array(
                        "idtype"     => $subid,
                        "idlang"     => $idlang,
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "noidung"    => $noidung,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                }
            }
        }
    } // CẬP NHẬT DỮ LIỆU
    else {
        $aray_insert = array("idtype" => $idtype,
            "loai"                        => $loai,
            "thutu"                       => $thutu,
            "anhien"                      => $anhien,
        );
        $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                // lap theo so luong ngon ngu
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = trim(ws_post('ten'.$idlang));
                $tieude     = trim(ws_post('tieude'.$idlang));
                $noidung    = trim(ws_post('noidung'.$idlang));
                $url        = trim(ws_post('url'.$idlang));
                $link       = trim(ws_post('link'.$idlang));
                $target     = trim(ws_post('target'.$idlang));
                $tukhoa     = trim(ws_post('tukhoa'.$idlang));
                $motatukhoa = trim(ws_post('motatukhoa'.$idlang));
                // kiem tra url neu da co roi thì them ki tu cuoi url
                if (ws_post('hi_url'.$idlang) != $url) {
                    $s_checkurl = "select url from $__tablenoidung where url = '$url'";
                    $d_checkurl = $db->rawQuery($s_checkurl);
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                }
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "select id from $__tablenoidung where idtype = '{$__postid}' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) {
                    // da tồn tại nội dung rồi thì update lại nội dung
                    // cap nhat lai du lieu
                    $aray_insert_lang = array("ten" => $ten,
                        "tieude"                        => $tieude,
                        "noidung"                       => $noidung,
                        "url"                           => $url,
                        "link"                          => $link,
                        "target"                        => $target,
                        "tukhoa"                        => $tukhoa,
                        "motatukhoa"                    => $motatukhoa,
                    );
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "idtype = '{$__postid}' and idlang = '{$idlang}'");
                } else {
                    $aray_insert_lang = array("idtype" => $__postid,
                        "idlang"                           => $idlang,
                        "ten"                              => $ten,
                        "tieude"                           => $tieude,
                        "noidung"                          => $noidung,
                        "url"                              => $url,
                        "link"                             => $link,
                        "target"                           => $target,
                        "tukhoa"                           => $tukhoa,
                        "motatukhoa"                       => $motatukhoa,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang
        // kiem tra neu thay hinh thi sẽ xóa hình cũ đi
        // xoa img
        if ($xoaimg == 1) {
            unlink("../uploads/option/" . $imgname);
            unlink("../uploads/option/thumb/" . $imgname);
            $aray_insert = array("img" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat img
        if ($img != '') {
            if ($imgname != '') {
                unlink("../uploads/option/" . $imgname);
                unlink("../uploads/option/thumb/" . $imgname);
            }
            //up file moi len
            $extfile = pathinfo($img, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/option/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/option/$imgfile");
            $ResizeImage->load("../uploads/option/" . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save("../uploads/option/thumb/" . $imgfile);
            $aray_insert = array("img" => $imgfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
    } // end if cap nhat
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $id = $subid;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $id;
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
