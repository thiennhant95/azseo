<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Hỗ trợ trực tuyến",
    "table"           => 'tbl_hotrotructuyen_type',
    "tablenoidung"    => 'tbl_hotrotructuyen_type_lang',
    "id_name"         => 'id',
    "id"              => 'id',
    "datatype"        => 0,
    "loai"            => 0,
    "thutu"           => 1,
    "anhien"          => 1,
    "ten"             => 1,
    "path_img"        => "../upload/option/",
    "path_file"       => "../upload/files/",
    "action"          => 1,
    "sizeimagesthumb" => 300);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
