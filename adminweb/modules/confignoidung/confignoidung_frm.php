<?php
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Process Thêm mới
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}
$_id_name = (isset($__config['id'])) ? $__config['id'] : null;
$_getop   = (ws_get('op')) ? ws_get('op') : null;
$_idtype  = (isset($__config['type'])) ? $__config['type'] : null;
$_idSP    = (isset($__config['idsp'])) ? $__config['idsp'] : null;
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
} else {
    $_getidSP = 1;
}
if ($__getid != '') {
    //*** Xử lý quá trình Chỉnh sửa ***//
    $array_data = get_data($__config['table'], '*', 'id,=,' . $__getid . '');
    $data       = count($array_data) ? $array_data[0] : null;

}
$id       = (!empty($data['id'])) ? $data['id'] : null;
$ten      = (!empty($data['ten'])) ? $data['ten'] : null;
$dinhdanh = (!empty($data['dinhdanh'])) ? $data['dinhdanh'] : null;
$anhien   = (!empty($data['anhien'])) ? $data['anhien'] : null;
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   var form = document.adminForm;
   // do field validation
   ';
// Kiểm tra tồn tại
if ($_idSP == 1) {
    if ($_getidSP != '') {
        $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' and idSP != '" . $idSP . "' ";
    } else {
        $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' ";
    }
    $d_idSP = $db->rawQuery($s_idSP);
    if (count($d_idSP) > 0) {
        foreach ($d_idSP as $keyidsp => $info_idSP) {
            $value_idSP = $info_idSP['idSP'];
            echo '
            else if (form.idSP.value == \'' . $value_idSP . '\'){
            alert( "Mã sản phẩm ' . $value_idSP . ' đã tồn tại! Vui lòng nhập mã khác!" );
            form.idSP.focus();
            }';
        }
    }
}
// Kiểm tra rỗng
if ($_idtype == 1) {
    echo '
      else if (form.idtype.value == ""){
         alert( "Bạn phải chọn một Loại sản phẩm" );
         form.idtype.focus();
      }';
}
// Hoàn thành validate ==> Submit frm
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
   <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . $id . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">';
//********** col right ********** //
$CONTENT .= '
<div class="col_data full">
<div class="container-fluid">
   <div class="row">

        <div class="col-sm-12 _left">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">' . $arraybien['cauhinhdanhmuc'] . '</div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>' . $arraybien['ten'] . '</label>
                        <input type="text" class="form-control" name="ten" value="' . $ten . '" placeholder="' . $arraybien['vuilongnhaptenkhoa'] . '" />
                    </div>
                    <div class="form-group">
                        <label>' . $arraybien['dinhdanh'] . '</label>
                        <input type="text" class="form-control" name="dinhdanh" value="' . $dinhdanh . '" placeholder="' . $arraybien['vuilongnhapkhoa'] . '" />
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>' . $arraybien['hienthi'] . '</label>
                            <div class="group-active">
                                <label>
                                    <input type="radio" name="anhien" value="1" ' . (($anhien == 1 || $__getid == '') ? 'checked="checked"' : null) . ' />
                                    ' . $arraybien['hien'] . '
                                </label>
                                <label>
                                    <input type="radio" name="anhien" value="0" ' . ($__getid != '' && $anhien == 0 ? 'checked="checked"' : null) . ' />
                                    ' . $arraybien['an'] . '
                                </label>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div><!-- End col6 Left -->

        <div class="col-sm-6 _right">
        </div><!-- End col6 Right -->

   </div><!-- End row -->
</div><!-- End Fluid -->
';
$CONTENT .= '
   <input type="hidden" value="' . ((isset($data['id'])) ? $data['id']:null) . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '
    </div><div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array(
    "{CONTENT}" => $CONTENT,
    "{ACTION}"  => $ACTION);
echo load_content($file_tempaltes, $array_bien);
