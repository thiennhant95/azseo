<?php
use Lazer\Classes\Database as Lazer;
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
$data = Lazer::table($__table)->find();
$kichhoat = $sotien = $phantram = 0;
if( $data->count() ) {
    $id                 = $data->id;
    $kichhoat           = $data->kichhoat;
    $sotien             = $data->sotien;
    $kieucongdiem       = $data->kieucongdiem;
    $congdiemphantram   = $data->congdiemphantram;
    $congdiemtien       = $data->congdiemtien;
    $kieugioithieu      = $data->kieugioithieu;
    $gioithieuphantram  = $data->gioithieuphantram;
    $gioithieutien      = $data->gioithieutien;
    $kieugioithieu2     = $data->kieugioithieu2;
    $gioithieuphantram2 = $data->gioithieuphantram2;
    $gioithieutien2     = $data->gioithieutien2;
}
    echo '
    <div class="panel-body tichdiem">
        <div class="col-sm-3" id="menuCol">
            <ul class="list-group">
                <li class="list-group-item '.(ws_get('tab')=='history'?'active':null).'">
                    <a href="'.$urlRequest.'&tab=history" title="">
                        <i class="fa fa-history"></i>
                        Lịch sử
                    </a>
                </li>
                <li class="list-group-item '.(ws_get('tab')=='status'?'active':null).'">
                    <a href="'.$urlRequest.'&tab=status" title="">
                       <i class="fa fa-sort" aria-hidden="true"></i>
                        Thống kê
                    </a>
                </li>
                <li class="list-group-item '.(ws_get('tab')=='settings'?'active':null).'">
                    <a href="'.$urlRequest.'&tab=settings"" title="">
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                        Cài đặt
                    </a>
                </li>
            </ul>
        </div><!-- /.col-sm-3 -->
        <div class="col-sm-9" id="contentCol">
            <div class="panel-body">
                <div class="content-title">';
                    switch( ws_get('tab') ) {
                        case 'status':
                            echo 'Thống kê tích điểm';
                            break;
                        case 'history':
                            echo 'Lịch sử tích điểm';
                            break;
                        case 'settings':
                            echo 'Cài đặt tích điểm';
                            break;
                        default:
                            echo 'Cài đặt tích điểm';
                            break;
                    }
                echo '
                </div><!-- /.content-title -->
                <hr />';
    $qtd_bonus = null;
    if( !$db->isSadmin() ) {
        $qtd_bonus = " WHERE iduser > 0
            AND iduser IS NOT NULL
            AND iduser != '' ";
    }
                    switch(ws_get('tab')) {
                        default:
                        case 'history':
                            echo '
                            <table id="lichsuTichdiem" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thành viên</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Số điểm</th>
                                        <th>Lịch sử</th>
                                        <th>Ngày</th>
                                    </tr>
                                </thead>
                                <tbody>';
                            $qtd = $db->rawQuery("SELECT *
                                FROM tbl_tichdiem {$qtd_bonus} ORDER BY id DESC
                            ");
                            if( count($qtd) > 0 ) {
                                foreach( $qtd as $ktd => $td ) {
                                    // checkdonhang
                                    $checkdh = $db->getNameFromID(
                                        "tbl_donhang",
                                        "id",
                                        "id",
                                        $td['iddonhang']
                                    );
                                    // checkUser
                                    $checkuser = $db->getNameFromID(
                                        "tbl_user",
                                        "id",
                                        "id",
                                        $td['iduser']
                                    );
                                    if( !$checkdh || !$checkuser ) {
                                        $db->where("id = {$td['id']}");
                                        $db->delete("tbl_tichdiem");
                                    }
                                    $ten = $db->getUser('ten', $td['iduser']);
                                    $ten = $ten ? $ten : '<i class="fa fa-user-times fa-2x" aria-hidden="true" style="font-size:25px;"></i>';
                                    $madonhang = $db->getNameFromID(
                                        'tbl_donhang',
                                        'madonhang',
                                        'id',
                                        $td['iddonhang']
                                    );
                                    echo '
                                    <tr>
                                        <td>'.($ktd+1).'</td>
                                        <td>'.$ten.'</td>
                                        <td>'.$madonhang.'</td>
                                        <td>'.round($td['sodiem'],3).'</td>
                                        <td>'.$td['lichsu'].'</td>
                                        <td>'.date('H:i:s - d/m/Y', strtotime($td['ngay'])).'</td>
                                    </tr>';
                                }
                            }
                                echo'
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thành viên</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Số điểm</th>
                                        <th>Lịch sử</th>
                                        <th>Ngày</th>
                                    </tr>
                                </tfoot>
                            </table>
                            ';
                            break;
                        case 'status':
                            echo '
                            <table id="thongkeTichdiem" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thành viên</th>
                                        <th>Số điểm</th>
                                        <th>Số Tiền</th>
                                    </tr>
                                </thead>
                                <tbody>';
                            $qq = $db->rawQuery("SELECT *
                                FROM tbl_tichdiem
                                {$qtd_bonus}
                                GROUP BY iduser
                                ORDER BY id DESC
                            ");
                            if( count($qq) > 0 ) {
                                foreach( $qq as $kqq => $tt ) {
                                    $ten = $db->getUser('ten', $tt['iduser']);
                                    $ten = $ten ? $ten : '<i class="fa fa-user-times fa-2x" aria-hidden="true" style="font-size:25px;"></i>';
                                    echo '
                                    <tr>
                                        <td>'.($kqq+1).'</td>
                                        <td>'.$ten.'</td>
                                        <td>'.(
                                            (int)($db->layTongDiem($tt['iduser']) < 0) ?
                                            0: $db->layTongDiem($tt['iduser'])
                                        ).'</td>
                                        <td>'.(
                                            (int)($db->layTongDiem($tt['iduser']) < 0) ?
                                            0: number_format(quydoi_tien($db->layTongDiem($tt['iduser']), $sotien))
                                        ).' VNĐ</td>
                                    </tr>';
                                }
                            }
                                echo'
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thành viên</th>
                                        <th>Số điểm</th>
                                        <th>Số Tiền</th>
                                    </tr>
                                </tfoot>
                            </table>
                            ';
                            break;
                        case 'settings':
                            echo '
                                <ul class="list-item">
                                    <li>
                                        <a class="item-view">
                                            <h3 class="item-label">
                                                Kích hoạt
                                            </h3>
                                            <span class="edit">
                                                <span class="edit-text">
                                                    <i class="fa fa-pencil"></i>
                                                    Chỉnh sửa
                                                </span><!-- /.edit-text -->
                                            </span><!-- /.edit -->
                                            <span class="item-content">';
                                            if( (int)$kichhoat ) {
                                                echo 'Đang kích hoạt';
                                            }
                                            else {
                                                echo 'Không kích hoạt';
                                            }
                                            echo'
                                            </span><!-- /.item-content -->
                                        </a>
                                        <div class="content">
                                            <form action="'.$urlRequest.'&method=query" method="post">
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="kichhoat" value="1" '.(
                                                            $kichhoat ? 'checked' : null
                                                        ).' />
                                                        Kích hoạt
                                                    </label>
                                                </div><!-- /.form-group -->
                                                <div class="group-btn">
                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                        <i class="fa fa-floppy-o"></i>
                                                        Lưu thay đổi</button>
                                                    <button type="button" class="btn btn-default btn-sm cancel">
                                                        <i class="fa fa-refresh"></i>
                                                        Hủy</button>
                                                </div><!-- /.form-group -->
                                                <input type="hidden" name="flag" value="kichhoat" />
                                                <input type="hidden" name="id" value="'.$id.'">
                                            </form>
                                        </div><!-- /.content -->
                                    </li>
                                    <li>
                                        <a class="item-view">
                                            <h3 class="item-label">
                                                Quy đổi điểm
                                            </h3>
                                            <span class="edit">
                                                <span class="edit-text">
                                                    <i class="fa fa-pencil"></i>
                                                    Chỉnh sửa
                                                </span><!-- /.edit-text -->
                                            </span><!-- /.edit -->
                                            <span class="item-content">
                                                1 điểm = '.number_format($sotien).' VNĐ
                                            </span><!-- /.item-content -->
                                        </a>
                                        <div class="content">
                                            <form action="'.$urlRequest.'&method=query" method="post">
                                                <div class="form-group">
                                                    1 điểm = <input type="text" class="input" value="'.$sotien.'" name="sotien" /> VNĐ
                                                </div><!-- /.form-group -->
                                                <div class="group-btn">
                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                        <i class="fa fa-floppy-o"></i>
                                                        Lưu thay đổi</button>
                                                    <button type="button" class="btn btn-default btn-sm cancel">
                                                        <i class="fa fa-refresh"></i>
                                                        Hủy</button>
                                                </div><!-- /.form-group -->
                                                <input type="hidden" name="flag" value="sotien" />
                                                <input type="hidden" name="id" value="'.$id.'">
                                            </form>
                                        </div><!-- /.content -->
                                    </li>
                                    <li>
                                        <a class="item-view">
                                            <h3 class="item-label">
                                                Cộng điểm mua hàng
                                            </h3><!-- /.item-label -->
                                            <span class="edit">
                                                <span class="edit-text">
                                                    <i class="fa fa-pencil"></i>
                                                    Chỉnh sửa
                                                </span><!-- /.edit-text -->
                                            </span><!-- /.edit -->
                                            <span class="item-content">';
                                                if( $kieucongdiem ) {
                                                    echo number_format($congdiemtien). ' VNĐ = 1 điểm';
                                                }
                                                else {
                                                    echo $congdiemphantram. '% giá trị đơn hàng';
                                                }
                                            echo'
                                            </span><!-- /.item-content -->
                                        </a><!-- /.item-view -->
                                        <div class="content">
                                            <form action="'.$urlRequest.'&method=query" method="post">

                                                <div class="head-check">
                                                    <label onClick="anhiendiv(\'#tap1\')">
                                                        <input type="radio" name="kieucongdiem" value="0" '.(
                                                            $kieucongdiem ? null : 'checked'
                                                        ).'>
                                                        Phần trăm của đơn hàng (%)
                                                    </label>
                                                    <label onClick="anhiendiv(\'#tap2\')">
                                                        <input type="radio" name="kieucongdiem" value="1" '.(
                                                            !$kieucongdiem ? null : 'checked'
                                                        ).'>
                                                        Tổng tiền của đơn hàng (đ)
                                                    </label>
                                                </div><!-- /.head-check -->

                                                <div class="body-check">
                                                    <div id="tap1" class="'.(
                                                        $kieucongdiem ? 'hide' : null
                                                    ).'">
                                                        <div class="form-group">
                                                            <input type="text" class="input" value="'.$congdiemphantram.'" name="congdiemphantram" /> % giá trị đơn hàng
                                                        </div><!-- /.form-group -->
                                                    </div><!-- /#tap1 -->

                                                    <div id="tap2" class="'.(
                                                        !$kieucongdiem ? 'hide' : null
                                                    ).'">
                                                        <div class="form-group">
                                                            <input type="text" class="input" value="'.$congdiemtien.'" name="congdiemtien" /> VNĐ = 1 điểm
                                                        </div><!-- /.form-group -->
                                                    </div><!-- /#tap2 -->
                                                </div><!-- /.body-check -->

                                                <div class="group-btn">
                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                        <i class="fa fa-floppy-o"></i>
                                                        Lưu thay đổi</button>
                                                    <button type="button" class="btn btn-default btn-sm cancel">
                                                        <i class="fa fa-refresh"></i>
                                                        Hủy</button>
                                                </div><!-- /.form-group -->
                                                <input type="hidden" name="flag" value="congdiem" />
                                                <input type="hidden" name="id" value="'.$id.'">
                                            </form>
                                        </div><!-- /.content -->
                                    </li>
                                    <li>
                                        <a class="item-view">
                                            <h3 class="item-label">
                                                Cộng điểm giới thiệu (A)
                                            </h3><!-- /.item-label -->
                                            <span class="edit">
                                                <span class="edit-text">
                                                    <i class="fa fa-pencil"></i>
                                                    Chỉnh sửa
                                                </span><!-- /.edit-text -->
                                            </span><!-- /.edit -->
                                            <span class="item-content">';
                                                if( $kieugioithieu ) {
                                                    echo number_format($gioithieutien). ' VNĐ = 1 điểm';
                                                }
                                                else {
                                                    echo $gioithieuphantram. '% giá trị đơn hàng';
                                                }
                                            echo'</span><!-- /.item-content -->
                                        </a><!-- /.item-view -->
                                        <div class="content">
                                            <form action="'.$urlRequest.'&method=query" method="post">

                                                <div class="head-check">
                                                    <label onClick="anhiendiv(\'#tap3\')">
                                                        <input type="radio" name="kieugioithieu" value="0" '.(
                                                            $kieugioithieu ? null : 'checked'
                                                        ).'>
                                                        Phần trăm của đơn hàng (%)
                                                    </label>
                                                    <label onClick="anhiendiv(\'#tap4\')">
                                                        <input type="radio" name="kieugioithieu" value="1" '.(
                                                            !$kieugioithieu ? null : 'checked'
                                                        ).'>
                                                        Tổng tiền của đơn hàng (đ)
                                                    </label>
                                                </div><!-- /.head-check -->

                                                <div class="body-check">
                                                    <div id="tap3" class="'.(
                                                        $kieugioithieu ? 'hide' : null
                                                    ).'">
                                                        <div class="form-group">
                                                            <input type="text" class="input" value="'.$gioithieuphantram.'" name="gioithieuphantram" /> % giá trị đơn hàng
                                                        </div><!-- /.form-group -->
                                                    </div><!-- /#tap3 -->

                                                    <div id="tap4" class="'.(
                                                        !$kieugioithieu ? 'hide' : null
                                                    ).'">
                                                        <div class="form-group">
                                                            <input type="text" class="input" value="'.$gioithieutien.'" name="gioithieutien" /> VNĐ = 1 điểm
                                                        </div><!-- /.form-group -->
                                                    </div><!-- /#tap4 -->
                                                </div><!-- /.body-check -->
                                                <div class="group-btn">
                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                        <i class="fa fa-floppy-o"></i>
                                                        Lưu thay đổi</button>
                                                    <button type="button" class="btn btn-default btn-sm cancel">
                                                        <i class="fa fa-refresh"></i>
                                                        Hủy</button>
                                                </div><!-- /.form-group -->
                                                <input type="hidden" name="flag" value="gioithieu" />
                                                <input type="hidden" name="id" value="'.$id.'">
                                            </form>
                                        </div><!-- /.content -->
                                    </li>
                                    <li>
                                        <a class="item-view">
                                            <h3 class="item-label">
                                                Cộng điểm giới thiệu (B)
                                            </h3><!-- /.item-label -->
                                            <span class="edit">
                                                <span class="edit-text">
                                                    <i class="fa fa-pencil"></i>
                                                    Chỉnh sửa
                                                </span><!-- /.edit-text -->
                                            </span><!-- /.edit -->
                                            <span class="item-content">';
                                                if( $kieugioithieu2 ) {
                                                    echo number_format($gioithieutien2). ' VNĐ = 1 điểm';
                                                }
                                                else {
                                                    echo $gioithieuphantram2. '% giá trị đơn hàng';
                                                }
                                            echo'</span><!-- /.item-content -->
                                        </a><!-- /.item-view -->
                                        <div class="content">
                                            <form action="'.$urlRequest.'&method=query" method="post">

                                                <div class="head-check">
                                                    <label onClick="anhiendiv(\'#tap5\')">
                                                        <input type="radio" name="kieugioithieu2" value="0" '.(
                                                            $kieugioithieu2 ? null : 'checked'
                                                        ).'>
                                                        Phần trăm của đơn hàng (%)
                                                    </label>
                                                    <label onClick="anhiendiv(\'#tap6\')">
                                                        <input type="radio" name="kieugioithieu2" value="1" '.(
                                                            !$kieugioithieu2 ? null : 'checked'
                                                        ).'>
                                                        Tổng tiền của đơn hàng (đ)
                                                    </label>
                                                </div><!-- /.head-check -->

                                                <div class="body-check">
                                                    <div id="tap5" class="'.(
                                                        $kieugioithieu2 ? 'hide' : null
                                                    ).'">
                                                        <div class="form-group">
                                                            <input type="text" class="input" value="'.$gioithieuphantram2.'" name="gioithieuphantram2" /> % giá trị đơn hàng
                                                        </div><!-- /.form-group -->
                                                    </div><!-- /#tap3 -->

                                                    <div id="tap6" class="'.(
                                                        !$kieugioithieu2 ? 'hide' : null
                                                    ).'">
                                                        <div class="form-group">
                                                            <input type="text" class="input" value="'.$gioithieutien2.'" name="gioithieutien2" /> VNĐ = 1 điểm
                                                        </div><!-- /.form-group -->
                                                    </div><!-- /#tap4 -->
                                                </div><!-- /.body-check -->
                                                <div class="group-btn">
                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                        <i class="fa fa-floppy-o"></i>
                                                        Lưu thay đổi</button>
                                                    <button type="button" class="btn btn-default btn-sm cancel">
                                                        <i class="fa fa-refresh"></i>
                                                        Hủy</button>
                                                </div><!-- /.form-group -->
                                                <input type="hidden" name="flag" value="gioithieu2" />
                                                <input type="hidden" name="id" value="'.$id.'">
                                            </form>
                                        </div><!-- /.content -->
                                    </li>
                                </ul>
                            ';
                            break;
                    }
                echo '
            </div><!-- /.panel-body -->
        </div><!-- /.col-sm-9 -->
    </div><!-- /.tichdiem -->';