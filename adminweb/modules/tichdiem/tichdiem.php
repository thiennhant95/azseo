<?php
    use Lazer\Classes\Database as Lazer;
    show_error();
    $__table        = "tichdiem";
    create_dir(LAZER_DATA_PATH, false);
    // CREATE TABLE
if( isset($_GET['removetable']) ) {
    Lazer::remove($__table);
}
try{
    \Lazer\Classes\Helpers\Validate::table($__table)->exists();
} catch(\Lazer\Classes\LazerException $e){
    Lazer::create($__table, [
        'id'                 => 'integer',
        'kichhoat'           => 'boolean',
        'sotien'             => 'integer',
        'kieucongdiem'       => 'integer',
        'congdiemphantram'   => 'string',
        'congdiemtien'       => 'integer',
        'kieugioithieu'      => 'integer',
        'gioithieuphantram'  => 'string',
        'gioithieutien'      => 'integer',
        'kieugioithieu2'     => 'integer',
        'gioithieuphantram2' => 'string',
        'gioithieutien2'     => 'integer',
    ]);
    $r = Lazer::table($__table);
    $r->kichhoat           = true;
    $r->sotien             = 1000;
    $r->kieucongdiem       = 0;
    $r->congdiemphantram   = '0.00001';
    $r->congdiemtien       = 100000;
    $r->kieugioithieu      = 0;
    $r->gioithieuphantram  = '0.000001';
    $r->gioithieutien      = 1000000;
    $r->kieugioithieu2     = 0;
    $r->gioithieuphantram2 = '0.000001';
    $r->gioithieutien2     = 1000000;
    $r->save();
}
    $url_reqquest = $db->getNameFromID(
        "tbl_user_group",
        "link",
        "link",
        "'./?op=".ws_get('op')."'"
    );
    $urlRequest = substr($url_reqquest, 2);
    switch( ws_get('method') ) {
        case 'query':
            include ws_get('op')."_query.php";
            break;
        default:
            include ws_get('op')."_frm.php";
            break;
    }