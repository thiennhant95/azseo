<?php
    use Lazer\Classes\Database as Lazer;
    mysql_query("
        CREATE TABLE IF NOT EXISTS `tbl_tichdiem` (
            `id` int(11) unsigned NOT NULL auto_increment,
            `iduser` int(11) NULL,
            `iddonhang` int(11) NULL,
            `anhien` tinyint(1) NOT NULL default '1',
            `sodiem` float(11,3) NULL,
            `lichsu` text NULL,
            `ngay` datetime NULL,
            PRIMARY KEY  (`id`)
        )
    ");
    $id = (int)ws_post('id');
    if ( !$id ) {
        /** THÊM MỚI DỮ LIỆU */
        $row = Lazer::table($__table);
    } else {
        $row = Lazer::table($__table)->find($id);
    }

    switch( ws_post('flag') ) {
        case 'kichhoat':
            $kichhoat = ws_post('kichhoat') ? true : false;
            $row->kichhoat = $kichhoat;
            break;

        case 'sotien':
            $sotien = (int)ws_post('sotien');
            $row->sotien = $sotien;
            break;

        case 'congdiem':
            $kieucongdiem     = intval(ws_post('kieucongdiem'));
            $congdiemphantram = strval(ws_post('congdiemphantram'));
            $congdiemtien     = intval(ws_post('congdiemtien'));
            $congdiemtien     = kieu_tien($congdiemtien);
            $congdiemtien     = intval($congdiemtien);

            $row->kieucongdiem     = $kieucongdiem;
            $row->congdiemphantram = $congdiemphantram;
            $row->congdiemtien     = $congdiemtien;
            break;

        case 'gioithieu':
            $kieugioithieu     = intval(ws_post('kieugioithieu'));
            $gioithieuphantram = strval(ws_post('gioithieuphantram'));
            $gioithieutien     = intval(ws_post('gioithieutien'));
            $gioithieutien     = kieu_tien($gioithieutien);
            $gioithieutien     = intval($gioithieutien);

            $row->kieugioithieu     = $kieugioithieu;
            $row->gioithieuphantram = $gioithieuphantram;
            $row->gioithieutien     = $gioithieutien;
            break;

        case 'gioithieu2':
            $kieugioithieu2     = intval(ws_post('kieugioithieu2'));
            $gioithieuphantram2 = strval(ws_post('gioithieuphantram2'));
            $gioithieutien2     = intval(ws_post('gioithieutien2'));
            $gioithieutien2     = kieu_tien($gioithieutien2);
            $gioithieutien2     = intval($gioithieutien2);

            $row->kieugioithieu2     = $kieugioithieu2;
            $row->gioithieuphantram2 = $gioithieuphantram2;
            $row->gioithieutien2     = $gioithieutien2;
            break;

        default:
            break;
    }
    $row->save();
    $link = "./?op=tichdiem&tab=settings";

echo '<script>
    location.href="'.$link.'";
</script>';
