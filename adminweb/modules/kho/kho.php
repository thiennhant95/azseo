<?php

$array_data = get_data("configsanpham", '*', 'anhien,=,1');
$_SESSION['array_data'] = array();
$_SESSION['array_data'] = $array_data;
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage       = new SimpleImage();
$ResizeImage_thumb = new SimpleImage();

$__config          = array(
    "module_title"   => $arraybien['modules_sanpham'],
    "table"          => 'tbl_noidung',
    "tablenoidung"   => 'tbl_noidung_lang',
    "jsontable"      => 'sizeimgproduct',
    "loai"           => array_search("product", $_arr_loai_noidung),
    "id"             => 'id',
    "id_name"        => 'idnoidung',
    "keymoudles"     => 'product',
    "urlwebsite"     => 0,
    "schedule"       => 0,
    "permission"     => 0,
    "idtype"         => 1,
    "masp"           => 1,
    "thanhphan"      => 1,
    "hinh"           => 1,
    "file"           => 1,
    "gia"            => 1,
    "giagoc"         => 1,
    "noibat"         => 1,
    "moi"            => 1,
    "banchay"        => 1,
    "home"           => 1,
    "khuyenmai"      => 1,
    "ngay"           => 1,
    "ngaycapnhat"    => 1,
    "inlogonoidung"  => 0,
    "solanxem"       => 1,
    "thutu"          => 1,
    "soluong"        => 1,
    "anhien"         => 1,
    "iduser"         => 1,
    "canvas"         => 1,
    "target"         => 1,
    "ten"            => 1,
    "tieude"         => 1,
    "url"            => 1,
    "link"           => 1,
    "mota"           => 1,
    "noidung"        => 1,
    "solanmua"       => 1,
    "tukhoa"         => 1,
    "motatukhoa"     => 1,
    "tag"            => 1,
    "action"         => 1,
    "add_item"       => 1,
    "option"         => 1,
    "path_img"       => "../uploads/noidung/",
    "path_file"      => "../uploads/files/",
    "chucnangkhac"   => 1,
    "action"         => 1,
    "imgwidth"       => 1500,
    "imgwidth_thumb" => 390,
    "compression"    => 80,
    "inlogo"         => 1,
    "vitrilogo"      => 0 // 0 can giua, 1 can tren trai, 2 can tren phai, 3 can duoi phai, 4 can duoi trai
);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];

add_col_sql("tbl_option_value", "idlang", "INT", "idnoidung");
add_col_sql($__table, "solanmua", "INT", "solanxem");
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} elseif ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}


if ( ws_get('sql') ) {


    // cap nhat du lieu tu data cu
    $s = "SELECT * from tbl_sanpham order by id ASC";
    $d = $db->rawQuery($s);
    foreach ($d as $keyd => $infod) {
        $idSP           = $infod['idSP'];
        $Ten_vie        = $infod['Ten_vie'];
        $Ten_eng        = $infod['Ten_eng'];
        $idNhanHieu     = $infod['idNhanHieu'];
        $idtype         = $infod['idtype'];
        $hinh        = $infod['hinh'];
        $NoiDung_vie    = $infod['NoiDung_vie'];
        $NoiDung_eng    = $infod['NoiDung_eng'];
        $Gia            = $infod['Gia'];
        $MoTa_vie       = $infod['MoTa_vie'];
        $MoTa_eng       = $infod['MoTa_eng'];
        $SanPhamNoiBat  = $infod['SanPhamNoiBat'];
        $SanPhamMoi     = $infod['SanPhamMoi'];
        $SanPhamBanChay = $infod['SanPhamBanChay'];
        $Ngay           = $infod['Ngay'];
        $NgayCapNhat    = $infod['NgayCapNhat'];
        $SetHomePage    = $infod['SetHomePage'];
        $AnHien         = $infod['AnHien'];
        $SoLanXem       = $infod['SoLanXem'];
        $masp           = $idSP;
        $gia            = $Gia;
        $noibat         = $SanPhamNoiBat;
        $moi            = $SanPhamMoi;
        $banchay        = $SanPhamBanChay;
        $ngay           = $Ngay;
        $ngaycapnhat    = $NgayCapNhat;
        $solanxem       = $SoLanXem;
        $loai           = 1;
        $anhien         = $AnHien;
        $hinh           = $hinh;

        // truy van cap nhat id theo id moi
        $a = "select a.id,b.idcu,b.ten from tbl_danhmuc AS a INner join tbl_danhmuc_lang AS b ON a.id = b.iddanhmuc where  b.idlang = $__defaultlang and b.idcu = '".$idtype."' order by b.ten Asc";
        $d = $db->rawQuery($a);
        $idtype = $d[0]['id'];

        $aray_insert = array(
            "masp"        => $masp,
            "hinh"        => $hinh,
            "idtype"      => $idtype,
            "file"        => $file,
            "gia"         => $gia,
            "giagoc"      => $giagoc,
            "ngay"        => $ngay,
            "ngaycapnhat" => $ngaycapnhat,
            "solanxem"    => $solanxem,
            "thutu"       => $thutu,
            "loai"        => $loai,
            "soluong"     => $soluong,
            "anhien"      => $anhien,
            "iduser"      => $_SESSION['user_id']
        );

        $id_insert = $db->insert("tbl_noidung", $aray_insert);

        if ( $id_insert == '' ) {
            continue;
        }


        if ($hinh != '') {
            $arrimg = array(
                'hinh'        => $hinh,
                'idnoidung'   =>  $id_insert,
                'ngaytao'     =>  $db->getDateTimes(),
                'hinhdaidien' =>  1,
                'tmp'         =>  0
            );
            $db->insert('tbl_noidung_hinh', $arrimg);
        }
        $s_lang = "
        select a.id,a.idkey,b.ten
        from tbl_lang AS a
        inner join tbl_lang_lang AS b On  a.id = b.iddanhmuc
        where b.idlang = '".$__defaultlang."'
        and a.anhien = 1
        order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang)>0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang    = $info_lang['ten'];
                $idlang     = $info_lang['id'];
                $idkey      = $info_lang['idkey'];
                if ($idlang==1) {
                    $ten         =  $Ten_vie;
                    $tieude      =  $ten;
                    $title_url   = tao_url($ten);
                    $title_url   = strtolower($title_url);
                    $name        = ws_get('name');
                    $rannumber   = rand(100, 999);
                    $urlcheck    = $db->getNameFromID("tbl_noidung_lang", "url", "url", "'".$title_url."'");
                    if ($urlcheck != '') {
                        $urlreturn   =$title_url.'-'.$rannumber;
                    } else {
                        $urlreturn   =$title_url;
                    }
                    $mota        = $MoTa_vie;
                    $tukhoa      = $ten.', '.tao_url($ten);
                    $motatukhoa  = $ten;
                    $noidung     = $NoiDung_vie;
                } elseif ($idlang==3) {
                    $ten         =  $Ten_eng;
                    $tieude      =  $ten;
                    $title_url   = tao_url($ten);
                    $title_url   = strtolower($title_url);
                    $name        = ws_get('name');
                    $rannumber   = rand(100, 999);
                    $urlcheck    = $db->getNameFromID("tbl_noidung_lang", "url", "url", "'".$title_url."'");
                    if ($urlcheck != '') {
                        $urlreturn   =$title_url.'-'.$rannumber;
                    } else {
                        $urlreturn   =$title_url;
                    }
                    $mota        = $MoTa_eng;
                    $tukhoa      = $ten.', '.tao_url($ten);
                    $motatukhoa  = $ten;
                    $noidung     = $NoiDung_eng;
                }
                $aray_insert_lang = array(
                    "idnoidung"  => $id_insert,
                    "idlang"     => $idlang,
                    "ten"        => $ten,
                    "tieude"     => $tieude,
                    "url"        => $urlreturn,
                    "link"       => $link,
                    "target"     => $target,
                    "mota"       => $mota,
                    "noidung"    => $noidung,
                    "tukhoa"     => $tukhoa,
                    "motatukhoa" => $motatukhoa,
                    "tag"        => $tag
                );
                $db->insert("tbl_noidung_lang", $aray_insert_lang);
            }
        }
    }
}