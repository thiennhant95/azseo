<?php
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Process Thêm mới
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}
$_id_name = $__config['id'];
$_getop   = ws_get('op');
$_idtype  = $__config['type'];
$_idSP    = $__config['idsp'];
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
//*** Xử lý quá trình Chỉnh sửa ***//
if ($__getid != '') {
    $__cid     = ws_get('id');
    $s_sanpham = "SELECT * FROM $__table WHERE id = $__getid";
    $data      = $db->rawQuery($s_sanpham);
    $data      = $data[0];
    $id        = $data['id'];
    $noidung   = $data['noidung'];
    $iduser    = $data['iduser'];
    $img_view  = $__config['path_img'] . $img;
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   var form = document.adminForm;
   // do field validation
   ';
// Kiểm tra tồn tại
if ($_idSP == 1) {
    if ($_getidSP != '') {
        $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' and idSP != '" . $idSP . "' ";
    } else {
        $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' ";
    }
    $d_idSP = $db->rawQuery($s_idSP);
    if (count($d_idSP) > 0) {
        foreach ($d_idSP as $keyidsp => $info_idSP) {
            $value_idSP = $info_idSP['idSP'];
            echo '
            else if (form.idSP.value == \'' . $value_idSP . '\'){
            alert( "Mã sản phẩm ' . $value_idSP . ' đã tồn tại! Vui lòng nhập mã khác!" );
            form.idSP.focus();
            }';
        }
    }
}
// Kiểm tra rỗng
if ($_idtype == 1) {
    echo '
      else if (form.idtype.value == ""){
         alert( "Bạn phải chọn một Loại sản phẩm" );
         form.idtype.focus();
      }';
}
// Hoàn thành validate ==> Submit frm
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['dong'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
   <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info hidden">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
$CONTENT .= '
         </div>
      </div>
</div>';
//********** col right ********** //
$CONTENT .= '
<div class="col_data setwidth">
<div class="panel panel-default">
  <div class="panel-heading thuoctinhtitle">&nbsp;THÔNG TIN CHI TIẾT</div>
  <div class="panel-body box-lienhe">';
if ($data['ten'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['hoten'] . '</div>
            <div class="group-active">
               <label for="hoten">' . $data['ten'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['email'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['email'] . '</div>
            <div class="group-active">
               <label for="email">' . $data['email'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['diachi'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['diachi'] . '</div>
            <div class="group-active">
               <label for="diachi">' . $data['diachi'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['dienthoai'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['dienthoai'] . '</div>
            <div class="group-active">
               <label for="dienthoai">' . $data['dienthoai'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['fax'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['fax'] . '</div>
            <div class="group-active">
               <label for="fax">' . $data['fax'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['congty'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['congty'] . '</div>
            <div class="group-active">
               <label for="congty">' . $data['congty'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['website'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['website'] . '</div>
            <div class="group-active">
               <label for="website">' . $data['website'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['ip'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['ip'] . '</div>
            <div class="group-active">
               <label for="ip">' . $data['ip'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['ngaytao'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['ngaygui'] . '</div>
            <div class="group-active">
               <label for="ngaytao">' . $data['ngaytao'] . '</label>
            </div>
         </div>
      </div>
     ';
}
if ($data['noidung'] != "") {
    $arrND = preg_replace('/<[^>]*>/', '', $data['noidung']);
    $arrND = json_decode($arrND, true);
    $CONTENT .= '
      <div class="panel panel-default">
         <div class="panel-heading">
         <div class="panel-title"><i class="fa fa-hand-o-right"></i>&nbsp;
             <a href="../'.$db->getNameFromID("tbl_noidung_lang", "url", "idnoidung", $arrND['id']).'" title="" target="_blank">
                            '.$db->getNameFromID("tbl_noidung_lang", "ten", "idnoidung", $arrND['id']).'
                        </a>
         </div>
         </div>
         <div class="panel-body">';
         foreach ($arrND as $kND => $vND) {
            $CONTENT.='
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">'.$arraybien[$kND].'</span>
                    <input type="text" value="'.$vND.'" class="form-control" readonly style="background-color:white;padding: 6px 12px;font-size: 14px;" />
                </div>
            </div>';
         }
$CONTENT .= '
         </div>
      </div>
     ';
}
$CONTENT .= '
      </div>
   </div>
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '</div>'; // end col data
$CONTENT .= '<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
