<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Templates mẫu",
    "table"           => 'tbl_layout_template',
    "tablenoidung"    => 'tbl_layout_template_lang',
    "id_name"         => 'id',
    "id"              => 'id',
    "datatype"        => 0,
    "noidung_tbl"     => 1,
    "loai"            => 0,
    "hinh"            => 1,
    "idkeyname"       => 1,
    "thutu"           => 1,
    "anhien"          => 1,
    "noidung"         => 1,
    "ten"             => 1,
    "path_img"        => "../uploads/layout/",
    "path_file"       => "../uploads/files/",
    "action"          => 1,
    "sizeimagesthumb" => 300,
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
