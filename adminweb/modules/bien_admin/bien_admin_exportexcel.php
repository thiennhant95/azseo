<?php
    require dirname(dirname(dirname(__DIR__))) . "/configs/inc.php";
$chucai = array(
    1 =>  "A",
    2 =>  "B",
    3 =>  "C",
    4 =>  "D",
    5 =>  "E",
    6 =>  "F",
    7 =>  "G",
    8 =>  "H",
    9 =>  "I",
    10 =>  "J",
    11 =>  "K",
    12 =>  "L",
    13 =>  "M",
    14 =>  "N",
    15 =>  "O",
    16 =>  "P",
    17 =>  "Q",
    18 =>  "R",
    19 =>  "S",
    20 =>  "T",
    21 =>  "U",
    22 =>  "V",
    23 =>  "W",
    24 =>  "X",
    25 =>  "Y",
    26 =>  "Z"
);



$s="select * from tbl_bien_admin group by idkey";
$d = $db->rawQuery($s);
$soluongbien = count($d);
     $vitridong = 2;
     if($soluongbien >0){
        // xuat ra file excel
        require_once '../../plugin/PHPExcel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();

        // truy van lay ngon ngu
        $s_lang = "select a.id,a.idkey,b.ten
                   from tbl_lang AS a
                   inner join tbl_lang_lang AS b
                   On a.id = b.idlang
                   and a.id = b.iddanhmuc
                   where a.anhien = 1
                   order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        $d_lang2 = $db->rawQuery($s_lang);
        $solang = count($d_lang);
        $kitu   = $chucai[$solang+1];


        $objPHPExcel->getActiveSheet()->setTitle("Sản Phẩm");
        $objPHPExcel->getActiveSheet()->getStyle("A1".":".$kitu."1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A2".":".$kitu."2")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1".":".$kitu."1")->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->freezePaneByColumnAndRow(($solang+1),3);

        // background
        $objPHPExcel->getActiveSheet()->getStyle("A3".":".$kitu."3")->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'f00')
                )
            )
        );

        // background
        $objPHPExcel->getActiveSheet()->getStyle("A2".":".$kitu."2")->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'dfdfdf')
                )
            )
        );

        // center text
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle("A1".":".$kitu."1")->applyFromArray($style);

/*        // Set the entire worksheet to locked
        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        // Unprotect those cells that you want to make editable
        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:B3')
            ->getProtection()->setLocked(
                PHPExcel_Style_Protection::PROTECTION_UNPROTECTED
            );
*/
        // set row height
        foreach($objPHPExcel->getActiveSheet()->getRowDimensions() as $rd) {
            $rd->setRowHeight(80);
        }


        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells("A1:".$kitu."1")
        ->setCellValue("A1", "DANH SÁCH BIẾN")
        ->setCellValue('A2', 'Tên biến')
        ->setCellValue('A3', 'idLang');
        foreach ($d_lang as $key_lang => $info_lang) {
            $tenlang = $info_lang['ten'];
            $idlang  = $info_lang['id'];
            $tenbien   = $chucai[$key_lang+2];
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($tenbien.'2', $tenlang);
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($tenbien.'3', $idlang);
        }


        $vitridongsp = 3;
        foreach($d as $idsp => $info_bien){
            $vitridongsp +=1;
            $idsp2 = $idsp;

            $idkey   = $info_bien['idkey'];
            $ten     = $info_bien['ten'];

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$vitridongsp, $idkey);
            // set bg cho dong chan
            if($vitridongsp%2==0){
                // background
                $objPHPExcel->getActiveSheet()->getStyle('A'.$vitridongsp.':'.$kitu.$vitridongsp)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'f3f3f3')
                        )
                    )
                );
            }

            // lap id lang theo cau truy van o tren
            foreach ($d_lang2 as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $tenbien   = $chucai[$key_lang+2];
                $tieude = $db->getNameFromID("tbl_bien_admin","ten","idkey","'".$idkey."' and idlang = '".$idlang."'");
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($tenbien.$vitridongsp, $tieude);



            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="bien.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        $_SESSION['array_cid'] = '';
    }else{ // neu chua chon muc nao ma scrip khong băt loi thi qua day
        echo'<script>swal("Vui lòng chọn từ danh sách"); </script>';
    }
?>
