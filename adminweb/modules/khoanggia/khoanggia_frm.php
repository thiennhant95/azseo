<?php
// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    if ($__sua == 0) {
        echo '<script language="javascript">
alert("' . $arraybien['khongcoquyensua'] . '");
location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
} else {
    // la them
    if ($__them == 0) {
        echo '<script language="javascript">
alert("' . $arraybien['khongcoquyenthem'] . '");
location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
}
$_getop  = ws_get('op');
$ACTION  = '';
$CONTENT = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
// tao file tmp de uploadhinh
$tmpimg             = time();
$_SESSION['tmpimg'] = $tmpimg;
if ($__getid != '') {
    $__cid     = ws_get('id');
    $s_sanpham = "SELECT * from {$__table} where id = '{$__getid}'";
    $data      = $db->rawQuery($s_sanpham);
    $data      = $data[0];
    $id        = check_select($data['id']);
    $ten       = check_select($data['ten']);
    $thutu     = check_select($data['thutu']);
    $loai      = check_select($data['loai']);
    $anhien    = check_select($data['anhien']);
    $khoanga   = check_select($data['khoanga']);
    $khoangb   = check_select($data['khoangb']);
} else {
    // set nhung gia tri mặc định khi thêm mới vào đây
    $solanxem = 0;
    $thutu    = 1;
    $loai     = $__config['loai'];
    $anhien   = 1;
    $noidung  = '<div id="getimgnoidung"></div>';
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
var form = document.adminForm;
// do field validation
if (pressbutton == \'cancel\') {
submitform( pressbutton );
return;
} else if (form.ten.value == "") {
alert(\'Tên không được để trống !\');
form.ten.focus();
} ';
echo '
else {
submitform( pressbutton );
}
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
<a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
<i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
</a>
';
if (ws_get('id')) {
    $geturl_xemtruoc = $db->getNameFromID("tbl_noidung_lang", "url", "idnoidung", "'" . ws_get('id') . "' and idlang = '{$__defaultlang}'");
    $ACTION .= '<a class="btn btn-default" data-toggle="tooltip" title="' . $arraybien['xemchuyenmuctrenwebsite'] . '" href="../' . $geturl_xemtruoc . '" target="_blank" >
    <i class="fa fa-external-link"></i>
    </a>';
}
$ACTION .= '
<a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
<i class="fa fa-floppy-o"></i> ' . $arraybien['luuvadong'] . '
</a>
<a class="btn btn-default" onclick="javascript: submitbutton_addnew(\'saveandnew\')" href="#">
<i class="fa fa-clone"></i> ' . $arraybien['luuvathem'] . '
</a>
<a class="btn btn-success" href="./?op=' . ws_get('op') . '">
<i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
</a>
<a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
<i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
</a>
</div>
<div class="clear"></div>';
// col left
$CONTENT .= '
<form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
<div class="panel panel-default">
<div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
<div class="panel-body">';

if ($__config['anhien'] == 1) {
    $CONTENT .= '
<div class="checkbox">
<label>';
    $checked_anhien = ($anhien == 1) ? ' checked="checked" ' : null;
    $CONTENT .= '<input value="1" type="checkbox" ' . $checked_anhien . ' name="anhien" id="anhien" />';
    $CONTENT .= '
' . $arraybien['hienthi'] . '</label>
</div>';
}

$CONTENT.='
<div class="form-group">
    <label>Khoảng giá
        <i class="fa fa-info-circle" data-toggle="tooltip" title="Nếu muốn nhập số nhỏ hơn thì để trống ô1 - Nếu muốn nhập số lớn hơn thì để trống ô2"></i>
    </label>
    <div class="input-group">
        <input class="form-control" value="'.$khoanga.'" name="khoanga" />
        <span class="input-group-addon">-</span>
        <input class="form-control" value="'.$khoangb.'" name="khoangb" />
        <span class="input-group-addon">đ</span>
    </div>
</div>';

$CONTENT .= '
</div>
</div>
</div>';
// col right
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
<div class="panel-heading"><strong>&nbsp;ĐIỀN THÔNG TIN</strong></div>
<div class="panel-body">
<div class="adminform">';
if ($__config['ten'] == 1) {
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">' . $arraybien['ten'] . '</label>
<input type="text" name="ten" id="ten" class="form-control" value="' . @$ten . '" />
</div>';
}
if (!$__config['mota']) {
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">Chức vụ ' . $tenlang . '</label>
<textarea class="form-control seo-placeholder"  name="mota' . $idlang . '" cols="100" rows="5">' . $mota . '</textarea>
</div>';
}
if (!$__config['noidung']) {
    // up multi hinh
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">Ý kiến ' . $tenlang . '</label>
<textarea name="noidung' . $idlang . '" id="noidung' . $idlang . '" cols="" rows="" class="ckeditor">' . $noidung . '</textarea>';
    $CONTENT .= '</div>';
}
if (!$__config['url']) {
    $CONTENT .= '
<div class="form-group">
<label class="control-label strong" for="Alias">' . $arraybien['duongdan'] . ' ' . $tenlang . '</label>
<div class="row_countkitu">' . $arraybien['chieudaitoiuuchoseo'] . ' <div class="sokitutoida">/115</div><div class="kitudadung" id="kitudadungurl' . $idlang . '">' . strlen($url) . '</div></div>
<div class="controls">
<div class="input-group">
<span class="input-group-addon">http://' . $_SERVER['HTTP_HOST'] . '/</span>
<input  onkeyup="CountKey(\'url' . $idlang . '\',\'kitudadungurl' . $idlang . '\');" class="form-control seo-placeholder" placeholder="URL chuyên mục, Bạn có thể nhập dạng: chuyen-muc" name="url' . $idlang . '" type="text" id="url' . $idlang . '" value="' . $url . '" size="70" />
<input type="hidden" value="' . $url . '" name="hi_url' . $idlang . '" />
</div>
</div>
</div>';
}
if (!$__config['link']) {
    $CONTENT .= '
<div class="input-group">
<span class="input-group-addon">' . $arraybien['lienket'] . ' ' . $tenlang . '</span>
<input class="form-control" placeholder="Nhập link nếu bạn muốn chuyển hướng đến một link khác: VD: http://webso.vn"  name="link' . $idlang . '" type="text" id="link' . $idlang . '" value="' . $link . '"  />
</div><br />
<div class="input-group">
<span class="input-group-addon" >' . $arraybien['phuongthuc'] . '</span>
<div class="col-lg-3 margin0 padding0">
<select name="target' . $idlang . '" id="target' . $idlang . '" class="form-control col-lg-2 seo-placeholder" >';
    foreach ($_arr_target as $key_target => $value_target) {
        if ($key_target == $target) {
            $CONTENT .= '<option selected="selected" value="' . $key_target . '">' . $value_target . '</option>';
        } else {
            $CONTENT .= '<option value="' . $key_target . '">' . $value_target . '</option>';
        }
    }
    $CONTENT .= '</select>';
    $CONTENT .= '
</div>
</div>
<div class="clear"></div><br />';
}
if (!$__config['tieude']) {
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">' . $arraybien['tieudetrang'] . ' ' . $tenlang . '</label>
<div class="row_countkitu">' . $arraybien['sokitudadung'] . ': <div class="sokitutoida">/70</div><div class="kitudadung" id="kitudadungtieude' . $idlang . '">' . strlen($tieude) . '</div></div>
<input class="form-control" onkeyup="CountKey(\'tieude' . $idlang . '\',\'kitudadungtieude' . $idlang . '\');"  name="tieude' . $idlang . '" type="text" id="tieude' . $idlang . '" value="' . $tieude . '"  />
</div>';
}
if (!$__config['motatukhoa']) {
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">' . $arraybien['motatukhoa'] . ' ' . $tenlang . '</label>
<div class="row_countkitu">' . $arraybien['sokitudadung'] . ': <div class="sokitutoida">/320</div><div class="kitudadung" id="kitudadungmota' . $idlang . '">' . strlen($motatukhoa) . '</div></div>
<textarea class="form-control" onkeyup="CountKey(\'motatukhoa' . $idlang . '\',\'kitudadungmota' . $idlang . '\');" placeholder="Mô tả từ khóa nhập ít hơn 320 kí tự"  id="motatukhoa' . $idlang . '" name="motatukhoa' . $idlang . '" cols="100" rows="3">' . $motatukhoa . '</textarea>
</div>';
}
if (!$__config['tukhoa']) {
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">' . $arraybien['tukhoa'] . ' ' . $tenlang . '</label>
<textarea class="form-control" placeholder="Nhập từ khóa cách nhau bằng dấu \',\' phẩy"  id="tukhoa' . $idlang . '" name="tukhoa' . $idlang . '" cols="100" rows="1">' . $tukhoa . '</textarea>
</div>';
}
if (!$__config['tag']) {
    $CONTENT .= '
<div class="form-group">
<label for="inputdefault">Tag ' . $tenlang . '</label>
<textarea placeholder="Mỗi từ khóa cách nhau bằng dấu | " class="form-control" name="tag' . $idlang . '" cols="100" rows="2">' . $tag . '</textarea>
</div>';
}
$CONTENT .= '
</div>
</div>';
$CONTENT .= '
<input type="hidden" value="' . $__getid . '" name="id">
<input type="hidden" value="" name="cid[]">
<input type="hidden" value="0" name="version">
<input type="hidden" value="0" name="mask">
<input type="hidden" value="' . $__getop . '" name="op">
<input type="hidden" value="" name="task">
<input type="hidden" value="" name="luuvathem">
<input type="hidden" value="" name="saveandnew">
<input type="hidden" value="'.ws_get('page').'" name="getpage">
</div>'; // end tab-text
$CONTENT .= '
</div>'; // end col data
$CONTENT .= '
<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array(
    "{CONTENT}" => $CONTENT,
    "{ACTION}"  => $ACTION);
echo load_content($file_tempaltes, $array_bien);
