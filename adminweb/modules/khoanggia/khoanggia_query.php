<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    #
    # TRƯỜNG HỢP ẨN TIN
    # ----------------------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'publish') {
    #
    # TRƯỜNG HỢP HIỆN TIN
    # ------------------------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'saveorder') {
    #
    # TRƯỜNG HỢP SẮP XẾP TIN
    # ------------------------------------
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
    $db->thuTu($__table, $__id, "thutu", "loai", $__config['loai']);
} elseif ($__post_task == 'excel_export') {
    $array_cid             = ws_post('cid');
    $_SESSION['array_cid'] = $array_cid;
    echo '<script>location.href="modules/noidung/noidung_exportexcel.php";</script>';
} elseif ($__post_task == 'remove') {
    #
    # TRƯỜNG HỢP XÓA TIN
    # --------------------------------
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";
        //
        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            // xoa hinh trong bang tbl_noidung_hinh
            $s_hinh_chitiet = "select hinh from tbl_noidung_hinh where idnoidung = '" . $id_order . "' ";
            $d_hinh_chitiet = $db->rawQuery($s_hinh_chitiet);
            if (count($d_hinh_chitiet) > 0) {
                foreach ($d_hinh_chitiet as $key_hinh_chitiet => $info_hinh_chitiet) {
                    if (file_exists($__config["path_img"] . $info_hinh_chitiet['hinh'])) {
                        unlink($__config["path_img"] . $info_hinh_chitiet['hinh']);
                    }
                    if (file_exists($__config["path_img"] . "thumb/" . $info_hinh_chitiet['hinh'])) {
                        unlink($__config["path_img"] . "thumb/" . $info_hinh_chitiet['hinh']);
                    }
                }
            }
            if ($value_file != '') {
                if (file_exists($__config['path_file'] . $value_file)) {
                    unlink($__config['path_file'] . $value_file);
                }
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idnoidung  = '" . $id_order . "' ");
            // xoa bang tbl_noidung_hinh
            $db->sqlDelete("tbl_noidung_hinh", " idnoidung  = '" . $id_order . "' ");
        }
    }
}
/** Tao thu muc neu chua ton tai */
create_dir($__config['path_img']);
create_dir($__config['path_file'], false);

if ($__getaction == 'save') {
    #
    # TRƯỜNG HỢP LƯU TIN
    # ----------------------------------------
    $thutu       = 1;
    $loai        = $__config['loai'];
    $idtype      = trim(ws_post('idtype'));
    $ngay        = trim(ws_post('ngay'));
    $ten         = trim(ws_post('ten'));
    $solanxem    = trim(ws_post('solanxem'));
    $soluong     = trim(ws_post('soluong'));
    $anhien      = trim(ws_post('anhien'));
    $khoanga     = trim(ws_post('khoanga'));
    $khoangb     = trim(ws_post('khoangb'));
    $_getidSP    = "";
    $ngay        = $db->getDateTimes();
    $ngaycapnhat = $db->getDateTimes();
    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $images_name = ws_post('url'.$__defaultlang);
    // lay id danh muc
    $_postselector = ws_post('selector');
    $str_selector  = '';
    if (count($_postselector) > 0) {
        for ($i = 0; $i < count($_postselector); $i++) {
            if ($i > 0) {
                $str_selector .= ',' . $_postselector[$i];
            } else {
                $str_selector .= $_postselector[$i];
            }
        }
    }

    /**
     * Them moi du lieu
     */
    if ( $__getid == '' ) {
        #
        # TRƯỜNG HỢP THÊM MỚI DỮ LIỆU
        # ----------------------------------------------
        $sql_update = "update $__table set thutu = thutu+1 where 1 = 1 ";
        $db->rawQuery($sql_update);
        // up load icon
        $id_insert = $db->insert($__table, array(
            "ten"     => $ten,
            "thutu"   => $thutu,
            "anhien"  => $anhien,
            "khoanga" => $khoanga,
            "khoangb" => $khoangb

        ));

    /**
     * Cap nhat du lieu
     */
    } elseif ( $__getid != '' ) {
        #
        # TRƯỜNG HỢP CẬP NHẬT DỮ LIỆU
        # ----------------------------------------------------
        $id_update = $db->sqlUpdate($__table, array(
            "ten"     => $ten,
            "anhien"  => $anhien,
            "khoanga" => $khoanga,
            "khoangb" => $khoangb
        ), "id = {$__postid} ");

    }
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if ($_SESSION['idtype'] != '') {
    $link_redirect .= '&idtype=' . $_SESSION['idtype'];
}
if (ws_post('luuvathem') == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $id;
}
if (ws_post('saveandnew') == 'saveandnew') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm';
}
if (ws_post('save') == 'saveandnew') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
}

echo '<script> location.href="' . $link_redirect . '"; </script>';
