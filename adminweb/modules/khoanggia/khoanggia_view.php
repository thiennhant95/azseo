<?php
// sap xep thu tu khi load lan dau
if ( $_SESSION['sapxepthutu_modules'] != ws_get('op') ) {
    $_SESSION['sapxepthutu_modules'] = ws_get('op');
    $db->thuTu($__config['table'], $__config['id'], "thutu", "loai", $__config['loai']);
}
if( $_SESSION['sapxepthutu_modules'] == ws_get('op') ) {
    $_SESSION['idtype'] = ws_get('idtype');
}
// luu session idtype dang chon
$_id_name = $__config['id_name'];
$s        = "SELECT * FROM {$__table} WHERE  1 = 1 ";
$id       = ws_get('key');
$ten      = ws_get('ten');
$loai     = ws_get('loai');
$ngaydang = ws_get('ngaydang');
$anhien   = ws_get('anhien');
$get_page = ws_get('page') ? ws_get('page') : 1;
if ($__gettukhoa != '') {
    $s = $s . " and (ten LIKE '%{$__gettukhoa}%' OR a.id LIKE '%{$__gettukhoa}%' ) ";
}
if ($__getidtype != '') {
    $s = $s . " AND a.idtype LIKE '{$__getidtype}%' ";
}
if ($__getanhien != '') {
    $s = $s . " AND anhien = '{$__getanhien}' ";
}
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . ' order by  ' . $__sortname . '  ' . $__sortcurent;
} else {
    $s = $s . ' order by  thutu  asc ';
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = count($array_data);
$total_row    = count($d_counttotal);
$page         = (int) !ws_get('page') ? 1 : ws_get('page');
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . ' ';
}

$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
echo '
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">' . $arraybien['capnhathinhbaiviet'] . '</h4>
      </div>
      <div class="modal-body" id="loadhinhchitiet">
      </div><div class="clear"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">' . $arraybien['dong'] . '</button>
      </div>
    </div>
  </div>
</div>
<div class="row_content_top">
   <div class="row_content_top_title">' . $__config['module_title'] . '</div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder(' . ($saveorder - 1) . ', \'saveorder\')">
               <i class="fa fa-sort-amount-desc"></i>' . $arraybien['sapxep'] . '
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert(\'Vui lòng lựa chọn từ danh sách\');}else{  submitbutton(\'publish\')}" href="#">
            <i class="fa fa-check-circle"></i>' . $arraybien['bat'] . '
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert(\'Vui lòng lựa chọn từ danh sách\');}else{  submitbutton(\'unpublish\')}" href="#">
          <i class="fa fa-check-circle color-black"></i> ' . $arraybien['tat'] . '
        </a>
';
if ($__xoa == 1) {
    echo '
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert(\'Vui lòng lựa chọn từ danh sách\');}else{  submitbutton_del(\'remove\')}" href="#">
        <i class="fa fa-times-circle color-black"></i> ' . $arraybien['xoa'] . '
        </a>';
}
if ($__them == 1) {
    echo '<a class="btn btn-primary" href="./?op=' . $__getop . '&method=frm">
         <i class="fa fa-plus-circle"></i> ' . $arraybien['themmoi'] . '
        </a>';
}
echo '<a class="btn btn-warning" onclick="popupWindow(\'http://supportv2.webso.vn/?op=' . ws_get('op') . '\', \'Trợ giúp\', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
        </a>
    </div>
';
echo'
    <div class="clear"></div>
</div>
<div class="row_content_content">';
$row_total = count($array_data);
if (ws_post('filter_order_Dir') == 'asc') {
    $filter_order_Dir = 'desc';
}
echo '
<form id="frm" name="adminForm" method="post" action="./?op=' . $__getop . '&method=query">
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" name="this_op" value="' . $__getop . '">
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>
<div class="form-group formsearchlist">
      <label for="focusedInput" class="float-left line-height30" >' . $arraybien['tukhoa'] . ': </label>
      <input type="text" title="' . $arraybien['dieukientimkiemdanhmuc'] . '" style="width:140px;" class="form-control col-xs-3" value="' . $__gettukhoa . '" id="focusedInput" name="tukhoa">';
$keyname   = @$__config['keymoudles'];
$idkeytype = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'{$keyname}'");
$danhmuc .= '<input type="hidden" name="hi_parenid" value="' . $compare_id . '" />';
echo $danhmuc;
echo '<select id="anhien" name="anhien" class="form-control trangthai">
      <option value="">' . $arraybien['tatca'] . '</option>';
for ($i = 0; $i < count($_arr_anhien); ++$i) {
    if ($i == $__getanhien && $__getanhien != '') {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    }
}
echo '</select>';
echo ' <div class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '&page=' . $__getpage . '&tukhoa=\'+document.adminForm.tukhoa.value+\'&idtype=\'+document.adminForm.idtype.value+\'&anhien=\'+document.adminForm.anhien.value"><i class="fa fa-search"></i> ' . $arraybien['tim'] . '</div>
   <div style="margin-left:10px;" class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '\'"><i class="fa fa-reply"></i> ' . $arraybien['botimkiem'] . '</div>';
echo '</div>';
if ($total_row > 0) {
    echo '<div id="paged">';
    echo Pages($total_row, $perpage, $path_page);
    echo '</div>';
}
echo '
<div class="table-responsive">
      <table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table -sm css' . $__getop . '">
      <thead>
        <tr class="panel-heading" >
          <th width="30"> # </th>
          <th width="5">';
if (isset($_SESSION['__limit'])) {
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
}
echo '<input type="checkbox" onclick="checkAll(' . $pagetotal . ');" value="" name="toggle">
          </th>';
if ($__config['thutu'] == 1) {
    echo '<th width="90" >
          <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=thutu">' . $arraybien['sapxep'];
    if ($__sortname == 'thutu') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '</a>
          </th>';
} // end sort
if ($__config['ten'] == 1) {
    echo '
          <th>
           <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=ten"> ' . $arraybien['tieude'];
    if ($__sortname == 'ten') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '</a>
          </th>';
}

if ( $__config['khoanggia'] ) {
    echo '<th class="text-center">
        Khoảng giá
    </th>';
}

if ($__config['action'] == 1) {
    echo '
          <th width="130">' . $arraybien['thaotac'] . '</th>';
}
if ($__config['anhien'] == 1) {
    echo '
          <th width="80">
            <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=anhien"> ' . $arraybien['hienthi'];
    if ($__sortname == 'anhien') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '</a></th>';
}
echo '</tr>
        </thead>
        <tbody '.(!empty(ws_get('tukhoa')) || ws_get('anhien')!="" || ws_get('idtype') ? null : 'id="keotha_row"').'>';
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        ++$i;
        if ($i % 2 == 0) {
            $row = 'row0';
        } else {
            $row = 'row1';
        }
        $id = $d['id'];
        echo '
          <tr data-thutu="' . $d['thutu'] . '" data-id="' . $d['id'] . '" data-vitri="' . $key . '" id="sectionid_' . $i . '" class="content_tr ' . $row . '">
            <td title="' . $d['id'] . '"> ' . $i . '</td>
            <td>
              <input id="cb' . ($i - 1).'" type="checkbox" onclick="isChecked(this.checked);" value="' . $d['id'] . '" name="cid[]">
              <input type="hidden" name="img[' . $d['id'] . ']" id="img' . ($i - 1).'" value="' . $d['hinh'] . '" />              <input type="hidden" name="file[' . $d['id'] . ']" id="file' . ($i - 1).'" value="' . $d['file'] . '" />
            </td>';
        if ($__config['thutu'] == 1) {
            echo '<td class="order"><input data-id="' . $d['id'] . '" onchange="ChangeStage(\'ajax.php?op=' . $__getop . '&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">' . '
            </td>';
        } // end thu tu
        if ($__config['ten'] == 1) {
            echo '
            <td class="tieude">
            <a href="./?op=' . ws_get('op') . '&method=frm&id=' . $d['id'] . '&page='.$get_page.'"  title="' . $d['url'] . '" >';
            $s_line = '';
            echo '<span class="textcap' . $sot . '">' . $d['ten'] . '</span>
            </a></td>';
        } // end ten

        /**
         * Khoang Gia
         */
        if ( $__config['khoanggia'] ) {
            echo '<td>
            '.number_format($d['khoanga']).' - '.number_format($d['khoangb']).' đ
            </td>';
        }

        if ($__config['action'] == 1) {
            echo '<td> <ul class="pagination">';
            if ($__sua == 1) {
                echo '<li><a data-toggle="tooltip" style="background: #337ab7; border-color: #337ab7;" title="' . $arraybien['sua'] . '" href="./?op=' . $__getop . '&method=frm&id=' . $d['id'] . '&page='.$get_page.'" >
               <i class="fa fa-pencil color-white"></i>
           </a></li>';
            }
            if ($__xoa == 1) {
                echo '
           <li class="active" ><a data-toggle="tooltip" title="' . $arraybien['xoadongnay'] . '" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del(\'cb'.($i - 1).'\',\'remove\')">
                 <i class="fa fa-trash-o color-white"></i>
            </a></li>';
            }
            echo '</ul></td>';
        } // end action
        // an hien
        if ($__config['anhien'] == 1) {
            echo '
            <td class="align-center"><div id="anhien' . $id . '"> ';
            $itemstage = ' <i title="' . $arraybien['tat'] . '" class="fa fa-eye-slash color-red"></i> ';
            if ($d['anhien'] == 1) {
                $itemstage = ' <i title="' . $arraybien['bat'] . '" class="fa fa-eye color-black">  </i>';
            }
            echo ' <span class="btn btn-default">
            <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $__getop . '&id=' . $id . '&name=anhien&value=' . $d['anhien'] . '\',\'anhien' . $id . '\')" > ' . $itemstage . ' </a>
            </span>
            </div>
            </td>';
        } // end an hien

            echo '</tr>';
    } // end foreach
} // end count
echo '
        </tbody>
    </table>
</div><!-- ./table-responsive -->
<div class="pagechoice"> ' . $arraybien['tongso'] . ': <span style="color:#FF0000; font-weight:bold"> ' . $total_row . '</span> ' . $arraybien['hienthi'] . ' #
<select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage" id="limit" name="limit">';
    for ($i = 1; $i <= count($_arr_listpage); ++$i) {
        if ($i == $_SESSION['__limit']) {
            echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
        } else {
            echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
        }
    }
    echo '</select>
    <div class="float-right">';
    if ($_SESSION['__limit'] != '') {
        //echo $row_total;
        if ($total_row > 0) {
            echo Pages($total_row, $perpage, $path_page);
        }
    }
    echo '
            </div><!-- ./float-right -->
        </div><!-- ./pagechoice -->
    </form>
</div>';
