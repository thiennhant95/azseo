<?php

include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage       = new SimpleImage();
$ResizeImage_thumb = new SimpleImage();
$__config          = array(
    "module_title"   => "Download",
    "table"          => 'tbl_noidung',
    "tablenoidung"   => 'tbl_noidung_lang',
    "loai"           => array_search('download', $_arr_loai_noidung),
    "id"             => 'id',
    "id_name"        => 'idnoidung',
    "keymodules"     => 'download',
    "idtype"         => 1,
    "masp"           => 1,
    "hinh"           => 1,
    "file"           => 1,
    "inlogolenhinh"  => 0,
    "gia"            => 0,
    "giagoc"         => 0,
    "noibat"         => 0,
    "moi"            => 0,
    "banchay"        => 0,
    "home"           => 0,
    "khuyenmai"      => 0,
    "ngay"           => 1,
    "ngaycapnhat"    => 1,
    "solanxem"       => 0,
    "thutu"          => 1,
    "soluong"        => 0,
    "anhien"         => 1,
    "iduser"         => 1,
    "target"         => 1,
    "ten"            => 1,
    "tieude"         => 1,
    "url"            => 1,
    "link"           => 1,
    "mota"           => 0,
    "noidung"        => 0,
    "tukhoa"         => 1,
    "motatukhoa"     => 1,
    "tag"            => 1,
    "action"         => 1,
    "add_item"       => 1,
    "path_img"       => "../uploads/sanpham/",
    "path_file"      => "../uploads/download/",
    "chucnangkhac"   => 0,
    "action"         => 1,
    "imgwidth"       => 800,
    "imgwidth_thumb" => 175,
    "vitrilogo"      => 0, // 0 can giua, 1 can tren trai, 2 can tren phai, 3 can duoi phai, 4 can duoi trai
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
