<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    // ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    // HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    // LƯU THỨ TỰ MỤC TIN
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
    $db->thuTu($__table, $__id, "thutu", "loai", $__config['loai']);
} else if ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            // xoa hinh trong bang tbl_noidung_hinh
            $s_hinh_chitiet = "select hinh from tbl_noidung_hinh where idnoidung = '" . $id_order . "' ";
            $d_hinh_chitiet = $db->rawQuery($s_hinh_chitiet);
            if (count($d_hinh_chitiet) > 0) {
                foreach ($d_hinh_chitiet as $key_hinh_chitiet => $info_hinh_chitiet) {
                    unlink("../uploads/noidung/" . $info_hinh_chitiet['hinh']);
                    unlink("../uploads/noidung/thumb/" . $info_hinh_chitiet['hinh']);
                }
            }
            if ($value_file != '') {
                if (file_exists($__config['path_file'] . $value_file)) {
                    unlink($__config['path_file'] . $value_file);
                }
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idnoidung  = '" . $id_order . "' ");
            // xoa bang tbl_noidung_hinh
            $db->sqlDelete("tbl_noidung_hinh", " idnoidung  = '" . $id_order . "' ");
        }
    }
}
if ($__getaction == 'save') {
    // LƯU TRỮ DỮ LIỆU
    function restructure_array(array $images)
    {
        $result = array();
        foreach ($images as $key => $value) {
            foreach ($value as $k => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $result[$i][$k] = $val[$i];
                }
            }
        }
        return $result;
    }
    $images      = restructure_array($_FILES);
    $idtype      = replace_html(ws_post('idtype'));
    $masp        = replace_html(ws_post('masp'));
    $hinh        = replace_html(ws_post('filefile'));
    $file        = replace_html($_FILES['file']["name"]);
    $gia         = replace_html(ws_post('gia'));
    $giagoc      = replace_html(ws_post('giagoc'));
    $noibat      = replace_html(ws_post('noibat'));
    $moi         = replace_html(ws_post('moi'));
    $banchay     = replace_html(ws_post('banchay'));
    $home        = replace_html(ws_post('home'));
    $khuyenmai   = replace_html(ws_post('khuyenmai'));
    $ngay        = $db->getDateTimes();
    $ngaycapnhat = $db->getDateTimes();
    $solanxem    = replace_html(ws_post('solanxem'));
    $thutu       = 1;
    $loai        = $__config['loai'];
    $soluong     = replace_html(ws_post('soluong'));
    $anhien      = replace_html(ws_post('anhien'));
    $xoaicon     = replace_html(ws_post('xoaicon'));
    $xoaimg      = replace_html(ws_post('xoaimg'));
    $xoafile     = replace_html(ws_post('xoafile'));
    $iconname    = replace_html(ws_post('iconname'));
    $imgname     = replace_html(ws_post('imgname'));
    $filename    = replace_html(ws_post('filename'));
    $_getidSP    = "";
    // Create url lien ket
    $urllink = '';
    $_cid    = ws_get('id');
    if (ws_post('action') == 'add') {
        $thutu = (int) substr($subid, -3);
    } else {
        $thutu = (int) substr($id, -3);
    }
    $images_name = ws_post('url'.$__defaultlang);
    // lay id danh muc
    $_postselector = ws_post('selector');
    $str_selector  = '';
    if (count($_postselector) > 0) {
        for ($i = 0; $i < count($_postselector); $i++) {
            if ($i > 0) {
                $str_selector .= ',' . $_postselector[$i];
            } else {
                $str_selector .= $_postselector[$i];
            }
        }
    }
    // neu them moi danh mục
    if ($__getid == '') {
        // update thutu tang len 1 cho tat ca
        $sql_update = "update $__table set thutu = thutu+1 where 1 = 1 ";
        $db->rawQuery($sql_update);
        // up load icon
        if ($icon != '') {
            $extfile  = pathinfo($icon, PATHINFO_EXTENSION);
            $iconfile = $images_name . '-icon.' . $extfile;
            if (file_exists("../uploads/noidung/" . $iconfile)) {
                $iconfile = rand(0, 100) . $iconfile;
            }
            move_uploaded_file($_FILES["icon"]["tmp_name"], "../uploads/noidung/$iconfile");
        }
        // upload hinh
        if ($img != '') {
            $extfile = pathinfo($img, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/noidung/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/noidung/$imgfile");
            $ResizeImage->load("../uploads/noidung/" . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save("../uploads/noidung/thumb/" . $imgfile);
        }
        // upload File
        if ($file != '') {
            $extfile  = pathinfo($file, PATHINFO_EXTENSION);
            $filefile = $images_name . '-file.' . $extfile;
            if (file_exists($__config['path_file'] . $filefile)) {
                $filefile = rand(100) . $filefile;
            }
            // Tao folder
            if (!file_exists($__config['path_file'])) {
                mkdir($__config['path_file'], 0777, true);
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], $__config['path_file'] . $filefile);
        }
        $aray_insert = array(
            "idtype"      => $str_selector,
            "hinh"        => $imgfile,
            "file"        => $filefile,
            "ngay"        => $ngay,
            "ngaycapnhat" => $ngaycapnhat,
            "solanxem"    => $solanxem,
            "thutu"       => $thutu,
            "loai"        => $loai,
            "soluong"     => $soluong,
            "anhien"      => $anhien,
            "iduser"      => $_SESSION['user_id'],
        );
        $id_insert = $db->insert($__table, $aray_insert);
        if ($id_insert > 0) {
            // kiem tra xem co hinh chi tiet hay khong
            $s_hinh = "select hinh,id from tbl_noidung_hinh where idnoidung = '" . $_SESSION['tmpimg'] . "' order by id asc";
            $d_hinh = $db->rawQuery($s_hinh);
            if (count($d_hinh) > 0) {
                foreach ($d_hinh as $key_hinh => $info_hinh) {
                    $id_hinh   = $info_hinh['id'];
                    $hinh_hinh = $info_hinh['hinh'];
                    // truy van lay ngon ngu dau tien
                    $s_lang_first = "select id from tbl_lang where anhien = 1 order by thutu Asc limit 0,1";
                    $d_lang_first = $db->rawQuery($s_lang_first);
                    if ($d_lang_first > 0) {
                        $id_first_lang = $d_lang_first[0]['id'];
                        // kiem tra neu tieu de khac rong
                        $tieude_first = ws_post('url'.$id_first_lang);
                    }
                    $hinhsave = $hinh_hinh;
                    // kiem tra va doi lại tên hình
                    if ($tieude_first != '') {
                        $arr_ext_img = explode('.', $hinh_hinh);
                        $ext_img     = $arr_ext_img[(count($arr_ext_img) - 1)];
                        $hinhsave    = $tieude_first . '-' . $key_hinh . '.' . $ext_img;
                    }
                    // đưa hình vào thư mục nội dung
                    $img_path = "plugin/dropzon/upload/";
                    // kiem tra kich thuoc hinh co lon hon cho phep hay khong
                    // lay kich thuoc hinh
                    //$handle = new Upload($img_path.$hinh_hinh);
                    //if ($handle->uploaded)
                    //{
                    //$handle->file_new_name_body = 'foo';
                    //$handle->Process('../uploads/noidung/');
                    //$handle->Process("../uploads/noidung/".$hinhsave);
                    //}
                    // gan logo len hinh
                    if (ws_post('inlogolenhinh') != '') {
                        $hinh_logo = '../uploads/logo/' . ws_post('inlogolenhinh');
                        $hinh_goc  = $img_path . $hinh_hinh;
                        in_logo_len_hinh($hinh_goc, $hinh_logo, "../uploads/noidung/" . $hinhsave, $__config['vitrilogo']);
                    } else {
                        $ResizeImage->load($img_path . $hinh_hinh);
                        $ResizeImage->save("../uploads/noidung/" . $hinhsave);
                    }
                    $ResizeImage_thumb->load("../uploads/noidung/" . $hinhsave);
                    $getsize = getimagesize($img_path . $hinh_hinh);
                    // lay do rong cua hinh
                    $getwidth = $getsize[1];
                    if ($getwidth > $__config['imgwidth']) {
                        //neu hinh lon hon thi se cat hinh
                        //$ResizeImage->resizeToWidth($__config['imgwidth']);
                    }
                    if ($getwidth > $__config['imgwidth_thumb']) {
                        //neu hinh lon hon thi se cat hinh
                        $ResizeImage_thumb->resizeToWidth($__config['imgwidth_thumb']);
                    }
                    // $ResizeImage->load($img_path . $hinh_hinh);
                    // $ResizeImage->save("../uploads/noidung/" . $hinhsave);
                    $ResizeImage_thumb->save("../uploads/noidung/thumb/" . $hinhsave);
                    // xoa hinh hien tai
                    unlink($img_path . $hinh_hinh);
                    // cap nhat lại hình đại diện
                    if ($key_hinh == 0) {
                        // set hình đàu tiên là default
                        $aray_img = array("hinhdaidien" => 1);
                        $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                        // up hinh dai dien cua bang noi dung
                        $aray_img = array("hinh" => $hinhsave);
                        $db->sqlUpdate("tbl_noidung", $aray_img, "id = '" . $id_insert . "' ");
                    }
                    //upate lại tên hình cho bảng tbl_noidung_hinh
                    if ($tieude_first != '') {
                        $aray_img = array("hinh" => $hinhsave);
                        $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                    }
                    // cap nhat lại id trong bảng tbl_noidung_hinh => idnoidung theo id mới thêm vào
                    $aray_img = array("idnoidung" => $id_insert,
                        "tmp"                         => 0);
                    $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                } // end for
            } //end coutn
        } // end insert thanh cong
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select a.id,a.idkey,b.ten
                 from tbl_lang AS a
                 inner join tbl_lang_lang AS b On  a.id = b.iddanhmuc
                 where b.idlang = '{$__defaultlang}'
                 and a.anhien = 1
                 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = replace_html(ws_post('ten'.$idlang));
                $tieude     = replace_html(ws_post('tieude'.$idlang));
                $mota       = replace_html(ws_post('mota'.$idlang));
                $noidung    = replace_html(ws_post('noidung'.$idlang));
                $url        = replace_html(ws_post('url'.$idlang));
                $link       = replace_html(ws_post('link'.$idlang));
                $target     = replace_html(ws_post('target'.$idlang));
                $tukhoa     = replace_html(ws_post('tukhoa'.$idlang));
                $motatukhoa = replace_html(ws_post('motatukhoa'.$idlang));
                $tag        = replace_html(ws_post('tag'.$idlang));
                // kiem tra url neu da co roi thì them ki tu cuoi url
                $s_checkurl = "select url from $__tablenoidung where url = '$url'";
                $d_checkurl = $db->rawQuery($s_checkurl);
                if (count($d_checkurl) > 0) {
                    $url = $url . '-' . rand(0, 100);
                }
                // luu du lieu vao bang danh muc lang
                $aray_insert_lang = array(
                    "idnoidung"  => $id_insert,
                    "idlang"     => $idlang,
                    "ten"        => $ten,
                    "tieude"     => $tieude,
                    "url"        => $url,
                    "link"       => $link,
                    "target"     => $target,
                    "mota"       => $mota,
                    "noidung"    => $noidung,
                    "tukhoa"     => $tukhoa,
                    "motatukhoa" => $motatukhoa,
                    "tag"        => $tag,
                );
                $db->insert($__tablenoidung, $aray_insert_lang);
            }
        }
    } else {
        /** KHI NGƯỜI DÙNG BẤM SỬA BÀI VIẾT */
        $aray_insert = array(
            "idtype"      => $str_selector,
            "masp"        => $masp,
            "ngaycapnhat" => $ngaycapnhat,
            "solanxem"    => $solanxem,
            "loai"        => $loai,
            "anhien"      => $anhien,
            "iduser"      => $_SESSION['user_id'],
        );
        $id_update = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
        if ($id_update > 0) {
            // kiem tra xem co hinh chi tiet hay khong
            $s_hinh = "select hinh,id from tbl_noidung_hinh where idnoidung = '" . $_SESSION['tmpimg'] . "' order by id asc";
            $d_hinh = $db->rawQuery($s_hinh);
            if (count($d_hinh) > 0) {
                foreach ($d_hinh as $key_hinh => $info_hinh) {
                    $id_hinh   = $info_hinh['id'];
                    $hinh_hinh = $info_hinh['hinh'];
                    // truy van lay ngon ngu dau tien
                    $s_lang_first = "select id from tbl_lang where anhien = 1 order by thutu Asc limit 0,1";
                    $d_lang_first = $db->rawQuery($s_lang_first);
                    if ($d_lang_first > 0) {
                        $id_first_lang = $d_lang_first[0]['id'];
                        // kiem tra neu tieu de khac rong
                        $tieude_first = ws_post('url'.$id_first_lang);
                    }
                    $hinhsave = $hinh_hinh;
                    // kiem tra va doi lại tên hình
                    if ($tieude_first != '') {
                        $arr_ext_img = explode('.', $hinh_hinh);
                        $ext_img     = $arr_ext_img[(count($arr_ext_img) - 1)];
                        $hinhsave    = $tieude_first . '-' . rand(100, 1000) . '.' . $ext_img;
                    }
                    // đưa hình vào thư mục nội dung
                    $img_path = "plugin/dropzon/upload/";
                    // kiem tra kich thuoc hinh co lon hon cho phep hay khong
                    // lay kich thuoc hinh
                    //$handle = new Upload($img_path.$hinh_hinh);
                    //if ($handle->uploaded)
                    //{
                    //$handle->file_new_name_body = 'foo';
                    //$handle->Process('../uploads/noidung/');
                    //$handle->Process("../uploads/noidung/".$hinhsave);
                    //}
                    // gan logo len hinh
                    if (ws_post('inlogolenhinh') != '') {
                        $hinh_logo = '../uploads/logo/' . ws_post('inlogolenhinh');
                        $hinh_goc  = $img_path . $hinh_hinh;
                        in_logo_len_hinh($hinh_goc, $hinh_logo, "../uploads/noidung/" . $hinhsave, $__config['vitrilogo']);
                    } else {
                        $ResizeImage->load($img_path . $hinh_hinh);
                        $ResizeImage->save("../uploads/noidung/" . $hinhsave);
                    }
                    $ResizeImage_thumb->load("../uploads/noidung/" . $hinhsave);
                    $getsize = getimagesize($img_path . $hinh_hinh);
                    // lay do rong cua hinh
                    $getwidth = $getsize[1];
                    if ($getwidth > $__config['imgwidth']) {
                        //neu hinh lon hon thi se cat hinh
                        //$ResizeImage->resizeToWidth($__config['imgwidth']);
                    }
                    if ($getwidth > $__config['imgwidth_thumb']) {
                        //neu hinh lon hon thi se cat hinh
                        $ResizeImage_thumb->resizeToWidth($__config['imgwidth_thumb']);
                    }
                    // $ResizeImage->load($img_path . $hinh_hinh);
                    // $ResizeImage->save("../uploads/noidung/" . $hinhsave);
                    $ResizeImage_thumb->save("../uploads/noidung/thumb/" . $hinhsave);
                    // xoa hinh hien tai
                    unlink($img_path . $hinh_hinh);
                    // cap nhat lại hình đại diện
                    if ($key_hinh == 0) {
                        //kiem tra xem hinh dai dien da duoc set chua hay da xoa mat hinh dai dien roi
                        $get_hinhdaidien = $db->getNameFromID("tbl_noidung", "hinh", "id", $__postid);
                        $flag            = 0;
                        if ($get_hinhdaidien != '') {
                            $check_hinhdaidien = $db->getNameFromID("tbl_noidung_hinh", "hinh", "hinh", "'" . $get_hinhdaidien . "' and idnoidung = '" . $__postid . "' ");
                            if ($check_hinhdaidien != '') {
                                $flag = 1;
                            }
                        }
                        if ($flag == 0) {
                            // set hình đàu tiên là default
                            $aray_img = array("hinhdaidien" => 1);
                            $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                            // up hinh dai dien cua bang noi dung
                            $aray_img = array("hinh" => $hinhsave);
                            $db->sqlUpdate("tbl_noidung", $aray_img, "id = '{$__postid}' ");
                        }
                    }
                    //upate lại tên hình cho bảng tbl_noidung_hinh
                    if ($tieude_first != '') {
                        $aray_img = array("hinh" => $hinhsave);
                        $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                    }
                    // cap nhat lại id trong bảng tbl_noidung_hinh => idnoidung theo id mới thêm vào
                    $aray_img = array("idnoidung" => $__postid,
                        "tmp"                         => 0);
                    $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                } // end for
            } //end coutn
        }
        // them du lieu vao bang noi dung danh muc
        $s_lang = "SELECT a.id,a.idkey,b.ten
                 from tbl_lang AS a
                 inner join tbl_lang_lang AS b On  a.id = b.iddanhmuc
                 where b.idlang = '{$__defaultlang}'
                 and a.anhien = 1
                 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
// lap theo so luong ngon ngu
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = replace_html(ws_post('ten'.$idlang));
                $tieude     = replace_html(ws_post('tieude'.$idlang));
                $mota       = replace_html(ws_post('mota'.$idlang));
                $noidung    = replace_html(ws_post('noidung'.$idlang));
                $url        = replace_html(ws_post('url'.$idlang));
                $link       = replace_html(ws_post('link'.$idlang));
                $target     = replace_html(ws_post('target'.$idlang));
                $tukhoa     = replace_html(ws_post('tukhoa'.$idlang));
                $motatukhoa = replace_html(ws_post('motatukhoa'.$idlang));
                $tag        = replace_html(ws_post('tag'.$idlang));
                // kiem tra url neu da co roi thì them ki tu cuoi url
                if (ws_post('hi_url'.$idlang) != $url) {
                    $s_checkurl = "select url from $__tablenoidung where url = '$url'";
                    $d_checkurl = $db->rawQuery($s_checkurl);
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                }
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "select id from $__tablenoidung where idnoidung = '" . $__postid . "' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) {
                    // da tồn tại nội dung rồi thì update lại nội dung
                    // cap nhat lai du lieu
                    $aray_insert_lang = array(
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "mota"       => $mota,
                        "noidung"    => $noidung,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                        "tag"        => $tag,
                    );
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "idnoidung = '" . $__postid . "' and idlang = '{$idlang}'");
                } else {
                    $aray_insert_lang = array(
                        "idnoidung"  => $__postid,
                        "idlang"     => $idlang,
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "mota"       => $mota,
                        "noidung"    => $noidung,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                        "tag"        => $tag,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang
        // kiem tra neu thay hinh thi sẽ xóa hình cũ đi
        // xoa icon
        if ($xoaicon == 1) {
            unlink("../uploads/noidung/" . $iconname);
            $aray_insert = array("icon" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // xoa img
        if ($xoaimg == 1) {
            unlink("../uploads/noidung/" . $imgname);
            $aray_insert = array("img" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }

        // neu cap nhat icon
        if ($icon != '') {
            if ($iconname != '') {
                unlink("../uploads/noidung/" . $iconname);
            }
            //up file moi len
            $extfile  = pathinfo($icon, PATHINFO_EXTENSION);
            $iconfile = $images_name . '-icon.' . $extfile;
            if (file_exists("../uploads/noidung/" . $iconfile)) {
                $iconfile = rand(0, 100) . $iconfile;
            }
            move_uploaded_file($_FILES["icon"]["tmp_name"], "../uploads/noidung/$iconfile");
            $aray_insert = array("icon" => $iconfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat img
        if ($img != '') {
            if ($imgname != '') {
                unlink("../uploads/noidung/" . $imgname);
                unlink("../uploads/noidung/thumb/" . $imgname);
            }
            //up file moi len
            $extfile = pathinfo($img, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/noidung/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/noidung/$imgfile");
            $ResizeImage->load("../uploads/noidung/" . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save("../uploads/noidung/thumb/" . $imgfile);
            $aray_insert = array("img" => $imgfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // xoa file
        if ($xoafile == 1) {
            if (file_exists($__config['path_file'] . $filename)) {
                unlink($__config['path_file'] . $filename);
            }
            $aray_insert = array("file" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat file
        if ($file != '') {
            if ($filename != '') {
                if (file_exists($__config['path_file'] . $filefile)) {
                    unlink($__config['path_file'] . $filefile);
                }
            }
            //up file moi len
            $extfile  = pathinfo($file, PATHINFO_EXTENSION);
            $filefile = $images_name . '-file.' . $extfile;
            if (file_exists($__config['path_file'] . $filefile)) {
                $filefile = rand(0, 100) . $filefile;
            }
            // Tao folder
            if (!file_exists($__config['path_file'])) {
                mkdir($__config['path_file'], 0777, true);
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], $__config['path_file'] . $filefile);
            $aray_insert = array("file" => $filefile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // update idtype neu chuyen danh muc di vi tri khac
        if (ws_post('hi_parenid') != ws_post('parenid')) {
            $subid = $db->createSubID($__table, $__id, ws_post('parenid'));
            // update bang tbl_danhmuc
            $aray_insert = array("id" => $subid);
            $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
            // update bang tbl_danhmuc_lang
            $aray_insert_lang = array("iddanhmuc" => $subid);
            $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = $__postid");
        }
    } // end if cap nhat
    // xoa hinh neu như đã up lên nhưng hủy upload hoặc tắt ngang trình duyệt
    $ngayhientai       = $db->getDate();
    $s_timhinh_quadate = "select hinh,id from tbl_noidung_hinh
                    where ngaytao < '" . $ngayhientai . "'
                    and tmp = 1 ";
    $d_timhinh_quadate = $db->rawQuery($s_timhinh_quadate);
    print_r($d_timhinh_quadate);
    if (count($d_timhinh_quadate) > 0) {
        foreach ($d_timhinh_quadate as $key_timhinh => $info_timhinh) {
            $timhinh_name = $info_timhinh['hinh'];
            $timhinh_id   = $info_timhinh['id'];
            $linkxoa      = "plugin/dropzon/upload/" . $timhinh_name;
            unlink($linkxoa);
            // xoa trong csdl
            $db->sqlDelete("tbl_noidung_hinh", " id   = '" . $timhinh_id . "' ");
        }
    }
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $id;
}
if (ws_post('saveandnew') == 'saveandnew') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm';
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
