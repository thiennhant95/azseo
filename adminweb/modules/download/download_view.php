<?php
$_id_name = $__config['id_name'];
$s        = "SELECT a.*, b.ten as ten, b.url, b.link
          FROM {$__table} AS a
          INNER JOIN {$__tablenoidung} AS b ON a.id = b.$_id_name
          where b.idlang = '{$__defaultlang}'
          and a.loai = {$__config['loai']} ";
$id       = ws_get('key');
$ten      = ws_get('ten');
$loai     = ws_get('loai');
$ngaydang = ws_get('ngaydang');
$anhien   = ws_get('anhien');
$get_page = ws_get('page') ? ws_get('page') : 1;
if ($__gettukhoa != '') {
    $s = $s . " and (ten LIKE '%{$__gettukhoa}%' OR a.id LIKE '%{$__gettukhoa}%' ) ";
}
if ($__getidtype != '') {
    $s = $s . " AND a.idtype LIKE '{$__getidtype}%' ";
}
if ($__getanhien != '') {
    $s = $s . " AND anhien = '{$__getanhien}' ";
}
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . " order by  " . $__sortname . "  " . $__sortcurent;
} else {
    $s = $s . " order by  thutu  asc ";
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = $db->rawQuery($s_counttotal);
$total_row    = count($d_counttotal);
$page         = (int) (!ws_get("page") ? 1 : ws_get("page"));
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . " ";
}
$data      = $db->rawQuery($s);
$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $arraybien['capnhathinhbaiviet']; ?></h4>
      </div>
      <div class="modal-body" id="loadhinhchitiet">
      </div><div class="clear"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $arraybien['dong']; ?></button>
      </div>
    </div>
  </div>
</div>
<div class="row_content_top">
   <div class="row_content_top_title"><?php echo $__config['module_title']; ?></div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder('<?php echo $saveorder - 1; ?>', 'saveorder')">
               <i class="fa fa-sort-amount-desc"></i> <?php echo $arraybien['sapxep']; ?>
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('publish')}" href="#">
            <i class="fa fa-check-circle"></i> <?php echo $arraybien['bat']; ?>
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('unpublish')}" href="#">
          <i class="fa fa-check-circle color-black"></i> <?php echo $arraybien['tat']; ?>
        </a>
      <?php
if ($__xoa == 1) {
    ?>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton_del('remove')}" href="#">
        <i class="fa fa-times-circle color-black"></i> <?php echo $arraybien['xoa']; ?>
        </a>
        <?php
}
?>
      <?php
if ($__them == 1) {
    ?>
        <a class="btn btn-primary" href="./?op=<?php echo $__getop; ?>&method=frm">
         <i class="fa fa-plus-circle"></i> <?php echo $arraybien['themmoi']; ?>
        </a>
      <?php
}
?>
        <a class="btn btn-warning" onclick="popupWindow('http://supportv2.webso.vn/?op=<?php echo ws_get('op'); ?>', 'Trợ giúp', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> <?php echo $arraybien['trogiup']; ?>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="row_content_content">
<?php
$row_total = $db->getValue($__table, "count(*)");
if (ws_post('filter_order_Dir') == 'asc') {
    $filter_order_Dir = 'desc';
}
?>
<form  id="frm" name="adminForm" method="post" action="./?op=<?php echo $__getop; ?>&method=query">
<?php
echo '
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>';
?>
<div class="form-group formsearchlist">
      <label for="focusedInput" class="float-left line-height30" ><?php echo $arraybien['tukhoa']; ?>: </label>
      <input type="text" title="<?php echo $arraybien['dieukientimkiemdanhmuc']; ?>" style="width:140px;" class="form-control col-xs-3" value="<?php echo $__gettukhoa; ?>" id="focusedInput" name="tukhoa">
    <?php
$keyname   = $__config['keymodules'];
$idkeytype = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'{$keyname}'");
$sql2      = "select a.id, b.ten
                from tbl_danhmuc As a
                inner join tbl_danhmuc_lang As b On a.id =  b.iddanhmuc
                where b.idlang = '{$__defaultlang}'
            and a.idtype = '{$idkeytype}'
                order by a.id";
$compare_id = substr($id, 0, strlen($id) - 4);
$option1    = '<option value="">' . $arraybien['danhmuc'] . '</option>';
$danhmuc .= $db->createComboboxDequySql("idtype", $option1, "form-control danhmuc", 4, $sql2, "ten", "id", $__getidtype);
$danhmuc .= '<input type="hidden" name="hi_parenid" value="' . $compare_id . '" />';
echo $danhmuc;
echo '
     <select id="anhien" name="anhien" class="form-control trangthai">
      <option value="">' . $arraybien['tatca'] . '</option>';
for ($i = 0; $i < count($_arr_anhien); $i++) {
    if ($i == $__getanhien && $__getanhien != '') {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    }
}
echo '</select>';
echo ' <div class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '&page=' . $__getpage . '&tukhoa=\'+document.adminForm.tukhoa.value+\'&idtype=\'+document.adminForm.idtype.value+\'&anhien=\'+document.adminForm.anhien.value"><i class="fa fa-search"></i> ' . $arraybien['tim'] . '</div>
   <div style="margin-left:10px;" class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '\'"><i class="fa fa-reply"></i> ' . $arraybien['botimkiem'] . '</div>';
?>
</div>
         <?php if ($total_row > 0) {echo Pages($total_row, $perpage, $path_page);}
?>
<div class="table-responsive">
      <table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table css<?php echo $__getop; ?>">
      <thead>
        <tr class="panel-heading" >
          <th width="30"> # </th>
          <th width="5">
          <?php
if (isset($_SESSION['__limit'])) {
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
}
?>
             <input type="checkbox" onclick="checkAll(<?php echo $pagetotal; ?>);" value="" name="toggle">
          </th>
          <?php
if ($__config['thutu'] == 1) {
    ?>
          <th width="90" >
          <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=thutu">
          <?php echo $arraybien['sapxep']; ?>
          <?php
if ($__sortname == 'thutu') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    ?>
          </a>
          </th>
          <?php
} // end sort
?>
          <?php
if ($__config['ten'] == 1) {
    ?>
          <th>
           <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=ten">
          <?php echo $arraybien['tieude']; ?>
          <?php
if ($__sortname == 'ten') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    ?>
          </a>
          </th>
          <?php
}
?>
          <?php
if ($__config['idtype'] == 1) {
    ?>
          <th width="160">
          <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=idtype">
          <?php echo $arraybien['loaitin']; ?>
          <?php
if ($__sortname == 'idtype') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    ?>
          </a>
          </th>
          <?php
}
?>
          <?php
if ($__config['hinh'] == 1) {
    ?>
          <th width="100"><?php echo $arraybien['hinh']; ?></th>
          <?php
}
?>
          <?php
if ($__config['action'] == 1) // action
{
    ?>
          <th width="130"><?php echo $arraybien['thaotac']; ?></th>
        <?php
}
?>
          <?php
if ($__config['anhien'] == 1) {
    ?>
          <th width="80">
            <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=anhien">
          <?php echo $arraybien['hienthi']; ?>
          <?php
if ($__sortname == 'anhien') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    ?>
          </a>
          </th>
          <?php
}
?>
           <?php
if ($__config['chucnangkhac'] == 1) {
    ?>
         <th style=" width:168px !important;"><?php echo $arraybien['chucnangkhac']; ?></th>
        <?php
}
?>
        </tr>
        </thead>
        <tbody>
<?php
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        $i++;
        if ($i % 2 == 0) {
            $row = "row0";
        } else {
            $row = "row1";
        }
        $id = $d['id'];
        ?>
          <tr id="sectionid_<?php echo $i; ?>" class="<?php echo $row; ?>">
            <td title="<?php echo $d['id']; ?>">
               <?php echo $i; ?>
            </td>
            <td>
              <input id="cb<?php echo $i - 1; ?>" type="checkbox" onclick="isChecked(this.checked);" value="<?php echo $d['id']; ?>" name="cid[]">
              <input type="hidden" name="img[<?php echo $d['id']; ?>]" id="img<?php echo $i - 1; ?>" value="<?php echo $d['hinh']; ?>" />              <input type="hidden" name="file[<?php echo $d['id']; ?>]" id="file<?php echo $i - 1; ?>" value="<?php echo $d['file']; ?>" />
            </td>
          <?php
if ($__config['thutu'] == 1) {
            ?>
               <td class="order">
              <?php
echo '
                <input onchange="ChangeStage(\'ajax.php?op='.$__getop.'&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
            ?>
            </td>
           <?php
} // end thu tu
        ?>
           <?php
if ($__config['ten'] == 1) // ten
        {
            ?>
            <td class="tieude">
            <a href="<?php echo './?op=' . ws_get('op') . '&method=frm&id=' . $d['id']; ?>"  title="<?php echo $d['ten']; ?>" >
            <?php
$s_line = '';
            echo '<span class="textcap' . $sot . '">' . $d['ten'] . '</span>';
            ?>
            </a></td>
           <?php
} // end ten
        ?>
            <?php
if ($__config['idtype'] == 1) // thuoc modules
        {
            ?>
            <td class="align-center font-bold">
            <?php
			$keyname = $__config['keymodules'];
            $idkeytype         = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'{$keyname}'");
            $strdanhmucthemmoi = '';
            $idtype            = $d['idtype'];
            $strdanhmuc        = '<div class="danhmuc_dachon" id="danhmucload' . $id . '">';
            $s_loaitru         = "SELECT a.id, b.ten
                    from tbl_danhmuc AS a
                    inner join tbl_danhmuc_lang AS b On a.id =  b.iddanhmuc
                    where b.idlang = '{$__defaultlang}'
               and a.idtype = '{$idkeytype}' ";
            if ($idtype != '') {
                $strdanhmuc .= '<ul class="ul_danhmucdachon">';
                $arr_idtype   = explode(",", $idtype);
                $count_idtype = count($arr_idtype);
                if ($count_idtype > 0) {
                    for ($u = 0; $u < $count_idtype; $u++) {
                        $iddanhmuc = $arr_idtype[$u];
                        $s_loaitru .= " and a.id != '" . $iddanhmuc . "' ";
                        $lenstr     = '';
                        $selectedid = '';
                        for ($kk = 4; $kk < strlen($iddanhmuc); $kk += 4) {
                            //$lenstr .= '&nbsp;&nbsp;&nbsp;';
                        }
                        $tendanhmuc = $db->getNameFromID("tbl_danhmuc_lang", "ten", "iddanhmuc", "'" . $iddanhmuc . "' and idlang = '{$__defaultlang}'");
                        $strdanhmuc .= '<li id="itemdmdachon' . $u . '">' . $lenstr . '<a title="' . $arraybien['xoa'] . '" href="#" onclick="Get_Data(\'ajax.php?op='.$__getop.'&name=updatedanhmuc&act=xoa&id=' . $d['id'] . '&idxoa=' . $iddanhmuc . '&idtype=' . $idtype . '\',\'danhmucload' . $id . '\'); return false;" ><i class="fa  fa-times-circle fa-lg"></i></a> ' . $tendanhmuc . '</li>';
                    }
                }
                $strdanhmuc .= '</ul>';
            }
            $s_loaitru .= ' order by a.id ';
            $d_loaitru = $db->rawQuery($s_loaitru);
            if (count($d_loaitru) > 0) {
                $strdanhmucthemmoi .= '<select onchange="Get_Data(\'ajax.php?op='.$__getop.'&name=updatedanhmuc&act=them&id=' . $d['id'] . '&idthem=\'+this.value+\'&idtype=' . $idtype . '\',\'danhmucload' . $id . '\')" name="iddanhmuc_themmoi" class="form-control danhmuc form-control2">
                     <option value="">' . $arraybien['themloai'] . '</option>';
                foreach ($d_loaitru as $key_loaitru => $info_loaitru) {
                    $lenstr            = '';
                    $danhmucmoi_idtype = $info_loaitru['id'];
                    $danhmucmoi_ten    = $info_loaitru['ten'];
                    for ($kk = 4; $kk < strlen($danhmucmoi_idtype); $kk += 4) {
                        $lenstr .= '===';
                    }
                    $strdanhmucthemmoi .= '<option value="' . $danhmucmoi_idtype . '">' . $lenstr . $danhmucmoi_ten . '</option>';
                }
                $strdanhmucthemmoi .= '</select>';
            }
            $strdanhmuc .= $strdanhmucthemmoi;
            $strdanhmuc .= '</div>';
            echo $strdanhmuc;
            ?>
            </td>
             <?php
} // end idtype
        ?>
           <?php
if ($__config['hinh'] == 1) // Img
        {
            ?>
            <td>
               <?php if ($d['hinh'] != '') {?>
                  <a  onclick="window.open(this.href, '', 'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50'); return false;" href="../uploads/noidung/<?php echo $d['hinh']; ?>"><img src="../uploads/noidung/thumb/<?php echo $d['hinh']; ?>" width="90" /></a>
                 <a href="#" onclick="Get_Data('ajax.php?op=<?=$__getop?>&name=loadhinh&idnoidung=<?php echo $d['id']; ?>&hinh=<?php echo $d['hinh']; ?>','loadhinhchitiet');" data-toggle="modal" data-target="#myModal"><?php echo $arraybien['xemthemhinh']; ?></a>
                <?php } else {?>
                  <img src="application/templates/images/noimage.png" alt="" width="60" />
                <?php }
            ?>
            </td>
           <?php
} // end img
        ?>
            <?php
if ($__config['action'] == 1) // action
        {
            ?>
            <td>
            <ul class="pagination">
            <?php
if ($__sua == 1) {
                ?>
               <li><a data-toggle="tooltip" style="background: #337ab7; border-color: #337ab7;" title="<?php echo $arraybien['sua']; ?>" href="<?php echo './?op=' . $__getop . '&method=frm&id=' . $d['id']; ?>" >
                   <i class="fa fa-pencil color-white"></i>
               </a></li>
                <?php
}
            ?>
               <li><a data-toggle="tooltip" title="<?php echo $arraybien['xemchuyenmuctrenwebsite']; ?>" href="../<?php echo $d['url']; ?>" target="_blank" >
                     <i class="fa fa-external-link"></i>
                </a></li>
            <?php
if ($__xoa == 1) {
                ?>
               <li class="active" ><a data-toggle="tooltip" title="<?php echo $arraybien['xoadongnay']; ?>" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del('cb<?php echo $i - 1; ?>','remove')" >
                     <i class="fa fa-trash-o color-white"></i>
                </a></li>
                <?php
}
            ?>
            </ul>
            </td>
            <?php
} // end action
        ?>
            <?php
// an hien
        if ($__config['anhien'] == 1) {
            ?>
            <td class="align-center">
              <?php
echo '<div id="anhien' . $id . '">';
            $itemstage = '<i title="' . $arraybien['tat'] . '" class="fa fa-eye-slash color-red"></i>';
            if ($d['anhien'] == 1) {
                $itemstage = '<i title="' . $arraybien['bat'] . '" class="fa fa-eye color-black"></i>';
            }
            echo '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=' . $d['anhien'] . '\',\'anhien' . $id . '\')">' . $itemstage . '</a> </span>';
            echo '</div>';
            ?>
            </td>
            <?php
} // end an hien
        ?>
      <?php
if ($__config['chucnangkhac'] == 1) // chuc nang khac
        {
            ?>
            <td>
          <?php
if ($__config['home'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=home">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="home' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['home'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=home&value=' . $d['home'] . '\',\'home' . $id . '\')">' . $arraybien['trangchu'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } // config homepage
            // col top
            if ($__config['noibat'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=noibat">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="noibat' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['noibat'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=noibat&value=' . $d['noibat'] . '\',\'noibat' . $id . '\')">' . $arraybien['noibat'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config top
            // col bottom
            if ($__config['moi'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=moi">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="moi' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['moi'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=moi&value=' . $d['moi'] . '\',\'moi' . $id . '\')">' . $arraybien['moi'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config bottom
            // col main
            if ($__config['banchay'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=banchay">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="banchay' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['banchay'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=banchay&value=' . $d['banchay'] . '\',\'banchay' . $id . '\')">' . $arraybien['banchay'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config main
            // col colleft
            if ($__config['khuyenmai'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li>
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=khuyenmai">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="khuyenmai' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['khuyenmai'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=khuyenmai&value=' . $d['khuyenmai'] . '\',\'khuyenmai' . $id . '\')">' . $arraybien['khuyenmai'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config colleft
            ?>
      </td>
      <?php
} // end chuc nang khac
        ?>
          </tr>
          <?php
} // end foreach
} // end count
?>
        </tbody>
      </table></div>
            <div class="pagechoice">
             <?php echo $arraybien['tongso']; ?>: <span style="color:#FF0000; font-weight:bold">
                  <?php echo $total_row; ?>
                  </span>
             <?php echo $arraybien['hienthi']; ?> #
             <?php
echo '
              <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage"  id="limit" name="limit">';
for ($i = 1; $i <= count($_arr_listpage); $i++) {
    if ($i == $_SESSION['__limit']) {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    }
}
echo '</select>';
?>
         <div class="float-right">
            <?php
if ($_SESSION['__limit'] != '') {
    //echo $row_total;
    if ($total_row > 0) {echo Pages($total_row, $perpage, $path_page);}
}
?>
            </div>
         </div>
</form>
</div>