<?php
use Intervention\Image\ImageManagerStatic as Image;

$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = ws_post('cid') ? count(ws_post('cid')) : null;
if ($__post_task == 'unpublish') {
    // ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = @ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    // HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = @ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    // LƯU THỨ TỰ MỤC TIN
    $soluong_row = count(@ws_post('cid'));
    $array_cid   = @ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = @ws_post('cid')[$a];
        $value_order = @ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(@ws_post('cid'));
    $array_cid   = @ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = @ws_post('cid')[$r];
        $value_img  = @ws_post('img')[$id_order];
        $value_file = @ws_post('file')[$id_order];
        $value_icon = @ws_post('icon')[$id_order];
        // thay doi thu tu san pham
        //$thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";
      //
        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
           //  xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddanhmuc  = '" . $id_order . "' ");
            $db->sqlDelete("tbl_mausac", " idtype  = '{$id_order}' ");

            // xoa idtype của danh mục này trong bảng nội dung
            $db->xoaIdTypeNoiDung($id_order);
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                if(file_exists("$__path")){
                    unlink("$__path");
                }
                if(file_exists("$__paththumb")){
                    unlink("$__paththumb");
                }
            }
            if ($value_file != '') {
                $__path = $__config['path_file'] . $value_file;
                unlink("$__path");
            }
            if ($value_icon != '' && file_exists("../uploads/danhmuc/" . $value_icon)) {
                unlink("../uploads/danhmuc/" . $value_icon);
            }
       }
    }
}
#
# BẤM NÚT LƯU
# --------------------------------------------------------------------------
if ($__getaction == 'save') {
    /** Tao thu muc neu chua ton tai */
create_dir($__config['path_img']);
create_dir($__config['path_file'], false);

    // LƯU TRỮ DỮ LIỆU
    $idtype     = ws_post('idtype');
    $keyname    = ws_post('keyname');
    $baiviet    = ws_post('baiviet');
    $img        = $_FILES['img']['name'];
    $file       = $_FILES['file']['name'];
    $colvalue   = @ws_post('colvalue');
    $colvalue   = implode(",",$colvalue);
    $loai       = ws_post('loai');
    $thutu      = ws_post('thutu');
    $anhien     = ws_post('anhien');
    $img_url    = '../uploads/danhmuc/' . $img;
    $id         = ws_post('id');
    $subid      = ws_post('subid');
    $anhien     = ws_post('anhien');
    $xoaimg     = ws_post('xoaimg');
    $xoafile    = ws_post('xoafile');
    $imgname    = ws_post('imgname');
    $filename   = ws_post('filename');

    // SET module default is home if not select module
    if( empty($idtype) ){
        $idtype = $db->getNameFromID(
            "tbl_danhmuc_type",
            "id",
            "op",
            "'home'"
        );
    }

    if ($__getid != '') {
        // neu la cap nhat
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->createSubID($__table, $__id, @ws_post('parenid'));
    }
    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $thutu       = (int) substr($subid, -3);
    $images_name = @ws_post('url'.$__defaultlang);
    #
    # TRƯỜNG HỢP THÊM MỚI DỮ LIỆU
    # ---------------------------------------------
    if ($__getid == '') {
        // upload img
        if ($img != '') {
            $extfile = pathinfo($img, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/danhmuc/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"],$__config['path_img'].$imgfile);
            $ResizeImage->load($__config['path_img'] . $imgfile);
            $ResizeImage->save($__config["path_img"] . $imgfile,$__config['compression']);

            // img thumb
            $ResizeImage->load($__config["path_img"] . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save($__config['path_img'] . "thumb/" . $imgfile, $__config['compression']);
        }
        // upload File
        if ($file != '') {
            $extfile  = pathinfo($file, PATHINFO_EXTENSION);
            $filefile = $images_name . '-file.' . $extfile;
            if (file_exists("../uploads/danhmuc/" . $filefile)) {
                $filefile = rand(100) . $filefile;
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], "../uploads/danhmuc/$filefile");
        }
        #
        # INSERT ICON
        # -------------------------------------------
        // B1. Neu la chon san
        $kieuIcon = ws_post('type_icon');
        if ($kieuIcon == 1 && $kieuIcon != -1) {
            $iconName = ws_post('icon_faicon');
        } else if ($kieuIcon == 0 && $kieuIcon != -1) {
            $iconName = $_FILES['icon_img']['name'];
            if (!empty($iconName)) {
                $tenicon =  $images_name ."-". $iconName;
                move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img'].$tenicon);
                $imgs = Image::make($__config['path_img'] . $tenicon);
                $imgs->resize($__config['icon_width'], null, function($constraint) {
                    $constraint->aspectRatio();
                });
                $imgs->save($__config["path_img"] . $tenicon, 100);
                $iconName = $tenicon;
            }
        } else {
            $iconName = '';
        }
        $aray_insert = array(
            "id"       => $subid,
            "idtype"   => $idtype,
            "icon"     => $iconName,
            "keyname"  => $keyname,
            "img"      => $imgfile,
            "file"     => $filefile,
            "colvalue" => $colvalue,
            "loai"     => $loai,
            "thutu"    => $thutu,
            "anhien"   => $anhien,
        );
        $id_insert = $db->insert($__table, $aray_insert);

        #
        # INSERT_COLOR
        # ---------------------------------
        if (ws_post('chonmausac') && !empty(ws_post('chonmausac'))) {
            $inserBG = ws_post('bg_danhmuc');
            $inserCL = ws_post('color_danhmuc');
            $arr_bg = array(
                'bgcolor' => $inserBG,
                'color'   => $inserCL
            );
            $db->sqlUpdate($__table, $arr_bg, "id = '$subid' ");
        }
        // them du lieu vao bang noi dung danh muc
        $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = ws_post('ten' . $idlang);
                $tieude     = ws_post('tieude' . $idlang);
                $mota       = ws_post('mota' . $idlang);
                $noidung    = ws_post('noidung' . $idlang);
                $url        = ws_post('url' . $idlang);
                $link       = ws_post('link' . $idlang);
                $target     = ws_post('target' . $idlang);
                $tukhoa     = ws_post('tukhoa' . $idlang);
                $motatukhoa = ws_post('motatukhoa' . $idlang);
                // kiem tra url neu da co roi thì them ki tu cuoi url
                $s_checkurl = "SELECT url from $__tablenoidung where url = '$url'";
                $d_checkurl = $db->rawQuery($s_checkurl);
                if (count($d_checkurl) > 0) {
                    $url = $url . '-' . rand(0, 100);
                }
                // luu du lieu vao bang danh muc lang
                $aray_insert_lang = array(
                    "iddanhmuc"  => $subid,
                    "idlang"     => $idlang,
                    "ten"        => $ten,
                    "tieude"     => $tieude,
                    "mota"       => $mota,
                    "noidung"    => $noidung,
                    "url"        => $url,
                    "link"       => $link,
                    "target"     => $target,
                    "tukhoa"     => $tukhoa,
                    "motatukhoa" => $motatukhoa,
                );
                $db->insert($__tablenoidung, $aray_insert_lang);

            }
        }
    } else {
        #
        # TRƯỜNG HỢP CẬP NHẬT DỮ LIỆU
        # ------------------------------------------
        $aray_insert = array(
            "idtype"   => $idtype,
            "keyname"  => $keyname,
            "colvalue" => $colvalue,
            "loai"     => $loai,
            "anhien"   => $anhien,
        );
        $db->where("id", $__postid);
        $db->update( $__table, $aray_insert);
        #
        # UPDATE ICON
        # ---------------------------------------
        $kieuIcon = ws_post('type_icon');
        $iconOld  = ws_post('iconname');
        if ($kieuIcon == 1 && $kieuIcon != -1) {
            // xoa icon cũ
            if (file_exists($__config['path_img'].$iconOld)) {
                unlink($__config['path_img'].$iconOld);
            }
            // Trường hợp update là fa-icon
            $iconName = ws_post('icon_faicon');
            $db->where("id", $__postid);
            $db->update($__table, array("icon" => $iconName));

        } else if ($kieuIcon == 0 && $kieuIcon != -1) {
            $iconName = $_FILES['icon_img']['name'];
            if (!empty($iconName)) {
                // 1. update icon new
                // xoa icon cũ
                if (file_exists($__config['path_img'].$iconOld)) {
                    unlink($__config['path_img'].$iconOld);
                }
                $tenicon =  $images_name ."-". $iconName;
                move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img']. $tenicon);
                $imgs = Image::make($__config['path_img'] . $tenicon);
                $imgs->resize($__config['icon_width'], null, function($constraint) {
                    $constraint->aspectRatio();
                });
                $imgs->save($__config["path_img"] . $tenicon, 100);
                $iconName = $tenicon;
                $db->where("id", $__postid);
                $db->update($__table, array("icon" => $iconName));
            }
        } else {
            $iconName = '';
            // 4. doi tu img -> khong (xoa icon)
            // xoa icon cũ
            if (file_exists($__config['path_img'].$iconOld)) {
                @unlink($__config['path_img'].$iconOld);
            }
            $db->where("id", $__postid);
            $db->update($__table, ["icon" => null]);
        }
        // 2. check xoa icon
        if ( ws_post('xoaicon') == 1 ) {
            if (file_exists($__config['path_img'].$iconOld)) {
                unlink($__config['path_img'].$iconOld);
            }
            $db->where("id", $__postid);
            $db->update($__table, ["icon" => null]);
        }
        #
        # UPDATE_COLOR
        # -----------------------------
        if (ws_post('chonmausac')) {
            if (!empty(ws_post('chonmausac'))) {
                $arr_bg_update = [
                    'bgcolor' => ws_post('bg_danhmuc'),
                    'color'   => ws_post('color_danhmuc')
                ];
            } else {
                $arr_bg_update = [
                    'bgcolor' => null,
                    'color'   => null,
                ];
            }
            $db->where("id", $__postid);
            $db->update($__table, $arr_bg_update);
        }
        if( ws_post('chonmausac') == 0 ) {
            $db->where("id", $__postid);
            $db->update($__table, [
                'bgcolor' => null,
                'color'   => null,
            ]);
        }
        // them du lieu vao bang noi dung danh muc
        $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                // lap theo so luong ngon ngu
                $tenlang = @$info_lang['ten'];
                $idlang  = @$info_lang['id'];
                $idkey   = @$info_lang['idkey'];
                // get noi dung post qua
                $ten        = ws_post('ten' . $idlang);
                $tieude     = ws_post('tieude' . $idlang);
                $mota       = ws_post('mota' . $idlang);
                $noidung    = ws_post('noidung' . $idlang);
                $url        = ws_post('url' . $idlang);
                $link       = ws_post('link' . $idlang);
                $target     = ws_post('target' . $idlang);
                $tukhoa     = ws_post('tukhoa' . $idlang);
                $motatukhoa = ws_post('motatukhoa' . $idlang);
                // kiem tra url neu da co roi thì them ki tu cuoi url
                if (@ws_post('hi_url'.$idlang) != $url) {
                    $s_checkurl = "select url from $__tablenoidung where url = '$url'";
                    $d_checkurl = $db->rawQuery($s_checkurl);
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                }
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "SELECT id from $__tablenoidung where iddanhmuc = '" . $__postid . "' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) {
                    // da tồn tại nội dung rồi thì update lại nội dung
                    // cap nhat lai du lieu
                    $aray_insert_lang = array(
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "mota"       => $mota,
                        "noidung"    => $noidung,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                    );
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = {$__postid} AND idlang = {$idlang} ");
                } else {
                    $aray_insert_lang = array(
                        "iddanhmuc"  => $__postid,
                        "idlang"     => $idlang,
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "mota"       => $mota,
                        "noidung"    => $noidung,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang
        // kiem tra neu thay img thi sẽ xóa hình cũ đi
        // xoa img
        if ($xoaimg == 1) {
            unlink("../uploads/danhmuc/" . $imgname);
            unlink("../uploads/danhmuc/thumb/" . $imgname);
            $aray_insert = array("img" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // xoa file
        if ($xoafile == 1) {
            unlink("../uploads/danhmuc/" . $filename);
            $aray_insert = array("file" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat img
        if ($img != '') {
            if ($imgname != '') {
                unlink("../uploads/danhmuc/" . $imgname);
                unlink("../uploads/danhmuc/thumb/" . $imgname);
            }
            //up file moi len
            $extfile = pathinfo($img, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/danhmuc/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"], $__config['path_img'] . $imgfile);
            $ResizeImage->load($__config['path_img'] . $imgfile);
            $ResizeImage->save($__config["path_img"] . $imgfile,$__config['compression']);

            // img thumb
            $ResizeImage->load($__config["path_img"] . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save($__config['path_img'] . "thumb/" . $imgfile, $__config['compression']);

            $aray_insert = array("img" => $imgfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat file
        if ($file != '') {
            if ($filename != '') {
                unlink("../uploads/danhmuc/" . $filename);
            }
            //up file moi len
            $extfile  = pathinfo($file, PATHINFO_EXTENSION);
            $filefile = $images_name . '-file.' . $extfile;
            if (file_exists("../uploads/danhmuc/" . $filefile)) {
                $filefile = rand(0, 100) . $filefile;
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], "../uploads/danhmuc/$filefile");
            $aray_insert = array("file" => $filefile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // update idtype neu chuyen danh muc di vi tri khac
        if (@ws_post('hi_parenid') != @ws_post('parenid')) {
           /* $subid = $db->createSubID($__table, $__id, @ws_post('parenid'));
            // update bang tbl_danhmuc
            $aray_insert = array("id" => $subid);
            $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
            // update bang tbl_danhmuc_lang
            $aray_insert_lang = array("iddanhmuc" => $subid);
            $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = $__postid");*/
            $db->taoLaiSubId( @ws_post('parenid'), $__postid, 1);
        }
    } // end if cap nhat
}
$link_redirect = './?op=' . ws_get("op") . '&page='.ws_post('getpage');

if ( ws_post('luuvathem') == 'luuvathem') {

    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $subid;
}
if ( ws_post('saveandnew') == 'saveandnew') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm';
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
