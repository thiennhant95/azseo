<?php
$array_data = get_data("configdanhmuc", '*', 'anhien,=,1');
$_SESSION['array_data'] = array();
$_SESSION['array_data'] = $array_data;

$array_keyname = get_data("configkeyname", '*', 'anhien,=,1');
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Quản lý danh mục",
    "table"           => 'tbl_danhmuc',
    "tablenoidung"    => 'tbl_danhmuc_lang',
    "id"              => 'id',
    "idtype"          => 1,
    "icon"            => 1,
    "keyname"         => 1,
    "img"             => 1,
    "mausac"          => 1,
    "file"            => 1,
    "loai"            => 1,
    "thutu"           => 1,
    "ten"             => 1,
    "tieude"          => 1,
    "mota"            => 1,
    "noidung"         => 1,
    "url"             => 1,
    "link"            => 1,
    "target"          => 1,
    "tukhoa"          => 1,
    "motatukhoa"      => 1,
    "anhien"          => 1,
    "noindex"         => 1,
    "action"          => 1,
    "icon_width"      => 30,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/danhmuc/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 1,
    "action"          => 1,
    "sizeimagesthumb" => 385,
    "compression"     => 80);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
