<?php

// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    if ($__sua == 0) {
        echo '
        <script language="javascript">
            alert("' . $arraybien['khongcoquyensua'] . '");
            location.href="./?op=' . $__getop . '"; </script>';
        exit;
    }
} else {
    // la them
    if ($__them == 0) {
        echo '
        <script language="javascript">
            alert("' . $arraybien['khongcoquyenthem'] . '");
            location.href="./?op=' . $__getop . '"; </script>';
        exit;
    }
}
$_getop              = ws_get('op');
$_id_name            = @$__config['id'];
$_idtype             = @$__config['type'];
$_idSP               = @$__config['idsp'];
$ACTION              = '';
$CONTENT             = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
if ($__getid != '') {
    $__cid     = ws_get('id');
    $s_sanpham = "SELECT * from $__table where id = $__getid";
    $data      = $db->rawQuery($s_sanpham);
    $data     = $data[0];
    $id       = check_select($data['id']);
    $idtype   = check_select($data['idtype']);
    $baiviet  = check_select($data['baiviet']);
    $keyname  = check_select($data['keyname']);
    $icon     = check_select($data['icon']);
    $img      = check_select($data['img']);
    $file     = check_select($data['file']);
    $colvalue = check_select($data['colvalue']);
    $loai     = check_select($data['loai']);
    $thutu    = check_select($data['thutu']);
    $anhien   = check_select($data['anhien']);
    // Get Mau
    $dColor      = check_select($data['color']);
    $dBackground = check_select($data['bgcolor']);
    $img_view   = '../uploads/danhmuc/' . $img;
    $icon_view  = '../uploads/danhmuc/' . $icon;
} else {
    $anhien      = 1;
    $colvalue    = '';
    $dColor      = '';
    $dBackground = '';
}
$ten_post = replace_html(ws_post('ten'.$__defaultlang) && ws_post('ten'.$__defaultlang));
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   var form = document.adminForm;
   // do field validation
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   } else if (form.ten' . $__defaultlang . '.value == "") {
      alert(\'Tên không được để trống !\');
      form.ten' . $__defaultlang . '.focus();
   } ';
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>';
if (ws_get('id')) {
    $geturl_xemtruoc = $db->getNameFromID('tbl_danhmuc_lang', 'url', 'iddanhmuc', "'" . ws_get('id') . "' and idlang = '{$__defaultlang}'");
    $ACTION .= '<a class="btn btn-default" data-toggle="tooltip" title="' . $arraybien['xemchuyenmuctrenwebsite'] . '" href="../' . $geturl_xemtruoc . '/" target="_blank" >
                     <i class="fa fa-external-link"></i>
                </a>';
}
$ACTION .= '
   <a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-times-circle"></i> ' . $arraybien['luuvadong'] . '
   </a>
   <a class="btn btn-default" onclick="javascript: submitbutton_addnew(\'saveandnew\')" href="#">
    <i class="fa fa-clone"></i> ' . $arraybien['luuvathem'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
// col left
$CONTENT .= '
    <form action="./?op=' . (!empty(ws_get('op')) ? ws_get('op') : null) . '&method=query&action=save&id=' . (!empty(ws_get('id')) ? ws_get('id') : null) . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
$CONTENT .= '
      <div class="form-group">
         <label for="inputdefault">' . $arraybien['danhmuccha'] . '</label>';
$sql2 = "SELECT a.id, b.ten
            FROM {$__table} AS a
            INNER JOIN {$__tablenoidung} AS b On a.id =  b.iddanhmuc
            where b.idlang = '{$__defaultlang}'
            order by a.id";
$compare_id = substr($id, 0, strlen($id) - 4);
$d_sql2     = $db->rawQuery($sql2);
if (count($d_sql2) > 0) {
    $CONTENT .= '<select class="form-control" name="parenid" id="parenid" onchange="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=thuocmodules&value=\'+this.value,\'loadthuocmodules\')">';
    $CONTENT .= '<option value="">' . $arraybien['chonmotloai'] . '</option>';
    foreach ($d_sql2 as $key_sql2 => $info_sql2) {
        $info2_ten  = $info_sql2['ten'];
        $info2_id   = $info_sql2['id'];
        $lenstr     = '';
        $selectedid = '';
        for ($kk = 4; $kk < strlen($info2_id); $kk += 4) {
            $lenstr .= '==';
        }
        if ($compare_id == $info2_id) {
            $CONTENT .= '<option selected="selected" value="' . $info2_id . '">' . $lenstr . '' . $info2_ten . '</option>';
        } else {
            $CONTENT .= '<option value="' . $info2_id . '">' . $lenstr . '' . $info2_ten . '</option>';
        }
    }
    $CONTENT .= '</select>';
    $CONTENT .= '<input type="hidden" name="hi_parenid" value="' . $compare_id . '" />';
}
$CONTENT .= '
      </div>';
$CONTENT .= '
      <div class="form-group">
         <label for="inputdefault">' . $arraybien['thuocmodules'] . '</label>';
$CONTENT .= '<div id="loadthuocmodules">';
$sql2 = "SELECT a.id, b.ten
                    from tbl_danhmuc_type AS a
                    inner join tbl_danhmuc_type_lang AS b On a.id =  b.iddanhmuc
                    where b.idlang = '{$__defaultlang}'
               and a.anhien = 1
                    order by a.id";
$compare_id = substr($id, 0, strlen($id) - 4);
$option1    = '<option value="">' . $arraybien['chonmotloai'] . '</option>';
$CONTENT .= $db->createComboboxDequySql('idtype', $option1, 'form-control', '', $sql2, 'ten', 'id', $idtype);
$CONTENT .= '
         </div>
      </div>';
// MAU SAC
// --------------------------------------------------------------------------
$CONTENT.= include("application/templates/inc/color.inc.php");

# ICON
// ===========================================================================
$CONTENT.= include 'application/templates/inc/icon.inc.php';


if ($__config['keyname'] == 1) {
    $d_keyname = get_data('configkeyname', '*', 'anhien,=,1');
    $sumid     = max_number_in_array($d_keyname, 'id');
    if (count($d_keyname) > 0) {
        $CONTENT .= '
        <div class="form-group" id="icon-select">
            <div class="input-group">
                <div class="input-group-addon">Nhóm dữ liệu</div>
                <select name="keyname" id="keyname" class="form-control">
                    <option value="">Vui lòng chọn</option>';
        foreach ($array_keyname as $key => $valuekeyname) {
            $id  = $valuekeyname['dinhdanh'];
            $ten = $valuekeyname['ten'];
            $CONTENT .= '<option value="' . $id . '" ' . ($keyname == $id ? 'selected="selected"' : null) . '>' . $ten . '</option>';
        }
        $CONTENT .= '
                </select>
            </div>
        </div>';
    }
}
// cot value
//$array_data lay ben file danhmuc
if (count($array_data) > 0) {
    foreach ($array_data as $key_configdanhmuc => $info_configdanhmuc) {
        $config_dinhdanh = $info_configdanhmuc['dinhdanh'];
        $config_ten      = $info_configdanhmuc['ten'];
        $CONTENT .= '
          <div class="checkbox">
            <label>';
        $checked_colvalue = (strpos($colvalue, $config_dinhdanh) > -1) ? ' checked="checked" ' : null;
        $CONTENT .= '<input value="' . $config_dinhdanh . '" ' . $checked_colvalue . ' type="checkbox" name="colvalue[]" />';
        $CONTENT .= '' . $config_ten . '
            </label>
         </div>';
    }
}
//kết quả: 1
if ($__config['anhien'] == 1) {
    $CONTENT .= '
         <div class="checkbox">
            <label>';
    $checked_anhien = ($anhien == 1) ? ' checked="checked" ' : null;
    $CONTENT .= '<input value="1" ' . $checked_anhien . ' type="checkbox" name="anhien" id="anhien" />';
    $CONTENT .= '
            ' . $arraybien['hienthi'] . '</label>
         </div>';
}
if ($__config['img'] == 1) {
    $CONTENT .= '
         <div class="form-group">
         <label for="inputdefault">Hình</label>
         <input type="file" name="img" id="img" />';
    if ($img != '') {
        $CONTENT .= '
          <img src="' . $img_view . '" border="0" width="30" />
          <label><input type="checkbox" name="xoaimg" id="xoaimg" value="1" />' . $arraybien['xoahinh'] . '</label>
          <input type="hidden" name="imgname" id="imgname" value="' . $img . '" />';
    }
    $CONTENT .= '
         </div>';
}
if ($__config['file'] == 1) {
    $CONTENT .= '
         <div class="form-group">
         <label for="inputdefault">File</label><br />
         ' . $arraybien['khiuploadfilenayhethongseuutienlienkettaifileongoaiwebsite'] . '
         <input type="file" name="file" id="file" />';
    if ($file != '') {
        $CONTENT .= '
          <b>File hiện tại:</b> ' . $file . '<br />
          <label><input type="checkbox" name="xoafile" id="xoafile" value="1" />' . $arraybien['xoafile'] . '</label>
          <input type="hidden" name="filename" id="filename" value="' . $file . '" />';
    }
    $CONTENT .= '
         </div>';
}
$CONTENT .= '
         </div>
      </div>
</div>';
// col right
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading">&nbsp;</div>
  <div class="panel-body">
    <div id="tabs">
     <ul>';
$s_lang = "SELECT a.id,a.idkey,b.ten
           from tbl_lang AS a
           inner join tbl_lang_lang AS b
           where a.id = b.iddanhmuc
           and b.idlang = '{$__defaultlang}'
           and a.anhien = 1
           order by thutu Asc";
$d_lang = $db->rawQuery($s_lang);
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $ten    = $info_lang['ten'];
        $idlang = $info_lang['id'];
        $idkey  = $info_lang['idkey'];
        $CONTENT .= '<li type="#tab' . ($key_lang + 1) . '">' . $ten . '</li>';
    }
}
$CONTENT .= '
        <div class="clear"></div>
    </ul>
    <div style="clear:both"></div>
    <div class="tab-text">';
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $tenlang = $info_lang['ten'];
        $idlang  = $info_lang['id'];
        $idkey   = $info_lang['idkey'];
        // lay du lieu theo ngon ngu
        $s_noidung_data = "SELECT * FROM {$__tablenoidung} where idlang = {$idlang} and iddanhmuc = '" . $__cid . "' ";
        $d_noidung_data = $db->rawQuery($s_noidung_data);
        @$d_noidung_data = $d_noidung_data[0];
        $ten            = check_select($d_noidung_data['ten']);
        $tieude         = check_select($d_noidung_data['tieude']);
        $url            = check_select($d_noidung_data['url']);
        $link           = check_select($d_noidung_data['link']);
        $mota           = check_select($d_noidung_data['mota']);
        $noidung        = check_select($d_noidung_data['noidung']);
        $tukhoa         = check_select($d_noidung_data['tukhoa']);
        $target         = check_select($d_noidung_data['target']);
        $motatukhoa     = check_select($d_noidung_data['motatukhoa']);
        $CONTENT .= '
         <div id="tab' . ($key_lang + 1) . '">
         <div class="adminform">';
        if ($__config['ten'] == 1) {
            $CONTENT .= '
         <div  class="form-group">
            <label for="inputdefault">' . $arraybien['ten'] . ' ' . $tenlang . '</label>';
            if ($__getid != '') {
                $CONTENT .= '
               <input class="form-control"   name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />
               <button style="margin-top:5px;" type="button" class="btn btn-success"  onclick="taoseo(\'ajax.php?op='.$__getop.'&name=url&id=' . $__getid . '&title=\'+document.adminForm.ten' . $idlang . '.value,\'url' . $idlang . '\',\'tieude' . $idlang . '\',\'tukhoa' . $idlang . '\')"><i class="fa fa-refresh"></i> ' . $arraybien['taolaiduongdan'] . '</button>
               ';
            } else {
                $CONTENT .= '
               <input data-val-required="Tên sản phẩm không được để trống" class="form-control"  onchange="taoseo(\'ajax.php?op='.$__getop.'&name=url&id=' . $__getid . '&title=\'+this.value,\'url' . $idlang . '\',\'tieude' . $idlang . '\',\'tukhoa' . $idlang . '\')" name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />';
            }
            $CONTENT .= '
         </div>';
        }
        if ($__config['mota'] == 1) {
            $CONTENT .= '
              <div class="form-group">
                 <label for="inputdefault">' . $arraybien['mota'] . ' ' . $tenlang . '</label>
                 <textarea class="form-control seo-placeholder ckeditor"  name="mota' . $idlang . '" cols="100" rows="4">' . $mota . '</textarea>
              </div>';
        }
        if ($__config['noidung'] == 1) {
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['noidung'] . ' ' . $tenlang . '</label>
                <textarea name="noidung' . $idlang . '" id="noidung' . $idlang . '" cols="" rows="" class="ckeditor">' . $noidung . '</textarea>';
            $CONTENT .= '</div>';
        }
        if ($__config['url'] == 1) {
            $CONTENT .= '
              <div class="form-group">
              <label class="control-label strong" for="Alias">' . $arraybien['duongdan'] . ' ' . $tenlang . '</label>
              <div class="row_countkitu">' . $arraybien['chieudaitoiuuchoseo'] . ' <div class="sokitutoida">/115</div><div class="kitudadung" id="kitudadungurl' . $idlang . '">' . strlen($url) . '</div></div>
                  <div class="controls">
                    <div class="input-group">
                     <span class="input-group-addon">http://' . $_SERVER['HTTP_HOST'] . '/</span>
                     <input  onkeyup="CountKey(\'url' . $idlang . '\',\'kitudadungurl' . $idlang . '\');" class="form-control seo-placeholder" placeholder="URL chuyên mục, Bạn có thể nhập dạng: chuyen-muc" name="url' . $idlang . '" type="text" id="url' . $idlang . '" value="' . $url . '" size="70" />
                <input type="hidden" value="' . $url . '" name="hi_url' . $idlang . '" />
                     </div>
                  </div>
              </div>';
        }
        if ($__config['link'] == 1) {
            $CONTENT .= '
              <div class="input-group">
                 <span class="input-group-addon">' . $arraybien['lienket'] . ' ' . $tenlang . '</span>
                <input class="form-control" placeholder="Nhập link nếu bạn muốn chuyển hướng đến một link khác: VD: http://webso.vn"  name="link' . $idlang . '" type="text" id="link' . $idlang . '" value="' . $link . '"  />
              </div><br />
              <div class="input-group">
                <span class="input-group-addon" >' . $arraybien['phuongthuc'] . '</span>
                <div class="col-lg-3 margin0 padding0">
               <select name="target' . $idlang . '" id="target' . $idlang . '" class="form-control col-lg-2 seo-placeholder" >';
            foreach ($_arr_target as $key_target => $value_target) {
                if ($key_target == $target) {
                    $CONTENT .= '<option selected="selected" value="' . $key_target . '">' . $value_target . '</option>';
                } else {
                    $CONTENT .= '<option value="' . $key_target . '">' . $value_target . '</option>';
                }
            }
            $CONTENT .= '</select>';
            $CONTENT .= '
                </div>
             </div>
             <div class="clear"></div><br />';
        }
        $CONTENT .= '<h4>Cấu hình seo</h4>';
        if ($__config['tieude'] == 1) {
            $CONTENT .= '
            <div class="form-group">
              <label for="inputdefault">' . $arraybien['tieudetrang'] . ' ' . $tenlang . '</label>
             <div class="row_countkitu">' . $arraybien['sokitudadung'] . ': <div class="sokitutoida">/70</div><div class="kitudadung" id="kitudadungtieude' . $idlang . '">' . strlen($tieude) . '</div></div>
              <input class="form-control" onkeyup="CountKey(\'tieude' . $idlang . '\',\'kitudadungtieude' . $idlang . '\');"  name="tieude' . $idlang . '" type="text" id="tieude' . $idlang . '" value="' . $tieude . '"  />
              </div>';
        }
        if ($__config['motatukhoa'] == 1) {
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['motatukhoa'] . ' ' . $tenlang . '</label>
                <div class="row_countkitu">' . $arraybien['sokitudadung'] . ': <div class="sokitutoida">/320</div><div class="kitudadung" id="kitudadungmota' . $idlang . '">' . strlen($motatukhoa) . '</div></div>
                <textarea class="form-control" onkeyup="CountKey(\'motatukhoa' . $idlang . '\',\'kitudadungmota' . $idlang . '\');" placeholder="Mô tả từ khóa nhập ít hơn 320 kí tự"  id="motatukhoa' . $idlang . '" name="motatukhoa' . $idlang . '" cols="100" rows="6">' . $motatukhoa . '</textarea>
            </div>';
        }
        if ($__config['tukhoa'] == 1) {
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['tukhoa'] . ' ' . $tenlang . '</label>
                <textarea class="form-control" placeholder="Nhập từ khóa cách nhau bằng dấu \',\' phẩy"  id="tukhoa' . $idlang . '" name="tukhoa' . $idlang . '" cols="100" rows="2">' . $tukhoa . '</textarea>
             </div>';
        }
        $CONTENT .= '
            </div>
        </div>';
    }
}
$CONTENT .= '
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="" name="saveandnew"><input type="hidden" value="'.ws_get('page').'" name="getpage">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">
   <input type="hidden" name="getpage" value="'.ws_get('page').'" />
   </div>'; // end tab-text
$CONTENT .= '
</div></div></div></div>'; // end col data
$CONTENT .= '
<div class="clear"></div>
</form>';
$file_tempaltes = 'application/files/templates.tpl';
$array_bien     = array(
    '{CONTENT}' => $CONTENT,
    '{ACTION}'  => $ACTION);
echo load_content($file_tempaltes, $array_bien);
