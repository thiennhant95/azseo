<?php
$data      = '';
$thisTable = $_SESSION['__config']['table'];
switch ($name) {
    case 'anhien':
        if ($value == 1) {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        } else {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        }
        $aray_update = array("anhien" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;

        case 'noindex':
        if ($value == 0) {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=noindex&value=1\',\'noindex' . $id . '\')">';
            $data .= '<i title="Không cho Google Index! Bài viết này sẽ không hiển thị trên kết quả tìm kiếm" class="fa fa-ban color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        } else {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=noindex&value=0\',\'noindex' . $id . '\')">';
            $data .= '<i title="Cho phép index! Google có thể đưa bài viết này lên kết quả tìm kiếm" class="fa fa-check-square-o color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        }
        $aray_update = array("noindex" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;

    case 'colvalue':
        // lay gia tri colvalue de so sanh
        $colvalue = $db->getNameFromID($thisTable, "colvalue", "id", "'" . $id . "'");
        if ($colvalue != '') {
            if (strpos($colvalue, $value) > -1) {
                $colvalue   = str_ireplace($value . ',', "", $colvalue);
                $colvalue   = str_ireplace(',' . $value, "", $colvalue);
                $colvalue   = str_ireplace($value, "", $colvalue);
                $dataupdate = $colvalue;
            } else {
                $dataupdate = $colvalue . ',' . $value;
            }
        } else {
            $dataupdate = $value;
        }
        $aray_update = array("colvalue" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");

        // xuat gia tri da xu ly ra
        $select_vitri = '';
        $vitridachon  = '';
        $colvalue     = $dataupdate;
        //$array_data lay ben file danhmuc
        if (count($_SESSION['array_data']) > 0) {
            $vitridachon .= '<div id="vitri' . $id . '">';
            $select_vitri .= '<select class="form-control danhmuc" onchange="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=colvalue&value=\'+this.value,\'vitri' . $id . '\')">
                      <option value="">Vị trí hiển thị</option>';
            foreach ($_SESSION['array_data'] as $key_configdanhmuc => $info_configdanhmuc) {
                $config_dinhdanh = $info_configdanhmuc['dinhdanh'];
                $config_ten      = $info_configdanhmuc['ten'];
                if (strpos($colvalue, $config_dinhdanh) > -1) {
                    $vitridachon .= '
                        <div class="btn_vitri">
                          <a class="cursor-pointer" data-toggle="tooltip" title="Bỏ chọn" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=colvalue&value=' . $config_dinhdanh . '&ten=' . $config_ten . '&i=' . $i . '\',\'vitri' . $id . '\')">
                            <i class="fa fa-times float-right"></i>
                          </a>
                          ' . $config_ten . '
                        </div>';
                } else {
                    $select_vitri .= '<option value="' . $config_dinhdanh . '">' . $config_ten . '</option>';
                }
            }
            $select_vitri .= '</select>';
            $vitridachon .= $select_vitri;
            $vitridachon .= '</div>';
            $data = $vitridachon;
        }
        break;

    case 'baiviet':
        if ($value == 1) {
            $data .= '<a class="cursor-pointer" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=baiviet&value=0\',\'baiviet' . $id . '\')">';
            $data .= 'Bài viết <i class="fa fa-times-circle color-red"></i>';
            $data .= '</a>';
            $dataupdate = 0;
        } else {
            $data .= '<a class="cursor-pointer" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=baiviet&value=1\',\'baiviet' . $id . '\')">';
            $data .= 'Bài viết<i class="fa fa-check-circle"></i>';
            $data .= '</a>';
            $dataupdate = 1;
        }
        $aray_update = array("baiviet" => $dataupdate);
        $result      = $db->sqlUpdate('tbl_danhmuc', $aray_update, "id = $id and loai = 0");
        break;

    case 'thutu':
        $aray_update = array("thutu" => $value);
        $result      = $db->sqlUpdate('tbl_danhmuc', $aray_update, "id = '$id' ");
        break;

    case 'thuocmodules':
        $idmodules = $db->getNameFromID($thisTable, "idtype", "id", "'" . $value . "'");
        $sql2      = "SELECT a.id, b.ten
                        from tbl_danhmuc_type AS a
                        inner join tbl_danhmuc_type_lang AS b On a.id =  b.iddanhmuc
                        where b.idlang = '" . $_SESSION['__defaultlang'] . "'
                        order by a.thutu Asc";
        $option1 = '<option value="">' . $arraybien['chonmotloai'] . '</option>';
        $data .= $db->createComboboxDequySql("idtype", $option1, "form-control", "", $sql2, "ten", "id", $idmodules);
        break;

    case 'url':
        if ($type == 'url') {
            if ($name == 'url') {
                // chieu dai url khoang 115 ki tu
                $chieudaidomain    = strlen('http://' . $_SERVER['HTTP_HOST']);
                $chieudaiurlcanlay = 112 - $chieudaidomain;
                $title_url2        = substr($title_url, 0, $chieudaiurlcanlay);
                $title_url2        = $title_url2 . " ";
                $title_url2        = str_replace("- ", "", $title_url2);
                $title_url2        = trim($title_url2);
                if ($id == '') {
                    $urlcheck = $db->getNameFromID("tbl_danhmuc_lang", "url", "url", "'" . $title_url2 . "'");
                    if ($urlcheck != '') {
                        $urlreturn .= $urlcheck . '-' . $rannumber;
                    } else {
                        $urlreturn = $title_url2;
                    }
                } else {
                    if ($title_url == $urlcu) {
                        $urlreturn = $urlcu;
                    } else {
                        $urlcheck = $db->getNameFromID("tbl_danhmuc_lang", "url", "url", "'" . $title_url2 . "'");
                        if ($urlcheck != '') {
                            $urlreturn .= $urlcheck . '-' . $rannumber;
                        } else {
                            $urlreturn = $title_url2;
                        }
                    }
                }
                $urlreturn = preg_replace('/[^a-zA-Z0-9_-]/', '', $urlreturn);
            }
        }
        // xu ly neu la title
        if ($type == 'title') {
            if (strlen($title) > 70) {
                // lay ki tu thu 71
                $kitu71    = $db->utf8_substr($title, 70, 1);
                $kitutrang = 0;
                if ($kitu71 == '' or $kitu71 == ',' or $kitu71 == '.') {
                    $kitutrang = 70;
                } else {
                    // kiem tra khi nao gap ki tu trang
                    for ($k = 70; $k > 0; $k--) {
                        $ktkitu = $db->utf8_substr($title, ($k - 1), 1);
                        if ($ktkitu == ' ' or $ktkitu == ',' or $ktkitu == '.') {
                            $kitutrang = $k;
                            break;

                        }
                    }
                }
                $urlreturn = $db->utf8_substr($title, 0, $kitutrang);
                //$urlreturn = strlen($title);
            } else {
                $urlreturn = $title;
            }
        }
        // xu ly neu la tu khoa
        if ($type == 'tukhoa') {
            $urlreturn = $title . ', ' . tao_url($title);
        }
        $data = $urlreturn;
        break;

} // End Swith

echo $data;
