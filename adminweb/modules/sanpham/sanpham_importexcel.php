<?php
    require dirname(dirname(dirname(__DIR__))) . "/configs/inc.php";

if (ws_get('view')) {
    $file  = $_FILES['file']['name'];
    if ($file != '') {
        $extfile  = pathinfo($file, PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["file"]["tmp_name"], 'sanpham.'.$extfile);
        echo'<h3 class="btn btn-success" style="text-align:center;"><a href="./sanpham_importexcel.php?upload=sanpham.'.$extfile.'">Import file đã upload</a></h3>';
    } else {
        echo'Vui lòng chọn file Upload <br /><a href="../../?op=sanpham">Trở về</a>';
    }
}
if (ws_get('upload')) {
    require_once '../../plugin/PHPExcel/PHPExcel.php';

    $filename = ws_get('upload');
    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    /**  Load $inputFileName to a PHPExcel Object  **/
    $objPHPExcel = $objReader->load("$filename");
    $total_sheets=$objPHPExcel->getSheetCount();
    $allSheetName=$objPHPExcel->getSheetNames();
    $objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow    = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $arraydata = array();
    for ($row = 3; $row <= $highestRow;++$row) {
        for ($col = 0; $col <$highestColumnIndex;++$col) {
            $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
            $arraydata[$row-2][$col]=$value;
        }
    }

    // xu ly du lieu và update hoặc insert vào danh sách
    if (count($arraydata)>0) {
        foreach ($arraydata as $key => $info) {
            $idsp            = $info[0];
            $masp            = $info[1];
            $gia             = $info[2];
            $giagoc          = $info[3];
            $solanxem        = $info[4];
            $thutu           = $info[5];
            $anhien          = $info[6];
            $id_noidung_lang = $info[7];
            $idlang          = $info[8];
            $tenlang         = $info[9];
            $ten             = $info[10];
            $tieude          = $info[11];
            $link            = $info[12];
            $target          = $info[13];
            $mota            = $info[14];
            $noidung         = $info[15];
            $tukhoa          = $info[16];
            $motatukhoa      = $info[17];
            $tags            = $info[18];
            $gia             = str_replace(",", "", $gia);
            $giagoc          = str_replace(",", "", $giagoc);

            // TRUONG HOP CO ID SAN PHAM
            if ($idsp !='') {
                // kiem tra xem idsanpham da co trong bang noi dung chua
                if ($id_noidung_lang!='') {
                    $s_checksp = "select count(id) as num from tbl_noidung where id = '".$idsp."' ";
                    $d_checksp = $db->rawQuery($s_checksp);
                    if (count($d_checksp)>0) {
                        // neu co ton tai roi thi update
                        $aray_update = array(
                            "masp"        => $masp,
                            "gia"         => $gia,
                            "giagoc"      => $giagoc,
                            "solanxem"    => $solanxem,
                            "soluong"     => $soluong,
                            "thutu"          => $thutu,
                            "anhien"      => $anhien
                        );
                        $db->sqlUpdate("tbl_noidung", $aray_update, "id = ".$idsp).'xx<br />';
                    }

                    // update bang noidung_lang
                    $s_checksplang = "select count(id) as num from tbl_noidung_lang where id = '".$id_noidung_lang."' ";
                    $d_checksplang = $db->rawQuery($s_checksplang);
                    if (count($d_checksplang)>0) {
                        // neu co ton tai roi thi update
                        $aray_update = array(
                            "ten"        => $ten,
                            "tieude"     => $tieude,
                            "link"       => $link,
                            "target"     => $target,
                            "mota"       => $mota,
                            "noidung"    => $noidung,
                            "tukhoa"     => $tukhoa,
                            "motatukhoa" => $motatukhoa,
                            "tag"        => $tag
                        );
                        $db->sqlUpdate("tbl_noidung_lang", $aray_update, "id = ".$id_noidung_lang);
                    }
                } else {
                    // neu chua co id noi dung thì them 1 dòng theo ngôn ngữ mới
                    // neu kiem tra thay idsp va idlang tuc la da them roi thi khongt them nua
                    $s_check_noidung =" SELECT id FROM tbl_noidung_lang WHERE idnoidung = ".$idsp." and idlang = ".$idlang;
                    $d_check_noidung = $db->rawQuery($s_check_noidung);
                    if (count($d_check_noidung)>0) {
                    } else {
                        $aray_insert = array(
                            "idnoidung"  => $idsp,
                            "idlang"     => $idlang,
                            "ten"        => $ten,
                            "tieude"     => $tieude,
                            "link"       => $link,
                            "target"     => $target,
                            "mota"       => $mota,
                            "noidung"    => $noidung,
                            "tukhoa"     => $tukhoa,
                            "motatukhoa" => $motatukhoa,
                            "tag"        => $tag
                        );
                        $db->insert("tbl_noidung_lang", $aray_insert);
                    }
                }
            } else { // else id sp ==''
                if ($id_noidung_lang=='') {
                    // if idsp == ''
                    //thêm moi san pham
                    //chi them duoc 1 ngon ngu dau tien, vi khong biet cai nao la ngon ngu thu 2 ca
                    $aray_insert = array(
                            "masp"        => $masp,
                            "gia"         => $gia,
                            "giagoc"      => $giagoc,
                            "solanxem"    => $solanxem,
                            "soluong"     => $soluong,
                            "thutu"          => 1,
                            "loai"          => 1,
                            "anhien"      => $anhien
                        );
                    $id_insert = $db->insert("tbl_noidung", $aray_insert);
                    $s_lang = "select a.id,a.idkey,b.ten
	                               from tbl_lang AS a
	                               inner join tbl_lang_lang AS b
	                               On a.id = b.idlang
	                               and a.macdinhwebsite = 1
	                               and a.id = b.iddanhmuc
	                               where a.anhien = 1
	                               order by thutu Asc";
                    $d_lang = $db->rawQuery($s_lang);
                    if (count($d_lang) > 0) {
                        foreach ($d_lang as $key_lang => $info_lang) {
                            $idlang  = $info_lang['id'];
                            break;
                        }
                    }
                    // them vao bang noi dung
                    $aray_insert = array(
                            "idnoidung"  => $id_insert,
                            "idlang"     => $idlang,
                            "ten"        => $ten,
                            "tieude"     => $tieude,
                            "link"       => $link,
                            "target"     => $target,
                            "mota"       => $mota,
                            "noidung"    => $noidung,
                            "tukhoa"     => $tukhoa,
                            "motatukhoa" => $motatukhoa,
                            "tag"        => $tag
                    );
                    $db->insert("tbl_noidung_lang", $aray_insert);
                } else {
                    // update bang noidung_lang
                    $s_checksplang = "select count(id) as num from tbl_noidung_lang where id = '".$id_noidung_lang."' ";
                    $d_checksplang = $db->rawQuery($s_checksplang);
                    if (count($d_checksplang)>0) {
                        // neu co ton tai roi thi update
                        $aray_update = array(
                            "ten"        => $ten,
                            "tieude"     => $tieude,
                            "link"       => $link,
                            "target"     => $target,
                            "mota"       => $mota,
                            "noidung"    => $noidung,
                            "tukhoa"     => $tukhoa,
                            "motatukhoa" => $motatukhoa,
                            "tag"        => $tag
                        );
                        $db->sqlUpdate("tbl_noidung_lang", $aray_update, "id = ".$id_noidung_lang);
                    }
                }
            }
        }
    }
    echo'<script>location.href="../../?op=sanpham";</script>';
}
