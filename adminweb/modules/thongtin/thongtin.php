<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Thông tin chung",
    "table"           => 'tbl_thongtin',
    "tablenoidung"    => 'tbl_thongtin_lang',
    "id_name"         => 'id',
    "id"              => 'id',
    "datatype"        => 0,
    "editor"          => 1,
    "loai"            => 0,
    "idkeyname"       => 1,
    "thutu"           => 1,
    "anhien"          => 1,
    "noidung"         => 1,
    "ten"             => 1,
    "path_img"        => "../upload/thongtin/",
    "path_file"       => "../upload/files/",
    "action"          => 1,
    "sizeimagesthumb" => 300);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} elseif ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
