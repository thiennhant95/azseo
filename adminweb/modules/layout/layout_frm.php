<?php
// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    if ($__sua == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
} else {
    // la them
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
}

$danhmuc_hiennoidung = '';
$danhmuc_dieukien    = '';
$_id_name            = $__config['id'];
$_getop              = ws_get('op');
$_idtype             = isset($__config['type']) && $__config['type'];
$_idSP               = isset($__config['idsp']) && $__config['idsp'];
$_sethomepage        = isset($__config['homepage']) && $__config['homepage'];
$_hinhsanpham        = isset($__config['images']) && $__config['images'];
$_hinhsanphamchitiet = isset($__config['hinhsanphamchitiet']) && $__config['hinhsanphamchitiet'];
$ACTION              = '';
$CONTENT             = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
if ($__getid != '') {
    $__cid                    = ws_get('id');
    $s_sanpham                = "SELECT * from {$__table} where id = '{$__getid}'";
    $data                     = $db->rawQuery($s_sanpham);
    $data                     = $data[0];
    $vitri                    = $data['vitri'];
    $loai                     = $data['loai'];
    $idtype                   = $data['idtype'];
    $danhmuc_dieukien         = $data['danhmuc_dieukien'];
    $danhmuc_hiennoidung      = $data['danhmuc_hiennoidung'];
    $hienthitieudechuyenmuc   = $data['hienthitieudechuyenmuc'];
    $laytieudechuyenmucdachon = $data['laytieudechuyenmucdachon'];
    $idtemplates              = $data['idtemplates'];
    $laytin                   = $data['laytin'];
    $soluongtin               = $data['soluongtin'];
    $hinh                     = $data['hinh'];
    $icon                     = $data['icon'];
    $anhien                   = $data['anhien'];
    $iduser                   = $data['iduser'];
    $img_view                 = $__config['path_img'] . $img;
    $ten_post                 = replace_html(ws_post('ten'.$__defaultlang) && ws_post('ten'.$__defaultlang));
    // Get Mau
    $coLor      = $data['color'];
    $backGround = $data['background'];
    $dBackground = $backGround;
    $vitrihienthi = explode(",", $data['vitrihienthi']);
} else {
    $vitri                    = 'cota';
    $loai                     = 0;
    $soluongtin               = 10;
    $laytin                   = 0;
    $idtemplates              = '';
    $hienthitieudechuyenmuc   = 1;
    $laytieudechuyenmucdachon = 1;
}
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luuvadong'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
// col left
$CONTENT .= '
    <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
if ($__config['vitri'] == 1) {
    //$array_data lay ben file danhmuc
    if (count($array_data) > 0) {
        $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['vitrilayout'] . '</label>
                <div class="checkbox">';
        foreach ($array_data as $key_configdanhmuc => $info_configdanhmuc) {
            $config_dinhdanh = $info_configdanhmuc['dinhdanh'];
            $config_ten      = $info_configdanhmuc['ten'];
            $CONTENT .= '
              <div style="float:left;">
                <label>';
            $checked_colvalue = (strpos($vitri, $config_dinhdanh) > -1) ? ' checked="checked" ' : null;
            $CONTENT .= '<input value="' . $config_dinhdanh . '" ' . $checked_colvalue . ' type="radio" name="vitri" />';
            $CONTENT .= '' . $config_ten . '
                </label>
            </div>';
        }
        $CONTENT .= '<div class="clear"></div></div>
            </div>';
    }
}
if ($__config['loai'] == 1) {
    $CONTENT .= '
         <div class="hr"></div>
         <div class="form-group">
         <label for="inputdefault">' . $arraybien['phanloaidulieu'] . '</label>
            <div class="checkbox">';
    $soluongloaidulieu = count($_arr_layout_loaidulieu);
    for ($t = 0; $t < $soluongloaidulieu; ++$t) {
        $loaidulieu_ten = $_arr_layout_loaidulieu[$t];
        $styledisplay   = ' style="display:none; " ';
        $CONTENT .= '
            <label>';
        if ($t == $loai) {
            $CONTENT .= '<input onclick="showdiv(\'loai' . $t . '\',\'loaidata\')" checked="checked"  value="' . $t . '" type="radio" name="loai" id="loai" />';
            $styledisplay = ' style="display:block;" ';
        } else {
            $CONTENT .= '<input onclick="showdiv(\'loai' . $t . '\',\'loaidata\')"  value="' . $t . '" type="radio" name="loai" id="loai" />';
        }
        $CONTENT .= '
            ' . $loaidulieu_ten . '</label>
            ';
    }
    $CONTENT .= '<div class="clear"></div><br />
         ';
    for ($t = 0; $t < $soluongloaidulieu; ++$t) {
        $loaidulieu_ten = $_arr_layout_loaidulieu[$t];
        $styledisplay   = ' style="display:none; " ';
        if ($t == $loai) {
            $styledisplay = ' style="display:block;" ';
        }
        if ($t == 0) {
            // Danh mục Menu
            $CONTENT .= '
               <div id="loai' . $t . '" class="loaidata" ' . $styledisplay . '>';
            $CONTENT .= '<label for="inputdefault"><b>' . $arraybien['danhmuc'] . '</b></label>';
            // lay danh mục menu lên
            $sql2 = "SELECT a.id, b.ten
                        from tbl_danhmuc As a
                        inner join tbl_danhmuc_lang As b On a.id =  b.iddanhmuc
                        where b.idlang = '{$__defaultlang}'
                        and a.idtype != ''
                        order by a.thutu ";
            $compare_id = substr($id, 0, strlen($id) - 4);
            $CONTENT .= '<select class="form-control" name="idtype' . $t . '" id="idtype' . $t . '" onchange="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=xemtruoc&value=\'+this.value+\'&laytin=\'+document.getElementById(\'laytin\').checked,\'loadnoidungpreview\')">';
            //$CONTENT .= '<option value="">'.$arraybien['danhmuc'].'</option>';
            $CONTENT .= $db->Create_Combobox_Dequy_Sql_no_select('idtype' . $t, $option1, 'form-control danhmuc', 4, $sql2, 'ten', 'id', $idtype);
            $CONTENT .= '</select>';
            $CONTENT .= '<div id="loadnoidungpreview"></div>';
            // lay tieu de tin hay lay danh mục con
            $CONTENT .= '<br />
                  ' . $arraybien['huongdanlaydulieu'] . '
                  <label><b>';
            if ($laytin == 1) {
                $CONTENT .= '<input onclick="checkshow(\'laytin\',\'dieukien_baiviet\',\'dieukien_danhmuc\'); Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=xemtruoc&value=\'+document.getElementById(\'idtype' . $t . '\').value+\'&laytin=\'+document.getElementById(\'laytin\').checked,\'loadnoidungpreview\');" checked="checked"  value="1" type="checkbox" name="laytin" id="laytin" />';
            } else {
                $CONTENT .= '<input onclick="checkshow(\'laytin\',\'dieukien_baiviet\',\'dieukien_danhmuc\'); Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=xemtruoc&value=\'+document.getElementById(\'idtype' . $t . '\').value+\'&laytin=\'+document.getElementById(\'laytin\').checked,\'loadnoidungpreview\');"  value="1" type="checkbox" name="laytin" id="laytin" />';
            }
            $CONTENT .= $arraybien['chonloaidulieu'] . '</b></label>';
            // so luong tin
            $style_danhmuc  = '';
            $style_baiviet  = '';
            $laytin_checked = '';
            if ($laytin == 1) {
                $laytin_checked = ' checked="checked" ';
                $style_danhmuc  = ' style="display:none;" ';
                $style_baiviet  = ' style="display:block;" ';
            } else {
                $style_danhmuc = ' style="display:block;" ';
                $style_baiviet = ' style="display:none;" ';
            }
            // lay mang dieu kien check danh muc
            $CONTENT .= ' <br /><br />
                     <div id="dieukien_danhmuc" ' . $style_danhmuc . '>';
            $CONTENT .= '<b><label>';
            if ($danhmuc_hiennoidung == 1) {
                $CONTENT .= '<input checked="checked"  value="1" type="checkbox" name="danhmuc_hiennoidung" id="danhmuc_hiennoidung" />';
            } else {
                $CONTENT .= '<input  value="1" type="checkbox" name="danhmuc_hiennoidung" id="danhmuc_hiennoidung" />';
            }
            $CONTENT .= 'Check để hiện thị nội dung của DANH MỤC đang chọn</label></b><br /><br />';
            $CONTENT .= '
                        <p><b>Chọn điều kiện để DANH MỤC được hiển thị</b></p>
                        <div  class="input-group">
                           <span class="input-group-addon" >' . $arraybien['dieukienhienthi'] . '</span>';
            $CONTENT .= '<select class="form-control"  name="danhmuc_dieukien">';
            $array_data_danhmuc = get_data('configdanhmuc', '*', 'anhien,=,1');
            // hien thi tat ca
            $CONTENT .= '<option  value="">Hiển thị tất cả</option>';
            foreach ($array_data_danhmuc as $key_dieukien => $info_dieukien) {
                $dieukien_ten      = $info_dieukien['ten'];
                $dieukien_dinhdanh = $info_dieukien['dinhdanh'];
                if ($dieukien_dinhdanh == $danhmuc_dieukien) {
                    $CONTENT .= '<option selected="selected" value="' . $dieukien_dinhdanh . '">' . $dieukien_ten . '</option>';
                } else {
                    $CONTENT .= '<option value="' . $dieukien_dinhdanh . '">' . $dieukien_ten . '</option>';
                }
            }
            $CONTENT .= '</select>';
            $CONTENT .= '</div>';
            $CONTENT .= '</div>';
            // lay mang dieu kien check tin
            $CONTENT .= '
                        <div id="dieukien_baiviet" ' . $style_baiviet . '>
                           <p><b>Chọn điều kiện để NỘI DUNG được hiển thị</b></p>
                           <div  class="input-group">
                           <span class="input-group-addon" >' . $arraybien['dieukienhienthi'] . '</span>';
            $CONTENT .= '<select class="form-control"  name="danhmuc_dieukien_baiviet">';
            foreach ($_arr_layout_dieukien_tin as $key_dieukien => $info_dieukien) {
                $dieukien_val = $_arr_layout_dieukien_tin[$key_dieukien];
                if ($key_dieukien == $danhmuc_dieukien) {
                    $CONTENT .= '<option selected="selected" value="' . $key_dieukien . '">' . $dieukien_val . '</option>';
                } else {
                    $CONTENT .= '<option value="' . $key_dieukien . '">' . $dieukien_val . '</option>';
                }
            }
            $CONTENT .= '</select>';
            $CONTENT .= '</div>';
            // thong tin templates
            $s_templates = "select a.id,hinh,b.ten
                              from tbl_layout_template AS a
                              inner join tbl_layout_template_lang AS b
                              On a.id = b.idtype
                              where anhien = 1
                              and b.idlang = '{$__defaultlang}'
                              order by thutu Asc";
            $d_templates = $db->rawQuery($s_templates);
            if (count($d_templates) > 0) {
                $CONTENT .= '<ul>';
                foreach ($d_templates as $key_templates => $info_templates) {
                    $layout_ten  = $info_templates['ten'];
                    $layout_id   = $info_templates['id'];
                    $layout_hinh = $info_templates['hinh'];
                    if ($idtemplates == '') {
                        $idtemplates = $d_templates[0]['id'];
                    }
                    if ($layout_id == $idtemplates) {
                        $CONTENT .= '<li><label><input checked="checked" type="radio" name="idtemplates" id="idtemplates" value="' . $layout_id . '" />' . $layout_ten . '</label><a  onclick="window.open(this.href, \'\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50\'); return false;"  href="../uploads/layout/' . $layout_hinh . '">Xem mẫu</a></li>';
                    } else {
                        $CONTENT .= '<li><label><input type="radio" name="idtemplates" id="idtemplates" value="' . $layout_id . '" />' . $layout_ten . '</label><a  onclick="window.open(this.href, \'\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50\'); return false;"  href="../uploads/layout/' . $layout_hinh . '">Xem mẫu</a></li>';
                    }
                }
                $CONTENT .= '</ul>';
            }
            $CONTENT .= '
                        </div><br />'; // end div dieukien_baiviet
            $CONTENT .= '
                  <div class="input-group">
                  <span class="input-group-addon" >' . $arraybien['soluongtin'] . '</span>
                  <input class="form-control" ' . $laytin_checked . '  name="soluongtin" type="text" id="soluongtin" value="' . $soluongtin . '"  />
                  </div>';
            $CONTENT .= '
               </div>'; // end div 1
        } elseif ($t == 1) {
            $CONTENT .= '
               <div id="loai' . $t . '" class="loaidata" ' . $styledisplay . '>';
            $CONTENT .= '<label for="inputdefault"><b>' . $arraybien['nhomquangcao'] . '</b></label>';
            // lay danh mục menu lên
            $sql2 = "select a.id, b.ten
                        from tbl_banner_type As a
                        inner join tbl_banner_type_lang As b On a.id =  b.idtype
                        where b.idlang = '{$__defaultlang}'
                        and a.anhien = 1
                        order by a.thutu ";
            $compare_id = substr($id, 0, strlen($id) - 4);
            $option1    = '<option value="">' . $arraybien['nhomquangcao'] . '</option>';
            $CONTENT .= $db->createComboboxDequySql('idtype' . $t, $option1, 'form-control danhmuc', '', $sql2, 'ten', 'id', $idtype);
            $CONTENT .= '
            </div>'; // end div 2
        } elseif ($t == 2) {
            $CONTENT .= '
               <div id="loai' . $t . '" class="loaidata" ' . $styledisplay . '>';
            $CONTENT .= '<label for="inputdefault"><b>' . $arraybien['chonmoduletienich'] . '</b></label>';
            // lay danh mục menu lên
            $sql2 = "select a.id, b.ten
                        from tbl_tienich As a
                        inner join tbl_tienich_lang As b On a.id =  b.idtype
                        where b.idlang = '{$__defaultlang}'
                        and a.anhien = 1
                        order by a.thutu ";
            $compare_id = substr($id, 0, strlen($id) - 4);
            $option1    = '<option value="">' . $arraybien['chonmoduletienich'] . '</option>';
            $CONTENT .= $db->createComboboxDequySql('idtype' . $t, $option1, 'form-control danhmuc', '', $sql2, 'ten', 'id', $idtype);
            $CONTENT .= '
               </div>'; // end div 3
        }
    }
    $CONTENT .= '</div>
            </div>';
}
// VI TRI HIEN THI
if ($__config['vitrihienthi'] == 1) {
    $CONTENT.='
    <div class="form-group"></div>
    <div class="form-group clearfix alert alert-success">
        <div class="clearfix">
        <label>Chọn vị trí cần hiện thị</label>
        <select name="vitrihienthi[]" class="selectpicker form-control" data-live-search="true" data-actions-box="true" title="Chọn vị trí" multiple>';

            if (count($dlocation) > 0) {
                foreach ($dlocation as $klocation => $vlocation) {
                    $id  = $vlocation['id'];
                    $ten = $vlocation['ten'];
                    $gach = '';
                    for ($i=8; $i <= strlen($id)  ; $i += 4) {
                        $gach .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    }

                        $CONTENT.= '<option value="'.$id.'" '.(is_array(@$vitrihienthi) && in_array($id, @$vitrihienthi)?'selected':null).'>'.$gach.$ten.'</option>';
                }
            }$CONTENT.='
        </select>
    </div>
    </div>';
}
if ($__config['anhien'] == 1) {
    $CONTENT .= '
         <div class="hr"></div>
         <div class="checkbox">
            <label>';
    if ($anhien == '') {
        $anhien = 1;
    }
    if ($anhien == 1) {
        $CONTENT .= '<input checked="checked"  value="1" type="checkbox" name="anhien" id="anhien" />';
    } else {
        $CONTENT .= '<input  value="1" type="checkbox" name="anhien" id="anhien" />';
    }
    $CONTENT .= '
            ' . $arraybien['hienthi'] . '</label>
         </div>';
}
if ($__config['hienthitieudechuyenmuc'] == 1) {
    $CONTENT .= '
         <div class="hr"></div>
         <div class="form-group">
            <label for="inputdefault">[Ẩn/Hiện Tiêu đề]</label>
            <div class="checkbox">
            <label>';
    if ($hienthitieudechuyenmuc == 1) {
        $CONTENT .= '<input checked="checked"  value="1" type="checkbox" name="hienthitieudechuyenmuc" id="hienthitieudechuyenmuc" />';
    } else {
        $CONTENT .= '<input  value="1" type="checkbox" name="hienthitieudechuyenmuc" id="hienthitieudechuyenmuc" />';
    }
    $CONTENT .= '
            Hiển thị tiêu đề</label>
            </div>
         </div>';
}
if ($__config['laytieudechuyenmucdachon'] == 1) {
    $CONTENT .= '
         <div class="hr"></div>
         <div class="form-group">
            <label for="inputdefault">[Chọn nguồn hiển thị cho tiêu đề]</label>
            <div class="checkbox">
            <label>';
    if ($laytieudechuyenmucdachon == 1) {
        $CONTENT .= '<input checked="checked"  value="1" type="checkbox" name="laytieudechuyenmucdachon" id="laytieudechuyenmucdachon" />';
    } else {
        $CONTENT .= '<input  value="1" type="checkbox" name="laytieudechuyenmucdachon" id="laytieudechuyenmucdachon" />';
    }
    $CONTENT .= '
            Lấy tiêu đề chuyên mục được chọn</label>
            </div>
         </div>';
}
if ($__config['hinh'] == 1) {
    $CONTENT .= '
         <div class="form-group">
         <label for="inputdefault">Hình</label>
         <input type="file" name="hinh" id="hinh" />';
    if ($img != '') {
        $CONTENT .= '
          <img src="' . $img_view . '" border="0" width="30" />
          <label><input type="checkbox" name="xoaimg" id="xoaimg" value="1" />' . $arraybien['xoahinh'] . '</label>
          <input type="hidden" name="imgname" id="imgname" value="' . $img . '" />';
    }
    $CONTENT .= '
         </div>';
}
// MAU SAC
// --------------------------------------------------------------------------
$CONTENT.= include("application/templates/inc/color.inc.php");

/*
 * ICON
 */
# ICON
// ===========================================================================
$CONTENT.= include 'application/templates/inc/icon.inc.php';
/*
 * END ICON
 */
$CONTENT .= '
         </div>
      </div>
</div>';
// col right
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading">&nbsp;</div>
  <div class="panel-body">
    <div id="tabs">
     <ul>';
$s_lang = "select a.id,a.idkey,b.ten
           from tbl_lang AS a
           inner join tbl_lang_lang AS b
           where a.id = b.iddanhmuc
           and b.idlang = '{$__defaultlang}'
           and a.anhien = 1
           order by thutu Asc";
$d_lang = $db->rawQuery($s_lang);
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $ten    = $info_lang['ten'];
        $idlang = $info_lang['id'];
        $idkey  = $info_lang['idkey'];
        $CONTENT .= '<li type="#tab' . ($key_lang + 1) . '">' . $ten . '</li>';
    }
}
$CONTENT .= '
        <div class="clear"></div>
    </ul>
    <div style="clear:both"></div>
    <div class="tab-text">';
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $tenlang = $info_lang['ten'];
        $idlang  = $info_lang['id'];
        $idkey   = $info_lang['idkey'];
        // lay du lieu theo ngon ngu
        $s_noidung_data = "SELECT * FROM {$__tablenoidung} where idlang = {$idlang} and idtype = '" . $__cid . "' ";
        $d_noidung_data = $db->rawQuery($s_noidung_data);
        @$d_noidung_data = $d_noidung_data[0];
        $ten            = $d_noidung_data['ten'];
        $CONTENT .= '
         <div id="tab' . ($key_lang + 1) . '">
         <div class="adminform">';
        if ($__config['ten'] == 1) {
            $CONTENT .= '
         <div  class="form-group">
            <label for="inputdefault">' . $arraybien['ten'] . ' ' . $tenlang . '</label>';
            if ($__getid != '') {
                $CONTENT .= '
               <input class="form-control"   name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />';
            } else {
                $CONTENT .= '
               <input data-val-required="Tên sản phẩm không được để trống" class="form-control" name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />';
            }
            $CONTENT .= '
         </div>';
        }
        $CONTENT .= '
            </div>
        </div>';
    }
}
$CONTENT .= '
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   </div>'; // end tab-text
$CONTENT .= '
</div></div></div></div>'; // end col data
$CONTENT .= '
<div class="clear"></div>
</form>';
$file_tempaltes = 'application/files/templates.tpl';
$array_bien     = array('{CONTENT}' => $CONTENT,
    '{ACTION}'                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
