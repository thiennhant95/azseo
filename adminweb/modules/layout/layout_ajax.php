<?php
$data = '';
$thisTable = "tbl_layout";
if ($name == 'anhien') {
    if ($value == 1) {
        $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
        $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
        $data .= '</a></span>';
        $dataupdate = 0;
    } else {
        $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
        $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
        $data .= '</a></span>';
        $dataupdate = 1;
    }
    $aray_update = array("anhien" => $dataupdate);
    $result      = $db->sqlUpdate('tbl_layout', $aray_update, "id = $id");
} else if ($name == 'laytin') {
    if ($value == 1) {
        $data .= '<a class="cursor-pointer" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=laytin&value=0\',\'laytin' . $id . '\')">';
        $data .= 'Lấy bài viết <i class="fa fa-times-circle color-red"></i>';
        $data .= '</a>';
        $dataupdate = 0;
    } else {
        $data .= '<a class="cursor-pointer" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=laytin&value=1\',\'laytin' . $id . '\')">';
        $data .= 'Lấy bài viết <i class="fa fa-check-circle"></i>';
        $data .= '</a>';
        $dataupdate = 1;
    }
    $aray_update = array("home" => $dataupdate);
    $result      = $db->sqlUpdate('tbl_layout', $aray_update, "id = $id ");
} else if ($name == 'xemtruoc') {
    $laytin = ws_get('laytin');
    if ($laytin == 'true') {
        $laytin = 1;
    } else {
        $laytin = 0;
    }
    if ($laytin == '1') {
        $s = "SELECT a.*, b.ten as ten, b.url, b.link
            from tbl_noidung As a
            Inner Join tbl_noidung_lang As b On a.id  = b.idnoidung
            where b.idlang = '" . $_SESSION['__defaultlang'] . "'
            and a.idtype like '" . $value . "%'
            limit 0,10";
        $d = $db->rawQuery($s);
        $data .= '<h4>Dữ liệu mẫu - Lấy từ nội dung</h4>
               <h3></h3>';
        if (count($d) > 0) {
            $data .= '<ul class="ul_preview">';
            foreach ($d as $key => $info) {
                $data .= '<li>' . $info['ten'] . '</li>';
            }
            $data .= '</ul>';
        } else {
            $data .= 'Chưa có dữ liệu mẫu';
        }
    } else if ($laytin == '0') {
        $s = "select a.*,b.ten
          from tbl_danhmuc AS a
          inner join tbl_danhmuc_lang AS b On a.id = b.iddanhmuc
          where idlang = '" . $_SESSION['__defaultlang'] . "' ";
        $s = $s . " and a.id like'" . $value . "%'
         order by thutu Asc";
        $d = $db->rawQuery($s);
        $data .= '<h4>Dữ liệu mẫu - Lấy từ Danh Mục</h4>
               <h3></h3>';
        if (count($d) > 0) {
            $data .= '<ul class="ul_preview">';
            foreach ($d as $key => $info) {
                $data .= '<li>' . $info['ten'] . '</li>';
            }
            $data .= '</ul>';
        } else {
            $data .= 'Chưa có dữ liệu mẫu';
        }
    }
} else if ($name == 'thutu') {
    $aray_update = array("thutu" => $value);
    $result      = $db->sqlUpdate('tbl_layout', $aray_update, "id = '$id' ");
} else if ($name == 'vitri') {
    $aray_update = array("vitri" => $value);
    $result      = $db->sqlUpdate('tbl_layout', $aray_update, "id = '$id' ");
}
echo $data;


if ($name == 'vitrihienthi') {
    $ketqua = $db->sqlUpdate($thisTable, array("vitrihienthi" => $value), "id = '{$id}'");
    if (empty($ketqua)) {
        $err = 'not_valid';
    } else {
        $err = 'success';
    }

    $data = array(
        "error" => $err
    );
    echo json_encode($data);
}