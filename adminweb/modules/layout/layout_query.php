<?php
use Intervention\Image\ImageManagerStatic as Image;

$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
// ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
// HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
// LƯU THỨ TỰ MỤC TIN
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
    //$db->thuTu($__table,$__id,"thutu","(loai","0 or loai = 1 or loai = 2)");
} else if ($__post_task == 'remove') {
// XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('hinh')[$id_order];
        $value_file = ws_post('file')[$id_order];
        $value_icon = ws_post('icon')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            if ($value_file != '') {
                unlink($__config['path_file'] . $value_file);
            }
            if ($value_icon != '' && file_exists("../uploads/layout/" . $value_icon)) {
                @unlink("../uploads/layout/" . $value_icon);
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idtype  = '{$id_order}' ");
        }
    }
}
if ($__getaction == 'save') {
    $vitri                    = replace_html(ws_post('vitri'));
    $loai                     = replace_html(ws_post('loai'));
    $idtype                   = replace_html(ws_post('idtype0'));
    $danhmuc_dieukien         = replace_html(ws_post('danhmuc_dieukien'));
    $danhmuc_hiennoidung      = replace_html(ws_post('danhmuc_hiennoidung'));
    $hienthitieudechuyenmuc   = replace_html(ws_post('hienthitieudechuyenmuc'));
    $laytieudechuyenmucdachon = replace_html(ws_post('laytieudechuyenmucdachon'));
    $idtemplates              = replace_html(ws_post('idtemplates'));
    $laytin                   = replace_html(ws_post('laytin'));
    $soluongtin               = replace_html(ws_post('soluongtin'));
    $hinh                     = $_FILES["hinh"]["name"];
    $thutu                    = 1;
    $anhien                   = replace_html(ws_post('anhien'));
    $img_url                  = $__config['path_img'] . $img;
    $xoaimg                   = replace_html(ws_post('xoaimg'));
    $imgname                  = replace_html(ws_post('imgname'));
    $color                    = trim(ws_post('color_danhmuc'));
    $chonmausac               = ws_post('chonmausac');
    $vitrihienthi             = implode(",", ws_post('vitrihienthi'));

    // xu ly du lieu
    if ($loai == 0) {
        $idtype     = ws_post('idtype0');
        $laytin     = ws_post('laytin');
        $soluongtin = ws_post('soluongtin');
        if ($laytin == 1) {
        // kiem tra neu lay noi dung bai viet
            $danhmuc_dieukien = ws_post('danhmuc_dieukien_baiviet');
        } else {
            $danhmuc_dieukien = ws_post('danhmuc_dieukien');
        }
    } else if ($loai == 1) {
        $idtype = ws_post('idtype1');
    } else if ($loai == 2) {
        $idtype = ws_post('idtype2');
    }
    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $images_name = tao_url(ws_post('ten'.$__defaultlang));

    // THÊM MỚI DỮ LIỆU
    if ( !$__getid ) {
        // update thutu tang len 1 don vi
        $s_update_thutu = "update " . $__table . " set thutu = thutu+1 where 1=1 ";
        $d_update_thutu = $db->rawQuery($s_update_thutu);
        // upload hinh
        if ($hinh != '') {
            $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists($__config['path_img'] . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["hinh"]["tmp_name"], $__config['path_img'] . $imgfile);
        }

        #
        # INSERT ICON
        # -------------------------------------------
        // B1. Neu la chon san
        $kieuIcon = ws_post('type_icon');
        if ($kieuIcon == 1 && $kieuIcon != -1) {
            $iconName = ws_post('icon_faicon');
        } else if ($kieuIcon == 0 && $kieuIcon != -1) {
            $iconName = $_FILES['icon_img']['name'];
            if (!empty($iconName)) {
                $tenicon =  $images_name ."-". $iconName;
                move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img'].$tenicon);
                $imgs = Image::make($__config['path_img'] . $tenicon);
                $imgs->resize($__config['icon_width'], null, function($constraint) {
                    $constraint->aspectRatio();
                });
                $imgs->save($__config["path_img"] . $tenicon, 100);
                $iconName = $tenicon;
            }
        } else {
            $iconName = '';
        }

        $aray_insert = array(
            "vitri"                    => $vitri,
            "loai"                     => $loai,
            "idtype"                   => $idtype,
            "danhmuc_dieukien"         => $danhmuc_dieukien,
            "danhmuc_hiennoidung"      => $danhmuc_hiennoidung,
            "hienthitieudechuyenmuc"   => $hienthitieudechuyenmuc,
            "laytieudechuyenmucdachon" => $laytieudechuyenmucdachon,
            "laytin"                   => $laytin,
            "idtemplates"              => $idtemplates,
            "soluongtin"               => $soluongtin,
            "hinh"                     => $hinh,
            "icon"                     => $iconName,
            "thutu"                    => $thutu,
            "anhien"                   => $anhien,
            "vitrihienthi"             => $vitrihienthi,
        );
        $id_insert = $db->insert($__table, $aray_insert);

        if( $id_insert ) {
            #
            # INSERT_COLOR
            # ---------------------------------
            if (ws_post('chonmausac') && !empty(ws_post('chonmausac'))) {
                $inserBG = ws_post('bg_danhmuc');
                $inserCL = ws_post('color_danhmuc');
                $db->where("id", $id_insert);
                $db->update($__table, array(
                    'background' => $inserBG,
                    'color'      => $inserCL
                ));
            }

            // them du lieu vao bang noi dung danh muc
            $s_lang = "SELECT * FROM tbl_lang WHERE anhien = 1 order by thutu Asc";
            $d_lang = $db->rawQuery($s_lang);
            if (count($d_lang) > 0) {
                foreach ($d_lang as $key_lang => $info_lang) {
                    $tenlang = $info_lang['ten'];
                    $idlang  = $info_lang['id'];
                    $idkey   = $info_lang['idkey'];
                    // get noi dung post qua
                    $ten = replace_html(ws_post('ten'.$idlang));
                    // kiem tra url neu da co roi thì them ki tu cuoi url
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }

                    // luu du lieu vao bang danh muc lang
                    $db->insert($__tablenoidung, [
                        "idtype" => $id_insert,
                        "idlang" => $idlang,
                        "ten"    => $ten,
                    ]);
                }
            }
        }
        else {
            vard(
                "Lỗi chèn lang: ".
                $db->getLastError()
            );
        }
    }
    // CẬP NHẬT DỮ LIỆU
    else {
        $aray_insert = array(
            "vitri"                    => $vitri,
            "loai"                     => $loai,
            "idtype"                   => $idtype,
            "danhmuc_dieukien"         => $danhmuc_dieukien,
            "danhmuc_hiennoidung"      => $danhmuc_hiennoidung,
            "hienthitieudechuyenmuc"   => $hienthitieudechuyenmuc,
            "laytieudechuyenmucdachon" => $laytieudechuyenmucdachon,
            "laytin"                   => $laytin,
            "idtemplates"              => $idtemplates,
            "soluongtin"               => $soluongtin,
            "hinh"                     => $hinh,
            "anhien"                   => $anhien,
            "vitrihienthi"             => $vitrihienthi,
        );
        $db->where("id", $__postid);
        $db->update($__table, $aray_insert);

        #
        # UPDATE ICON
        # ---------------------------------------
        $kieuIcon = ws_post('type_icon');
        $iconOld  = ws_post('iconname');

        switch( $kieuIcon ) {
            // Upload hình icon
            case 0:
                $iconName = $_FILES['icon_img']['name'];

                if ( $iconName ) {
                    // 1. update icon new
                    // xoa icon cũ
                    if (file_exists($__config['path_img'].$iconOld)) {
                        unlink($__config['path_img'].$iconOld);
                    }
                    $tenicon =  $images_name ."-". $iconName;
                    move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img']. $tenicon);
                    $imgs = Image::make($__config['path_img'] . $tenicon);
                    $imgs->resize($__config['icon_width'], null, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    $imgs->save($__config["path_img"] . $tenicon, 100);
                    $iconName = $tenicon;
                    $db->where("id", $__postid);
                    $db->update($__table, array("icon" => $iconName));
                }
                break;

            // Chọn sẵn icon
            case 1:
                // xoa icon cũ
                if (file_exists($__config['path_img'].$iconOld)) {
                    unlink($__config['path_img'].$iconOld);
                }
                // Trường hợp update là fa-icon
                $iconName = ws_post('icon_faicon');
                $db->where("id", $__postid);
                $db->update($__table, array("icon" => $iconName));
                break;

            // Không chọn
            default:
                $iconName = '';
                // 4. doi tu img -> khong (xoa icon)
                // xoa icon cũ
                if (file_exists($__config['path_img'].$iconOld)) {
                    @unlink($__config['path_img'].$iconOld);
                }
                $db->where("id", $__postid);
                $db->update($__table, ["icon" => null]);
                break;
        }

        // 2. check xoa icon
        if ( ws_post('xoaicon') == 1 ) {
            if (file_exists($__config['path_img'].$iconOld)) {
                unlink($__config['path_img'].$iconOld);
            }
            $db->where("id", $__postid);
            $db->update($__table, ["icon" => null]);
        }

        #
        # UPDATE_COLOR
        # -----------------------------
        if (ws_post('chonmausac')) {
            if (!empty(ws_post('chonmausac'))) {
                $arr_bg_update = array(
                    'background' => ws_post('bg_danhmuc'),
                    'color'      => ws_post('color_danhmuc')
                );
            } else {
                $arr_bg_update = array(
                    'background' => null,
                    'color'      => null,
                );
            }

            $db->where("id", $__postid);
            $db->update($__table, $arr_bg_update);
        }

        if( ws_post('chonmausac') == 0 ) {
            $db->where("id", $__postid);
            $db->update($__table, [
                'background' => null,
                'color'      => null,
            ]);
        }

        // them du lieu vao bang noi dung danh muc
        $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
// lap theo so luong ngon ngu
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten = ws_post('ten'.$idlang);
                // kiem tra url neu da co roi thì them ki tu cuoi url
                if (ws_post('hi_url'.$idlang) != $url) {
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                }
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "select id from $__tablenoidung where idtype = '{$__postid}' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) {
// da tồn tại nội dung rồi thì update lại nội dung
                    // cap nhat lai du lieu
                    $aray_insert_lang = array("ten" => $ten);
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "idtype = '{$__postid}' and idlang = '{$idlang}'");
                } else {
                    $aray_insert_lang = array(
                        "idtype" => $__getid,
                        "idlang" => $idlang,
                        "ten"    => $ten,
                    );
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang

        // xoa img
        if ($xoaimg == 1) {
            unlink($__config['path_img'] . $imgname);
            $aray_insert = array("hinh" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat img
        if ($hinh != '') {
            if ($imgname != '') {
                unlink($__config['path_img'] . $imgname);
            }
            //up file moi len
            $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists($__config['path_img'] . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["hinh"]["tmp_name"], $__config['path_img'] . $imgfile);
            $aray_insert = array("hinh" => $imgfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }

    } // end if cap nhat
} // End Luu Du lieu
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get("op") . '&method=frm&id=' . $id;
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
