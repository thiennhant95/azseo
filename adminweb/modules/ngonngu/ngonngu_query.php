<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') // ẨN MỤC TIN
{
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') // HIỂN THỊ MỤC TIN
{
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') // LƯU THỨ TỰ MỤC TIN
{
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') // XOA DU LIEU
{
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        //$supdate = "update $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '".$__config['Loai']."' ";

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            if ($value_file != '') {
                $__path = $__config['path_file'] . $value_file;
                unlink("$__path");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddanhmuc  = '" . $id_order . "' ");
        }
    }
}
if ($__getaction == 'save') // LƯU TRỮ DỮ LIỆU
{
    $idtype   = ws_post('idtype');
    $loai     = ws_post('loai');
    $hinh     = $_FILES["hinh"]["name"];
    $thutu    = ws_post('thutu');
    $anhien   = ws_post('anhien');
    $id       = ws_post('id');
    $op       = ws_post('inputop');
    $act      = ws_post('act');
    $subid    = ws_post('subid');
    $anhien   = ws_post('anhien');
    $idkey    = ws_post('idkey');
    $macdinh  = ws_post('macdinh');
    $imgname  = ws_post('imgname');
    $xoaimg   = ws_post('xoaimg');
    $_getidSP = "";
    if ($__getid != '') // neu la cap nhat
    {
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->createSubID($__table, $__id, ws_post('parenid'));
    }
    // Create url lien ket
    $urllink = '';
    $_cid    = ws_get('id');
    if (ws_post('action') == 'add') {
        $thutu = (int) substr($subid, -3);
    } else {
        $thutu = (int) substr($id, -3);
    }
    $images_name = tao_url(ws_post('ten'.$__defaultlang));
    // neu them moi danh mục
    if ($__getid == '') {
        // upload hinh
        if ($hinh != '') {
            $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/lang/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["hinh"]["tmp_name"], "../uploads/lang/$imgfile");
        }
        $aray_insert = array("idkey" => $idkey,
            "macdinh"                    => $macdinh,
            "anhien"                     => $anhien,
            "hinh"                       => $imgfile,
        );
        $id_insert = $db->insert($__table, $aray_insert);
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten = ws_post('ten'.$idlang);
                // luu du lieu vao bang danh muc lang
                $aray_insert_lang = array("iddanhmuc" => $id_insert,
                    "idlang"                              => $idlang,
                    "ten"                                 => $ten);
                $db->insert($__tablenoidung, $aray_insert_lang);
            }
        }
    } // end box id != rong
    else {
        $aray_insert = array("idkey" => $idkey,
            "macdinh"                    => $macdinh,
            "anhien"                     => $anhien,
        );
        $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
        // them du lieu vao bang noi dung danh muc
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) // lap theo so luong ngon ngu
            {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                // get noi dung post qua
                $ten = ws_post('ten'.$idlang);
                // kiem tra url neu da co roi thì them ki tu cuoi url
                if (ws_post('hi_url'.$idlang) != $url) {
                    $s_checkurl = "select url from $__tablenoidung where url = '$url'";
                    $d_checkurl = $db->rawQuery($s_checkurl);
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                }
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $s_check_tontai = "select id from $__tablenoidung where iddanhmuc = '" . $__postid . "' and idlang = '{$idlang}' ";
                $d_check_tontai = $db->rawQuery($s_check_tontai);
                if (count($d_check_tontai) > 0) // da tồn tại nội dung rồi thì update lại nội dung
                {
                    // cap nhat lai du lieu
                    $aray_insert_lang = array("ten" => $ten);
                    $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = '" . $__postid . "' and idlang = '{$idlang}'");
                } else {
                    $aray_insert_lang = array("iddanhmuc" => $__postid,
                        "idlang"                              => $idlang,
                        "ten"                                 => $ten);
                    $db->insert($__tablenoidung, $aray_insert_lang);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang
        // xoa img
        if ($xoaimg == 1) {
            unlink("../uploads/lang/" . $imgname);
            $aray_insert = array("hinh" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat img
        if ($hinh != '') {
            if ($imgname != '') {
                unlink("../uploads/lang/" . $imgname);
            }
            //up file moi len
            $extfile = pathinfo($hinh, PATHINFO_EXTENSION);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/lang/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["hinh"]["tmp_name"], "../uploads/lang/$imgfile");
            $aray_insert = array("hinh" => $imgfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
    } // end if cap nhat
}
echo '<script> location.href="./?op=' . $__getop . '"; </script>';
