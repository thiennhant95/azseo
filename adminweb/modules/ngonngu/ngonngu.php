<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array("module_title" => "Ngôn ngữ",
    "table"                             => 'tbl_lang',
    "tablenoidung"                      => 'tbl_lang_lang',
    "id"                                => 'id',
    "ten"                               => 1,
    "thutu"                             => 1,
    "idkey"                             => 1,
    "macdinh"                           => 1,
    "macdinhwebsite"                    => 1,
    "img"                               => 1,
    "hinh"                              => 1,
    "anhien"                            => 1,
    "path_img"                          => "../upload/lang/",
    "action"                            => 1,
    "sizeimagesthumb"                   => 300);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
