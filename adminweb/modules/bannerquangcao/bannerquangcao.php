<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Banner quảng cáo",
    "table"           => 'tbl_banner',
    "tablenoidung"    => 'tbl_banner_lang',
    "id"              => 'id',
    "idtype"          => 1,
    "iddanhmuc"       => 1,
    "thutu"           => 1,
    "loai"            => array_search("bannerqc", $_arr_loai_banner),
    "anhien"          => 1,
    "ngaytao"         => 1,
    "ngaycapnhat"     => 1,
    "ten"             => 1,
    "hinh"            => 1,
    "linkhinh"        => 1,
    "link"            => 1,
    "manhung"         => 1,
    "target"          => 1,
    "rong"            => 1,
    "cao"             => 1,
    "action"          => 1,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/logo/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300,
    "compression"    => 80);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
