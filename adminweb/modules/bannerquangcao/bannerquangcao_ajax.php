<?php

$thisTable         = $_SESSION['__config']['table'];
switch ($name) {
    case 'updatedanhmuc':
        $idxoa      = ws_get('idxoa');
        $idtype     = ws_get('idtype');
        $str_idtype = '';
        $strdanhmuc = '<div class="danhmuc_dachon" id="danhmucload' . $id . '">';
        // truy van lay danh muc con lại chưa thêm vào
        $keyname   = 'noidung';
        $idkeytype = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'{$keyname}'");
        $s_loaitru = "SELECT a.id, b.ten
                      from tbl_danhmuc AS a
                      inner join tbl_danhmuc_lang AS b On a.id =  b.iddanhmuc
                      where b.idlang = '" . $_SESSION['__defaultlang'] . "' ";
        // neu them moi loai tin
        if (ws_get('act') == 'them') {
            if ($idtype != '') {
                if (ws_get('idthem') != '') {
                    $idtype .= ',' . ws_get('idthem');
                }
            } else {
                $idtype = ws_get('idthem');
            }
        }
        if ($idtype != '') {
            $strdanhmuc .= '<ul class="ul_danhmucdachon">';
            $arr_idtype   = explode(",", $idtype);
            $count_idtype = count($arr_idtype);
            if ($count_idtype > 0) {
                $dem = 0;
                for ($u = 0; $u < $count_idtype; $u++) {
                    $iddanhmuc = $arr_idtype[$u];
                    // truy van lay danh mục còn lại chưa thêm vào
                    $s_loaitru .= " and a.id != '" . $iddanhmuc . "' ";
                    if ($idxoa != $iddanhmuc) {
                        $dem++;
                        $lenstr     = '';
                        $selectedid = '';
                        if ($dem == 1) {
                            $str_idtype .= $iddanhmuc;
                        } else {
                            $str_idtype .= ',' . $iddanhmuc;
                        }
                    } // end if
                } // end for
                for ($y = 0; $y < $count_idtype; $y++) {
                    $iddanhmuc = $arr_idtype[$y];
                    if ($idxoa != $iddanhmuc) {
                        $tendanhmuc = $db->getNameFromID("tbl_danhmuc_lang", "ten", "iddanhmuc", "'" . $iddanhmuc . "' and idlang = '" . $_SESSION['__defaultlang'] . "'");
                        $strdanhmuc .= '<li id="itemdmdachon' . $y . '">' . $lenstr . '<a title="' . $arraybien['xoa'] . '" href="#" onclick="Get_Data(\'ajax.php?op='.$__getop.'&name=updatedanhmuc&id=' . $id . '&idxoa=' . $iddanhmuc . '&idtype=' . $str_idtype . '\',\'danhmucload' . $id . '\'); return false;" ><i class="fa  fa-times-circle fa-lg"></i></a> ' . $tendanhmuc . '</li>';
                    }
                }
            }
            $strdanhmuc .= '</ul>';
        } // end idtype != ''
        $aray_update = array("iddanhmuc" => $str_idtype);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        // truy vấn lấy danh mục còn lại chưa thêm vào
        $s_loaitru .= ' order by a.id ';
        $d_loaitru = $db->rawQuery($s_loaitru);
        if (count($d_loaitru) > 0) {
            $strdanhmucthemmoi .= '<select onchange="Get_Data(\'ajax.php?op='.$__getop.'&name=updatedanhmuc&act=them&id=' . $id . '&idthem=\'+this.value+\'&idtype=' . $str_idtype . '\',\'danhmucload' . $id . '\')" name="iddanhmuc_themmoi" class="form-control danhmuc form-control2">
                   <option value="">' . $arraybien['themloai'] . '</option>';
            foreach ($d_loaitru as $key_loaitru => $info_loaitru) {
                $lenstr            = '';
                $danhmucmoi_idtype = $info_loaitru['id'];
                $danhmucmoi_ten    = $info_loaitru['ten'];
                for ($kk = 4; $kk < strlen($danhmucmoi_idtype); $kk += 4) {
                    $lenstr .= '===';
                }
                $strdanhmucthemmoi .= '<option value="' . $danhmucmoi_idtype . '">' . $lenstr . $danhmucmoi_ten . '</option>';
            }
            $strdanhmucthemmoi .= '</select>';
        }
        $strdanhmuc .= $strdanhmucthemmoi;
        $strdanhmuc .= '</div>';
        echo $strdanhmuc;
        break;

    case 'anhien':
        if ($value == 1) {
            $data .= '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        } else {
            $data .= '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        }
        $aray_update = array("anhien" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'thutu':
        $aray_update = array("thutu" => $value);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'sapxepthutu':
        $idget    = trim(ws_get('id'));
        $thutuget = trim(ws_get('thutu'));
        $db->sqlUpdate($thisTable, array("thutu" => $thutuget), "id = $idget");
        break;
}// END SWITH

echo $data;
