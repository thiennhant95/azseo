<?php

$rannumber = rand(100, 999);
$urlreturn = '';
$type      = ws_get('type');
if ($type == 'url') {
    if ($name == 'url') {
        // chieu dai url khoang 115 ki tu
        $chieudaidomain    = strlen('http://' . $_SERVER['HTTP_HOST']);
        $chieudaiurlcanlay = 112 - $chieudaidomain;
        $title_url2        = substr($title_url, 0, $chieudaiurlcanlay);
        $title_url2        = $title_url2 . " ";
        $title_url2        = str_replace("- ", "", $title_url2);
        $title_url2        = trim($title_url2);
        if ($id == '') {
            $urlcheck = $db->getNameFromID("tbl_option_lang", "url", "url", "'" . $title_url2 . "'");
            if ($urlcheck != '') {
                $urlreturn .= $urlcheck . '-' . $rannumber;
            } else {
                $urlreturn = $title_url2;
            }
        } else {
            if ($title_url == $urlcu) {
                $urlreturn = $urlcu;
            } else {
                $urlcheck = $db->getNameFromID("tbl_option_lang", "url", "url", "'" . $title_url2 . "'");
                if ($urlcheck != '') {
                    $urlreturn .= $urlcheck . '-' . $rannumber;
                } else {
                    $urlreturn = $title_url2;
                }
            }
        }
        $urlreturn = preg_replace('/[^a-zA-Z0-9_-]/', '', $urlreturn);
    }
}
// xu ly neu la title
if ($type == 'title') {
    if (strlen($title) > 70) {
        // lay ki tu thu 71
        $kitu71    = $db->utf8_substr($title, 70, 1);
        $kitutrang = 0;
        if ($kitu71 == '' or $kitu71 == ',' or $kitu71 == '.') {
            $kitutrang = 70;
        } else {
// kiem tra khi nao gap ki tu trang
            for ($k = 70; $k > 0; $k--) {
                $ktkitu = $db->utf8_substr($title, ($k - 1), 1);
                if ($ktkitu == ' ' or $ktkitu == ',' or $ktkitu == '.') {
                    $kitutrang = $k;
                    break;
                }
            }
        }
        $urlreturn = $db->utf8_substr($title, 0, $kitutrang);
        //$urlreturn = strlen($title);
    } else {
        $urlreturn = $title;
    }
}
// xu ly neu la tu khoa
if ($type == 'tukhoa') {
    $urlreturn = $title . ', ' . tao_url($title);
}
echo $urlreturn;
