<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array("module_title" => "Tỉnh Thành",
    "table"                             => 'tbl_type',
    "tablenoidung"                      => 'tbl_type_lang',
    "id"                                => 'id',
    "idtype"                            => 0,
    "baiviet"                           => 1,
    "img"                               => 1,
    "loai"                              => 0,
    "thutu"                             => 1,
    "ten"                               => 1,
    "tieude"                            => 0,
    "mota"                              => 0,
    "noidung"                           => 0,
    "url"                               => 0,
    "link"                              => 0,
    "target"                            => 0,
    "tukhoa"                            => 0,
    "motatukhoa"                        => 0,
    "anhien"                            => 1,
    "action"                            => 1,
    "add_item"                          => 1,
    "date"                              => 1,
    "path_img"                          => "../uploads/option/",
    "path_file"                         => "../uploads/files/",
    "chucnangkhac"                      => 0,
    "action"                            => 1,
    "sizeimagesthumb"                   => 300);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}

//$s_update  = "update tbl_type2 set idquanhuyen = '' where length(id) = 12 ";
//$d_update = $db->rawQuery($s_update);
// copy tinh thanh
/*
$s_tinhthanhmain = "select * from tbl_type2 where  idquanhuyen != '' limit 650,50";
$d_tinhthanhmain = $db->rawQuery($s_tinhthanhmain);

foreach($d_tinhthanhmain as $key_main => $info_tinhthanh_main)
{
    $idtinhthanhmain = $info_tinhthanh_main['id'];
    $idquanhuyen     = $info_tinhthanh_main['idquanhuyen'];
    $s_tinhthanh = "select * from  ward where districtid = '$idquanhuyen' order by  wardid  ASC";
    $d_tinhthanh = $db->rawQuery($s_tinhthanh);
    foreach($d_tinhthanh as $key_tinhthanh => $info_tinhthanh){
        $name        = $info_tinhthanh['name'];
        $type        = $info_tinhthanh['type'];
        $idquanhuyen = $info_tinhthanh['districtid'];
        $subid       = $db->createSubID("tbl_type2", "id", $idtinhthanhmain);
        $thutu       = substr($subid,-3);
        if($type=='Phường'){
            $value_type = 4;
        }else{
            $value_type = 5;
        }
        $aray_insert = array(
            "id"            => $subid,
            "loai"          => 0,
            "thutu"         => $thutu,
            "type"          => $value_type,
            "anhien"        => 1
        );
        $id_insert = $db->insert("tbl_type2", $aray_insert);
        $s_lang = "select * from tbl_lang where anhien = 1 order by thutu Asc";
        $d_lang = $db->rawQuery($s_lang);
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = $name;
                $url = tao_url($ten);
                if($idlang==3){ // tieng viet
                    $ten    = tao_url($ten);
                    $url = tao_url($ten).'-en';

                }

                $s_checkurl = "select url from tbl_type2_lang where url = '$url'";
                $d_checkurl = $db->rawQuery($s_checkurl);
                if (count($d_checkurl) > 0) {
                    $url = $url . '-' . rand(0, 100);
                }
                // luu du lieu vao bang danh muc lang
                $aray_insert_lang = array(
                        "idtype" => $subid,
                        "idlang" => $idlang,
                        "ten"    => $ten,
                        "url"    => $url
                );
                $db->insert("tbl_type2_lang", $aray_insert_lang);


            }
        }

    }
}
*/
?>