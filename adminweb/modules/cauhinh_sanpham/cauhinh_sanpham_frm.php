<?php
$__table      = $__config['table'];
$_id_name     = $__config['id'];
$_idSP        = $__config['idsp'];
$_idtype      = $__config['idtype'];
$_getop       = $_GET['op'];
$_sethomepage = $__config['homepage'];
$_hot         = $__config['hot'];
$_hinhtin     = $__config['images'];
$_getidSP     = 1;
if ($__post_task == 'add') {
    $id             = 1;
    $stop_website   = '';
    $thongbao_stop  = '';
    $ten_vie        = '';
    $ten_eng        = '';
    $soluongsanpham = '';
    $soluongtin     = '';
} else {
    $array_data           = get_data($__config['table'], '*', 'id,=,' . $_getidSP . '');
    $data                 = $array_data[0];
    $id                   = $data['id'];
    $trangchu             = $data['trangchu'];
    $sanphamnoibat        = $data['sanphamnoibat'];
    $sanphambanchay_click = $data['sanphambanchay_click'];
    $sanphambanchay_order = $data['sanphambanchay_order'];
    $sanphammoi           = $data['sanphammoi'];
    $giasanpham           = $data['giasanpham'];
    $datmua               = $data['datmua'];
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton)
{
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   submitform( pressbutton );
}
//-->
</script>
';
?>
<div id="toolbar-box">
            <div class="t">
            <div class="t">
               <div class="t"></div>
            </div>
         </div>
         <div class="m">
            <div id="toolbar" class="toolbar">
<table class="toolbar"><tbody><tr>
<td id="toolbar-save" class="button">
  <a class="toolbar" onclick="javascript: submitbutton('save')" href="#">
  <span title="Lưu" class="icon-32-save">
  </span>
    Lưu
  </a>
</td>
<td id="toolbar-help" class="button">
  <a class="toolbar" onclick="popupWindow('http://webso.vn/helps/?op=<?php echo $_GET['op']; ?>&act=form', 'Trợ giúp', 640, 480, 1)" href="#">
  <span title="Trợ giúp" class="icon-32-help">
  </span>
    Trợ giúp
  </a>
</td>
</tr></tbody></table>
</div>
    <div class="header icon-48-addedit">
      <?php
if ($_POST['task'] == 'add') {
    echo 'Thêm mới ';
} else {
    echo 'Cập nhật ';
}
echo $__config['module_title'];?>
    </div>
            <div class="clr"></div>
         </div>
         <div class="b">
            <div class="b">
               <div class="b"></div>
            </div>
         </div>
      </div>
<div class="clr"></div>
<div id="element-box">
         <div class="t">
            <div class="t">
               <div class="t"></div>
            </div>
         </div>
         <div class="m">
                  <form name="adminForm" method="post" action="./?op=<?php echo $_GET['op']; ?>">
            <div id="config-document">
         <div id="page-site" style="display: block;">
            <table class="noshow">
               <tbody><tr>
                  <td width="65%">
                     <fieldset class="adminform">
   <legend><?php echo $__config['module_title']; ?></legend>
   <table cellspacing="1" class="admintable">
   <tbody>
      <tr>
         <td width="185" class="key">
         <span class="editlinktip hasTip">
         Homepage</span>
         </td>
         <td>
         <?php
if ($trangchu == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="trangchu0" name="trangchu">
                <label for="trangchu0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="trangchu1" name="trangchu">
                <label for="trangchu1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="trangchu0" name="trangchu">
                <label for="trangchu0">Không</label>
                <input type="radio" class="inputbox" value="1" id="trangchu1" name="trangchu">
                <label for="trangchu1">Có</label>
            <?php
}
?>
         </td>
      </tr>
      <tr>
         <td valign="top" class="key">Sản phẩm nổi bật</td>
         <td>
         <?php
if ($sanphamnoibat == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="sanphamnoibat0" name="sanphamnoibat">
                <label for="sanphamnoibat0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="sanphamnoibat1" name="sanphamnoibat">
                <label for="sanphamnoibat1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="sanphamnoibat0" name="sanphamnoibat">
                <label for="sanphamnoibat0">Không</label>
                <input type="radio" class="inputbox" value="1" id="sanphamnoibat1" name="sanphamnoibat">
                <label for="sanphamnoibat1">Có</label>
            <?php
}
?>
            </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
            Sản phẩm bán chạy</span>
         </td>
         <td>
         <?php
if ($sanphambanchay_click == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="sanphambanchay_click0" name="sanphambanchay_click">
                <label for="sanphambanchay_click0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="sanphambanchay_click1" name="sanphambanchay_click">
                <label for="sanphambanchay_click1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="sanphambanchay_click0" name="sanphambanchay_click">
                <label for="sanphambanchay_click0">Không</label>
                <input type="radio" class="inputbox" value="1" id="sanphambanchay_click1" name="sanphambanchay_click">
                <label for="sanphambanchay_click1">Có</label>
            <?php
}
?>
            </td>
      </tr>
      <tr>
         <td class="key">Sản phẩm bán chạy(Khi mua hàng)</td>
         <td>
         <?php
if ($sanphambanchay_order == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="sanphambanchay_order0" name="sanphambanchay_order">
                <label for="sanphambanchay_order0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="sanphambanchay_order1" name="sanphambanchay_order">
                <label for="sanphambanchay_order1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="sanphambanchay_order0" name="sanphambanchay_order">
                <label for="sanphambanchay_order0">Không</label>
                <input type="radio" class="inputbox" value="1" id="sanphambanchay_order1" name="sanphambanchay_order">
                <label for="sanphambanchay_order1">Có</label>
            <?php
}
?>
            </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
               Sản phẩm mới</span>
         </td>
         <td>
         <?php
if ($sanphammoi == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="sanphammoi0" name="sanphammoi">
                <label for="sanphammoi0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="sanphammoi1" name="sanphammoi">
                <label for="sanphammoi1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="sanphammoi0" name="sanphammoi">
                <label for="sanphammoi0">Không</label>
                <input type="radio" class="inputbox" value="1" id="sanphammoi1" name="sanphammoi">
                <label for="sanphammoi1">Có</label>
            <?php
}
?>
            </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
               Hiển thị giá sản phẩm</span></td>
         <td>
         <?php
if ($giasanpham == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="giasanpham0" name="giasanpham">
                <label for="giasanpham0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="giasanpham1" name="giasanpham">
                <label for="giasanpham1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="giasanpham0" name="giasanpham">
                <label for="giasanpham0">Không</label>
                <input type="radio" class="inputbox" value="1" id="giasanpham1" name="giasanpham">
                <label for="giasanpham1">Có</label>
            <?php
}
?>
            </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
               Hiển thị nút đặt mua sản phẩm</span>
         </td>
         <td>
         <?php
if ($datmua == 1) {
    ?>
                <input type="radio" class="inputbox"  value="0" id="datmua0" name="datmua">
                <label for="datmua0">Không</label>
                <input type="radio" class="inputbox" checked="checked" value="1" id="datmua1" name="datmua">
                <label for="datmua1">Có</label>
            <?php
} else {
    ?>
                <input type="radio" class="inputbox" checked="checked" value="0" id="datmua0" name="datmua">
                <label for="datmua0">Không</label>
                <input type="radio" class="inputbox" value="1" id="giasanpham1" name="datmua">
                <label for="datmua1">Có</label>
            <?php
}
?>
            </td>
      </tr>
   </tbody>
   </table>
</fieldset></td>
                  </tr>
            </tbody></table>
         </div>
         </td>
               </tr>
            </tbody></table>
         </div>
         <div id="page-server" style="display: none;">
            <table class="noshow">
               <tbody><tr>
                  <td width="60%">
                     <fieldset class="adminform">
   <legend>Cấu hình máy chủ</legend>
   <table cellspacing="1" class="admintable">
      <tbody>
         <tr>
            <td valign="top" class="key">
               <span class="editlinktip hasTip">
                  Đường dẫn tới thư mục Temp             </span>
            </td>
            <td>
               <input type="text" value="\hoangminhcorp\tmp" name="tmp_path" size="50" class="text_area">
            </td>
         </tr>
         <tr>
            <td class="key">
               <span class="editlinktip hasTip">
                  Nén trang dạng GZIP              </span>
            </td>
            <td>
   <input type="radio" class="inputbox" checked="checked" value="0" id="gzip0" name="gzip">
   <label for="gzip0">Không</label>
   <input type="radio" class="inputbox" value="1" id="gzip1" name="gzip">
   <label for="gzip1">Có</label>
            </td>
         </tr>
         <tr>
            <td class="key">
               <span class="editlinktip hasTip">
                  Báo cáo lỗi             </span>
            </td>
            <td>
               <select size="1" class="inputbox" id="error_reporting" name="error_reporting"><option selected="selected" value="-1">Mặc định của hệ thống</option><option value="0">Không</option><option value="7">Đơn giản</option><option value="6143">Lớn nhất</option></select>            </td>
         </tr>
      </tbody>
   </table>
</fieldset>
                     <fieldset class="adminform">
   <legend>Cấu hình Múi giờ</legend>
   <table cellspacing="1" class="admintable">
      <tbody>
      <tr>
         <td width="185" class="key">
            <span class="editlinktip hasTip">
               Múi giờ           </span>
         </td>
         <td>
            <select size="1" class="inputbox" id="offset" name="offset"><option value="-12">(UTC -12:00) International Date Line West</option><option value="-11">(UTC -11:00) Midway Island, Samoa</option><option value="-10">(UTC -10:00) Hawaii</option><option value="-9.5">(UTC -09:30) Taiohae, Marquesas Islands</option><option value="-9">(UTC -09:00) Alaska</option><option value="-8">(UTC -08:00) Pacific Time (US &amp; Canada)</option><option value="-7">(UTC -07:00) Mountain Time (US &amp; Canada)</option><option value="-6">(UTC -06:00) Central Time (US &amp; Canada), Mexico City</option><option value="-5">(UTC -05:00) Eastern Time (US &amp; Canada), Bogota, Lima</option><option value="-4">(UTC -04:00) Atlantic Time (Canada), Caracas, La Paz</option><option value="-3.5">(UTC -03:30) St. John's, Newfoundland and Labrador</option><option value="-3">(UTC -03:00) Brazil, Buenos Aires, Georgetown</option><option value="-2">(UTC -02:00) Mid-Atlantic</option><option value="-1">(UTC -01:00) Azores, Cape Verde Islands</option><option selected="selected" value="0">(UTC 00:00) Western Europe Time, London, Lisbon, Casablanca, Reykjavik</option><option value="1">(UTC +01:00) Amsterdam, Berlin, Brussels, Copenhagen, Madrid, Paris</option><option value="2">(UTC +02:00) Istanbul, Jerusalem, Kaliningrad, South Africa</option><option value="3">(UTC +03:00) Baghdad, Riyadh, Moscow, St. Petersburg</option><option value="3.5">(UTC +03:30) Tehran</option><option value="4">(UTC +04:00) Abu Dhabi, Muscat, Baku, Tbilisi</option><option value="4.5">(UTC +04:30) Kabul</option><option value="5">(UTC +05:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option><option value="5.5">(UTC +05:30) Bombay, Calcutta, Madras, New Delhi</option><option value="5.75">(UTC +05:45) Kathmandu</option><option value="6">(UTC +06:00) Almaty, Dhaka, Colombo</option><option value="6.3">(UTC +06:30) Yagoon</option><option value="7">(UTC +07:00) Bangkok, Hanoi, Jakarta, Phnom Penh</option><option value="8">(UTC +08:00) Beijing, Perth, Singapore, Hong Kong</option><option value="8.75">(UTC +08:00) Western Australia</option><option value="9">(UTC +09:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option><option value="9.5">(UTC +09:30) Adelaide, Darwin, Yakutsk</option><option value="10">(UTC +10:00) Eastern Australia, Guam, Vladivostok</option><option value="10.5">(UTC +10:30) Lord Howe Island (Australia)</option><option value="11">(UTC +11:00) Magadan, Solomon Islands, New Caledonia</option><option value="11.3">(UTC +11:30) Norfolk Island</option><option value="12">(UTC +12:00) Auckland, Wellington, Fiji, Kamchatka</option><option value="12.75">(UTC +12:45) Chatham Island</option><option value="13">(UTC +13:00) Tonga</option><option value="14">(UTC +14:00) Kiribati</option></select>       </td>
      </tr>
      </tbody>
   </table>
</fieldset>
                     <fieldset class="adminform">
   <legend>Cấu hình FTP</legend>
   <table cellspacing="1" class="admintable">
      <tbody>
      <tr>
         <td width="185" class="key">
            <span class="editlinktip hasTip">
                  Bật FTP              </span>
         </td>
         <td>
   <input type="radio" class="inputbox" checked="checked" value="0" id="ftp_enable0" name="ftp_enable">
   <label for="ftp_enable0">Không</label>
   <input type="radio" class="inputbox" value="1" id="ftp_enable1" name="ftp_enable">
   <label for="ftp_enable1">Có</label>
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Máy chủ FTP             </span>
         </td>
         <td>
            <input type="text" value="127.0.0.1" size="25" name="ftp_host" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Cổng FTP             </span>
         </td>
         <td>
            <input type="text" value="21" size="25" name="ftp_port" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Tên đăng nhập FPT             </span>
         </td>
         <td>
            <input type="text" value="root" size="25" name="ftp_user" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Mật khẩu FTP               </span>
         </td>
         <td>
            <input type="password" value="" size="25" name="ftp_pass" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Đường dẫn FTP              </span>
         </td>
         <td>
            <input type="text" value="" size="50" name="ftp_root" class="text_area">
         </td>
      </tr>
      </tbody>
   </table>
</fieldset>
                  </td>
                  <td width="40%">
                     <fieldset class="adminform">
   <legend>Cấu hình CSDL</legend>
   <table cellspacing="1" class="admintable">
      <tbody>
      <tr>
         <td width="185" class="key">
            <span class="editlinktip hasTip">
                  Loại CSDL               </span>
         </td>
         <td>
            <input type="text" value="mysql" size="30" name="dbtype" class="text_area">
         </td>
      </tr>
      <tr>
         <td width="185" class="key">
            <span class="editlinktip hasTip">
                  Tên host             </span>
         </td>
         <td>
            <input type="text" value="localhost" size="30" name="host" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Tên đăng nhập              </span>
         </td>
         <td>
            <input type="text" value="root" size="30" name="user" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Cơ sở dữ liệu              </span>
         </td>
         <td>
            <input type="text" value="a_kh_hoangminhcropdb" size="30" name="db" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Tiền tố của CSDL              </span>
         </td>
         <td>
            <input type="text" value="jos_" size="10" name="dbprefix" class="text_area">
            &nbsp;
            <span class="error hasTip">
               <img border="0" alt="" src="http://localhost/khachhang/hoangminhcorp_cu/includes/js/ThemeOffice/warning.png">           </span>
         </td>
      </tr>
      </tbody>
   </table>
</fieldset>
                     <fieldset class="adminform">
   <legend>Cấu hình mail</legend>
   <table cellspacing="1" class="admintable">
      <tbody>
      <tr>
         <td width="185" class="key">
            <span class="editlinktip hasTip">
                  Người gửi               </span>
         </td>
         <td>
            <select size="1" class="inputbox" id="mailer" name="mailer"><option selected="selected" value="mail">Chức năng email PHP</option><option value="sendmail">Gửi email</option><option value="smtp">Máy chủ SMTP</option></select>        </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Người gửi               </span>
         </td>
         <td>
            <input type="text" value="thanhpv@twin.com.vn" size="30" name="mailfrom" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Từ             </span>
         </td>
         <td>
            <input type="text" value="CÃÂÃÂÃÂÃÂ´ng ty TNHH DV hÃÂÃÂÃÂÃÂ&nbsp;ng hÃÂÃÂ¡ÃÂÃÂºÃÂÃÂ£i vÃÂÃÂÃÂÃÂ&nbsp; thÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂ¡ng mÃÂÃÂ¡ÃÂÃÂºÃÂÃÂ¡i HoÃÂÃÂÃÂÃÂ&nbsp;ng Minh" size="30" name="fromname" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Đường dẫn gửi email              </span>
         </td>
         <td>
            <input type="text" value="/usr/sbin/sendmail" size="30" name="sendmail" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  SMTP Auth               </span>
         </td>
         <td>
   <input type="radio" class="inputbox" checked="checked" value="0" id="smtpauth0" name="smtpauth">
   <label for="smtpauth0">Không</label>
   <input type="radio" class="inputbox" value="1" id="smtpauth1" name="smtpauth">
   <label for="smtpauth1">Có</label>
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Tài khoản SMTP             </span>
         </td>
         <td>
            <input type="text" value="" size="30" name="smtpuser" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Mật khẩu SMTP              </span>
         </td>
         <td>
            <input type="password" value="" size="30" name="smtppass" class="text_area">
         </td>
      </tr>
      <tr>
         <td class="key">
            <span class="editlinktip hasTip">
                  Máy chủ SMTP               </span>
         </td>
         <td>
            <input type="text" value="localhost" size="30" name="smtphost" class="text_area">
         </td>
      </tr>
      </tbody>
   </table>
</fieldset>
                  </td>
               </tr>
            </tbody></table>
         </div>
      <div class="clr"></div>
      <input type="hidden" value="1" name="id">
      <input type="hidden" value="global" name="c">
      <input type="hidden" value="" name="live_site">
      <input type="hidden" value="com_config" name="option">
      <input type="hidden" value="aAdMNkxFuJZbvPyI" name="secret">
      <input type="hidden" value="" name="task">
      <input type="hidden" value="1" name="ac85ded099b2b1b351b137e3ab49f5ac">    </form>
            <div class="clr"></div>
         </div>
         <div class="b">
            <div class="b">
               <div class="b"></div>
            </div>
         </div>
         </div>
</div>
