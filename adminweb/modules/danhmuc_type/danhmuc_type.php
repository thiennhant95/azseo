<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array("module_title" => "Modules",
    "table"                             => 'tbl_danhmuc_type',
    "tablenoidung"                      => 'tbl_danhmuc_type_lang',
    "id"                                => 'id',
    "ten"                               => 1,
    "thutu"                             => 1,
    "op"                                => 1,
    "act"                               => 1,
    "anhien"                            => 1,
    "path_img"                          => "../upload/danhmucsanpham/",
    "path_file"                         => "../upload/files/",
    "action"                            => 1,
    "sizeimagesthumb"                   => 300);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
