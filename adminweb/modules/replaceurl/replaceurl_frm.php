<?php
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Process Thêm mới
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}
$_id_name = $__config['id'];
$_getop   = ws_get('op');
$_idtype  = $__config['type'];
$_idSP    = $__config['idsp'];
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
} else {
    $_getidSP = 1;
}
//*** Xử lý quá trình Chỉnh sửa ***//
$array_data        = get_data($__config['table'], '*', 'id,=,' . $_getidSP . '');
$data              = $array_data[0];
$id                = $data['id'];
$stop_website      = $data['stop_website'];
$thongbao_stop     = $data['thongbao_stop'];
$soluongsanpham    = $data['soluongsanpham'];
$soluongtin        = $data['soluongtin'];
$sendemail         = $data['sendemail'];
$hostmail          = $data['hostmail'];
$hostmail_user     = $data['hostmail_user'];
$hostmail_pass     = $data['hostmail_pass'];
$hostmail_fullname = $data['hostmail_fullname'];
$email_nhan        = $data['email_nhan'];
$rewriteurl        = $data['rewriteurl'];
$analytics         = $data['analytics'];
$webmaster         = $data['webmaster'];
$hinh           = $data['hinh'];
$hinh           = '../upload/' . $hinh;
$img_view          = $__config['path_img'] . $img;
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   var form = document.adminForm;
   // do field validation
   ';
// Kiểm tra tồn tại
if ($_idSP == 1) {
    if ($_getidSP != '') {
        $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' and idSP != '" . $idSP . "' ";
    } else {
        $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' ";
    }
    $d_idSP = $db->rawQuery($s_idSP);
    if (count($d_idSP) > 0) {
        foreach ($d_idSP as $keyidsp => $info_idSP) {
            $value_idSP = $info_idSP['idSP'];
            echo '
            else if (form.idSP.value == \'' . $value_idSP . '\'){
            alert( "Mã sản phẩm ' . $value_idSP . ' đã tồn tại! Vui lòng nhập mã khác!" );
            form.idSP.focus();
            }';
        }
    }
}
// Kiểm tra rỗng
if ($_idtype == 1) {
    echo '
      else if (form.idtype.value == ""){
         alert( "Bạn phải chọn một Loại sản phẩm" );
         form.idtype.focus();
      }';
}
// Hoàn thành validate ==> Submit frm
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
<div class="panel panel-default">
      <div class="panel-heading">
         <div class="panel-title">Sửa đường dẫn hình</div>
      </div>
      <div class="panel-body">
      <form action="./?op=replaceurl&method=replace" id="replaceurl" method="post" name="replaceurl">
         <div class="input-group">
            <div class="input-group-addon">Đường dẫn cũ &nbsp;</div>
            <div class="input-group">
               <input class="form-control" style="min-width:300px;" type="text" name="urlcu" value="" />
            </div>
         </div>
         <br />
         <div class="input-group">
            <div class="input-group-addon">Đường dẫn mới</div>
            <div class="input-group">
               <input class="form-control"  style="min-width:300px;" type="text" name="urlmoi" value="" />
            </div>
         </div><br />
         <input type="submit" class="form-control" style="width:100px; "  name="" value="Thực hiện">
      </form>
   </div>
 </div>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
