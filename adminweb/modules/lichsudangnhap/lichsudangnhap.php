<?php
show_error();
    $db->get('tbl_login_history');
    $total_row = $db->count;

    $page         = (int) !ws_get('page') ? 1 : ws_get('page');
    $page         = ($page == 0 ? 1 : $page);
    $pagelimit    = 0;

    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagelimit = $total_row;
    } else {
        $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
    }
    $perpage    = $pagelimit;
    $startpoint = ($page * $perpage) - $perpage;

    $db->orderBy('thutu');
    $lichsu = $db->get('tbl_login_history', [$startpoint, $perpage]);
    echo '
    <div class="row_content_top">
        <div class="row_content_top_title">
        Lịch sử đăng nhập
        </div>
    </div><!-- /.row_content_top -->
    <div class="row_content_content">';

    if( $db->count ) {
        echo '
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Địa chỉ IP</th>
                    <th>Ngày đăng nhập</th>
                </tr>
            </thead>
            <tbody>
        ';
        foreach( $lichsu as $ksu => $su ) {
            $db->where('id', $su['idtype']);
            $ten = $db->getValue('tbl_user', 'ten');
            echo '
            <tr>
                <td>'.($ksu+1).'</td>
                <td>'.$ten.'</td>
                <td>'.$su['ip'].'</td>
                <td>'.$su['ngay'].'</td>
            </tr>';
        }
        echo '
            </tbody>
        </table>

        <div class="pagechoice"> ' . $arraybien['tongso'] . ': <span style="color:#FF0000; font-weight:bold"> ' . $total_row . '</span> ' . $arraybien['hienthi'] . ' #
            <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage" id="limit" name="limit">';
                for ($i = 1; $i <= count($_arr_listpage); ++$i) {
                    if ($i == $_SESSION['__limit']) {
                        echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
                    } else {
                        echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
                    }
                }
                echo '</select>
                <div class="float-right">';
                if ($_SESSION['__limit'] != '') {
                    //echo $row_total;
                    if ($total_row > 0) {
                        echo Pages($total_row, $perpage, $path_page);
                    }
                }
                echo '
                        </div><!-- ./float-right -->
                    </div><!-- ./pagechoice -->';
    }

echo '</div><!-- /.row_content_content -->';