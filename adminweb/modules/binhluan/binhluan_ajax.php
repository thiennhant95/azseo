<?php
// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;
use Lazer\Classes\Database as Lazer;
$manager = new ImageManager(array('driver' => 'gd'));
$json = Lazer::table('sizeimgcontent')->find(1);

$thisTable = 'tbl_comment';

switch ($name) {
    case 'anhien':
        if ($value == 1) {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        } else {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        }
        $aray_update = array("anhien" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'traloianhien':
        if ($value == 1) {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        } else {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        }
        $aray_update = array("anhien" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'thutu':
        $aray_update = array("thutu" => $value);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id ");
        break;
    case 'sapxepthutu':
        $idget    = trim(ws_get('id'));
        $thutuget = trim(ws_get('thutu'));
        $db->sqlUpdate($thisTable, array("thutu" => $thutuget), "id = $idget");
        break;
} // END SWITCH
echo $data;