<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Hỗ trợ trực tuyến",
    "table"           => 'tbl_hotrotructuyen',
    "tablenoidung"    => 'tbl_hotrotructuyen_lang',
    "id"              => 'id',
    "idtype"          => 1,
    "baiviet"         => 1,
    "img"             => 0,
    "loai"            => 1,
    "thutu"           => 1,
    "ten"             => 1,
    "yahoo"           => 0,
    "skype"           => 1,
    "dienthoai"       => 1,
    "email"           => 1,
    "facebook"        => 1,
    "anhien"          => 1,
    "action"          => 1,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/option/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300);
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
