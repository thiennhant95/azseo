<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
//*** CÁC PHÍM CHỨC NĂNG ***
if ($__post_task == 'unpublish') {
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') {
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            if ($value_file != '') {
                $__path = $__config['path_file'] . $value_file;
                unlink("$__path");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddanhmuc  = '" . $id_order . "' ");
        }
    }
}
if ($__getaction == 'save') {
    $tendangnhap = replace_html(trim(ws_post('tendangnhap')));
    $matkhau     = replace_html(trim(ws_post('rematkhau')));
    $hotendem    = replace_html(trim(ws_post('hotenlot')));
    $ten         = replace_html(trim(ws_post('hoten')));
    $diachi      = replace_html(trim(ws_post('diachi')));
    $email       = replace_html(trim(ws_post('email')));
    $sodienthoai = replace_html(trim(ws_post('dienthoai')));
    $active      = replace_html(trim(ws_post('active')));
    $chonnhom    = replace_html(trim(ws_post('chonnhom')));
    $chonquyen   = replace_html(trim(ws_post('chonquyen')));
    $gioitinh    = replace_html(trim(ws_post('gioitinh')));
    $tinhthanh   = replace_html(trim(ws_post('tinhthanh')));
    $ngaysinh    = replace_html(trim(ws_post('ngaysinh')));
    $website     = replace_html(trim(ws_post('website')));
    $yahoo       = replace_html(trim(ws_post('yahoo')));
    $lat         = rand(100, 999);
    $matkhau     = md5(md5($matkhau) . $lat);
    $_getidSP    = "";
    if ($__getid != '') // neu la cap nhat
    {
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->createSubID($__table, $__id, ws_post('parenid'));
    }
    // Create url lien ket
    $urllink = '';
    $_cid    = ws_get('id');
    if (ws_post('action') == 'add') {
        $thutu = (int) substr($subid, -3);
    } else {
        $thutu = (int) substr($id, -3);
    }
    $images_name = ws_post('url'.$__defaultlang);
    //*** THEM MOI NGUOI DUNG
    if (!isset($your_id) || $your_id == "") {
        echo '<script language="javascript">alert(\'dayne\');
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    } else {
        $aray_insert = array(
            "hotendem"    => $hotendem,
            "ten"         => $ten,
            "gioitinh"    => $gioitinh,
            "email"       => $email,
            "diachi"      => $diachi,
            "tinhthanh"   => $tinhthanh,
            "sodienthoai" => $sodienthoai,
            "ngaysinh"    => $ngaysinh,
            "website"     => $website,
            "yahoo"       => $yahoo,
            "ngaycapnhat" => $db->getDateTimes(),
        );
        $db->sqlUpdate($__table, $aray_insert, "id = " . $your_id);
        // *** ĐỔI MẬT KHẨU ***
        $pass_cu    = replace_html(trim(ws_post('matkhaucu')));
        $pass_moi   = replace_html(trim(ws_post('matkhaumoi')));
        $pass_remoi = replace_html(trim(ws_post('rematkhaumoi')));
        if ($pass_cu != "" && filter_var($pass_cu, FILTER_SANITIZE_STRING)) {
            // NẾU NHẬP MẬT KHẨU THÌ TIẾN HÀNH KIỂM TRA
            $old_pass       = $db->getNameFromID("tbl_user", "matkhau", "id", $your_id);
            $old_lat        = $db->getNameFromID("tbl_user", "lat", "id", $your_id);
            $matkhauhientai = md5(md5($pass_cu) . $old_lat);
            if ($matkhauhientai == $old_pass) {
                // NẾU NHẬP ĐÚNG MẬT KHẨU CỦ THÌ MỚI TIẾP TỤC
                if ($pass_moi != "") {
                    if ($pass_remoi != "") {
                        if ($pass_moi == $pass_remoi) {
                            $passchondoi   = md5(md5($pass_remoi) . $old_lat);
                            $array_doipass = array("matkhau" => $passchondoi);
                            $db->sqlUpdate("tbl_user", $array_doipass, "id = " . $your_id);
                            echo "<script>alert('Mật khẩu đã được thay đổi.')</script>";
                        } else {
                            echo "<script>alert('Mật khẩu mới không khớp.')</script>";
                            echo "<script>location.href='./?op=doithongtin&method=frm';</script>";
                        }
                    } else {
                        echo "<script>alert('Vui lòng nhập lại mật khẩu mới.')</script>";
                        echo "<script>location.href='./?op=doithongtin&method=frm';</script>";
                    }
                } else {
                    echo "<script>alert('Vui lòng nhập mật khẩu mới.')</script>";
                    echo "<script>location.href='./?op=doithongtin&method=frm';</script>";
                }
            } else {
                echo "<script>alert('Mật khẩu cũ không chính xác.')</script>";
                echo "<script>location.href='./?op=doithongtin&method=frm';</script>";
            }
        }
    } // end if cap nhat
}
echo '<script> location.href="./?op=' . ws_get("op") . '"; </script>';
