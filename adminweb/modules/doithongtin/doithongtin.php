<?php session_start();
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array("module_title" => "Cập nhật thông tin",
    "table"                             => 'tbl_user',
    "tablenoidung"                      => 'tbl_danhmuc_lang',
    "id"                                => 'id',
    "idtype"                            => 0,
    "tendangnhap"                       => 1,
    "active"                            => 1,
    "nhom"                              => 1,
    "hotenem"                           => 1,
    "ten"                               => 1,
    "gioitinh"                          => 1,
    "email"                             => 1,
    "sodienthoai"                       => 1,
    "anhien"                            => 1,
    "action"                            => 1,
    "add_item"                          => 1,
    "date"                              => 1,
    "path_img"                          => "../upload/danhmucsanpham/",
    "path_file"                         => "../upload/files/",
    "chucnangkhac"                      => 0,
    "action"                            => 1,
    "sizeimagesthumb"                   => 300);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$your_id        = $_SESSION['user_id'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_frm.php";
}
