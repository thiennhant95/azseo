<?php
//*** XỬ LÝ PHẦN QUYỀN ***
if ($your_id != '') {
    //**** SỬA ****
    if ($__sua == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
} else {
    //**** THÊM ****
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
}
$_id_name = $__config['id'];
$_getop   = ws_get('op');
$_idtype  = $__config['type'];
$ACTION   = '';
$CONTENT  = '';
if ($your_id != '') {
    $__cid       = ws_get('id');
    $s_sql       = "SELECT * from $__table where id = " . $your_id;
    $d_query     = $db->rawQuery($s_sql);
    $d_query     = $d_query[0];
    $id          = check_select($d_query['id']);
    $idtype      = check_select($d_query['idtype']);
    $iduser      = check_select($d_query['iduser']);
    $tendangnhap = check_select($d_query['tendangnhap']);
    $matkhau     = check_select($d_query['matkhau']);
    $hotendem    = check_select($d_query['hotendem']);
    $ten         = check_select($d_query['ten']);
    $hoten       = $hotendem . "&nbsp;" . $ten;
    $diachi      = check_select($d_query['diachi']);
    $email       = check_select($d_query['email']);
    $dienthoai   = check_select($d_query['sodienthoai']);
    $active      = check_select($d_query['active']);
    $chonnhom    = check_select($d_query['nhom']);
    $chonquyen   = check_select($d_query['supperadmin']);
    $gioitinh    = check_select($d_query['gioitinh']);
    $tinhthanh   = check_select($d_query['tinhthanh']);
    $ngaysinh    = check_select($d_query['ngaysinh']);
    $website     = check_select($d_query['website']);
    $yahoo       = check_select($d_query['yahoo']);
} else {
    echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
    exit();
}
//*** VALIDATE FORM ***
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   var form = document.adminForm;
   var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   } else if (form.hoten.value.length == 0) {
      form.hoten.focus();
      bootstrap_alert(\'Bạn chưa nhập tên\',\'alert-danger\');
   } else if (form.email.value.length == 0) {
      form.email.focus();
      bootstrap_alert(\'Email không được để trống\',\'alert-danger\');
   } else if (!form.email.value.match(mailformat)) {
      form.email.focus();
      bootstrap_alert(\'Email không hợp lệ\',\'alert-danger\');
   } ';
// Kiểm tra trùng Email
$postEmail = replace_html(trim(ws_post('email')));
if ($email != $postEmail) {
    $s_tendn = "SELECT email FROM tbl_user WHERE email != '' AND id !=  " . $_SESSION['user_id'];
    $d_tendn = $db->rawQuery($s_tendn);
    if (count($d_tendn) > 0) {
        foreach ($d_tendn as $info_tendn) {
            $value_tendn = $info_tendn['email'];
            echo '
               else if (form.email.value == \'' . $value_tendn . '\'){
               alert( "Email [ ' . $value_tendn . ' ] đã được sử dụng." );
               form.email.focus();
               }';
        }
    }
}
//***TRƯỜNG HỢP THỎA TẤT CẢ CÁC ĐIỀU KIỆN CHECK FORM
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
//*** HIỂN THỊ CÁC NÚT CHỨC NĂNG ***
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action" id="user_btn_action">
   <a class="btn btn-primary " onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://webso.vn/helps/?op=' . ws_get('op') . '&act=form&v=2\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//*** HIỂN THỊ COLS TRÁI ***
$CONTENT .= '
<form action="./?op=' . ws_get('op') . '&method=query&action=save" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">';
/*** update 23/02/2016 ***/
//*** HIỂN THỊ COLS PHẢI ***
$CONTENT .= '
<div class="col_data doithongtinuser">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">&nbsp;' . $arraybien['thongtintaikhoan'] . '</div>
      <div class="panel-body">
         <div id="tabs">
            <div class="tab-text frm-user" id="frm-register">';
//*** HIỆN THỊ FORM TẠO THÀNH VIÊN ***
//---Tên đăng nhập
$CONTENT .= '
         <div class="form-group">
            <label class="control-label">' . $arraybien['tendangnhap'] . '</label>';
if ($tendangnhap != "") {
    $CONTENT .= '
                  <div class="input-group tendangnhap-view clearfix">
                     <label class="label label-default">' . $tendangnhap . '</label>
                  </div>
               ';
} else {
    $CONTENT .= '<input type="text" class="form-control" id="tendangnhap" name="tendangnhap" placeholder="' . $arraybien['nhaptendangnhap'] . '" />';
}
$CONTENT .= '</div> ';
//---Mật khẩu
if ($matkhau != "") {
    $CONTENT .= '
            <div class="checkbox large">
               <label>
                  <input type="checkbox" id="click_matkhau" >
                  ' . $arraybien['doimatkhau'] . '
               </label>
            </div>
            <div id="box_doimatkhau">
               <div class="form-group" >
                  <label class="control-label">' . $arraybien['matkhaucu'] . '</label>
                  <input type="password" class="form-control" id="matkhaucu" name="matkhaucu" placeholder="' . $arraybien['nhapmatkhaucu'] . '" />
               </div>
               <div class="form-group" >
                  <label class="control-label">' . $arraybien['matkhaumoi'] . '</label>
                  <input type="password" class="form-control" id="matkhaumoi" name="matkhaumoi" placeholder="' . $arraybien['nhapmatkhaumoi'] . '" />
               </div>
               <div class="form-group">
                  <label class="control-label">' . $arraybien['xacnhanmatkhaumoi'] . '</label>
                  <input type="password" class="form-control" id="rematkhaumoi" name="rematkhaumoi" placeholder="' . $arraybien['nhaplaimatkhaumoi'] . '" />
               </div>
            </div>
               ';
} else {
    $CONTENT .= '
            <div class="form-group">
               <label class="control-label">' . $arraybien['matkhau'] . '</label>
               <input type="password" class="form-control" id="matkhau" name="matkhau" placeholder="' . $arraybien['nhapmatkhau'] . '" />
            </div>
            <div class="form-group">
               <label class="control-label">' . $arraybien['xacnhanmatkhau'] . '</label>
               <input type="password" class="form-control" id="rematkhau" name="rematkhau" placeholder="' . $arraybien['nhaplaimatkhau'] . '" />
            </div>
            ';
}
//---Họ Tên lots
$CONTENT .= '
         <div class="form-group">
            <label class="control-label">' . $arraybien['hotenlot'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="hotenlot" name="hotenlot" value="' . $hotendem . '" placeholder="' . $arraybien['nhaphotenlot'] . '" />';
$CONTENT .= '</div>';
//---Họ Tên
$CONTENT .= '
         <div class="form-group">
            <label for="hoten" class="control-label">' . $arraybien['ten'] . '</label>
            <span class="star">*</span>';
$CONTENT .= '<input type="text" class="form-control" id="hoten" name="hoten" value="' . $ten . '" placeholder="' . $arraybien['nhapten'] . '" />';
$CONTENT .= '</div> ';
//---Địa chỉ
$CONTENT .= '
         <div class="form-group">
            <label for="diachi" class="control-label">' . $arraybien['diachi'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="diachi" name="diachi" value="' . $diachi . '" placeholder="' . $arraybien['nhapdiachi'] . '" />';
$CONTENT .= '</div>';
//---Email
$CONTENT .= '
         <div class="form-group">
            <label for="email" class="control-label">' . $arraybien['email'] . '</label>
            <span class="star">*</span>';
$CONTENT .= '<input type="text" class="form-control" id="email" name="email" value="' . $email . '" placeholder="' . $arraybien['nhapemail'] . '" />';
$CONTENT .= '</div>';
//---Điện thoại
$CONTENT .= '
         <div class="form-group">
            <label for="dienthoai" class="control-label">' . $arraybien['dienthoai'] . '</label>';
$CONTENT .= '<input type="text" onkeyup="CheckNumber(this)" class="form-control" id="dienthoai" value="' . $dienthoai . '" name="dienthoai" placeholder="' . $arraybien['nhapdienthoai'] . '" />';
$CONTENT .= '</div>';
//---Tỉnh Thành
$CONTENT .= '
         <div class="form-group">
            <label for="tinhthanh" class="control-label">' . $arraybien['tinhthanh'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="tinhthanh" value="' . $tinhthanh . '" name="tinhthanh" placeholder="' . $arraybien['nhaptinhthanh'] . '" />';
$CONTENT .= '</div>';
//---Ngày Sinh
$CONTENT .= '
         <div class="form-group">
            <label for="ngaysinh" class="control-label">' . $arraybien['ngaysinh'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="ngaysinh" value="' . $ngaysinh . '" name="ngaysinh" placeholder="' . $arraybien['nhapngaysinh'] . '" />';
$CONTENT .= '</div>';
//---WEBSITE
$CONTENT .= '
         <div class="form-group">
            <label for="website" class="control-label">' . $arraybien['website'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="website" value="' . $website . '" name="website" placeholder="' . $arraybien['nhapwebsite'] . '" />';
$CONTENT .= '</div>';
//---YAHOO
$CONTENT .= '
         <div class="form-group">
            <label for="yahoo" class="control-label">' . $arraybien['yahoo'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="yahoo" value="' . $yahoo . '" name="yahoo" placeholder="' . $arraybien['nhapyahoo'] . '" />';
$CONTENT .= '</div>';
//-- Giới tính
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['gioitinh'] . '</div>
               <div class="group-active">';
if ($gioitinh == 0) {
    $CONTENT .= '
                  <label><input type="radio" checked="checked" name="gioitinh" value="0" />' . $arraybien['nam'] . '</label>
                  <label><input type="radio" name="gioitinh" value="1">' . $arraybien['nu'] . '</label>
                  ';
} else {
    $CONTENT .= '
                  <label><input type="radio" name="gioitinh" value="0" />' . $arraybien['nam'] . '</label>
                  <label><input type="radio" checked="checked" name="gioitinh" value="1">' . $arraybien['nu'] . '</label>
                  ';
}
$CONTENT .= '</div></div></div>';
$CONTENT .= '
               <input type="hidden" value="' . $your_id . '" name="id">
               <input type="hidden" value="" name="cid[]">
               <input type="hidden" value="0" name="version">
               <input type="hidden" value="0" name="mask">
               <input type="hidden" value="' . $__getop . '" name="op">
               <input type="hidden" value="" name="task">
               <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clear"></div>
</form>';
//*** GÁN VÀO TEMPLATES ***
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
