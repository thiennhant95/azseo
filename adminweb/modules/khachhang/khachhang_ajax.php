<?php

$thisTable         = $_SESSION['__config']['table'];

switch ($name) {
    case 'updatedanhmuc':
        $idxoa      = ws_get('idxoa') ? ws_get('idxoa') : null;
        $idtype     = ws_get('idtype') ? ws_get('idtype') : null;
        $str_idtype = '';
        $strdanhmuc = '<div class="danhmuc_dachon" id="danhmucload' . $id . '">';
        // truy van lay danh muc con lại chưa thêm vào
        $keyname   = 'noidung';
        $idkeytype = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'{$keyname}'");
        $s_loaitru = " SELECT a.id, b.ten
                  from tbl_danhmuc AS a
                  inner join tbl_danhmuc_lang AS b On a.id =  b.iddanhmuc
                  where b.idlang = '" . $_SESSION['__defaultlang'] . "'
                  and a.idtype = '{$idkeytype}' ";
        // neu them moi loai tin
        if (ws_get('act') == 'them') {
            if ($idtype != '') {
                $str_idtype = $idtype;
                if (ws_get('idthem') != '') {
                    $str_idtype .= ',' . ws_get('idthem');
                }
            } else {
                $str_idtype = ws_get('idthem');
            }
        }
        if ($idxoa != '') {
            if ($idtype != '') {
                $arr_idtype   = explode(",", $idtype);
                $count_idtype = count($arr_idtype);
                if ($count_idtype > 0) {
                    $dem = 0;
                    for ($u = 0; $u < $count_idtype; $u++) {
                        $iddanhmuc = $arr_idtype[$u];
                        // truy van lay danh mục còn lại chưa thêm vào
                        $s_loaitru .= " and a.id != '" . $iddanhmuc . "' ";
                        if ($idxoa != $iddanhmuc) {
                            $dem++;
                            $lenstr     = '';
                            $selectedid = '';
                            if ($dem == 1) {
                                $str_idtype .= $iddanhmuc;
                            } else {
                                $str_idtype .= ',' . $iddanhmuc;
                            }
                        } // end if
                    } // end for
                }
            } // end idtype != ''
        }
        $aray_update = array("idtype" => $str_idtype);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = ".$id);
        $idtype      = $str_idtype;
        if ($idtype != '') {
            $strdanhmuc .= '<ul class="ul_danhmucdachon">';
            $arr_idtype   = explode(",", $idtype);
            $count_idtype = count($arr_idtype);
            if ($count_idtype > 0) {
                $dem = 0;
                for ($y = 0; $y < $count_idtype; $y++) {
                    $iddanhmuc = $arr_idtype[$y];
                    $s_loaitru .= " and a.id != '" . $iddanhmuc . "' ";
                    if ($idxoa != $iddanhmuc) {
                        $tendanhmuc = $db->getNameFromID("tbl_danhmuc_lang", "ten", "iddanhmuc", "'" . $iddanhmuc . "' and idlang = '" . $_SESSION['__defaultlang'] . "'");
                        $strdanhmuc .= '<li id="itemdmdachon' . $y . '">' . $lenstr . '<a title="' . $arraybien['xoa'] . '" href="#" onclick="Get_Data(\'ajax.php?op=' . $op . '&name=updatedanhmuc&id=' . $id . '&idxoa=' . $iddanhmuc . '&idtype=' . $str_idtype . '\',\'danhmucload' . $id . '\'); return false;" ><i class="fa  fa-times-circle fa-lg"></i></a> ' . $tendanhmuc . '</li>';
                    }
                }
            }
            $strdanhmuc .= '</ul>';
        } // end idtype != ''
        // truy vấn lấy danh mục còn lại chưa thêm vào
        $s_loaitru .= ' order by a.id ';
        $d_loaitru = $db->rawQuery($s_loaitru);
        if (count($d_loaitru) > 0) {
            $strdanhmucthemmoi .= '<select onchange="Get_Data(\'ajax.php?op=' . $op . '&name=updatedanhmuc&act=them&id=' . $id . '&idthem=\'+this.value+\'&idtype=' . $str_idtype . '\',\'danhmucload' . $id . '\')" name="iddanhmuc_themmoi" class="form-control danhmuc form-control2">
               <option value="">' . $arraybien['themloai'] . '</option>';
            foreach ($d_loaitru as $key_loaitru => $info_loaitru) {
                $lenstr            = '';
                $danhmucmoi_idtype = $info_loaitru['id'];
                $danhmucmoi_ten    = $info_loaitru['ten'];
                for ($kk = 4; $kk < strlen($danhmucmoi_idtype); $kk += 4) {
                    $lenstr .= '===';
                }
                $strdanhmucthemmoi .= '<option value="' . $danhmucmoi_idtype . '">' . $lenstr . $danhmucmoi_ten . '</option>';
            }
            $strdanhmucthemmoi .= '</select>';
        }
        $strdanhmuc .= $strdanhmucthemmoi;
        $strdanhmuc .= '</div>';
        echo $strdanhmuc;
        break;

    case 'loadhinh':
        $s_hinh = "SELECT id,hinh,alt from tbl_noidung_hinh
                   where idnoidung = '" . $idnoidung . "' ";
        $d_hinh = $db->rawQuery($s_hinh);
        if (count($d_hinh) > 0) {
            $CONTENT = '';
            foreach ($d_hinh as $key_select_hinh => $info_hinh) {
                $hinh_id          = $info_hinh['id'];
                $hinh_hinh        = $info_hinh['hinh'];
                $hinh_alt         = $info_hinh['alt'];
                $checkhinhmacdinh = '';
                if ($hinh_hinh == $hinh) {
                    $checkhinhmacdinh = ' checked="checked" ';
                }
                $CONTENT .= '<div id="hinhchitiet' . $hinh_id . '"><div class="img_thumb_item img_thumb_itemajax"><div class="deleteimg"><a href="#" onclick="Get_Data(\'ajax.php?op=' . $op . '&name=deleteimg2&id=' . $hinh_id . '&hinh=' . $hinh_hinh . '\',\'hinhchitiet' . $hinh_id . '\');"><i class="fa fa-times-circle fa-2x"></i></a></div>
                <a onclick="window.open(this.href, \'xemhinh\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50\'); return false;" href="../uploads/noidung/' . $hinh_hinh . '">
                    <img  src="../uploads/noidung/thumb/' . $hinh_hinh . '" />
                </a>';
                $CONTENT .= '
                <div class="hinhdaidien hinhdaidienajax">
                    <div id="alt_hinh_' . $hinh_id . '" class="form-group inputalt">
                        <input onchange="Ajax_noreturn(\'ajax.php?op=' . $op . '&name=alt&id=' . $hinh_id . '&idnoidung=' . $op . '&alt=\'+this.value);" placeholder="Nhập alt images" value="' . $hinh_alt . '" class="form-control" value="' . $hinh_alt . '" />
                    </div>
                    <div class="btn_althinh" onclick="showhidediv(\'alt_hinh_' . $hinh_id . '\')">
                        Alt <i class="fa fa-eye"></i>
                    </div>
                <label><input ' . $checkhinhmacdinh . ' onclick="Ajax_noreturn(\'ajax.php?op=' . $op . '&name=hinhdaidien&id=' . $hinh_id . '&idnoidung=' . $idnoidung . '&hinh=' . $hinh_hinh . '\');" type="radio" name="hinhdaidien" value="1">Đại diện</label></div>';
                $CONTENT .= '
                </div></div>';

            }
        }
        $CONTENT .= '<div class="clear"></div>';
        $CONTENT .= '<div style="color:red; text-align:center; cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&name=loadhinh&idnoidung=' . ws_get('idnoidung') . '&url=' . ws_get('url') . '&hinh=' . ws_get('hinh') . '\',\'loadhinhchitiet\');">[Làm mới danh sách]</div>';
        $CONTENT .= '<iframe width="100%" height="500px" name="images_up" src="modules/' . $op . '/' . $op . '_iframe.php?id=' . $idnoidung . '&op=' . $op . '&url=' . ws_get('url') . '" />';
        echo $CONTENT;
        break;

    case 'luuthemhinh':
        $s_demhinh     = "SELECT count(id) as sohinh FROM tbl_noidung_hinh WHERE idnoidung = '" . ws_get('id') . "' and tmp = 0";
        $d_demhinh     = $db->rawQuery($s_demhinh);
        $sohinhhientai = 0;
        if (count($d_demhinh) > 0) {$sohinhhientai = $d_demhinh[0]['sohinh'];}
        $s_hinh = "SELECT hinh,id from tbl_noidung_hinh where idnoidung = '" . ws_get('idtmp') . "' order by id asc";
        $d_hinh = $db->rawQuery($s_hinh);
        if (count($d_hinh) > 0) {
            foreach ($d_hinh as $key_hinh => $info_hinh) {
                $id_hinh   = $info_hinh['id'];
                $hinh_hinh = $info_hinh['hinh'];
                $sohinhhientai += 1;
                $hinhsave = $hinh_hinh;
                // kiem tra va doi lại tên hình
                if (ws_get('url') != '') {
                    $arr_ext_img = explode('.', $hinh_hinh);
                    $ext_img     = $arr_ext_img[(count($arr_ext_img) - 1)];
                    $hinhsave    = ws_get('url') . '-' . $sohinhhientai . '.' . $ext_img;
                }
                // đưa hình vào thư mục nội dung
                $img_path = "plugin/dropzon/upload/";
                // gan logo len hinh
                if (ws_get('ganlogo') != '') {
                    $hinh_logo = '../uploads/logo/' . ws_get('ganlogo');
                    $hinh_goc  = $img_path . $hinh_hinh;
                    in_logo_len_hinh($hinh_goc, $hinh_logo, $_SESSION['__config']['path_img'] . $hinhsave, ws_get('vitrilogo'));
                } else {
                    $ResizeImage->load($img_path . $hinh_hinh);
                    $ResizeImage->save($_SESSION['__config']['path_img'] . $hinhsave);
                }
                $ResizeImage_thumb->load($_SESSION['__config']['path_img'] . $hinhsave);
                $getsize = getimagesize($img_path . $hinh_hinh);
                // lay do rong cua hinh
                $getwidth = $getsize[1];
                if ($getwidth > $__config['imgwidth']) {
                    //neu hinh lon hon thi se cat hinh
                    //$ResizeImage->resizeToWidth($__config['imgwidth']);
                }
                if ($getwidth > $__config['imgwidth_thumb']) {
                    //neu hinh lon hon thi se cat hinh
                    $ResizeImage_thumb->resizeToWidth($_SESSION['__config']['imgwidth_thumb']);
                }
                // $ResizeImage->load($img_path . $hinh_hinh);
                // $ResizeImage->save($_SESSION['__config']['path_img'] . $hinhsave);
                $ResizeImage_thumb->save($_SESSION['__config']['path_img'] . "thumb/" . $hinhsave);
                // xoa hinh hien tai
                unlink($img_path . $hinh_hinh);
                // cap nhat lại hình đại diện
                //upate lại tên hình cho bảng tbl_noidung_hinh
                $aray_img = array("hinh" => $hinhsave);
                $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
                // cap nhat lại id trong bảng tbl_noidung_hinh => idnoidung theo id mới thêm vào
                $aray_img = array(
                    "idnoidung" => ws_get('id'),
                    "tmp"       => 0);
                $db->sqlUpdate("tbl_noidung_hinh", $aray_img, "id = '" . $id_hinh . "' ");
            } // end for
        } //end coutn
        echo 'Lưu thành công';
        break;
    case 'hinhdaidien':
        $hinh_hinh   = ws_get('hinh');
        $aray_update = array("hinh" => $hinh_hinh);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $idnoidung");
        $aray_update = array("hinhdaidien" => 0);
        $result      = $db->sqlUpdate('tbl_noidung_hinh', $aray_update, "idnoidung = $idnoidung");
        $aray_update = array("hinhdaidien" => 1);
        $result      = $db->sqlUpdate('tbl_noidung_hinh', $aray_update, "id = $id");
        break;
    case 'deleteimg2':
        $unlink       = "../uploads/noidung/" . ws_get('hinh');
        $unlink_thumb = "../uploads/noidung/thumb/" . ws_get('hinh');
        unlink($unlink);
        unlink($unlink_thumb);
        $idxoa = $db->sqlDelete("tbl_noidung_hinh", " id = '" . ws_get('id') . "' ");
        break;
    case 'giagoc':
        $value       = str_replace('.', '', $value);
        $value       = str_replace(',', '', $value);
        $value       = str_replace(' ', '', $value);
        $aray_update = array("giagoc" => $value);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        $loadgia     = $db->getNameFromID($thisTable, "giagoc", "id", $id);
        $loadgia     = trim($loadgia);
        if (is_numeric($loadgia)) {
            $loadgia = number_format($loadgia);
        }
        echo $loadgia;
        break;
    case 'gia':
        $value       = str_replace('.', '', $value);
        $value       = str_replace(',', '', $value);
        $value       = str_replace(' ', '', $value);
        $aray_update = array("gia" => $value);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        $loadgia     = $db->getNameFromID($thisTable, "gia", "id", $id);
        $loadgia     = trim($loadgia);
        if (is_numeric($loadgia)) {
            $loadgia = number_format($loadgia);
        }
        echo $loadgia;
        break;
    case 'uploadimg':
        if (ws_get('act') == 'add') {
            $random = rand(10, 1000);
            if (!empty($_FILES)) {
                $tempFile   = $_FILES['file']['tmp_name'];
                $fileName   = $_FILES['file']['name'];
                $targetPath = './plugin/dropzon/upload/';
                // $new_fileName = explode('.', $fileName);
                $new_fileName = $fileName;
                // $new_fileName = $new_fileName[0] . '.' . $new_fileName[1];
                $targetFile = $targetPath . $new_fileName;
                move_uploaded_file($tempFile, $targetFile);
                // lưu dữ liệu vào database
                $ngaytao = $db->getDate();
                $arrimg  = array(
                    'hinh'      => $new_fileName,
                    'idnoidung' => $_SESSION['tmpimg'],
                    'ngaytao'   => $ngaytao,
                    'tmp'       => 1,
                );
                $flag = $db->insert('tbl_noidung_hinh', $arrimg);
            }
        }
        break;
    case 'anhien':
        if ($value == 1) {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        } else {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
            $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        }
        $aray_update = array("anhien" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'noindex':
        if ($value == 0) {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=noindex&value=1\',\'noindex' . $id . '\')">';
            $data .= '<i title="Không cho Google Index! Bài viết này sẽ không hiển thị trên kết quả tìm kiếm" class="fa fa-ban color-red"></i>';
            $data .= '</a></span>';
            $dataupdate = 1;
        } else {
            $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=noindex&value=0\',\'noindex' . $id . '\')">';
            $data .= '<i title="Cho phép index! Google có thể đưa bài viết này lên kết quả tìm kiếm" class="fa fa-check-square-o color-black"></i>';
            $data .= '</a></span>';
            $dataupdate = 0;
        }
        $aray_update = array("noindex" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'chuyenloai':
        $aray_update = array("loai" => $value);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        break;
    case 'colvalue':
        // lay gia tri colvalue de so sanh
        $colvalue = $db->getNameFromID($thisTable, "colvalue", "id", $id);
        if ($colvalue != '') {
            if (strpos($colvalue, $value) > -1) {
                $colvalue   = str_ireplace($value . ',', "", $colvalue);
                $colvalue   = str_ireplace(',' . $value, "", $colvalue);
                $colvalue   = str_ireplace($value, "", $colvalue);
                $dataupdate = $colvalue;
            } else {
                $dataupdate = $colvalue . ',' . $value;
            }
        } else {
            $dataupdate = $value;
        }
        $itemhome = '<i class="fa fa-times-circle color-red"></i>';
        if (strpos($dataupdate, $value) > -1) {
            $itemhome = '<i class="fa fa-check-circle"></i>';
        }
        $aray_update = array("colvalue" => $dataupdate);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id");
        // xuat gia tri da xu ly ra
        $select_vitri = '';
        $vitridachon  = '';
        $colvalue     = $dataupdate;
        //$array_data lay ben file danhmuc
        if (count($_SESSION['array_data']) > 0) {
            $vitridachon .= '<div id="vitri' . $id . '">';
            $select_vitri .= '<select class="form-control danhmuc" onchange="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=colvalue&value=\'+this.value,\'vitri' . $id . '\')">
                  <option value="">Vị trí hiển thị</option>';
            foreach ($_SESSION['array_data'] as $key_configdanhmuc => $info_configdanhmuc) {
                $config_dinhdanh = $info_configdanhmuc['dinhdanh'];
                $config_ten      = $info_configdanhmuc['ten'];
                if (strpos($colvalue, $config_dinhdanh) > -1) {
                    $vitridachon .= '
                    <div class="btn_vitri">
                      <a class="cursor-pointer" data-toggle="tooltip" title="Bỏ chọn" onclick="Get_Data(\'ajax.php?op=' . $op . '&id=' . $id . '&name=colvalue&value=' . $config_dinhdanh . '&ten=' . $config_ten . '&i=' . $i . '\',\'vitri' . $id . '\')">
                        <i class="fa fa-times float-right"></i>
                      </a>
                      ' . $config_ten . '
                    </div>';
                } else {
                    $select_vitri .= '<option value="' . $config_dinhdanh . '">' . $config_ten . '</option>';
                }
            }
            $select_vitri .= '</select>';
            $vitridachon .= $select_vitri;
            $vitridachon .= '</div>';
            $data = $vitridachon;
        }
        break;
    case 'thutu':
        $aray_update = array("thutu" => $value);
        $result      = $db->sqlUpdate($thisTable, $aray_update, "id = $id ");
        break;
    case 'sapxepthutu':
        $idget    = trim(ws_get('id'));
        $thutuget = trim(ws_get('thutu'));
        $db->sqlUpdate($thisTable, array("thutu" => $thutuget), "id = $idget");
        break;
    case 'importexcel':
        $data.='
        <form method="post" enctype="multipart/form-data" action="./modules/'.$op.'/'.$op.'_importexcel.php?view=upload">
            Chọn file excel để import<br />
            <i>Ghi chú: file excel phải có cấu trúc chuẩn mới có thể import được. Nếu chưa có file chuẩn vui lòng export để có file chuẩn</i>
            <div class="form-control">
                <input class="" required type="file" name="file" />
            </div><br />
            <button type="submit" class="btn btn-success">Upload</button>
        </form>';
        break;
    case 'url':
        if ($type == 'url') {
            if ($name == 'url') {
                // chieu dai url khoang 115 ki tu
                $chieudaidomain    = strlen('http://' . $_SERVER['HTTP_HOST']);
                $chieudaiurlcanlay = 112 - $chieudaidomain;
                $title_url2        = substr($title_url, 0, $chieudaiurlcanlay);
                $title_url2        = $title_url2 . " ";
                $title_url2        = str_replace("- ", "", $title_url2);
                $title_url2        = trim($title_url2);
                if ($id == '') {
                    $urlcheck = $db->getNameFromID("tbl_noidung_lang", "url", "url", "'" . $title_url2 . "'");
                    if ($urlcheck != '') {
                        $urlreturn .= $urlcheck . '-' . $rannumber;
                    } else {
                        $urlreturn = $title_url2;
                    }
                } else {
                    if ($title_url == $urlcu) {
                        $urlreturn = $urlcu;
                    } else {
                        $urlcheck = $db->getNameFromID("tbl_noidung_lang", "url", "url", "'" . $title_url2 . "'");
                        if ($urlcheck != '') {
                            $urlreturn .= $urlcheck . '-' . $rannumber;
                        } else {
                            $urlreturn = $title_url2;
                        }
                    }
                }
                $urlreturn = preg_replace('/[^a-zA-Z0-9_-]/', '', $urlreturn);
            }
        }
        // xu ly neu la title
        if ($type == 'title') {
            if (strlen($title) > 70) {
                // lay ki tu thu 71
                $kitu71    = $db->utf8_substr($title, 70, 1);
                $kitutrang = 0;
                if ($kitu71 == '' or $kitu71 == ',' or $kitu71 == '.') {
                    $kitutrang = 70;
                } else {
                    // kiem tra khi nao gap ki tu trang
                    for ($k = 70; $k > 0; $k--) {
                        $ktkitu = $db->utf8_substr($title, ($k - 1), 1);
                        if ($ktkitu == ' ' or $ktkitu == ',' or $ktkitu == '.') {
                            $kitutrang = $k;
                            break;
                        }
                    }
                }
                $urlreturn = $db->utf8_substr($title, 0, $kitutrang);
                //$urlreturn = strlen($title);
            } else {
                $urlreturn = $title;
            }
        }
        // xu ly neu la tu khoa
        if ($type == 'tukhoa') {
            $urlreturn = $title . ', ' . tao_url($title);
        }
        $data = $urlreturn;
        break;
} // END SWITCH

// xoa hinh
if (ws_get('deleteimg') && ws_get('deleteimg') != '') {
    $unlink = "plugin/dropzon/upload/" . ws_get('deleteimg');
    unlink($unlink);
    // xoa trong database
    $db->sqlDelete("tbl_noidung_hinh", " idnoidung = '" . $_SESSION['tmpimg'] . "' and hinh = '" . ws_post('deleteimg') . "' ");
}
echo $data;
