<?php
$array_data = get_data("confignoidung", '*', 'anhien,=,1');
$_SESSION['array_data'] = array();
$_SESSION['array_data'] = $array_data;
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage       = new SimpleImage();
$ResizeImage_thumb = new SimpleImage();
$__config          = array(
    "module_title"   => $arraybien['modules_dichvu'],
    "table"          => 'tbl_noidung',
    "tablenoidung"   => 'tbl_noidung_lang',
    "loai"           => array_search("service", $_arr_loai_noidung),
    "id"             => 'id',
    "id_name"        => 'idnoidung',
    "keymoudles"     => 'service',
    "idtype"         => 1,
    "masp"           => 1,
    "hinh"           => 1,
    "file"           => 1,
    "gia"            => 0,
    "giagoc"         => 0,
    "noibat"         => 0,
    "moi"            => 0,
    "banchay"        => 0,
    "home"           => 0,
    "khuyenmai"      => 0,
    "ngay"           => 1,
    "ngaycapnhat"    => 1,
    "solanxem"       => 1,
    "thutu"          => 1,
    "soluong"        => 1,
    "anhien"         => 1,
    "noindex"        => 1,
    "iduser"         => 1,
    "target"         => 1,
    "ten"            => 1,
    "tieude"         => 1,
    "url"            => 1,
    "link"           => 1,
    "mota"           => 1,
    "noidung"        => 1,
    "tukhoa"         => 1,
    "motatukhoa"     => 1,
    "tag"            => 1,
    "action"         => 1,
    "add_item"       => 1,
    "option"         => 0,
    "path_img"       => "../uploads/noidung/",
    "path_file"      => "../uploads/files/",
    "chucnangkhac"   => 1,
    "action"         => 1,
    "imgwidth"       => 1500,
    "imgwidth_thumb" => 250,
    "compression"    => 80,
    "inlogo"         => 1,
    "vitrilogo"      => 0 // 0 can giua, 1 can tren trai, 2 can tren phai, 3 can duoi phai, 4 can duoi trai
);
create_dir( $__config['path_file'], false );
create_dir( $__config['path_img'] );
$_SESSION['__config'] = array();
$_SESSION['__config'] = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
