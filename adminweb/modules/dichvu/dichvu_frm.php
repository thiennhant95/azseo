<?php
// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    if ($__sua == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
} else {
    // la them
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
}
$_getop              = ws_get('op');
$_id_name            = @$__config['id'];
$_idtype             = @$__config['type'];
$_idSP               = @$__config['idsp'];
$_sethomepage        = isset($__config['homepage']) ? $__config['homepage'] : null;
$_filedownload       = isset($__config['download']) ? $__config['download'] : null;
$_nhanhieu           = isset($__config['nhanhieu']) ? $__config['nhanhieu'] : null;
$_hinhsanpham        = isset($__config['images']) ? $__config['images'] : null;
$_hinhsanphamchitiet = isset($__config['hinhsanphamchitiet']) ? $__config['hinhsanphamchitiet'] : null;
$ACTION              = '';
$CONTENT             = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
// tao file tmp de uploadhinh
$tmpimg             = time();
$_SESSION['tmpimg'] = $tmpimg;
if ($__getid != '') {
    $__cid       = ws_get('id');
    $s_sanpham   = "SELECT * from $__table where id = $__getid";
    $data        = $db->rawQuery($s_sanpham);
    $data        = $data[0];
    $id          = check_select($data['id']);
    $idtype      = check_select($data['idtype']);
    $masp        = check_select($data['masp']);
    $hinh        = check_select($data['hinh']);
    $file        = check_select($data['file']);
    $colvalue    = check_select($data['colvalue']);
    $ngay        = check_select($data['ngay']);
    $ngaycapnhat = check_select($data['ngaycapnhat']);
    $solanxem    = check_select($data['solanxem']);
    $thutu       = check_select($data['thutu']);
    $loai        = check_select($data['loai']);
    $soluong     = check_select($data['soluong']);
    $anhien      = check_select($data['anhien']);
    $iduser      = check_select($data['iduser']);
    $ten_post    = replace_html(ws_post('ten'.$__defaultlang));
} else {
    // set nhung gia tri mặc định khi thêm mới vào đây
    $solanxem = 0;
    $thutu    = 1;
    $loai     = $__config['loai'];
    $anhien   = 1;
    $noidung  = '<div id="getimgnoidung"></div>';
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   var form = document.adminForm;
   // do field validation
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   } else if (form.ten' . $__defaultlang . '.value == "") {
      alert(\'Tên không được để trống !\');
      form.ten' . $__defaultlang . '.focus();
   } ';
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
';
if (ws_get('id')) {
    $geturl_xemtruoc = $db->getNameFromID("tbl_noidung_lang", "url", "idnoidung", "'" . ws_get('id') . "' and idlang = '{$__defaultlang}'");
    $ACTION .= '<a class="btn btn-default" data-toggle="tooltip" title="' . $arraybien['xemchuyenmuctrenwebsite'] . '" href="../' . $geturl_xemtruoc . '" target="_blank" >
                     <i class="fa fa-external-link"></i>
                </a>';
}
$ACTION .= '
   <a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luuvadong'] . '
   </a>
   <a class="btn btn-default" onclick="javascript: submitbutton_addnew(\'saveandnew\')" href="#">
    <i class="fa fa-clone"></i> ' . $arraybien['luuvathem'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
// col left
$CONTENT .= '
    <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
if ($__config['idtype'] != "") {
    $CONTENT .= '
      <div class="form-group">
         <label for="inputdefault">' . $arraybien['loaitin'] . '</label>';
    $table_name_loai = 'tbl_danhmuc';
    $collumn_name    = "ten";
    $keyname         = @$__config['keymoudles'];
    $idkeytype       = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'{$keyname}'");
    $sql2 = "SELECT a.id, b.ten
            from tbl_danhmuc AS a
            inner join tbl_danhmuc_lang AS b On a.id =  b.iddanhmuc
            where b.idlang = '{$__defaultlang}'
            and a.idtype = '{$idkeytype}'
            order by a.id";
    $d_danhmuc = $db->rawQuery($sql2);
    if (count($d_danhmuc) > 0) {
        if ($idtype != '') {
            $arr_idtype = explode(",", $idtype);
        }
        $CONTENT .= '<ul class="ul_danhmuccheck">';
        $chieudaiid = 4;
        foreach ($d_danhmuc as $key_danhmuc => $info_danhmuc) {
            $danhmuc_id  = $info_danhmuc['id'];
            $danhmuc_ten = $info_danhmuc['ten'];
            $lenstr      = '';
            $selectedid  = '';
            //$chieudaihientai = strlen($danhmuc_id);
            for ($kk = 4; $kk < strlen($danhmuc_id); $kk += 4) {
                // $lenstr .= '===';
                $lenstr .= '!__&nbsp;';
            }
            // kiem tra selected
            if (count($arr_idtype) > 0) {
                if (in_array($danhmuc_id, $arr_idtype) == true) {
                    $selectedid = ' checked = "checked" ';
                }
            }
            $CONTENT .= '<li class="check' . (strlen($danhmuc_id) / 4) . '">';
            $CONTENT .= '<label>' . $lenstr . '<input ' . $selectedid . ' type="checkbox" name="selector[]" id="selector" value="' . $danhmuc_id . '"> ' . $danhmuc_ten . '</label>';
            $CONTENT .= '</li>';
        }
        $CONTENT .= '</ul>';
    }
    $CONTENT .= '
       </div>';
}
// cot value
//$array_data lay ben file sanpham
if ($__config['chucnangkhac'] != '') {
    if (count($array_data) > 0) {
        foreach ($array_data as $key_configdanhmuc => $info_configdanhmuc) {
            $config_dinhdanh = $info_configdanhmuc['dinhdanh'];
            $config_ten      = $info_configdanhmuc['ten'];
            $CONTENT .= '
          <div class="checkbox">
            <label>';
            $checked_colvalue = (strpos($colvalue, $config_dinhdanh) > -1) ? ' checked="checked" ' : null;
            $CONTENT .= '<input value="' . $config_dinhdanh . '" ' . $checked_colvalue . ' type="checkbox" name="colvalue[]" />';
            $CONTENT .= '' . $config_ten . '
            </label>
         </div>';
        }
    }
}

if ($__config['anhien'] == 1) {
    $CONTENT .= '
         <div class="checkbox">
            <label>';
    $checked_anhien = ($anhien == 1) ? ' checked="checked" ' : null;
    $CONTENT .= '<input value="1" type="checkbox" ' . $checked_anhien . ' name="anhien" id="anhien" />';
    $CONTENT .= '
            ' . $arraybien['hienthi'] . '</label>
         </div>';
}
if ($__config['solanxem'] == 1) {
    $CONTENT .= '
        <div class="form-group">
          <label for="inputdefault">' . $arraybien['solanxem'] . '</label>
          <input class="form-control" type="number" min="0"  name="solanxem" id="solanxem" value="' . $solanxem . '" />
        </div>';
}
if ($__config['inlogo'] != '') {
    // truy van lay hinh in logo len hinh
    $s_banner = "SELECT b.ten,hinh,a.iddanhmuc
         from tbl_banner AS a
         inner join tbl_banner_lang AS b On a.id = b.idtype
         where a.loai = 10
         and b.idlang = '{$__defaultlang}'
         and a.anhien = 1
         order by thutu asc ";
    $d_banner = $db->rawQuery($s_banner);
    if (count($d_banner) > 0) {
        $CONTENT .= '<div class="inlogolenhinh">
          <p style="font-size:16px;">Chọn Logo cần gắn lên hình</p>
          <label><input type="radio" name="inlogolenhinh" value="">Không gắn</label><br>';
        foreach ($d_banner as $key_banner => $info_banner) {
            $hinhlogo  = $info_banner['hinh'];
            $tenhinh   = $info_banner['ten'];
            $iddanhmuc = $info_banner['iddanhmuc'];
            $CONTENT .= '<label><input type="radio" name="inlogolenhinh" value="' . $hinhlogo . '">Gắn logo này <img src="../uploads/logo/' . $hinhlogo . '" style="width:120px;"/></label><br>';
        }
        // xuat vi tri logo duoc chon và kh có thể gắn vị trí khác tùy hình
        $CONTENT .= '<br />Vị trí logo';
        $CONTENT .= '<select name="vitrilogo" class="form-control">';
        foreach ($_arr_vitrilogo as $key_vitrilogo => $info_vitrilogo) {
            if ($key_vitrilogo == $iddanhmuc) {
                $CONTENT .= '<option selected="selected" value="' . $key_vitrilogo . '">' . $info_vitrilogo . '</option>';
            } else {
                $CONTENT .= '<option value="' . $key_vitrilogo . '">' . $info_vitrilogo . '</option>';
            }
        }
        $CONTENT .= '</select>';
        $CONTENT .= '</div>';
    }
}
if ($__config['hinh'] == 1) {
    //$CONTENT.='<input type="file" multiple="multiple" name="hinh" id="hinh" />';
    $CONTENT .= ' <div class="form-group">
                     <label for="inputdefault">' . $arraybien['hinh'] . '</label>
                     <div class="clear"></div>';
    // load hinh cũ
    if ($__getid != '') {
        $s_hinh = "select id,hinh,alt from tbl_noidung_hinh
                                      where idnoidung = '{$__getid}' ";
        $d_hinh = $db->rawQuery($s_hinh);
        if (count($d_hinh) > 0) {
            foreach ($d_hinh as $key_select_hinh => $info_hinh) {
                $hinh_id          = $info_hinh['id'];
                $hinh_hinh        = $info_hinh['hinh'];
                $hinh_alt         = $info_hinh['alt'];
                $checkhinhmacdinh = '';
                if ($hinh_hinh == $hinh) {
                    $checkhinhmacdinh = ' checked="checked" ';
                }
                $CONTENT .= '
                <div id="hinhchitiet' . $hinh_id . '">
                  <div class="img_thumb_item">
                    <div class="deleteimg">
                      <a href="#" onclick="if(confirm(\'' . $arraybien['xoahinh'] . '?\')){ javascript:Get_Data(\'ajax.php?op=' . $__getop . '&name=deleteimg2&id=' . $hinh_id . '&hinh=' . $hinh_hinh . '\',\'hinhchitiet' . $hinh_id . '\'); return false;}else{return false; }">
                        <i class="fa fa-times-circle fa-2x"></i>
                      </a>
                    </div>
                    <img src="../uploads/noidung/thumb/' . $hinh_hinh . '" />';
                $CONTENT .= '
                    <div class="hinhdaidien">
                      <div id="alt_hinh_' . $hinh_id . '" class="form-group inputalt">
                        <input onchange="Ajax_noreturn(\'ajax.php?op=' . $__getop . '&name=alt&id=' . $hinh_id . '&idnoidung=' . $__getid . '&alt=\'+this.value);" placeholder="Nhập alt images" value="' . $hinh_alt . '" class="form-control" value="' . $hinh_alt . '" />
                      </div>
                      <div class="btn_althinh" onclick="showhidediv(\'alt_hinh_' . $hinh_id . '\')">
                        Alt <i class="fa fa-eye"></i>
                      </div>

                      <label>
                        <input ' . $checkhinhmacdinh . ' onclick="Ajax_noreturn(\'ajax.php?op=' . $__getop . '&name=hinhdaidien&id=' . $hinh_id . '&idnoidung=' . $__getid . '&hinh=' . $hinh_hinh . '\');" type="radio" name="hinhdaidien" value="1">
                          Đại diện
                      </label>
                    </div>';
                $CONTENT .= '
                  </div>
                </div>';
            }
        }
    }
    $CONTENT .= '
                  <div class="clear"></div>
                        <div id="dZUpload" class="dropzone">
                           <div class="dz-default dz-message">' . $arraybien['huongdanuphinh'] . '</div>
                        </div>
                  </div>
                      <script type="text/javascript">
                        $(document).ready(function() {
                           $(\'#dZUpload\').dropzone({
                              url: "./ajax.php?name=uploadimg&act=add&op=' . $_getop . '",
                              maxFilesize: 100,
                              maxThumbnailFilesize: 5,
                              addRemoveLinks: true,
                              removedfile: function(file) {
                                 var name = file.name;
                                 $.ajax({
                                    type: \'GET\',
                                    url: \'ajax.php\',
                                    data: "&op=' . $_getop . '&id=' . $_SESSION['tmpimg'] . '&deleteimg="+name,
                                    dataType: \'html\',
                                    success: function(result){
                                    }
                                 });
                                 var _ref;
                                 return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                              }
                           });
                        });
                     </script>';
}
if ($__config['file'] == 1) {
    $CONTENT .= '
         <div class="form-group">
         <label for="inputdefault">File</label><br />
         <input type="file" name="file" id="file" />';
    if ( $file != '' && file_exists($__config['path_file']) ) {
        $CONTENT .= '
          <b>File hiện tại:</b> ' . $file . '<br />
          <label><input type="checkbox" name="xoafile" id="xoafile" value="1" />' . $arraybien['xoafile'] . '</label>
          <input type="hidden" name="filename" id="filename" value="' . $file . '" />';
    }
    $CONTENT .= '
         </div>';
}
$CONTENT .= '
         </div>
      </div>
</div>';
// col right
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading">&nbsp;</div>
  <div class="panel-body">
    <div id="tabs">
     <ul>';
$s_lang = "select a.id,a.idkey,b.ten
           from tbl_lang AS a
           inner join tbl_lang_lang AS b On  a.id = b.iddanhmuc
           where b.idlang = '{$__defaultlang}'
           and a.anhien = 1
           order by thutu Asc";
$d_lang = $db->rawQuery($s_lang);
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $tenlang = $info_lang['ten'];
        $idlang  = $info_lang['id'];
        $idkey   = $info_lang['idkey'];
        $CONTENT .= '<li type="#tab' . ($key_lang + 1) . '">' . $tenlang . '</li>';
    }
}
$CONTENT .= '
        <div class="clear"></div>
    </ul>
    <div style="clear:both"></div>
    <div class="tab-text">';
$d_lang2 = $db->rawQuery($s_lang);
if (count($d_lang2) > 0) {
    foreach ($d_lang2 as $key_lang => $info_lang) {
        $tenlang = $info_lang['ten'];
        $idlang  = $info_lang['id'];
        $idkey   = $info_lang['idkey'];
        // lay du lieu theo ngon ngu
        $s_noidung_data = "SELECT * FROM {$__tablenoidung} where idlang = {$idlang} and idnoidung = '{$__cid}' and idnoidung != ''";
        $d_noidung_data = $db->rawQuery($s_noidung_data);
        @$d_noidung_data = $d_noidung_data[0];
        $idnoidung      = check_select($d_noidung_data['idnoidung']);
        $ten            = check_select($d_noidung_data['ten']);
        $tieude         = check_select($d_noidung_data['tieude']);
        $url            = check_select($d_noidung_data['url']);
        $link           = check_select($d_noidung_data['link']);
        $target         = check_select($d_noidung_data['target']);
        $mota           = check_select($d_noidung_data['mota']);
        $noidung        = check_select($d_noidung_data['noidung']);
        $tukhoa         = check_select($d_noidung_data['tukhoa']);
        $motatukhoa     = check_select($d_noidung_data['motatukhoa']);
        $tag            = check_select($d_noidung_data['tag']);
        $CONTENT .= '
         <div id="tab' . ($key_lang + 1) . '">
         <div class="adminform">';
        if ($__config['ten'] == 1) {
            $CONTENT .= '
         <div  class="form-group">
            <label for="inputdefault">' . $arraybien['ten'] . ' ' . $tenlang . '</label>';
            if ($__getid != '') {
                $CONTENT .= '
               <input class="form-control"   name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />
               <button style="margin-top:5px;" type="button" class="btn btn-success"  onclick="taoseo(\'./ajax.php?op=' . $__getop . '&act=checkUrl&name=url&id=' . $__getid . '&title=\'+document.adminForm.ten' . $idlang . '.value,\'url' . $idlang . '\',\'tieude' . $idlang . '\',\'tukhoa' . $idlang . '\')"><i class="fa fa-refresh"></i> ' . $arraybien['taolaiduongdan'] . '</button>
               ';
            } else {
                $CONTENT .= '
               <input data-val-required="Tên sản phẩm không được để trống" class="form-control"  onchange="taoseo(\'ajax.php?op=' . $__getop . '&act=checkUrl&name=url&id=' . $__getid . '&title=\'+this.value,\'url' . $idlang . '\',\'tieude' . $idlang . '\',\'tukhoa' . $idlang . '\')" name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />';
            }
            $CONTENT .= '
         </div>';
        }
        if ($__config['mota'] == 1) {
            $CONTENT .= '
              <div class="form-group">
                 <label for="inputdefault">' . $arraybien['mota'] . ' ' . $tenlang . '</label>
                 <textarea class="form-control seo-placeholder ckeditor"  name="mota' . $idlang . '" cols="100" rows="5">' . $mota . '</textarea>
              </div>';
        }
        if ($__config['noidung'] == 1) {
            // up multi hinh
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['noidung'] . ' ' . $tenlang . '</label>
                <textarea name="noidung' . $idlang . '" id="noidung' . $idlang . '" cols="" rows="" class="ckeditor">' . $noidung . '</textarea>';
            $CONTENT .= '</div>';
        }
        if ($__config['url'] == 1) {
            $CONTENT .= '
              <div class="form-group">
              <label class="control-label strong" for="Alias">' . $arraybien['duongdan'] . ' ' . $tenlang . '</label>
              <div class="row_countkitu">' . $arraybien['chieudaitoiuuchoseo'] . ' <div class="sokitutoida">/115</div><div class="kitudadung" id="kitudadungurl' . $idlang . '">' . strlen($url) . '</div></div>
                  <div class="controls">
                    <div class="input-group">
                     <span class="input-group-addon">http://' . $_SERVER['HTTP_HOST'] . '/</span>
                     <input  onkeyup="CountKey(\'url' . $idlang . '\',\'kitudadungurl' . $idlang . '\');" class="form-control seo-placeholder" placeholder="URL chuyên mục, Bạn có thể nhập dạng: chuyen-muc" name="url' . $idlang . '" type="text" id="url' . $idlang . '" value="' . $url . '" size="70" />
                <input type="hidden" value="' . $url . '" name="hi_url' . $idlang . '" />
                     </div>
                  </div>
              </div>';
        }
        if ($__config['link'] == 1) {
            $CONTENT .= '
              <div class="input-group">
                 <span class="input-group-addon">' . $arraybien['lienket'] . ' ' . $tenlang . '</span>
                <input class="form-control" placeholder="Nhập link nếu bạn muốn chuyển hướng đến một link khác: VD: http://webso.vn"  name="link' . $idlang . '" type="text" id="link' . $idlang . '" value="' . $link . '"  />
              </div><br />
              <div class="input-group">
                <span class="input-group-addon" >' . $arraybien['phuongthuc'] . '</span>
                <div class="col-lg-3 margin0 padding0">
               <select name="target' . $idlang . '" id="target' . $idlang . '" class="form-control col-lg-2 seo-placeholder" >';
            foreach ($_arr_target as $key_target => $value_target) {
                if ($key_target == $target) {
                    $CONTENT .= '<option selected="selected" value="' . $key_target . '">' . $value_target . '</option>';
                } else {
                    $CONTENT .= '<option value="' . $key_target . '">' . $value_target . '</option>';
                }
            }
            $CONTENT .= '</select>';
            $CONTENT .= '
                </div>
             </div>
             <div class="clear"></div><br />';
        }
        if ($__config['tieude'] == 1) {
            $CONTENT .= '
            <div class="form-group">
              <label for="inputdefault">' . $arraybien['tieudetrang'] . ' ' . $tenlang . '</label>
             <div class="row_countkitu">' . $arraybien['sokitudadung'] . ': <div class="sokitutoida">/70</div><div class="kitudadung" id="kitudadungtieude' . $idlang . '">' . strlen($tieude) . '</div></div>
              <input class="form-control" onkeyup="CountKey(\'tieude' . $idlang . '\',\'kitudadungtieude' . $idlang . '\');"  name="tieude' . $idlang . '" type="text" id="tieude' . $idlang . '" value="' . $tieude . '"  />
              </div>';
        }
        if ($__config['motatukhoa'] == 1) {
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['motatukhoa'] . ' ' . $tenlang . '</label>
                <div class="row_countkitu">' . $arraybien['sokitudadung'] . ': <div class="sokitutoida">/320</div><div class="kitudadung" id="kitudadungmota' . $idlang . '">' . strlen($motatukhoa) . '</div></div>
                <textarea class="form-control" onkeyup="CountKey(\'motatukhoa' . $idlang . '\',\'kitudadungmota' . $idlang . '\');" placeholder="Mô tả từ khóa nhập ít hơn 320 kí tự"  id="motatukhoa' . $idlang . '" name="motatukhoa' . $idlang . '" cols="100" rows="3">' . $motatukhoa . '</textarea>
            </div>';
        }
        if ($__config['tukhoa'] == 1) {
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">' . $arraybien['tukhoa'] . ' ' . $tenlang . '</label>
                <textarea class="form-control" placeholder="Nhập từ khóa cách nhau bằng dấu \',\' phẩy"  id="tukhoa' . $idlang . '" name="tukhoa' . $idlang . '" cols="100" rows="1">' . $tukhoa . '</textarea>
             </div>';
        }
        if ($__config['tag'] == 1) {
            $CONTENT .= '
            <div class="form-group">
                <label for="inputdefault">Tag ' . $tenlang . '</label>
               <textarea placeholder="Mỗi từ khóa cách nhau bằng dấu | " class="form-control" name="tag' . $idlang . '" cols="100" rows="2">' . $tag . '</textarea>
            </div>';
        }
        $CONTENT .= '
            </div>
        </div>';
    }
}
$CONTENT .= '
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="" name="saveandnew"><input type="hidden" value="'.ws_get('page').'" name="getpage">
   </div>'; // end tab-text
$CONTENT .= '
</div>'; // end col data
$CONTENT .= '
<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
