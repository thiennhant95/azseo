<?php
// sap xep thu tu khi load lan dau
if ( $_SESSION['sapxepthutu_modules'] != ws_get('op') ) {
    $_SESSION['sapxepthutu_modules'] = ws_get('op');
    $db->thuTu($__config['table'], $__config['id'], "thutu", "loai", $__config['loai']);
}
if( $_SESSION['sapxepthutu_modules'] == ws_get('op') ) {
    $_SESSION['idtype'] = ws_get('idtype');
}
// luu session idtype dang chon
$_id_name = $__config['id_name'];
$s        = "SELECT a.*, b.ten as ten, b.url, b.link
          FROM {$__table} AS a
          INNER JOIN {$__tablenoidung} AS b ON a.id = b.$_id_name
         where b.idlang = '{$__defaultlang}'
          and a.loai = {$__config['loai']} ";
$id       = ws_get('key');
$ten      = ws_get('ten');
$loai     = ws_get('loai');
$ngaydang = ws_get('ngaydang');
$anhien   = ws_get('anhien');
$get_page = ws_get('page') ? ws_get('page') : 1;
if ($__gettukhoa != '') {
    $s = $s . " and (ten LIKE '%{$__gettukhoa}%' OR a.id LIKE '%{$__gettukhoa}%' ) ";
}
if ($__getidtype != '') {
    $s = $s . " AND a.idtype LIKE '{$__getidtype}%' ";
}
if ($__getanhien != '') {
    $s = $s . " AND anhien = '{$__getanhien}' ";
}
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . ' order by  ' . $__sortname . '  ' . $__sortcurent;
} else {
    $s = $s . ' order by  thutu  asc ';
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = $db->rawQuery($s_counttotal);
$total_row    = count($d_counttotal);
$page         = (int) !ws_get('page') ? 1 : ws_get('page');
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . ' ';
}
$data      = $db->rawQuery($s);
$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
echo '
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">' . $arraybien['capnhathinhbaiviet'] . '</h4>
      </div>
      <div class="modal-body" id="loadhinhchitiet">
      </div><div class="clear"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">' . $arraybien['dong'] . '</button>
      </div>
    </div>
  </div>
</div>
<div class="row_content_top">
   <div class="row_content_top_title">' . $__config['module_title'] . '</div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder(' . ($saveorder - 1) . ', \'saveorder\')">
               <i class="fa fa-sort-amount-desc"></i>' . $arraybien['sapxep'] . '
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert(\'Vui lòng lựa chọn từ danh sách\');}else{  submitbutton(\'publish\')}" href="#">
            <i class="fa fa-check-circle"></i>' . $arraybien['bat'] . '
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert(\'Vui lòng lựa chọn từ danh sách\');}else{  submitbutton(\'unpublish\')}" href="#">
          <i class="fa fa-check-circle color-black"></i> ' . $arraybien['tat'] . '
        </a>
';
if ($__xoa == 1) {
    echo '
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert(\'Vui lòng lựa chọn từ danh sách\');}else{  submitbutton_del(\'remove\')}" href="#">
        <i class="fa fa-times-circle color-black"></i> ' . $arraybien['xoa'] . '
        </a>';
}
if ($__them == 1) {
    echo '<a class="btn btn-primary" href="./?op=' . $__getop . '&method=frm">
         <i class="fa fa-plus-circle"></i> ' . $arraybien['themmoi'] . '
        </a>';
}
echo '<a class="btn btn-warning" onclick="popupWindow(\'http://supportv2.webso.vn/?op=' . ws_get('op') . '\', \'Trợ giúp\', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
        </a>
    </div>
';
if ($_arr_excel['bat']==1) {
    echo'
    <div class="row_content_top_action btn-group">
    <span title="Excel" class="btn btn-success"><i class="fa fa-file-excel-o"></i> EXCEL</span>
        <a data-toggle="tooltip" title="'.$arraybien['xuat'].'" class="btn btn-default"  href="#"  onclick="javascript:if(document.adminForm.boxchecked.value==0){swal(\'Vui lòng lựa chọn từ danh sách\');}else{ submitbutton(\'excel_export\')}">
               <i class="fa fa-download"></i>'.$arraybien['xuat'].'
        </a>
        <a class="btn btn-default" href="#" onclick="Get_Data(\'ajax.php?op=' . $__getop . '&name=importexcel\',\'loadhinhchitiet\'); return false;" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-upload"></i>' . $arraybien['nhap'] . '
        </a>
    </div>';
}
echo'
    <div class="clear"></div>
</div>
<div class="row_content_content">';
$row_total = $db->getValue($__table, 'count(*)');
if (ws_post('filter_order_Dir') == 'asc') {
    $filter_order_Dir = 'desc';
}
echo '
<form id="frm" name="adminForm" method="post" action="./?op=' . $__getop . '&method=query">
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" name="this_op" value="' . $__getop . '">
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>
<div class="form-group formsearchlist">
      <label for="focusedInput" class="float-left line-height30" >' . $arraybien['tukhoa'] . ': </label>
      <input type="text" title="' . $arraybien['dieukientimkiemdanhmuc'] . '" style="width:140px;" class="form-control col-xs-3" value="' . $__gettukhoa . '" id="focusedInput" name="tukhoa">';
$keyname   = @$__config['keymoudles'];
$idkeytype = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'{$keyname}'");
$sql2      = "SELECT a.id, b.ten
                from tbl_danhmuc As a
                inner join tbl_danhmuc_lang As b On a.id =  b.iddanhmuc
                where b.idlang = '{$__defaultlang}'
            and a.idtype = '{$idkeytype}'
                order by a.id";
$compare_id = substr($id, 0, strlen($id) - 4);
$option1    = '<option value="">' . $arraybien['danhmuc'] . '</option>';
$danhmuc .= $db->createComboboxDequySql('idtype', $option1, 'form-control danhmuc', 4, $sql2, 'ten', 'id', $__getidtype);
$danhmuc .= '<input type="hidden" name="hi_parenid" value="' . $compare_id . '" />';
echo $danhmuc;
echo '<select id="anhien" name="anhien" class="form-control trangthai">
      <option value="">' . $arraybien['tatca'] . '</option>';
for ($i = 0; $i < count($_arr_anhien); ++$i) {
    if ($i == $__getanhien && $__getanhien != '') {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    }
}
echo '</select>';
echo ' <div class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '&page=' . $__getpage . '&tukhoa=\'+document.adminForm.tukhoa.value+\'&idtype=\'+document.adminForm.idtype.value+\'&anhien=\'+document.adminForm.anhien.value"><i class="fa fa-search"></i> ' . $arraybien['tim'] . '</div>
   <div style="margin-left:10px;" class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '\'"><i class="fa fa-reply"></i> ' . $arraybien['botimkiem'] . '</div>';
echo '</div>';
if ($total_row > 0) {
    echo '<div id="paged">';
    echo Pages($total_row, $perpage, $path_page);
    echo '</div>';
}
echo '
<div class="table-responsive">
      <table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table -sm css' . $__getop . '">
      <thead>
        <tr class="panel-heading" >
          <th width="30"> # </th>
          <th width="5">';
if (isset($_SESSION['__limit'])) {
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
}
echo '<input type="checkbox" onclick="checkAll(' . $pagetotal . ');" value="" name="toggle">
          </th>';
if ($__config['thutu'] == 1) {
    echo '<th width="90" >
          <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=thutu">' . $arraybien['sapxep'];
    if ($__sortname == 'thutu') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '</a>
          </th>';
} // end sort
if ($__config['ten'] == 1) {
    echo '
          <th>
           <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=ten"> ' . $arraybien['tieude'];
    if ($__sortname == 'ten') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '</a>
          </th>';
}
if ($__config['idtype'] == 1) {
    echo '
          <th width="160">
          <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=idtype"> ' . $arraybien['loaitin'];
    if ($__sortname == 'idtype') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '
          </a>
          </th>';
}
if ($__config['hinh'] == 1) {
    echo '<th width="100">' . $arraybien['hinh'] . '</th>';
}
if ($__config['gia'] == 1) {
    echo '
          <th width="130">
          <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=gia"> ' . $arraybien['gia'];
    if ($__sortname == 'gia') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '
          </a>
          </th>';
}
if ($__config['action'] == 1) {
    echo '
          <th width="130">' . $arraybien['thaotac'] . '</th>';
}
if ($__config['anhien'] == 1) {
    echo '
          <th width="80">
            <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=anhien"> ' . $arraybien['hienthi'];
    if ($__sortname == 'anhien') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo '</a></th>';
}
if ($__config['noindex'] == 1) {
    echo'
  <th width="80">
    <a data-toggle="tooltip" title="'.$arraybien['nhapchuotdesapxeptheocotnay'].'" href="'.$path_sort.'&sortname=noindex">
  '.$arraybien['noindex'];
    if ($__sortname == 'noindex') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
    echo'
  </a>
  </th>';
}
if ($__config['chucnangkhac'] == 1) {
    echo '<th style=" width:170px !important;">' . $arraybien['chucnangkhac'] . '</th>';
}
echo '</tr>
        </thead>
        <tbody '.(!empty(ws_get('tukhoa')) || ws_get('anhien')!="" || ws_get('idtype') ? null : 'id="keotha_row"').'>';
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        ++$i;
        if ($i % 2 == 0) {
            $row = 'row0';
        } else {
            $row = 'row1';
        }
        $id = $d['id'];
        echo '
          <tr data-thutu="' . $d['thutu'] . '" data-id="' . $d['id'] . '" data-vitri="' . $key . '" id="sectionid_' . $i . '" class="content_tr ' . $row . '">
            <td title="' . $d['id'] . '"> ' . $i . '</td>
            <td>
              <input id="cb' . ($i - 1).'" type="checkbox" onclick="isChecked(this.checked);" value="' . $d['id'] . '" name="cid[]">
              <input type="hidden" name="img[' . $d['id'] . ']" id="img' . ($i - 1).'" value="' . $d['hinh'] . '" />              <input type="hidden" name="file[' . $d['id'] . ']" id="file' . ($i - 1).'" value="' . $d['file'] . '" />
            </td>';
        if ($__config['thutu'] == 1) {
            echo '<td class="order"><input data-id="' . $d['id'] . '" onchange="ChangeStage(\'ajax.php?op=' . $__getop . '&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">' . '
            </td>';
        } // end thu tu
        if ($__config['ten'] == 1) {
            echo '
            <td class="tieude">
				<a href="./?op=' . ws_get('op') . '&method=frm&id=' . $d['id'] . '&page='.$get_page.'"  title="' . $d['url'] . '" >';
				$s_line = '';
				echo '
					<span class="textcap' . $sot . '">
					' . $d['ten'] . '
					</span>
				</a>';
			if( ! empty($d['schedule']) ){
				echo '
				<br/>
				<small>
					<span class="schedule" style="color:'.(
						$d['anhien'] ?
						'#CCC' :
						'green'
					).';font-weight:normal;">
					'.(
						$d['anhien'] ?
						'Đã được đăng vào lúc:' :
						'Sẽ được đăng vào lúc:'
					).'
						<strong>
							'. date('H:i:s d/m/Y', strtotime($d['schedule'])) .'
						</strong>
					</span> <!-- /.schedule -->
				</small>';
			}echo'
			</td>';
        } // end ten
        if ($__config['idtype'] == 1) {
            echo '<td class="align-center font-bold">';
            $keyname           = @$__config['keymoudles'];
            $idkeytype         = $db->getNameFromID('tbl_danhmuc_type', 'id', 'op', "'{$keyname}'");
            $strdanhmucthemmoi = '';
            $idtype            = $d['idtype'];
            $strdanhmuc        = '<div class="danhmuc_dachon" id="danhmucload' . $id . '">';
            $s_loaitru         = "SELECT a.id, b.ten
                                from tbl_danhmuc AS a
                                inner join tbl_danhmuc_lang AS b On a.id =  b.iddanhmuc
                                where b.idlang = '{$__defaultlang}'
                                 and a.idtype = '{$idkeytype}' ";
            if ($idtype != '') {
                $strdanhmuc .= '<ul class="ul_danhmucdachon">';
                $arr_idtype   = explode(',', $idtype);
                $count_idtype = count($arr_idtype);
                if ($count_idtype > 0) {
                    for ($u = 0; $u < $count_idtype; ++$u) {
                        $iddanhmuc = $arr_idtype[$u];
                        $s_loaitru .= " and a.id != '" . $iddanhmuc . "' ";
                        $lenstr     = '';
                        $selectedid = '';
                        for ($kk = 4; $kk < strlen($iddanhmuc); $kk += 4) {
                            //$lenstr .= '&nbsp;&nbsp;&nbsp;';
                        }
                        $tendanhmuc = $db->getNameFromID('tbl_danhmuc_lang', 'ten', 'iddanhmuc', "'" . $iddanhmuc . "' and idlang = '{$__defaultlang}'");
                        $strdanhmuc .= '<li id="itemdmdachon' . $u . '">' . $lenstr . '<a title="' . $arraybien['xoa'] . '" href="#" onclick="Get_Data(\'ajax.php?op=' . $__getop . '&name=updatedanhmuc&act=xoa&id=' . $d['id'] . '&idxoa=' . $iddanhmuc . '&idtype=' . $idtype . '\',\'danhmucload' . $id . '\'); return false;" ><i class="fa  fa-times-circle fa-lg"></i></a> ' . $tendanhmuc . '</li>';
                    }
                }
                $strdanhmuc .= '</ul>';
            }
            $s_loaitru .= ' order by a.id ';
            $d_loaitru = $db->rawQuery($s_loaitru);
            if (count($d_loaitru) > 0) {
                $strdanhmucthemmoi .= '<select onchange="Get_Data(\'ajax.php?op=' . $__getop . '&name=updatedanhmuc&act=them&id=' . $d['id'] . '&idthem=\'+this.value+\'&idtype=' . $idtype . '\',\'danhmucload' . $id . '\')" name="iddanhmuc_themmoi" class="form-control danhmuc form-control2">
                     <option value="">' . $arraybien['themloai'] . '</option>';
                foreach ($d_loaitru as $key_loaitru => $info_loaitru) {
                    $lenstr            = '';
                    $danhmucmoi_idtype = $info_loaitru['id'];
                    $danhmucmoi_ten    = $info_loaitru['ten'];
                    for ($kk = 4; $kk < strlen($danhmucmoi_idtype); $kk += 4) {
                        $lenstr .= '===';
                    }
                    $strdanhmucthemmoi .= '<option value="' . $danhmucmoi_idtype . '">' . $lenstr . $danhmucmoi_ten . '</option>';
                }
                $strdanhmucthemmoi .= '</select>';
            }
            $strdanhmuc .= $strdanhmucthemmoi;
            $strdanhmuc .= '</div>';
            echo $strdanhmuc;
            // lay combobox chuyển đỏi danh mục
            if ($_SESSION['user_supperadmin']==1) {
                echo'
                <div class="chuyen-modules clearfix">
                <br />Chuyển modules khác

                <select onchange="ChangeStage(\'ajax.php?op=' . $__getop . '&name=chuyenloai&id='.$id.'&value=\'+this.value)" name="chuyenloai" class="form-control danhmuc form-control2">';
                foreach ($_arr_loai_noidung as $key_loai_noidung => $value_loai_noidung) {
                    if ($__config['loai']==$key_loai_noidung) {
                        echo'<option selected="selected" value="'.$key_loai_noidung.'">'.$value_loai_noidung.'</option>';
                    } else {
                        echo'<option value="'.$key_loai_noidung.'">'.$value_loai_noidung.'</option>';
                    }
                }
                echo'</select>
                </div>
                <!-- .chuyen-modules clearfix -->';
            }
            echo'
            </td>';
        } // end idtype
        if ($__config['hinh'] == 1) {
            echo '
            <td style="text-align: center;">';
            if ($d['hinh'] != '') {
                echo '
                    <a  onclick="window.open(this.href, \'xemhinh\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50\'); return false;" href="../uploads/noidung/' . $d['hinh'] . '"><img src="../uploads/noidung/thumb/' . $d['hinh'] . '" width="90" /></a>
                    <a href="#" onclick="Get_Data(\'ajax.php?op=' . ws_get('op') . '&name=loadhinh&idnoidung=' . $d['id'] . '&url=' . $d['url'] . '&hinh=' . $d['hinh'] . '\',\'loadhinhchitiet\');" data-toggle="modal" data-target="#myModal">[' . $arraybien['themhinh'] . ']</a>';
            } else {
                echo '
                <img src="application/templates/images/noimage.png" alt="" width="60" /><br />
                <a href="#" onclick="Get_Data(\'ajax.php?op=' . ws_get('op') . '&name=loadhinh&idnoidung=' . $d['id'] . '&url=' . $d['url'] . '&hinh=' . $d['hinh'] . '\',\'loadhinhchitiet\');" data-toggle="modal" data-target="#myModal">[' . $arraybien['themhinh'] . ']</a>';
            }
            echo '</td>';
        } // end img
        if ($__config['gia'] == 1) {
            echo '<td> ';
            $gia    = $d['gia'];
            $giagoc = $d['giagoc'];
            if (is_numeric($gia)) {
                $gia = number_format($gia);
            }
            if (is_numeric($giagoc)) {
                $giagoc = number_format($giagoc);
            }
            echo '<div class="form-group">
                <label for="inputdefault">' . $arraybien['gia'] . '</label>
                  <div class="controls">
                     <div class="input-group">
                         <span class="input-group-addon">đ</span>
                         <input onchange="changeprice(\'./ajax.php?op=' . $__getop . '&id=' . $id . '&name=gia&value=\'+this.value,\'gia' . $id . '\');" class="form-control" type="text"  name="gia" id="gia' . $d['id'] . '" value="' . $gia . '" />
                     </div>
                  </div>
              </div>';
            echo '<div class="form-group">
                <label for="inputdefault">' . $arraybien['giagoc'] . '</label>
                  <div class="controls">
                     <div class="input-group">
                         <span class="input-group-addon">đ</span>
                         <input  onchange="changeprice(\'./ajax.php?op=' . $__getop . '&id=' . $id . '&name=giagoc&value=\'+this.value,\'giagoc' . $id . '\');" class="form-control" type="text"  name="giagoc" id="giagoc' . $d['id'] . '" value="' . $giagoc . '" />
                     </div>
                  </div>
              </div>
          </td>';
        }
        if ($__config['action'] == 1) {
            echo '<td> <ul class="pagination">';
            if ($__sua == 1) {
                echo '<li><a data-toggle="tooltip" style="background: #337ab7; border-color: #337ab7;" title="' . $arraybien['sua'] . '" href="./?op=' . $__getop . '&method=frm&id=' . $d['id'] . '&page='.$get_page.'" >
               <i class="fa fa-pencil color-white"></i>
           </a></li>';
            }
            echo '<li><a data-toggle="tooltip" title="' . $arraybien['xemchuyenmuctrenwebsite'] . '" href="../' . $d['url'] . '" target="_blank" >
                 <i class="fa fa-external-link"></i>
            </a></li>';
            if ($__xoa == 1) {
                echo '
           <li class="active" ><a data-toggle="tooltip" title="' . $arraybien['xoadongnay'] . '" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del(\'cb'.($i - 1).'\',\'remove\')">
                 <i class="fa fa-trash-o color-white"></i>
            </a></li>';
            }
            echo '</ul></td>';
        } // end action
        // an hien
        if ($__config['anhien'] == 1) {
            echo '
            <td class="align-center"><div id="anhien' . $id . '"> ';
            $itemstage = ' <i title="' . $arraybien['tat'] . '" class="fa fa-eye-slash color-red"></i> ';
            if ($d['anhien'] == 1) {
                $itemstage = ' <i title="' . $arraybien['bat'] . '" class="fa fa-eye color-black">  </i>';
            }
            echo ' <span class="btn btn-default">
            <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $__getop . '&id=' . $id . '&name=anhien&value=' . $d['anhien'] . '\',\'anhien' . $id . '\')" > ' . $itemstage . ' </a>
            </span>
            </div>
            </td>';
        } // end an hien
        ?>
        <?php
        if ($__config['noindex'] == 1) {
            ?>
            <td class="align-center">
              <?php
        echo '<div id="noindex' . $id . '">';
            $itemstage = '<i title="' . $arraybien['khongchoindex'] . '" class="fa fa-ban color-red"></i>';
            if ($d['noindex'] == 0) {
                $itemstage = '<i title="' . $arraybien['choindex'] . '" class="fa fa-check-square-o color-black"></i>';
            }
            echo '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op=' . $__getop . '&id=' . $id . '&name=noindex&value=' . $d['noindex'] . '\',\'noindex' . $id . '\')">' . $itemstage . '</a> </span>';
            echo '</div>'; ?>
            </td>
            <?php

        } // end an hien
        ?>
        <?php

        if ($__config['chucnangkhac'] == 1) {
            echo '<td>';
            $select_vitri = '';
            $vitridachon  = '';
            $colvalue     = $d['colvalue'];
            //$array_data lay ben file danhmuc
            if (count($array_data) > 0) {
                $vitridachon .= '<div id="vitri' . $id . '">';
                $select_vitri .= '<select class="form-control danhmuc chonvitri" onchange="Get_Data(\'ajax.php?op=' . $__getop . '&id=' . $id . '&name=colvalue&thaotac=them&value=\'+this.value,\'vitri' . $id . '\')">
                  <option value="">Vị trí hiển thị</option>';
                foreach ($array_data as $key_configdanhmuc => $info_configdanhmuc) {
                    $config_dinhdanh = $info_configdanhmuc['dinhdanh'];
                    $config_ten      = $info_configdanhmuc['ten'];
                    if (strpos($colvalue, $config_dinhdanh) > -1) {
                        $vitridachon .= '
                    <div class="btn_vitri">
                      <a class="cursor-pointer" data-toggle="tooltip" title="Bỏ chọn" onclick="Get_Data(\'ajax.php?op=' . $__getop . '&id=' . $id . '&name=colvalue&thaotac=xoa&value=' . $config_dinhdanh . '&ten=' . $config_ten . '&i=' . $i . '\',\'vitri' . $id . '\')">
                        <i class="fa fa-times float-right"></i>
                      </a>
                      ' . $config_ten . '
                    </div>';
                    } else {
                        $select_vitri .= '<option value="' . $config_dinhdanh . '">' . $config_ten . '</option>';
                    }
                }
                $select_vitri .= '</select>';
                $vitridachon .= $select_vitri;
                $vitridachon .= '</div>';
                echo $vitridachon;
            }
            echo '</td>';
        } // end chuc nang khac
            echo '</tr>';
    } // end foreach
} // end count
echo '
        </tbody>
    </table>
</div><!-- ./table-responsive -->
<div class="pagechoice"> ' . $arraybien['tongso'] . ': <span style="color:#FF0000; font-weight:bold"> ' . $total_row . '</span> ' . $arraybien['hienthi'] . ' #
<select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage" id="limit" name="limit">';
    for ($i = 1; $i <= count($_arr_listpage); ++$i) {
        if ($i == $_SESSION['__limit']) {
            echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
        } else {
            echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
        }
    }
    echo '</select>
    <div class="float-right">';
    if ($_SESSION['__limit'] != '') {
        //echo $row_total;
        if ($total_row > 0) {
            echo Pages($total_row, $perpage, $path_page);
        }
    }
    echo '
            </div><!-- ./float-right -->
        </div><!-- ./pagechoice -->
    </form>
</div>';
