<?php

    require dirname(dirname(dirname(__DIR__))) . "/configs/inc.php";

$tmpimg             = time();
$_SESSION['tmpimg'] = $tmpimg;
$CONTENT = ''; ?>

<?php
$_getop = ws_get('op');
$CONTENT.='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UP hình</title>
<link rel="stylesheet" href="../../application/templates/css/bootstrap-awesome-animate-hover.min.css">
<script type="text/javascript" src="../../application/templates/js/jquery-1.12.1.min-gop.js"></script>
<script type="text/javascript" src="../../application/templates/js/bootstrap-carousel-sweetalert.min.js"></script>
<!-- Dropzon -->
<link rel="stylesheet" type="text/css" href="../../application/templates/css/css.css" />
<link rel="stylesheet" type="text/css" href="../../application/templates/css/cssv2.css" />
<link rel="stylesheet" type="text/css" href="../../application/templates/css/responsive.css" />
<!-- script -->
<script type="application/javascript" src="../../application/templates/js/javascript.js" ></script>
<script type="application/javascript" src="../../application/templates/js/ajax.js" ></script>
<script type="text/javascript" src="../../application/templates/js/load.js"></script>

<link rel="stylesheet" href="../../plugin/dropzon/css/style-admin.css" type="text/css" />
<link rel="stylesheet" href="../../plugin/dropzon/css/dropzone.css" type="text/css" />
<script type="text/javascript" src="../../plugin/dropzon/js/jquery.min.js"></script>
<script type="text/javascript" src="../../plugin/dropzon/js/dropzone.min.js"></script>

</head>
<body style="background-color:#FFF;">
<hr />
<div id="idluuhinhchitiet">
		<h3>Thêm hình
<button style="float:right; cursor:pointer; type="button" class="btn btn-primary" onclick="Get_Data(\'../../ajax.php?op='.ws_get('op').'&id='.ws_get('id').'&idtmp=' . $_SESSION['tmpimg'] . '&url='.ws_get('url').'&name=luuthemhinh&value=1&vitrilogo=\'+document.getElementById(\'vitrilogo\').value+\'&ganlogo=\'+document.getElementById(\'ganlogo\').value,\'idluuhinhchitiet\')">LƯU HÌNH</button>
<input type="hidden" name="ganlogo" id="ganlogo" value="" />
		</h3>
		<div class="clear"></div><br />';

// truy van lay hinh in logo len hinh
$s_banner = "select b.ten,hinh,a.iddanhmuc
         from tbl_banner AS a
         inner join tbl_banner_lang AS b On a.id = b.idtype
         where a.loai = 10
         and b.idlang = " . $_SESSION['__defaultlang'] . "
         and a.anhien = 1
         order by thutu asc ";
$d_banner    = $db->rawQuery($s_banner);
if (count($d_banner)>0) {
    $CONTENT.='<div class="inlogolenhinh">
  <p style="font-size:16px;">Chọn Logo cần gắn lên hình</p>
  <label><input onclick="document.getElementById(\'ganlogo\').value = this.value" type="radio" name="inlogolenhinh" value="">Không gắn</label><br>';
    foreach ($d_banner  as $key_banner => $info_banner) {
        $hinhlogo = $info_banner['hinh'];
        $tenhinh = $info_banner['ten'];
        $iddanhmuc = $info_banner['iddanhmuc'];
        $CONTENT.='<label><input onclick="document.getElementById(\'ganlogo\').value = this.value" type="radio" name="inlogolenhinh" value="'.$hinhlogo.'">Gắn logo này <img src="../../../uploads/logo/'.$hinhlogo.'" style="width:120px;"/></label><br>';
    }
    // xuat vi tri logo duoc chon và kh có thể gắn vị trí khác tùy hình
    $CONTENT.='<br />Vị trí logo';
    $CONTENT.='<select name="vitrilogo" class="form-control" id="vitrilogo">';
    foreach ($_arr_vitrilogo as $key_vitrilogo => $info_vitrilogo) {
        if ($key_vitrilogo==$iddanhmuc) {
            $CONTENT.='<option selected="selected" value="'.$key_vitrilogo.'">'.$info_vitrilogo.'</option>';
        } else {
            $CONTENT.='<option value="'.$key_vitrilogo.'">'.$info_vitrilogo.'</option>';
        }
    }
    $CONTENT.='</select>';
    $CONTENT.='</div>';
}

$CONTENT.='<br />
        <div class="item-manager">
        <div id="dZUpload" class="dropzone" style="margin-top:0px;">
            <div class="dz-default dz-message">Kéo thả hình vào ô này hoặc click để chọn</div>
        </div>
    </div>
          <script type="text/javascript">
            $(document).ready(function() {
               $(\'#dZUpload\').dropzone({
                  url: "../../ajax.php?name=uploadimg&act=add&op='.ws_get('op').'",
                  maxFilesize: 100,
                  maxThumbnailFilesize: 5,
                  addRemoveLinks: true,
                  removedfile: function(file) {
                     var name = file.name;
                     $.ajax({
                        type: \'GET\',
                        url: \'../../ajax.php\',
                        data: "&op='.ws_get('op').'&id=' . $_SESSION['tmpimg'] . '&deleteimg="+name,
                        dataType: \'html\',
                        success: function(result){
                        }
                     });
                     var _ref;
                     return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                  }
               });
            });
         </script>';

$CONTENT.='</div></body>
</html>';
echo $CONTENT;
