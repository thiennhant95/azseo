<?php
use Lazer\Classes\Database as Lazer;
$__table        = $__config['table'];

// lay cid
$id            = (int)ws_post('id');
$chonchieurong = (int)ws_post('chonchieurong');
$chonchieucao  = (int)ws_post('chonchieucao');
$chieurong     = $chonchieurong ? (int)ws_post('chieurong'): 0;
$chieucao      = $chonchieucao ? (int)ws_post('chieucao'):   0;
$chatluong     = (int)ws_post('chatluong');
$canvas        = (int)ws_post('canvas');
$color         = (string)ws_post('color');
$border        = (int)ws_post('border');
$bordercolor   = (string)ws_post('bordercolor');


$chonchieurong2 = (int)ws_post('chonchieurong2');
$chonchieucao2  = (int)ws_post('chonchieucao2');
$chieurong2     = $chonchieurong2 ? (int)ws_post('chieurong2') : 0;
$chieucao2      = $chonchieucao2 ? (int)ws_post('chieucao2') : 0;
$chatluong2     = (int)ws_post('chatluong2');

if ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '
        <script language="javascript">
         alert("' . $arraybien['khongcoquyenxoa'] . '");
         location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    //del_data($__table, 'id', $id_order);
}
if ($__getaction == 'save') {
    // LƯU TRỮ DỮ LIỆU
    if ( !$id ) {
        /** THÊM MỚI DỮ LIỆU */
        $row = Lazer::table($__table);
    } else {
        $row = Lazer::table($__table)->find($id);
    }

    $row->chonchieurong  = $chonchieurong;
    $row->chonchieucao   = $chonchieucao;
    $row->chieurong      = $chieurong;
    $row->chieucao       = $chieucao;
    $row->chatluong      = $chatluong;
    $row->canvas         = $canvas;
    $row->color          = $color;
    $row->border         = $border;
    $row->bordercolor    = $bordercolor;
    $row->chonchieurong2 = $chonchieurong2;
    $row->chonchieucao2  = $chonchieucao2;
    $row->chieurong2     = $chieurong2;
    $row->chieucao2      = $chieucao2;
    $row->chatluong2     = $chatluong2;

    $row->save();

}
$link_redirect = './?op=' . ws_get('op');
echo '<script> location.href="' . $link_redirect . '"; </script>';
