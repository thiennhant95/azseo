<?php session_start();
$exportlienhe = null;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $hoten     = ws_post('hoten');
    $dienthoai = ws_post('dienthoai');
    $email     = ws_post('email');
    $captcha   = ws_post('captcha');
    $tieude    = ws_post('tieude');
    $noidung   = ws_post('noidung');
    $arrloi    = array();
    $strloi    = "";
    if ($hoten == '') {
        $arrloi[] = 'nullhoten';
        $strloi   = $arraybien['banchuanhaphoten'];
    } elseif (strlen($hoten) < 2) {
        $arrloi[] = 'shorthoten';
        $strloi   = $arraybien['tenquanganvuilongnhaplai'];
    } elseif ($dienthoai == '') {
        $arrloi[] = 'nulldienthoai';
        $strloi   = $arraybien['banchuanhapdienthoai'];
    } elseif (!is_numeric($dienthoai)) {
        $arrloi[] = 'intdienthoai';
        $strloi   = $arraybien['dienthoaiphailaso'];
    } elseif (strlen($dienthoai) < 10 || strlen($dienthoai) > 11) {
        $arrloi[] = 'lendienthoai';
        $strloi   = $arraybien['dienthoaiphailonhon9vanhohon12so'];
    } elseif ($email == '') {
        $arrloi[] = 'nullemail';
        $strloi   = $arraybien['banchuanhapemail'];
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $arrloi[] = 'worngemail';
        $strloi   = $arraybien['emailkhonghople'];
    } elseif ($captcha == '') {
        $arrloi[] = 'nullcaptcha';
        $strloi   = $arraybien['banchuanhapcaptcha'];
    } elseif (strlen($captcha) < 3 || strlen($captcha) > 3) {
        $arrloi[] = 'lencaptcha';
        $strloi   = $arraybien['captchaphai3chuso'];
    } elseif ($captcha != $_SESSION['captcha_code']) {
        $arrloi[] = 'worngcaptcha';
        $strloi   = $arraybien['captchakhongdung'];
    } elseif ($noidung == '') {
        $arrloi[] = 'nullnoidung';
        $strloi   = $arraybien['banchuanhapnoidunglienhe'];
    } elseif (strlen($noidung) < 6) {
        $arrloi[] = 'lennoidung';
        $strloi   = $arraybien['noidunglienhecanrorang'];
    }
    if (empty($arrloi)) {
        $your_ip = $config->get_client_ip();
        // Thuc hien SQL
        $arr_lienhe = array(
            'ten'       => $hoten,
            'email'     => $email,
            'dienthoai' => $dienthoai,
            'chude'     => $tieude,
            'noidung'   => $noidung,
            'ip'        => $your_ip,
            'ngaytao'   => $db->getDateTimes(),
        );
        $result_lienhe = $db->insert("tbl_lienhe", $arr_lienhe);
        if ($result_lienhe > 0) // gui email den admin
        {
            if ($_getArrayconfig['sendemail'] == 1) {
                $subject = $_SERVER['HTTP_HOST'] . ' Liên hệ';
                $message = '
           <table cellpadding=\'4\' cellspacing=\'0\' border=\'1\' style=\'border-collapse:colspan;\'>
            <tr><td colspan="2">' . $arraybien['thongtinkhachhanglienhe'] . '</td></tr>
            <tr><td>' . $arraybien['hoten'] . '</td><td>' . $hoten . '</td></tr>
            <tr><td>' . $arraybien['email'] . '</td><td>' . $email . '</td></tr>
            <tr><td>' . $arraybien['dienthoai'] . '</td><td>' . $dienthoai . '</td></tr>
            <tr><td>' . $arraybien['ip'] . '</td><td>' . $your_ip . '</td></tr>
            <tr><td>' . $arraybien['chude'] . '</td><td>' . $tieude . '</td></tr>
            <tr><td>' . $arraybien['noidung'] . '</td><td>' . $noidung . '</td></tr>
           </table>';
                include "plugin/mail/gmail/class.phpmailer.php";
                include "plugin/mail/gmail/class.smtp.php";
                // hot mail
                $array_sendmail = array("emailnhan" => $_getArrayconfig['email_nhan'],
                    "emailgui"                          => $_getArrayconfig['hostmail_user'],
                    "hostmail"                          => $_getArrayconfig['hostmail'],
                    "user"                              => $_getArrayconfig['hostmail_user'],
                    "pass"                              => $_getArrayconfig['hostmail_pass'],
                    "tieude"                            => $_getArrayconfig['hostmail_fullname'],
                    "fullname"                          => $_getArrayconfig['hostmail_fullname'],
                    "port"                              => 25,
                    "ssl"                               => 0,
                    "subject"                           => $subject,
                    "message"                           => $message,
                );
                sendmail($array_sendmail);
            } // end send mail
        }
        if ($result_lienhe != '') {
            echo "<script>alert('" . $arrraybien['guilienhethanhcong'] . "')</script>";
            echo "<script>window.location.href='./'</script>";
        }
    }
}
$exportlienhe .= '
   <div class="panel panel-default">
      <div class="panel-heading">
         <h3 class="panel-title text-center"><label for="lienhe">' . $arraybien['thongtinlienhe'] . '</label></h3>
      </div>
      <div class="panel-body box-lienhe">
         <form action="" method="post" >';
if (isset($strloi) && $strloi != '') {
    $exportlienhe .= '
               <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  ' . $strloi . '
               </div>
            ';
}
$exportlienhe .= '
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user"></i>&nbsp;' . $arraybien['hoten'] . '</div>
                  <input type="text" name="hoten" class="form-control" id="" placeholder="' . $arraybien['vuilongnhaphovaten'] . '" value="' . (ws_post('hoten') ? ws_post('hoten') : null) . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-phone"></i>&nbsp;' . $arraybien['dienthoai'] . '</div>
                  <input type="text" name="dienthoai" class="form-control" id="" placeholder="' . $arraybien['vuilongnhapsodienthoai'] . '" value="' . (ws_post('dienthoai') ? ws_post('dienthoai') : null) . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-envelope"></i>&nbsp;' . $arraybien['email'] . '</div>
                  <input type="text" name="email" class="form-control" id="" placeholder="' . $arraybien['vuilongnhapemail'] . '" value="' . (ws_post('email') ? ws_post('email') : null) . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-key"></i>&nbsp;' . $arraybien['captcha'] . '</div>
                  <input style="height:47px;" type="text" name="captcha" class="form-control" id="" placeholder="' . $arraybien['vuilongnhapcaptcha'] . '">
                  <div class="input-group-addon">
                     <img src="' . ROOT_PATH . 'plugin/captcha/image.php" id="img-captcha"/>
                     <a onclick="$(\'#img-captcha\').attr(\'src\', \'' . ROOT_PATH . 'plugin/captcha/image.php?rand=\' + Math.random())" title="reload">
                        <img src="' . ROOT_PATH . 'application/templates/images/reload.png" style="width: 30px; height: 30px; cursor: pointer;">
                     </a>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-bell"></i>&nbsp;' . $arraybien['tieude'] . '</div>
                  <input type="text" class="form-control" name="tieude" id="" placeholder="' . $arraybien['vuilongnhaptieude'] . '">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-file-text-o"></i>&nbsp;' . $arraybien['noidung'] . '</div>
                  <textarea class="form-control" name="noidung" rows="3" placeholder="' . $arraybien['vuilongnhapnoidung'] . '">' . (ws_post('noidung') ? ws_post('noidung') : null) . '</textarea>
               </div>
            </div>
            <div class="text-center">
              <input type="submit" name="" class="btn btn-primary btn-lg" value="' . $arraybien['gui'] . '">
              <input type="reset" name="reset" class="btn btn-default btn-lg" value="' . $arraybien['nhaplai'] . '">
           </div>
         </form>
      </div>
   </div>
   ';
return $exportlienhe;
