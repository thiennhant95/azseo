<?php
$s_maps = "SELECT a.*,b.ten,b.diachi,b.zoom,b.toado
               FROM tbl_maps a INNER JOIN tbl_maps_lang b ON
               a.id = b.idtype
               WHERE a.anhien = 1
               AND b.idlang = 1
               ORDER BY a.thutu ASC";
$d_maps = $db->rawQuery($s_maps);
// Get Background
$array_maps = $d_maps[0];
$databando  = '';
if ($array_maps['anhien'] == 1) {
    $databando .= '
<style>
.likbando
{
   font-size:12px;
}
.likbando a
{
   color:#000;
}
.likbando a:hover
{
   color:#09F;
}
</style>
<script type="text/javascript" src="http://code.google.com/apis/gears/gears_init.js" >
</script>
<script type="text/javascript"
src="http://maps.googleapis.com/maps/api/js?sensor=false&language=vi"></script>
<script type="text/javascript">
var map;
function initialize() {
      var myLatlng = new google.maps.LatLng(' . $array_maps['toado'] . ')
      var myOptions = {
    zoom:' . $array_maps['zoom'] . ',
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
}
map = new google.maps.Map(document.getElementById("div_id"), myOptions);
  // Bi?n text ch?a n?i dung s? du?c hi?n th?
var text;
text = \'<div style="width:280px; color:#000; font-size:12px; padding-right:10px;"><b>' . $array_maps['ten'] . '</b><br /><b>Address: </b>' . $array_maps['diachi'] . '</div>\'
   var infowindow = new google.maps.InfoWindow(
    { content: text,
        size: new google.maps.Size(100,50),
        position: myLatlng
    });
       infowindow.open(map);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title:"B?n d? hu?ng d?n du?ng di"
  });
}
</script>
<div  id="div_id" class="css_maps"></div>';
    $databando .= '
<script>window.onload=initialize();</script>';
}
return $databando;
