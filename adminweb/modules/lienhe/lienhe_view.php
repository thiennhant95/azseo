<?php
$_id_name = $__config['id'];
$s        = "SELECT * FROM $__table WHERE 1=1 ";
$id       = ws_get('key');
$ten      = ws_get('ten');
$loai     = ws_get('loai');
$ngaydang = ws_get('ngaydang');
$anhien   = ws_get('anhien');
$get_page = ws_get('page') ? ws_get('page') : 1;
if ($__gettukhoa != '') {
    $s = $s . " and (ten LIKE '%{$__gettukhoa}%' OR a.id LIKE '%{$__gettukhoa}%' ) ";
}
if ($__getidtype != '') {
    $s = $s . " AND a.idtype LIKE '{$__getidtype}%' ";
}
if ($__getanhien != '') {
    $s = $s . " AND anhien = '{$__getanhien}' ";
}
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . " order by  " . $__sortname . "  " . $__sortcurent;
} else {
    $s = $s . " order by  id  Desc ";
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = $db->rawQuery($s_counttotal);
$total_row    = count($d_counttotal);
$page         = (int) (!ws_get("page") ? 1 : ws_get("page"));
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . " ";
}
$data      = $db->rawQuery($s);
$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
?>
<div class="row_content_top">
   <div class="row_content_top_title"><?php echo $__config['module_title']; ?></div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder('<?php echo $saveorder - 1; ?>', 'saveorder')">
               <i class="fa fa-sort-amount-desc"></i> <?php echo $arraybien['sapxep']; ?>
        </a>
        <a class="btn btn-default hidden" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('publish')}" href="#">
            <i class="fa fa-check-circle"></i> <?php echo $arraybien['bat']; ?>
        </a>
        <a class="btn btn-default hidden" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('unpublish')}" href="#">
          <i class="fa fa-check-circle color-black"></i> <?php echo $arraybien['tat']; ?>
        </a>
      <?php
if ($__xoa == 1) {
    ?>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton_del('remove')}" href="#">
        <i class="fa fa-times-circle color-black"></i> <?php echo $arraybien['xoa']; ?>
        </a>
        <?php
}
?>
      <?php
if ($__them == 1) {
    ?>
        <a class="btn btn-primary hidden" href="./?op=<?php echo $__getop; ?>&method=frm">
         <i class="fa fa-plus-circle"></i> <?php echo $arraybien['themmoi']; ?>
        </a>
      <?php
}
?>
        <a class="btn btn-warning" onclick="popupWindow('http://supportv2.webso.vn/?op=<?php echo ws_get('op'); ?>', 'Trợ giúp', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> <?php echo $arraybien['trogiup']; ?>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="row_content_content">
<?php
$row_total = $db->getValue($__table, "count(*)");
if (ws_post('filter_order_Dir') == 'asc') {
    $filter_order_Dir = 'desc';
}
?>
<form  id="frm" name="adminForm" method="post" action="./?op=<?php echo $__getop; ?>&method=query">
<?php
echo '
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>';
?>
         <?php if ($total_row > 0) {echo Pages($total_row, $perpage, $path_page);}
?>
<div class="table-responsive">
      <table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table">
      <thead>
        <tr class="panel-heading" >
<!-- STRART TH -->
         <th width="30"> # </th>
         <th width="5">
          <?php
if (isset($_SESSION['__limit'])):
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
endif?>
            <input type="checkbox" onclick="checkAll(<?php echo $pagetotal; ?>);" value="" name="toggle">
         </th>
          <?php
if ($__config['thutu'] == 1): ?>
             <th width="90" >
             <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=thutu"> <?php
echo $arraybien['sapxep'];
if ($__sortname == 'thutu') {
    if ($__sortcurent == 'desc') {
        echo '<i class="fa fa-sort-amount-desc color-black"></i>';
    } else {
        echo '<i class="fa fa-sort-amount-asc color-black"></i>';
    }
}
 ?>
             </a>
            </th> <?php
endif;
?>
          <?php
if ($__config['ten'] == 1): ?>
          <th>
            <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=ten"> <?php
echo $arraybien['hoten'];
if ($__sortname == 'ten') {
    if ($__sortcurent == 'desc') {
        echo '<i class="fa fa-sort-amount-desc color-black"></i>';
    } else {
        echo '<i class="fa fa-sort-amount-asc color-black"></i>';
    }
}
 ?>
            </a>
          </th> <?php
endif;?>
         <?php if ($__config['email'] == 1): ?>
            <th>
               <?php echo $arraybien['email']; ?>
            </th>
         <?php endif?>
         <?php if ($__config['sodienthoai'] == 1): ?>
            <th>
               <?php echo $arraybien['sodienthoai']; ?>
            </th>
         <?php endif?>
         <?php if ($__config['diachi'] == 1): ?>
            <th>
               <?php echo $arraybien['diachi']; ?>
            </th>
         <?php endif?>
         <?php if ($__config['ngaygui'] == 1): ?>
            <th>
               <?php echo $arraybien['ngaygui']; ?>
            </th>
         <?php endif?>
         <?php if ($__config['action'] == 1): ?>
                <th width="130"><?php echo $arraybien['thaotac']; ?></th>
         <?php endif;?>
         <?php if ($__config['id'] == 1): ?>
            <th>
               <?php echo $arraybien['id']; ?>
            </th>
         <?php endif?>
        </tr>
        </thead>
<!-- START BODY -->
        <tbody>
<?php
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        $i++;
        if ($i % 2 == 0) {
            $row = "row0";
        } else {
            $row = "row1";
        }
        $id = $d['id'];
        ?>
          <tr id="sectionid_<?php echo $i; ?>" class="<?php echo $row; ?>">
            <td title="<?php echo $d['id']; ?>">
               <?php echo $i; ?>
            </td>
            <td>
              <input id="cb<?php echo $i - 1; ?>" type="checkbox" onclick="isChecked(this.checked);" value="<?php echo $d['id']; ?>" name="cid[]">
            </td>
    <?php if ($__config['thutu'] == 1): ?>
            <td class="order">
              <?php
if (strlen($d['id']) == 8) {
            echo '
                   <input type="number" size="10" style="background-color:#f0f0f0; text-align: center" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
        } else if (strlen($d['id']) == 12) {
            echo '
                   <input type="number" size="10"  class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
        } else if (strlen($d['id']) == 16) {
            echo '
                   <input type="number" size="10"  class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
        } else {
            echo '
                   <input type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
        }
        ?>
            </td>
  <?php endif;?>
  <?php if ($__config['ten'] == 1): ?>
            <td class="tieude">
            <a href="<?php echo './?op=' . ws_get('op') . '&method=frm&id=' . $d['id']; ?>"  title="<?php echo $d['id']; ?>" >
            <?php
$s_line = '';
        $s_line = '<div class="cap1 btn btn-primary"><i>1</i></div>';
        for ($t = 4; $t < 100; $t += 4) {
            if (strlen($d['id']) == $t) {
                $sot         = $t / 4;
                $margin_left = ' style="margin-left:' . (($sot - 1) * 20) . 'px;" ';
                $s_line      = '<button ' . $margin_left . ' class="cap' . $sot . ' btn btn-primary"><i>' . $sot . '</i></button>';
                break;
            }
        }
        echo $s_line . '<span class="textcap' . $sot . '">' . $d['ten'] . '</span>';
        ?>
            </a></td>
   <?php endif;?>
   <?php if ($__config['email'] == 1): ?>
      <td>
         <?php echo $d['email']; ?>
      </td>
   <?php endif?>
   <?php if ($__config['sodienthoai'] == 1): ?>
      <td>
         <?php echo $d['dienthoai']; ?>
      </td>
   <?php endif?>
   <?php if ($__config['diachi'] == 1): ?>
      <td>
         <?php echo $d['diachi']; ?>
      </td>
   <?php endif?>
   <?php if ($__config['ngaygui'] == 1): ?>
      <td>
         <?php echo $d['ngaytao']; ?>
      </td>
   <?php endif?>
   <?php if ($__config['action'] == 1): ?>
      <td>
      <ul class="pagination">
      <?php
if ($__sua == 1) {
            ?>
         <li><a data-toggle="tooltip" style="background: #337ab7; border-color: #337ab7;" title="<?php echo $arraybien['sua']; ?>" href="<?php echo './?op=' . $__getop . '&method=frm&id=' . $d['id']; ?>" >
             <i class="fa fa-pencil color-white"></i>
         </a></li>
          <?php
}
        ?>
      <?php
if ($__xoa == 1) {
            ?>
         <li class="active" ><a data-toggle="tooltip" title="<?php echo $arraybien['xoadongnay']; ?>" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del('cb<?php echo $i - 1; ?>','remove')" >
               <i class="fa fa-trash-o color-white"></i>
          </a></li>
          <?php
}
        ?>
      </ul>
      </td>
   <?php endif;?>
   <?php if ($__config['id'] == 1): ?>
      <td align="center">
         <?php echo $d['id']; ?>
      </td>
   <?php endif?>
</tr>
          <?php
} // end foreach
} // end count
?>
        </tbody>
      </table></div>
      <div class="pagechoice">
       <?php echo $arraybien['tongso']; ?>: <span style="color:#FF0000; font-weight:bold">
               <?php echo $total_row; ?>
            </span>
       <?php echo $arraybien['hienthi']; ?> #
       <?php echo '
        <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage"  id="limit" name="limit">';
for ($i = 1; $i <= count($_arr_listpage); $i++) {
    if ($i == $_SESSION['__limit']) {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    }
}
echo '</select>';
?>
      <div class="float-right">
      <?php
if ($_SESSION['__limit'] != '') {
    //echo $row_total;
    if ($total_row > 0) {echo Pages($total_row, $perpage, $path_page);}
}
?>
      </div>
      </div>
</form>
</div>