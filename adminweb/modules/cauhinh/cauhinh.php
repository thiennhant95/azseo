<?php

// Xóa bài viết thừa
$dxoa = $db->rawQuery("
    SELECT id FROM tbl_noidung_lang WHERE idnoidung NOT IN (SELECT id FROM tbl_noidung)
");
if ( $db->count > 0) {
    foreach ($dxoa as $vxoa) {
        $db->where("id", $vxoa['id']);
        $db->delete("tbl_noidung_lang");
    }
}
// Cập nhật ICON
$dirIcon = "../uploads/config/lib_favicon/";
if (count(glob("$dirIcon/*")) === 0 || !is_dir($dirIcon)) {
    include $__getop . '_taiicon.php';
    unlink("../uploads/config/lib_favicon.zip");
    exit("Cập nhật icon thành công, ấn F5 để tiếp tục");
}

include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();

if (!isset($_SESSION['active_config']) || empty($_SESSION['active_config'])) {
    $_SESSION['active_config'] = 'general';
}

$__config = array(
    "module_title"    => "Cấu hình chung",
    "table"           => 'config',
    "tablenoidung"    => '',
    "id"              => 'id',
    "stop_website"    => 1,
    "soluongsanpham"  => 1,
    "soluongtin"      => 1,
    "website"         => 1,
    "binhluanwebsite" => 1,
    "email"           => 1,
    "bgtop"           => 1,
    "bgmenu"          => 1,
    "bgmain"          => 1,
    "bgbottom"        => 1,
    "analytics"       => 1,
    "webmaster"       => 1,
    "appfacebookid"   => 1,
    "appgoogleid"     => 1,
    "embed_before"    => 1,
    "embed_after"     => 1,
    "sitemap"         => 1,
    "login"           => 1,
    "idtype"          => 1,
    "favicon"         => 1,
    "loai"            => 1,
    "thutu"           => 1,
    "ten"             => 1,
    "anhien"          => 1,
    "action"          => 1,
    "nutdathang"      => 1,
    "binhluanwebsite" => 1,
    "cache"           => 1,
    "backtotop"       => 1,
    "add_item"        => 1,
    "errorreporting"  => 1,
    "smooth_scroll"   => 1,
    "date"            => 1,
    "path_img"        => "../uploads/config/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300,
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$issupperadmin  = 0;
if (isset($_SESSION['user_supperadmin']) && $_SESSION['user_supperadmin'] == 1) {
    $issupperadmin = 1;
}
switch ($__getmethod) {
    case 'query':
        include $__getop . "_query.php";
        break;
    case 'frm':
        include $__getop . "_frm.php";
        break;
    case 'taositemap':
        include $__getop . "_taositemap.php";
        break;
    default:
        include $__getop . "_frm.php";
        break;
}
