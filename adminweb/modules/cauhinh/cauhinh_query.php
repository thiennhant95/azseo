<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
if (!empty($_SESSION['configerror'])) {
    unset($_SESSION['configerror']);
    $_SESSION['configerror'] = '';
} else {
    $_SESSION['configerror'] = '';
}
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    # ---------------------
    # ẨN MỤC TIN
    # ---------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    # ------------------
    # HIỂN THỊ MỤC TIN
    # ------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    # -----------------
    # LƯU THỨ TỰ MỤC TIN
    # -----------------
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "UPDATE {$__table} SET thutu = {$value_order} WHERE id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') {
    # ---------------------
    # XÓA DỮ LIỆU
    # ---------------------
    if ($__xoa == 0) {
        echo '<script language="javascript">
        alert("' . $arraybien['khongcoquyenxoa'] . '");
        location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        // update thutu
        $s_update_thutu = "UPDATE {$__table} SET thutu = thutu-1 WHERE thutu > $thutu_value ";
        $d_update_thutu = $db->rawQuery($s_update_thutu);
        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddanhmuc  = '" . $id_order . "' ");
        }
    }
}
if ($__getaction == 'save') {
    # ----------------------
    # LƯU TRỮ DỮ LIỆU
    # ----------------------
    $array_data        = get_data($__table, '*', '');
    $id                = max_number_in_array($array_data, "id");
    $id                = $id + 1;
    $array_data        = get_data($__config['table'], '*', 'id,=,' . $__postid . '');
    $data              = $array_data[0];
    $stop_website      = ws_post('stop_website');
    $thongbao_stop     = ws_post('thongbao_stop');
    $soluongsanpham    = ws_post('soluongsanpham');
    $soluongtin        = ws_post('soluongtin');
    $binhluanwebsite   = ws_post('binhluanwebsite');
    $website           = ws_post('website');
    $manhungdau        = ws_post('manhungdau');
    $manhungcuoi       = ws_post('manhungcuoi');
    $backtotop         = ws_post('backtotop') ? ws_post('backtotop') : $data['backtotop'];
    $sendemail         = ws_post('sendemail') ? ws_post('sendemail') : $data['sendemail'];
    $hostmail          = ws_post('hostmail') ? ws_post('hostmail') : $data['hostmail'];
    $hostmail_user     = ws_post('hostmail_user') ? ws_post('hostmail_user') : $data['hostmail_user'];
    $hostmail_port     = ws_post('hostmail_port') ? ws_post('hostmail_port') : $data['hostmail_port'];
    $hostmail_ssl      = ws_post('hostmail_ssl') ? ws_post('hostmail_ssl') : $data['hostmail_ssl'];
    $hostmail_fullname = ws_post('hostmail_fullname') ? ws_post('hostmail_fullname') : $data['hostmail_fullname'];
    $email_nhan        = ws_post('email_nhan') ? ws_post('email_nhan') : $data['email_nhan'];
    $analytics         = ws_post('analytics') ? ws_post('analytics') : $data['analytics'];
    $webmaster         = ws_post('webmaster') ? ws_post('webmaster') : $data['webmaster'];
    $appfacebookid     = ws_post('appfacebookid') ? ws_post('appfacebookid') : $data['appfacebookid'];
    $appgoogleid       = ws_post('appgoogleid') ? ws_post('appgoogleid') : $data['appgoogleid'];
    $smoothScroll      = ws_post('smooth_scroll') ? ws_post('smooth_scroll') : $data['smooth_scroll'];
    $fileallowed       = ws_post('fileallowed');
    $favicon           = $_FILES['favicon']['name'];
    $__postid          = ws_post('id');
    $rebuild_xml       = 0;

    $group_color       = ws_post('group_color') ? ws_post('group_color') : $data['group_color'];
    $group_email       = ws_post('group_email') ? ws_post('group_email') : $data['group_email'];
    $group_social      = ws_post('group_social') ? ws_post('group_social') : $data['group_social'];
    $group_feature     = ws_post('group_feature') ? ws_post('group_feature') : $data['group_feature'];
    $group_embed       = ws_post('group_embed') ? ws_post('group_embed') : $data['group_embed'];

    $bgtop    = ws_post('opacitytop') ? $db->hex2rgba(ws_post('bgtop'), "0.".ws_post('opacitytop')) : ws_post('bgtop');
    $bgmenu   = ws_post('opacitymenu') ? $db->hex2rgba(ws_post('bgmenu'), "0.".ws_post('opacitymenu')) : ws_post('bgmenu');
    $bgmain   = ws_post('opacitymain') ? $db->hex2rgba(ws_post('bgmain'), "0.".ws_post('opacitymain')) : ws_post('bgmain');
    $bgbottom = ws_post('opacitybottom') ? $db->hex2rgba(ws_post('bgbottom'), "0.".ws_post('opacitybottom')) : ws_post('bgbottom');
    $bgdesign = ws_post('opacitydesign') ? $db->hex2rgba(ws_post('bgdesign'), "0.".ws_post('opacitydesign')) : ws_post('bgdesign');
    $bghover = ws_post('opacityhover') ? $db->hex2rgba(ws_post('bghover'), "0.".ws_post('opacityhover')) : ws_post('bghover');
    $bgtitle = ws_post('opacitytitle') ? $db->hex2rgba(ws_post('bgtitle'), "0.".ws_post('opacitytitle')) : ws_post('bgtitle');

    $colortop    = ws_post('colortop') ? ws_post('colortop') : null;
    $colormenu   = ws_post('colormenu') ? ws_post('colormenu') : null;
    $colormain   = ws_post('colormain') ? ws_post('colormain') : null;
    $colorbottom = ws_post('colorbottom') ? ws_post('colorbottom') : null;
    $colordesign = ws_post('colordesign') ? ws_post('colordesign') : null;
    $colorhover  = ws_post('colorhover') ? ws_post('colorhover') : null;
    $colortitle  = ws_post('colortitle') ? ws_post('colortitle') : null;

    $domain   = ws_post('domain');
    $glid     = ws_post('glid');
    $glsecret = ws_post('glsecret');
    $gldev    = ws_post('gldev');
    $fbid     = ws_post('fbid');
    $fbsecret = ws_post('fbsecret');

    if ( ws_post('off_bgtop') ) {
        if ( ws_post('off_bgtop') ) {
            $bgtop = '';
        }
    }
    if ( ws_post('off_bgmenu') ) {
        if ( ws_post('off_bgmenu') ) {
            $bgmenu = '';
        }
    }
    if ( ws_post('off_bgmain') ) {
        if ( ws_post('off_bgmain') ) {
            $bgmain = '';
        }
    }
    if ( ws_post('off_bgbottom') ) {
        if ( ws_post('off_bgbottom') ) {
            $bgbottom = '';
        }
    }
    if ( ws_post('off_bgdesign') ) {
        if ( ws_post('off_bgdesign') ) {
            $bgdesign = '';
        }
    }
    if ( ws_post('off_bghover') ) {
        if ( ws_post('off_bghover') ) {
            $bghover = '';
        }
    }
    if ( ws_post('off_bgtitle') ) {
        if ( ws_post('off_bgtitle') ) {
            $bgtitle = '';
        }
    }

    $manhungdau = str_replace("\'","'",$manhungdau);
    $manhungcuoi = str_replace("\'","'",$manhungcuoi);

    # ----------------------
    # XỬ LÝ NHỮNG FEILD NÀO CÓ CHỈNH SỬA THÌ MỚI CẬP NHẬT
    # ----------------------

    # ----------------------
    # START XỬ LÝ DÀNH RIÊNG CHO SUPPERADMIN
    # ----------------------
    if ( $issupperadmin ) {
        $soluotxem      = ((ws_post('soluotxem')) ? trim(ws_post('soluotxem')) : trim(ws_post('soluotxem')));
        $ngaytontaichua = $db->getNameFromID("tbl_counter", "solanxem", "ngay", "'" . $db->getDate() . "'");
        if ($ngaytontaichua != "") {
            $arrluotxem = array(
                'ngay'     => $db->getDate(),
                'solanxem' => $soluotxem,
            );
            $db->sqlUpdate("tbl_counter", $arrluotxem, " ngay = '" . $db->getDate() . "' ");
        } else {
            $arrluotxem = array(
                'ngay'     => $db->getDate(),
                'solanxem' => $soluotxem,
            );
            $db->insert("tbl_counter", $arrluotxem);
        } // ./LuotXem

        $rebuild_xml = (ws_post('rebuild_xml') ? ws_post('rebuild_xml') : 0);
    }//if ($issupperadmin)
    # ----------------------
    # END XỬ LÝ DÀNH RIÊNG CHO SUPPERADMIN
    # ----------------------

    // Báo lỗi web
    if (ws_post('baoloiweb') && trim(ws_post('baoloiweb')) != $data['baoloiweb']) {
        $baoloiweb = trim(ws_post('baoloiweb'));
    } else {
        $baoloiweb = $data['baoloiweb'];
    }
    // Nút đặt hàng
    if (ws_post('nutdathang') && trim(ws_post('nutdathang')) != $data['nutdathang']) {
        $nutdathang = trim(ws_post('nutdathang'));
    } else {
        $nutdathang = $data['nutdathang'];
    }
    // Mật khẩu hostmail
     if (trim(ws_post('hostmail_pass'))!='') {
        $hostmail_pass = trim(ws_post('hostmail_pass'));
    } else {
        $hostmail_pass = $data['hostmail_pass'];
    }

    //Hiển thị vơi
    $viewfor = !empty(ws_post('viewfor')) ? trim(ws_post('viewfor')) : $data['viewfor'];
    #---------------
    # Upload Favicon
    #---------------
    // TRƯỜNG HỢP NGƯỜI DÙNG CHỌN 1 FILE ĐỂ UPLOAD
    if (!empty($favicon)) {
        // những định dạng cho phép làm favicon
        $allowTypeFile = array('png' ,'jpg', 'ico');
        // lấy định dạng file
        $ext = pathinfo($favicon, PATHINFO_EXTENSION);
        // Kiểm tra xem định dạng cho đc phép ko?
        if(in_array($ext, $allowTypeFile) ) {
            // Kiểm tra chiều dài & chiều rộng của file favicon
            list($favicon_width, $favicon_height) = getimagesize($_FILES['favicon']['tmp_name']);
            if ($favicon_width == "32" && $favicon_height == "32") {
                $favicon_name = str_replace(" ", "-", $favicon);
                move_uploaded_file($_FILES['favicon']['tmp_name'], $__config['path_img'].$favicon_name);
            } else {
                $favicon_name = $data['favicon'];
                $_SESSION['configerror'] .= "<p>favicon 32x32 !!</p>";
            }
        } else {
            $favicon_name = $data['favicon'];
            $_SESSION['configerror'] .= '<p>favicon chỉ chấp nhận đuôi là: png, jpg, ico</p>';
        }
    } else {
        // TRƯỜNG HỢP KO CHỌN ẢNH UPLOAD THÌ LẤY ẢNH TRONG XML
        if (!empty(ws_post('favicon')) && trim(ws_post('favicon')) != $data['favicon']) {
            // xoa icon old
            if (file_exists($__config['path_img'].$data['favicon'])) {
                unlink($__config['path_img'].$data['favicon']);
            }
            $favicon_name = ws_post('favicon');
        } else {
            $favicon_name = $data['favicon'];
        }
    }

/**============================================================================
@ KHAI BAO MANG */
$arrFieldName = array(
    'id',
    'viewfor',
    'baoloiweb',
    'nutdathang',
    'stop_website',
    'thongbao_stop',
    'soluongsanpham',
    'soluongtin',
    'backtotop',
    'binhluanwebsite',
    'manhungdau',
    'manhungcuoi',
    'sendemail',
    'hostmail',
    'hostmail_user',
    'hostmail_pass',
    'hostmail_port',
    'hostmail_ssl',
    'hostmail_fullname',
    'email_nhan',
    'analytics',
    'webmaster',
    'appfacebookid',
    'appgoogleid',
    'website',
    'favicon',
    'smooth_scroll',
    'colortop',
    'colormenu',
    'colormain',
    'colorbottom',
    'colordesign',
    'colorhover',
    'colortitle',
    'bgtop',
    'bgmenu',
    'bgmain',
    'bgbottom',
    'bgdesign',
    'bghover',
    'bgtitle',
    'group_color',
    'group_email',
    'group_social',
    'group_feature',
    'group_embed',
    'off_bgtop',
    'off_bgmenu',
    'off_bgmain',
    'off_bgbottom',
    'off_bgdesign',
    'off_bghover',
    'off_bgtitle',
    'domain',
    'glid',
    'glsecret',
    'gldev',
    'fbid',
    'fbsecret',
);
$arrFieldValue = array(
    $viewfor,
    $baoloiweb,
    $nutdathang,
    $stop_website,
    $thongbao_stop,
    $soluongsanpham,
    $soluongtin,
    $backtotop,
    $binhluanwebsite,
    $manhungdau,
    $manhungcuoi,
    $sendemail,
    $hostmail,
    $hostmail_user,
    $hostmail_pass,
    $hostmail_port,
    $hostmail_ssl,
    $hostmail_fullname,
    $email_nhan,
    $analytics,
    $webmaster,
    $appfacebookid,
    $appgoogleid,
    $website,
    $favicon_name,
    $smoothScroll,
    $colortop,
    $colormenu,
    $colormain,
    $colorbottom,
    $colordesign,
    $colorhover,
    $colortitle,
    $bgtop,
    $bgmenu,
    $bgmain,
    $bgbottom,
    $bgdesign,
    $bghover,
    $bgtitle,
    $group_color,
    $group_email,
    $group_social,
    $group_feature,
    $group_embed,
    ws_post('off_bgtop'),
    ws_post('off_bgmenu'),
    ws_post('off_bgmain'),
    ws_post('off_bgbottom'),
    ws_post('off_bgdesign'),
    ws_post('off_bghover'),
    ws_post('off_bgtitle'),
    $domain,
    $glid,
    $glsecret,
    $gldev,
    $fbid,
    $fbsecret,
);

# Trường hợp thêm mới hoặc tạo tại xml
if ( !$__getid || $rebuild_xml) {
    array_unshift($arrFieldValue, 1);
} else {
    # trường hợp cập nhật
    array_unshift($arrFieldValue, $__postid);
}
    # ----------------------
    # THÊM MỚI DỮ LIỆU XML
    # ----------------------
    if ($__getid == '') {
        if ( !file_exists($folder_xml . $__config['table'] . '.xml') ) {
            # NẾU FILE XML CHƯA TỒN TẠI
            // Tạo bảng trong XML
            create_table($__config['table'], $arrFieldName);
            //Chèn dữ liệu lần đầu
            add_data($__config['table'], $arrFieldValue);
        } // End Tạo bảng
    } else {
        # --------------------------------------------------
        # CẬP NHẬT LẠI DỮ LIỆU XML
        # --------------------------------------------------
        if ($rebuild_xml == 1) {
            # NẾU CHỌN TẠO LẠI FILE XML
            // Tạo bảng trong XML
            create_table($__config['table'], $arrFieldName);
            //Chèn dữ liệu lần đầu
            add_data($__config['table'], $arrFieldValue);
        } else {
            # KHÔNG CHỌN TẠO XML MÀ CHỈ THAY ĐỔI DỮ LIỆU
            update_data($__table, array('id', $__postid), $arrFieldValue);
        }
    } // Ket thuc kiem tra
}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') == 'luuvathem') {
    $id = (!empty($id_insert) ? $id_insert : '');
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
//http://sinhvienit.net/forum/php-code-doc-xoa-cap-nhat-them-vao-csdl-xml.24965.html
