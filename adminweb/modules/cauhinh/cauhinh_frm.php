<?php
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if (!empty($__getid)) {
    // Process Chỉnh Sửa
    if (!$__sua) {
        echo '
        <script language="javascript">
            alert("' . $arraybien['khongcoquyensua'] . '");
            location.href="./?op=' . $__getop . '";
        </script>';
        exit;
    }
} else {
    // Process Thêm mới
    if (!$__them) {
        echo '
        <script language="javascript">
            alert("' . $arraybien['khongcoquyenthem'] . '");
            location.href="./?op=' . $__getop . '";
        </script>';
        exit;
    }
}
/*CREATE FOLDER IF NOT EXISTS*/
create_dir($__config['path_img']);
create_dir($__config['path_file'], false);

$_id_name = (isset($__config['id'])) ? $__config['id'] : null;
$_getop   = ws_get('op');
$_idtype  = (isset($__config['type'])) ? $__config['type'] : null;
$_idSP    = (isset($__config['idsp'])) ? $__config['idsp'] : null;
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
} else {
    $_getidSP = 1;
}
#------------------------------------------------------------------
# LẤY DỮ LIỆU TRONG XML RA NGOÀI FORM
#------------------------------------------------------------------
$array_data        = get_data($__config['table'], '*', 'id,=,' . $_getidSP . '');
$data              = $array_data[0];
$id                = @$data['id'];
$stop_website      = @$data['stop_website'];
$thongbao_stop     = @$data['thongbao_stop'];
$soluongsanpham    = @$data['soluongsanpham'];
$soluongtin        = @$data['soluongtin'];
$sendemail         = @$data['sendemail'];
$hostmail          = @$data['hostmail'];
$hostmail_user     = @$data['hostmail_user'];
$hostmail_port     = @$data['hostmail_port'];
$hostmail_ssl      = @$data['hostmail_ssl'];
$hostmail_fullname = @$data['hostmail_fullname'];
$email_nhan        = @$data['email_nhan'];
$baoloiweb         = @$data['baoloiweb'];
$nutdathang        = @$data['nutdathang'];
$backtotop         = @$data['backtotop'];
$binhluanwebsite   = @$data['binhluanwebsite'];
$website           = @$data['website'];
$analytics         = @$data['analytics'];
$webmaster         = @$data['webmaster'];
$appfacebookid     = @$data['appfacebookid'];
$appgoogleid       = @$data['appgoogleid'];
$manhungdau        = @$data['manhungdau'];
$manhungcuoi       = @$data['manhungcuoi'];
$favicon           = @$data['favicon'];
$fileallowed       = @$data['fileallowed'];
$viewfor           = @$data['viewfor'];
$smoothScroll      = @$data['smooth_scroll'];

$bgtop        = @$data['bgtop'];
$bgmenu       = @$data['bgmenu'];
$bgmain       = @$data['bgmain'];
$bgbottom     = @$data['bgbottom'];
$bgdesign     = @$data['bgdesign'];
$bghover      = @$data['bghover'];
$bgtitle      = @$data['bgtitle'];

$off_bgtop    = @$data['off_bgtop'];
$off_bgmenu   = @$data['off_bgmenu'];
$off_bgmain   = @$data['off_bgmain'];
$off_bgbottom = @$data['off_bgbottom'];
$off_bgdesign = @$data['off_bgdesign'];
$off_bghover  = @$data['off_bghover'];
$off_bgtitle  = @$data['off_bgtitle'];

$group_color       = @$data['group_color'];
$group_email       = @$data['group_email'];
$group_social      = @$data['group_social'];
$group_feature     = @$data['group_feature'];
$group_embed       = @$data['group_embed'];

$domain   = @$data['domain'];
$glid     = @$data['glid'];
$glsecret = @$data['glsecret'];
$gldev    = @$data['gldev'];
$fbid     = @$data['fbid'];
$fbsecret = @$data['fbsecret'];

echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   ';
// Hoàn thành validate ==> Submit frm
echo '
   else {
    var form = document.adminForm;
       // do field validation
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="luucauhinh()" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//********** col left **********//
// Export error if exists
if (!empty($_SESSION['configerror'])) {
    $CONTENT.='
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        '.$_SESSION['configerror'].'
    </div>';
}
/**===================================================================
@ GIAO DIỆN CẤU HÌNH MỚI*/
$CONTENT .= '
<form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . $id . '" method="post" enctype="multipart/form-data" class="clearfix" name="adminForm" id="adminForm">

<div class="config-container clearfix">
    <div class="config-content clearfix">
        <div class="config-row expand">

    <div class="config-L config-item clearfix">

    <div class="row-menu config-padding">
        <h3 class="h3 menu">
            CẤU HÌNH
        </h3>
        <!-- .h3 -->
        <div class="extract" id="collapseConfig">
            <i class="fa fa-bars"></i>
        </div>
        <!-- .collapse -->
    </div>
    <!-- .row-menu -->

    <div class="config-box clearfix">

        <div class="left-item row-general clearfix '.(@$_SESSION['active_config']=='general'?'active':null).'" data-name="general">
            <h4 class="row-title">
                <i class="fa fa-bank"></i>
                <span>
                    <a href="#general" class="config-click" title="general">
                        Tổng quan
                    </a>
                </span>
            </h4>
            <!-- .row-title -->
        </div>
        <!-- .left-item row-general clearfix -->';
if ( $group_email == "on" ) {
$CONTENT.='
<div class="left-item row-mail clearfix '.(@$_SESSION['active_config']=='email'?'active':null).'" data-name="email">
    <h4 class="row-title">
        <i class="fa fa-envelope"></i>
        <span>
            <a href="#email" class="config-click" title="email">
                Email
            </a>
        </span>
    </h4>
    <!-- .row-title -->
</div>
<!-- .left-item row-mail clearfix -->
';
}

if ( $group_color == "on" ) {
$CONTENT.='
<div class="left-item row-color clearfix '.(@$_SESSION['active_config']=='color'?'active':null).'" data-name="color">
    <h4 class="row-title">
        <i class="fa fa-paint-brush"></i>
        <span>
            <a href="#color" class="config-click" title="color">
                Màu sắc
            </a>
        </span>
    </h4>
    <!-- .row-title -->
</div>
<!-- .left-item row-color clearfix -->
';
}

if ( $group_social == "on" ) {
$CONTENT.='
<div class="left-item row-social clearfix '.(@$_SESSION['active_config']=='social'?'active':null).'" data-name="social">
    <h4 class="row-title">
        <i class="fa fa-facebook-square"></i>
        <span>
            <a href="#social" class="config-click" title="social">
                Mạng xã hội
            </a>
        </span>
    </h4>
    <!-- .row-title -->
</div>
<!-- .left-item row-social clearfix -->';
}

if ( $group_feature == "on" ) {
$CONTENT.='
<div class="left-item row-feature clearfix '.(@$_SESSION['active_config']=='feature'?'active':null).'" data-name="feature">
    <h4 class="row-title">
        <i class="fa fa-sun-o"></i>
        <span>
            <a href="#feature" class="config-click" title="feature">
                Tiện ích
            </a>
        </span>
    </h4>
    <!-- .row-title -->
</div>
<!-- .left-item row-feature clearfix -->';
}

if ( $group_embed == "on" ) {
$CONTENT.='
<div class="left-item row-embed clearfix '.(@$_SESSION['active_config']=='embed'?'active':null).'" data-name="embed">
    <h4 class="row-title">
        <i class="fa fa-empire"></i>
        <span>
            <a href="#embed" class="config-click" title="embed">
                Mã nhúng
            </a>
        </span>
    </h4>
    <!-- .row-title -->
</div>
<!-- .left-item row-embed clearfix -->';
}

        if ( $issupperadmin ) {
            $CONTENT.='
            <div class="left-item row-configsadmin clearfix '.(@$_SESSION['active_config']=='configsadmin'?'active':null).'" data-name="configsadmin">
                <h4 class="row-title">
                    <i class="fa fa-user-secret"></i>
                    <span>
                        <a href="#configsadmin" class="config-click" title="configsadmin">
                            S-ADMIN
                        </a>
                    </span>
                </h4>
                <!-- .row-title -->
            </div>
            <!-- .left-item row-configsadmin clearfix -->
            ';
        }$CONTENT.='

    </div>
    <!-- .config-box clearfix -->

</div>
<!-- ./config-L -->

<div class="config-R config-item clearfix">

    <div class="right-item '.(@$_SESSION['active_config']=='general'?'active':null).'" id="general">
        <div class="right-title config-padding">
            <h3 class="menu">
                CẤU HÌNH TỔNG QUAN
            </h3>
            <!-- .h3 -->
        </div>
        <!-- .right-title -->
        <div class="right-box clearfix config-padding">
        <div class="panel-body clearfix">';
/**===========================================
@ TẠM DỪNG WEBSITE */
$CONTENT.='
<div class="row general-row1 clearfix">
';
if ( $__config['stop_website'] ) {
    $CONTENT.='
    <div class="col-xs-12 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong class="h4">
                    ' . $arraybien['dongwebsite'] . '
                </strong>
            </div>
            <!-- .panel-heading -->
            <div class="panel-body radios">
                <label>
                    <input type="radio" name="stop_website" value="on" '.($stop_website=="on"?"checked":null).' />
                     ' . $arraybien['khoa'] . '
                </label>

                <label class="form-group">
                    <input type="radio" name="stop_website" value="off" '.($stop_website=="off"?"checked":null).' />
                     ' . $arraybien['mo'] . '
                </label>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group row-no-padding clearfix" id="thongbao_stop">
                           <label for="textarea" class="col-sm-12 control-label">
                                ' . $arraybien['thongbaodongwebsite'] . ':
                            </label>
                           <div class="col-sm-10">
                              <textarea name="thongbao_stop" id="textarea" class="form-control" rows="3" placeholder="Nhập nội dung mà bạn muốn thông báo đến người dùng">' . $thongbao_stop . '</textarea>
                           </div>
                        </div><!-- ./thongbao_stop -->
                    </div>
                    <!-- .col-xs-12 -->
                </div>
                <!-- .row -->
            </div>
            <!-- ./panel-body radios -->
        </div>
        <!-- ./panel panel-default -->
    </div>
    <!-- .col-xs-12 col-sm-6 -->
';
}
/** Favicon */
if ( $__config['favicon'] ) {
    $CONTENT .= '
<div class="col-xs-12 col-sm-6 clearfix">
    <div class="panel panel-default">
        <div class="panel-heading">';
        if (!empty($favicon)) {
            if (substr($favicon, 0, 2) == 'fa' && strlen($favicon) <= 8) {
                $faviconshow = $__config['path_img'].'lib_favicon/'.$favicon;
            } else {
                $faviconshow = $__config['path_img'].$favicon;
            }
        }$CONTENT.='
            <strong class="h4 title-faicon">Icon Website (favicon)</strong>
            <img src="'.$faviconshow.'" alt="'.$favicon.'" title="'.$favicon.'" class="img-responsive img-faicon" />
        </div>
        <!-- .panel-heading -->
        <div class="panel-body">
            <div class="input-group">
                <input type="file" class="form-control" name="favicon" '.(!empty($favicon)?'value="'.$favicon.'"':null).' />
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default">
                        <a data-toggle="modal" href=\'#modal-favicon\'>
                            <i class="fa fa-hand-o-up fa-lg"></i> chọn sẵn
                        </a>
                    </button>
                </div>
            </div>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel -->
</div>
<!-- .col-xs-12 col-sm-6 clearfix -->';
}
$CONTENT.='
</div>
<!-- .row .general-row1 -->
';

/**==================================================
@ #General row 2*/
$CONTENT.='
<div class="row general-row2 clearfix">
';
if ( $__config['website'] ) {
$CONTENT.='
    <div class="col-xs-12 col-sm-6 clearfix">
        <div class="panel panel-default panel-website">
            <div class="panel-heading">
                <strong class="h4">
                    Trang chính
                </strong>
            </div>
            <!-- .panel-heading -->

            <div class="panel-body">
                <input type="text" maxlength="100" class="form-control" name="website" value="' . $website . '" placeholder="Nhập website mà bạn muốn luôn chuyển hướng tới" />
               <br />
               <p class="lab" style="font-weight:bold">Tất cả các tên miền trỏ về sẽ tự động chuyển hướng theo link này</p>
            </div>
            <!-- .panel-body -->
        </div>
        <!-- .panel panel-default panel-website -->
    </div>
    <!-- .col-xs-12 col-sm-6 clearfix -->
';
}

/**=============================================================
@ TONG SAN PHAM VS TONG TIN TUC*/
if ( $__config['soluongsanpham'] ||  $__config['soluongtin']) {
$CONTENT.='
<div class="col-xs-12 col-sm-6 clearfix sumTinTucSanPham">

<div class="panel panel-default panel-tintucsanpham">
<div class="panel-heading">
    <strong class="h4">Tin tức & Sản phẩm</strong>
</div>
<!-- .panel-heading -->
<div class="panel-body">
';
/**============================================================
@ Số lượng sản phẩm*/
if ( $__config['soluongsanpham'] ) {
    $CONTENT.='
    <div class="form-group soluongsanpham">
       <div class="input-group">
          <div class="input-group-addon">Tổng sản phẩm / 1 trang</div>
          <input type="text" maxlength="2" class="form-control" name="soluongsanpham" value="' . $soluongsanpham . '" onkeyup="CheckNumber(this)" placeholder="nhập số sản phẩm hiện thị trên 1 trang" />
       </div>
    </div><!-- ./soluongsanpham -->
    ';
}
/**============================================================
@ Số lượng Tin tức*/
if ( $__config['soluongtin'] ) {
    $CONTENT.='
    <div class="soluongtin">
       <div class="input-group">
          <div class="input-group-addon">Tổng tin tức / 1 trang</div>
          <input type="text" maxlength="2" class="form-control" name="soluongtin" value="' . $soluongtin . '" onkeyup="CheckNumber(this)" placeholder="nhập số nội dung hiện thị trên 1 trang" />
       </div>
    </div><!-- ./soluongtin -->
    ';
}
$CONTENT.='
            </div>
            <!-- .panel-body -->
        </div>
        <!-- .panel panel-tintucsanpham -->
    </div>
    <!-- .col-xs-12 col-sm-6 clearfix sumTinTucSanPham -->
';
}
$CONTENT.='
    </div>
    <!-- .row general-row2 -->
';

$CONTENT.='
<div class="row general-row3 clearfix">';
/**=====================================
@ Hien thi binh luan */
if ( $__config['binhluanwebsite'] ) {
    $CONTENT .= '
<div class="col-xs-12 col-sm-6 clearfix">
    <div class="panel panel-default panel-website">
        <div class="panel-heading">
            <strong class="h4">
                Bình luận website
            </strong>
        </div>
        <!-- .panel-heading -->

        <div class="panel-body radios">
            <label><input type="radio" name="binhluanwebsite" value="on" '.($binhluanwebsite=="on"?"checked":null).'>Hiện</label>
            <label><input type="radio" name="binhluanwebsite" value="off" '.($binhluanwebsite=="off"?"checked":null).'>Ẩn</label>
        </div>
        <!-- ./panel-body -->
    </div>
    <!-- ./panel -->
    </div>
    <!-- .col-xs-12 col-sm-6 clearfix -->
';
}

/**=============================================
@ SITE MAP*/
if ( $__config['sitemap'] ) {
$CONTENT.='
<div class="col-xs-12 col-sm-6 clearfix">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">
            ' . $arraybien['taositemap'] . '
            </strong>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div id="sitemapNotifi" class="alert alert-success" style="display: none;">
                <strong></strong>
            </div> <!-- #sitemapNotifi -->
            <h5><p>
                <label class="label label-info">Chú ý:</label>
                mục thời gian này tùy vào nội dung trên website bạn có thay đổi thường xuyên hay không
                -- <a href="../sitemap.xml" target="_blank" title="">Xem</a> </p>
            </h5>
            <div id="taositemap">
                <div class="input-group">
                    <div class="input-group-addon">Thời gian</div>
                    <select name="thoigianquet" id="thoigianquet" class="form-control">
                        <option value="hourly">Hàng giờ</option>
                        <option value="daily">Hàng ngày</option>
                        <option value="weekly">Hàng tuần</option>
                        <option value="monthly">Hàng Tháng</option>
                        <option value="yearly">Hàng năm</option>
                        <option value="never">Không bao giờ</option>
                    </select>
                    <div class="input-group-btn">
                        <input type="button" id="createSitemap" class="form-control"  name="" value="Tao sitemap">
                    </div>
                </div>
            </div>
          </div>
          <!-- /.panel-body -->
    </div>
    <!-- ./panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->';
}
$CONTENT.='
</div>
<!-- .row general-row3 clearfix -->
';

$CONTENT .= '
    </div>
    <!-- .panel-body -->
    </div>
    <!-- .right-box clearfix -->
</div>
<!-- #general.right-item active -->';

if ( $group_email == "on" ) {
$CONTENT.='
<div class="right-item '.(@$_SESSION['active_config']=='email'?'active':null).'" id="email">
    <div class="right-title config-padding">
        <h3 class="menu">
            CẤU HÌNH EMAIL
        </h3>
        <!-- .h3 -->
    </div>
    <!-- .right-title -->

    <div class="right-box clearfix config-padding">

    <div class="panel-body box-thietlapmail">

        <div class="form-group sendemail">
            <div class="input-group">
                <div class="input-group-addon">Kích hoạt</div>
                <div class="group-active">
                    <label>
                        <input type="radio"  name="sendemail" value="on" '.($sendemail=="on"?"checked":null) . ' />
                    ' . $arraybien['co'] . '
                    </label>
                    <label>
                        <input type="radio" name="sendemail" value="off" '.($sendemail=="off"?"checked":null).' />
                    ' . $arraybien['khong'] . '
                    </label>
                </div>
                <!-- .group-active -->
            </div>
            <!-- ./input-group -->
        </div>
        <!-- ./sendemail -->

        <div id="body_setup_email">
            <div class="form-group" class="email_nhan">
                <div class="input-group">
                    <div class="input-group-addon">Email nhận</div>
                    <input type="text" name="email_nhan" class="form-control"  value="' . $email_nhan . '" placeholder="Các email cách nhau bởi dấu phẩy (email1,email2,...)" />
                </div>
            </div>
            <!-- ./email_nhan -->

           <div class="form-group" class="hostmail_fullname">
              <div class="input-group">
                <div class="input-group-addon">Tiêu đề Email</div>
                <input type="text" class="form-control" name="hostmail_fullname" value="' . $hostmail_fullname . '" placeholder="Nhập tiêu đề của Email" />
              </div>
           </div>
           <!-- ./hostmail_fullname -->

            <div class="form-group" class="hostmail">
                <div class="input-group">
                    <div class="input-group-addon">Hostmail</div>
                    <input type="text" class="form-control" name="hostmail" value="' . $hostmail . '" placeholder="Địa chỉ của máy chủ mail (m.hotmail.com)" />
                </div>
            </div>
            <!-- ./hostmail -->

            <div class="form-group" class="hostmail_user">
                <div class="input-group">
                    <div class="input-group-addon">Hostmail User</div>
                    <input type="text" class="form-control" name="hostmail_user" value="' . $hostmail_user . '" placeholder="tài khoản dùng để gửi mail" />
                </div>
            </div>
            <!-- ./hostmail_user -->

            <div class="form-group" id="hostmail_pass" class="hostmail_pass">
                <div class="input-group">
                    <div class="input-group-addon">Hostmail Pass</div>
                    <input type="password" class="form-control" name="hostmail_pass" value="" placeholder="Nhập mật khẩu mới nếu có sự thay đổi" />
                    <span class="input-group-btn">
                        <button type="button" id="viewHostmail" class="btn btn-default">
                        <i class="fa fa-eye"></i>
                        </button>
                    </span>
                </div>
            </div>
            <!-- ./hostmail_pass -->

           <div class="form-group" class="hostmail_port">
                <div class="input-group">
                    <div class="input-group-addon">Port</div>
                    <input type="text" name="hostmail_port" value="' . $hostmail_port . '" class="form-control" placeholder="Port của máy chủ mail (465)">
                </div>
           </div>
           <!-- ./hostmail_port -->

            <div class="form-group" class="hostmail_ssl">
                <div class="input-group">
                    <div class="input-group-addon">SSL</div>
                    <div class="group-active">
                        <label>
                            <input type="radio" name="hostmail_ssl" value="on" '.($hostmail_ssl=="on"?"checked":null) . ' />
                            Bật
                        </label>
                        <label>
                            <input type="radio" name="hostmail_ssl" value="off" '.($hostmail_ssl=="off"?"checked":null).' />
                            Tắt
                        </label>
                    </div>
                </div>
            </div>
            <!-- ./hostmail_ssl -->

            <button type="button" data-loading-text="<i class=\'fa fa-spinner fa-spin \'></i> đang test..." class="btn btn-default" id="testMail">
                Test Mail
            </button>
        </div>
        <!-- #body_setup_email -->
    </div>
    <!-- ./box-thietlapmail -->

</div>
<!-- .right-box clearfix config-padding -->
</div>
<!-- #email.right-item -->';
}

if ( $group_color == "on" ) {
$CONTENT.='
<div class="right-item '.(@$_SESSION['active_config']=='color'?'active':null).'" id="color">
<div class="right-title clearfix config-padding">
    <h3 class="menu">
        CẤU HÌNH MÀU SẮC
    </h3>
    <!-- .h3 -->
</div>
<!-- .right-title -->

<div class="right-box clearfix config-padding">
    <div class="panel-body row">

<div class="color-item bgtop">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Top</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bgtop" value="1" class="configColorDisable" '.($off_bgtop?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bgtop" class="form-control col-3 jscolor '.($off_bgtop?'vshidden':null).'" value="'.$bgtop.'" />
        <input type="hidden" data-getcolor="1" name="colortop" value="" />

        <input type="text" name="opacitytop" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bgtop?'vshidden':null).'" value="'.$db->rgba2opacity($bgtop).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bgtop -->

<div class="color-item bgmenu">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Menu</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bgmenu" value="1" class="configColorDisable" '.($off_bgmenu?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bgmenu" class="form-control jscolor '.($off_bgmenu?'vshidden':null).'" value="'.$bgmenu.'" />
        <input type="hidden" data-getcolor="1" name="colormenu" value="" />
        <input type="text" name="opacitymenu" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bgmenu?'vshidden':null).'" value="'.$db->rgba2opacity($bgmenu).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bgmenu -->

<div class="color-item bgmain">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Main</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bgmain" value="1" class="configColorDisable" '.($off_bgmain?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bgmain" class="form-control jscolor '.($off_bgmain?'vshidden':null).'" value="'.$bgmain.'" />
        <input type="hidden" data-getcolor="1" name="colormain" value="" />
        <input type="text" name="opacitymain" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bgmain?'vshidden':null).'" value="'.$db->rgba2opacity($bgmain).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bgmain -->

<div class="color-item bgbottom">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Bottom</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bgbottom" value="1" class="configColorDisable" '.($off_bgbottom?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bgbottom" class="form-control jscolor '.($off_bgbottom?'vshidden':null).'" value="'.$bgbottom.'" />
        <input type="hidden" data-getcolor="1" name="colorbottom" value="" />
        <input type="text" name="opacitybottom" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bgbottom?'vshidden':null).'" value="'.$db->rgba2opacity($bgbottom).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bgbottom -->

<div class="color-item bgdesign">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Design</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bgdesign" value="1" class="configColorDisable" '.($off_bgdesign?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bgdesign" class="form-control jscolor '.($off_bgdesign?'vshidden':null).'" value="'.$bgdesign.'" />
        <input type="hidden" data-getcolor="1" name="colordesign" value="" />
        <input type="text" name="opacitydesign" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bgdesign?'vshidden':null).'" value="'.$db->rgba2opacity($bgdesign).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bgdesign -->

<div class="color-item bghover">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Hover</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bghover" value="1" class="configColorDisable" '.($off_bghover?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bghover" class="form-control jscolor '.($off_bghover?'vshidden':null).'" value="'.$bghover.'" />
        <input type="hidden" data-getcolor="1" name="colorhover" value="" />
        <input type="text" name="opacityhover" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bghover?'vshidden':null).'" value="'.$db->rgba2opacity($bghover).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bghover -->

<div class="color-item bgtitle">
    <div class="input-group">
        <div class="input-group-addon">
            <strong>Màu Nền Title</strong> |
            <label>
                <span class="label label-default">xóa màu</span>
                <input type="checkbox" name="off_bgtitle" value="1" class="configColorDisable" '.($off_bgtitle?'checked':null).' />
            </label>
        </div>
        <!-- .input-group-addon -->
        <input type="text" name="bgtitle" class="form-control jscolor '.($off_bgtitle?'vshidden':null).'" value="'.$bgtitle.'" />
        <input type="hidden" data-getcolor="1" name="colortitle" value="" />
        <input type="text" name="opacitytitle" maxlength="1" onkeyup="CheckNumber(this)" placeholder="Nhập độ trong suốt (1-9)" class="form-control '.($off_bgtitle?'vshidden':null).'" value="'.$db->rgba2opacity($bgtitle).'" />
    </div>
    <!-- .input-group -->
</div>
<!-- .form-group bgtitle -->

        </div>
        <!-- .panel-body -->
    </div>
    <!-- .right-box clearfix config-padding -->
</div>
<!-- #color.right-item -->';
}// END BOX COLOR

if ( $group_social == "on" ) {
$CONTENT.='
<div class="right-item '.(@$_SESSION['active_config']=='social'?'active':null).'" id="social">
    <div class="right-title config-padding">
        <h3 class="menu">
            CẤU HÌNH MẠNG XÃ HỘI
        </h3>
        <!-- .h3 -->
    </div>
    <!-- .right-title -->

    <div class="right-box clearfix config-padding">
        <div class="panel-body box-thietlapseo">

            <div class="form-group analytics">
                <div class="input-group">
                    <div class="input-group-addon">Google Analytics</div>
                    <input type="text" class="form-control" name="analytics" value="' . $analytics . '" placeholder="UA-51182xxx-xx" />
                </div>
            </div>
            <!-- ./analytics -->

            <div class="form-group webmaster">
                <div class="input-group">
                    <div class="input-group-addon">Google Webmaster</div>
                    <input type="text" class="form-control" name="webmaster" value="' . $webmaster . '" placeholder="xxxghTmKYSvLde05xxxxx" />
                </div>
            </div>
            <!-- ./webmaster -->

            <div class="form-group appfacebookid">
                <div class="input-group">
                    <div class="input-group-addon">Facebook AppID</div>
                    <input type="text" class="form-control" name="appfacebookid" value="' . $appfacebookid . '" placeholder="54263xxxxx" />
                    <div class="input-group-btn">
                        <a href="https://developers.facebook.com/apps/" title="TạoID" target="_blank">
                        <button type="button" class="btn btn-default">Get AppID</button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- ./appfacebookid -->

            <div class="form-group appgoogleid">
                <div class="input-group">
                    <div class="input-group-addon">Google AppID</div>
                    <input type="text" class="form-control" name="appgoogleid" value="' . $appgoogleid . '" placeholder="AIzavfsxxxx" />
                    <div class="input-group-btn">
                        <a href="https://console.developers.google.com/flows/enableapi?apiid=maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend,places_backend&keyType=CLIENT_SIDE&reusekey=true" title="CreateID" target="_blank">
                        <button type="button" class="btn btn-default">Get AppID</button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- ./appgoogleid -->

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        Domain
                    </span>
                    <input type="text" class="form-control" name="domain" value="'.$domain.'" placeholder="Nhập tên miền của bạn vào đây" />
                </div><!-- /.input-group -->
            </div><!-- /.form-group -->
            ';

            if( $__config['login'] ) {
                $CONTENT.='
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title h5 clearfix">
                                    <div class="pull-left">
                                        Đăng nhập bằng Google
                                    </div><!-- /.pull-left -->
                                    <div class="pull-right">
                                        <a href="https://console.developers.google.com/start" title="GetID" target="_blank" class="h6">
                                            GetID
                                        </a>
                                    </div><!-- /.pull-right -->
                                </div><!-- /.panel-title -->
                            </div><!-- /.panel-heading -->

                            <div class="panel-body">

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            Client ID
                                        </span><!-- /.input-group-addon -->
                                        <input type="text" class="form-control" name="glid" value="'.$glid.'" placeholder="xxxxi9e65e.apps.googleusercontent.com" />
                                    </div><!-- /.input-group -->
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            Client Secret
                                        </span><!-- /.input-group-addon -->
                                        <input type="text" class="form-control" name="glsecret" value="'.$glsecret.'" placeholder="xxxxYmIgVHha" />
                                    </div><!-- /.input-group -->
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            Developer Key
                                        </span><!-- /.input-group-addon -->
                                        <input type="text" class="form-control" name="gldev" value="'.$gldev.'" placeholder="xxxYa6_zX9xxx" />
                                    </div><!-- /.input-group -->
                                </div><!-- /.form-group -->

                            </div>
                            <!-- /.panel-body -->
                        </div><!-- /.panel panel-default -->
                    </div><!-- /.col-md-6 col-xs-12 -->

                    <div class="col-md-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <div class="panel-title h5 pull-left">
                                    Đăng nhập bằng Facebook
                                </div><!-- /.panel-title h5 -->
                                <div class="pull-right">
                                    <a href="https://developers.facebook.com" title="GetID" target="_blank" class="h6">
                                        GetID
                                    </a>
                                </div><!-- /.pull-right -->
                            </div><!-- /.panel-heading -->

                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            App ID
                                        </span><!-- /.input-group-addon -->
                                        <input type="text" name="fbid" value="'.$fbid.'" class="form-control" />
                                    </div><!-- /.input-group -->
                                </div><!-- /.input-group -->
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            App Secret
                                        </span><!-- /.input-group-addon -->
                                        <input type="text" name="fbsecret" value="'.$fbsecret.'" class="form-control" />
                                    </div><!-- /.input-group -->
                                </div><!-- /.input-group -->
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel panel-default -->
                    </div><!-- /.col-md-6 col-xs-12 -->
                </div><!-- /.row -->';
            }
        $CONTENT.='
        </div>
        <!-- .panel-body box-thietlapseo -->
    </div>
    <!-- .right-box clearfix config-padding -->

</div>
<!-- #social.right-item -->';
}// END BOX SOCIAL

if ( $group_feature == "on" ) {
$CONTENT.='
<div class="right-item '.(@$_SESSION['active_config']=='feature'?'active':null).'" id="feature">
    <div class="right-title config-padding">
        <h3 class="menu">
            CẤU HÌNH TIỆN ÍCH
        </h3>
        <!-- .h3 -->
    </div>
    <!-- .right-title -->

    <div class="right-box clearfix config-padding">
    <div class="panel-body">

<div class="row feature-row1">

<div class="col-xs-12 col-sm-6 clearfix backtotop">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">Nút lên đầu trang</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body radios">
            <label><input type="radio" name="backtotop" value="on" '.($backtotop=="on"?"checked":null) . '>Hiện</label>
            <label><input type="radio" name="backtotop" value="off" '.($backtotop=="off"?"checked":null).'>Ẩn</label>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 clearfix backtotop -->

<div class="col-xs-12 col-sm-6 clearfix smoothScroll">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">'.@$arraybien['cuonmuot'].'</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body radios">
            <label>
                <input type="radio" name="smooth_scroll" value="on" '.($smoothScroll=="on"?"checked":null).'>
                '.$arraybien['co'].'
            </label>
            <label>
                <input type="radio" name="smooth_scroll" value="off" '.($smoothScroll=="off"?"checked":null).'>
                '.$arraybien['khong'].'
            </label>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 clearfix smoothScroll -->

</div>
<!-- .row feature-row1 -->

</div>
<!-- .panel-body -->

</div>
<!-- .right-box clearfix config-padding -->

</div>
<!-- #feature.right-item -->';
}// END FEATURE BOX

if ( $group_embed == "on" ) {
$CONTENT.='
<div class="right-item '.(@$_SESSION['active_config']=='embed'?'active':null).'" id="embed">
    <div class="right-title config-padding">
        <h3 class="menu">
            CẤU HÌNH MÃ NHÚNG
        </h3>
        <!-- .h3 -->
    </div>
    <!-- .right-title -->

    <div class="right-box clearfix config-padding">
        <div class="panel-body">
<div class="row embed-row1 clearfix">

<div class="col-xs-12 col-sm-6">
<div class="panel panel-default manhungdau">
    <div class="panel-heading">
        <h3 class="panel-title">Mã nhúng đầu trang (trước <&nbsp;/head>)</h3>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Mã nhúng</label>
            <textarea name="manhungdau" class="form-control" cols="" rows="3" placeholder="Mã cần thiết muốn chèn lên đầu trang">' . (!empty($manhungdau) ? $manhungdau : null) . '</textarea>
        </div>
    </div>
</div><!-- ./manhungdau -->
</div>
<!-- .col-xs-12 col-sm-6 -->

<div class="col-xs-12 col-sm-6">
    <div class="panel panel-default manhungcuoi">
        <div class="panel-heading">
            <h3 class="panel-title">Mã nhúng cuối trang (trước <&nbsp;/body>)</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label>Mã nhúng</label>
                <textarea name="manhungcuoi" class="form-control" cols="" rows="3" placeholder="Mã cần thiết muốn chèn lên đầu trang">' . (!empty($manhungcuoi) ? $manhungcuoi : null) . '</textarea>
            </div>
        </div>
    </div><!-- ./manhungcuoi -->
</div>
<!-- .col-xs-12 col-sm-6 -->

</div>
<!-- .row embed-row1 clearfix -->
</div>
<!-- .panel-body -->
    </div>
    <!-- .right-box clearfix config-padding -->
</div>
<!-- #embed.right-item -->';
}// END EMBED BOX

if ( $issupperadmin ) {
$CONTENT.='
<div class="right-item '.(@$_SESSION['active_config']=='configsadmin'?'active':null).'" id="configsadmin">
    <div class="right-title config-padding">
        <h3 class="menu">
            CẤU HÌNH ROOT
        </h3>
        <!-- .h3 -->
    </div>
    <!-- .right-title -->

    <div class="right-box clearfix config-padding">
        <div class="panel-body clearfix">';

$CONTENT.='
<div class="row root-row1">
<div class="col-xs-12 col-sm-6">
<div class="panel panel-default">
    <div class="panel-heading">
        <strong class="h4">Tạo lại xml</strong>
    </div>
    <!-- .panel-heading -->
    <div class="panel-body">
        <label>
            <input type="checkbox" name="rebuild_xml" value="1" />
            Xóa và tạo lại XML
        </label>
    </div>
    <!-- .panel-body -->
</div>
<!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->

<div class="col-xs-12 col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">Hiển thị website với</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body radios">
            <label>
                <input type="radio" name="viewfor" value="1" class="form-control" '.($viewfor==1?'checked':null).' />
                Tất cả
            </label>
            <label>
                <input type="radio" name="viewfor" value="2" class="form-control" '.($viewfor==2?'checked':null).' />
                Admin & sAdmin
            </label>
            <label>
                <input type="radio" name="viewfor" value="3" class="form-control" '.($viewfor==3?'checked':null).' />
                sAdmin
            </label>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->

</div>
<!-- .row root-row1 -->

<div class="row root-row2">

<div class="col-xs-12 col-sm-6">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <strong class="h4">XÓA CACHE</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body">
            <div id="result_cache"></div><!-- #result_cache -->

            <button type="button" onClick="Get_Data(\'ajax.php?op='.$__getop.'&name=xoacache&value=js\',\'result_cache\')" class="btn btn-danger">Xóa cache JS</button>
                        &nbsp;&nbsp;&nbsp;
            <button type="button" onClick="Get_Data(\'ajax.php?op='.$__getop.'&name=xoacache&value=css\',\'result_cache\')" class="btn btn-danger">Xóa cache CSS</button>
            &nbsp;&nbsp;&nbsp;
            <button type="button" onClick="Get_Data(\'ajax.php?op='.$__getop.'&name=xoacache&value=tpl\',\'result_cache\')" class="btn btn-danger">Xóa cache TPL</button>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->
';
$solanxemhientai = $db->getNameFromID("tbl_counter", "solanxem", "ngay", "'" . $db->getDate() . "'");
$CONTENT.='
<div class="col-xs-12 col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">Tùy chỉnh thống kê</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body">
        <div class="input-group">
            <input type="text" class="form-control" onkeyup="CheckNumber(this)" name="soluotxem" value="' . $solanxemhientai . '" maxlength="3" id="suasoluotxem" placeholder="nhập số lượt xem cho ngày hôm nay hoặc xóa toàn bộ">
            <span class="input-group-addon">lượt xem / hôm nay</span>
            <span class="input-group-btn">
                <button onclick="if(confirm(\'Bạn có muốn xóa tất cả lượt xem ?\')) submitbutton(\'xoathongke\')" class="btn btn-default" id="delview" type="button">Xóa</button>
            </span>
        </div>
        <!-- .input-group -->

        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->

</div>
<!-- .row root-row2 -->

<div class="row root-row3">
<div class="col-xs-12 col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">Nút mua hàng</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body radios">
            <label><input type="radio" name="nutdathang" value="on" '.($nutdathang=="on"?"checked":null).'>Hiện</label>
            <label><input type="radio" name="nutdathang" value="off" '.($nutdathang=="off"?"checked":null).'>Ẩn</label>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->

<div class="col-xs-12 col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong class="h4">Hiển thị lỗi (phpError)</strong>
        </div>
        <!-- .panel-heading -->
        <div class="panel-body radios">
            <label>
                <input type="radio" name="baoloiweb" value="on" '.($baoloiweb=="on"?"checked":null).' />
                Bật báo lỗi
            </label>
            <label>
                <input type="radio" name="baoloiweb" value="off" '.($baoloiweb=="off"?"checked":null).' />
                Tắt báo lỗi
            </label>
        </div>
        <!-- .panel-body -->
    </div>
    <!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 -->
</div>
<!-- .row root-row3 -->

<div class="row root-row4">
<div class="col-xs-12 col-sm-6 clearfix">
<div class="panel panel-default">
    <div class="panel-heading">
        <strong class="h4">Ẩn/Hiện Cấu Hình</strong>
    </div>
    <!-- .panel-heading -->
    <div class="panel-body">

        <div class="form-group Email">
            <div class="input-group">
                <div class="input-group-addon">EMail</div>
                <!-- .input-group-addon -->
                <div class="group-active radios">
                    <label>
                        <input type="radio" name="group_email" value="on" '.($group_email=="on"?'checked':null).' />
                        Hiện
                    </label>
                    <label>
                        <input type="radio" name="group_email" value="off" '.($group_email=="off"?'checked':null).' />
                        Ẩn
                    </label>
                </div>
                <!-- .input-active -->
            </div>
            <!-- .input-group -->
        </div>
        <!-- .form-group -->

        <div class="form-group MauSac">
            <div class="input-group">
                <div class="input-group-addon">Màu sắc</div>
                <!-- .input-group-addon -->
                <div class="group-active radios">
                    <label>
                        <input type="radio" name="group_color" value="on" '.($group_color=="on"?'checked':null).' />
                        Hiện
                    </label>
                    <label>
                        <input type="radio" name="group_color" value="off" '.($group_color=="off"?'checked':null).' />
                        Ẩn
                    </label>
                </div>
                <!-- .input-active -->
            </div>
            <!-- .input-group -->
        </div>
        <!-- .form-group -->

        <div class="form-group Social">
            <div class="input-group">
                <div class="input-group-addon">Mạng xã hội</div>
                <!-- .input-group-addon -->
                <div class="group-active radios">
                    <label>
                        <input type="radio" name="group_social" value="on" '.($group_social=="on"?'checked':null).' />
                        Hiện
                    </label>
                    <label>
                        <input type="radio" name="group_social" value="off" '.($group_social=="off"?'checked':null).' />
                        Ẩn
                    </label>
                </div>
                <!-- .input-active -->
            </div>
            <!-- .input-group -->
        </div>
        <!-- .form-group -->

        <div class="form-group Feature">
            <div class="input-group">
                <div class="input-group-addon">Tiện ích</div>
                <!-- .input-group-addon -->
                <div class="group-active radios">
                    <label>
                        <input type="radio" name="group_feature" value="on" '.($group_feature=="on"?'checked':null).' />
                        Hiện
                    </label>
                    <label>
                        <input type="radio" name="group_feature" value="off" '.($group_feature=="off"?'checked':null).' />
                        Ẩn
                    </label>
                </div>
                <!-- .input-active -->
            </div>
            <!-- .input-group -->
        </div>
        <!-- .form-group -->

        <div class="form-group Embed">
            <div class="input-group">
                <div class="input-group-addon">Mã nhúng</div>
                <!-- .input-group-addon -->
                <div class="group-active radios">
                    <label>
                        <input type="radio" name="group_embed" value="on" '.($group_embed=="on"?'checked':null).' />
                        Hiện
                    </label>
                    <label>
                        <input type="radio" name="group_embed" value="off" '.($group_embed=="off"?'checked':null).' />
                        Ẩn
                    </label>
                </div>
                <!-- .input-active -->
            </div>
            <!-- .input-group -->
        </div>
        <!-- .form-group -->

    </div>
    <!-- .panel-body -->
</div>
<!-- .panel panel-primary -->
</div>
<!-- .col-xs-12 col-sm-6 clearfix -->

<div class="col-xs-12 col-sm-6 clearfix">
<div class="panel panel-default">
    <div class="panel-heading">
        <strong class="h4">Sao Lưu</strong>
        <i>(Chức năng sao lưu cấu hình)</i>
    </div>
    <!-- .panel-heading -->
    <div class="panel-body">';
        $fileExi = false;
    if ( file_exists( dirname(__FILE__)."/../../plugin/xml/data/config.bak.xml" ) ) {
        $fileExi = true;
    }$CONTENT.='
        <button type="button" data-loading-text="Đang sao lưu..." class="btn btn-default" id="backupConfig">
            <i class="fa fa-download"></i>&nbsp; Sao lưu</button>
            &nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" data-loading-text="Đang khôi phục..." id="restoreConfig" class="btn btn-default '.(!$fileExi?'disabled':null).'"><i class="fa fa-upload"></i>&nbsp; Phục hồi</button>
    </div>
    <!-- .panel-body -->
</div>
<!-- .panel panel-default -->
</div>
<!-- .col-xs-12 col-sm-6 clearfix -->

</div>
<!-- .row root-row4 -->
';

    $CONTENT.='
        </div>
        <!-- .panel-body clearfix -->
    </div>
    <!-- .right-box clearfix config-padding -->

</div>
<!-- #configsadmin.right-item -->
';
}
$CONTENT.='
            </div>
            <!-- ./config-R -->
        </div>
        <!-- .config-row row -->
    </div>
    <!-- .config-content -->
</div>
<!-- .config-container -->';

/*Favicon Modal*/
$CONTENT.='
<!-- Modal -->
<div class="modal fade" id="modal-favicon">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><center>THƯ VIỆN FAVICON</center></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="faviconli">
                    <label for="favicon1">
                        <img src="'.$__config['path_img'].'lib_favicon/fa1.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa1.png'?'checked="checked"':null).' id="favicon1" value="fa1.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon2">
                        <img src="'.$__config['path_img'].'lib_favicon/fa2.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa2.png'?'checked="checked"':null).' id="favicon2" value="fa2.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon3">
                        <img src="'.$__config['path_img'].'lib_favicon/fa3.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa3.png'?'checked="checked"':null).' id="favicon3" value="fa3.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon4">
                        <img src="'.$__config['path_img'].'lib_favicon/fa4.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa4.png'?'checked="checked"':null).' id="favicon4" value="fa4.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon5">
                        <img src="'.$__config['path_img'].'lib_favicon/fa5.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa5.png'?'checked="checked"':null).' id="favicon5" value="fa5.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon6">
                        <img src="'.$__config['path_img'].'lib_favicon/fa6.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa6.png'?'checked="checked"':null).' id="favicon6" value="fa6.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon7">
                        <img src="'.$__config['path_img'].'lib_favicon/fa7.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa7.png'?'checked="checked"':null).' id="favicon7" value="fa7.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon8">
                        <img src="'.$__config['path_img'].'lib_favicon/fa8.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa8.png'?'checked="checked"':null).' id="favicon8" value="fa8.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon9">
                        <img src="'.$__config['path_img'].'lib_favicon/fa9.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa9.png'?'checked="checked"':null).' id="favicon9" value="fa9.png" />
                </div>
                <div class="faviconli">
                    <label for="favicon10">
                        <img src="'.$__config['path_img'].'lib_favicon/fa10.png" width="40" alt="" />
                    </label>
                    <input type="radio" name="favicon" '.($favicon=='fa10.png'?'checked="checked"':null).' id="favicon10" value="fa10.png" />
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Đồng ý</button>
            </div>
        </div>
    </div>
</div>';
$CONTENT .= '
   <input type="hidden" value="' . $data['id'] . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="off" name="version">
   <input type="hidden" value="off" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvadong">
   <input type="hidden" value="on" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '
<div class="clear"></div>
</form>';

$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array(
    "{CONTENT}" => $CONTENT,
    "{ACTION}"  => $ACTION);
echo load_content($file_tempaltes, $array_bien);
