<?php

$resultCache = '';
$rs = array();
function displayNotifi($mess='')
{
    return '
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            '.$mess.'
    </div>
    ';
}
switch ($name) {
    case 'backup_config':
        $file = dirname(__FILE__)."/../../plugin/xml/data/config.xml";
        $file2 = dirname(__FILE__)."/../../plugin/xml/data/config.bak.xml";
        if ( !copy($file, $file2) ) {
            $rs['status'] = 'error';
        } else {
            $rs['status'] = 'success';
        }
        break;

    case 'restore_config':
        if ( file_exists(dirname(__FILE__)."/../../plugin/xml/data/config.bak.xml") ) {
            $file = dirname(__FILE__)."/../../plugin/xml/data/config.bak.xml";
            $file2 = dirname(__FILE__)."/../../plugin/xml/data/config.xml";
            if ( !copy($file, $file2) ) {
                $rs['status'] = 'error';
            } else {
                $rs['status'] = 'success';
            }
        } else {
            $rs['status'] = 'error';
        }
        break;

    case 'xoacache':
        switch ($value) {
            case 'js':
                unlink("../templates/layout1/js/load.cache.js");
                $resultCache = displayNotifi("CACHE JS ĐÃ ĐƯỢC XÓA THÀNH CÔNG !");
                break;

            case 'css':
                unlink("../templates/layout1/css/css.cache.css");
                $resultCache = displayNotifi("CACHE CSS ĐÃ ĐƯỢC XÓA THÀNH CÔNG !");
                break;

            case 'tpl':
                delete_dir("../smarty/templates_c");
                $resultCache = displayNotifi("templates_c ĐÃ ĐƯỢC XÓA THÀNH CÔNG !");
                break;


            default:
                # code...
                break;
        }
        echo $resultCache;
        exit();
        break;

    case 'testmail':
        if (true) {
            $subject = 'TEST MAIL ' . $_SERVER['HTTP_HOST'];
            $message = "ĐÂY LÀ EMAIL TEST TỰ ĐỘNG. VUI LÒNG KHÔNG REPLY.";
            // hot mail
            $array_sendmail = array(
                "emailnhan" => $_getArrayconfig['email_nhan'],
                "emailgui"  => $_getArrayconfig['hostmail_user'],
                "hostmail"  => $_getArrayconfig['hostmail'],
                "user"      => $_getArrayconfig['hostmail_user'],
                "pass"      => $_getArrayconfig['hostmail_pass'],
                "tieude"    => $subject,
                "fullname"  => $_getArrayconfig['hostmail_fullname'],
                "port"      => $_getArrayconfig['hostmail_port'],
                "ssl"       => $_getArrayconfig['hostmail_ssl'],
                "subject"   => $subject,
                "message"   => $message,
            );
            $rsMail = sendmail($array_sendmail);
            $rs = array("result" => $rsMail);

        }
        break;

    case 'menu_active':
        unset($_SESSION['active_config']);
        $_SESSION['active_config'] = ws_get('index');
        $rs = array('result'=>$_SESSION['active_config']);
        break;

    default:
        # code...
        break;
}

echo json_encode($rs);

