<?php
// Đường dẫn tải file
$url = "https://webso.vn/uploads/files/lib_favicon.zip";
// Đường dẫn lưu file
$zipFile = "../uploads/config/lib_favicon.zip";
$zipResource = fopen($zipFile, "w");
// Truy cập file zip từ máy chủ
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FAILONERROR, true);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_AUTOREFERER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER,true);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_FILE, $zipResource);
$page = curl_exec($ch);
if(!$page) {
 echo "Error :- ".curl_error($ch);
}
curl_close($ch);

/* Open the Zip file */
$zip = new ZipArchive;
// Đường dẫn xã nén file
$extractPath = "../uploads/config/";
if($zip->open($zipFile) != "true"){
 echo "Error :- Unable to open the Zip File";
}
/* Extract Zip File */
$zip->extractTo($extractPath);
$zip->close();

 ?>