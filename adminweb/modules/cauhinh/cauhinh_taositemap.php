<?php
$s_danhmuc = "SELECT a.id,a.idtype,b.ten,link,url,noindex
             FROM tbl_danhmuc AS a
             INNER JOIN tbl_danhmuc_lang AS b
             ON a.id = b.iddanhmuc
              where anhien = 1
             and b.idlang = '{$__defaultlang}'
            ";
$d_danhmuc = $db->rawQuery($s_danhmuc);
$noidungsitemap = '';
$noidungsitemap2 = '';
$host       = 'http://' . $_SERVER['HTTP_HOST'] . '/';
if (count($d_danhmuc) > 0) {
    $thoigianquet   = ws_post('thoigianquet');
    $noidungsitemap .= '<?xml version="1.0" encoding="utf-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    $noidungsitemap .= '
        <url>
        <loc>'.$host.'</loc>
        <changefreq>' . $thoigianquet . '</changefreq>
        <priority>1.00</priority>
        </url>';
    foreach ($d_danhmuc as $key_danhmuc => $info_danhmuc) {
        $id         = $info_danhmuc['id'];
        $idtype     = $info_danhmuc['idtype'];
        $ten        = $info_danhmuc['ten'];
        $link       = $info_danhmuc['link'];
        $url        = $info_danhmuc['url'];
        $noindex    = $info_danhmuc['noindex'];
        $id_modules = $db->getNameFromID("tbl_danhmuc_type", "id", "op", "'home'");
        $linkdm     = $host . $url . '/';
        if ($link != '') {
            $linkdm = $link;
        }
        $douutien = '0.80';
        if ($id_modules == $idtype) {
            $douutien = '1.00';
        }
        if($noindex==0 && $link=='' && $id_modules!=$idtype){
             $noidungsitemap .= '
            <url>
            <loc>' . $linkdm . '</loc>
            <changefreq>' . $thoigianquet . '</changefreq>
            <priority>' . $douutien . '</priority>
            </url>';
        }

        // lay noidung
        $s_tintuc = "SELECT a.id,b.ten,link,url,noindex
                   FROM tbl_noidung AS a
                   INNER JOIN tbl_noidung_lang AS b
                   ON a.id = b.idnoidung
                   where a.anhien = 1
                   and b.idlang = '{$__defaultlang}'
                   and a.idtype like '" . $id . "%'
                  ";
        $d_tintuc = $db->rawQuery($s_tintuc);
        if (count($d_tintuc) > 0) {
            foreach ($d_tintuc as $key_tintuc => $info_tintuc) {
                $url      = $info_tintuc['url'];
                $ten      = $info_tintuc['ten'];
                $link     = $info_tintuc['link'];
                $noindex  = $info_tintuc['noindex'];
                $douutien = 0.7;
                $lienket  = $host . $url;
                if ($link != '') {
                    $lienket = $link;
                }
                if($noindex==0){
                    $noidungsitemap2 .= '<url>
                    <loc>' . $lienket . '</loc>
                    <changefreq>' . $thoigianquet . '</changefreq>
                    <priority>' . $douutien . '</priority>
                    </url>';
                }

            }
        }
    }
    $noidungsitemap.= $noidungsitemap2;
    $noidungsitemap .= '</urlset>';
    $file = fopen("../sitemap.xml", 'w');
    fwrite($file, $noidungsitemap);
    fclose($file);
    echo 'Tạo sitemap thành công: <a href="./?op=cauhinh">Trở lại</a>';
} else {
    echo 'Tạo sitemap thất bại <a href="./?op=cauhinh">Trở lại</a>';
}
