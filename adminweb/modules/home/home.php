<?php include 'plugin/counter/counter.php';?>
<div class="container-fluid container-home-admin">
   <div class="page-heading page-heading-md">
      <div class="form-group"></div>
   </div>
   <div class="notification-ads hidden">
      <a href="#" title="#"><?php echo NULL; ?></a>
   </div>
<?php
if ($_SESSION['user_supperadmin'] == 1) {
    $s_itemHome = "SELECT *
                  FROM tbl_user_group
                  WHERE anhien = 1
                  AND home = 1";
} else {
    $s_itemHome = "SELECT *
                  FROM tbl_user_group
                  WHERE anhien = 1
                  AND idgroup = 1
                  AND home = 1";
}
$d_itemHome = $db->rawQuery($s_itemHome);
if (count($d_itemHome) > 0) {
    echo '<div class="row box-itemHome">';
    foreach ($d_itemHome as $key_itemHome => $info_itemHome) {
        $itemHome_id         = $info_itemHome['id'];
        $itemHome_link       = $info_itemHome['link'];
        $itemHome_name       = $db->getNameFromID("tbl_user_group_lang", "ten", "iddanhmuc", $itemHome_id . " AND idlang = $__defaultlang ");
        $itemHome_idgroup    = $info_itemHome['idgroup'];
        $itemHome_icon       = $info_itemHome['icon'];
        $itemHome_background = $db->getNameFromID("tbl_mausac", "background", "idtype", " '" . $itemHome_id . "' ");
        $itemHome_color      = $db->getNameFromID("tbl_mausac", "color", "idtype", " '" . $itemHome_id . "' ");
        $nhom_isUser         = $db->getNameFromID("tbl_user", "nhom", "id", $_SESSION['user_id']);
        // SET VALUE
        $setValue = "";
        if (substr($itemHome_link, 6) == 'sanpham') {
            $sd_sanpham = count($db->rawQuery("SELECT * FROM tbl_noidung WHERE anhien = 1 AND loai = 1 "));
            $setValue   = $sd_sanpham;
        } elseif (substr($itemHome_link, 6) == 'noidung') {
            $sd_noidung = count($db->rawQuery("SELECT * FROM tbl_noidung WHERE anhien = 1 AND loai = 0 "));
            $setValue   = $sd_noidung;
        } elseif (substr($itemHome_link, 6) == 'donhang') {
            $sd_donhang = count($db->rawQuery("SELECT * FROM tbl_donhang "));
            $setValue   = $sd_donhang;
        } elseif (substr($itemHome_link, 6) == 'user') {
            $sd_user  = count($db->rawQuery("SELECT * FROM tbl_user WHERE active = 1 "))-1;
            $setValue = $sd_user;
        }
        if ($nhom_isUser == 1) {
            echo '
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
               <div class="panel panel-default clearfix" style="background:#' . $itemHome_background . '">
                  <div class="panel-body"">
                     <a href="' . $itemHome_link . '" style="color:' . $itemHome_color . '" title="' . $itemHome_name . '">
                        <div class="value">' . $setValue . '</div>
                        <div class="icon">
                           <i class="' . $itemHome_icon . '"></i>
                        </div>
                        <header><h3 class="thin">' . $itemHome_name . '</h3></header>
                     </a>
                  </div>
               </div>
            </div>';
        }
    }
    echo '
         </div>
      ';
}
$donhang_moi       = count($db->rawQuery("SELECT * FROM tbl_donhang WHERE trangthai = 0"));
$donhang_chuaxuly  = count($db->rawQuery("SELECT * FROM tbl_donhang WHERE trangthai = 1"));
$donhang_dangxuly  = count($db->rawQuery("SELECT * FROM tbl_donhang WHERE trangthai = 2"));
$donhang_thanhcong = count($db->rawQuery("SELECT * FROM tbl_donhang WHERE trangthai = 3"));
$donhang_dahuy     = count($db->rawQuery("SELECT * FROM tbl_donhang WHERE trangthai = 4"));
echo '
         <div class="row">
            <div class="col-md-12">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="col-md-8">
                     <h4 class="thin no-margin-top">THỐNG KÊ TRUY CẬP</h4>';
include "plugin/animated-charts-with-chart-js/index.php";
echo '</div>';
/*
<table class="table thongkedonhang">
<tbody>
<tr>
<td>Đang online:</td>
<td class="semi-bold">'.online().'</td>
</tr>
<tr>
<td>Truy cập hôm nay:</td>
<td class="semi-bold">'.today().'</td>
</tr>
<tr>
<td>Truy cập hôm qua:</td>
<td class="semi-bold">'.yesterday().'</td>
</tr>
<tr>
<td>Tổng số truy cập:</td>
<td class="semi-bold">'.total().'</td>
</tr>
<tr>
<td>Truy cập trung bình:</td>
<td class="semi-bold">'.avg().'</td>
</tr>
</tbody>
</table>
 */
// Kiem tra module donhang co hien thi không
@$anhien_donhang = $db->getNameFromID("tbl_user_group", "anhien", "link", "'./?op=donhang'");
@$group_donhang  = $db->getNameFromID("tbl_user_group", "idgroup", "link", "'./?op=donhang'");
if ($_SESSION['user_supperadmin'] != 1) {
    if ($anhien_donhang == 1 && $group_donhang == 1 && $nhom_isUser == 1) {
        echo '
            <div class="col-md-4">
               <h4 class="thin no-margin-top">CHI TIẾT ĐƠN HÀNG</h4>
               <table class="table thongkedonhang">
                  <tbody>
                     <tr>
                        <td>Đơn hàng mới</td>
                        <td class="semi-bold">' . $donhang_moi . '</td>
                     </tr>
                     <tr>
                        <td>Đơn hàng chưa xử lý</td>
                        <td class="semi-bold">' . $donhang_chuaxuly . '</td>
                     </tr>
                     <tr>
                        <td>Đơn hàng đang xử lý</td>
                        <td class="semi-bold">' . $donhang_dangxuly . '</td>
                     </tr>
                     <tr>
                        <td>Đơn hàng thành công</td>
                        <td class="semi-bold">' . $donhang_thanhcong . '</td>
                     </tr>
                     <tr>
                        <td>Đơn hàng đã hủy</td>
                        <td class="semi-bold">' . $donhang_dahuy . '</td>
                     </tr>
                  </tbody>
               </table>
            </div>';
    }
} elseif ($anhien_donhang == 1) {
    echo '
         <div class="col-md-4">
            <h4 class="thin no-margin-top">CHI TIẾT ĐƠN HÀNG</h4>
            <table class="table thongkedonhang">
               <tbody>
                  <tr>
                     <td>Đơn hàng mới</td>
                     <td class="semi-bold">' . $donhang_moi . '</td>
                  </tr>
                  <tr>
                     <td>Đơn hàng chưa xử lý</td>
                     <td class="semi-bold">' . $donhang_chuaxuly . '</td>
                  </tr>
                  <tr>
                     <td>Đơn hàng đang xử lý</td>
                     <td class="semi-bold">' . $donhang_dangxuly . '</td>
                  </tr>
                  <tr>
                     <td>Đơn hàng thành công</td>
                     <td class="semi-bold">' . $donhang_thanhcong . '</td>
                  </tr>
                  <tr>
                     <td>Đơn hàng đã hủy</td>
                     <td class="semi-bold">' . $donhang_dahuy . '</td>
                  </tr>
               </tbody>
            </table>
         </div>';
}
echo '
                  </div>
               </div>
            </div>
         </div>
      ';
/*
echo '
<div class="row">
<div class="col-md-8">
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title thuoctinhtitle">NHẬT KÝ HOẠT ĐỘNG</div>
</div>
<div class="panel-body">
Basic panel example
</div>
</div>
</div>
<div class="col-md-4 col-lg-4">
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title thuoctinhtitle">Blog webso.vn</div>
</div>
<div class="panel-body">
<ul>
<li><a href="#">Thông tin cập nhật mới nhất 1</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 2</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 3</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 4</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 5</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 6</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 7</a></li>
<li><a href="#">Thông tin cập nhật mới nhất 8</a></li>
</ul>
</div>
</div>
</div>
</div>
';
 */
?>
</div>
