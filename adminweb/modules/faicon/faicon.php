<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Quản lý Icon",
    "table"           => 'tbl_faicon',
    "tablenoidung"    => 'tbl_faicon_lang',
    "id"              => 'id',
    "idtype"          => 1,
    "loai"            => 1,
    "thutu"           => 1,
    "ten"             => 1,
    "anhien"          => 1,
    "bieutuong"       => 1,
    "action"          => 1,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/option/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300,
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
