<?php
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Process Thêm mới
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}
$_id_name = $__config['id'];
$_getop   = ws_get('op');
$_idtype  = $__config['type'];
$_idSP    = $__config['idsp'];
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
//*** Xử lý quá trình Chỉnh sửa ***//
if ($__getid != '') {
    $__cid     = ws_get('id');
    $s_sanpham = "SELECT * FROM $__table WHERE id = $__getid";
    $data      = $db->rawQuery($s_sanpham);
    $data      = $data[0];
    $id        = $data['id'];
    $thutu     = $data['thutu'];
    $anhien    = $data['anhien'];
    $ten       = $data['ten'];
    $bieutuong = $data['bieutuong'];
}
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
   <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
if ($__config['anhien'] == 1) {
    $CONTENT .= '
         <div class="checkbox">
            <label>';
    if ($anhien == '') {
        $anhien = 1;
    }
    if ($anhien == 1) {
        $CONTENT .= '<input checked="checked"  value="1" type="checkbox" name="anhien" id="anhien" />';
    } else {
        $CONTENT .= '<input  value="1" type="checkbox" name="anhien" id="anhien" />';
    }
    $CONTENT .= '
            ' . $arraybien['hienthi'] . '</label>
         </div>';
}
$CONTENT .= '
         </div>
      </div>
</div>';
//********** col right ********** //
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading thuoctinhtitle">&nbsp;' . $arraybien['thongtinchitiet'] . '</div>
  <div class="panel-body">
     <div class="form-group">
        <div class="input-group">
           <div class="input-group-addon">' . $arraybien['tenicon'] . '</div>
           <input type="text" value="' . $ten . '" class="form-control" name="ten" placeholder="' . $arraybien['nhaptenicon'] . '" />
        </div>
     </div>
     <div class="form-group">
        <div class="input-group">
           <div class="input-group-addon">' . $arraybien['giatriicon'] . '</div>
           <input type="text" value="' . $bieutuong . '" class="form-control" name="bieutuong" placeholder="' . $arraybien['nhapgiatriicon'] . '" />
        </div>
     </div>
';
$CONTENT .= '
      </div>
   </div>
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '</div>'; // end col data
$CONTENT .= '<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
