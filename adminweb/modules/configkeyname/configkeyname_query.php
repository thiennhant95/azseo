<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyenxoa'] . '");
         location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    $id_order = ws_post('cid');
    del_data($__table, 'id', $id_order);
}
if ($__getaction == 'save') {
    // LƯU TRỮ DỮ LIỆU
    $array_data = get_data($__table, '*', '');
    $id         = max_number_in_array($array_data, "id");
    $id += 1;
    $ten      = ((ws_post('ten')) ? trim(addslashes(ws_post('ten'))) : '');
    $dinhdanh = ((ws_post('dinhdanh')) ? trim(addslashes(ws_post('dinhdanh'))) : '');
    $anhien   = ((ws_post('anhien')) ? trim(addslashes(ws_post('anhien'))) : 1);

    $__postid = ((ws_post('id')) ? ws_post('id') : '');

    if ($__getid == '') {
        /** THÊM MỚI DỮ LIỆU */
        /** Kiểm tra đã tồn tại file xml table chưa */
        if (!file_exists($folder_xml . $__config['table'] . '.xml')) {
            $aray_insert = array(
                'id',
                'ten',
                'dinhdanh',
                'anhien',
            );
            create_table($__config['table'], $aray_insert);
        } // End Tạo bảng

        /** Thêm tiếp dữ liệu */
        //Chèn dữ liệu lần đầu
        $arr_sql = array(
            $id,
            $ten,
            $dinhdanh,
            $anhien,
        );
        add_data($__config['table'], $arr_sql);

    } else {
        /** CẬP NHẬT DỮ LIỆU */
        $data_new = array(
            $__postid,
            $ten,
            $dinhdanh,
            $anhien,
        );
        update_data($__table, array('id', '' . $__postid . ''), $data_new);
    }

}
$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if (ws_post('luuvathem') && ws_post('luuvathem') == 'luuvathem') {
    $id = (!empty($id_insert) ? $id_insert : '');
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
//http://sinhvienit.net/forum/php-code-doc-xoa-cap-nhat-them-vao-csdl-xml.24965.html
