<?php
$__table        = @$__config['table'];
$__tablenoidung = @$__config['tablenoidung'];
$__id           = @$__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
//*** CÁC PHÍM CHỨC NĂNG ***
if ($__post_task == 'unpublish') {
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') {
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = @ws_post('cid')[$r];
        $value_img  = @ws_post('img')[$id_order];
        $value_file = @ws_post('file')[$id_order];
        // thay doi thu tu san pham
        if ( $db->sqlDelete($__table, " id = '{$id_order}' ") ) {
            if ($value_img != '') {
                $__path      = @$__config['path_img'] . $value_img;
                $__paththumb = @$__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            if ($value_file != '') {
                $__path = @$__config['path_file'] . $value_file;
                unlink("$__path");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddanhmuc  = '" . $id_order . "' ");
            $db->sqlDelete("tbl_user_active", " iduser   = '" . $id_order . "' ");
        }
    }
}
if ($__getaction == 'save') {
    @$tendangnhap = trim(replace_html(ws_post('ten')));
    @$matkhau     = trim(replace_html(ws_post('rematkhau')));
    @$hotendem    = trim(replace_html(ws_post('hotenlot')));
    @$ten         = trim(replace_html(ws_post('hoten')));
    @$diachi      = trim(replace_html(ws_post('diachi')));
    @$email       = trim(replace_html(ws_post('email')));
    @$sodienthoai = trim(replace_html(ws_post('dienthoai')));
    @$active      = trim(replace_html(ws_post('active')));
    @$chonnhom    = trim(replace_html(ws_post('chonnhom')));
    @$chonquyen   = trim(replace_html(ws_post('chonquyen')));
    @$gioitinh    = trim(replace_html(ws_post('gioitinh')));
    @$tinhthanh   = trim(replace_html(ws_post('tinhthanh')));
    @$ngaysinh    = trim(replace_html(ws_post('ngaysinh')));
    @$website     = trim(replace_html(ws_post('website')));
    @$yahoo       = trim(replace_html(ws_post('yahoo')));
    @$lat         = rand(100, 999);
    @$matkhau     = md5(md5($matkhau) . $lat);
    @$_getidSP    = "";
    if ($__getid != '') // neu la cap nhat
    {
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->createSubID($__table, $__id, ws_post('parenid'));
    }
    // Create url lien ket
    $urllink = '';
    $_cid    = ws_get('id');
    if (ws_post('action') == 'add') {
        $thutu = (int) substr($subid, -3);
    } else {
        $thutu = (int) substr($id, -3);
    }
    $images_name = @ws_post('url'.$__defaultlang);
    //*** THEM MOI NGUOI DUNG
    if ($__getid == '') {
        $aray_insert = array(
            "tendangnhap" => $tendangnhap,
            "matkhau"     => $matkhau,
            "active"      => $active,
            "lat"         => $lat, //????
            "nhom"        => $chonnhom,
            "supperadmin" => $chonquyen,
            "hotendem"    => $hotendem,
            "ten"         => $ten,
            "gioitinh"    => $gioitinh,
            "email"       => $email,
            "diachi"      => $diachi,
            "tinhthanh"   => $tinhthanh,
            "sodienthoai" => $sodienthoai,
            "ngaysinh"    => $ngaysinh,
            "website"     => $website,
            "yahoo"       => $yahoo,
            "ngaydangky"  => $db->getDateTimes(),
            "ngaycapnhat" => $db->getDateTimes(),
        );
        $id_insert = $db->insert($__table, $aray_insert);
        // THÊM PHÂN QUYỀN
        if ($id_insert > 0) {
            $id_user     = $id_insert;
            $tong_module = ws_post('tong_module');
            for ($imo = 0; $imo < $tong_module; $imo++) {
                $xem             = trim(replace_html(ws_post('xem'.$imo)));
                $them            = trim(replace_html(ws_post('them'.$imo)));
                $sua             = trim(replace_html(ws_post('sua'.$imo)));
                $xoa             = trim(replace_html(ws_post('xoa'.$imo)));
                $id_module       = trim(replace_html(ws_post('id_module'.$imo)));
                $array_phanquyen = array(
                    "idtype" => $id_module,
                    "iduser" => $id_user,
                    "active" => $xem,
                    "them"   => $them,
                    "xoa"    => $xoa,
                    "sua"    => $sua,
                );
                $db->insert("tbl_user_active", $array_phanquyen);
            }
        }
        /* echo "<script>alert()</script>"; */
    } else {
        $aray_insert = array(
            "active"      => $active,
            "nhom"        => $chonnhom,
            "supperadmin" => $chonquyen,
            "hotendem"    => $hotendem,
            "ten"         => $ten,
            "gioitinh"    => $gioitinh,
            "email"       => $email,
            "diachi"      => $diachi,
            "tinhthanh"   => $tinhthanh,
            "sodienthoai" => $sodienthoai,
            "ngaysinh"    => $ngaysinh,
            "website"     => $website,
            "yahoo"       => $yahoo,
            "ngaycapnhat" => $db->getDateTimes(),
        );
        $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
        // SỬA PHÂN QUYỀN
        $id_user     = $__postid;
        $tong_module = trim(replace_html(ws_post('tong_module')));
        for ($imo = 0; $imo < $tong_module; $imo++) {
            $xem       = trim(replace_html(@ws_post('xem'.$imo)));
            $them      = trim(replace_html(@ws_post('them'.$imo)));
            $sua       = trim(replace_html(@ws_post('sua'.$imo)));
            $xoa       = trim(replace_html(@ws_post('xoa'.$imo)));
            $id_module = trim(replace_html(@ws_post('id_module'.$imo)));
            $id_per2   = trim(replace_html(@ws_post('id_per2'.$imo)));
            $s_ktra    = "SELECT id FROM tbl_user_active WHERE iduser = '$__getid' AND idtype = $id_module";
            $d_ktra    = $db->rawQuery($s_ktra);
            if (count($d_ktra) > 0) {
                $array_phanquyen2 = array(
                    "active" => $xem,
                    "them"   => $them,
                    "xoa"    => $xoa,
                    "sua"    => $sua,
                );
                $db->sqlUpdate("tbl_user_active", $array_phanquyen2, "id = '" . $id_per2 . "' ");
            } else {
                $array_phanquyen = array(
                    "idtype" => $id_module,
                    "iduser" => $id_user,
                    "active" => $xem,
                    "them"   => $them,
                    "xoa"    => $xoa,
                    "sua"    => $sua,
                );
                $db->insert("tbl_user_active", $array_phanquyen);
            }
        }
        // *** ĐỔI MẬT KHẨU ***
        $post_matkhaimoi   = replace_html(trim(ws_post('matkhaumoi')));
        $post_rematkhaimoi = replace_html(trim(ws_post('rematkhaumoi')));
        if ($post_matkhaimoi != '') {
            if (strlen($post_matkhaimoi) > 5 && filter_var($post_matkhaimoi, FILTER_SANITIZE_STRING)) {
                if (strlen($post_rematkhaimoi) > 5 && filter_var($post_rematkhaimoi, FILTER_SANITIZE_STRING)) {
                    if ($post_matkhaimoi == $post_rematkhaimoi) {
                        $old_lat       = $db->getNameFromID("tbl_user", "lat", "id", $__getid);
                        $passchondoi   = md5(md5($post_rematkhaimoi) . $old_lat);
                        $array_doipass = array("matkhau" => $passchondoi);
                        $db->sqlUpdate("tbl_user", $array_doipass, "id = '" . ws_get('id') . "' ");
                        echo "<script>alert('Mật khẩu đã được thay đổi.')</script>";
                    } else {
                        echo "<script>alert('Mật khẩu mới không khớp.')</script>";
                        echo "<script>location.href='./?op=user&method=frm&id=" . $__getid . "';</script>";
                    }
                } else {
                    echo "<script>alert('Vui lòng nhập lại mật khẩu mới.')</script>";
                    echo "<script>location.href='./?op=user&method=frm&id=" . $__getid . "';</script>";
                }
            } else {
                echo "<script>alert('Mật khẩu phải từ 6 ký tự trở lên')</script>";
                echo "<script>location.href='./?op=user&method=frm&id=" . $__getid . "';</script>";
            }
        }
    } // end if cap nhat
}
echo '<script> location.href="./?op=' . ws_get("op") . '"; </script>';
