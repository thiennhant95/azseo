<?php
$supperadm_getid = $supperadm_sessionid = $nhom_getid = null;
//*** XỬ LÝ PHẦN QUYỀN ***
if( ws_get('id') ) {
    $supperadm_getid     = $db->getNameFromID("tbl_user", "supperadmin", "id", ws_get('id'));
    $nhom_getid          = $db->getNameFromID("tbl_user", "nhom", "id", ws_get('id'));
}
if( get_id_user() ) {
    $supperadm_sessionid = $db->getNameFromID("tbl_user", "supperadmin", "id", get_id_user());
}

if ($supperadm_sessionid < $supperadm_getid) {
    echo '<script language="javascript">
      swal("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
    exit();
}
if ($__getid != '') {
    //**** SỬA ****
    if ($__sua == 0) {
        echo '<script language="javascript">
      swal("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $title_onForm = "[ " . $arraybien['chinhsua'] . " ]";
} else {
    //**** THÊM ****
    if ($__them == 0) {
        echo '<script language="javascript">
      swal("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $title_onForm = "[ " . $arraybien['themmoi'] . " ]";
}
$_id_name = @$__config['id'];
$_getop   = ws_get('op');
$_idtype  = @$__config['type'];
$ACTION   = '';
$CONTENT  = '';
if ( ws_post('cid') ) {
    $_getidSP = ws_post('cid')[0];
}
if ($__getid != '') {
    $__cid       = ws_get('id');
    $s_sql       = "SELECT * from $__table where id = $__getid";
    $d_query     = $db->rawQuery($s_sql);
    $d_query     = $d_query[0];
    $id          = check_select($d_query['id']);
    $tendangnhap = check_select($d_query['tendangnhap']);
    $matkhau     = check_select($d_query['matkhau']);
    $hotendem    = check_select($d_query['hotendem']);
    $ten         = check_select($d_query['ten']);
    $hoten       = $hotendem . "&nbsp;" . $ten;
    $diachi      = check_select($d_query['diachi']);
    $email       = check_select($d_query['email']);
    $dienthoai   = check_select($d_query['sodienthoai']);
    $active      = check_select($d_query['active']);
    $chonnhom    = check_select($d_query['nhom']);
    $chonquyen   = check_select($d_query['supperadmin']);
    $gioitinh    = check_select($d_query['gioitinh']);
    $tinhthanh   = check_select($d_query['tinhthanh']);
    $ngaysinh    = check_select($d_query['ngaysinh']);
    $website     = check_select($d_query['website']);
    $yahoo       = check_select($d_query['yahoo']);
} else {
    $id          = "";
    $idtype      = "";
    $iduser      = "";
    $tendangnhap = "";
    $matkhau     = "";
    $hotendem    = "";
    $ten         = "";
    $hoten       = "";
    $diachi      = "";
    $email       = "";
    $dienthoai   = "";
    $active      = "";
    $chonnhom    = "";
    $chonquyen   = "";
    $gioitinh    = "";
    $tinhthanh   = "";
    $ngaysinh    = "";
    $website     = "";
    $yahoo       = "";
}
//*** VALIDATE FORM ***
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   var form = document.adminForm;
   var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }';
/**
 * Chỉ kiểm tra khi thêm mới thành viên
 */
if (!isset($__getid)) {
    echo '
      else if (form.ten.value.length == 0) {
         form.ten.focus();
         swal(\'\',\'Vui lòng nhập tên đăng nhập\',\'warning\');
      } else if (form.ten.length == 0 || form.ten.value.length < 4) {
         form.ten.focus();
         swal(\'\',\'Tên đăng nhập phải lớn hơn 4 ký tự\',\'warning\');
      }';
    // Kiểm tra trùng nickName
    $s_tendn = "SELECT tendangnhap FROM tbl_user WHERE tendangnhap != '' ";
    $d_tendn = $db->rawQuery($s_tendn);
    if (count($d_tendn) > 0) {
        foreach ($d_tendn as $info_tendn) {
            $value_tendn = $info_tendn['tendangnhap'];
            echo '
            else if (form.ten.value == \'' . $value_tendn . '\'){
               form.ten.focus();
               swal( "", "Tên đăng nhập [ ' . $value_tendn . ' ] đã được sử dụng.", "warning" );
            }';
        }
    }
}
/**
 *
 */
if (!isset($__getid)) {
    echo '
      else if (form.matkhau.value.length == 0) {
         form.matkhau.focus();
         swal(\'\',\'Vui lòng nhập mật khẩu\',\'warning\');
      } else if (form.matkhau.value.length < 6) {
         form.matkhau.focus();
         swal(\'\',\'Mật khẩu phải từ 6 ký tự trở lên\',\'warning\');
      } else if (form.rematkhau.value.length == 0) {
         form.rematkhau.focus();
         swal(\'\',\'Vui lòng xác nhận lại mật khẩu\',\'warning\');
      } else if (form.rematkhau.value != form.matkhau.value) {
         form.rematkhau.focus();
         swal(\'\',\'Mật khẩu không khớp nhau\',\'warning\');
      }';
} else {
    echo '
      else if (form.matkhaumoi.value.length == 0 && form.click_matkhau.checked) {
         form.matkhaumoi.focus();
         swal(\'\',\'Vui lòng nhập mật khẩu mới\',\'warning\');
      } else if (form.matkhaumoi.value.length < 6 && form.click_matkhau.checked) {
         form.matkhaumoi.focus();
         swal(\'\',\'Mật khẩu mới phải từ 6 ký tự trở lên\',\'warning\');
      } else if (form.rematkhaumoi.value.length == 0 && form.click_matkhau.checked) {
         form.rematkhaumoi.focus();
         swal(\'\',\'Vui lòng xác nhận lại mật khẩu mới\',\'warning\');
      } else if (form.rematkhaumoi.value != form.matkhaumoi.value && form.click_matkhau.checked) {
         form.rematkhaumoi.focus();
         swal(\'\',\'Mật khẩu mới không khớp nhau\',\'warning\');
      }';
}
echo '
   else if (form.hoten.value.length == 0) {
      form.hoten.focus();
      swal(\'\',\'Vui lòng nhập tên\',\'warning\');
   } else if (form.hoten.value.length < 2) {
      form.hoten.focus();
      swal(\'\',\'Tên phải lớn hơn 2 ký tự\',\'warning\');
   } else if (form.email.value.length == 0) {
      form.email.focus();
      swal(\'\',\'Vui lòng nhập Email\',\'warning\');
   } else if (!form.email.value.match(mailformat)) {
      form.email.focus();
      swal(\'\',\'Email không hợp lệ\',\'warning\');
   } ';
// Kiểm tra trùng Email
if (!isset($__getid)) {
    $s_tendn = "SELECT email FROM tbl_user WHERE email != '' ";
} else {
    $s_tendn = "SELECT email FROM tbl_user WHERE email != '' AND id != $__getid ";
}
$d_tendn = $db->rawQuery($s_tendn);
if (count($d_tendn) > 0) {
    foreach ($d_tendn as $info_tendn) {
        $value_tendn = $info_tendn['email'];
        echo '
            else if (form.email.value == \'' . $value_tendn . '\'){
            form.email.focus();
            swal("", "Email [ ' . $value_tendn . ' ] đã được sử dụng.","warning" );
            }';
    }
}
//***TRƯỜNG HỢP THỎA TẤT CẢ CÁC ĐIỀU KIỆN CHECK FORM
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
//*** HIỂN THỊ CÁC NÚT CHỨC NĂNG ***
$ACTION .= '
<div class="row_content_top_title">' . @$__config['module_title'] . '</div>
<div class="row_content_top_action" id="user_btn_action">
   <a class="btn btn-primary save" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://webso.vn/helps/?op=' . ws_get('op') . '&act=form&v=2\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//*** HIỂN THỊ COLS TRÁI ***
if ($__getid != "") {
    if ($supperadm_getid < 1) {
        $NotAdmin = ($nhom_getid < 1) ? "hidden" : null;
    }
} else {
    $NotAdmin = null;
}
$CONTENT .= '
<form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
   <div class="col_info box-permission ' . $NotAdmin . ' " id="group-permission">
      <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle ' . $NotAdmin . ' ">' . $arraybien['phanquyenmodule'] . '
            <div class="checkall-per">
                  <label>
                     <input type="checkbox" id="checkAllPer">
                     Check All
                  </label>
            </div>
      </div>
         <div class="panel-body ' . $NotAdmin . ' ">';
//*** Truy vấn module ***
$isSupperAdmin = ($supperadm_getid < 1) ? " AND a.idgroup = 1 " : null;
$s_permission  = "SELECT a.*, b.ten as ten
                           FROM tbl_user_group AS a
                           INNER JOIN tbl_user_group_lang  AS b
                           ON a.id  = b.iddanhmuc
                           WHERE b.idlang = '{$__defaultlang}'
                           " . $isSupperAdmin . "
                           AND a.anhien = 1
                           ORDER BY id ASC
                           ";
$d_permission = $db->rawQuery($s_permission);
if ($supperadm_getid > 0) {
}
if (count($d_permission) > 0) {
    $CONTENT .= '
               <table class="table table-bordered table-hover">
                  <thead>
                     <tr>
                        <th>Tên Module</th>
                        <th>Chọn quyền</th>
                     </tr>
                  </thead>
                  <tbody id="list_checkPer">';
    foreach ($d_permission as $key_permission => $value_permission) {
        $permission_id   = $value_permission['id'];
        $permission_name = $value_permission['ten'];
        //*** CHỈNH SỬA PHÂN QUYỀN
        $checkxem  = "";
        $checkthem = "";
        $checksua  = "";
        $checkxoa  = "";
        if( ws_get('id') ) {
            $s_per2    = "SELECT * FROM tbl_user_active
                            WHERE iduser = " . ws_get('id') . "
                            AND idtype = '{$permission_id}'
                            ORDER BY id ASC";
            $d_per2 = $db->rawQuery($s_per2);
        }
        if (isset($d_per2) && count($d_per2) > 0) {
            $d_per2        = $d_per2[0];
            $per2_id       = $d_per2['id'];
            $per2_idmodule = $d_per2['idtype'];
            $per2_iduser   = $d_per2['iduser'];
            $per2_xem      = $d_per2['active'];
            $per2_them     = $d_per2['them'];
            $per2_sua      = $d_per2['sua'];
            $per2_xoa      = $d_per2['xoa'];
            $checkxem      = ($per2_xem == 1) ? 'checked="checked"' : null;
            $checkthem     = ($per2_them == 1) ? 'checked="checked"' : null;
            $checksua      = ($per2_sua == 1) ? 'checked="checked"' : null;
            $checkxoa      = ($per2_xoa == 1) ? 'checked="checked"' : null;
            @$checkDefault  = ($__cid != "") ? null : 'checked="checked"';
        }
        $CONTENT .= '
                  <tr>
                  <td class="tieude">
                  ';
        $s_line = '';
        $s_line = '<div class="cap1 btn btn-primary"><i>1</i></div>';
        for ($t = 4; $t < 100; $t += 4) {
            if (strlen($permission_id) == $t) {
                $sot         = $t / 4;
                $margin_left = ' style="margin-left:' . (($sot - 1) * 20) . 'px;"';
                $s_line      = '<button ' . $margin_left . ' class="cap' . $sot . ' btn btn-primary"><i>' . $sot . '</i></button>';
                break;
            }
        }
        $CONTENT .= $s_line . '<span class="textcap' . $sot . '">' . $permission_name . '</span>
                  </td>
                     <td width="130">
                     <div class="form-group">
                        <label>
                           <input type="checkbox" ' . $checkxem . ' value="1" name="xem' . $key_permission . '">
                           Xem
                        </label>
                        <label>
                           <input type="checkbox" ' . $checkthem . ' ' . @$checkDefault . ' value="1" name="them' . $key_permission . '">
                           Thêm
                        </label>
                     </div>
                     <label>
                        <input type="checkbox" ' . $checksua . ' ' . @$checkDefault . ' value="1" name="sua' . $key_permission . '">
                        Sửa
                     </label>
                     <label>
                        <input type="checkbox" ' . $checkxoa . ' ' . @$checkDefault . ' value="1" name="xoa' . $key_permission . '">
                        Xóa
                     </label>
                     <input type="hidden" name="id_module' . $key_permission . '" value="' . $permission_id . '">
                     <input type="hidden" name="id_per2' . $key_permission . '" value="' . @$per2_id . '">
                     <input type="hidden" name="tong_module" value="' . count($d_permission) . '">
                     </td>
                     </tr>';
    }
    $CONTENT .= '
                  </tbody>
               </table>
            ';
}
$CONTENT .= '
         </div>
      </div>
   </div>';
/*** update 23/02/2016 ***/
//*** HIỂN THỊ COLS PHẢI ***
$CONTENT .= '
<div class="col_data">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">&nbsp;' . $title_onForm . " --- " . $arraybien['thongtintaikhoan'] . '</div>
      <div class="panel-body">
         <div id="tabs">
            <div class="tab-text frm-user" id="frm-register">';
//*** HIỆN THỊ FORM TẠO THÀNH VIÊN ***
//---Tên đăng nhập
$CONTENT .= '
         <div class="form-group">
            <label class="control-label">' . $arraybien['tendangnhap'] . '</label>
            <span class="star">*</span>';
if ($tendangnhap != "") {
    $CONTENT .= '
                  <div class="input-group tendangnhap-view clearfix">
                     <label class="label label-default">' . $tendangnhap . '</label>
                  </div>
               ';
} else {
    $CONTENT .= '<input type="text" class="form-control" id="tendangnhap" name="ten"  placeholder="' . $arraybien['nhaptendangnhap'] . '" required />';
}
$CONTENT .= '</div> ';
//---Mật khẩu
if ($matkhau != "") {
    $CONTENT .= '
            <div class="checkbox large">
               <label>
                  <input type="checkbox" id="click_matkhau" name="click_matkhau" >
                  ' . $arraybien['doimatkhau'] . '
               </label>
            </div>
            <div id="box_doimatkhau">
               <div class="form-group" >
                  <label class="control-label">' . $arraybien['matkhaumoi'] . '</label>
                  <span class="star">*</span>
                  <input type="password" class="form-control" id="matkhaumoi" name="matkhaumoi" placeholder="' . $arraybien['nhapmatkhaumoi'] . '" />
               </div>
               <div class="form-group">
                  <label class="control-label">' . $arraybien['xacnhanmatkhaumoi'] . '</label>
                  <span class="star">*</span>
                  <input type="password" class="form-control" id="rematkhaumoi" name="rematkhaumoi" placeholder="' . $arraybien['nhaplaimatkhaumoi'] . '" />
               </div>
            </div>
               ';
} else {
    $CONTENT .= '
            <div class="form-group">
               <label class="control-label">' . $arraybien['matkhau'] . '</label>
               <span class="star">*</span>
               <input type="password" class="form-control" id="matkhau" name="matkhau" placeholder="' . $arraybien['nhapmatkhau'] . '" />
            </div>
            <div class="form-group">
               <label class="control-label">' . $arraybien['xacnhanmatkhau'] . '</label>
               <span class="star">*</span>
               <input type="password" class="form-control" id="rematkhau" name="rematkhau" placeholder="' . $arraybien['nhaplaimatkhau'] . '" />
            </div>
            ';
}
//---Họ Tên lots
$CONTENT .= '
         <div class="form-group">
            <label class="control-label">' . $arraybien['hotenlot'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="hotenlot" name="hotenlot" value="' . $hotendem . '" placeholder="' . $arraybien['nhaphotenlot'] . '" />';
$CONTENT .= '</div>';
//---Họ Tên
$CONTENT .= '
         <div class="form-group">
            <label for="hoten" class="control-label">' . $arraybien['ten'] . '</label>
            <span class="star">*</span>';
$CONTENT .= '<input type="text" class="form-control" id="hoten" name="hoten" value="' . $ten . '" placeholder="' . $arraybien['nhapten'] . '" />';
$CONTENT .= '</div> ';
//---Địa chỉ
$CONTENT .= '
         <div class="form-group">
            <label for="diachi" class="control-label">' . $arraybien['diachi'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="diachi" name="diachi" value="' . $diachi . '" placeholder="' . $arraybien['nhapdiachi'] . '" />';
$CONTENT .= '</div>';
//---Email
$CONTENT .= '
         <div class="form-group">
            <label for="email" class="control-label">' . $arraybien['email'] . '</label>
            <span class="star">*</span>';
$CONTENT .= '<input type="text" class="form-control" id="email" name="email" value="' . $email . '" placeholder="' . $arraybien['nhapemail'] . '" />';
$CONTENT .= '</div>';
//---Điện thoại
$CONTENT .= '
         <div class="form-group">
            <label for="dienthoai" class="control-label">' . $arraybien['dienthoai'] . '</label>';
$CONTENT .= '<input type="text" onkeyup="CheckNumber(this)" class="form-control" id="dienthoai" value="' . $dienthoai . '" name="dienthoai" placeholder="' . $arraybien['nhapdienthoai'] . '" />';
$CONTENT .= '</div>';
//---Tỉnh Thành
$CONTENT .= '
         <div class="form-group">
            <label for="tinhthanh" class="control-label">' . $arraybien['tinhthanh'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="tinhthanh" value="' . $tinhthanh . '" name="tinhthanh" placeholder="' . $arraybien['nhaptinhthanh'] . '" />';
$CONTENT .= '</div>';
//---Ngày Sinh
$CONTENT .= '
         <div class="form-group">
            <label for="ngaysinh" class="control-label">' . $arraybien['ngaysinh'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="ngaysinh" value="' . $ngaysinh . '" name="ngaysinh" placeholder="' . $arraybien['nhapngaysinh'] . '" />';
$CONTENT .= '</div>';
//---WEBSITE
$CONTENT .= '
         <div class="form-group">
            <label for="website" class="control-label">' . $arraybien['website'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="website" value="' . $website . '" name="website" placeholder="' . $arraybien['nhapwebsite'] . '" />';
$CONTENT .= '</div>';
//---YAHOO
$CONTENT .= '
         <div class="form-group">
            <label for="yahoo" class="control-label">' . $arraybien['yahoo'] . '</label>';
$CONTENT .= '<input type="text" class="form-control" id="yahoo" value="' . $yahoo . '" name="yahoo" placeholder="' . $arraybien['nhapyahoo'] . '" />';
$CONTENT .= '</div>';
if ($supperadm_sessionid == 1) {
    //--Chọn Persion
    $CONTENT .= '
         <div class="form-group">
            <label for="group-admin" class="form-label">' . $arraybien['chonquyen'] . '</label>
            <div class="form-group">
               <select name="chonquyen" id="changechonquyen" class="form-control">';
    if ($chonquyen == 0) {
        $CONTENT .= '
                  <option value="0" selected="selected"> ' . $arraybien['admin'] . ' </option>
                  <option value="1"> ' . $arraybien['supperadmin'] . ' </option>';
    } else {
        $CONTENT .= '
                  <option value="0"> ' . $arraybien['admin'] . ' </option>
                  <option value="1" selected="selected"> ' . $arraybien['supperadmin'] . ' </option>';
    }
    $CONTENT .= '
               </select>
            </div>
         </div>
         ';
}
//-- Giới tính
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['gioitinh'] . '</div>
               <div class="group-active">';
if ($gioitinh == 0) {
    $CONTENT .= '
                  <label><input type="radio" checked="checked" name="gioitinh" value="0" />' . $arraybien['nam'] . '</label>
                  <label><input type="radio" name="gioitinh" value="1">' . $arraybien['nu'] . '</label>
                  ';
} else {
    $CONTENT .= '
                  <label><input type="radio" name="gioitinh" value="0" />' . $arraybien['nam'] . '</label>
                  <label><input type="radio" checked="checked" name="gioitinh" value="1">' . $arraybien['nu'] . '</label>
                  ';
}
$CONTENT .= '</div></div></div>';
//-- Chọn Nhóm
$CONTENT .= '
            <div class="form-group" id="box-chonnhom">
               <div class="input-group">
                  <div class="input-group-addon">' . $arraybien['chonnhom'] . '</div>
                  <div class="group-active">';
if ($chonnhom == 0) {
    $CONTENT .= '
                     <label><input type="radio" checked="checked" name="chonnhom" value="0" />' . $arraybien['nguoidung'] . '</label>
                     <label><input type="radio" name="chonnhom" value="1">' . $arraybien['quantri'] . '</label>
                     ';
} else {
    $CONTENT .= '
                     <label><input type="radio" name="chonnhom" value="0" />' . $arraybien['nguoidung'] . '</label>
                     <label><input type="radio" checked="checked" name="chonnhom" value="1">' . $arraybien['quantri'] . '</label>
                     ';
}
$CONTENT .= '</div></div></div>';
//--Active
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['kichhoat'] . '</div>
               <div class="group-active">';
if ($active == 0) {
    $CONTENT .= '
                  <label><input type="radio" name="active" value="1">Actived</label>
                  <label><input type="radio" checked="checked" name="active" value="0">Not Actived</label>
                  ';
} else {
    $CONTENT .= '
                  <label><input type="radio" checked="checked" name="active" value="1">Actived</label>
                  <label><input type="radio" name="active" value="0">Not Actived</label>
                  ';
}
$CONTENT .= '</div></div></div>';
$CONTENT .= '
               <input type="hidden" name="validate_user_frm" value="" />
               <input type="hidden" value="' . $__getid . '" name="id">
               <input type="hidden" value="" name="cid[]">
               <input type="hidden" value="0" name="version">
               <input type="hidden" value="0" name="mask">
               <input type="hidden" value="' . $__getop . '" name="op">
               <input type="hidden" value="" name="task">
               <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clear"></div>
</form>';
//*** GÁN VÀO TEMPLATES ***
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
