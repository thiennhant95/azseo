<?php
$_id_name = $__config['id'];
$s        = "SELECT a.*,b.ten
          FROM {$__table} AS a
          INNER JOIN {$__tablenoidung} AS b On a.id = b.idtype
          where idlang = '{$__defaultlang}' ";
$id       = ws_get('key');
$ten      = ws_get('ten');
$loai     = ws_get('loai');



$ngaydang = ws_get('ngaydang');
$anhien   = ws_get('anhien');
$get_page = ws_get('page') ? ws_get('page') : 1;
if ($__gettukhoa != '') {
    $s = $s . " and (ten LIKE '%{$__gettukhoa}%' OR a.id LIKE '%{$__gettukhoa}%' ) ";
}
if ($__getidtype != '') {
    $s = $s . " and a.id like'" . $__getidtype . "%' ";
}
if ($__getanhien != '') {
    $s = $s . " AND anhien = '{$__getanhien}' ";
}
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . ' order by  ' . $__sortname . '  ' . $__sortcurent;
} else {
    $s = $s . ' order by  thutu  asc ';
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = $db->rawQuery($s_counttotal);
$total_row    = count($d_counttotal);
$page         = (int) !ws_get('page') ? 1 : ws_get('page');
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . ' ';
}
$data      = $db->rawQuery($s);
$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
?>
<div class="row_content_top">
   <div class="row_content_top_title"><?php echo $__config['module_title']; ?></div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder('<?php echo $saveorder - 1; ?>', 'saveorder')">
               <i class="fa fa-sort-amount-desc"></i> <?php echo $arraybien['sapxep']; ?>
        </a>
        <a class="btn btn-primary" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('publish')}" href="#">
            <i class="fa fa-check-circle"></i> <?php echo $arraybien['bat']; ?>
        </a>
        <a class="btn btn-success" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('unpublish')}" href="#">
          <i class="fa fa-check-circle color-black"></i> <?php echo $arraybien['tat']; ?>
        </a>
      <?php
if ($__xoa == 1) {
    ?>
        <a class="btn btn-info" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton_del('remove')}" href="#">
        <i class="fa fa-times-circle color-black"></i> <?php echo $arraybien['xoa']; ?>
        </a>
        <?php

}
?>
      <?php
if ($__them == 1) {
    ?>
        <a class="btn btn-danger" href="./?op=<?php echo $__getop; ?>&method=frm">
         <i class="fa fa-plus-circle"></i> <?php echo $arraybien['themmoi']; ?>
        </a>
      <?php

}
?>
        <a class="btn btn-warning" onclick="popupWindow('http://supportv2.webso.vn/?op=<?php echo ws_get('op'); ?>&v=2', 'Trợ giúp', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> <?php echo $arraybien['trogiup']; ?>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="row_content_content">
<?php
$row_total = $db->getValue($__table, 'count(*)');
if (ws_post('filter_order_Dir') == 'asc') {
    $filter_order_Dir = 'desc';
}
?>
<form  id="frm" name="adminForm" method="post" action="./?op=<?php echo $__getop; ?>&method=query">
<?php
echo '
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" name="this_op" value="' . $__getop . '">
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>';
?>
<div class="form-group formsearchlist">
      <label for="focusedInput" class="float-left line-height30" ><?php echo $arraybien['tukhoa']; ?>: </label>
      <input type="text" title="<?php echo $arraybien['dieukientimkiemdanhmuc']; ?>" style="width:140px;" class="form-control col-xs-3" value="<?php echo $__gettukhoa; ?>" id="focusedInput" name="tukhoa">
    <?php
echo '
     <select id="anhien" name="anhien" class="form-control trangthai">
      <option value="">' . $arraybien['tatca'] . '</option>';
for ($i = 0; $i < count($_arr_anhien); ++$i) {
    if ($i == $__getanhien && $__getanhien != '') {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    }
}
echo '</select>';
echo ' <div class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '&page=' . $__getpage . '&tukhoa=\'+document.adminForm.tukhoa.value+\'&anhien=\'+document.adminForm.anhien.value"><i class="fa fa-search"></i> ' . $arraybien['tim'] . '</div>
   <div style="margin-left:10px;" class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '\'"><i class="fa fa-reply"></i> ' . $arraybien['botimkiem'] . '</div>';
?>
</div>
<?php
if ($total_row > 0) {
    echo '<div id="paged">';
    echo Pages($total_row, $perpage, $path_page);
    echo '</div>';
}
?>
      <div class="table-responsive"><table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table" >
      <thead>
        <tr class="panel-heading">
          <th width="30"> # </th>
          <th width="5">
          <?php
if (isset($_SESSION['__limit'])) {
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
}
?>
             <input type="checkbox" onclick="checkAll(<?php echo $pagetotal; ?>);" value="" name="toggle">
          </th>
          <?php
if ($__config['thutu'] == 1) {
    ?>
          <th width="90" >
          <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=thutu">
          <?php echo $arraybien['sapxep']; ?>
          <?php
if ($__sortname == 'thutu') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }?>
          </a>
          </th>
          <?php

} // end sort
?>
          <?php
if ($__config['ten'] == 1) {
    ?>
          <th>
           <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=ten">
          <?php echo $arraybien['tieude']; ?>
          <?php
if ($__sortname == 'ten') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }?>
          </a>
          </th>
          <?php

}
?>
          <?php
if ($__config['datatype'] == 1) {
    ?>
          <th width="270">
          <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=idtype">
          <?php echo $arraybien['kieudulieu']; ?>
          <?php
if ($__sortname == 'idtype') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }?>
          </a>
          </th>
          <?php

}
?>
          <?php
if ($__config['img'] == 1) {
    ?>
          <th width="70"><?php echo $arraybien['hinh']; ?></th>
          <?php

}
?>

<?php
if ($__config['keyname'] == 1) {
    // action
    ?>
          <th width="150">keyname</th>
        <?php

}
?>

          <?php
if ($__config['action'] == 1) {
    // action
    ?>
          <th width="90"><?php echo $arraybien['thaotac']; ?></th>
        <?php

}
?>

          <?php
if ($__config['anhien'] == 1) {
    ?>
          <th width="80">
            <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=anhien">
          <?php echo $arraybien['hienthi']; ?>
          <?php
if ($__sortname == 'anhien') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }?>
          </a>
          </th>
          <?php

}
?>
        </tr>
        </thead>
<?php
echo '<tbody '.(!empty(ws_get('tukhoa')) || ws_get('anhien')!="" || ws_get('idtype') ? null : 'id="keotha_row"').'>';
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        ++$i;
        if ($i % 2 == 0) {
            $row = 'row0';
        } else {
            $row = 'row1';
        }
        $id = $d['id'];?>
        <tr data-thutu="<?php echo $d['thutu']; ?>" data-id="<?php echo $d['id']; ?>" data-vitri="<?php echo $key; ?>" id="sectionid_<?php echo $i; ?>" class="content_tr <?php echo $row; ?>">
            <td title="<?php echo $d['id']; ?>">
               <?php echo $i; ?>
            </td>
            <td>
              <input id="cb<?php echo $i - 1; ?>" type="checkbox" onclick="isChecked(this.checked);" value="<?php echo $d['id']; ?>" name="cid[]">
              <input type="hidden" name="img[<?php echo $d['id']; ?>]" id="img<?php echo $i - 1; ?>" value="<?php echo $d['hinh']; ?>" />              <input type="hidden" name="file[<?php echo $d['id']; ?>]" id="file<?php echo $i - 1; ?>" value="<?php echo $d['file']; ?>" />
            </td>
          <?php
if ($__config['thutu'] == 1) {
            ?>
<td class="order">
<?php
echo '<input data-id="' . $d['id'] . '" onchange="ChangeStage(\'ajax.php?op=' . $__getop . '&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
?>
</td>
           <?php

        } // end thu tu
        ?>
           <?php
if ($__config['ten'] == 1) {
            // ten
            ?>
            <td class="tieude">
            <a href="<?php echo './?op=' . ws_get('op') . '&method=frm&id=' . $d['id']; ?>"  title="<?php echo $d['ten']; ?>" >
            <?php
echo '<span class="textcap' . $sot . '">' . $d['ten'] . '</span>'; ?>
            </a></td>
           <?php

        } // end ten
        ?>
            <?php
if ($__config['datatype'] == 1) {
            // thuoc modules
            ?>
            <td class="align-center font-bold">
            <?php echo $_arr_datatype[$d['datatype']]; ?>
            </td>
             <?php

        } // end idtype
        ?>
           <?php
if ($__config['img'] == 1) {
            // Img
            ?>
            <td>
               <?php if ($d['img'] != '') {
                ?>
                  <a  onclick="window.open(this.href, '', 'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50'); return false;" href="../uploads/danhmuc/<?php echo $d['img']; ?>"><img src="../uploads/danhmuc/thumb/<?php echo $d['img']; ?>" width="60" /></a>
                <?php
} else {
                ?>
                  <img src="application/templates/images/noimage.png" alt="" width="60" />
                <?php
}?>
            </td>
           <?php

        } // end img
        ?>

<?php
if ($__config['keyname'] == 1) {
            ?>
    <td class="align-center font-bold">
      <?php echo $d['keyname']; ?>
    </td>
    <?php

        }?>

            <?php
if ($__config['action'] == 1) {
            // action
            ?>
            <td>
            <ul class="pagination">
            <?php
if ($__sua == 1) {
                ?>
               <li><a data-toggle="tooltip" title="<?php echo $arraybien['sua']; ?>" href="<?php echo './?op=' . $__getop . '&method=frm&id=' . $d['id']; ?>" >
                   <i class="fa fa-pencil"></i>
               </a></li>
                <?php

            }?>
            <?php
if ($__xoa == 1) {
                ?>
               <li class="active" ><a data-toggle="tooltip" title="<?php echo $arraybien['xoadongnay']; ?>" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del('cb<?php echo $i - 1; ?>','remove')" >
                     <i class="fa fa-times-circle color-white"></i>
                </a></li>
                <?php

            }?>
            </ul>
            </td>
            <?php

        } // end action
        ?>

<?php
// an hien
        if ($__config['anhien'] == 1) {
            ?>
            <td class="align-center">
              <?php
echo '<div id="anhien' . $id . '">';
            $itemstage = '<i title="' . $arraybien['tat'] . '" class="fa fa-eye-slash color-red"></i>';
            if ($d['anhien'] == 1) {
                $itemstage = '<i title="' . $arraybien['bat'] . '" class="fa fa-eye color-black"></i>';
            }
            echo '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'./ajax.php?op=' . $__getop . '&id=' . $id . '&name=anhien&value=' . $d['anhien'] . '\',\'anhien' . $id . '\')">' . $itemstage . '</a> </span>';
            echo '</div>'; ?>
            </td>
            <?php

        } // end an hien
        ?>
          </tr>
          <?php

    } // end foreach
} // end count
?>
        </tbody>
      </table></div>
            <div class="pagechoice">
             <?php echo $arraybien['tongso']; ?>: <span style="color:#FF0000; font-weight:bold">
                  <?php echo $total_row; ?>
                  </span>
             <?php echo $arraybien['hienthi']; ?> #
             <?php
echo '
              <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage"  id="limit" name="limit">';
for ($i = 1; $i <= count($_arr_listpage); ++$i) {
    if ($i == $_SESSION['__limit']) {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    }
}
echo '</select>';
?>
         <div class="float-right">
            <?php
if ($_SESSION['__limit'] != '') {
    //echo $row_total;
    if ($total_row > 0) {
        echo Pages($total_row, $perpage, $path_page);
    }
}
?>
            </div>
         </div>

</form>
</div>