<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Logo in lên hình sản phẩm",
    "table"           => 'tbl_banner',
    "tablenoidung"    => 'tbl_banner_lang',
    "id"              => 'id',
    "idtype"          => 0,
    "thutu"           => 1,
    "loai"            => array_search("sanpham_logo",$_arr_loai_banner),
    "anhien"          => 1,
    "ngaytao"         => 0,
    "ngaycapnhat"     => 0,
    "ten"             => 1,
    "hinh"            => 1,
    "linkhinh"        => 0,
    "link"            => 0,
    "manhung"         => 0,
    "target"          => 0,
    "rong"            => 0,
    "cao"             => 0,
    "action"          => 1,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/logo/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
