<?php
$_id_name = $__config['id'];
$s        = "SELECT a.*, b.ten, b.hinh
          FROM {$__table} AS a
          INNER JOIN {$__tablenoidung} AS b On a.id  = b.idtype
          where b.idlang = '{$__defaultlang}'
          and a.loai = {$__config['loai']} ";
$id       = ws_get('key');
$ten      = ws_get('ten');
$loai     = ws_get('loai');
$ngaydang = ws_get('ngaydang');
$anhien   = ws_get('anhien');
$get_page = ws_get('page') ? ws_get('page') : 1;
if ($__gettukhoa != '') {
    $s = $s . " and (ten LIKE '%{$__gettukhoa}%' OR a.id LIKE '%{$__gettukhoa}%' ) ";
}
if ($__getidtype != '') {
    $s = $s . " and a.idtype = '" . $__getidtype . "%' ";
}
if ($__getanhien != '') {
    $s = $s . " AND anhien = '{$__getanhien}' ";
}
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . " order by  " . $__sortname . "  " . $__sortcurent;
} else {
    $s = $s . " order by  a.thutu  asc ";
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = $db->rawQuery($s_counttotal);
$total_row    = count($d_counttotal);
$page         = (int) (!ws_get("page") ? 1 : ws_get("page"));
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . " ";
}
$data      = $db->rawQuery($s);
$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
?>
<div class="row_content_top">
   <div class="row_content_top_title"><?php echo $__config['module_title']; ?></div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder('<?php echo $saveorder - 1; ?>', 'saveorder')">
               <i class="fa fa-sort-amount-desc"></i> <?php echo $arraybien['sapxep']; ?>
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('publish')}" href="#">
            <i class="fa fa-check-circle"></i> <?php echo $arraybien['bat']; ?>
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('unpublish')}" href="#">
          <i class="fa fa-check-circle color-black"></i> <?php echo $arraybien['tat']; ?>
        </a>
      <?php
if ($__xoa == 1) {
?>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton_del('remove')}" href="#">
        <i class="fa fa-times-circle color-black"></i> <?php echo $arraybien['xoa']; ?>
        </a>
        <?php
}
?>
      <?php
if ($__them == 1) {
?>
        <a class="btn btn-primary" href="./?op=<?php echo $__getop; ?>&method=frm">
         <i class="fa fa-plus-circle"></i> <?php echo $arraybien['themmoi']; ?>
        </a>
      <?php
}
?>
        <a class="btn btn-warning" onclick="popupWindow('http://supportv2.webso.vn/?op=<?php echo ws_get('op'); ?>', 'Trợ giúp', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> <?php echo $arraybien['trogiup']; ?>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="row_content_content">
<?php
$row_total = $db->getValue($__table, "count(*)");
if (ws_post('filter_order_Dir') == 'asc') {
    $filter_order_Dir = 'desc';
}
?>
<form  id="frm" name="adminForm" method="post" action="./?op=<?php echo $__getop; ?>&method=query">
<?php
echo '
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>';
?>
<div class="form-group formsearchlist">
      <label for="focusedInput" class="float-left line-height30" ><?php echo $arraybien['tukhoa']; ?>: </label>
      <input type="text" title="<?php echo $arraybien['dieukientimkiemdanhmuc']; ?>" style="width:140px;" class="form-control col-xs-3" value="<?php echo $__gettukhoa; ?>" id="focusedInput" name="tukhoa">
    <?php
$sql2 = "select a.id, b.ten
                from tbl_banner_type As a
                inner join tbl_banner_type_lang As b On a.id =  b.idtype
                where b.idlang = '{$__defaultlang}'
                order by a.thutu asc ";
$compare_id = substr($id, 0, strlen($id) - 4);
$option1    = '<option value="">' . $arraybien['danhmuc'] . '</option>';
$danhmuc .= $db->createComboboxDequySql("idtype", $option1, "form-control danhmuc", "", $sql2, "ten", "id", $__getidtype);
$danhmuc .= '<input type="hidden" name="hi_parenid" value="' . $compare_id . '" />';
echo $danhmuc;
echo '
     <select id="anhien" name="anhien" class="form-control trangthai">
      <option value="">' . $arraybien['tatca'] . '</option>';
for ($i = 0; $i < count($_arr_anhien); $i++) {
    if ($i == $__getanhien && $__getanhien != '') {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_anhien[$i] . '</option>';
    }
}
echo '</select>';
echo ' <div class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '&page=' . $__getpage . '&tukhoa=\'+document.adminForm.tukhoa.value+\'&idtype=\'+document.adminForm.idtype.value+\'&anhien=\'+document.adminForm.anhien.value"><i class="fa fa-search"></i> ' . $arraybien['tim'] . '</div>
   <div style="margin-left:10px;" class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '\'"><i class="fa fa-reply"></i> ' . $arraybien['botimkiem'] . '</div>';
?>
</div>
         <?php if ($total_row > 0) {echo Pages($total_row, $perpage, $path_page);}
?>
<div class="table-responsive">
      <table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table">
      <thead>
        <tr class="panel-heading" >
          <th width="30"> # </th>
          <th width="5">
<?php
if (isset($_SESSION['__limit'])) {
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
}
?>
             <input type="checkbox" onclick="checkAll(<?php echo $pagetotal; ?>);" value="" name="toggle">
          </th>
<?php
if ($__config['thutu'] == 1) {
?>
          <th width="90" >
          <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=thutu">
<?php echo $arraybien['sapxep']; ?>
<?php
if ($__sortname == 'thutu') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
?>
          </a>
          </th>
<?php
} // end sort
?>
<?php
if ($__config['ten'] == 1) {
?>
          <th>
           <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=ten">
<?php echo $arraybien['tieude']; ?>
<?php
if ($__sortname == 'ten') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
?>
          </a>
          </th>
<?php
}
?>
<th>Vị trí logo</th>
<?php
if ($__config['idtype'] == 1) {
?>
          <th width="130">
          <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=idtype">
<?php echo $arraybien['thuocnhom']; ?>
<?php
if ($__sortname == 'idtype') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
?>
          </a>
          </th>

<?php
}
?>
<?php
if ($__config['hinh'] == 1) {
?>
          <th width="70"><?php echo $arraybien['hinh']; ?></th>
<?php
}
?>
<?php
if ($__config['action'] == 1) // action
{
?>
          <th width="90"><?php echo $arraybien['thaotac']; ?></th>
        <?php
}
?>
<?php
if ($__config['anhien'] == 1) {
?>
          <th width="80">
            <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=anhien">
<?php echo $arraybien['hienthi']; ?>
<?php
if ($__sortname == 'anhien') {
        if ($__sortcurent == 'desc') {
            echo '<i class="fa fa-sort-amount-desc color-black"></i>';
        } else {
            echo '<i class="fa fa-sort-amount-asc color-black"></i>';
        }
    }
?>
          </a>
          </th>
<?php
}
?>
<?php
if ($__config['chucnangkhac'] == 1) {
?>
         <th style=" width:480px !important;"><?php echo $arraybien['chucnangkhac']; ?></th>
        <?php
}
?>
        </tr>
        </thead>
        <tbody>
<?php
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        $i++;
        if ($i % 2 == 0) {
            $row = "row0";
        } else {
            $row = "row1";
        }
        $id = $d['id'];
?>
          <tr id="sectionid_<?php echo $i; ?>" class="<?php echo $row; ?>">
            <td title="<?php echo $d['id']; ?>">
<?php echo $i; ?>
            </td>
            <td>
              <input id="cb<?php echo $i - 1; ?>" type="checkbox" onclick="isChecked(this.checked);" value="<?php echo $d['id']; ?>" name="cid[]">
              <input type="hidden" name="img[<?php echo $d['id']; ?>]" id="img<?php echo $i - 1; ?>" value="<?php echo $d['hinh']; ?>" />              <input type="hidden" name="file[<?php echo $d['id']; ?>]" id="file<?php echo $i - 1; ?>" value="<?php echo $d['file']; ?>" />
            </td>
<?php
if ($__config['thutu'] == 1) {
?>
               <td class="order">
<?php
if (strlen($d['id']) == 8) {
                echo '
                <input onchange="ChangeStage(\'ajax.php?op='.$__getop.'&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10" style="background-color:#f0f0f0; text-align: center" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
            } else
            if (strlen($d['id']) == 12) {
                echo '
                <input onchange="ChangeStage(\'ajax.php?op='.$__getop.'&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10"  class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
            } else
            if (strlen($d['id']) == 16) {
                echo '
                <input onchange="ChangeStage(\'ajax.php?op='.$__getop.'&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10"  class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
            } else {
                echo '
                <input onchange="ChangeStage(\'ajax.php?op='.$__getop.'&id=' . $d['id'] . '&name=thutu&value=\'+this.value)" type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['thutu'] . '" size="3" name="order[]">';
            }
?>
            </td>
<?php
} // end thu tu
?>
<?php
if ($__config['ten'] == 1) // ten
        {
?>
            <td class="tieude">
            <a href="<?php echo './?op=' . ws_get('op') . '&method=frm&id=' . $d['id']; ?>"  title="<?php echo $d['ten']; ?>" >
<?php
$s_line = '';
            $s_line = '<div class="cap1 btn btn-primary"><i>1</i></div>';
            for ($t = 4; $t < 100; $t += 4) {
                if (strlen($d['id']) == $t) {
                    $sot         = $t / 4;
                    $margin_left = ' style="margin-left:' . (($sot - 1) * 20) . 'px;" ';
                    $s_line      = '<button ' . $margin_left . ' class="cap' . $sot . ' btn btn-primary"><i>' . $sot . '</i></button>';
                    break;
                }
            }
            echo $s_line . '<span class="textcap' . $sot . '">' . $d['ten'] . '</span>';
?>
            </a></td>
<?php
} // end ten
?>
        <td><?php echo $_arr_vitrilogo[$d['iddanhmuc']]; ?></td>
<?php
if ($__config['idtype'] == 1) // thuoc modules
        {
?>
            <td class="align-center font-bold">
<?php echo $db->getNameFromID("tbl_banner_type_lang", "ten", "idtype", "'" . $d['idtype'] . "' and idlang= '{$__defaultlang}'"); ?>
            </td>
<?php
} // end idtype
?>
<?php
if ($__config['hinh'] == 1) // Img
        {
?>
            <td>
<?php
$hinh = $db->getNameFromID("tbl_banner_lang", "hinh", "idtype", "'" . $d['id'] . "' and idlang = '{$__defaultlang}'");
            if ($hinh != '') {?>
                  <a  onclick="window.open(this.href, '', 'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50'); return false;" href="<?php echo $__config['path_img'] . $hinh; ?>"><img src="<?php echo $__config['path_img'] . $hinh; ?>" width="60" /></a>
<?php } else {?>
                  <img src="application/templates/images/noimage.png" alt="" width="60" />
<?php }
?>
            </td>
<?php
} // end img
?>
<?php
if ($__config['action'] == 1) // action
        {
?>
            <td>
            <ul class="pagination">
<?php
if ($__sua == 1) {
?>
               <li><a data-toggle="tooltip" style="background: #337ab7; border-color: #337ab7;" title="<?php echo $arraybien['sua']; ?>" href="<?php echo './?op=' . $__getop . '&method=frm&id=' . $d['id']; ?>" >
                   <i class="fa fa-pencil color-white"></i>
               </a></li>
<?php
}
?>
<?php
if ($__xoa == 1) {
?>
               <li class="active" ><a data-toggle="tooltip" title="<?php echo $arraybien['xoadongnay']; ?>" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del('cb<?php echo $i - 1; ?>','remove')" >
                     <i class="fa fa-trash-o color-white"></i>
                </a></li>
<?php
}
?>
            </ul>
            </td>
<?php
} // end action
?>
<?php
// an hien
        if ($__config['anhien'] == 1) {
?>
            <td class="align-center">
<?php
echo '<div id="anhien' . $id . '">';
            $itemstage = '<i title="' . $arraybien['tat'] . '" class="fa fa-eye-slash color-red"></i>';
            if ($d['anhien'] == 1) {
                $itemstage = '<i title="' . $arraybien['bat'] . '" class="fa fa-eye color-black"></i>';
            }
            echo '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=' . $d['anhien'] . '\',\'anhien' . $id . '\')">' . $itemstage . '</a> </span>';
            echo '</div>';
?>
            </td>
<?php
} // end an hien
?>
      <?php
if ($__config['chucnangkhac'] == 1) // chuc nang khac
        {
?>
            <td>
<?php
if ($__config['home'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=home">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="home' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['home'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=home&value=' . $d['home'] . '\',\'home' . $id . '\')">' . $arraybien['trangchu'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } // config homepage
            // col top
            if ($__config['coltop'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=coltop">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="coltop' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['coltop'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=coltop&value=' . $d['coltop'] . '\',\'coltop' . $id . '\')">' . $arraybien['tren'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config top
            // col bottom
            if ($__config['colbottom'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=colbottom">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="colbottom' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['colbottom'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=colbottom&value=' . $d['colbottom'] . '\',\'colbottom' . $id . '\')">' . $arraybien['duoi'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config bottom
            // col main
            if ($__config['main'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li >
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=main">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="main' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['main'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=main&value=' . $d['main'] . '\',\'main' . $id . '\')">' . $arraybien['main'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config main
            // col colleft
            if ($__config['colleft'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li>
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=colleft">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="colleft' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['colleft'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=colleft&value=' . $d['colleft'] . '\',\'colleft' . $id . '\')">' . $arraybien['trai'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config colleft
            // col colright
            if ($__config['colright'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li>
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=colright">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="colright' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['colright'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=colright&value=' . $d['colright'] . '\',\'colright' . $id . '\')">' . $arraybien['phai'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config colright
            // col baiviet
            if ($__config['baiviet'] == 1) {
                echo '<ul class="pagination pagination_action">
               <li>
                  <a data-toggle="tooltip" title="' . $arraybien['nhapchuotdesapxeptheocotnay'] . '" href="' . $path_sort . '&sortname=baiviet">';
                if ($__sortcurent == 'desc') {
                    echo '<i class="fa fa-sort"></i>';
                } else {
                    echo '<i class="fa fa-sort"></i>';
                }
                echo '</a>
               </li>
               <li id="baiviet' . $id . '"  >';
                $itemhome = '<i class="fa fa-times-circle color-red"></i>';
                if ($d['baiviet'] == 1) {
                    $itemhome = '<i class="fa fa-check-circle"></i>';
                }
                echo '<a class="cursor-pointer" data-toggle="tooltip" title="' . $arraybien['clickdebathoactatchucnangnay'] . '"" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=baiviet&value=' . $d['baiviet'] . '\',\'baiviet' . $id . '\')">' . $arraybien['baiviet'] . ' ' . $itemhome . '</a>';
                echo '</li>
         </ul>';
            } //end config baiviet
?>
      </td>
      <?php
} // end chuc nang khac
?>
          </tr>
<?php
} // end foreach
} // end count
?>
        </tbody>
      </table></div>
            <div class="pagechoice">
<?php echo $arraybien['tongso']; ?>: <span style="color:#FF0000; font-weight:bold">
<?php echo $total_row; ?>
                  </span>
<?php echo $arraybien['hienthi']; ?> #
<?php
echo '
              <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage"  id="limit" name="limit">';
for ($i = 1; $i <= count($_arr_listpage); $i++) {
    if ($i == $_SESSION['__limit']) {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    }
}
echo '</select>';
?>
         <div class="float-right">
<?php
if ($_SESSION['__limit'] != '') {
    //echo $row_total;
    if ($total_row > 0) {echo Pages($total_row, $perpage, $path_page);}
}
?>
            </div>
         </div>

</form>
</div>