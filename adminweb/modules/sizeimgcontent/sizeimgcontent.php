<?php
use Lazer\Classes\Database as Lazer;
$__config    = array(
    "module_title"    => "Kích thước nội dung",
    "table"           => 'sizeimgcontent',
    "tablenoidung"    => '',
    "id"              => 'id',
    "idtype"          => 1,
    "loai"            => 1,
    "thutu"           => 1,
    "ten"             => 1,
    "anhien"          => 1,
    "action"          => 1,
    "dinhdanh"      => 1,
    "add_item"        => 1,
    "errorreporting"  => 1,
    "date"            => 1,
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300,
);
$__table        = $__config['table'];
create_dir(LAZER_DATA_PATH, false);
// CREATE TABLE

try{
    \Lazer\Classes\Helpers\Validate::table($__table)->exists();
} catch(\Lazer\Classes\LazerException $e){
    Lazer::create($__table, [
        'id'             => 'integer',
        'chonchieurong'  => 'integer',
        'chonchieucao'   => 'integer',
        'chieurong'      => 'integer',
        'chieucao'       => 'integer',
        'chatluong'      => 'integer',
        'canvas'         => 'integer',
        'border'         => 'integer',
        'bordercolor'    => 'string',
        'color'          => 'string',
        'chonchieurong2' => 'integer',
        'chonchieucao2'  => 'integer',
        'chieurong2'     => 'integer',
        'chieucao2'      => 'integer',
        'chatluong2'     => 'integer',
    ]);
}

if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_frm.php";
}
