<?php
use Lazer\Classes\Database as Lazer;
//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Process Thêm mới
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}

$data = Lazer::table($__table)->find();

$chatluonganh = $id = $kichthuocanh = $ACTION = $CONTENT = null;
if( $data->count() ) {
    $id = $data->id;
    $chonchieurong  = $data->chonchieurong;
    $chonchieucao   = $data->chonchieucao;
    $chieurong      = $data->chieurong;
    $chieucao       = $data->chieucao;
    $chatluong      = $data->chatluong;
    $canvas         = $data->canvas;
    $color          = $data->color;
    $border         = $data->border;
    $bordercolor    = $data->bordercolor;
    $chonchieurong2 = $data->chonchieurong2;
    $chonchieucao2  = $data->chonchieucao2;
    $chieurong2     = $data->chieurong2;
    $chieucao2      = $data->chieucao2;
    $chatluong2     = $data->chatluong2;
}

echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
    var form = document.adminForm;
   // do field validation
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }';
// Hoàn thành validate ==> Submit frm
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
   <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
   <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . $id . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">';
//********** col right ********** //
$CONTENT .= '
<div class="col_data full">
<div class="container-fluid">
   <div class="row">

        <div class="col-sm-12 _left">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Cấu hình ảnh cho sản phẩm
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Cắt hình
                                    </div><!-- /.panel-title -->
                                </div><!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="chonchieurong" value="1" '.(
                                                            $chonchieurong ? 'checked' : null
                                                        ).' onchange="check_and_show(this, \'chieurong\')" /> Cắt theo chiều rộng
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="chonchieucao" value="1" '.(
                                                            $chonchieucao ? 'checked' : null
                                                        ).' onchange="check_and_show(this, \'chieucao\')" /> Cắt theo chiều cao
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <br/>
                                                        Chất lượng hình
                                                    </label>
                                                </div>

                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="canvas" value="1" '.(
                                                            $canvas ? 'checked' : null
                                                        ).' onchange="check_and_show(this, \'canvas\')" /> Canvas
                                                    </label>
                                                </div>


                                            </div><!-- /.form-group -->
                                        </div>

                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="chieurong" id="chieurong" value="'.$chieurong.'" onkeyup="CheckNumber(this);" placeholder="Kích thước chiều rộng bạn muốn cắt" maxlength="6" '.(
                                                            $chonchieurong ? null : 'disabled'
                                                        ).' />
                                                        <span class="input-group-addon">
                                                            px
                                                        </span><!-- /.input-group-addon -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="chieucao" id="chieucao" value="'.$chieucao.'" onkeyup="CheckNumber(this);" placeholder="Kích thước chiều cao bạn muốn cắt" maxlength="6" '.(
                                                            $chonchieucao ? null : 'disabled'
                                                        ).' />
                                                        <span class="input-group-addon">
                                                            px
                                                        </span><!-- /.input-group-addon -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="chatluong" value="'.$chatluong.'" maxlength="3" onkeyup="CheckNumber(this);" placeholder="1-100" />
                                                        <span class="input-group-addon">
                                                            %
                                                        </span><!-- /.input-group-addon -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control jscolor input-sm" name="color" id="canvas" value="'.$color.'" />
                                                </div>

                                            </div><!-- /.form-group -->
                                        </div>
                                    </div><!-- /.row -->
                                </div><!-- /.panel-body -->
                            </div><!-- /.panel panel-danger -->
                        </div><!-- /.col-sm-6 -->

                        <div class="col-sm-6">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Cắt hình Thumb
                                    </div><!-- /.panel-title -->
                                </div><!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="chonchieurong2" value="1" '.(
                                                            $chonchieurong2 ? 'checked' : null
                                                        ).' onchange="check_and_show(this, \'chieurong2\')" /> Cắt theo chiều rộng
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="chonchieucao2" value="1" '.(
                                                            $chonchieucao2 ? 'checked' : null
                                                        ).' onchange="check_and_show(this, \'chieucao2\')" />
                                                        Cắt theo chiều cao
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <br/>
                                                        Chất lượng hình
                                                    </label>
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <label>
                                                    <br>
                                                    Border
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="chieurong2" id="chieurong2" value="'.$chieurong2.'" onkeyup="CheckNumber(this);" placeholder="Kích thước chiều rộng bạn muốn cắt" maxlength="6" '.(
                                                            $chonchieurong2 ? null : 'disabled'
                                                        ).' />
                                                        <span class="input-group-addon">
                                                            px
                                                        </span><!-- /.input-group-addon -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="chieucao2" id="chieucao2" value="'.$chieucao2.'" onkeyup="CheckNumber(this);" placeholder="Kích thước chiều cao bạn muốn cắt" maxlength="6" '.(
                                                            $chonchieucao2 ? null : 'disabled'
                                                        ).' />
                                                        <span class="input-group-addon">
                                                            px
                                                        </span><!-- /.input-group-addon -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="chatluong2" value="'.$chatluong2.'" maxlength="3" onkeyup="CheckNumber(this);" placeholder="1-100" />
                                                        <span class="input-group-addon">
                                                            %
                                                        </span><!-- /.input-group-addon -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" name="border" value="'.$border.'" onkeyup="CheckNumber(this);" placeholder="" maxlength="6" />
                                                        <span class="input-group-addon">
                                                            px
                                                        </span><!-- /.input-group-addon -->
                                                        <span class="input-group-addon">
                                                            Màu
                                                        </span><!-- /.input-group-addon -->
                                                        <input type="text" class="form-control input-sm jscolor" name="bordercolor" value="'.$bordercolor.'" onkeyup="CheckNumber(this);" placeholder="" maxlength="6" />
                                                    </div><!-- /.input-group -->
                                                </div>
                                            </div><!-- /.form-group -->
                                        </div>
                                    </div><!-- /.row -->
                                </div><!-- /.panel-body -->
                            </div><!-- /.panel panel-danger -->
                        </div><!-- /.col-sm-6 -->
                    </div><!-- /.row -->
                </div>
            </div>


        </div><!-- End col6 Left -->

        <div class="col-sm-6 _right">
        </div><!-- End col6 Right -->

   </div><!-- End row -->
</div><!-- End Fluid -->
';
$CONTENT .= '
   <input type="hidden" value="' . ($id) . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '
    </div><div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array(
    "{CONTENT}" => $CONTENT,
    "{ACTION}"  => $ACTION);
echo load_content($file_tempaltes, $array_bien);
