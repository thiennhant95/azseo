<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Nội dung đặt hàng của khách hàng",
    "table"           => 'tbl_dathang',
    "tablenoidung"    => 'tbl_dathang',
    "id"              => 'id',
    "idtype"          => 0,
    "loai"            => 1,
    "thutu"           => 0,
    "email"           => 1,
    "hoten"           => 1,
    "sodienthoai"     => 1,
    "dangkygoi"       => 1,
    "noidung"         => 0,
    "ip"              => 1,
    "anhien"          => 0,
    "ngaygui"         => 1,
    "id"              => 1,
    "action"          => 1,
    "add_item"        => 1,
    "date"            => 1,
    "path_img"        => "../uploads/option/",
    "path_file"       => "../uploads/files/",
    "chucnangkhac"    => 0,
    "action"          => 1,
    "sizeimagesthumb" => 300,
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
