<?php

//*** KIỂM TRA QUYỀN HẠN Administrator ***
if ($__getid != '') {
    // Process Chỉnh Sửa
  if ($__sua == 0) {
    echo '<script language="javascript">
    alert("' . $arraybien['khongcoquyensua'] . '");
    location.href="./?op=' . $__getop . '";
    </script>';
    exit();
  }
} else {
    // Process Thêm mới
  if ($__them == 0) {
    echo '<script language="javascript">
    alert("' . $arraybien['khongcoquyenthem'] . '");
    location.href="./?op=' . $__getop . '";
    </script>';
    exit();
  }
}
$_id_name = $__config['id'];
$_getop   = $_GET['op'];
$_idtype  = $__config['type'];
$_idSP    = $__config['idsp'];
$ACTION   = '';
$CONTENT  = '';
if (isset($_POST['cid'])) {
  $_getidSP = $_POST['cid'][0];
}
//*** Xử lý quá trình Chỉnh sửa ***//
if ($__getid != '') {
  $__cid     = $_GET['id'];
  $s_sanpham = "SELECT * FROM $__table WHERE id = $__getid";
  $data      = $db->sqlSelectSql($s_sanpham);
  $data      = $data[0];
  $id        = $data['id'];
  $idtype    = $data['idtype'];
  $baiviet   = $data['baiviet'];
  $icon      = $data['icon'];
  $img       = $data['img'];
  $file      = $data['file'];
  $coltop    = $data['coltop'];
  $colleft   = $data['colleft'];
  $colright  = $data['colright'];
  $colbottom = $data['colbottom'];
  $main      = $data['main'];
  $home      = $data['home'];
  $loai      = $data['loai'];
  $thutu     = $data['thutu'];
  $anhien    = $data['anhien'];
  $iduser    = $data['iduser'];
  $img_view  = $__config['path_img'] . $img;
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
 if (pressbutton == \'cancel\') {
  submitform( pressbutton );
  return;
}
var form = document.adminForm;
   // do field validation
';
// Kiểm tra tồn tại
if ($_idSP == 1) {
  if ($_getidSP != '') {
    $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' and idSP != '" . $idSP . "' ";
  } else {
    $s_idSP = "SELECT idSP from tbl_noidung where idSP != '' ";
  }
  $d_idSP = $db->sqlSelectSql($s_idSP);
  // echo '<pre>'; print_r($d_idSP); echo '</pre>'; exit();

  if (count($d_idSP) > 0) {
    foreach ($d_idSP as $keyidsp => $info_idSP) {
      $value_idSP = $info_idSP['idSP'];
      echo '
      else if (form.idSP.value == \'' . $value_idSP . '\'){
        alert( "Mã sản phẩm ' . $value_idSP . ' đã tồn tại! Vui lòng nhập mã khác!" );
        form.idSP.focus();
      }';
    }
  }
}
// Kiểm tra rỗng
if ($_idtype == 1) {
  echo '
  else if (form.idtype.value == ""){
   alert( "Bạn phải chọn một Loại sản phẩm" );
   form.idtype.focus();
 }';
}
// Hoàn thành validate ==> Submit frm
echo '
else {
 submitform( pressbutton );
}
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action btn-group">
<a class="btn btn-success" href="./?op=' . $_GET['op'] . '">
<i class="fa fa-ban"></i> ' . $arraybien['dong'] . '
</a>
<a class="btn btn-warning" onclick="popupWindow(\'http://suportv2.webso.vn/?op=' . $_GET['op'] . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
<i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
</a>
</div>
<div class="clear"></div>';
//********** col left **********//
$CONTENT .= '
<form action="./?op=' . $_GET['op'] . '&method=query&action=save&id=' . $_GET['id'] . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info hidden">
<div class="panel panel-default">
<div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
<div class="panel-body">';
$CONTENT .= '
</div>
</div>
</div>';
//********** col right ********** //
$CONTENT .= '
<div class="col_data setwidth">
<div class="panel panel-default">
<div class="panel-heading thuoctinhtitle">&nbsp;THÔNG TIN CHI TIẾT</div>
<div class="panel-body box-lienhe">';


if ($data['email'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">' . $arraybien['email'] . '</div>
  <div class="group-active">
  <label for="email">' . $data['email'] . '</label>
  </div>
  </div>
  </div>
  ';
}

if ($data['hoten'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">' . $arraybien['hoten'] . '</div>
  <div class="group-active">
  <label for="hoten">' . $data['hoten'] . '</label>
  </div>
  </div>
  </div>
  ';
}

if ($data['sodienthoai'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">' . $arraybien['sodienthoai'] . '</div>
  <div class="group-active">
  <label for="dienthoai">' . $data['sodienthoai'] . '</label>
  </div>
  </div>
  </div>
  ';
}
// echo '<pre>'; print_r($data['dangkygoi']); echo '</pre>';
if ($data['dangkygoi'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">Chọn khóa học</div>
  <div class="group-active">
  <label for="dangkygoi">';
  if ($data['dangkygoi']==0){
    $CONTENT.= "4-7 tuổi";
  }else if($data['dangkygoi']==1){
    $CONTENT.= "7-11 tuổi";
  }else if($data['dangkygoi']==2){
    $CONTENT.= "11-16 tuổi";
  }
  $CONTENT.='</label>
  </div>
  </div>
  </div>
  ';
}

if ($data['ngaygui'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">' . $arraybien['ngaygui'] . '</div>
  <div class="group-active">
  <label for="ngaygui">' . $data['ngaygui'] . '</label>
  </div>
  </div>
  </div>
  ';
}

if ($data['ip'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">' . $arraybien['ip'] . '</div>
  <div class="group-active">
  <label for="ip">' . $data['ip'] . '</label>
  </div>
  </div>
  </div>
  ';
}
if ($data['ngaytao'] != "") {
  $CONTENT .= '
  <div class="form-group">
  <div class="input-group">
  <div class="input-group-addon">' . $arraybien['ngaygui'] . '</div>
  <div class="group-active">
  <label for="ngaytao">' . $data['ngaytao'] . '</label>
  </div>
  </div>
  </div>
  ';
}

if ($data['noidung'] != "") {
  $CONTENT .= '
  <div class="panel panel-default">
  <div class="panel-heading">
  <div class="panel-title"><i class="fa fa-hand-o-right"></i>&nbsp;' . $arraybien['noidunggopy'] . '</div>
  </div>
  <div class="panel-body">
  ' . $data['noidung'] . '
  </div>
  </div>
  ';
}
$CONTENT .= '
</div>
</div>
<input type="hidden" value="' . $__getid . '" name="id">
<input type="hidden" value="" name="cid[]">
<input type="hidden" value="0" name="version">
<input type="hidden" value="0" name="mask">
<input type="hidden" value="' . $__getop . '" name="op">
<input type="hidden" value="" name="task">
<input type="hidden" value="" name="luuvathem">
<input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">';
$CONTENT .= '</div>'; // end col data
$CONTENT .= '<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
  "{ACTION}"                          => $ACTION);
echo Load_content($file_tempaltes, $array_bien);