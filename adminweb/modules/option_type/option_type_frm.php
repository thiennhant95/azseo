<?php
// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    if ($__sua == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
} else {
    // la them
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
}
$_id_name            = $__config['id'];
$_getop              = ws_get('op');
$ACTION              = '';
$CONTENT             = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
if ($__getid != '') {
    $__cid     = ws_get('id');
    $s_sanpham = "select * from $__table where id = $__getid";
    $data      = $db->rawQuery($s_sanpham);
    $data      = $data[0];
    $id        = check_select($data['id']);
    $datatype  = check_select($data['datatype']);
    $loai      = check_select($data['loai']);
    $thutu     = check_select($data['thutu']);
    $anhien    = check_select($data['anhien']);
    $locnhanh  = check_select($data['locnhanh']);
    $img_view  = '../uploads/danhmuc/' . $img;
    $icon_view = '../uploads/danhmuc/' . $icon;
}
$ten_post = replace_html(ws_post('ten'.$__defaultlang));
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton) {
   var form = document.adminForm;
   // do field validation
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   } else if (form.ten' . $__defaultlang . '.value == "") {
      alert(\'Tên không được để trống !\');
      form.ten' . $__defaultlang . '.focus();
   } ';
if ($__getid != '') {
    $s_ten = "SELECT ten from $__tablenoidung where ten != '' AND idlang = $__defaultlang AND ten != '" . $ten_post . "' AND idtype != $__getid ";
} else {
    $s_ten = "SELECT ten from $__tablenoidung where ten != '' AND idlang = $__defaultlang ";
}
$d_ten = $db->rawQuery($s_ten);
if (count($d_ten) > 0) {
    foreach ($d_ten as $key_ten => $info_ten) {
        $value_ten = $info_ten['ten'];
        echo '
            else if (form.ten' . $__defaultlang . '.value == \'' . $value_ten . '\') {
               alert( "Tên [ ' . $value_ten . ' ] đã được sử dụng, Vui lòng nhập tên khác!" );
               form.ten' . $__defaultlang . '.focus();
            }';
    }
}
echo '
   else {
     submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action">
   <a class="btn btn-primary" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://supportv2.webso.vn/?op=' . ws_get('op') . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
// col left
$CONTENT .= '
    <form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
if ($__config['datatype'] == 1) {
    $CONTENT .= '
        <div class="form-group">
          <label for="inputdefault">Kiểu dữ liệu</label>';
    if (count($_arr_datatype) > 0) {
        $CONTENT .= '<select name="datatype" id="datatype" class="form-control">';
        for ($t = 0; $t < count($_arr_datatype); $t++) {
            $datatype_id  = $t;
            $datatype_ten = $_arr_datatype[$t];
            if ($datatype == $datatype_id) {
                $CONTENT .= '<option selected="selected" value="' . $datatype_id . '">' . $datatype_ten . '</option>';
            } else {
                $CONTENT .= '<option value="' . $datatype_id . '">' . $datatype_ten . '</option>';
            }
        }
        $CONTENT .= '</select>';
    }
    $CONTENT .= '
        </div>';
}
if ($__config['anhien'] == 1) {
    $CONTENT .= '
         <div class="checkbox">
            <label>';
    if ($anhien == '') {
        $anhien = 1;
    }
    if ($anhien == 1) {
        $CONTENT .= '<input checked="checked"  value="1" type="checkbox" name="anhien" id="anhien" />';
    } else {
        $CONTENT .= '<input  value="1" type="checkbox" name="anhien" id="anhien" />';
    }
    $CONTENT .= '
            ' . $arraybien['hienthi'] . '</label>
         </div>';
}
if ( @$__config['locnhanh'] ) {
   $locnhanh_checked = @$locnhanh ? ' checked="checked" ': null;
    $CONTENT .= '
   <div class="form-group">
      <div class="checkbox">
         <label>
            <input type="checkbox" name="locnhanh" value="1" '.$locnhanh_checked.' />
            Lọc nhanh
         </label>
      </div>
   </div>';
}
$CONTENT .= '
         </div>
      </div>
</div>';
// col right
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading">&nbsp;</div>
  <div class="panel-body">
    <div id="tabs">
     <ul>';
$s_lang = "select a.id,a.idkey,b.ten
           from tbl_lang AS a
           inner join tbl_lang_lang AS b
           where a.id = b.iddanhmuc
           and b.idlang = '{$__defaultlang}'
           and a.anhien = 1
           order by thutu Asc";
$d_lang = $db->rawQuery($s_lang);
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $ten    = $info_lang['ten'];
        $idlang = $info_lang['id'];
        $idkey  = $info_lang['idkey'];
        $CONTENT .= '<li type="#tab' . ($key_lang + 1) . '">' . $ten . '</li>';
    }
}
$CONTENT .= '
        <div class="clear"></div>
    </ul>
    <div style="clear:both"></div>
    <div class="tab-text">';
if (count($d_lang) > 0) {
    foreach ($d_lang as $key_lang => $info_lang) {
        $tenlang = $info_lang['ten'];
        $idlang  = $info_lang['id'];
        $idkey   = $info_lang['idkey'];
        // lay du lieu theo ngon ngu
        $s_noidung_data = "SELECT * FROM {$__tablenoidung} where idlang = {$idlang} and idtype = '" . $__cid . "' ";
        $d_noidung_data = $db->rawQuery($s_noidung_data);
        @$d_noidung_data = $d_noidung_data[0];
        $ten            = check_select($d_noidung_data['ten']);
        $CONTENT .= '
         <div id="tab' . ($key_lang + 1) . '">
         <div class="adminform">';
        if ($__config['ten'] == 1) {
            $CONTENT .= '
         <div  class="form-group">
            <label for="inputdefault">' . $arraybien['ten'] . ' ' . $tenlang . '</label>';
            if ($__getid != '') {
                $CONTENT .= '<input class="form-control"   name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />';
            } else {
                $CONTENT .= '
               <input data-val-required="Tên sản phẩm không được để trống" class="form-control"  name="ten' . $idlang . '" type="text" id="ten' . $idlang . '" value="' . $ten . '"  />';
            }
            $CONTENT .= '
         </div>';
        }

        $CONTENT .= '
            </div>
        </div>';
    }
}
$CONTENT .= '
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">
   </div>'; // end tab-text
$CONTENT .= '
</div></div></div></div></div>'; // end col data
$CONTENT .= '
<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
