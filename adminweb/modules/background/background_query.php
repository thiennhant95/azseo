<?php
$__table = $__config['table'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $get_data_row = get_data($__config['table'], '*', 'id,=,' . $__postid . '');
        $get_data_row = $get_data_row[0];
        $update_thutu = update_data($__config['table'], array('id', $get_data_row['id']), array($get_data_row['id'], $get_data_row['mota'], $get_data_row['urlfile'], '0', $get_data_row['xemhinh'], $get_data_row['color'], $get_data_row['canle'], $get_data_row['laphinh'], $get_data_row['fixed']));
    }
    echo '<script> location.href="./?op=' . ws_get("op") . '"; </script>';
} else if ($__post_task == 'publish') {
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $get_data_row = get_data($__config['table'], '*', 'id,=,' . $__postid . '');
        $get_data_row = $get_data_row[0];
        $update_thutu = update_data($__config['table'], array('id', $get_data_row['id']), array($get_data_row['id'], $get_data_row['mota'], $get_data_row['urlfile'], '1', $get_data_row['xemhinh'], $get_data_row['color'], $get_data_row['canle'], $get_data_row['laphinh'], $get_data_row['fixed']));
    }
    echo '<script> location.href="./?op=' . ws_get("op") . '"; </script>';
} else if ($__post_task == 'remove') {
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    $remove      = del_data($__config['table'], 'id', array($array_cid));
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order = ws_post('cid')[$r];
        $s_bg     = get_data($__config['table'], 'urlfile', 'id,=,' . $id_order . '');
        $file_bg  = $s_bg[0]['urlfile'];
        if (file_exists($__config['path_img'] . "$file_bg")) {
            unlink($__config['path_img'] . "$file_bg");
        }
        $value_img        = ws_post('img')[$id_order];
        $value_file       = ws_post('file')[$id_order];
        $s_bg             = get_data($__config['table'], 'urlfile', 'id,=,' . $__postid . '');
        $file_background2 = $s_bg[0]['urlfile'];
        $remove_data      = del_data($__config['table'], 'id', array($id_order));
    }
    echo '<script> location.href="./?op=' . ws_get("op") . '"; </script>';
}
if ($__getaction == 'save') {
    /** Tao thu muc neu chua ton tai */
    create_dir($__config['path_img']);
    $array_data = get_data('background', '*', '');
    $id         = max_number_in_array($array_data, "id");
    $id         = $id + 1;
    $mota       = ws_post('mota');
    $urlfile    = $_FILES["urlfile"]["name"];
    $default    = ws_post('default');
    $xemhinh    = ws_post('xemhinh');
    $color      = ws_post('color');
    $canle      = ws_post('canle');
    $laphinh    = ws_post('laphinh');
    $fixed      = ws_post('fixed');
    if ($urlfile != '') {
        $chuoi = "ABCDEFGHIJKLMNOPQRSTUVWYWZ0123456789";
        $i     = 0;
        while ($i < 2) {
            $vitri_ran = mt_rand(0, 35);
            $giatri_ran .= substr($chuoi, $vitri_ran, 1);
            $i++;
        }
        $file_background = $giatri_ran . $urlfile;
        move_uploaded_file($_FILES["urlfile"]["tmp_name"], $__config['path_img'] . "$file_background");
    }
    $__postid = ws_post('id');
    // neu cap nhat thong tin san pham
    if ($__postid == '') {
        $aray_insert       = array($id, $mota, $file_background, $default, $xemhinh, $color, $canle, $laphinh, $fixed);
        $id_sanpham_insert = add_data($__config['table'], $aray_insert);
    } else {
        $s_bg             = get_data($__config['table'], 'urlfile', 'id,=,' . $__postid . '');
        $file_background2 = $s_bg[0]['urlfile'];
        if ($urlfile != '') {
            if (file_exists($__config['path_img'] . "$file_background2")) {
                unlink($__config['path_img'] . "$file_background2");
            }
            $aray_insert = array($__postid, $mota, $file_background, $default, $xemhinh, $color, $canle, $laphinh, $fixed);
        } else {
            $aray_insert = array($__postid, $mota, $file_background2, $default, $xemhinh, $color, $canle, $laphinh, $fixed);
        }
        $update_thutu = update_data($__config['table'], array('id', '' . $__postid . ''), $aray_insert);
    }
    echo '<script> location.href="./?op=' . ws_get("op") . '"; </script>';
}
