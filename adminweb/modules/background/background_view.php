<?php
$_id_name       = $__config['id'];
$__table        = $__config['table'];
   $count_row = count($data) - 1;
   $saveorder = $perpage;
   if ($perpage>$total_row) {
       $saveorder = $total_row;
   }
    ?>
<div class="row_content_top">
   <div class="row_content_top_title"><?php echo $__config['module_title']; ?></div>
   <div class="row_content_top_action btn-group">
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default hidden"  href="javascript:saveorder('<?php echo $saveorder-1; ?>', 'saveorder')">
               <i class="fa fa-sort-amount-desc"></i> <?php echo $arraybien['sapxep']; ?>
        </a>
        <a class="btn btn-primary" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('publish')}" href="#">
            <i class="fa fa-check-circle"></i> <?php echo $arraybien['bat']; ?>
        </a>
        <a class="btn btn-success" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('unpublish')}" href="#">
          <i class="fa fa-check-circle color-black"></i> <?php echo $arraybien['tat']; ?>
        </a>
        <a class="btn btn-info" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton_del('remove')}" href="#">
        <i class="fa fa-times-circle color-black"></i> <?php echo $arraybien['xoa']; ?>
        </a>
        <a class="btn btn-danger" href="./?op=<?php echo $__getop; ?>&method=frm">
         <i class="fa fa-plus-circle"></i> <?php echo $arraybien['themmoi']; ?>
        </a>
        <a class="btn btn-warning" onclick="popupWindow('http://webso.vn/helps/?op=<?php echo ws_get('op'); ?>&v=2', 'Trợ giúp', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> <?php echo $arraybien['trogiup']; ?>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="row_content_content">
<?php
$row_total = count($data);
if (ws_post('filter_order_Dir')=='asc') {
    $filter_order_Dir = 'desc';
}
?>
<form id="frm" name="adminForm" method="post" action="./?op=<?php echo $__getop; ?>&method=query">
<?php
echo '
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" value="' . ws_post('filter_order_Dir') . '" name="filter_order_Dir" \>';
?>
         <?php if ($total_row>0) {
    echo Pages($total_row, $perpage, $path_page);
} ?>
      <div class="table-responsive"><table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default table" >
      <thead>
        <tr class="panel-heading">
         <th>#</th>
         <th width="5"> <?php
         if (isset($_SESSION['__limit'])) {
             if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
                 $pagetotal = $total_row;
             } else {
                 $pagetotal = $_arr_listpage[$_SESSION['__limit']];
             }
             if ($_arr_listpage[$_SESSION['__limit']]=='All') {
                 $pagetotal = $total_row;
             }
         } else {
             $total_row = count($data);
             $pagetotal = $total_row;
         }
         $pagetotal = count($data);
         ?>
          <input type="checkbox" onclick="checkAll(<?php echo $pagetotal; ?>);" value="" name="toggle">
         </th>
         <?php if ($__config['ten'] == 1): ?>
            <th><?php echo $arraybien['tenhinhnen']; ?></th>
         <?php endif ?>
         <?php if ($__config['maunen'] == 1): ?>
            <th><?php echo $arraybien['maunen']; ?></th>
         <?php endif ?>
         <?php if ($__config['hienthihinh'] == 1): ?>
            <th width="90"><?php echo $arraybien['hienthihinh']; ?></th>
         <?php endif ?>
         <?php if ($__config['canle'] == 1): ?>
            <th><?php echo $arraybien['canle']; ?></th>
         <?php endif ?>
         <?php if ($__config['kieuhienthihinh'] == 1): ?>
            <th><?php echo $arraybien['kieuhienthihinh']; ?></th>
         <?php endif ?>
         <?php if ($__config['xemtruoc'] == 1): ?>
            <th><?php echo $arraybien['xemtruoc']; ?></th>
         <?php endif ?>
         <?php if ($__config['action'] == 1): ?>
            <th><?php echo $arraybien['action']; ?></th>
         <?php endif ?>
         <?php if ($__config['default'] == 1): ?>
            <th width="90"><?php echo $arraybien['default']; ?></th>
         <?php endif ?>
         <?php if ($__config['id'] == 1): ?>
            <th><?php echo $arraybien['id']; ?></th>
         <?php endif ?>
        </tr>
        </thead>
        <tbody>
<?php
$data = $array_data;
if (count($data)>0) {
    $i = 0;
    foreach ($data as $key=> $d) {
        $i++;
        if ($i %2 == 0) {
            $row = "row0";
        } else {
            $row = "row1";
        }
        $id = $d['id']; ?>
          <tr id="<?php echo $i; ?>" class="<?php echo $row; ?>">
            <td title="<?php echo $d['id']; ?>">
               <?php echo $i; ?>
            </td>
            <td>
              <input id="cb<?php echo $i - 1; ?>" type="checkbox" onclick="isChecked(this.checked);" value="<?php echo $d['id']; ?>" name="cid[]">
            </td>
            <?php if ($__config['ten']): ?>
               <td class="tieude">
                  <a href="<?php echo './?op='.ws_get('op').'&method=frm&id='.$d['id']; ?>"  title="<?php echo $d['ten']; ?>" >
                  <?php
                  $s_line ='';
        $s_line ='<div class="cap1 btn btn-primary"><i>1</i></div>';
        echo $s_line.'<span class="textcap'.$sot.'">'.$d['mota'].'</span>'; ?>
                  </a>
               </td>
            <?php endif ?>
            <?php if ($__config['maunen'] == 1): ?>
               <td align="center">
                  <div style="background-color:#<?php echo $d['color']; ?>;width:50px;height:50px;border: 1px solid #D8D8D8;border-radius: 6px;">&nbsp;</div>
               </td>
            <?php endif ?>
            <?php if ($__config['hienthihinh'] == 1): ?>
               <td align="center" style="font-size: 14px;">
                  <?php
                     if ($d['xemhinh']==1) {
                         echo 'Có';
                     } else {
                         echo 'Không';
                     } ?>
               </td>
            <?php endif ?>
            <?php if ($__config['canle'] == 1): ?>
               <td align="center" style="font-size: 14px;"><?php
               if ($d['canle'] == 'top left') {
                   echo 'Trên - trái';
               } elseif ($d['canle'] == 'top center') {
                   echo 'Trên - giữa';
               } elseif ($d['canle'] == 'top right') {
                   echo 'Trên - phải';
               } elseif ($d['canle'] == 'center left') {
                   echo 'Giữa - trái';
               } elseif ($d['canle'] == 'center center') {
                   echo 'Giữa - giữa';
               } elseif ($d['canle'] == 'center right') {
                   echo 'Giữa - phải';
               } elseif ($d['canle'] == 'bottom left') {
                   echo 'Dưới - trái';
               } elseif ($d['canle'] == 'bottom center') {
                   echo 'Dưới - giữa';
               } elseif ($d['canle'] == 'bottom right') {
                   echo 'Dưới - phải';
               } ?>
              </td>
            <?php endif ?>
            <?php if ($__config['kieuhienthihinh'] == 1): ?>
               <td align="center" style="font-size: 14px;">
               <?php
                  if ($d['fixed']=='fixed') {
                      echo 'Hình đứng im';
                  } else {
                      echo 'Hình cuộn';
                  } ?>
               </td>
            <?php endif ?>
            <?php if ($__config['xemtruoc'] == 1): ?>
               <td align="left" style="text-align:left">
               <img alt="<?php echo $d['urlfile']; ?>" src="<?php echo $__config['url_img'].$d['urlfile']; ?>" width="100" height="70" onerror="xulyloi(this)" />
              </td>
            <?php endif ?>
            <?php if ($__config['action'] == 1): ?>
               <td align="center">
                  <ul class="pagination" style="float: none;">
                     <li><a data-toggle="tooltip" style="background: #337ab7; border-color: #337ab7;" title="<?php echo $arraybien['sua']; ?>" href="<?php echo './?op='.$__getop.'&method=frm&id='.$d['id']; ?>" >
                         <i class="fa fa-pencil color-white"></i>
                     </a></li>
                     <li class="active" ><a data-toggle="tooltip" title="<?php echo $arraybien['xoadongnay']; ?>" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del('cb<?php echo $i-1; ?>','remove')" >
                           <i class="fa fa-trash-o color-white"></i>
                      </a></li>
                  </ul>
               </td>
            <?php endif ?>
            <?php if ($__config['default'] == 1): ?>
               <td align="center">
               <?php if ($d['default'] == 1): ?>
                  <span class="btn btn-default">
                     <a onclick="return listItemTask('cb<?php echo $i - 1; ?>','unpublish')" href="javascript:void(0);">
                        <i class="fa fa-eye"></i>
                     </a>
                  </span>
               <?php else: ?>
                  <span class="btn btn-default">
                     <a onclick="return listItemTask('cb<?php echo $i - 1; ?>','publish')" href="javascript:void(0);">
                        <i class="fa fa-eye-slash"></i>
                     </a>
                  </span>
               <?php endif ?>
               </td>
            <?php endif ?>
            <?php if ($__config['id'] == 1): ?>
               <td>
                  <?php echo $d['id']; ?>
               </td>
            <?php endif ?>
   </tr>
          <?php
    } // end foreach
}// end count
     ?>
        </tbody>
      </table></div>
            <div class="pagechoice">
             <?php echo $arraybien['tongso']; ?>: <span style="color:#FF0000; font-weight:bold">
                  <?php echo $total_row; ?>
                  </span>
             <?php echo $arraybien['hienthi']; ?> #
             <?php
          echo'
              <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage"  id="limit" name="limit">';
           for ($i=1; $i<=count($_arr_listpage); $i++) {
               if ($i== $_SESSION['__limit']) {
                   echo'<option selected="selected" value="'.$i.'">'.$_arr_listpage[$i].'</option>';
               } else {
                   echo'<option value="'.$i.'">'.$_arr_listpage[$i].'</option>';
               }
           }
              echo'</select>';
           ?>
         <div class="float-right">
            <?php
            if ($_SESSION['__limit'] != '') {
                //echo $row_total;
                if ($total_row>0) {
                    echo Pages($total_row, $perpage, $path_page);
                }
            }
            ?>
            </div>
         </div>
</form>
</div>