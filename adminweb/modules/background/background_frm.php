<?php
// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    // Xử lý Sửa
    if ($__sua == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyensua'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
} else {
    // Xử lý Thêm
    if ($__them == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyenthem'] . '");
         location.href="./?op=' . $__getop . '";
      </script>';
        exit();
    }
}
$_id_name = $__config['id'];
$_getop   = ws_get('op');
$ACTION   = '';
$CONTENT  = '';
if (ws_post('cid')) {
    $_getidSP = ws_post('cid')[0];
}
if ($__getid != '') {
	/** Thực hiện chỉnh sửa dữ liệu */
    $array_data = get_data($__config['table'], '*', 'id,=,' . $__getid . '');
    $data       = $array_data[0];
    $id         = $data['id'];
    $mota       = $data['mota'];
    $urlfile    = $data['urlfile'];
    $default    = $data['default'];
    $xemhinh    = $data['xemhinh'];
    $color      = $data['color'];
    $canle      = $data['canle'];
    $laphinh    = $data['laphinh'];
    $fixed      = $data['fixed'];
} else {
    $mota    = '';
    $urlfile = '';
    $default = 0;
    $xemhinh = 1;
    $color   = '';
    $canle   = 'top left';
    $laphinh = 'repeat';
    $fixed   = 'fixed';
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton)
{
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   var form = document.adminForm;
   // do field validation
   if (form.mota.value == ""){
      alert( "Vui lòng nhập Tên hình nền" );
      form.mota.focus();
   }';
echo '
   else
   {
   submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action">
   <a class="btn btn-primary btn-save" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . ws_get('op') . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://webso.vn/helps/?op=' . ws_get('op') . '&act=form&v=2\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
$CONTENT .= '
<form action="./?op=' . ws_get('op') . '&method=query&action=save&id=' . ws_get('id') . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body frm-info-bg">';
//-- Hiển thị hình nền
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['hienthihinhnen'] . '</div>
               <div class="group-active">';
if ($xemhinh == 0) {
    $CONTENT .= '
                  <label><input type="radio" checked="checked" name="xemhinh" value="0" />' . $arraybien['khong'] . '</label>
                  <label><input type="radio" name="xemhinh" value="1">' . $arraybien['co'] . '</label>
                  ';
} else {
    $CONTENT .= '
                  <label><input type="radio" name="xemhinh" value="0" />' . $arraybien['khong'] . '</label>
                  <label><input type="radio" checked="checked" name="xemhinh" value="1">' . $arraybien['co'] . '</label>
                  ';
}
$CONTENT .= '</div></div></div>';
//-- Căn lề
$_selected = ' selected="selected" ';
if ($canle == 'top left') {
    $canle1 = $_selected;
} else if ($canle == 'top center') {
    $canle2 = $_selected;
} else if ($canle == 'top right') {
    $canle3 = $_selected;
} else if ($canle == 'center left') {
    $canle4 = $_selected;
} else if ($canle == 'center center') {
    $canle5 = $_selected;
} else if ($canle == 'center right') {
    $canle6 = $_selected;
} else if ($canle == 'bottom left') {
    $canle7 = $_selected;
} else if ($canle == 'bottom center') {
    $canle8 = $_selected;
} else if ($canle == 'bottom right') {
    $canle9 = $_selected;
}
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['canle'] . '</div>
                  <select name="canle" class="form-control">
                     <option ' . $canle1 . ' value="top left">Trên - Trái</option>
                     <option ' . $canle2 . ' value="top center">Trên - Giữa</option>
                     <option ' . $canle3 . ' value="top right">Trên - Phải</option>
                     <option ' . $canle4 . ' value="center left">Giữa - Trái</option>
                     <option ' . $canle5 . ' value="center center">Giữa - Giữa</option>
                     <option ' . $canle6 . ' value="center right">Giữa - Phải</option>
                     <option ' . $canle7 . ' value="bottom left">Dưới - Trái</option>
                     <option ' . $canle8 . ' value="bottom center">Dưới - Giữa</option>
                     <option ' . $canle9 . ' value="bottom right">Dưới - Phải</option>
                  </select>
            </div>
         </div> ';
//-- Default
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['default'] . '</div>
               <div class="group-active">';
if ($default == 0) {
    $CONTENT .= '
                  <label><input type="radio" checked="checked" name="default" value="0" />' . $arraybien['khong'] . '</label>
                  <label><input type="radio" name="default" value="1">' . $arraybien['co'] . '</label>
                  ';
} else {
    $CONTENT .= '
                  <label><input type="radio" name="default" value="0" />' . $arraybien['khong'] . '</label>
                  <label><input type="radio" checked="checked" name="default" value="1">' . $arraybien['co'] . '</label>
                  ';
}
$CONTENT .= '</div></div></div>';
if ($urlfile != '') {
    $CONTENT .= '
               <div class="form-group">
                  <p><label>' . $arraybien['xemtruoc'] . '</label></p>
                  <img src="' . $__config['path_img'] . $urlfile . '" width="250px" />
               </div>
            ';
}
//-- Close col_info
$CONTENT .= '
      </div>
   </div>
</div>
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading thuoctinhtitle">&nbsp;' . $arraybien['thongtinhinhnen'] . '</div>
  <div class="panel-body" id="frm-bgwebsite">
    <div id="tabs" class="can-inputaddon">';
//--Tên Hình nền
if ($mota != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['tenhinhnen'] . '</div>
            <input type="text" value="' . $mota . '" class="form-control" name="mota" placeholder="' . $arraybien['nhaptenhinhnen'] . '" />
         </div>
      </div>
    ';
} else {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['tenhinhnen'] . '</div>
            <input type="text" class="form-control" name="mota" placeholder="' . $arraybien['nhaptenhinhnen'] . '" />
         </div>
      </div>
    ';
}
//--Màu Hình nền
if ($color != "") {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['maunen'] . ' #</div>
            <input type="text" class="form-control color" value="' . $color . '" name="color" placeholder="Mã màu" />
         </div>
      </div>';
} else {
    $CONTENT .= '
      <div class="form-group">
         <div class="input-group">
            <div class="input-group-addon">' . $arraybien['maunen'] . ' #</div>
            <input type="text" class="form-control color" name="color" placeholder="Mã màu" />
         </div>
      </div>';
}
//--Upload Hinh
$CONTENT .= '
         <div id="urlfile">
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group-addon">' . $arraybien['hinhnen'] . '</div>
                  <input type="file" name="urlfile" class="form-control" value="" placeholder="">
               </div>
            </div>';
//-- Kiểu hiển thị
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['kieuhienthi'] . '</div>
               <div class="group-active">';
if ($fixed == 'scroll') {
    $CONTENT .= '
                  <label><input type="radio" name="fixed" value="fixed">' . $arraybien['hinhnendungim'] . '</label>
                  <label><input type="radio" checked="checked" name="fixed" value="scroll" />' . $arraybien['hinhnencuon'] . '</label>
                  ';
} else {
    $CONTENT .= '
                  <label><input type="radio" checked="checked" name="fixed" value="fixed">' . $arraybien['hinhnendungim'] . '</label>
                  <label><input type="radio" name="fixed" value="scroll" />' . $arraybien['hinhnencuon'] . '</label>
                  ';
}
$CONTENT .= '</div></div></div>';
//-- Lặp hình
if ($laphinh == 'no-repeat') {
    $laphinh0 = $_selected;
} else if ($laphinh == 'repeat') {
    $laphinh1 = $_selected;
} else if ($laphinh == 'repeat-x') {
    $laphinh2 = $_selected;
} else if ($laphinh == 'repeat-y') {
    $laphinh3 = $_selected;
}
$CONTENT .= '
         <div class="form-group">
            <div class="input-group">
               <div class="input-group-addon">' . $arraybien['laphinh'] . '</div>
                  <select name="laphinh" class="form-control">
                     <option ' . $laphinh0 . ' value="no-repeat">Không lặp</option>
                     <option ' . $laphinh1 . ' value="repeat">Lặp ngang - dọc</option>
                     <option ' . $laphinh2 . ' value="repeat-x">Lặp chiều ngang</option>
                     <option ' . $laphinh3 . ' value="repeat-y">Lặp chiều dọc</option>
                  </select>
            </div>
         </div>
      </div>';
$CONTENT .= '
   </div>
</div>
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="1" name="30322df89e1904fa7cc728289b7d4ef6">
   </div>
</div>
<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array("{CONTENT}" => $CONTENT,
    "{ACTION}"                          => $ACTION);
echo load_content($file_tempaltes, $array_bien);
