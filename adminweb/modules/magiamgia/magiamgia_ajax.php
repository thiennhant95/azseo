<?php
$thisTable = $_SESSION['__config']['table'];
switch ($name) {
    case 'taonhieuma':
        $cont = '';
        if (! is_numeric($value) || $value > 10 ) return null;
        for ( $i=0; $i<$value; $i++ ) {
            $magiamgia = strtoupper($models->random_string(6));
            $cont .= '
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>('.($i+1).') Mã của bạn: <strong>'.$magiamgia.'</strong></h4>
                    <input type="hidden" name="magiamgia'.$i.'" value="'.$magiamgia.'" />
                </div>
                <div class="panel-body">
        
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <div class="clearfix">
                                    <label>
                                        <input type="radio" name="loai'.$i.'" value="0" checked />
                                        %
                                    </label>
                                    &nbsp;&nbsp;&nbsp;
                                    <label>
                                        <input type="radio" name="loai'.$i.'" value="1" />
                                        vnđ
                                    </label>
                                </div>
                            </div>
                            <input type="text" name="sogiam'.$i.'" class="form-control" onkeyup="CheckNumber(this)" placeholder="mời nhập giá trị cần giảm giá" />

                            <div class="input-group-addon">
                                Số lần sử dụng
                            </div>
                            <input type="text" name="sudung'.$i.'" class="form-control" onkeyup="CheckNumber(this)" placeholder="Mời nhập số lần sử dụng của mã" value="1" />
                        </div>
                    </div>
        
                    <div class="form-group">
        
                        <div class="input-group">
                            <span class="input-group-addon">Từ ngày</span>
                            <input type="text" class="form-control datepicker" name="tungay'.$i.'" />
                            <span class="input-group-addon">Đến ngày</span>
                            <input type="text" class="form-control datepicker" name="denngay'.$i.'" />
                        </div>
                    
                    </div>
        
                </div>
            </div>';
        }
        $data = $cont;
        break;

    default:
        break;
}

echo $data;
