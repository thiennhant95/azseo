<?php
// xu ly neu khong co quyen them xoa sua
if ($__getid != '') {
    if ($__sua == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyensua'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
} else {
    // la them
    if ($__them == 0) {
        echo '<script language="javascript">
      alert("' . $arraybien['khongcoquyenthem'] . '");
      location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
}
$_id_name            = $__config['id'];
$_getop              = $_GET['op'];
$_idtype             = $__config['type'];
$_idSP               = $__config['idsp'];
$ACTION              = '';
$CONTENT             = '';
if (isset($_POST['cid'])) {
    $_getidSP = $_POST['cid'][0];
}
if ($__getid != '') {
    $__cid     = $_GET['id'];
    $s_sanpham = "SELECT * FROM {$__table} WHERE id = {$__getid}";
    $data      = $models->sql_select_sql($s_sanpham);
    $data      = $data[0];
    $id        = $data['id'];

    $magiamgia = $data['magiam'];
    $loai      = $data['loai'];
    $sogiam    = $data['sogiam'];
    $sudung    = $data['sudung'];
    $dasudung  = $data['dasudung'];
    $tungay    = date('d/m/Y', strtotime($data['tungay']));
    $denngay   = date('d/m/Y', strtotime($data['denngay']));
} else {
    $tungay    = '';
    $denngay   = '';
    $sudung    = 1;
    $magiamgia = strtoupper($models->random_string(6));
}
echo '
<script type="text/javascript" language="javascript">
function submitbutton(pressbutton)
{
   if (pressbutton == \'cancel\') {
      submitform( pressbutton );
      return;
   }
   var form = document.adminForm;
   // do field validation
   ';
if ($_idSP == 1) {
    if ($_getidSP != '') {
        $s_idSP = "select idSP from tbl_noidung where idSP != '' and idSP != '" . $idSP . "' ";
    } else {
        $s_idSP = "select idSP from tbl_noidung where idSP != '' ";
    }
    $d_idSP = $models->sql_select_sql($s_idSP);
    if (count($d_idSP) > 0) {
        foreach ($d_idSP as $keyidsp => $info_idSP) {
            $value_idSP = $info_idSP['idSP'];
            echo '
            else if (form.idSP.value == \'' . $value_idSP . '\'){
            alert( "Mã sản phẩm ' . $value_idSP . ' đã tồn tại! Vui lòng nhập mã khác!" );
            form.idSP.focus();
            }';
        }
    }
}
if ($_idtype == 1) {
    echo '
      else if (form.idtype.value == ""){
         alert( "Bạn phải chọn một Loại sản phẩm" );
         form.idtype.focus();
      }';
}
echo '
   else
   {
   submitform( pressbutton );
   }
}
//-->
</script>
';
$ACTION .= '
<div class="row_content_top_title">' . $__config['module_title'] . '</div>
<div class="row_content_top_action">';

    if ( $__getid ) {
        $ACTION .='
        <a class="btn btn-default" onclick="javascript: submitbutton_newpost(\'luuvathem\')" href="#">
                <i class="fa fa-floppy-o"></i> ' . $arraybien['luu'] . '
        </a>';
    }
    $ACTION.='
   <a class="btn btn-default" onclick="javascript: submitbutton(\'save\')" href="#">
    <i class="fa fa-floppy-o"></i> ' . $arraybien['luuvadong'] . '
   </a>
   <a class="btn btn-default" onclick="javascript: submitbutton_addnew(\'saveandnew\')" href="#">
    <i class="fa fa-clone"></i> ' . $arraybien['luuvathem'] . '
   </a>
   <a class="btn btn-success" href="./?op=' . $_GET['op'] . '">
       <i class="fa fa-ban"></i> ' . $arraybien['huy'] . '
   </a>
   <a class="btn btn-warning" onclick="popupWindow(\'http://supportv2.webso.vn/?op=' . $_GET['op'] . '&act=form\', \'Trợ giúp\', 640, 480, 1)" href="#">
      <i class="fa fa-info-circle"></i> ' . $arraybien['trogiup'] . '
   </a>
</div>
<div class="clear"></div>';
// col left
$CONTENT .= '
    <form action="./?op=' . $_GET['op'] . '&method=query&action=save&id=' . $_GET['id'] . '" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
<div class="col_info">
   <div class="panel panel-default">
      <div class="panel-heading thuoctinhtitle">' . $arraybien['thuoctinh'] . '</div>
      <div class="panel-body">';
if ( $__config['magiamgia'] ) {
    $CONTENT .= '
        <div class="form-group" '.( $__getid ? 'style="display:none;"' : null ).'>
          <label for="inputdefault">Số lượng mã muốn tạo</label>
            <div class="input-group">
                <input class="form-control" type="text" name="soluong" id="magiamgia" placeholder="Mời bạn nhập số lượng mã muốn tạo" onkeyup="CheckNumber(this)" value="1" onchange="Get_Data(\'./ajax.php?op='.$__getop.'&name=taonhieuma&value=\'+this.value, \'loadingdata\')" />
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default">
                        Xác nhận
                    </button>
                </div>
            </div>
        </div>

<script>
    $(\'#magiamgia\').on(\'change\', function() {
        if ( $(this).val() > 10 ) {
            alert(\'Bạn chỉ được tạo tối đa 10 mã cùng 1 lúc\');
            $(this).val(0);
        }
    });
</script>
';
}

$CONTENT .= '
         </div>
      </div>
</div>';
// col right
$CONTENT .= '
<div class="col_data">
<div class="panel panel-default">
  <div class="panel-heading">&nbsp;</div>
  <div class="panel-body" id="loadingdata">
  
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Mã của bạn: <strong>'.$magiamgia.'</strong></h4>
            <input type="hidden" name="magiamgia0" value="'.$magiamgia.'" />
        </div>
        <div class="panel-body">';

        // CHECKED loai giam
        if ( $__getid ) {
            if ( $loai ) {
                $loaiChecked = 1;
            } else {
                $loaiChecked = 0;
            }
        } else {
            $loaiChecked = 0;
        }
        $CONTENT.='
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <div class="clearfix">
                            <label>
                                <input type="radio" name="loai0" value="0" '.(! $loaiChecked ? 'checked' : null).' />
                                %
                            </label>
                            &nbsp;&nbsp;&nbsp;
                            <label>
                                <input type="radio" name="loai0" value="1" '.($loaiChecked ? 'checked' : null).' />
                                vnđ
                            </label>
                        </div>
                    </div>
                    <input type="text" name="sogiam0" onkeyup="CheckNumber(this)" class="form-control" value="'.$sogiam.'" placeholder="mời nhập giá trị cần giảm giá" />

                    <div class="input-group-addon">
                        Số lần sử dụng
                    </div>
                    <input type="text" name="sudung0" class="form-control" placeholder="Mời nhập số lần sử dụng của mã" value="'.$sudung.'" onkeyup="CheckNumber(this)" />
                    
                </div>
            </div>

            <div class="form-group">

                <div class="input-group">
                    <span class="input-group-addon">Từ ngày</span>
                    <input type="text" class="form-control datepicker" name="tungay0" value="'.$tungay.'" />
                    <span class="input-group-addon">Đến ngày</span>
                    <input type="text" class="form-control datepicker" name="denngay0" value="'.$denngay.'" />
                </div>
            
            </div>

        </div>
    </div>
    
</div>
<!-- loadingdata -->
  
  ';
$CONTENT .= '
   <input type="hidden" value="' . $__getid . '" name="id">
   <input type="hidden" value="" name="cid[]">
   <input type="hidden" value="0" name="version">
   <input type="hidden" value="0" name="mask">
   <input type="hidden" value="' . $__getop . '" name="op">
   <input type="hidden" value="" name="task">
   <input type="hidden" value="" name="luuvathem">
   <input type="hidden" value="" name="saveandnew">
'; // end tab-text
$CONTENT .= '

<div class="clear"></div>
</form>';
$file_tempaltes = "application/files/templates.tpl";
$array_bien     = array(
    "{CONTENT}" => $CONTENT,
    "{ACTION}"  => $ACTION);
echo $models->Load_content($file_tempaltes, $array_bien);
