<?php

if ($name == 'anhien') {
    if ($value == 1) {
        $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=0\',\'anhien' . $id . '\')">';
        $data .= '<i title="Đang ẩn" class="fa fa-eye-slash color-red"></i>';
        $data .= '</a></span>';
        $dataupdate = 0;
    } else {
        $data .= ' <span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'ajax.php?op='.$__getop.'&id=' . $id . '&name=anhien&value=1\',\'anhien' . $id . '\')">';
        $data .= '<i title="Đang hiển thị" class="fa fa-eye color-black"></i>';
        $data .= '</a></span>';
        $dataupdate = 1;
    }
    $aray_update = array("anhien" => $dataupdate);
    $result      = $db->sqlUpdate('tbl_option', $aray_update, "id = $id");
}else if ($name == 'phigiaohang') {
    $aray_update = array("phigiaohang" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'diachi') {
    $aray_update = array("diachi" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'email') {
    $aray_update = array("email" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'ten') {
    $aray_update = array("ten" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'dienthoai') {
    $aray_update = array("dienthoai" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'tinhthanh') {
    $aray_update = array("tinhthanh" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'ghichu') {
    $aray_update = array("ghichu" => $value);
    $result      = $db->sqlUpdate('tbl_donhang', $aray_update, "id = $id");
}else if ($name == 'soluong') {
    $aray_update = array("soluong" => $value);
    $result      = $db->sqlUpdate('tbl_donhangchitiet', $aray_update, "id = $id");
}else if ($name == 'gia') {
    $value       = str_replace('.', '', $value);
    $value       = str_replace(',', '', $value);
    $value       = str_replace(' ', '', $value);
    $aray_update = array("dongia" => $value);
    $result      = $db->sqlUpdate('tbl_donhangchitiet', $aray_update, "id = $id");
    $loadgia     = $db->getNameFromID("tbl_donhangchitiet", "dongia", "id", $id);
    $loadgia     = trim($loadgia);
    if (is_numeric($loadgia)) {
        $data = number_format($loadgia);
    }
}
if ($name == 'trangthai') {
    if ( !empty($value) ) {
        $iduser_gioithieu = $iduser_gioithieu2 = null;
        $diem             = false;
        $diemtichluy      = 0;

        // KÍCH HOẠT TÍCH ĐIỂM
        if( lay_cauhinh_tichdiem() ) {
            // Lấy tổng giá trị đơn hàng
            $tongtien_full = $db->tongTienCuoiCung( $id );
            // Lấy iduser của chủ đơn hàng
            $iduser = $db->getNameFromID(
                'tbl_donhang', 'iduser', 'id', "'{$id}'"
            );

            if( $iduser ) {
                // Lấy mã người giới thiệu
                $db->where('id', $iduser);
                $manguoigioithieu = $db->getValue('tbl_user', 'manguoigioithieu');
                // Lấy ID của người giới thiệu
                if( $manguoigioithieu ) {
                    $db->where('magioithieu', $manguoigioithieu);
                    $iduser_gioithieu = $db->getValue('tbl_user', 'id');
                }
            }

            // Lấy ID của người giới thiệu 2
            if( $iduser_gioithieu ) {
                $db->where('id', $iduser_gioithieu);
                $manguoigt = $db->getValue("tbl_user", "manguoigioithieu");
                if( $manguoigt ){
                    $db->where("magioithieu", $manguoigt);
                    $iduser_gioithieu2 = $db->getValue("tbl_user", "id");
                }
            }

            // Tổng điểm tích lũy của thành viên
            $sodiemtichluy = $db->layTichDiem( $iduser, $id );

            if( $iduser_gioithieu ) {
                // Tổng điểm tích lũy của thành viên
                $sodiemtichluy_gioithieu = $db->layTichDiem( $iduser_gioithieu, $id );
            }

            // Lấy tổng điểm tích luỹ của đơn hàng này của giới thiệu 2
            if( $iduser_gioithieu2 ){
                $sodiemtichluy_gioithieu2 = $db->layTichDiem(
                    $iduser_gioithieu2, $id
                );
            }

            // Kiểm tra xem đang chọn tích điểm bằng tiền hay %
            if( lay_cauhinh_tichdiem(kieucongdiem) ) {
                // Tiền tệ
                $congdiemtien = lay_cauhinh_tichdiem(congdiemtien);
                if( $tongtien_full >= $congdiemtien ) {
                    $diemtichluy = truncate_number($tongtien_full / $congdiemtien);
                }
            }
            else {
                // Phần trăm
                $phantram_tichdiem = lay_cauhinh_tichdiem(congdiemphantram);
                $diemtichluy = $phantram_tichdiem * $tongtien_full;
            }

            // CỘNG ĐIỂM CHO NGƯỜI GIỚI THIỆU
            if($iduser_gioithieu){
                // Kiểm tra xem đang chọn tích điểm bằng tiền hay %
                if( lay_cauhinh_tichdiem(kieugioithieu) ) {
                    // Tiền tệ
                    $gioithieutien = lay_cauhinh_tichdiem(gioithieutien);
                    if( $tongtien_full >= $gioithieutien ) {
                        $diemtichluy_gioithieu = truncate_number($tongtien_full / $gioithieutien);
                    }
                }
                else {
                    // Phần trăm
                    $gioithieuphantram = lay_cauhinh_tichdiem(gioithieuphantram);
                    $diemtichluy_gioithieu = $gioithieuphantram * $tongtien_full;
                }
            }

            // CỘNG ĐIỂM CHO NGƯỜI GIỚI THIỆU 2
            if( $iduser_gioithieu2 ){
                // Kiểm tra xem đang chọn tích điểm bằng tiền hay %
                if( lay_cauhinh_tichdiem(kieugioithieu2) ) {
                    // Tiền tệ
                    $gioithieutien2 = lay_cauhinh_tichdiem(gioithieutien2);
                    if( $tongtien_full >= $gioithieutien2 ) {
                        $diemtichluy_gioithieu2 = truncate_number($tongtien_full / $gioithieutien2);
                    }
                }
                else {
                    // Phần trăm
                    $gioithieuphantram2 = lay_cauhinh_tichdiem(gioithieuphantram2);
                    $diemtichluy_gioithieu2 = $gioithieuphantram2 * $tongtien_full;
                }
            }

            $db->where("id", $id);
            $trangthai_hientai = $db->getValue('tbl_donhang', 'trangthai');

            // TRẠNG THÁI HOÀN THÀNH ĐƠN HÀNG
            if( intval($value) == 3 && $trangthai_hientai != 3 ) {

                // Cộng điểm
                if( $iduser && is_numeric($diemtichluy) ) {
                    $db->insert('tbl_tichdiem', array(
                        "iduser"    => $iduser,
                        "iddonhang" => $id,
                        "anhien"    => 1,
                        "sodiem"    => $diemtichluy,
                        "lichsu"    => $db->lichSuTichDiem($diemtichluy,$id,$iduser),
                        "ngay"      => $db->now()
                    ));
                    // Cộng điểm cho thành viên
                    $db->diemTichLuyUser($diemtichluy, true, $iduser);
                }

                // Cộng điểm Giới thiệu
                if( $iduser_gioithieu && is_numeric($diemtichluy_gioithieu) ) {

                    $db->insert('tbl_tichdiem', array(
                        "iduser"    => $iduser_gioithieu,
                        "iddonhang" => $id,
                        "anhien"    => 1,
                        "sodiem"    => $diemtichluy_gioithieu,
                        "lichsu"    => $db->lichSuTichDiem($diemtichluy_gioithieu, $id, $iduser_gioithieu, true). " [giới thiệu cấp 1] ",
                        "ngay"      => $db->now()
                    ));
                    // Cộng điểm cho thành viên
                    $db->diemTichLuyUser($diemtichluy_gioithieu, true, $iduser_gioithieu);


                    // Cộng điểm Giới thiệu 2
                    if( $iduser_gioithieu2 && is_numeric($diemtichluy_gioithieu2) ) {

                        $db->insert('tbl_tichdiem', array(
                            "iduser"    => $iduser_gioithieu2,
                            "iddonhang" => $id,
                            "anhien"    => 1,
                            "sodiem"    => $diemtichluy_gioithieu2,
                            "lichsu"    => $db->lichSuTichDiem($diemtichluy_gioithieu2, $id, $iduser_gioithieu2, true) . " [giới thiệu cấp 2] ",
                            "ngay"      => $db->now()
                        ));
                        // Cộng điểm cho thành viên
                        $db->diemTichLuyUser($diemtichluy_gioithieu2, true, $iduser_gioithieu2);
                    }
                }
            }
            // Trừ điểm
            elseif( $trangthai_hientai == 3 && intval($value) != 3 ) {
                // Tổng điểm tích lũy phải lớn hơn 0 mới tiếp tục trừ
                if( $diemtichluy ) {
                    $diemtichluy = ($diemtichluy - ($diemtichluy * 2));

                    if( $iduser && is_numeric($diemtichluy) ) {
                        $db->insert('tbl_tichdiem', array(
                            "iduser"    => $iduser,
                            "iddonhang" => $id,
                            "anhien"    => 1,
                            "sodiem"    => $diemtichluy,
                            "lichsu"    => $db->lichSuTichDiem($diemtichluy, $id, $iduser),
                            "ngay"      => $db->now()
                        ));
                        // Trừ điểm cho thành viên
                        $db->diemTichLuyUser($diemtichluy, false, $iduser);
                    }
                }

                // Trừ điểm giới thiệu
                if( $diemtichluy_gioithieu ) {
                    $diemtichluy_gioithieu = ($diemtichluy_gioithieu - ($diemtichluy_gioithieu * 2));

                    if( $iduser_gioithieu && is_numeric($diemtichluy_gioithieu) ) {
                        $db->insert('tbl_tichdiem', array(
                            "iduser"    => $iduser_gioithieu,
                            "iddonhang" => $id,
                            "anhien"    => 1,
                            "sodiem"    => $diemtichluy_gioithieu,
                            "lichsu"    => $db->lichSuTichDiem($diemtichluy_gioithieu, $id, $iduser_gioithieu, true) . " [giới thiệu cấp 1] ",
                            "ngay"      => $db->now()
                        ));
                        // Trừ điểm cho thành viên
                        $db->diemTichLuyUser($diemtichluy_gioithieu, false, $iduser_gioithieu);
                    }
                }

                // Trừ điểm giới thiệu2
                if( $diemtichluy_gioithieu2 && $iduser_gioithieu2 ) {
                    $diemtichluy_gioithieu2 = ($diemtichluy_gioithieu2 - ($diemtichluy_gioithieu2 * 2));

                    if( $iduser_gioithieu2 && is_numeric($diemtichluy_gioithieu2) ) {
                        $db->insert('tbl_tichdiem', array(
                            "iduser"    => $iduser_gioithieu2,
                            "iddonhang" => $id,
                            "anhien"    => 1,
                            "sodiem"    => $diemtichluy_gioithieu2,
                            "lichsu"    => $db->lichSuTichDiem($diemtichluy_gioithieu2, $id, $iduser_gioithieu2, true) . " [giới thiệu cấp 2] ",
                            "ngay"      => $db->now()
                        ));
                        // Trừ điểm cho thành viên
                        $db->diemTichLuyUser($diemtichluy_gioithieu2, false, $iduser_gioithieu2);
                    }
                }
            }
        }
        $d1 = $db->rawQuery("
                SELECT trangthai
                FROM tbl_donhang
                WHERE id = {$id}
                ");
        
        // Cập nhật trạng thái cho đơn hàng
        $db->where("id", $id);
        $result = $db->update('tbl_donhang', array("trangthai" => $value));
        switch ($value) {
            case 2:
                if (count($d1) > 0 && $d1[0]['trangthai'] ==3){
                    //update kho
                    $d2 = $db->rawQuery("
                SELECT idsanpham,soluong
                FROM tbl_donhangchitiet
                WHERE iddonhang = {$id}
                ");
                    foreach ($d2 as $row_d2){
                        $db->rawQuery("UPDATE tbl_noidung SET soluong=soluong+".$row_d2['soluong']." WHERE id=".$row_d2['idsanpham']);
                    }
                    #end update kho
                }
                break;
            case 3:
                //update kho
                $d2 = $db->rawQuery("
                SELECT idsanpham,soluong
                FROM tbl_donhangchitiet
                WHERE iddonhang = {$id}
                ");
                foreach ($d2 as $row_d2){
                    $db->rawQuery("UPDATE tbl_noidung SET soluong=soluong-".$row_d2['soluong']." WHERE id=".$row_d2['idsanpham']);
                }
                #end update kho
                break;
            case 4:
                if (count($d1) > 0 && $d1[0]['trangthai'] ==3){
                    //update kho
                    $d2 = $db->rawQuery("
                SELECT idsanpham,soluong
                FROM tbl_donhangchitiet
                WHERE iddonhang = {$id}
                ");
                    foreach ($d2 as $row_d2){
                        $db->rawQuery("UPDATE tbl_noidung SET soluong=soluong+".$row_d2['soluong']." WHERE id=".$row_d2['idsanpham']);
                    }
                    #end update kho
                }
                break;
            default:

        }
    }

}
echo $data;
