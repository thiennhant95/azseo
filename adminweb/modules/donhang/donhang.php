<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"        => "Quản lý đơn hàng",
    "table"               => 'tbl_donhang',
    "tablenoidung"        => 'tbl_donhangchitiet',
    "id"                  => 'id',
    "idtype"              => 0,
    "loai"                => 1,
    "thutu"               => 0,
    "ten"                 => 1,
    "sotien"              => 1,
    "tinhtrang"           => 1,
    "email"               => 1,
    "diachi"              => 1,
    "dienthoai"           => 1,
    "fax"                 => 1,
    "tinhthanh"           => 1,
    "tencongty"           => 1,
    "masothue"            => 1,
    "diachicongty"        => 1,
    "phuongthucthanhtoan" => 1,
    "phuongthucvanchuyen" => 1,
    "hotennguoinhan"      => 1,
    "diachinguoinhan"     => 1,
    "tinhthanhnguoinhan"  => 1,
    "faxnguoinhan"        => 1,
    "emailnguoinhan"      => 1,
    "loaithe"             => 1,
    "hotenchuthe"         => 1,
    "masothe"             => 1,
    "ngayhieuluc"         => 1,
    "loichucguiqua"       => 1,
    "phigiaohang"         => 1,
    "ghichu"              => 1,
    "anhien"              => 0,
    "action"              => 1,
    "add_item"            => 1,
    "date"                => 1,
    "path_img"            => "../uploads/option/",
    "path_file"           => "../uploads/files/",
    "chucnangkhac"        => 0,
    "action"              => 1,
    "sizeimagesthumb"     => 300,
);
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];

switch ($__getmethod) {
    case 'query':
        include $__getop . "_query.php";
        break;

    case 'frm':
        include $__getop . "_frm.php";
        break;

    case 'print':
        include $__getop . "_print.php";
        break;

    case 'excel':
        include $__getop . "_excel.php";
        break;

    default:
        include $__getop . "_view.php";
        break;
}
