<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Ho_Chi_Minh');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

error_reporting(0);
include "../../../configs/define.php";
$models = new Models();
include "../../../configs/ws_function.php";
$__defaultlang = $db->getNameFromID("tbl_lang", "id", "macdinh", 1);
if ($__defaultlang=='') {
    $__defaultlang = $db->getNameFromID("tbl_lang", "id", "macdinh", 0);
}
if ($_SESSION['_lang']==null) {
    $_SESSION['_lang']=$__defaultlang;
}

/** Include PHPExcel */
require_once dirname(__DIR__) . '/../../plugins/phpexcel/PHPExcel.php';

if( ! isset( $_GET['id'] ) ){
    die('Not found data');
}

$s = "SELECT * from tbl_donhang where id = '{$_GET['id']}' ";
$d = $db->rawQuery($s);
print_r($s);
die();
if (count($d) > 0) {
    $data        = $d[0];
    $ten         = $data['ten'];
    $id          = $data['id'];
    $madonhang   = $data['madonhang'];
    $ngaydat     = $data['ngaydat'];
    $ten         = $data['ten'];
    $dienthoai   = $data['dienthoai'];
    $diachi      = $data['diachi'];
    $email       = $data['email'];
    $ghichu      = $data['ghichu'];
    $idAdmin     = '';
    $phigiaohang = $data['phigiaohang'];
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$setSheet = $objPHPExcel->setActiveSheetIndex(0);
$getSheet = $objPHPExcel->getActiveSheet();

// TIÊU ĐỀ ĐƠN HÀNG
$setSheet->mergeCells('A1:E1');
$style1 = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'Calibri'
    )
);
$getSheet->getRowDimension('1')->setRowHeight(40);
$getSheet->getStyle('A1')->applyFromArray($style1);
$setSheet->setCellValue('A1', 'THÔNG TIN ĐƠN HÀNG');

// THÔNG TIN ĐƠN HÀNG
for ($i = 3; $i < 7 ; $i++) {
    $setSheet->mergeCells('A'.$i.':C'.$i);
}
$setSheet->setCellValue('A3', 'Họ tên: '.$ten)
        ->setCellValue('D3', 'Mã đơn hàng:')
        ->setCellValue('E3', $madonhang)
        ->setCellValue('A4', 'Điện thoại: '.$dienthoai)
        ->setCellValue('D4', 'Ngày đặt:')
        ->setCellValue('E4', date('d/m/Y H:i:s', strtotime($ngaydat)))
        ->setCellValue('A5', 'Email: '.$email)
        ->setCellValue('D5', 'Ngày giao:')
        ->setCellValue('E5', date('d/m/Y', time()))
        ->setCellValue('A6', 'Địa chỉ: '.$diachi)
        ;

foreach(range('A','G') as $columnID) {
    $getSheet->getColumnDimension($columnID)
        ->setAutoSize(TRUE);
}

// TABLE
$setSheet->setCellValue('A8', 'STT')
        ->setCellValue('B8', 'Tên sản phẩm')
        ->setCellValue('C8', 'Số lượng')
        ->setCellValue('D8', 'Đơn giá')
        ->setCellValue('E8', 'Thành tiền')
        ;

$styleHead = array(
    'font' => array(
        'bold' => true
    )
);

$getSheet->getStyle('A8:E8')->applyFromArray($styleHead);

$dhchitiet = $db->rawQuery("
    SELECT * from tbl_donhangchitiet where iddonhang = '{$_GET['id'] }'
");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
        ),
    ),
);
$countCell = count($dhchitiet) + 8;
$getSheet->getStyle('A8:E'.$countCell)->applyFromArray($styleArray);

$heightRow = 17;

if( count($dhchitiet) > 0 ){
    $stt = 0;
    $num = 8;
    $tongthanhtien = 0;
    $tongtien = 0;
    $tongdongia = 0;
    $tongsoluong   = 0;
    foreach( $dhchitiet as $kdh => $idh ){
        $num++;
        $stt++;
        $idsanpham  = $idh['idsanpham'];
        $TenSanPham = $db->getNameFromID("tbl_noidung_lang", "ten", "idnoidung", $idsanpham);
        $soluong    = $idh['soluong'];
        $dongia     = $idh['dongia'];
        $tongthanhtien = ($soluong * $dongia);

        $setSheet->setCellValue('A'.$num, $stt)
                ->setCellValue('B'.$num, $TenSanPham)
                ->setCellValue('C'.$num, $soluong)
                ->setCellValue('D'.$num, $dongia)
                ->setCellValue('E'.$num, $tongthanhtien)
        ;

        $getSheet->getStyle('D'.$num)->getNumberFormat()->setFormatCode('#,##0');

        $getSheet->duplicateStyle($getSheet->getStyle('D'.$num), 'E'.$num);

        $tongsoluong+= $soluong;
        $tongdongia+= $dongia;
        $tongtien+= $tongthanhtien;

        $getSheet->getRowDimension($num)->setRowHeight($heightRow);

    }// End FOREACH

    // TONG CONG
    $sumCell = $countCell + 1;
    $setSheet->mergeCells('A'.$sumCell.':B'.$sumCell);
    $setSheet->setCellValue('A'.$sumCell, 'Tổng cộng')
            ->setCellValue('C'.$sumCell, $tongsoluong)
            ->setCellValue('C'.$sumCell, $tongsoluong)
            ->setCellValue('D'.$sumCell, $tongdongia)
            ->setCellValue('E'.$sumCell, $tongtien);

    $getSheet->getRowDimension($sumCell)->setRowHeight($heightRow);

    $getSheet->getStyle('D'.$sumCell)->getNumberFormat()->setFormatCode('#,##0');
    $getSheet->duplicateStyle($getSheet->getStyle('D'.$sumCell), 'E'.$sumCell);

    $getSheet->getStyle('A'.$sumCell.':E'.$sumCell)->applyFromArray($styleArray);

    $styleFooter = array(
        'font' => array(
            'bold' => true
        )
    );

    $getSheet->getStyle('A'.$sumCell.':E'.$sumCell)->applyFromArray($styleFooter);

}

// Rename worksheet
$getSheet->setTitle('DonHang');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$fileName = 'DonHang';

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;