<?php
$_id_name = $__config['id'];
$s        = "SELECT *
FROM {$__table}
where 1 = 1";
$id             = $_GET['key'];
$ten            = $_GET['ten'];
if( ws_get('startdate') ){
	$__getdatestart = convertDateMYSQL($_GET['startdate']);
}
if( ws_get('enddate') ){
	$__getdateend   = convertDateMYSQL($_GET['enddate']);
}
// Tìm theo từ khóa
// Tìm theo mã cộng tác viên
if( $__gettukhoa ){
	if( substr($__gettukhoa,0,2) == 'RB' ){
		$getIDUserCur = $db->getNameFromID(
			"tbl_user",
			"id",
			"macongtacvien",
			"'#{$__gettukhoa}'"
		);
		if( ! empty( $getIDUserCur ) ){
			$s.= " AND iduser = {$getIDUserCur} ";
		}else {
			$s = $s . " AND (ten LIKE '%{$__gettukhoa}%' OR id LIKE '%{$__gettukhoa}%' OR madonhang LIKE '%{$__gettukhoa}%' ) ";
		}
	}else {
		$s = $s . " AND (ten LIKE '%{$__gettukhoa}%' OR id LIKE '%{$__gettukhoa}%' OR madonhang LIKE '%{$__gettukhoa}%' ) ";
	}
}
// Tìm theo ngày bắt đầu
if( ws_get('startdate') &&  ! ws_get('enddate') ){
	// Ngay bat dau
	$s.= " AND DATE(ngaydat) >= '{$__getdatestart}' ";
}elseif( ! ws_get('startdate') &&  ws_get('enddate') ){
	// Ngày kết thúc
	$s.= " AND DATE(ngaydat) <= '{$__getdateend}' ";
}elseif(  ws_get('startdate') &&  ws_get('enddate') ){
	// cả 2 ngày
	$s.= " AND DATE(ngaydat) >= '{$__getdatestart}' AND DATE(ngaydat) <= '{$__getdateend}' ";
}
// ORDER
$filter_order_Dir = 'asc';
if ($__sortname != '' && $__sortcurent != '') {
    $s = $s . " order by  " . $__sortname . "  " . $__sortcurent;
} else {
    $s = $s . " order by  id  Desc ";
}
$s_counttotal = $s;
// lay tong so tin
$d_counttotal = $db->rawQuery($s_counttotal);
$total_row    = count($d_counttotal);
$page         = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$page         = ($page == 0 ? 1 : $page);
$pagelimit    = 0;
if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
    $pagelimit = $total_row;
} else {
    $pagelimit = $_arr_listpage[$_SESSION['__limit']]; //limit in each page
}
$perpage    = $pagelimit;
$startpoint = ($page * $perpage) - $perpage;
if ($_SESSION['__limit'] != 0) {
    $s = $s . " LIMIT $startpoint," . $perpage . " ";
}
$data      = $db->rawQuery($s);
$count_row = count($data) - 1;
$saveorder = $perpage;
if ($perpage > $total_row) {
    $saveorder = $total_row;
}
if ($_S)
?>
<div class="row_content_top">
   <div class="row_content_top_title"><?php echo $__config['module_title']; ?></div>
   <div class="row_content_top_action btn-group">
       <a data-toggle="tooltip" title="Xuất đơn hàng Excel" class="btn btn-info"  href="modules/donhang/donhang_export.php">
           <i class="fa fa-download"></i> Xuất Excel
       </a>
        <a data-toggle="tooltip" title="Sắp xếp thứ tự" class="btn btn-default"  href="javascript:saveorder('<?php echo $saveorder - 1; ?>', 'saveorder')">
               <i class="fa fa-sort-amount-desc"></i> <?php echo $arraybien['sapxep']; ?>
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('publish')}" href="#">
            <i class="fa fa-check-circle"></i> <?php echo $arraybien['bat']; ?>
        </a>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton('unpublish')}" href="#">
          <i class="fa fa-check-circle color-black"></i> <?php echo $arraybien['tat']; ?>
        </a>
      <?php
if ($__xoa == 1) {
    ?>
        <a class="btn btn-default" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Vui lòng lựa chọn từ danh sách');}else{  submitbutton_del('remove')}" href="#">
        <i class="fa fa-times-circle color-black"></i> <?php echo $arraybien['xoa']; ?>
        </a>
        <?php
}
?>
      <?php
if ($__them == 1 && false) {
    ?>
        <a class="btn btn-primary" href="./?op=<?php echo $__getop; ?>&method=frm">
         <i class="fa fa-plus-circle"></i> <?php echo $arraybien['themmoi']; ?>
        </a>
      <?php
}
?>
        <a class="btn btn-warning" onclick="popupWindow('http://supportv2.webso.vn/?op=<?php echo $_GET['op']; ?>', 'Trợ giúp', 640, 480, 1)" href="#">
          <i class="fa fa-info-circle"></i> <?php echo $arraybien['trogiup']; ?>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="row_content_content">
<?php
$row_total = $db->getValue($__table, 'count(id)');
if ($_POST['filter_order_Dir'] == 'asc') {
    $filter_order_Dir = 'desc';
}
?>
<form id="frm" name="adminForm" method="post" action="./?op=<?php echo $__getop; ?>&method=query">
<?php
echo '
        <input type="hidden" value="sanpham_type" name="option" \>
        <input type="hidden" value="" name="task" \>
        <input type="hidden" value="0" name="boxchecked" \>
        <input type="hidden" value="-1" name="redirect" \>
        <input type="hidden" value="' . $_POST['filter_order_Dir'] . '" name="filter_order_Dir" \>';
?>
<div class="form-group formsearchlist">
      <label for="focusedInput" class="float-left line-height30" ><?php echo $arraybien['tukhoa']; ?>: </label>
      <input type="text" title="<?php echo $arraybien['dieukientimkiemdanhmuc']; ?>" style="width:140px;" class="form-control col-xs-3" value="<?= ws_get('tukhoa'); ?>" id="focusedInput" name="tukhoa">
    <?php
echo '
      <div class="input-daterange filter-date input-group" id="datepicker">
         <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          <input type="text" class="input-md form-control datepicker2" name="start" value="'.ws_get('startdate').'" placeholder="Ngày bắt đầu" />
          <span class="input-group-addon">đến</span>
          <input type="text" class="input-md form-control datepicker2" value="'.ws_get('enddate').'" name="end" placeholder="Ngày kết thúc" />
      </div>
     ';
echo ' <div class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '&page=' . $__getpage . '&tukhoa=\'+document.adminForm.tukhoa.value+\'&startdate=\'+document.adminForm.start.value+\'&enddate=\'+document.adminForm.end.value"><i class="fa fa-search"></i> ' . $arraybien['tim'] . '</div>
   <div style="margin-left:10px;" class="btn btn-primary" onclick="location.href=\'./?op=' . $__getop . '\'"><i class="fa fa-reply"></i> ' . $arraybien['botimkiem'] . '</div>';
?>
</div>
         <?php if ($total_row > 0) {
    echo Pages($total_row, $perpage, $path_page);
}
?>
      <table width="100%" border="0" cellpadding="4" cellspacing="0" class="adminlist panel panel-default">
      <thead>
        <tr class="panel-heading" >
<!-- STRART TH -->
         <th width="30"> # </th>
         <th width="5">
          <?php
if (isset($_SESSION['__limit'])):
    if ($_arr_listpage[$_SESSION['__limit']] > $total_row) {
        $pagetotal = $total_row;
    } else {
        $pagetotal = $_arr_listpage[$_SESSION['__limit']];
    }
    if ($_arr_listpage[$_SESSION['__limit']] == 'All') {
        $pagetotal = $total_row;
    }
endif?>
            <input type="checkbox" onclick="checkAll(<?php echo $pagetotal; ?>);" value="" name="toggle">
         </th>
          <?php
if ($__config['thutu'] == 1): ?>
             <th width="90" >
             <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=thutu"> <?php
echo $arraybien['sapxep'];
if ($__sortname == 'thutu') {
    if ($__sortcurent == 'desc') {
        echo '<i class="fa fa-sort-amount-desc color-black"></i>';
    } else {
        echo '<i class="fa fa-sort-amount-asc color-black"></i>';
    }
}
?>
             </a>
            </th> <?php
endif;
?>
  <th width="60">Mã DH</th>
          <?php
if ($__config['ten'] == 1): ?>
          <th>
            <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=ten"> <?php
echo $arraybien['tenkhachhang'];
if ($__sortname == 'ten') {
    if ($__sortcurent == 'desc') {
        echo '<i class="fa fa-sort-amount-desc color-black"></i>';
    } else {
        echo '<i class="fa fa-sort-amount-asc color-black"></i>';
    }
}
?>
            </a>
          </th> <?php
endif;?>
        <?php if ($__config['sotien'] == 1): ?>
            <th width="170"><?php echo $arraybien['sotien']; ?></th>
        <?php endif?>
        <?php if ($__config['date'] == 1): ?>
            <th width="160"><?php echo $arraybien['ngaydat']; ?></th>
        <?php endif?>
        <?php if ($__config['date'] == 1): ?>
            <th width="220"><?php echo $arraybien['xuly']; ?></th>
        <?php endif?>
        <?php if ($__config['tinhtrang'] == 1): ?>
            <th width="110"><?php echo $arraybien['tinhtrang']; ?></th>
        <?php endif?>
   <?php if ($__config['idtype'] == 1): ?>
          <th width="130">
             <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=idtype"><?php
echo $arraybien['nhomsanpham'];
if ($__sortname == 'idtype') {
    if ($__sortcurent == 'desc') {
        echo '<i class="fa fa-sort-amount-desc color-black"></i>';
    } else {
        echo '<i class="fa fa-sort-amount-asc color-black"></i>';
    }
}
?>
             </a>
          </th> <?php
endif;
if ($__config['img'] == 1): ?>
          <th width="70"><?php echo $arraybien['hinh']; ?></th>
          <?php
endif;
if ($__config['action'] == 1): ?>
    <th width="145" align="center">
        <?php echo $arraybien['thaotac']; ?>
    </th>
<?php
endif;
if ($__config['anhien'] == 1): ?>
          <th width="80">
            <a data-toggle="tooltip" title="<?php echo $arraybien['nhapchuotdesapxeptheocotnay']; ?>" href="<?php echo $path_sort; ?>&sortname=anhien"><?php
echo $arraybien['hienthi'];
if ($__sortname == 'anhien') {
    if ($__sortcurent == 'desc') {
        echo '<i class="fa fa-sort-amount-desc color-black"></i>';
    } else {
        echo '<i class="fa fa-sort-amount-asc color-black"></i>';
    }
}
?>
            </a>
          </th>
          <?php
endif;
if ($__config['chucnangkhac'] == 1): ?>
            <th style=" width:480px !important;"><?php echo $arraybien['chucnangkhac']; ?></th>
           <?php
endif;
?>
        </tr>
        </thead>
<!-- START BODY -->
        <tbody>
<?php
if (count($data) > 0) {
    $i = 0;
    foreach ($data as $key => $d) {
        $i++;
        if ($i % 2 == 0) {
            $row = "row0";
        } else {
            $row = "row1";
        }
        $id = $d['id']; ?>
          <tr id="sectionid_<?php echo $i; ?>" class="<?php echo $row; ?>">
            <td title="<?php echo $d['id']; ?>">
               <?php echo $i; ?>
            </td>
            <td>
              <input id="cb<?php echo $i - 1; ?>" type="checkbox" onclick="isChecked(this.checked);" value="<?php echo $d['id']; ?>" name="cid[]">
            </td>
    <?php if ($__config['thutu'] == 1): ?>
            <td class="order">
              <?php
if (strlen($d['id']) == 8) {
            echo '
                   <input type="number" size="10" style="background-color:#f0f0f0; text-align: center" class="text_area" value="' . $d['id'] . '" size="3" name="order[]">';
        } elseif (strlen($d['id']) == 12) {
            echo '
                   <input type="number" size="10"  class="text_area" value="' . $d['id'] . '" size="3" name="order[]">';
        } elseif (strlen($d['id']) == 16) {
            echo '
                   <input type="number" size="10"  class="text_area" value="' . $d['id'] . '" size="3" name="order[]">';
        } else {
            echo '
                   <input type="number" size="10" style=" background-color:#CCC;" class="text_area" value="' . $d['id'] . '" size="3" name="order[]">';
        } ?>
            </td>
  <?php endif; ?>
    <td class="tieude" width="60">
        <?php echo "&nbsp;&nbsp;".$d['madonhang']; ?>
    </td>
  <?php if ($__config['ten'] == 1): ?>
            <td class="tieude">
            <a href="<?php echo './?op=' . $_GET['op'] . '&method=frm&id=' . $d['id']; ?>"  title="<?php echo $d['url']; ?>" >
            <?php
$s_line = '';
        $s_line = '<div class="cap1 btn btn-primary"><i>1</i></div>';
        for ($t = 4; $t < 100; $t += 4) {
            if (strlen($d['id']) == $t) {
                $sot         = $t / 4;
                $margin_left = ' style="margin-left:' . (($sot - 1) * 20) . 'px;" ';
                $s_line      = '<button ' . $margin_left . ' class="cap' . $sot . ' btn btn-primary"><i>' . $sot . '</i></button>';
                break;
            }
        }
        echo $s_line . '<span class="textcap' . $sot . '">' . $d['ten'] . ' '.$d['thongtinsanpham'].'</span>'; ?>
            </a></td>
   <?php endif; ?>
   <?php
if ($__config['sotien'] == 1) {
    $d2 = $db->rawQuery("
        SELECT SUM( dongia * soluong ) AS tongtien
        FROM tbl_donhangchitiet
        WHERE iddonhang = {$id}
	");
    $sogiam		= 0;
	$loaigiam	= null;
    $tongtien2 = $d2[0]['tongtien'];
    if ( $d['masothe'] ) {
        $sogiam = $db->getNameFromID(
			"tbl_magiamgia",
			"sogiam",
			"magiam",
			"'{$d['masothe']}'"
		);
        $kieugiam = $db->getNameFromID(
			"tbl_magiamgia",
			"loai",
			"magiam",
			"'{$d['masothe']}'"
		);
        if ( $kieugiam ) {
            $tongtien2 -= $sogiam;
            $loaigiam = 'vnđ';
        } else {
            if ( function_exists('tinhGiaGiam') ) {
                $tongtien2 = tinhGiaGiam( $tongtien2, $sogiam );
            }
            $loaigiam = '%';
        }
    }
    if( $d['diemtichluy'] ) {
        $tongtien2 -= $d['diemtichluy'];
    }
    if (count($d2) > 0) {
        $d2 = $d2[0];
        // Trừ giảm giá vào số tiền tổng
        if( $d['giamgia'] ) {
            $sogiamgia = tach_so_tu_chuoi($d['giamgia']);
            $kieugiam =  mb_substr($d['giamgia'], -1);
            if( $kieugiam == '%' ) {
                $tongtien2 -= tinh_phantram($tongtien2, $sogiamgia);
            }
            else {
                $tongtien2 -= $sogiamgia;
            }
        }
        echo "
        <td class='text-left'>&nbsp;
        	" . number_format($tongtien2, 0, ',', '.') . "&nbsp;VNĐ";
			if ( $sogiam ) {
				echo "
				<p style='padding-bottom: 5px;'>
                    <span class='label label-danger'>
                        Giảm giá: ".number_format($sogiam).$loaigiam."</span></p>
				";
			}
            if( $d['diemtichluy'] ) {
                echo "
                <p> <span class='label label-success'>
                    Đổi điểm: ".number_format($d['diemtichluy'])."đ</span>
                </p>";
            }
            // Tính giảm giá
            if( $d['giamgia'] ) {
                echo  "
                    <p>
                        <span class='label label-primary'>
                            Khuyến mãi: -{$d['giamgia']}
                        </span>
                    </p>
                ";
            }
            echo "
        </td>";
    }
    ;
}
?>
      <?php if ($__config['date'] == 1): ?>
         <td>
			<?php echo date('d/m/Y H:i:s', strtotime($d['ngaydat'])); ?>
         </td>
      <?php endif?>
      <td>
      <select name="" id="input" class="form-control" onChange="Ajax_noreturn('ajax.php?op=<?= $__getop ?>&name=trangthai&id=<?php echo $id; ?>&value='+this.value)">
      <option disabled value="">Vui lòng chọn...</option>
      ';<?php
foreach ($_arr_xuly_trangthai_donhang as $key_xuly_donhang => $info_xuly_donhang) {
            if ($d['trangthai'] == $key_xuly_donhang) {
                echo '
               <option selected="selected" value="' . $key_xuly_donhang . '">' . $info_xuly_donhang . '</option> ';
            } else {
                echo '
               <option value="' . $key_xuly_donhang . '">' . $info_xuly_donhang . '</option>';
            }
        }
        $CONTENT .= '</select>
      </td>';
        if ($__config['tinhtrang'] == 1) {
            foreach ($_arr_trangthai_donhang as $key_trangthai_donhang => $info_trangthai_donhang) {
                if ($d['trangthai'] == $key_trangthai_donhang) {
                    echo $info_trangthai_donhang;
                }
            }
        } ?>
   <?php if ($__config['idtype'] == 1): ?>
            <td class="align-center font-bold">
            <?php echo $db->getNameFromID("tbl_hotrotructuyen_type_lang", "ten", "idtype", "'" . $d['idtype'] . "' and idlang= '" . $__defaultlang . "'"); ?>
            </td>
    <?php endif; // end idtype?>
   <?php if ($__config['img'] == 1): ?>
            <td>
               <?php if ($d['img'] != ''): ?>
                  <a target="_blank" onclick="window.open(this.href, '', 'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,left=50,height=500,top=50'); return false;" href="<?php echo $__config['path_img']; ?><?php echo $d['img']; ?>"><img src="<?php echo $__config['path_img']; ?>thumb/<?php echo $d['img']; ?>" width="60" /></a>
                <?php else: ?>
                  <img src="application/templates/images/noimage.png" alt="" width="60" />
                <?php endif?>
            </td>
  <?php endif; ?>
   <?php if ($__config['action'] == 1): ?>
            <td>
            <ul class="pagination">
            <?php
if ($__sua == 1) {
            ?>
                  <li><a data-toggle="tooltip" title="<?php echo $arraybien['sua']; ?>" href="<?php echo './?op=' . $__getop . '&method=frm&id=' . $d['id']; ?>" >
                      <i class="fa fa-pencil"></i>
                  </a></li>
                   <?php
        } ?>
            <li><a data-toggle="tooltip" title="<?php echo $arraybien['in']; ?>" target="_blank" href="modules/donhang/donhang_print.php?id=<?php echo $id; ?>" >
                      <i class="fa fa-print"></i>
                  </a>
            </li>
            <li>
                <a href="modules/donhang/donhang_excel.php?id=<?= $id; ?>" title="Xuất Excel" data-toggle="tooltip">
                    <i class="fa fa-file-excel-o"></i>
                </a>
            </li>
<?php
$co_tichdiem = $db->getNameFromID(
    "tbl_tichdiem",
    "id",
    "iddonhang",
    "'{$id}'"
);
if ( $__xoa ) {
?>
    <li class="active">
        <a data-toggle="tooltip" title="<?php echo $arraybien['xoadongnay']; ?>" style="background-color:#D9534F; border-color: #D9534F;" href="javascript:void(0);" onclick="return listItemTask_del('cb<?php echo $i - 1; ?>','remove')" >
            <i class="fa fa-times-circle color-white"></i>
        </a>
    </li>
    <?php
} ?>
            </ul>
            </td>
   <?php endif; ?>
   <?php if ($__config['anhien'] == 1): ?>
         <td class="align-center">
           <?php
echo '<div id="anhien' . $id . '">';
        $itemstage = '<i title="' . $arraybien['tat'] . '" class="fa fa-eye-slash color-red"></i>';
        if ($d['anhien'] == 1) {
            $itemstage = '<i title="' . $arraybien['bat'] . '" class="fa fa-eye color-black"></i>';
        }
        echo '<span class="btn btn-default"> <a style="cursor:pointer;" onclick="Get_Data(\'modules/' . $__getop . '/' . $__getop . '_ajax.php?id=' . $id . '&name=anhien&value=' . $d['anhien'] . '\',\'anhien' . $id . '\')">' . $itemstage . '</a> </span>';
        echo '</div>'; ?>
         </td>
   <?php endif; ?>
   <?php if ($__config['chucnangkhac'] == 1): ?>
     <td> </td>
   <?php endif; ?>
</tr>
          <?php
    } // end foreach
} // end count
?>
        </tbody>
      </table>
      <div class="pagechoice">
       <?php echo $arraybien['tongso']; ?>: <span style="color:#FF0000; font-weight:bold">
               <?php echo $total_row; ?>
            </span>
       <?php echo $arraybien['hienthi']; ?> #
       <?php echo '
        <select onchange="location.href=\'./application/files/changepage.php?limit=\'+this.value" size="1" class="form-control selectpage"  id="limit" name="limit">';
for ($i = 1; $i <= count($_arr_listpage); $i++) {
    if ($i == $_SESSION['__limit']) {
        echo '<option selected="selected" value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    } else {
        echo '<option value="' . $i . '">' . $_arr_listpage[$i] . '</option>';
    }
}
echo '</select>';
?>
      <div class="float-right">
      <?php
if ($_SESSION['__limit'] != '') {
    //echo $row_total;
    if ($total_row > 0) {
        echo Pages($total_row, $perpage, $path_page);
    }
}
?>
      </div>
      </div>
</form>
</div>