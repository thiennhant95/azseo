<?php
    require dirname(dirname(dirname(__DIR__))) . "/configs/inc.php";

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

$__defaultlang = $db->getNameFromID("tbl_lang", "id", "macdinh", 1);
if ($__defaultlang=='') {
    $__defaultlang = $db->getNameFromID("tbl_lang", "id", "macdinh", 0);
}
if ($_SESSION['__defaultlang']==null) {
    $_SESSION['__defaultlang']=$__defaultlang;
}
function doc3so($so)
{
    $achu  = array(" không ", " một ", " hai ", " ba ", " bốn ", " năm ", " sáu ", " bảy ", " tám ", " chín ");
    $aso   = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    $kq    = "";
    $tram  = floor($so / 100); // Hàng trăm
    $chuc  = floor(($so / 10) % 10); // Hàng chục
    $donvi = floor(($so % 10)); // Hàng đơn vị
    if ($tram == 0 && $chuc == 0 && $donvi == 0) {
        $kq = "";
    }

    if ($tram != 0) {
        $kq .= $achu[$tram] . " trăm ";
        if (($chuc == 0) && ($donvi != 0)) {
            $kq .= " lẻ ";
        }
    }
    if (($chuc != 0) && ($chuc != 1)) {
        $kq .= $achu[$chuc] . " mươi";
        if (($chuc == 0) && ($donvi != 0)) {
            $kq .= " linh ";
        }
    }
    if ($chuc == 1) {
        $kq .= " mười ";
    }

    switch ($donvi) {
        case 1:
            if (($chuc != 0) && ($chuc != 1)) {
                $kq .= " mốt ";
            } else {
                $kq .= $achu[$donvi];
            }
            break;
        case 5:
            if ($chuc == 0) {
                $kq .= $achu[$donvi];
            } else {
                $kq .= " lăm ";
            }
            break;
        default:
            if ($donvi != 0) {
                $kq .= $achu[$donvi];
            }
            break;
    }
    if ($kq == "") {
        $kq = 0;
    }

    return $kq;
}
function doc_so($so)
{
    $so = preg_replace("([a-zA-Z{!@#$%^&*()_+<>?,.}]*)", "", $so);
    if (strlen($so) <= 21) {
        $kq   = "";
        $c    = 0;
        $d    = 0;
        $tien = array("", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ", " tỷ tỷ");
        for ($i = 0; $i < strlen($so); $i++) {
            if ($so[$i] == "0") {
                $d++;
            } else {
                break;
            }
        }
        $so = substr($so, $d);
        for ($i = strlen($so); $i > 0; $i -= 3) {
            $a[$c] = substr($so, $i, 3);
            $so    = substr($so, 0, $i);
            $c++;
        }
        $a[$c] = $so;
        for ($i = count($a); $i > 0; $i--) {
            if (strlen(trim(@$a[$i])) != 0) {
                if (doc3so($a[$i]) != "") {
                    if (($tien[$i - 1] == "")) {
                        if (count($a) > 2) {
                            $kq .= " không trăm lẻ " . doc3so($a[$i]) . $tien[$i - 1];
                        } else {
                            $kq .= doc3so($a[$i]) . $tien[$i - 1];
                        }
                    } elseif ((trim(doc3so($a[$i])) == "mười") && ($tien[$i - 1] == "")) {
                        if (count($a) > 2) {
                            $kq .= " không trăm " . doc3so($a[$i]) . $tien[$i - 1];
                        } else {
                            $kq .= doc3so($a[$i]) . $tien[$i - 1];
                        }
                    } else {
                        $kq .= doc3so($a[$i]) . $tien[$i - 1];
                    }
                }
            }
        }
        return $kq;
    } else {
        return "Số quá lớn!";
    }
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>In đơn hàng</title>
<style type="text/css">
#donhang{
   padding:10px;
   width:700px;
   margin:auto;
   line-height:25px;
   font-trongluong:11px;
}
p{
   margin:0px;
}
h2{
   margin:5px;
}
.tbl_donhangchitiet{
   border-collapse:collapse;
   width:100%;
}
.tbl_donhangchitiet td{
   border:1px solid #CCC;
}
</style>
</head>
<body style="padding-top:0px; margin-top:0px;">
<?php
$s = "SELECT * from tbl_donhang where id = '" . ws_get('id') . "' ";
$d = $db->rawQuery($s);
if (count($d) > 0) {
    $data        = $d[0];
    $ten         = $data['ten'];
    $id          = $data['id'];
    $madonhang   = $data['madonhang'];
    $ngaydat     = $data['ngaydat'];
    $ten         = $data['ten'];
    $dienthoai   = $data['dienthoai'];
    $diachi      = $data['diachi'];
    $email       = $data['email'];
    $ghichu      = $data['ghichu'];
    $idAdmin     = '';
    $phigiaohang = $data['phigiaohang'];
}
echo '
<div id="donhang">
      ' . $db->getThongTin("indonhang_top"). '
   <p style="text-align:center"><strong>Mã đơn hàng:</strong> ' . $madonhang . ' - <strong>Ngày đặt hàng:</strong> ' . $db->getDateFormat('d/m/Y', $ngaydat) . ' - <strong>Ngày giao hàng:</strong>' . $db->getDateFormat('d/m/Y', $db->getDate()) . '</p>
   <p style="margin-top:5px;"><strong>Họ Tên Người Nhận:</strong> ' . $ten . ' &nbsp;&nbsp; - &nbsp; <strong>Email:</strong> ' . $email . '</p>
   <p><strong>Địa chỉ:</strong> ' . $diachi . '&nbsp;&nbsp; - &nbsp;<strong>Điện thoại:</strong> ' . $dienthoai . '</p>';
$s_donhangchitiet = "SELECT * from tbl_donhangchitiet where iddonhang = '" . ws_get('id') . "' ";
$d_donhangchitiet = $db->rawQuery($s_donhangchitiet);
if (count($d_donhangchitiet) > 0) {
    $tongthanhtien = 0;
    $tongsoluong   = 0;
    echo '
      <table  width="600" border="1" cellspacing="0" cellpadding="2" class="tbl_donhangchitiet" align="center">
            <tr class="title" style="background-color:#F3F3F3; font-weight:bold;" >
              <td>STT</td>
              <td>Tên Sản Phẩm</td>
              <td width="70">Số lượng</td>
              <td width="80">Đơn giá</td>
              <td width="100">Thành tiền</td>
        </tr>';
    foreach ($d_donhangchitiet as $keydonhang => $info_donhangchitiet) {
        $idsanpham  = $info_donhangchitiet['idsanpham'];
        $TenSanPham = $db->getNameFromID("tbl_noidung_lang", "ten", "idnoidung", $idsanpham);
        $soluong    = $info_donhangchitiet['soluong'];
        $dongia     = $info_donhangchitiet['dongia'];
        $trongluong = $info_donhangchitiet['trongluong'];
        $tongthanhtien += ($soluong * $dongia);
        $tongsoluong += $soluong;
        echo '
            <tr>
              <td>' . ($keydonhang + 1) . '</td>
              <td>' . $TenSanPham . '</td>
              <td>' . $soluong . '</td>
              <td>' . number_format($dongia) . 'đ</td>
              <td>' . number_format($soluong * $dongia) . 'đ</td>
            </tr>';
    }
    echo '
            <tr style="background-color:#F3F3F3;" >
              <td colspan="4">Tổng cộng(1)</td>
              <td style="font-trongluong:18px; text-align:right;">' . number_format($tongthanhtien) . ' đ</td>
            </tr>
         <tr style="background-color:#F3F3F3;" >
              <td colspan="4">Phí dịch vụ giao hàng(2)</td>
              <td style="font-trongluong:18px; text-align:right;">' . @number_format($phigiaohang) . ' đ</td>
            </tr>
         <tr style="background-color:#F3F3F3; font-weight:bold;" >
              <td colspan="4">TỔNG TIỀN PHẢI THANH TOÁN</td>
              <td style="font-trongluong:18px; text-align:right;">' . number_format($tongthanhtien + $phigiaohang) . ' đ</td>
            </tr>
      </table>
     <p style="text-align:center; font-style:italic; text-transform:capitalize;">
      (Số tiền bằng chữ: ' . doc_so($tongthanhtien + $phigiaohang) . ')
     </p>';
    if ($ghichu != '') {
        echo '
     <p>
       <b><i>Ghi chú: ' . $ghichu . '</i></b>
     </p>';
    }
    echo '<p>
      ' . $db->getThongTin("indonhang_bottom"). '
     </p>';
}
echo '
</div>';
?>
</body>
</html>