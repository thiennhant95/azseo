<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count($_POST['cid']);
if ($__post_task == 'unpublish') {
    // ẨN MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = $_POST['cid'][$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sql_update($__table, $arraycollumn, "id = $__postid ");
    }
} elseif ($__post_task == 'publish') {
    // HIỂN THỊ MỤC TIN
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = $_POST['cid'][$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sql_update($__table, $arraycollumn, "id = $__postid ");
    }
} elseif ($__post_task == 'saveorder') {
    // LƯU THỨ TỰ MỤC TIN
    $soluong_row = count($_POST['cid']);
    $array_cid   = $_POST['cid'];
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = $_POST['cid'][$a];
        $value_order = $_POST['order'][$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} elseif ($__post_task == 'remove') {
    // XOA DU LIEU
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">
         alert("' . $arraybien['khongcoquyenxoa'] . '");
         location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count($_POST['cid']);
    $array_cid   = $_POST['cid'];
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = $_POST['cid'][$r];
        $value_img  = $_POST['img'][$id_order];
        $value_file = $_POST['file'][$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        // update thutu
        if( $thutu_value ){
            $s_update_thutu = "update " . $__table . " set thutu = thutu-1 where thutu > $thutu_value ";
            $d_update_thutu = $db->rawQuery($s_update_thutu);
        }
        if ($db->sqlDelete($__table, " id = '" . $id_order . "' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                unlink("$__path");
                unlink("$__paththumb");
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddonhang = '" . $id_order . "' ");
        }

        // Xóa lịch sử và điểm
        $db->sqlDelete(
            "tbl_tichdiem", " iddonhang = '{$id_order}' "
        );
    }
}
if ($__getaction == 'save') {
    // LƯU TRỮ DỮ LIỆU
    $idtype   = $_POST['idtype'];
    $img      = $_FILES["img"]["name"];
    $loai     = $_POST['loai'];
    $thutu    = 1;
    $anhien   = $_POST['anhien'];
    $img_url  = '../uploads/option/' . $img;
    $id       = $_POST['id'];
    $xoaimg   = $_POST['xoaimg'];
    $imgname  = $_POST['imgname'];
    $filename = $_POST['filename'];
    $_getidSP = "";
    if ($__getid != '') {
        // neu la cap nhat
        $subid = $__getid;
    } else {
        // neu them moi
        $subid = $db->CreateSubID($__table, $__id, "");
    }
    // Create url lien ket
    $urllink     = '';
    $_cid        = $_GET['id'];
    $images_name = $_POST['url' . $__defaultlang];
    $aray_insert = array(
        "idtype" => $idtype,
        "thutu"  => $thutu,
        "ngay"   => $db->GetDateTimes(),
        "anhien" => $anhien,
        "iduser" => $_SESSION['user_id'],
    );
    $id_insert = $db->insert($__table, $aray_insert);
    // them du lieu vao bang noi dung danh muc
    $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
    $d_lang = $db->rawQuery($s_lang);
    if (count($d_lang) > 0) {
        foreach ($d_lang as $key_lang => $info_lang) {
            $tenlang = $info_lang['ten'];
            $idlang  = $info_lang['id'];
            $idkey   = $info_lang['idkey'];
            // get noi dung post qua
            $ten        = $_POST['ten' . $idlang];
            $tieude     = $_POST['tieude' . $idlang];
            $mota       = $_POST['mota' . $idlang];
            $noidung    = $_POST['noidung' . $idlang];
            $url        = $_POST['url' . $idlang];
            $link       = $_POST['link' . $idlang];
            $target     = $_POST['target' . $idlang];
            $tukhoa     = $_POST['tukhoa' . $idlang];
            $motatukhoa = $_POST['motatukhoa' . $idlang];
            // kiem tra url neu da co roi thì them ki tu cuoi url
            $s_checkurl = "SELECT url from $__tablenoidung where url = '$url'";
            $d_checkurl = $db->rawQuery($s_checkurl);
            if (count($d_checkurl) > 0) {
                $url = $url . '-' . rand(0, 100);
            }
            // luu du lieu vao bang danh muc lang
            $aray_insert_lang = array(
                "idtype" => $id_insert,
                "idlang" => $idlang,
                "ten"    => $ten,
            );
            $db->insert($__tablenoidung, $aray_insert_lang);
        }
    }
    // neu them moi danh mục
    if ($__getid == '') {
        // update thutu
        $s_update_thutu = "UPDATE " . $__table . " set thutu = thutu+1 where 1=1 ";
        $d_update_thutu = $db->rawQuery($s_update_thutu);
        // up load icon
        if ($icon != '') {
            $extfile  = $db->LayPhanMoRongFileUpload($icon);
            $iconfile = $images_name . '-icon.' . $extfile;
            if (file_exists("../uploads/option/" . $iconfile)) {
                $iconfile = rand(0, 100) . $iconfile;
            }
            move_uploaded_file($_FILES["icon"]["tmp_name"], "../uploads/option/$iconfile");
        }
        // upload hinh
        if ($img != '') {
            $extfile = $db->LayPhanMoRongFileUpload($img);
            $imgfile = $images_name . '-img.' . $extfile;
            if (file_exists("../uploads/option/" . $imgfile)) {
                $imgfile = rand(0, 100) . $imgfile;
            }
            move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/option/$imgfile");
            $ResizeImage->load("../uploads/option/" . $imgfile);
            $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
            $ResizeImage->save("../uploads/option/thumb/" . $imgfile);
        }
    }
} else {
    // them du lieu vao bang noi dung danh muc
    $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
    $d_lang = $db->rawQuery($s_lang);
    if (count($d_lang) > 0) {
        foreach ($d_lang as $key_lang => $info_lang) {
            // lap theo so luong ngon ngu
            $tenlang = $info_lang['ten'];
            $idlang  = $info_lang['id'];
            $idkey   = $info_lang['idkey'];
            // get noi dung post qua
            $ten        = $_POST['ten' . $idlang];
            $tieude     = $_POST['tieude' . $idlang];
            $noidung    = $_POST['noidung' . $idlang];
            $url        = $_POST['url' . $idlang];
            $link       = $_POST['link' . $idlang];
            $target     = $_POST['target' . $idlang];
            $tukhoa     = $_POST['tukhoa' . $idlang];
            $motatukhoa = $_POST['motatukhoa' . $idlang];
            // kiem tra url neu da co roi thì them ki tu cuoi url
            if ($_POST['hi_url' . $idlang] != $url) {
                $s_checkurl = "select url from $__tablenoidung where url = '$url'";
                $d_checkurl = $db->rawQuery($s_checkurl);
                if (count($d_checkurl) > 0) {
                    $url = $url . '-' . rand(0, 100);
                }
            }
            // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang

        } // end for lang
    } // end count lang
    // kiem tra neu thay hinh thi sẽ xóa hình cũ đi
    // xoa img
    if ($xoaimg == 1) {
        unlink("../uploads/option/" . $imgname);
        unlink("../uploads/option/thumb/" . $imgname);
        $aray_insert = array("img" => "");
        $result      = $db->sqlUpdate($__table, $aray_insert, "id = $__postid  ");
    }
    // neu cap nhat img
    if ($img != '') {
        if ($imgname != '') {
            unlink("../uploads/option/" . $imgname);
            unlink("../uploads/option/thumb/" . $imgname);
        }
        //up file moi len
        $extfile = $db->LayPhanMoRongFileUpload($img);
        $imgfile = $images_name . '-img.' . $extfile;
        if (file_exists("../uploads/option/" . $imgfile)) {
            $imgfile = rand(0, 100) . $imgfile;
        }
        move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/option/$imgfile");
        $ResizeImage->load("../uploads/option/" . $imgfile);
        $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
        $ResizeImage->save("../uploads/option/thumb/" . $imgfile);
        $aray_insert = array("img" => $imgfile);
        $result      = $db->sqlUpdate($__table, $aray_insert, "id = $__postid  ");
    }
}
$link_redirect = './?op=' . $_GET["op"];
if ($_POST['luuvathem'] == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . $_GET["op"] . '&method=frm&id=' . $id;
}
echo '<script> location.href="' . $link_redirect . '"; </script>';
