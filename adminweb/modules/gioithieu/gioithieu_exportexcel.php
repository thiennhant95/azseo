<?php

$soluongsp = count($_SESSION['array_cid']);
     $vitridong = 2;
     if ($soluongsp >0) {
         // xuat ra file excel
        require_once '../../plugin/PHPExcel/PHPExcel.php';
         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getActiveSheet()->setTitle("Sản Phẩm");
         $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true);
         $objPHPExcel->getActiveSheet()->getStyle("A2:S2")->getFont()->setBold(true);
         $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setSize(16);
         $objPHPExcel->getActiveSheet()->freezePaneByColumnAndRow(19, 3);
        // background
        $objPHPExcel->getActiveSheet()->getStyle('A2:S2')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'dfdfdf')
                )
            )
        );

        // center text
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
         $objPHPExcel->getActiveSheet()->getStyle("A1:s1")->applyFromArray($style);

        // set row height
        foreach ($objPHPExcel->getActiveSheet()->getRowDimensions() as $rd) {
            $rd->setRowHeight(80);
        }

         $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells("A1:S1")
        ->setCellValue("A1", "DANH SÁCH BÀI VIẾT")
        ->setCellValue('A2', 'ID')
        ->setCellValue('B2', 'Số lần xem')
        ->setCellValue('C2', 'Thứ tự')
        ->setCellValue('D2', 'Ẩn hiện')
        ->setCellValue('E2', 'id Ngôn ngữ')
        ->setCellValue('F2', 'Idlang')
        ->setCellValue('G2', 'Ngôn ngữ')
        ->setCellValue('H2', 'Tên')
        ->setCellValue('I2', 'Tiêu đề')
        ->setCellValue('J2', 'Link')
        ->setCellValue('K2', 'Target')
        ->setCellValue('L2', 'Mô tả')
        ->setCellValue('M2', 'Nội dung')
        ->setCellValue('N2', 'Từ khóa')
        ->setCellValue('O2', 'Mô tả từ khóa')
        ->setCellValue('P2', 'Tags');
         $vitridongsp = 2;
         foreach ($_SESSION['array_cid'] as $idsp) {
             $vitridongsp +=1;
             $idsp2 = $idsp;
             $s_noidung = "select * from tbl_noidung where id = '".$idsp."' ";
             $d_noidung = $db->rawQuery($s_noidung);
             if (count($d_noidung)>0) {
                 foreach ($d_noidung as $key_noidung => $info_noidung) {
                     $masp     = $info_noidung['masp'];
                     $idtype   = $info_noidung['idtype'];
                     $gia      = $info_noidung['gia'];
                     $giagoc   = $info_noidung['giagoc'];
                     $colvalue = $info_noidung['colvalue'];
                     $solanxem = $info_noidung['solanxem'];
                     $soluong  = $info_noidung['soluong'];
                     $thutu    = $info_noidung['thutu'];
                     $anhien   = $info_noidung['anhien'];
                     if (is_numeric($gia)) {
                         $gia = number_format($gia);
                     }
                     if (is_numeric($giagoc)) {
                         $giagoc = number_format($giagoc);
                     }

                    // truy van lay ngon ngu
                    $s_lang = "select a.id,a.idkey,b.ten
                               from tbl_lang AS a
                               inner join tbl_lang_lang AS b
                               On a.id = b.idlang
                               and a.id = b.iddanhmuc
                               where a.anhien = 1
                               order by thutu Asc";
                     $d_lang = $db->rawQuery($s_lang);
                     if (count($d_lang) > 0) {
                         foreach ($d_lang as $key_lang => $info_lang) {
                             $tenlang = $info_lang['ten'];
                             $idlang  = $info_lang['id'];
                            // truy van lay noi dung lang
                            $s_noidung_lang = "select * from tbl_noidung_lang where idnoidung = '".$idsp."' and idlang = ".$idlang;
                            //echo $s_noidung_lang.'<br />';
                            $d_noidung_lang =$db->rawQuery($s_noidung_lang);
                             if (count($d_noidung_lang)>0) {
                                 foreach ($d_noidung_lang as $key_noidunglang => $info_noidung_lang) {
                                     $vitridong +=1;
                                // xuat gia tri lang
                                    $id_noidung_lang = $info_noidung_lang['id'];
                                     $ten             = $info_noidung_lang['ten'];
                                     $tieude          = $info_noidung_lang['tieude'];
                                     $url             = $info_noidung_lang['url'];
                                     $link            = $info_noidung_lang['link'];
                                     $target          = $info_noidung_lang['target'];
                                     $mota            = $info_noidung_lang['mota'];
                                     $noidung         = $info_noidung_lang['noidung'];
                                     $tukhoa          = $info_noidung_lang['tukhoa'];
                                     $motatukhoa      = $info_noidung_lang['motatukhoa'];
                                     $tag             = $info_noidung_lang['tag'];

                                     if ($vitridongsp%2==0) {
                                         // background
                                        $objPHPExcel->getActiveSheet()->getStyle('A'.$vitridong.':S'.$vitridong)->applyFromArray(
                                            array(
                                                'fill' => array(
                                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                                    'color' => array('rgb' => 'f3f3f3')
                                                )
                                            )
                                        );
                                     }

                                     $objPHPExcel->setActiveSheetIndex(0)
                                                ->setCellValue('A'.$vitridong, $idsp2)
                                                ->setCellValue('B'.$vitridong, $solanxem)
                                                ->setCellValue('C'.$vitridong, $thutu)
                                                ->setCellValue('D'.$vitridong, $anhien)
                                                ->setCellValue('E'.$vitridong, $id_noidung_lang)
                                                ->setCellValue('F'.$vitridong, $idlang)
                                                ->setCellValue('G'.$vitridong, $tenlang)
                                                ->setCellValue('H'.$vitridong, $ten)
                                                ->setCellValue('I'.$vitridong, $tieude)
                                                ->setCellValue('J'.$vitridong, $link)
                                                ->setCellValue('K'.$vitridong, $target)
                                                ->setCellValue('L'.$vitridong, $mota)
                                                ->setCellValue('M'.$vitridong, $noidung)
                                                ->setCellValue('N'.$vitridong, $tukhoa)
                                                ->setCellValue('O'.$vitridong, $motatukhoa)
                                                ->setCellValue('P'.$vitridong, $tags);
                                    // xoa trang neu la ngon ngu thu 2 tro di
                                    $idsp2    = '';
                                     $masp     = '';
                                     $idtype   = '';
                                     $gia      = '';
                                     $giagoc   = '';
                                     $solanxem = '';
                                     $thutu    = '';
                                     $anhien   = '';
                                 }
                             } else {
                                 $vitridong +=1;
                                 $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValue('A'.$vitridong, $idsp)
                                            ->setCellValue('B'.$vitridong, '')
                                            ->setCellValue('C'.$vitridong, '')
                                            ->setCellValue('D'.$vitridong, '')
                                            ->setCellValue('E'.$vitridong, '')
                                            ->setCellValue('F'.$vitridong, $idlang)
                                            ->setCellValue('G'.$vitridong, '')
                                            ->setCellValue('H'.$vitridong, '')
                                            ->setCellValue('I'.$vitridong, '')
                                            ->setCellValue('J'.$vitridong, '')
                                            ->setCellValue('K'.$vitridong, '')
                                            ->setCellValue('L'.$vitridong, '')
                                            ->setCellValue('M'.$vitridong, '')
                                            ->setCellValue('N'.$vitridong, '')
                                            ->setCellValue('O'.$vitridong, '')
                                            ->setCellValue('P'.$vitridong, '');

                                 if ($vitridongsp%2==0) {
                                     // background
                                    $objPHPExcel->getActiveSheet()->getStyle('A'.$vitridong.':S'.$vitridong)->applyFromArray(
                                        array(
                                            'fill' => array(
                                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                                'color' => array('rgb' => 'f3f3f3')
                                            )
                                        )
                                    );
                                 }
                                // end
                             }
                         }
                     }
                 }
             }
         }
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="gioithieu.xlsx"');
         header('Cache-Control: max-age=0');
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         $objWriter->save('php://output');

         $_SESSION['array_cid'] = '';
     } else { // neu chua chon muc nao ma scrip khong băt loi thi qua day
        echo'<script>swal("Vui lòng chọn từ danh sách"); </script>';
     }
