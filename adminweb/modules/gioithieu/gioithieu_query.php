<?php

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;
use Lazer\Classes\Database as Lazer;
// create an image manager instance with favored driver
$manager = new ImageManager(array('driver' => 'gd'));
$json = Lazer::table($__config['jsontable'])->find(1);

if( ws_post('canvas') ) {
    $json->canvas = (int)ws_post('canvas');
}

if( ws_post('color') ) {
    $json->color = (string)ws_post('color');
}


$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];

// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    #
    # TRƯỜNG HỢP ẨN TIN
    # ---------------------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $result       = $db->sqlUpdate($__table, [
            "anhien" => 0
        ], "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'publish') {
    #
    # TRƯỜNG HỢP HIỆN TIN
    # ---------------------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $result       = $db->sqlUpdate($__table, [
            "anhien" => 1
        ], "id = '{$__postid}' ");
    }
} elseif ($__post_task == 'saveorder') {
    #
    # TRƯỜNG HỢP SẮP XẾP TIN
    # ---------------------------------
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $db->rawQuery(
            "UPDATE $__table SET thutu = '{$value_order}'
            WHERE id = '{$id_order}'"
        );
    }
    $db->thuTu($__table, $__id, "thutu", "loai", $__config['loai']);
} elseif ($__post_task == 'excel_export') {
    $array_cid             = ws_post('cid');
    $_SESSION['array_cid'] = $array_cid;
    echo '<script>location.href="modules/noidung/noidung_exportexcel.php";</script>';
} elseif ($__post_task == 'remove') {
    #
    # TRƯỜNG HỢP XÓA TIN
    # ---------------------------------
    // kiem tra quyen xoa du lieu
    if ($__xoa == 0) {
        echo '<script language="javascript">alert("' . $arraybien['khongcoquyenxoa'] . '"); location.href="./?op=' . $__getop . '"; </script>';
        exit();
    }
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            // xoa hinh trong bang tbl_noidung_hinh
            $s_hinh_chitiet = "select hinh from tbl_noidung_hinh where idnoidung = '" . $id_order . "' ";
            $d_hinh_chitiet = $db->rawQuery($s_hinh_chitiet);
            if (count($d_hinh_chitiet) > 0) {
                foreach ($d_hinh_chitiet as $key_hinh_chitiet => $info_hinh_chitiet) {
                    if (file_exists($__config["path_img"] . $info_hinh_chitiet['hinh'])) {
                        unlink($__config["path_img"] . $info_hinh_chitiet['hinh']);
                    }
                    if (file_exists($__config["path_img"] . "thumb/" . $info_hinh_chitiet['hinh'])) {
                        unlink($__config["path_img"] . "thumb/" . $info_hinh_chitiet['hinh']);
                    }
                }
            }
            if ($value_file != '') {
                if (file_exists($__config['path_file'] . $value_file)) {
                    unlink($__config['path_file'] . $value_file);
                }
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " idnoidung  = '" . $id_order . "' ");
            // xoa bang tbl_noidung_hinh
            $db->sqlDelete("tbl_noidung_hinh", " idnoidung  = '" . $id_order . "' ");
        }
    }
}
/** Tao thu muc neu chua ton tai */
create_dir($__config['path_img']);
create_dir($__config['path_file'], false);

if ($__getaction == 'save') {
    #
    # TRƯỜNG HỢP LƯU TIN
    # --------------------
    function restructure_array(array $images)
    {
        $result = array();
        $val    = array();
        if( count($images) > 0 ) {
            foreach ($images as $key => $value) {
                foreach ($value as $k => $val) {
                    for ($i = 0; $i < count($val); $i++) {
                        @$result[$i][$k] = $val[$i];
                    }
                }
            }
            return $result;
        }
    }
    $images   = restructure_array($_FILES);
    $thutu    = 1;
    $loai     = $__config['loai'];
    $idtype   = addslashes(trim(ws_post('idtype')));
    $masp     = addslashes(trim(ws_post('masp')));
    $hinh     = addslashes(trim(ws_post('filefile')));
    $file     = addslashes(trim(ws_post('file')));
    $gia      = addslashes(trim(ws_post('gia')));
    $giagoc   = addslashes(trim(ws_post('giagoc')));
    $colvalue = ws_post('colvalue');
    if (is_array($colvalue)) {
        $colvalue = implode(",", $colvalue);
    }
    $ngay        = addslashes(trim(ws_post('ngay')));
    $ngaycapnhat = addslashes(trim(ws_post('ngaycapnhat')));
    $solanxem    = addslashes(trim(ws_post('solanxem')));
    $soluong     = addslashes(trim(ws_post('soluong')));
    $anhien      = addslashes(trim(ws_post('anhien')));
    $xoaicon     = addslashes(trim(ws_post('xoaicon')));
    $xoaimg      = addslashes(trim(ws_post('xoaimg')));
    $xoafile     = addslashes(trim(ws_post('xoafile')));
    $iconname    = addslashes(trim(ws_post('iconname')));
    $imgname     = addslashes(trim(ws_post('imgname')));
    $filename    = addslashes(trim(ws_post('filename')));
    $_getidSP    = "";
    $ngay        = $db->getDateTimes();
    $ngaycapnhat = $db->getDateTimes();

    // Create url lien ket
    $urllink     = '';
    $_cid        = ws_get('id');
    $images_name = ws_post('url'.$__defaultlang);
    // lay id danh muc
    $_postselector = ws_post('selector');
    $str_selector  = '';
    if (count($_postselector) > 0) {
        for ($i = 0; $i < count($_postselector); $i++) {
            if ($i > 0) {
                $str_selector .= ',' . $_postselector[$i];
            } else {
                $str_selector .= $_postselector[$i];
            }
        }
    }

    if ($__getid == '') {
        #
        # TRƯỜNG HỢP THÊM MỚI DỮ LIỆU
        # ----------------------------
        $db->rawQuery("UPDATE {$__table} SET thutu = thutu+1 WHERE 1 = 1 ");
        // up load icon
        if ($icon != '') {
            $extfile  = pathinfo($icon, PATHINFO_EXTENSION);
            $iconfile = $images_name . '-icon.' . $extfile;
            if (file_exists($__config["path_img"] . $iconfile)) {
                $iconfile = rand(0, 100) . $iconfile;
            }
            move_uploaded_file($_FILES["icon"]["tmp_name"], $__config["path_img"] . $iconfile);
        }

        // upload File
        $filefile = null;
        if ($file != '') {
            $extfile  = pathinfo($file, PATHINFO_EXTENSION);
            $filefile = $images_name . '-file.' . $extfile;
            if (file_exists($__config["path_img"] . $filefile)) {
                $filefile = rand(100) . $filefile;
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], $__config['path_img'] . $filefile);
        }

        // THÊM MỚI VÀO BẢNG $__TABLE
        $id_insert = $db->insert($__table, [
            "idtype"      => @$str_selector,
            "masp"        => $masp,
            "hinh"        => $filefile,
            "file"        => $file,
            "gia"         => $gia,
            "giagoc"      => $giagoc,
            "colvalue"    => $colvalue,
            "ngay"        => $ngay,
            "ngaycapnhat" => $ngaycapnhat,
            "solanxem"    => $solanxem,
            "thutu"       => $thutu,
            "loai"        => $loai,
            "soluong"     => $soluong,
            "anhien"      => $anhien,
            "iduser"      => $_SESSION['user_id'],
        ]);

        if ($id_insert > 0) {
            // kiểm tra thêm vào nhóm dữ liệu
            $_postnhom   = ws_post('nhom');
            $soluongnhom = count($_postnhom);
            if ($soluongnhom > 0) {
                for ($u = 0; $u < $soluongnhom; $u++) {
                    $idnhom = @$_postnhom[$u];

                    // Tìm Data type
                    $data_type = $db->getNameFromID(
                        "tbl_option_type",
                        "datatype",
                        "id",
                        $idnhom
                    );

                    if( !$data_type ) {
                        // Kiểu text

                        $qlang = $db->rawQuery("
                            SELECT a.id,a.idkey,b.ten
                            FROM tbl_lang AS a
                            INNER JOIN tbl_lang_lang AS b On  a.id = b.iddanhmuc
                            WHERE b.idlang = '{$__defaultlang}'
                                AND a.anhien = 1
                            ORDER BY thutu ASC
                        ");

                        if( count($qlang) > 0 ) {
                            foreach( $qlang as $lang ) {
                                $idlang = $lang['id'];
                                $data_nhomi = ws_post('nhom_val_'.$idnhom.'_'.$idlang);

                                // thêm dữ liệu vào bảng tbl_option_value
                                $aray_nhom_val = array(
                                    "idlang"       => $idlang,
                                    "idnoidung"    => $id_insert,
                                    "idoptiontype" => $idnhom,
                                    "noidung"      => $data_nhomi);
                                $db->insert("tbl_option_value", $aray_nhom_val);
                            }
                        }
                    }
                    else {
                        // chạy vòng lặp để lấy giá trị của từng nhóm
                        $data_nhomi        = @ws_post('nhom_val_'.$idnhom);
                        $noidungnhomcanluu = '';
                        if (is_array($data_nhomi) == 1) {
                            // kiem tra co phai nhom khong
                            if (count($data_nhomi) > 0) {
                                // dem nhom >0
                                for ($k = 0; $k < count($data_nhomi); $k++) {
                                    if ($k == 0) {
                                        // neu la phan tu đầu tiên
                                        $noidungnhomcanluu .= $data_nhomi[$k];
                                    } else {
                                        $noidungnhomcanluu .= ',' . $data_nhomi[$k];
                                    }
                                }
                            }
                        } else {
                            $noidungnhomcanluu = $data_nhomi;
                        } // end if du lieu la nhom
                        // thêm dữ liệu vào bảng tbl_option_value

                        $db->insert("tbl_option_value", [
                            "idnoidung"    => $id_insert,
                            "idoptiontype" => $idnhom,
                            "noidung"      => $noidungnhomcanluu
                        ]);
                    }
                }
            }

        } // end insert thanh cong
        // them du lieu vao bang noi dung danh muc
        $d_lang = $db->rawQuery("
            SELECT a.id,a.idkey,b.ten
            FROM tbl_lang AS a
            INNER join tbl_lang_lang AS b On  a.id = b.iddanhmuc
            WHERE b.idlang = '{$__defaultlang}'
            AND a.anhien = 1
            ORDER by thutu Asc
        ");
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = addslashes(trim(@ws_post('ten'.$idlang)));
                $tieude     = addslashes(trim(@ws_post('tieude'.$idlang)));
                $mota       = replace_image_content(trim(@ws_post('mota'.$idlang)), $ten);
                $noidung    = replace_image_content(trim(@ws_post('noidung'.$idlang)), $ten);
                $url        = addslashes(trim(@ws_post('url'.$idlang)));
                $link       = addslashes(trim(@ws_post('link'.$idlang)));
                $target     = addslashes(trim(@ws_post('target'.$idlang)));
                $tukhoa     = addslashes(trim(@ws_post('tukhoa'.$idlang)));
                $motatukhoa = addslashes(trim(@ws_post('motatukhoa'.$idlang)));
                $tag        = addslashes(trim(@ws_post('tag'.$idlang)));
                $urlwebsite = addslashes(trim(@ws_post('urlwebsite'.$idlang)));
                // kiem tra url neu da co roi thì them ki tu cuoi url
                $d_checkurl = $db->rawQuery("
                    SELECT url FROM {$__tablenoidung} where url = '{$url}'
                ");
                if (count($d_checkurl) > 0) {
                    $url = $url . '-' . rand(0, 100);
                }
                // luu du lieu vao bang danh muc lang

                $db->insert($__tablenoidung, [
                    "idnoidung"  => $id_insert,
                    "idlang"     => $idlang,
                    "ten"        => $ten,
                    "tieude"     => $tieude,
                    "url"        => $url,
                    "link"       => $link,
                    "target"     => $target,
                    "mota"       => $mota,
                    "noidung"    => $noidung,
                    "tukhoa"     => $tukhoa,
                    "motatukhoa" => $motatukhoa,
                    "tag"        => $tag,
                ]);
            }
        }

    } else {
        #
        # TRƯỜNG HỢP CẬP NHẬT DỮ LIỆU
        # ------------------------------------
        $id_update = $db->sqlUpdate($__table, [
            "idtype"      => $str_selector,
            "masp"        => $masp,
            "gia"         => $gia,
            "giagoc"      => $giagoc,
            "colvalue"    => $colvalue,
            "ngaycapnhat" => $ngaycapnhat,
            "solanxem"    => $solanxem,
            "soluong"     => $soluong,
            "anhien"      => $anhien,
            "iduser"      => $_SESSION['user_id'],
        ], "id = '{$__postid}' ");

        if ($id_update > 0) {

            // kiểm tra cập nhật hoặc thêm nhóm dữ liệu khi update thành công
            $_postnhom   = ws_post('nhom');
            $soluongnhom = count($_postnhom);
            if ($soluongnhom > 0) {
                for ($u = 0; $u < $soluongnhom; $u++) {
                    $idnhom = $_postnhom[$u];

                    // Tìm Data type
                    $data_type = $db->getNameFromID(
                        "tbl_option_type",
                        "datatype",
                        "id",
                        $idnhom
                    );

                    if( !$data_type ) {
                        // Kiểu text
                        $qlang = $db->rawQuery("
                            SELECT a.id,a.idkey,b.ten
                            FROM tbl_lang AS a
                            INNER JOIN tbl_lang_lang AS b On  a.id = b.iddanhmuc
                            WHERE b.idlang = '{$__defaultlang}'
                                AND a.anhien = 1
                            ORDER BY thutu ASC
                        ");

                        if( count($qlang) > 0 ) {
                            foreach( $qlang as $lang ) {
                                $idlang = $lang['id'];
                                $data_nhomi = $_POST['nhom_val_'.$idnhom.'_'.$idlang];

                               // kiểm tra xem tbl_option_value đã có hay chưa.
                               // nếu chưa có thì insert else update
                                $check_exis_nhom = $db->GetNameFromID(
                                    "tbl_option_value",
                                    "id",
                                    "idnoidung", "'{$__postid}' and idoptiontype = '{$idnhom}' AND idlang = {$idlang} ");

                                if ($check_exis_nhom > 0) {
                                    // thêm dữ liệu vào bảng tbl_option_value
                                    $aray_nhom_val = array("noidung" => $data_nhomi);

                                    $db->where("idnoidung", $__postid);
                                    $db->where("idoptiontype", $idnhom);
                                    $db->where("idlang", $idlang);
                                    $db->update( "tbl_option_value", $aray_nhom_val);
                                } else {
                                    // cập nhật liệu vào bảng tbl_option_value
                                    $aray_nhom_val = array(
                                        "idnoidung"    => $__postid,
                                        "idlang"       => $idlang,
                                        "idoptiontype" => $idnhom,
                                        "noidung"      => $data_nhomi
                                    );
                                    $db->insert("tbl_option_value", $aray_nhom_val);
                                }
                            }
                        }
                    }
                    else {

                        // chạy vòng lặp để lấy giá trị của từng nhóm
                        $data_nhomi = @ws_post('nhom_val_'.$idnhom);
                        $noidungnhomcanluu = '';
                        if (is_array($data_nhomi) == 1) {
                            // kiem tra co phai nhom khong
                            if (count($data_nhomi) > 0) {
                                // dem nhom >0
                                for ($k = 0; $k < count($data_nhomi); $k++) {
                                    if ($k == 0) {
                                        // neu la phan tu đầu tiên
                                        $noidungnhomcanluu .= $data_nhomi[$k];
                                    } else {
                                        $noidungnhomcanluu .= ',' . $data_nhomi[$k];
                                    }
                                }
                            }
                        } else {
                            $noidungnhomcanluu = $data_nhomi;
                        } // end if du lieu la nhom
                        // kiểm tra xem tbl_option_value đã có hay chưa. nếu chưa có thì insert else update
                        $check_exis_nhom = $db->getNameFromID("tbl_option_value", "id", "idnoidung", "'{$__postid}' and idoptiontype = '{$idnhom}' ");
                        if ($check_exis_nhom > 0) {
                            // thêm dữ liệu vào bảng tbl_option_value
                            $aray_nhom_val = array();
                            $db->sqlUpdate("tbl_option_value", [
                                "noidung" => $noidungnhomcanluu
                            ], "idnoidung = '{$__postid}' AND idoptiontype = '{$idnhom}' ");
                        } else {
                            // cập nhật liệu vào bảng tbl_option_value
                            $db->insert("tbl_option_value", [
                                "idnoidung"    => $__postid,
                                "idoptiontype" => $idnhom,
                                "noidung"      => $noidungnhomcanluu
                            ]);
                        }
                    }
                }
            }
        }
        // them du lieu vao bang noi dung danh muc
        $d_lang = $db->rawQuery("
            SELECT a.id,a.idkey,b.ten
            FROM tbl_lang AS a
            INNER JOIN tbl_lang_lang AS b
                On a.id = b.iddanhmuc
            WHERE b.idlang = '{$__defaultlang}'
                AND a.anhien = 1
            ORDER by thutu Asc
        ");
        if (count($d_lang) > 0) {
            foreach ($d_lang as $key_lang => $info_lang) {
                // lap theo so luong ngon ngu
                $tenlang = $info_lang['ten'];
                $idlang  = $info_lang['id'];
                $idkey   = $info_lang['idkey'];
                // get noi dung post qua
                $ten        = addslashes(trim(@ws_post('ten'.$idlang)));
                $tieude     = addslashes(trim(@ws_post('tieude'.$idlang)));
                $mota       = replace_image_content(trim(@ws_post('mota'.$idlang)), $ten);
                $noidung    = replace_image_content(trim(@ws_post('noidung'.$idlang)), $ten);
                $url        = addslashes(trim(@ws_post('url'.$idlang)));
                $link       = addslashes(trim(@ws_post('link'.$idlang)));
                $target     = addslashes(trim(@ws_post('target'.$idlang)));
                $tukhoa     = addslashes(trim(@ws_post('tukhoa'.$idlang)));
                $motatukhoa = addslashes(trim(@ws_post('motatukhoa'.$idlang)));
                $tag        = addslashes(trim(@ws_post('tag'.$idlang)));
                $urlwebsite = addslashes(trim(@ws_post('urlwebsite'.$idlang)));
                // kiem tra url neu da co roi thì them ki tu cuoi url
                if (ws_post('hi_url'.$idlang) != $url) {
                    $d_checkurl = $db->rawQuery("
                        SELECT url from {$__tablenoidung}
                        WHERE url = '{$url}'
                    ");
                    if (count($d_checkurl) > 0) {
                        $url = $url . '-' . rand(0, 100);
                    }
                }
                // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                $d_check_tontai = $db->rawQuery("
                    SELECT id from {$__tablenoidung}
                    WHERE idnoidung = '{$__postid}' and idlang = '{$idlang}'
                ");
                if (count($d_check_tontai) > 0) {
                    // da tồn tại nội dung rồi thì update lại nội dung
                    // cap nhat lai du lieu
                    $db->sqlUpdate($__tablenoidung, [
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "mota"       => $mota,
                        "noidung"    => $noidung,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                        "tag"        => $tag,
                    ], "idnoidung = '{$__postid}' AND idlang = '{$idlang}'");
                } else {

                    $db->insert($__tablenoidung, [
                        "idnoidung"  => $__postid,
                        "idlang"     => $idlang,
                        "ten"        => $ten,
                        "tieude"     => $tieude,
                        "url"        => $url,
                        "link"       => $link,
                        "target"     => $target,
                        "mota"       => $mota,
                        "noidung"    => $noidung,
                        "tukhoa"     => $tukhoa,
                        "motatukhoa" => $motatukhoa,
                        "tag"        => $tag,
                    ]);
                } // end kiem tra thêm mới hay cập nhật
            } // end for lang
        } // end count lang
        // kiem tra neu thay hinh thi sẽ xóa hình cũ đi
        // xoa icon
        if ($xoaicon == 1) {
            unlink($__config["path_img"] . $iconname);
            $aray_insert = array("icon" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // xoa img
        if ($xoaimg == 1) {
            if (file_exists($__config["path_img"] . $imgname)) {
                unlink($__config["path_img"] . $imgname);
            }
            $aray_insert = array("img" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // xoa file
        if ($xoafile == 1) {
            unlink($__config["path_img"] . $filename);
            $aray_insert = array("file" => "");
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat icon
        if ($icon != '') {
            if ($iconname != '') {
                unlink($__config["path_img"] . $iconname);
            }
            //up file moi len
            $extfile  = pathinfo($icon, PATHINFO_EXTENSION);
            $iconfile = $images_name . '-icon.' . $extfile;
            if (file_exists($__config["path_img"] . $iconfile)) {
                $iconfile = rand(0, 100) . $iconfile;
            }
            move_uploaded_file($_FILES["icon"]["tmp_name"], "../uploads/noidung/$iconfile");
            $aray_insert = array("icon" => $iconfile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // neu cap nhat file
        if ($file != '') {
            if ($filename != '') {
                unlink($__config["path_img"] . $filename);
            }
            //up file moi len
            $extfile  = pathinfo($file, PATHINFO_EXTENSION);
            $filefile = $images_name . '-file.' . $extfile;
            if (file_exists($__config["path_img"] . $filefile)) {
                $filefile = rand(0, 100) . $filefile;
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], $__config['path_img'] . $filefile);
            $aray_insert = array("file" => $filefile);
            $result      = $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}'  ");
        }
        // update idtype neu chuyen danh muc di vi tri khac
        if ( ws_post('hi_parenid')  != ws_post('parenid') ) {
            $subid = $db->createSubID($__table, $__id, ws_post('parenid'));
            // update bang tbl_danhmuc
            $db->sqlUpdate($__table, array("id" => $subid), "id = '{$__postid}' ");
            // update bang tbl_danhmuc_lang
            $aray_insert_lang = array("iddanhmuc" => $subid);
            $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = $__postid");
        }
    } // end if cap nhat

    // XỬ  LÝ HÌNH ẢNH
    // kiem tra xem co hinh chi tiet hay khong
    $d_hinh = $db->rawQuery("
        SELECT hinh,id FROM tbl_noidung_hinh
        WHERE idnoidung = '{$_SESSION['tmpimg']}' ORDER BY id ASC
    ");

    if (count($d_hinh) > 0) {
        foreach ($d_hinh as $key_hinh => $info_hinh) {
            $id_hinh   = $info_hinh['id'];
            $hinh_hinh = $info_hinh['hinh'];

            // truy van lay ngon ngu dau tien
            $db->where("anhien", 1);
            $db->orderBy("thutu", "asc");
            $id_lang_first = $db->getValue("tbl_lang", "id", 1);
            if ( $id_lang_first ) {
                // kiem tra neu tieu de khac rong
                $tieude_first = @ws_post('url'.$id_lang_first);
            }

            $hinhsave = $hinh_hinh;
            // kiem tra va doi lại tên hình
            if ( $tieude_first ) {
                $ext_img     = pathinfo($hinh_hinh, PATHINFO_EXTENSION);
                $hinhsave    = $tieude_first . '-' . $key_hinh . '.' . $ext_img;
            }

            // Kiểm tra tồn tại
            $db->where("hinh = '{$hinhsave}' OR hinh = '{$hinh_hinh}'");
            $row = $db->getOne("tbl_noidung_hinh", "count(*)");
            if( $row ) {
                $ext = pathinfo($hinhsave, PATHINFO_EXTENSION);
                $nme = pathinfo($hinhsave, PATHINFO_FILENAME);
                $hinhsave = $nme."-".rand(111,999).".".$ext;
            }

            // đưa hình vào thư mục nội dung
            $img_path = "plugin/dropzon/upload/";

            resize_image([
                "json"    => $json,
                "class"   => $manager,
                "pathin"  => $img_path,
                "pathout" => $__config["path_img"],
                "imgin"   => $hinh_hinh,
                "imgout"  => $hinhsave,
            ]);

            if ( ws_post('inlogolenhinh') ) {
                $hinh_logo = '../uploads/logo/' . ws_post('inlogolenhinh');
                $hinh_goc  = $__config["path_img"] . $hinhsave;
                in_logo_len_hinh($hinh_goc, $hinh_logo, $__config["path_img"] . $hinhsave, ws_post('vitrilogo'));
            }

            // XỬ LÝ HÌNH THUMB
            resize_image([
                "json"    => $json,
                "class"   => $manager,
                "pathin"  => $__config["path_img"],
                "pathout" => $__config["path_img"] . "thumb/",
                "imgin"   => $hinhsave,
                "imgout"  => $hinhsave,
                "thumb"   => true,
            ]);

            if( $__getid ) {
                // xoa hinh hien tai
                unlink($img_path . $hinh_hinh);
            }

            // cap nhat lại hình đại diện
            if ($key_hinh == 0) {

                if( $__getid ) {
                    // UPDATE
                    $get_hinhdaidien = $db->getNameFromID(
                        "tbl_noidung", "hinh", "id", $__postid);
                    $flag = 0;

                    if ( $get_hinhdaidien ) {
                        $check_hinhdaidien = $db->getNameFromID(
                            "tbl_noidung_hinh", "hinh", "hinh",
                            "'{$get_hinhdaidien}' AND idnoidung = '{$__postid}' ");
                        if ( $check_hinhdaidien ) {
                            $flag = 1;
                        }
                    }
                    if ( !$flag ) {
                        // set hình đàu tiên là default
                        $db->sqlUpdate("tbl_noidung_hinh", array(
                            "hinhdaidien" => 1
                        ), "id = '{$id_hinh}' ");
                        // up hinh dai dien cua bang noi dung
                        $db->sqlUpdate("tbl_noidung", [
                            "hinh" => $hinhsave
                        ], "id = '{$__postid}' ");
                    }

                }
                else {
                    //INSERT
                    // set hình đàu tiên là default
                    $db->sqlUpdate("tbl_noidung_hinh", [
                        "hinhdaidien" => 1
                    ], "id = '{$id_hinh}' ");

                    // up hinh dai dien cua bang noi dung
                    $db->sqlUpdate("tbl_noidung", [
                        "hinh" => $hinhsave
                    ], "id = '{$id_insert}' ");
                }

            }

            //upate lại tên hình cho bảng tbl_noidung_hinh
            if ( $tieude_first ) {
                $db->sqlUpdate("tbl_noidung_hinh", [
                    "hinh" => $hinhsave
                ], "id = '{$id_hinh}' ");
            }

            // cap nhat lại id trong bảng tbl_noidung_hinh => idnoidung theo id mới thêm vào
            $db->sqlUpdate("tbl_noidung_hinh", [
                "idnoidung" => ($__postid ? $__postid : $id_insert),
                "tmp"       => 0
            ], "id = '{$id_hinh}' ");
        } // end for
    } //end coutn

    // xoa hinh neu như đã up lên nhưng hủy upload hoặc tắt ngang trình duyệt
    $ngayhientai       = $db->getDate();
    $d_timhinh_quadate = $db->rawQuery("
        SELECT hinh,id from tbl_noidung_hinh
        WHERE ngaytao < '{$ngayhientai}'
        AND tmp = 1
    ");
    if (count($d_timhinh_quadate) > 0) {
        foreach ($d_timhinh_quadate as $key_timhinh => $info_timhinh) {
            $timhinh_name = $info_timhinh['hinh'];
            $timhinh_id   = $info_timhinh['id'];
            $linkxoa      = "plugin/dropzon/upload/" . $timhinh_name;
            unlink($linkxoa);
            // xoa trong csdl
            $db->sqlDelete("tbl_noidung_hinh", " id = '{$timhinh_id}' ");
        }
    }
}

$link_redirect = './?op=' . ws_get('op') . '&page=' . ws_post('getpage');
if ($_SESSION['idtype'] != '') {
    $link_redirect .= '&idtype=' . $_SESSION['idtype'];
}
if ( ws_post('luuvathem') == 'luuvathem') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_post('op') . '&method=frm&id=' . $id;
}
if ( ws_post('saveandnew') == 'saveandnew') {
    $id = $id_insert;
    if ($__getid != '') {
        $id = $__getid;
    }
    $link_redirect = './?op=' . ws_post('op') . '&method=frm';
}
echo '<script>location.href="' . $link_redirect . '"; </script>';
