<?php
include 'plugin/ResizeImage/SimpleImage.php';
$ResizeImage = new SimpleImage();
$__config    = array(
    "module_title"    => "Admin Menu",
    "table"           => 'tbl_user_group',
    "tablenoidung"    => 'tbl_user_group_lang',
    "id"              => 'id',
    "loai"            => 1,
    "thutu"           => 1,
    "ten"             => 1,
    "link"            => 1,
    "anhien"          => 1,
    "action"          => 1,
    "idgroup"         => 1,
    "add_item"        => 1,
    "chucnangkhac"    => 1,
    "home"            => 1,
    "icon"            => 1,
    "bg"              => 1,
    "sizeimagesthumb" => 300,
);
$_SESSION['__config']        = array();
$_SESSION['__config']        = $__config;
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
if ($__getmethod == 'query') {
    include $__getop . "_query.php";
} else if ($__getmethod == 'frm') {
    include $__getop . "_frm.php";
} else {
    include $__getop . "_view.php";
}
