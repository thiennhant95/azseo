<?php
$__table        = $__config['table'];
$__tablenoidung = $__config['tablenoidung'];
$__id           = $__config['id'];
// lay cid
$count_cid = count(ws_post('cid'));
if ($__post_task == 'unpublish') {
    #
    # TRƯỜNG HỢP BẤM ẨN
    # -------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 0);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'publish') {
    #
    # TRƯỜNG HỢP BẤM HIỆN
    # -------------------
    for ($k = 0; $k < $count_cid; $k++) {
        $__postid     = ws_post('cid')[$k];
        $arraycollumn = array("anhien" => 1);
        $result       = $db->sqlUpdate($__table, $arraycollumn, "id = '{$__postid}' ");
    }
} else if ($__post_task == 'saveorder') {
    #
    # TRƯỜNG HỢP ĐỔI THỨ TỰ
    # -------------------
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($a = 0; $a < $soluong_row; $a++) {
        $id_order    = ws_post('cid')[$a];
        $value_order = ws_post('order')[$a];
        $sql_update  = "update $__table set thutu = $value_order where id = $id_order ";
        $db->rawQuery($sql_update);
    }
} else if ($__post_task == 'remove') {
    #
    # TRƯỜNG HỢP BẤM XÓA
    # -------------------
    $soluong_row = count(ws_post('cid'));
    $array_cid   = ws_post('cid');
    for ($r = 0; $r < $soluong_row; $r++) {
        $id_order   = ws_post('cid')[$r];
        $value_img  = ws_post('img')[$id_order];
        $value_file = ws_post('file')[$id_order];
        // thay doi thu tu san pham
        $thutu_value = $db->getNameFromID($__table, "thutu", "id", $id_order);
        $supdate     = "UPDATE $__table set thutu  = (thutu - 1) where thutu > $thutu_value  and Loai = '" . (isset($__config['Loai']) ? $__config['Loai'] : '') . "' ";

        if ($db->sqlDelete($__table, " id = '{$id_order}' ") == 1) {
            if ($value_img != '') {
                $__path      = $__config['path_img'] . $value_img;
                $__paththumb = $__config['path_img'] . 'thumb/' . $value_img;
                if (file_exists($__path)) {
                    @unlink($__path);
                }
                if (file_exists($__paththumb)) {
                    @unlink($__paththumb);
                }
            }
            if ($value_file != '') {
                $__path = $__config['path_file'] . $value_file;
                @unlink($__path);
            }
            // xoa noi dung bang danhmuc_lang
            $db->sqlDelete($__tablenoidung, " iddanhmuc  = '" . $id_order . "' ");
            $db->sqlDelete("tbl_mausac", " idtype  = '{$id_order}' ");
        }
    }
}
if ($__getaction == 'save') {
    #
    # TRƯỜNG HỢP BẤM NÚT LƯU
    # -------------------
    $idtype     = trim(addslashes(ws_post('idtype')));
    $loai       = trim(addslashes(ws_post('loai')));
    $thutu      = trim(addslashes(ws_post('thutu')));
    $anhien     = trim(addslashes(ws_post('anhien')));
    $id         = trim(addslashes(ws_post('id')));
    $link       = trim(addslashes(ws_post('link')));
    $idgroup    = trim(addslashes(ws_post('idgroup')));
    $subid      = trim(addslashes(ws_post('subid')));
    $anhien     = trim(addslashes(ws_post('anhien')));
    $home       = trim(addslashes(ws_post('home')));
    $background = trim(addslashes(ws_post('background')));

    // Tạo thư mục tương ứng với modlude tao ra
    $thumuctao = substr($link,6);
    if (!is_dir('modules/'.$thumuctao)) {
        mkdir('modules/'.$thumuctao, 0777, true);
    }
    if (!empty(ws_post('return_color'))) {
        $color = trim(addslashes(ws_post('return_color')));
    } else {
        $color = '';
    }
    $_getidSP = "";
    if ($__getid != '') {
        // neu la cap nhat
        $subid      = $__getid;
        $linkExists = count($db->rawQuery("SELECT id FROM $__table WHERE link = '$link' AND id != $__getid "));
    } else {
        // neu them moi
        $subid      = $db->createSubID($__table, $__id, ws_post('parenid'));
        $linkExists = count($db->rawQuery("SELECT id FROM $__table WHERE link = '$link' "));
    }
    // Create url lien ket
    $urllink = '';
    $_cid    = ws_get('id');
    if (!empty(ws_post('action')) && ws_post('action') == 'add') {
        $thutu = (int) substr($subid, -3);
    } else {
        $thutu = (int) substr($id, -3);
    }
    // neu them moi danh mục
    if ($__getid == '') {
        #
        # TRƯỜNG HỢP THÊM MỚI DỮ LIỆU
        # -------------------
        /** Kiểm tra link đã tồn tại hay chưa */
        if ( !$linkExists ) {
            #
            # INSERT ICON
            # -------------------
            // B1. Neu la chon san
            $kieuIcon = ws_post('type_icon');
            if ($kieuIcon == 1 && $kieuIcon != -1) {
                $iconName = ws_post('icon_faicon');
            } else if ($kieuIcon == 0 && $kieuIcon != -1) {
                $iconName = $_FILES['icon_img']['name'];
                if (!empty($iconName)) {
                    $tenicon =  $images_name ."-". $iconName;
                    move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img']. $tenicon);
                    $iconName = $tenicon;
                }
            } else {
                $iconName = '';
            }
            // upload hinh
            if (!empty($img)) {
                $extfile = pathinfo($img, PATHINFO_EXTENSION);
                $imgfile = $images_name . '-img.' . $extfile;
                if (file_exists("../uploads/danhmuc/" . $imgfile)) {
                    $imgfile = rand(0, 100) . $imgfile;
                }
                move_uploaded_file($_FILES["img"]["tmp_name"], "../uploads/danhmuc/$imgfile");
                $ResizeImage->load("../uploads/danhmuc/" . $imgfile);
                $ResizeImage->resizeToWidth($__config['sizeimagesthumb']);
                $ResizeImage->save("../uploads/danhmuc/thumb/" . $imgfile);
            }
            // upload File
            if (!empty($file)) {
                $extfile  = pathinfo($file, PATHINFO_EXTENSION);
                $filefile = $images_name . '-file.' . $extfile;
                if (file_exists("../uploads/danhmuc/" . $filefile)) {
                    $filefile = rand(100) . $filefile;
                }
                move_uploaded_file($_FILES["file"]["tmp_name"], "../uploads/danhmuc/$filefile");
            }
            $thutu       = (int) substr($subid, -3);
            $aray_insert = array(
                "id"      => $subid,
                "link"    => $link,
                "idgroup" => $idgroup,
                "loai"    => $loai,
                "thutu"   => $thutu,
                "anhien"  => $anhien,
                "home"    => $home,
                "icon"    => $iconName,
            );
            $id_insert = $db->insert($__table, $aray_insert);
            $arr_Mau   = array(
                'background' => $background,
                'color'      => $color,
                'idtype'     => $subid,
                'module'     => $__getop,
                'anhien'     => 1,
            );
            $db->insert("tbl_mausac", $arr_Mau);
            // them du lieu vao bang noi dung danh muc
            $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
            $d_lang = $db->rawQuery($s_lang);
            if (count($d_lang) > 0) {
                foreach ($d_lang as $key_lang => $info_lang) {
                    $tenlang = isset($info_lang['ten']) ? $info_lang['ten'] : '';
                    $idlang  = isset($info_lang['id']) ? $info_lang['id'] : '';
                    $idkey   = isset($info_lang['idkey']) ? $info_lang['idkey'] : '';
                    // get noi dung post qua
                    $ten = replace_html(ws_post('ten'.$idlang));

                    // luu du lieu vao bang danh muc lang
                    $aray_insert_lang = array(
                        "iddanhmuc" => $subid,
                        "idlang"    => $idlang,
                        "ten"       => $ten);
                    $db->insert($__tablenoidung, $aray_insert_lang);
                }
            }
        } else {
            echo "<script>
                alert('Modules này đã tồn tại');
            </script>";
        }
    } else {
        #
        # TRƯỜNG HỢP CẬP NHẬT DỮ LIỆU
        # -------------------
        if ($linkExists == 0) {
            $aray_insert = array(
                "link"    => $link,
                "idgroup" => $idgroup,
                "loai"    => $loai,
                "anhien"  => $anhien,
                "home"    => $home,
            );
            $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");


            #
            # UPDATE ICON
            # -------------------
            $kieuIcon = ws_post('type_icon');
            $iconOld  = ws_post('iconname');
            if ($kieuIcon == 1 && $kieuIcon != -1) {
                // xoa icon cũ
                if (file_exists($__config['path_img'].$iconOld)) {
                    unlink($__config['path_img'].$iconOld);
                }
                // Trường hợp update là fa-icon
                $iconName = ws_post('icon_faicon');
                $db->sqlUpdate($__table, array("icon" => $iconName), "id = '{$__postid}'");
            } else if ($kieuIcon == 0 && $kieuIcon != -1) {
                $iconName = $_FILES['icon_img']['name'];
                if (!empty($iconName)) {
                    // 1. update icon new
                    // xoa icon cũ
                    if (file_exists($__config['path_img'].$iconOld)) {
                        unlink($__config['path_img'].$iconOld);
                    }
                    $tenicon =  $images_name ."-". $iconName;
                    move_uploaded_file($_FILES['icon_img']['tmp_name'], $__config['path_img']. $tenicon);
                    $iconName = $tenicon;
                    $db->sqlUpdate($__table, array("icon" => $iconName), "id = '{$__postid}'");
                }
            } else {
                $iconName = '';
                // 4. doi tu img -> khong (xoa icon)
                // xoa icon cũ
                if (file_exists($__config['path_img'].$iconOld)) {
                    unlink($__config['path_img'].$iconOld);
                }
                $db->sqlUpdate($__table, array("icon" => null), "id = '{$__postid}'");
            }
            // 2. check xoa icon
            if (ws_post('xoaicon') && ws_post('xoaicon') == 1) {
                if (file_exists($__config['path_img'].$iconOld)) {
                    unlink($__config['path_img'].$iconOld);
                }
                $db->sqlUpdate($__table, array("icon" => null), "id = '{$__postid}'");
            }


            // Kiem tra neu chua co thi them - co roi thi update
            $s_exits_mau = "SELECT id FROM tbl_mausac WHERE idtype = '{$__postid}' ";
            $d_exits_mau = $db->rawQuery($s_exits_mau);
            if (count($d_exits_mau) > 0) {
                $arr_Mau = array(
                    'background' => $background,
                    'color'      => $color,
                    'module'     => $__getop,
                );
                $db->sqlUpdate("tbl_mausac", $arr_Mau, "idtype = '{$__postid}' ");
            } else {
                $arr_Mau = array(
                    'background' => $background,
                    'color'      => $color,
                    'idtype'     => $__postid,
                    'module'     => $__getop,
                    'anhien'     => 1,
                );
                $db->insert("tbl_mausac", $arr_Mau);
            }
            // them du lieu vao bang noi dung danh muc
            $s_lang = "SELECT * from tbl_lang where anhien = 1 order by thutu Asc";
            $d_lang = $db->rawQuery($s_lang);
            if (count($d_lang) > 0) {
                foreach ($d_lang as $key_lang => $info_lang) {
                    // lap theo so luong ngon ngu
                    $tenlang = isset($info_lang['ten']) ? $info_lang['ten'] : '';
                    $idlang  = isset($info_lang['id']) ? $info_lang['id'] : '';
                    $idkey   = isset($info_lang['idkey']) ? $info_lang['idkey'] : '';
                    // get noi dung post qua
                    $ten = replace_html(ws_post('ten'.$idlang));

                    // kiem tra xem ngon ngu da co chưa. Nếu chưa có thêm thêm một dòng vào bảng tbl_danhmuc_lang
                    $s_check_tontai = "SELECT id from $__tablenoidung where iddanhmuc = '" . $__postid . "' and idlang = '{$idlang}' ";
                    $d_check_tontai = $db->rawQuery($s_check_tontai);
                    if (count($d_check_tontai) > 0) {
                        // da tồn tại nội dung rồi thì update lại nội dung
                        // cap nhat lai du lieu
                        $aray_insert_lang = array("ten" => $ten);
                        $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = '" . $__postid . "' and idlang = '{$idlang}'");
                    } else {
                        $aray_insert_lang = array(
                            "iddanhmuc" => $__postid,
                            "idlang"    => $idlang,
                            "ten"       => $ten);
                        $db->insert($__tablenoidung, $aray_insert_lang);
                    } // end kiem tra thêm mới hay cập nhật
                } // end for lang
            } // end count lang
            // update idtype neu chuyen danh muc di vi tri khac
            if (ws_post('hi_parenid') != ws_post('parenid')) {
                $subid = $db->createSubID($__table, $__id, ws_post('parenid'));
                // update bang tbl_danhmuc
                $aray_insert = array("id" => $subid);
                $db->sqlUpdate($__table, $aray_insert, "id = '{$__postid}' ");
                // update bang tbl_danhmuc_lang
                $aray_insert_lang = array("iddanhmuc" => $subid);
                $db->sqlUpdate($__tablenoidung, $aray_insert_lang, "iddanhmuc = $__postid");
            }
        } else {
            echo "<script>
                alert('Modules này đã tồn tại');
            </script>";
        }
    } // end if cap nhat
}
echo '<script> location.href="./?op=' . $__getop . '"; </script>';
