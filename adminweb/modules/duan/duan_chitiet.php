<?php
$s_noidung = "SELECT a.id,a.idtype,hinh,solanxem,ngaycapnhat,b.ten,url,link,target,mota,noidung,tag
           FROM tbl_noidung AS a
           INNER JOIN tbl_noidung_lang AS b
           ON a.id = b.idnoidung
           where b.idlang = '" . $_SESSION['__defaultlang'] . "'
           and url = '" . $_geturl . "'
           and anhien = 1
           order by thutu ASC ";
$d_noidung            = $db->rawQuery($s_noidung);
$data_noidung_chitiet = '';
if (count($d_noidung) > 0) {
    $d_noidung   = $d_noidung[0];
    $ten         = $d_noidung['ten'];
    $id          = $d_noidung['id'];
    $idtype      = $d_noidung['idtype'];
    $hinh        = $d_noidung['hinh'];
    $solanxem    = $d_noidung['solanxem'];
    $url         = $d_noidung['url'];
    $link        = $d_noidung['link'];
    $target      = $d_noidung['target'];
    $mota        = $d_noidung['mota'];
    $noidung     = $d_noidung['noidung'];
    $tag         = $d_noidung['tag'];
    $ngaycapnhat = $d_noidung['ngaycapnhat'];
    $ngayhientai = $db->getDateTimes();
    $arraylabel  = array("ngay" => " " . $arraybien['ngay'] . " ",
        "ngaytruoc"                 => " " . $arraybien['ngaytruoc'] . " ",
        "gio"                       => " " . $arraybien['gio'] . " ",
        "phut"                      => " " . $arraybien['phut'] . "",
        "luc"                       => "" . $arraybien['luc'] . " ",
        "cachday"                   => "" . $arraybien['cachday'] . " ");
    $data_noidung_chitiet .= '<div class="noidungchitiet">';
    $data_noidung_chitiet .= '<h1 class="title">' . $ten . '</h1>';
    if ($_array_config_noidung['ngay'] == 1) {
        $data_noidung_chitiet .= '<div class="ngaycapnhat">' . $db->ngaydang($ngayhientai, $ngaycapnhat, $arraylabel) . '</div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }
    $data_noidung_chitiet .= '<div class="clear"></div>';
    $data_noidung_chitiet .= '<div class="mota">' . $mota . '</div>';
    $data_noidung_chitiet .= '<div class="noidung">' . $noidung . '</div>';
// tag
    $tagarr     = '';
    $tagcontent = '';
    $tags       = trim($tags);
    $tagcontent = '';
    if ($tag != '') {
        $tagarr = explode('|', $tag);
        if (count($tagarr) > 0) {
            $tagcontent .= '<div class="clear"></div>';
            $tagcontent .= '<ul class="tags"><li class="tags_label"><i class="fa fa-tags"></i>Tags</li>';
            $soluongtag = count($tagarr);
            for ($i = 0; $i <= $soluongtag; $i++) {
                if ($tagarr[$i] != '' && $tagarr[$i] != ',') {
                    $link_tag = ROOT_PATH . urlencode(trim($tagarr[$i]) . '.tag');
                    $tagcontent .= '<li class="tagitem"><a href="' . $link_tag . '" title="' . $tagarr[$i] . '">' . $tagarr[$i] . '</a></li>';
                }
            }
            $tagcontent .= '</ul>';
        }
    }
    $data_noidung_chitiet .= $tagcontent;
    $data_noidung_chitiet .= '<div class="clear"></div>';
    if ($_array_config_noidung['commentfacebook'] == 1) {
        $data_noidung_chitiet .= '
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=401401003393108";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, \'script\', \'facebook-jssdk\'));</script>
         <div class="fb-comments" data-href="http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] . '"
      data-width="100%" data-numposts="5" data-colorscheme="light"></div>';
    }
    if ($_array_config_noidung['share'] == 1) {
        $data_noidung_chitiet .= include "plugin/share/share.php";
    }
    $data_noidung_chitiet .= '<div class="clear"></div><br />';
    // lay tin lien quan
    if ($_array_config_noidung['tinlienquan'] == 1) {
        $s_noidung_lienquan = "select a.hinh,a.ngaycapnhat,ten,tieude,url,link,target,mota
                       FROM tbl_noidung AS a
                       INNER JOIN tbl_noidung_lang AS b
                       ON a.id = b.idnoidung
                       where a.anhien = 1
                       and a.idtype like '%" . $__idtype_danhmuc . "%'
                       and b.idlang = '" . $_SESSION['__defaultlang'] . "'
                       and url != '" . $url . "'
                       order by thutu Asc
                       limit 0," . $_array_config_noidung['sotinlienquan'] . " ";
        $d_noidung_lienquan = $db->rawQuery($s_noidung_lienquan);
        if (count($d_noidung_lienquan) > 0) {
            $data_noidung_chitiet .= '<div class="title_tinlienquan"><i class="fa fa-chevron-circle-right"></i>' . $arraybien['tincungchuyenmuc'] . '<h2></h2></div>';
            if ($_array_config_noidung['tinlienquan_thumb'] == 1) // if thumb
            {
                foreach ($d_noidung_lienquan as $key_noidung_lienquan => $info_lienquan) {
                    $ten         = $info_lienquan['ten'];
                    $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_lienquan['hinh'];
                    $ngaycapnhat = $info_lienquan['ngaycapnhat'];
                    $tieude      = $info_lienquan['tieude'];
                    $url         = $info_lienquan['url'];
                    $link        = $info_lienquan['link'];
                    $target      = $info_lienquan['target'];
                    $mota        = $info_lienquan['mota'];
                    $noidung     = $info_lienquan['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_noidung_chitiet .= '<div class="itemnoidung">
               <div class="img"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '"><img onerror="xulyloi(this);" src="' . $hinh . '" alt="' . $ten . '" /></a></div>
               <p class="tieude"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '">' . $ten . '</a></p>
               <span class="mota">' . $mota . '</span>
               <div class="xemchitiet"><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '">' . $arraybien['chitiet'] . '</a></div>
               <div class="clear"></div>
            </div>';
                }
            } else if ($_array_config_noidung['tinlienquan_thumb'] == 0) {
                // end if thumb
                $data_noidung_chitiet .= '<ul class="tinlienquan">';
                foreach ($d_noidung_lienquan as $key_noidung_lienquan => $info_lienquan) {
                    $ten         = $info_lienquan['ten'];
                    $hinh        = ROOT_PATH . 'uploads/noidung/thumb/' . $info_lienquan['hinh'];
                    $ngaycapnhat = $info_lienquan['ngaycapnhat'];
                    $tieude      = $info_lienquan['tieude'];
                    $url         = $info_lienquan['url'];
                    $link        = $info_lienquan['link'];
                    $target      = $info_lienquan['target'];
                    $mota        = $info_lienquan['mota'];
                    $noidung     = $info_lienquan['noidung'];
                    $lienket     = ROOT_PATH . $url;
                    if ($link != '') {
                        $lienket      = $link;
                        $target_value = ' target="' . $target . '" ';
                    }
                    $data_noidung_chitiet .= '<li><a href="' . $lienket . '" ' . $target_value . ' title="' . $ten . '"><i class="fa fa-angle-right fa-lg"></i> ' . $ten . '</a></li>';
                }
                $data_noidung_chitiet .= '</ul>';
            }
        }
    }
    $data_noidung_chitiet .= '<div class="clear"></div></div>'; // end div noi dung chi tiet
} else {
    $data_noidung_chitiet .= include "application/files/404.php";
}
return $data_noidung_chitiet;
