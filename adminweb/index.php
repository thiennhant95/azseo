<?php

    require dirname(__DIR__).'/configs/inc.php';


$__defaultlang = $db->getNameFromID('tbl_lang', 'id', 'macdinh', 1);
if ($__defaultlang == '') {
    $__defaultlang = $db->getNameFromID('tbl_lang', 'id', 'macdinh', 0);
}
if ( !isset($_SESSION['__defaultlang']) ) {
    $_SESSION['__defaultlang'] = $__defaultlang;
}
// tao session sapxepthutu
if (!isset($_SESSION['sapxepthutu_modules'])) {
    $_SESSION['sapxepthutu_modules'] = '';
}
$_number_item = 1;

if (!function_exists('ws_get') || !function_exists('ws_post')) {
    exit('File ws_function.php not found !');
}

$__getop     = ws_get('op');
$__getmethod = ws_get('method');
$__post_task = ws_post('task');
$__getid     = ws_get('id');
$__postid    = ws_post('id');
$__getaction = ws_get('action');
$__getpage   = ws_get('page');
$__getsort   = ws_get('sort');
$__getidtype = ws_get('idtype');
$__gettukhoa = ws_get('tukhoa');
$__getanhien = ws_get('anhien');
$__sortname  = ws_get('sortname');

if ($__getsort == 'asc') {
    $__sortcurent = 'desc';
} else {
    $__sortcurent = 'asc';
}
// link page va link sort
$path_page = './?op='.$__getop.'&idtype='.$__getidtype.'&tukhoa='.$__gettukhoa.'&anhien='.$__getanhien.'&sort='.$__getsort.'&sortname='.$__sortname.'&page=';
$path_sort = './?op='.$__getop.'&idtype='.$__getidtype.'&tukhoa='.$__gettukhoa.'&anhien='.$__getanhien.'&page='.$__getpage.'&sort='.$__sortcurent;
// option cho de quy select
$option1 = '<option value="">Vui lòng chọn...</option>';
// luu session pagekhi vao trang
if ($_SESSION['__limit'] == '' or !isset($_SESSION['__limit']) or $_SESSION['__limit'] > 11) {
    $_SESSION['__limit'] = 2;
}

// logout
if (ws_get('logout')) {
    unset($_SESSION['user_supperadmin']);
    unset($_SESSION['tendangnhap']);
    unset($_SESSION['matkhau']);
    unset($_SESSION['user_id']);
    unset($_SESSION['user_hotendem']);
    unset($_SESSION['user_ten']);
    unset($_SESSION['user_gioitinh']);
    unset($_SESSION['user_email']);
    unset($_SESSION['user_diachi']);
    unset($_SESSION['user_tinhthanh']);
    unset($_SESSION['user_sodienthoai']);
    unset($_SESSION['user_ngaysinh']);
    unset($_SESSION['user_website']);
    unset($_SESSION['user_yahoo']);

    header('location: login.php');
}

// kiem tra dnag nhap
if (ws_get('checkLogin')) {
    $tendangnhap = strip_tags(ws_post('email'));
    $matkhau = strip_tags(ws_post('matkhau'));
    if (!empty($tendangnhap) && !empty($matkhau)) {
        $matkhau = md5(ws_post('matkhau'));
        // kiem tra login
        $checkLogin = $db->checkLogin($tendangnhap, $matkhau);
        if (count($checkLogin) == 1) {
            session_regenerate_id();
            $_SESSION['tendangnhap']      = $tendangnhap;
            $_SESSION['matkhau']          = $matkhau;
            $_SESSION['user_id']          = $checkLogin[0]['id'];
            $_SESSION['user_hotendem']    = $checkLogin[0]['hotendem'];
            $_SESSION['user_supperadmin'] = $checkLogin[0]['supperadmin'];
            $_SESSION['user_ten']         = $checkLogin[0]['ten'];
            $_SESSION['user_gioitinh']    = $checkLogin[0]['gioitinh'];
            $_SESSION['user_email']       = $checkLogin[0]['email'];
            $_SESSION['user_diachi']      = $checkLogin[0]['diachi'];
            $_SESSION['user_tinhthanh']   = $checkLogin[0]['tinhthanh'];
            $_SESSION['user_sodienthoai'] = $checkLogin[0]['sodienthoai'];
            $_SESSION['user_ngaysinh']    = $checkLogin[0]['ngaysinh'];
            $_SESSION['user_website']     = $checkLogin[0]['website'];
            $_SESSION['user_yahoo']       = $checkLogin[0]['yahoo'];
            echo "<script>sefl.location.href='index.php'</script>";
        } else {
            header('location:login.php');
        }
    } else {
        header('location:login.php');
    }
}

// check login moi lan load page
if (count($db->checkLogin($_SESSION['tendangnhap'], $_SESSION['matkhau'])) == 0) {
    header('location:login.php');
}

?>
<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Quản trị website</title>

<!-- library Jquery & Upgrade -->
<script type="text/javascript" src="application/templates/js/jquery-1.12.1.min-gop.js"></script>


<!-- jQueryUI -->
<script src="plugin/jquery-ui/jquery-ui.min.js"></script>
<script src="plugin/jquery-ui/datepicker-vi.js"></script>
<link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.structure.min.css">
<link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.theme.min.css">

<script type="text/javascript" src="application/templates/js/bootstrap-carousel-sweetalert.min.js"></script>

<!-- CSS-mergre -->
<link rel="stylesheet" href="application/templates/css/bootstrap-awesome-animate-hover.min.css">

<!-- Dropzon -->
<script type="text/javascript" src="application/templates/js/before_dropzon.js"></script>
<script type="text/javascript" src="plugin/dropzon/js/dropzone.js"></script>
<link rel="stylesheet" href="plugin/dropzon/css/basic.css" />
<link rel="stylesheet" href="plugin/dropzon/css/_dropzone.css" />
<script type="text/javascript">
    Dropzone.autoDiscover = false;
</script>

<!-- SELECT2 -->
<link rel="stylesheet" href="plugin/select2/css/select2.min.css" />
<script type="text/javascript" src="plugin/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="plugin/select2/js/i18n/vi.js"></script>

<link rel="stylesheet" type="text/css" href="application/templates/css/css.css" />
<link rel="stylesheet" type="text/css" href="application/templates/css/cssv2.css" />
<link rel="stylesheet" type="text/css" href="application/templates/css/responsive.css" />
<!-- script -->
<script type="application/javascript" src="application/templates/js/javascript.js" ></script>
<script type="application/javascript" src="application/templates/js/ajax.js" ></script>

<!-- menu -->
<link rel="stylesheet" type="text/css" href="plugin/cssmenu/styles.css" />
<script type="application/javascript" src="plugin/cssmenu/script.js" ></script>
<!-- menu -->

<!-- table scroll header -->
<script type="application/javascript" src="application/templates/js/jquery.fixedtableheader.min.js" ></script>
<!-- ckeditor -->
<script type="text/javascript" language="javascript" src="plugin/fckeditor/ckeditor/ckeditor.js" ></script>

<!-- jscolor -->
<script src="plugin/jscolor/jscolor.min.js" type="text/javascript"></script>

<!-- bootstrapSelect -->
<link rel=stylesheet href="../plugins/bootstrap-select/css/bootstrap-select.min.css" />
<script src="../plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<!-- notifyJS -->
<script src="plugin/notify/notify.min.js"></script>

<link rel="stylesheet" href="../plugins/data-table/jquery.dataTables.min.css">
<script src="../plugins/data-table/jquery.dataTables.min.js"></script>

</head>
<body>
<?php
// set action của user là them, xoa, sua
$__them  = 1;
$__xoa   = 1;
$__sua   = 1;
// neu user là supperadmin thi có toàn quyền truy cập và ngược lại thì mới kiểm tra phân quyền user
if ($_SESSION['user_supperadmin'] == 0) {
    // kiem tra xem có quyền truy cập modules hay không?
    // lay id modules dang truy cap
    $__getidmodules = $db->getNameFromID('tbl_user_group', 'id', 'link', "'./?op=".$__getop."'");
    if ($__getidmodules != '') {
        $s_check_phanquyenuser = "select them,xoa,sua from tbl_user_active where idtype = '".$__getidmodules."' and iduser = '".$_SESSION['user_id']."' and active = 1";
        $d_check_phanquyenuser = $db->sqlSelectSql($s_check_phanquyenuser);
        if (count($d_check_phanquyenuser) > 0) {
            $__them = $d_check_phanquyenuser[0]['them'];
            $__xoa = $d_check_phanquyenuser[0]['xoa'];
            $__sua = $d_check_phanquyenuser[0]['sua'];
        } else {
            echo '<script>alert("'.$arraybien['khongcoquyen'].'");
         window.location.href="./";
         </script>';
            exit();
        }
    }
}
?>
<!-- wapper_main -->
<div id="wapper_main">
   <!-- row_top -->
   <div id="row_top">
      <div class="logo"><img src="https://azseo.vn/wp-content/uploads/2018/06/logo-azseo-2.png" height="35px" alt="Logo"/>

<div style="color:#CCC;font-size:10px;">
<?php
$date = new DateTime("now", new DateTimeZone('Asia/Ho_Chi_Minh') );
echo $date->format('H:i:s d/m/Y');
?>
</div>
	  </div>
        <div class="logo btn " style="margin-left:10px;"><a title="Xem website" target="_blank" href="../"><i class="fa fa-2x fa-external-link color-white"></i></a></div>
        <!-- START QUICK SEARCH -->
         <div class="search-everything form-group col-sm-push-1 col-sm-5">
              <input type="text" class="form-control" id="timnhanh-admin" data-chontim="danhmuc" placeholder="Bạn có thể tìm nhanh sản phẩm, nội dung, danh mục" data-url="<?php echo @$_SESSION['ROOT_PATH']; ?>">
            <div class="suggest">
               <ul></ul>
            </div>
         </div>
         <!-- END QUICK SEARCH -->
        <div class="btn-group open usergroup">
          <a class="btn btn-primary" href="#"><i class="fa fa-user fa-fw"></i> <?php echo $arraybien['xinchao']; ?> <?php if ($_SESSION['user_gioitinh'] == 0) {
    echo $arraybien['anh'];
} else {
    echo $arraybien['chi'];
}
echo '&nbsp;'.$_SESSION['user_ten']; ?></a>
          <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="fa fa-caret-down"></span></a>
          <ul class="dropdown-menu">
            <li><a href="./?op=doithongtin&method=frm"><i class="fa fa-pencil fa-fw"></i> <?php echo $arraybien['capnhatthongtin']; ?></a></li>
            <li><a href="./?logout=1"><i class="fa fa-ban fa-fw"></i> <?php echo $arraybien['thoat']; ?></a></li>
          </ul>
        </div>
   <div class="clear"></div>
    </div>
    <!-- end row_top -->
    <!-- row_menu -->
   <div id="row_menu">
      <div id='cssmenu'>
      <?php
$sql2 = "
SELECT a.id,a.link, b.ten
FROM tbl_user_group As a
INNER JOIN tbl_user_group_lang AS b
ON a.id = b.iddanhmuc  ";
if ($_SESSION['user_supperadmin'] == 0) {
    $sql2 .= "
    INNER JOIN tbl_user_active AS c
    ON a.id = c.idtype";
}
$sql2 .= "
WHERE a.anhien = 1 AND b.idlang = '{$__defaultlang}' ";
if ($_SESSION['user_supperadmin'] == 0) {
    $sql2 .= "
    AND c.iduser = '{$_SESSION['user_id']}'
    AND c.active = 1 ";
}
$sql2 .= " order by a.thutu asc";

echo $db->menuDequyAdminSql(4, $sql2, 'ten', 'id', ''); ?>
        </div>
    </div>
    <!-- end row_menu -->
    <!-- Thông báo -->
    <div id="alert_placeholder"></div>
   <!-- Ads -->
   <div id="row_content"><?php
$module = 'modules/';
if ($__getop == '') {
    $module = $module.'home/home.php';
} else {
    $module .= $__getop.'/'.$__getop;
    $module = $module.'.php';
}
if (!file_exists($module)) {
    $module = '';
    $module = 'modules/home/home.php';
}
include $module;
?>
    </div>
    <!-- end row_content -->
    <!-- row_bottom -->
   <div id="row_bottom">
      Webso.vn | Kết Nối Doanh Nghiệp - Vươn Tới Thành Công
    </div>
    <!-- end row_bottom -->
</div>
<!-- end wapper_main -->
<footer class="footer">
   <div id="feedback-box" class="clearfix">
      <a href="#" data-toggle="modal" data-target="#myModal2" class="md-trigger report-icon" title="feedback">
         <i class="fa fa-flag">
            <span>Phản hồi</span>
         </i>
      </a>
   </div>
<!-- Modal -->
<div class="modal zoom" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header maunenxanh">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Thông báo lỗi</h4>
      </div>
      <div class="modal-body" id="feedback_frm">
      <div class="form-group clearfix">
         <label for="input" class="col-sm-3 control-label">Họ tên:
         <span class="star">*</span></label>
         <div class="col-sm-9">
            <input type="text" name="hoten" class="form-control" value="" required />
         </div>
      </div>
      <div class="form-group clearfix">
         <label for="input" class="col-sm-3 control-label">Số điện thoại:</label>
         <div class="col-sm-9">
            <input type="text" name="dienthoai" class="form-control" value="" />
         </div>
      </div>
      <div class="form-group clearfix">
         <label for="input" class="col-sm-3 control-label">Email:</label>
         <div class="col-sm-9">
            <input type="text" name="email" class="form-control" value="" />
         </div>
      </div>
      <div class="form-group clearfix">
         <label for="input" class="col-sm-3 control-label">Nội dung:
         <span class="star">*</span></label>
         <div class="col-sm-9">
            <textarea name="noidung" class="form-control" rows="3" required></textarea>
         </div>
      </div>
      <div class="form-group clearfix">
         <label for="input" class="col-sm-3 control-label">Captcha:
         <span class="star">*</span></label>
         <div class="col-sm-9">
            <div class="col-sm-4" style="padding: 0;">
               <img src="plugin/captcha/image.php" id="img-captcha"/>
            </div>
            <div class="col-sm-6">
               <input type="text" class="form-control" name="captcha" id="captcha" value="" required maxlength="3" minlength="3" />
            </div>
            <div class="col-sm-2">
            <a onclick="$('#img-captcha').attr('src', 'plugin/captcha/image.php?rand=' + Math.random())" title="reload"><img src="application/templates/images/reload.png" style="width: 30px; height: 30px; cursor: pointer;"></a>
            </div>
         </div>
      </div>
        <input type="hidden" name="getop" value="<?php echo $__getop; ?>" placeholder="" />
        <input type="hidden" name="getmethod" value="<?php echo $__getmethod; ?>" placeholder="" />
        <input type="hidden" name="getlang" value="<?php echo $__defaultlang; ?>" placeholder="" />
        <?php $timezone = +7; ?>
        <input type="hidden" name="getthoigian" value="<?php echo gmdate('H:i:s A - d/m/Y ', time() + 3600 * ($timezone + date('I'))); ?>" />
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
         <button type="submit" id="submit_feedback" class="btn btn-primary">Gửi yêu cầu</button>
      </div>
    </div>
  </div>
</div>
<div id="alert_error"><?php echo $arraybien['capnhatthanhcong']; ?></div>
<div id="loading"></div>
<script type="text/javascript" src="application/templates/js/load.js"></script>
</footer>
</body>
</html>
